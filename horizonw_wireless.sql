-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2021 at 12:12 PM
-- Server version: 5.7.33
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `horizonw_wireless`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessory_brands`
--

CREATE TABLE `accessory_brands` (
  `id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `accessories` int(10) DEFAULT '0',
  `image` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accessory_brands`
--

INSERT INTO `accessory_brands` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `accessories`, `image`) VALUES
(25, 'ZIZO', 'zizo', 1, NULL, 1, 1, NULL),
(26, 'Incipio', 'incipio', 1, NULL, 0, 0, NULL),
(27, 'Kate Spade', 'kate-spade', 1, NULL, 0, 0, NULL),
(28, 'Speck', 'speck', 1, NULL, 0, 0, NULL),
(31, 'Zuve', 'zuve', 1, NULL, 0, 0, NULL),
(32, 'HAVIT', 'havit', 1, NULL, 0, 0, NULL),
(33, 'Aegis', 'aegis', 1, NULL, 0, 0, NULL),
(36, 'Max Power', 'max-power', 1, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accessory_types`
--

CREATE TABLE `accessory_types` (
  `id` int(191) NOT NULL,
  `accessory_brand_id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accessory_types`
--

INSERT INTO `accessory_types` (`id`, `accessory_brand_id`, `name`, `slug`, `status`) VALUES
(74, 25, 'Bolt Series', 'bolt-series', 1),
(75, 25, 'Transform Series', 'transform-series', 1),
(76, 25, 'Refine Series', 'refine-series', 1),
(77, 25, 'Pulse', 'pulse', 1),
(78, 32, 'E58P', 'e58p', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(191) NOT NULL DEFAULT '0',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `role_id`, `photo`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `shop_name`) VALUES
(1, 'Admin', 'admin@gmail.com', '713 988 6565', 0, '1556780563user.png', '$2y$10$p35S2FczpEfpbe41CX4j4.XE548tHBtF5weGLPxZ56MX5dsOFtaCC', 1, 'ZZVqOYgMesElyczFbnpRQE1O7fjBtxvF3MR0mJZhHFUyVmNtUTl6D5hO9vEJ', '2018-02-28 23:27:08', '2020-06-12 06:06:07', 'Horizon Wireless'),
(5, 'Mr Mamun', 'mamun@gmail.com', '34534534', 17, '1568803644User.png', '$2y$10$3AEjcvFBiQHECgtH9ivXTeQZfMf.rw318G820TtVBsYaCt7UNOwGC', 1, NULL, '2019-09-18 04:47:24', '2019-09-18 21:21:49', NULL),
(6, 'Mr. Manik', 'manik@gmail.com', '5079956958', 18, '1568863361user-admin.png', '$2y$10$Z3Jx5jHjV2m4HtZHzeaKMuwxkLAKfJ1AX3Ed5MPACvFJLFkEWN9L.', 1, NULL, '2019-09-18 21:22:41', '2019-09-18 21:22:41', NULL),
(7, 'Mr. Pratik', 'pratik@gmail.com', '34534534', 16, '1568863396user-admin.png', '$2y$10$u.93l4y6wOz6vq3BlAxvU.LuJ16/uBQ9s2yesRGTWUtLRiQSwoH1C', 1, 'iZPbEaxjSWBJMvncLqeMtAQsG7VoSirVMJ1EBfdJogvgXK2DM5mw236fBCOq', '2019-09-18 21:23:16', '2019-09-18 21:23:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_languages`
--

CREATE TABLE `admin_languages` (
  `id` int(191) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rtl` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_languages`
--

INSERT INTO `admin_languages` (`id`, `is_default`, `language`, `file`, `name`, `rtl`) VALUES
(1, 1, 'English', '1567232745AoOcvCtY.json', '1567232745AoOcvCtY', 0),
(2, 0, 'RTL English', '1584887310NzfWDhO8.json', '1584887310NzfWDhO8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_conversations`
--

CREATE TABLE `admin_user_conversations` (
  `id` int(191) NOT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` enum('Ticket','Dispute') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_conversations`
--

INSERT INTO `admin_user_conversations` (`id`, `subject`, `user_id`, `message`, `created_at`, `updated_at`, `type`, `order_number`) VALUES
(1, 'Order Confirmation', 22, 'rfgdfgfd', '2020-01-21 01:18:38', '2020-01-21 01:18:38', 'Ticket', NULL),
(2, 'test', 30, 'hello shayan', '2020-06-16 05:57:07', '2020-06-16 05:57:07', NULL, NULL),
(3, 'TEST', 32, 'TEST', '2020-11-11 06:47:33', '2020-11-11 06:47:33', NULL, NULL),
(4, 'Hello There', 31, 'lkb', '2020-11-18 23:34:38', '2020-11-18 23:34:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_messages`
--

CREATE TABLE `admin_user_messages` (
  `id` int(191) NOT NULL,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_messages`
--

INSERT INTO `admin_user_messages` (`id`, `conversation_id`, `message`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'rfgdfgfd', 22, '2020-01-21 01:18:38', '2020-01-21 01:18:38'),
(2, 2, 'hello shayan', NULL, '2020-06-16 05:57:07', '2020-06-16 05:57:07'),
(3, 3, 'TEST', NULL, '2020-11-11 06:47:33', '2020-11-11 06:47:33'),
(4, 4, 'lkb', NULL, '2020-11-18 23:34:38', '2020-11-18 23:34:38');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `attributable_id` int(11) DEFAULT NULL,
  `attributable_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `input_name` varchar(255) DEFAULT NULL,
  `price_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `details_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(191) NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Large','TopSmall','BottomSmall') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `photo`, `link`, `type`) VALUES
(1, '1568889151top2.jpg', 'https://www.google.com/', 'TopSmall'),
(2, '1568889146top1.jpg', NULL, 'TopSmall'),
(3, '1568889164bottom1.jpg', 'https://www.google.com/', 'Large'),
(4, '1564398600side-triple3.jpg', 'https://www.google.com/', 'BottomSmall'),
(5, '1564398579side-triple2.jpg', 'https://www.google.com/', 'BottomSmall'),
(6, '1564398571side-triple1.jpg', 'https://www.google.com/', 'BottomSmall');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(191) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `meta_tag` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `title`, `details`, `photo`, `source`, `views`, `status`, `meta_tag`, `meta_description`, `tags`, `created_at`) VALUES
(9, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700986-min.jpg', 'www.geniusocean.com', 36, 1, 'b1,b2,b3', 'Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level.', 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-02-06 09:53:41'),
(10, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700902-min.jpg', 'www.geniusocean.com', 14, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-03-06 09:54:21'),
(12, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700821-min.jpg', 'www.geniusocean.com', 19, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-04-06 22:04:20'),
(13, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700676-min.jpg', 'www.geniusocean.com', 57, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-05-06 22:04:36'),
(14, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700595-min.jpg', 'www.geniusocean.com', 3, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-06-03 06:02:30'),
(15, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700464-min.jpg', 'www.geniusocean.com', 6, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-07-03 06:02:53'),
(16, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700383-min.jpg', 'www.geniusocean.com', 5, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-08-03 06:03:14'),
(17, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700322-min.jpg', 'www.geniusocean.com', 50, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:37'),
(18, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700251-min.jpg', 'www.geniusocean.com', 151, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:59'),
(20, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542699136-min.jpg', 'www.geniusocean.com', 10, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-08-03 06:03:14'),
(21, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542699045-min.jpg', 'www.geniusocean.com', 36, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:37'),
(22, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542698954-min.jpg', 'www.geniusocean.com', 68, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:59'),
(23, 7, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542698893-min.jpg', 'www.geniusocean.com', 5, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-08-03 06:03:14'),
(24, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542698832-min.jpg', 'www.geniusocean.com', 34, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:37');
INSERT INTO `blogs` (`id`, `category_id`, `title`, `details`, `photo`, `source`, `views`, `status`, `meta_tag`, `meta_description`, `tags`, `created_at`) VALUES
(25, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15557542831-min.jpg', 'www.geniusocean.com', 40, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`) VALUES
(2, 'Oil & gas', 'oil-and-gas'),
(3, 'Manufacturing', 'manufacturing'),
(4, 'Chemical Research', 'chemical_research'),
(5, 'Agriculture', 'agriculture'),
(6, 'Mechanical', 'mechanical'),
(7, 'Entrepreneurs', 'entrepreneurs'),
(8, 'Technology', 'technology');

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `name`, `slug`, `status`, `photo`) VALUES
(1, 'Boost Mobile', 'boost-mobile', 1, 'boost_mobile.png'),
(2, 'Sprint', 'sprint', 1, 'sprint.png'),
(3, 'Verizon', 'verizon', 1, 'verizon.png'),
(4, 'AT&T', 'at-t', 1, 'at_and_t.png'),
(5, 'T-Mobile', 't-mobile', 1, 't_mobile.png'),
(6, 'Metro by T-Mobile', 'metro-by-t-mobile', 1, 'metro_by_t.png'),
(7, 'Cricket Wireless', 'cricket-wireless', 1, 'cricket_wireless.png');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT '0',
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devices` int(10) DEFAULT '0',
  `brands` int(10) DEFAULT '0',
  `carriers` int(10) DEFAULT '0',
  `accessories` int(10) DEFAULT '0',
  `arrivals` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `image`, `devices`, `brands`, `carriers`, `accessories`, `arrivals`) VALUES
(44, 'Apple', 'apple', 1, '1603990596device_32.png', 1, '159732661111-pro.png', 1, 1, 0, 1, 0),
(45, 'Samsung', 'samsung', 1, '1603992775device_6.png', 1, '1597326594s20-ultra.png', 1, 1, 0, 1, 0),
(46, 'LG', 'lg', 1, '1604002235device_4.png', 1, '1597326570LG-G8-ThinQ.png', 1, 1, 0, 1, 0),
(47, 'Motorola', 'motorola', 1, '1604002258motorola.png', 1, '1597326553Moto-G-Stylus.png', 1, 1, 0, 0, 0),
(48, 'Google', 'google', 1, '1603992691device_48.png', 1, '1597326523Google-Pixel-4-XL.png', 1, 1, 0, 0, 0),
(49, 'All', 'all', 1, NULL, 0, NULL, 0, 0, 0, 0, 0),
(50, 'Acellories', 'acellories', 1, '1604346656accellories.png', 0, NULL, 0, 1, 0, 0, 0),
(51, 'ADATA', 'adata', 1, '1604346710a_data.png', 0, NULL, 0, 1, 0, 0, 0),
(52, 'Airium', 'airium', 1, '1604346735airium.png', 0, NULL, 0, 1, 0, 0, 0),
(53, 'Cellhelmet', 'cellhelmet', 1, '1604346760cellhelmet.png', 0, NULL, 0, 1, 0, 0, 0),
(54, 'Axess', 'axess', 1, '1604346783axess.png', 0, NULL, 0, 1, 0, 0, 0),
(55, 'Valor', 'valor', 1, '1604346828valor.png', 0, NULL, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cat_types`
--

CREATE TABLE `cat_types` (
  `id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat_types`
--

INSERT INTO `cat_types` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `image`) VALUES
(19, 'Phones', 'phones', 1, NULL, 0, NULL),
(20, 'Accessories', 'accessories', 1, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cat_type_children`
--

CREATE TABLE `cat_type_children` (
  `id` int(191) NOT NULL,
  `cat_type_id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `accessories` int(10) DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat_type_children`
--

INSERT INTO `cat_type_children` (`id`, `cat_type_id`, `name`, `slug`, `status`, `accessories`, `image`) VALUES
(13, 20, 'Cases/Screen Protector', 'cases-screen-protector', 1, 1, '1602603895cases.png'),
(14, 20, 'Charger/Power', 'charger-power', 1, 1, '1602603880charger.png'),
(15, 20, 'Headphones', 'headphones', 1, 1, '1602603871bluetooth.png'),
(16, 20, 'Leather Pouches', 'leather-pouches', 1, 1, '1602603850pouches.png'),
(17, 20, 'Memory', 'memory', 1, 1, '1602603827memory.png'),
(18, 20, 'Apple watch', 'wearable', 1, 1, '1602603816smartwatch.png'),
(19, 20, 'Other Accessories', 'other-accessories', 1, 1, '1602603805others.png'),
(20, 20, 'Handsfree', 'handsfree', 1, 0, '1602603792handsfree.png'),
(21, 20, 'Phones', 'phones-accessory', 1, 0, '1603733402phones-accessory.png'),
(23, 20, 'Cables', 'cables-accessory', 1, 0, '1603735416power-cable.png'),
(25, 19, 'Mounts', 'mount-accessory', 1, 0, '1603738959mount-accessory.png'),
(26, 20, 'Bluetooth', 'bluetooth-accessory', 1, 0, '1603739045bluetooth-accessory.png'),
(27, 20, 'APPLE AIRPODS', 'airpods-accessory', 1, 0, '1603741330airpods-512.png'),
(28, 20, 'TEMPERED GLASS', 'tempered-glass-protectors-accessory', 1, 0, '1603741437telephone-cartoon-png-download-1024681-free-transparent-tempered-glass-png-900_600.png'),
(29, 20, 'TABLETS', 'tablets-accessory', 1, 0, '1603741593content-marketing.png'),
(30, 20, 'SPEAKERS', 'speakers-accessory', 1, 0, '1603741709speaker.png'),
(31, 20, 'POWER BANKS', 'power-banks-accessory', 1, 0, '1603742179power-bank.png'),
(32, 20, 'STANDS & GRIPS', 'stands-and-grips-accessory', 1, 0, '1603742283Phone_accessory-58-512.png'),
(33, 20, 'DISPLAYS', 'displays-accessory', 1, 0, '1605545546power-cable.png');

-- --------------------------------------------------------

--
-- Table structure for table `childcategories`
--

CREATE TABLE `childcategories` (
  `id` int(191) NOT NULL,
  `subcategory_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(191) NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(191) NOT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) NOT NULL,
  `recieved_user` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `counters`
--

CREATE TABLE `counters` (
  `id` int(11) NOT NULL,
  `type` enum('referral','browser') NOT NULL DEFAULT 'referral',
  `referral` varchar(255) DEFAULT NULL,
  `total_count` int(11) NOT NULL DEFAULT '0',
  `todays_count` int(11) NOT NULL DEFAULT '0',
  `today` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counters`
--

INSERT INTO `counters` (`id`, `type`, `referral`, `total_count`, `todays_count`, `today`) VALUES
(1, 'referral', 'www.facebook.com', 5, 0, NULL),
(2, 'referral', 'geniusocean.com', 2, 0, NULL),
(3, 'browser', 'Windows 10', 6774, 0, NULL),
(4, 'browser', 'Linux', 1289, 0, NULL),
(5, 'browser', 'Unknown OS Platform', 15891, 0, NULL),
(6, 'browser', 'Windows 7', 1585, 0, NULL),
(7, 'referral', 'yandex.ru', 19, 0, NULL),
(8, 'browser', 'Windows 8.1', 861, 0, NULL),
(9, 'referral', 'www.google.com', 1820, 0, NULL),
(10, 'browser', 'Android', 1892, 0, NULL),
(11, 'browser', 'Mac OS X', 1491, 0, NULL),
(12, 'referral', 'l.facebook.com', 50, 0, NULL),
(13, 'referral', 'codecanyon.net', 6, 0, NULL),
(14, 'browser', 'Windows XP', 113, 0, NULL),
(15, 'browser', 'Windows 8', 50, 0, NULL),
(16, 'browser', 'iPad', 58, 0, NULL),
(17, 'browser', 'Ubuntu', 219, 0, NULL),
(18, 'browser', 'iPhone', 1283, 0, NULL),
(19, 'referral', 'www.google.co.jp', 23, 0, NULL),
(20, 'referral', 'horizonwireless.myshopify.com', 8, 0, NULL),
(21, 'referral', 'duckduckgo.com', 21, 0, NULL),
(22, 'referral', 'anti-crisis-seo.com', 10, 0, NULL),
(23, 'referral', 'l.instagram.com', 25, 0, NULL),
(24, 'referral', 'www.bing.com', 68, 0, NULL),
(25, 'referral', 'mediacomtoday.com', 1, 0, NULL),
(26, 'referral', 'www.google.ca', 17, 0, NULL),
(27, 'referral', 'l.wl.co', 45, 0, NULL),
(28, 'referral', 'indyhealthnet.org', 21, 0, NULL),
(29, 'referral', 'www.groupon.com', 4, 0, NULL),
(30, 'referral', 'www.shcrose.com', 1, 0, NULL),
(31, 'browser', 'Windows Vista', 33, 0, NULL),
(32, 'referral', 'google.com', 106, 0, NULL),
(33, 'referral', 'www.beinewsecurities.com', 1, 0, NULL),
(34, 'referral', 'global.bing.com', 1, 0, NULL),
(35, 'referral', 'www.netcraft.com', 3, 0, NULL),
(36, 'referral', 'baidu.com', 50, 0, NULL),
(37, 'referral', 'www.lamberthome.top', 1, 0, NULL),
(38, 'browser', 'Windows 2000', 23, 0, NULL),
(39, 'referral', 'search.yahoo.com', 14, 0, NULL),
(40, 'referral', 'www.google.de', 5, 0, NULL),
(41, 'referral', 'lm.facebook.com', 6, 0, NULL),
(42, 'referral', 'www.imdpm.net', 1, 0, NULL),
(43, 'referral', NULL, 34, 0, NULL),
(44, 'referral', 'server.horizonwirelesstx.com', 3, 0, NULL),
(45, 'referral', 'www.google.com.au', 1, 0, NULL),
(46, 'referral', 'hnlmh.pctip.net', 1, 0, NULL),
(47, 'referral', 'objcj.rfesc.net', 1, 0, NULL),
(48, 'referral', 'buhba.hxfoot.com', 1, 0, NULL),
(49, 'referral', 'czapsp.hxfoot.com', 1, 0, NULL),
(50, 'referral', 'yqkg.meansung.com', 1, 0, NULL),
(51, 'referral', 'xhcp.meansung.com', 1, 0, NULL),
(52, 'referral', 'www.google.com.hk', 2, 0, NULL),
(53, 'referral', 'www.shopify.com', 3, 0, NULL),
(54, 'referral', 'search.aol.com', 1, 0, NULL),
(55, 'referral', 'www.linkedin.com', 12, 0, NULL),
(56, 'referral', 'r.search.yahoo.com', 4, 0, NULL),
(57, 'referral', 'pkw.lightenupnh.org', 1, 0, NULL),
(58, 'referral', 'acrhxp.lightenupnh.org', 1, 0, NULL),
(59, 'referral', 'www.google.com.pk', 2, 0, NULL),
(60, 'referral', 'odlr.prizesk.com', 1, 0, NULL),
(61, 'referral', 'sucuri.net', 2, 0, NULL),
(62, 'referral', 'sov.skfusion.com', 1, 0, NULL),
(63, 'referral', 'search.tb.ask.com', 1, 0, NULL),
(64, 'referral', 'www.horizonwirelesstx.com', 5, 0, NULL),
(65, 'referral', 'com.google.android.googlequicksearchbox', 1, 0, NULL),
(66, 'referral', 'searchassist.verizon.com', 1, 0, NULL),
(67, 'referral', 'brzeg-3009691323.w-poblizu.pl', 2, 0, NULL),
(68, 'referral', 'www.google.co.in', 3, 0, NULL),
(69, 'referral', 'www.google.com.pr', 1, 0, NULL),
(70, 'referral', 'search.earthlink.net', 2, 0, NULL),
(71, 'referral', 'www.ediblearrangements.com', 1, 0, NULL),
(72, 'referral', 'HORIZONWIRELESSTX.COM', 26, 0, NULL),
(73, 'referral', 'www.coralbruce.top', 1, 0, NULL),
(74, 'referral', 'www.google.com.sg', 1, 0, NULL),
(75, 'referral', 'vyolw.ammgm.org', 1, 0, NULL),
(76, 'referral', ' www.horizonwirelesstx.com', 8, 0, NULL),
(77, 'referral', ' horizonwirelesstx.com', 4, 0, NULL),
(78, 'referral', 'www.maaco.com', 1, 0, NULL),
(79, 'referral', 'www.google.cl', 2, 0, NULL),
(80, 'referral', 'hugwv.fxrst.com', 1, 0, NULL),
(81, 'referral', 'www.google.fr', 3, 0, NULL),
(82, 'referral', 'greencitizen.com', 2, 0, NULL),
(83, 'referral', 'bmbu.immobilieralgerie.net', 1, 0, NULL),
(84, 'referral', 'www.google.co.uk', 3, 0, NULL),
(85, 'referral', 'www.yahoo.com', 2, 0, NULL),
(86, 'referral', 'fbkwriter.com', 2, 0, NULL),
(87, 'referral', '67.200.159.253', 10, 0, NULL),
(88, 'referral', 'pfspvb.pjty9.com', 1, 0, NULL),
(89, 'referral', 'www.google.ro', 2, 0, NULL),
(90, 'referral', 'nortonsafe.search.ask.com', 1, 0, NULL),
(91, 'referral', 'www.google.com.br', 4, 0, NULL),
(92, 'referral', 'www.google.com.tw', 1, 0, NULL),
(93, 'referral', 'neethome.com', 5, 0, NULL),
(94, 'referral', 'xqig.makenahartlin.com', 1, 0, NULL),
(95, 'referral', 'www.google.com.mx', 1, 0, NULL),
(96, 'referral', 't.co', 12, 0, NULL),
(97, 'referral', 'site.ru', 1, 0, NULL),
(98, 'browser', 'Windows Server 2003/XP x64', 1, 0, NULL),
(99, 'referral', 'www.google.co.nz', 1, 0, NULL),
(100, 'referral', 'vurg.csucces.net', 1, 0, NULL),
(101, 'referral', 'yyei.wzhek.com', 1, 0, NULL),
(102, 'referral', 'www.google.com.pe', 1, 0, NULL),
(103, 'referral', 'www.google.co.ve', 1, 0, NULL),
(104, 'referral', 'www.horizonwirelesstx.com.', 1, 0, NULL),
(105, 'referral', 'start.duckduckgo.com', 1, 0, NULL),
(106, 'referral', 'wcwt.semxiu.com', 1, 0, NULL),
(107, 'referral', 'www.google.nl', 1, 0, NULL),
(108, 'referral', 'www.google.it', 1, 0, NULL),
(109, 'referral', 'www.google.se', 1, 0, NULL),
(110, 'referral', 'bctc.yqpc.net', 1, 0, NULL),
(111, 'referral', 'www.google.co.il', 1, 0, NULL),
(112, 'referral', 'www.lifttruckinc.com', 1, 0, NULL),
(113, 'referral', 'ejzxyx.kadoin.com', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CD', 'Democratic Republic of the Congo'),
(50, 'CG', 'Republic of Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DK', 'Denmark'),
(58, 'DJ', 'Djibouti'),
(59, 'DM', 'Dominica'),
(60, 'DO', 'Dominican Republic'),
(61, 'TP', 'East Timor'),
(62, 'EC', 'Ecuador'),
(63, 'EG', 'Egypt'),
(64, 'SV', 'El Salvador'),
(65, 'GQ', 'Equatorial Guinea'),
(66, 'ER', 'Eritrea'),
(67, 'EE', 'Estonia'),
(68, 'ET', 'Ethiopia'),
(69, 'FK', 'Falkland Islands (Malvinas)'),
(70, 'FO', 'Faroe Islands'),
(71, 'FJ', 'Fiji'),
(72, 'FI', 'Finland'),
(73, 'FR', 'France'),
(74, 'FX', 'France, Metropolitan'),
(75, 'GF', 'French Guiana'),
(76, 'PF', 'French Polynesia'),
(77, 'TF', 'French Southern Territories'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germany'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GK', 'Guernsey'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'IM', 'Isle of Man'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran (Islamic Republic of)'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'CI', 'Ivory Coast'),
(109, 'JE', 'Jersey'),
(110, 'JM', 'Jamaica'),
(111, 'JP', 'Japan'),
(112, 'JO', 'Jordan'),
(113, 'KZ', 'Kazakhstan'),
(114, 'KE', 'Kenya'),
(115, 'KI', 'Kiribati'),
(116, 'KP', 'Korea, Democratic People\'s Republic of'),
(117, 'KR', 'Korea, Republic of'),
(118, 'XK', 'Kosovo'),
(119, 'KW', 'Kuwait'),
(120, 'KG', 'Kyrgyzstan'),
(121, 'LA', 'Lao People\'s Democratic Republic'),
(122, 'LV', 'Latvia'),
(123, 'LB', 'Lebanon'),
(124, 'LS', 'Lesotho'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libyan Arab Jamahiriya'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lithuania'),
(129, 'LU', 'Luxembourg'),
(130, 'MO', 'Macau'),
(131, 'MK', 'North Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MW', 'Malawi'),
(134, 'MY', 'Malaysia'),
(135, 'MV', 'Maldives'),
(136, 'ML', 'Mali'),
(137, 'MT', 'Malta'),
(138, 'MH', 'Marshall Islands'),
(139, 'MQ', 'Martinique'),
(140, 'MR', 'Mauritania'),
(141, 'MU', 'Mauritius'),
(142, 'TY', 'Mayotte'),
(143, 'MX', 'Mexico'),
(144, 'FM', 'Micronesia, Federated States of'),
(145, 'MD', 'Moldova, Republic of'),
(146, 'MC', 'Monaco'),
(147, 'MN', 'Mongolia'),
(148, 'ME', 'Montenegro'),
(149, 'MS', 'Montserrat'),
(150, 'MA', 'Morocco'),
(151, 'MZ', 'Mozambique'),
(152, 'MM', 'Myanmar'),
(153, 'NA', 'Namibia'),
(154, 'NR', 'Nauru'),
(155, 'NP', 'Nepal'),
(156, 'NL', 'Netherlands'),
(157, 'AN', 'Netherlands Antilles'),
(158, 'NC', 'New Caledonia'),
(159, 'NZ', 'New Zealand'),
(160, 'NI', 'Nicaragua'),
(161, 'NE', 'Niger'),
(162, 'NG', 'Nigeria'),
(163, 'NU', 'Niue'),
(164, 'NF', 'Norfolk Island'),
(165, 'MP', 'Northern Mariana Islands'),
(166, 'NO', 'Norway'),
(167, 'OM', 'Oman'),
(168, 'PK', 'Pakistan'),
(169, 'PW', 'Palau'),
(170, 'PS', 'Palestine'),
(171, 'PA', 'Panama'),
(172, 'PG', 'Papua New Guinea'),
(173, 'PY', 'Paraguay'),
(174, 'PE', 'Peru'),
(175, 'PH', 'Philippines'),
(176, 'PN', 'Pitcairn'),
(177, 'PL', 'Poland'),
(178, 'PT', 'Portugal'),
(179, 'PR', 'Puerto Rico'),
(180, 'QA', 'Qatar'),
(181, 'RE', 'Reunion'),
(182, 'RO', 'Romania'),
(183, 'RU', 'Russian Federation'),
(184, 'RW', 'Rwanda'),
(185, 'KN', 'Saint Kitts and Nevis'),
(186, 'LC', 'Saint Lucia'),
(187, 'VC', 'Saint Vincent and the Grenadines'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'Sao Tome and Principe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SK', 'Slovakia'),
(198, 'SI', 'Slovenia'),
(199, 'SB', 'Solomon Islands'),
(200, 'SO', 'Somalia'),
(201, 'ZA', 'South Africa'),
(202, 'GS', 'South Georgia South Sandwich Islands'),
(203, 'SS', 'South Sudan'),
(204, 'ES', 'Spain'),
(205, 'LK', 'Sri Lanka'),
(206, 'SH', 'St. Helena'),
(207, 'PM', 'St. Pierre and Miquelon'),
(208, 'SD', 'Sudan'),
(209, 'SR', 'Suriname'),
(210, 'SJ', 'Svalbard and Jan Mayen Islands'),
(211, 'SZ', 'Swaziland'),
(212, 'SE', 'Sweden'),
(213, 'CH', 'Switzerland'),
(214, 'SY', 'Syrian Arab Republic'),
(215, 'TW', 'Taiwan'),
(216, 'TJ', 'Tajikistan'),
(217, 'TZ', 'Tanzania, United Republic of'),
(218, 'TH', 'Thailand'),
(219, 'TG', 'Togo'),
(220, 'TK', 'Tokelau'),
(221, 'TO', 'Tonga'),
(222, 'TT', 'Trinidad and Tobago'),
(223, 'TN', 'Tunisia'),
(224, 'TR', 'Turkey'),
(225, 'TM', 'Turkmenistan'),
(226, 'TC', 'Turks and Caicos Islands'),
(227, 'TV', 'Tuvalu'),
(228, 'UG', 'Uganda'),
(229, 'UA', 'Ukraine'),
(230, 'AE', 'United Arab Emirates'),
(231, 'GB', 'United Kingdom'),
(232, 'US', 'United States'),
(233, 'UM', 'United States minor outlying islands'),
(234, 'UY', 'Uruguay'),
(235, 'UZ', 'Uzbekistan'),
(236, 'VU', 'Vanuatu'),
(237, 'VA', 'Vatican City State'),
(238, 'VE', 'Venezuela'),
(239, 'VN', 'Vietnam'),
(240, 'VG', 'Virgin Islands (British)'),
(241, 'VI', 'Virgin Islands (U.S.)'),
(242, 'WF', 'Wallis and Futuna Islands'),
(243, 'EH', 'Western Sahara'),
(244, 'YE', 'Yemen'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `price` double NOT NULL,
  `times` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `type`, `price`, `times`, `used`, `status`, `start_date`, `end_date`) VALUES
(1, 'eqwe', 1, 12.22, '990', 18, 1, '2019-01-15', '2026-08-20'),
(2, 'sdsdsasd', 0, 11, NULL, 2, 1, '2019-05-23', '2022-05-26'),
(3, 'werwd', 0, 22, NULL, 3, 1, '2019-05-23', '2023-06-08'),
(4, 'asdasd', 1, 23.5, NULL, 1, 1, '2019-05-23', '2020-05-28'),
(5, 'kopakopakopa', 0, 40, NULL, 3, 1, '2019-05-23', '2032-05-20'),
(6, 'rererere', 1, 9, '665', 1, 1, '2019-05-23', '2022-05-26');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(191) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `sign`, `value`, `is_default`) VALUES
(1, 'USD', '$', 1, 1),
(4, 'BDT', '৳', 84.63, 0),
(6, 'EUR', '€', 0.89, 0),
(8, 'INR', '₹', 68.95, 0),
(9, 'NGN', '₦', 363.919, 0);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(191) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Tier 1', '2020-05-28 03:44:34', '2020-06-23 16:39:06', NULL),
(5, 'Tier 2', '2020-05-30 02:11:55', '2020-06-18 09:09:13', NULL),
(6, 'Tier 3', '2020-11-24 00:07:10', '2020-11-24 00:07:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `email_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_subject` mediumtext COLLATE utf8_unicode_ci,
  `email_body` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_type`, `email_subject`, `email_body`, `status`) VALUES
(1, 'new_order', 'Your Order Placed Successfully', '<p>Hello {customer_name},<br>Your Order Number is {order_number}<br>Your order has been placed successfully</p>', 1),
(2, 'new_registration', 'Welcome To Royal Commerce', '<p>Hello {customer_name},<br>You have successfully registered to {website_title}, We wish you will have a wonderful experience using our service.</p><p>Thank You<br></p>', 1),
(3, 'vendor_accept', 'Your Vendor Account Activated', '<p>Hello {customer_name},<br>Your Vendor Account Activated Successfully. Please Login to your account and build your own shop.</p><p>Thank You<br></p>', 1),
(4, 'subscription_warning', 'Your subscrption plan will end after five days', '<p>Hello {customer_name},<br>Your subscription plan duration will end after five days. Please renew your plan otherwise all of your products will be deactivated.</p><p>Thank You<br></p>', 1),
(5, 'vendor_verification', 'Request for verification.', '<p>Hello {customer_name},<br>You are requested verify your account. Please send us photo of your passport.</p><p>Thank You<br></p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `details`, `status`) VALUES
(1, 'Right my front it wound cause fully', '<span style=\"color: rgb(70, 85, 65); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 1),
(3, 'Man particular insensible celebrated', '<span style=\"color: rgb(70, 85, 65); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 1),
(4, 'Civilly why how end viewing related', '<span style=\"color: rgb(70, 85, 65); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 0),
(5, 'Six started far placing saw respect', '<span style=\"color: rgb(70, 85, 65); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 0),
(6, 'She jointure goodness interest debat', '<div style=\"text-align: center;\"><div style=\"text-align: center;\"><br></div></div><div style=\"text-align: center;\"><span style=\"color: rgb(70, 85, 65); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.<br></span></div>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `favorite_sellers`
--

CREATE TABLE `favorite_sellers` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `vendor_id` int(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite_sellers`
--

INSERT INTO `favorite_sellers` (`id`, `user_id`, `vendor_id`) VALUES
(1, 22, 13);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `product_id`, `photo`) VALUES
(125, 122, '1568027503rFK94cnU.jpg'),
(126, 122, '1568027503i1X2FtIi.jpg'),
(127, 122, '156802750316jxawoZ.jpg'),
(128, 122, '1568027503QRBf290F.jpg'),
(129, 121, '1568027539SQqUc8Bu.jpg'),
(130, 121, '1568027539Zs5OTzjq.jpg'),
(131, 121, '1568027539C45VRZq1.jpg'),
(132, 121, '15680275398ovCzFnJ.jpg'),
(133, 120, '1568027565bJgX744G.jpg'),
(134, 120, '1568027565j0RPFUgX.jpg'),
(135, 120, '1568027565QGi6Lhyo.jpg'),
(136, 120, '15680275658MAY3VKp.jpg'),
(137, 119, '1568027610p9R6ivC6.jpg'),
(138, 119, '1568027610t2Aq7E5D.jpg'),
(139, 119, '1568027611ikz4n0fx.jpg'),
(140, 119, '15680276117BLgrCub.jpg'),
(141, 118, '156802763634t0c8tG.jpg'),
(142, 118, '1568027636fuJplSf3.jpg'),
(143, 118, '1568027636MXcgCQHU.jpg'),
(144, 118, '1568027636lfexGTpt.jpg'),
(145, 117, '1568027665rFHWlsAJ.jpg'),
(146, 117, '15680276655LPktA9k.jpg'),
(147, 117, '1568027665vcNWWq3u.jpg'),
(148, 117, '1568027665gQnqKhCw.jpg'),
(149, 116, '1568027692FPQpwtWN.jpg'),
(150, 116, '1568027692zBaGjOIC.jpg'),
(151, 116, '1568027692UXpDx63F.jpg'),
(152, 116, '1568027692KdIWbIGK.jpg'),
(153, 95, '1568027743xS8gHocM.jpg'),
(154, 95, '1568027743aVUOljdD.jpg'),
(155, 95, '156802774327OOA1Zj.jpg'),
(156, 95, '1568027743kGBx6mxa.jpg'),
(249, 186, '1591173870featured_1.jpg'),
(250, 186, '1591173870featured_2.jpg'),
(251, 186, '1591173870featured_3.jpg'),
(470, 300, '16056407222.jpeg'),
(471, 300, '16056407223.jpeg'),
(472, 300, '16056407224.jpeg'),
(473, 300, '16056407225.jpeg'),
(474, 301, '16056417792.jpeg'),
(475, 301, '16056417793.jpeg'),
(476, 301, '16056417794.jpeg'),
(477, 301, '16056417795.jpeg'),
(478, 302, '16056427641.jpg'),
(479, 303, '16056434761.jpg'),
(480, 303, '16056434762.jpg'),
(481, 303, '16056434765.jpg'),
(482, 304, '16056444741.jpg'),
(483, 304, '16056444742.jpg'),
(484, 305, '16056454351.jpg'),
(485, 305, '1605645435d.jpg'),
(486, 306, '16056457181.jpg'),
(487, 307, '16056504491.jpg'),
(488, 307, '16056504493.jpg'),
(489, 308, '16056508361.jpg'),
(490, 308, '16056508363.jpg'),
(491, 308, '16056508364.jpg'),
(492, 309, '16056515474.jpg'),
(493, 310, '1605711939blue.jpeg'),
(494, 310, '1605712047DTdz7X29.jpg'),
(495, 311, '16057125484.jpg'),
(496, 312, '16057133331.jpg'),
(497, 312, '16057133332.jpg'),
(498, 312, '16057133333.jpg'),
(499, 312, '16057133334.jpg'),
(500, 313, '16057137971.jpg'),
(501, 313, '160571379761-dEvsdZdL._AC_SL1500_.jpg'),
(502, 314, '16057142171.jpg'),
(503, 315, '16057148521.jpg'),
(504, 316, '16057162491.jpg'),
(505, 317, '16057174811 (1).jpg'),
(506, 317, '16057174811.jpg'),
(507, 318, '16057215111.jpg'),
(508, 319, '16057217527.jpg'),
(509, 321, '16057285811.jpg'),
(510, 321, '1605728581d.jpg'),
(511, 322, '16057288431.jpg'),
(512, 324, '16057299091.jpg'),
(513, 325, '16057313341.jpg'),
(514, 325, '1605731334d.jpg'),
(515, 329, '16057325281.jpg'),
(516, 329, '1605732528d.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `generalsettings`
--

CREATE TABLE `generalsettings` (
  `id` int(191) NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_email` text COLLATE utf8mb4_unicode_ci,
  `header_phone` text COLLATE utf8mb4_unicode_ci,
  `footer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `colors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loader` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_loader` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_talkto` tinyint(1) NOT NULL DEFAULT '1',
  `talkto` text COLLATE utf8mb4_unicode_ci,
  `is_language` tinyint(1) NOT NULL DEFAULT '1',
  `is_loader` tinyint(1) NOT NULL DEFAULT '1',
  `map_key` text COLLATE utf8mb4_unicode_ci,
  `is_disqus` tinyint(1) NOT NULL DEFAULT '0',
  `disqus` longtext COLLATE utf8mb4_unicode_ci,
  `is_contact` tinyint(1) NOT NULL DEFAULT '0',
  `is_faq` tinyint(1) NOT NULL DEFAULT '0',
  `guest_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_check` tinyint(1) NOT NULL DEFAULT '0',
  `cod_check` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_key` text COLLATE utf8mb4_unicode_ci,
  `stripe_secret` text COLLATE utf8mb4_unicode_ci,
  `currency_format` tinyint(1) NOT NULL DEFAULT '0',
  `withdraw_fee` double NOT NULL DEFAULT '0',
  `withdraw_charge` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `smtp_host` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_smtp` tinyint(1) NOT NULL DEFAULT '0',
  `is_comment` tinyint(1) NOT NULL DEFAULT '1',
  `is_currency` tinyint(1) NOT NULL DEFAULT '1',
  `add_cart` text COLLATE utf8mb4_unicode_ci,
  `out_stock` text COLLATE utf8mb4_unicode_ci,
  `add_wish` text COLLATE utf8mb4_unicode_ci,
  `already_wish` text COLLATE utf8mb4_unicode_ci,
  `wish_remove` text COLLATE utf8mb4_unicode_ci,
  `add_compare` text COLLATE utf8mb4_unicode_ci,
  `already_compare` text COLLATE utf8mb4_unicode_ci,
  `compare_remove` text COLLATE utf8mb4_unicode_ci,
  `color_change` text COLLATE utf8mb4_unicode_ci,
  `coupon_found` text COLLATE utf8mb4_unicode_ci,
  `no_coupon` text COLLATE utf8mb4_unicode_ci,
  `already_coupon` text COLLATE utf8mb4_unicode_ci,
  `order_title` text COLLATE utf8mb4_unicode_ci,
  `order_text` text COLLATE utf8mb4_unicode_ci,
  `is_affilate` tinyint(1) NOT NULL DEFAULT '1',
  `affilate_charge` int(100) NOT NULL DEFAULT '0',
  `affilate_banner` text COLLATE utf8mb4_unicode_ci,
  `already_cart` text COLLATE utf8mb4_unicode_ci,
  `fixed_commission` double NOT NULL DEFAULT '0',
  `percentage_commission` double NOT NULL DEFAULT '0',
  `multiple_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `multiple_packaging` tinyint(4) NOT NULL DEFAULT '0',
  `vendor_ship_info` tinyint(1) NOT NULL DEFAULT '0',
  `reg_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `cod_text` text COLLATE utf8mb4_unicode_ci,
  `paypal_text` text COLLATE utf8mb4_unicode_ci,
  `stripe_text` text COLLATE utf8mb4_unicode_ci,
  `header_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin_loader` tinyint(1) NOT NULL DEFAULT '0',
  `menu_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_hover_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `is_verification_email` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_text` text COLLATE utf8mb4_unicode_ci,
  `is_instamojo` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_sandbox` tinyint(1) NOT NULL DEFAULT '0',
  `is_paystack` tinyint(1) NOT NULL DEFAULT '0',
  `paystack_key` text COLLATE utf8mb4_unicode_ci,
  `paystack_email` text COLLATE utf8mb4_unicode_ci,
  `paystack_text` text COLLATE utf8mb4_unicode_ci,
  `wholesell` int(191) NOT NULL DEFAULT '0',
  `is_capcha` tinyint(1) NOT NULL DEFAULT '0',
  `error_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_popup` tinyint(1) NOT NULL DEFAULT '0',
  `popup_title` text COLLATE utf8mb4_unicode_ci,
  `popup_text` text COLLATE utf8mb4_unicode_ci,
  `popup_background` text COLLATE utf8mb4_unicode_ci,
  `invoice_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_secure` tinyint(1) NOT NULL DEFAULT '0',
  `allow_inks` tinyint(1) NOT NULL DEFAULT '0',
  `is_report` tinyint(1) NOT NULL,
  `paypal_check` tinyint(1) DEFAULT '0',
  `paypal_business` text COLLATE utf8mb4_unicode_ci,
  `footer_logo` text COLLATE utf8mb4_unicode_ci,
  `email_encryption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paytm_merchant` text COLLATE utf8mb4_unicode_ci,
  `paytm_secret` text COLLATE utf8mb4_unicode_ci,
  `paytm_website` text COLLATE utf8mb4_unicode_ci,
  `paytm_industry` text COLLATE utf8mb4_unicode_ci,
  `is_paytm` int(11) NOT NULL DEFAULT '1',
  `paytm_text` text COLLATE utf8mb4_unicode_ci,
  `paytm_mode` enum('sandbox','live') CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `is_molly` tinyint(1) NOT NULL DEFAULT '0',
  `molly_key` text COLLATE utf8mb4_unicode_ci,
  `molly_text` text COLLATE utf8mb4_unicode_ci,
  `is_razorpay` int(11) NOT NULL DEFAULT '1',
  `razorpay_key` text COLLATE utf8mb4_unicode_ci,
  `razorpay_secret` text COLLATE utf8mb4_unicode_ci,
  `razorpay_text` text COLLATE utf8mb4_unicode_ci,
  `show_stock` tinyint(1) NOT NULL DEFAULT '0',
  `is_maintain` tinyint(1) NOT NULL DEFAULT '0',
  `maintain_text` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generalsettings`
--

INSERT INTO `generalsettings` (`id`, `logo`, `favicon`, `title`, `header_email`, `header_phone`, `footer`, `copyright`, `colors`, `loader`, `admin_loader`, `is_talkto`, `talkto`, `is_language`, `is_loader`, `map_key`, `is_disqus`, `disqus`, `is_contact`, `is_faq`, `guest_checkout`, `stripe_check`, `cod_check`, `stripe_key`, `stripe_secret`, `currency_format`, `withdraw_fee`, `withdraw_charge`, `tax`, `shipping_cost`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `from_email`, `from_name`, `is_smtp`, `is_comment`, `is_currency`, `add_cart`, `out_stock`, `add_wish`, `already_wish`, `wish_remove`, `add_compare`, `already_compare`, `compare_remove`, `color_change`, `coupon_found`, `no_coupon`, `already_coupon`, `order_title`, `order_text`, `is_affilate`, `affilate_charge`, `affilate_banner`, `already_cart`, `fixed_commission`, `percentage_commission`, `multiple_shipping`, `multiple_packaging`, `vendor_ship_info`, `reg_vendor`, `cod_text`, `paypal_text`, `stripe_text`, `header_color`, `footer_color`, `copyright_color`, `is_admin_loader`, `menu_color`, `menu_hover_color`, `is_home`, `is_verification_email`, `instamojo_key`, `instamojo_token`, `instamojo_text`, `is_instamojo`, `instamojo_sandbox`, `is_paystack`, `paystack_key`, `paystack_email`, `paystack_text`, `wholesell`, `is_capcha`, `error_banner`, `is_popup`, `popup_title`, `popup_text`, `popup_background`, `invoice_logo`, `user_image`, `vendor_color`, `is_secure`, `allow_inks`, `is_report`, `paypal_check`, `paypal_business`, `footer_logo`, `email_encryption`, `paytm_merchant`, `paytm_secret`, `paytm_website`, `paytm_industry`, `is_paytm`, `paytm_text`, `paytm_mode`, `is_molly`, `molly_key`, `molly_text`, `is_razorpay`, `razorpay_key`, `razorpay_secret`, `razorpay_text`, `show_stock`, `is_maintain`, `maintain_text`) VALUES
(1, '16085656731589598709logo.png', '1589598859logo.png', 'Horizon Wireless', 'smtp', '0123 456789', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae', 'COPYRIGHT © 2020. All Rights Reserved By <a href=\"https://mrm-soft.com/\" title=\"MRM-Soft\" target=\"_blank\">MRM-Soft.com</a>', '#14407d', '1592224445loading.gif', '1592224443loading.gif', 0, '<script type=\"text/javascript\">\r\nvar Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n(function(){\r\nvar s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];\r\ns1.async=true;\r\ns1.src=\'https://embed.tawk.to/5bc2019c61d0b77092512d03/default\';\r\ns1.charset=\'UTF-8\';\r\ns1.setAttribute(\'crossorigin\',\'*\');\r\ns0.parentNode.insertBefore(s1,s0);\r\n})();\r\n</script>', 1, 0, 'AIzaSyB1GpE4qeoJ__70UZxvX9CTMUTZRZNHcu8', 0, '<div id=\"disqus_thread\">         \r\n    <script>\r\n    /**\r\n    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.\r\n    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/\r\n    /*\r\n    var disqus_config = function () {\r\n    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page\'s canonical URL variable\r\n    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page\'s unique identifier variable\r\n    };\r\n    */\r\n    (function() { // DON\'T EDIT BELOW THIS LINE\r\n    var d = document, s = d.createElement(\'script\');\r\n    s.src = \'https://junnun.disqus.com/embed.js\';\r\n    s.setAttribute(\'data-timestamp\', +new Date());\r\n    (d.head || d.body).appendChild(s);\r\n    })();\r\n    </script>\r\n    <noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>\r\n    </div>', 1, 1, 1, 0, 1, 'pk_test_UnU1Coi1p5qFGwtpjZMRMgJM', 'sk_test_QQcg3vGsKRPlW6T3dXcNJsor', 0, 5, 5, 0, 5, 'mail.horizonwirelesstx.com', '465', 'info@horizonwirelesstx.com', 'admin@123#@!', 'info@horizonwirelesstx.com', 'Horizon Wireless', 0, 1, 1, 'Successfully Added To Cart', 'Out Of Stock', 'Add To Wishlist', 'Already Added To Wishlist', 'Successfully Removed From The Wishlist', 'Successfully Added To Compare', 'Already Added To Compare', 'Successfully Removed From The Compare', 'Successfully Changed The Color', 'Coupon Found', 'No Coupon Found', 'Coupon Already Applied', 'THANK YOU FOR YOUR PURCHASE.', 'We\'ll email you an order confirmation with details and tracking info.', 1, 8, '15587771131554048228onepiece.jpeg', 'Already Added To Cart', 5, 5, 1, 1, 1, 1, 'Pay with cash upon delivery.', 'Pay via your PayPal account.', 'Pay via your Credit Card.', '#ffffff', '#232323', '#ededed', 1, '#ff5500', '#02020c', 0, 1, 'test_172371aa837ae5cad6047dc3052', 'test_4ac5a785e25fc596b67dbc5c267', 'Pay via your Instamojo account.', 0, 1, 0, 'pk_test_162a56d42131cbb01932ed0d2c48f9cb99d8e8e2', 'junnuns@gmail.com', 'Pay via your Paystack account.', 6, 1, '1566878455404.png', 0, 'NEWSLETTER', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita porro ipsa nulla, alias, ab minus dummy.', '1584934329adv-banner.jpg', '1589598709logo.png', '1567655174profile.jpg', '#666666', 1, 1, 1, 1, 'horizonwirelesstxonline@gmail.com', '1571567309footers.png', 'tls', 'tkogux49985047638244', 'LhNGUUKE9xCQ9xY8', 'WEBSTAGING', 'Retail', 0, 'Pay via your Paytm account.', 'sandbox', 0, 'test_5HcWVs9qc5pzy36H9Tu9mwAyats33J', 'Pay with Molly Payment.', 0, 'rzp_test_xDH74d48cwl8DF', 'cr0H1BiQ20hVzhpHfHuNbGri', 'Pay via your Razorpay account.', 1, 0, '<div style=\"text-align: center;\"><font size=\"5\"><br></font></div><h1 style=\"text-align: center;\"><font size=\"6\">UNDER MAINTENANCE</font></h1>');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(191) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `is_default`, `language`, `file`) VALUES
(1, 1, 'English', '1579926860LzpDa1Y7.json'),
(2, 0, 'RTL English', '1579927527QjLMUGyj.json');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(191) NOT NULL,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) DEFAULT NULL,
  `recieved_user` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(191) NOT NULL,
  `order_id` int(191) UNSIGNED DEFAULT NULL,
  `user_id` int(191) DEFAULT NULL,
  `vendor_id` int(191) DEFAULT NULL,
  `product_id` int(191) DEFAULT NULL,
  `conversation_id` int(191) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `order_id`, `user_id`, `vendor_id`, `product_id`, `conversation_id`, `is_read`, `created_at`, `updated_at`) VALUES
(2, 2, NULL, NULL, NULL, NULL, 1, '2020-05-27 13:12:12', '2020-06-16 06:06:46'),
(3, 3, NULL, NULL, NULL, NULL, 1, '2020-06-01 06:58:08', '2020-06-16 06:06:46'),
(4, 4, NULL, NULL, NULL, NULL, 1, '2020-06-01 07:09:46', '2020-06-16 06:06:45'),
(5, 5, NULL, NULL, NULL, NULL, 1, '2020-06-01 07:14:06', '2020-06-16 06:06:45'),
(6, 6, NULL, NULL, NULL, NULL, 1, '2020-06-01 07:19:39', '2020-06-16 06:06:45'),
(7, 7, NULL, NULL, NULL, NULL, 1, '2020-06-05 04:39:02', '2020-06-16 06:06:45'),
(8, 8, NULL, NULL, NULL, NULL, 1, '2020-06-05 04:43:35', '2020-06-16 06:06:45'),
(9, 9, NULL, NULL, NULL, NULL, 1, '2020-06-05 04:48:27', '2020-06-16 06:06:45'),
(10, 10, NULL, NULL, NULL, NULL, 1, '2020-06-16 04:36:59', '2020-06-16 06:06:45'),
(12, 11, NULL, NULL, NULL, NULL, 1, '2020-06-16 04:55:44', '2020-06-16 06:06:45'),
(13, 12, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:03:00', '2020-06-16 06:06:45'),
(14, 13, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:37:10', '2020-06-16 06:06:45'),
(15, 14, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:37:56', '2020-06-16 06:06:45'),
(16, 15, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:38:56', '2020-06-16 06:06:45'),
(17, 16, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:40:48', '2020-06-16 06:06:45'),
(18, 17, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:42:30', '2020-06-16 06:06:45'),
(19, 18, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:49:11', '2020-06-16 06:06:45'),
(20, 19, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:52:33', '2020-06-16 06:06:45'),
(21, 20, NULL, NULL, NULL, NULL, 1, '2020-06-17 09:49:00', '2020-06-20 10:47:08'),
(24, 21, NULL, NULL, NULL, NULL, 1, '2020-06-19 09:26:09', '2020-06-20 10:47:08'),
(25, 22, NULL, NULL, NULL, NULL, 1, '2020-06-20 00:33:35', '2020-06-20 10:47:08'),
(27, 23, NULL, NULL, NULL, NULL, 1, '2020-06-20 10:46:00', '2020-06-20 10:47:08'),
(28, 24, NULL, NULL, NULL, NULL, 1, '2020-06-20 10:47:56', '2020-07-06 08:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `cart` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pickup_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalQty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_amount` float NOT NULL,
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(255) NOT NULL,
  `customer_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `coupon_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_discount` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `affilate_user` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `affilate_charge` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_value` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `packing_cost` double NOT NULL DEFAULT '0',
  `tax` int(191) NOT NULL,
  `dp` tinyint(1) NOT NULL DEFAULT '0',
  `pay_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_shipping_id` int(191) NOT NULL DEFAULT '0',
  `vendor_packing_id` int(191) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `cart`, `website`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(1, 27, 'BZh91AY&SYÄµ\0ßPXø;oþ¿ïÿú`=÷@ æ\00M0\0\04Ð02hOEÐ\0\0Ð\0æ\00M0\0\04Ð0))§©ÈÑ\Zd\0\0\0\09À&\0L&L\0\0&M4$Dòb)íQ<MG Ô14ÙMêM<SP)´\nmëè=ÄGØÀìïOÙòó>±rFK_°ÄoýH»¬~Ã$3¡ñ=ä¡~D ·³ äi\"üä­g3Aå%z|)ZASÑdHÔ]l5Î¤óÐ_Ì{?NÃ7ewq^Ô xï<Îfí<N¶7d¤BCÔ{\raIë=A:úQrLK$-³6sAIq\'\'#CÔzÌ)ù	I ÆÆòDl\r1¦-ôÛ}Yc#$É^Ñ¥°Å`Á\0BYS<Vcâ-yW¹Q)Lh¦+5ð.³([Aª\n\n,|UWâq(\\±{°l)y¦D* Ñ­6a6ËVfÖ ©«®Ã0A¸¡B\\\n,H{!AU&eë1XªxÅxÄ0©A2ówjÜ*¨¡Rö¢h\nZõ¨­aIT¸AR¤««ZJª¾Åü$.ÈÑ¨ ,pMZ8ñ(,Ã$¡ÑÃÑ\Zu&°	Ia`Ó2B¥\n-ãvèît1Eö1k<%a+C}¥P²|8TÂµëf\" BÓRÂ´ÖÃ´Gã	DG Ã M!z5Z1gwÊÔd.x<i5â,V$ÈÂ\Zç¾#Ào1e(dÉ°r¹5°ÐÁ ÌþOoxÿ±ã­öqUiU4¼o>ÌøæÄd­å¤ ¦±î<²D]39äQIÄ8SVjr§5¸kò!hÅSîÖ9R\0Ôò0jo3Ð¾U;Ñ¸wK#6\r\r0÷C,²Ç¤ÔôÛhÁ0Ê¥càdÆHÅÐ?\'é<þ¨wwçßL±íÚc\nçæHÏäØâfXiIÁyÐ²õ]ya¹8èo­M8SPvó>?À×CÞxdªu5í7;p|(P©ä©ý4.Ïxz<(GþxÑ^fÄÖtÕ\'×é±=Á°3²aädAéÐ$F·gzÝô$Üóô.k¼láÚe]ç _t\nv÷t/ÅJ`\n5¤ÌI;Dò¨Ê¬* bð0¡*`rÍZ *KÄR8Úf¾æj¨Zòiª0uùà²°Ó>ó$eøqº¥Rc´_!þgqÞØTö{?SJViyÃÀmnâØEõ;Tã~Öc+\nTgd(<ÎvDÓfçþ×nß¹SÚÂ²1``XÂ\ZD(%F\rãlàÜç¯»Uë-cAB¸J2(D.h3:ôp*¦ðw\Zå71Ò35P133QS¨P\n,AôEJ¹®«èÀ¾ñqô:¥Ð!**(ºèæOÐ}8ès$(²èÐa¯F¯Sâ#W ñuMì?\049ü]ÉáB@Öx', NULL, 'Paypal', 'shipto', 'Azampur', '1', 130, NULL, NULL, '2csj1590602773', 'Pending', 'junajunnun@gmail.com', 'john doe', 'United States', '34534534', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-05-27 13:06:13', '2020-05-27 13:06:13', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(2, 27, 'BZh91AY&SYÉþ\0_PXø;oþ¿ïÿú`=÷@ÆÅ@h9À&\0L&L\0\0&M4%	¦MH4\0\0\0@M\09À&\0L&L\0\0&M4!JJi¦Ê\0È¨õ\0\0\0\0a0		Ó\0\0	M	&E<ADÞ¦QèdzOÔ\no­ôâ#ì`u\0÷§ìùzX¹B£%¯Øb7~]ÖH?aÐøÌÉ`Ê5tlÔ©*LÊ³¡K²ÝÒ³V¾Tày,\ZK­£\\ª@n=Eý´ïßîqÆÌ³0lì+Ö\0ÏÀ¡ÈÐô=¼cÓpÆAÖHs$<ÎÓXRyi\"aÊ¿aAj1.)`·83i×8´o8q4>ÃÌÂ\0Tm6´\ZcM[©·uYc#$É^Á¥´b°`!,©+±ñ¼«úÖ	Q0M l\Z¤Zmàal([Qª\n\n,|UWâu.X½Ø6¼ÌÓ\"JPhÖQXM¡2Õµ$jjæk¸f  ö(@ËARåi$(*²dÑ¢Ùf2«O8°R/(&^ní[U*^ÔM3 x¤æ$\\\n$zfI(U&áLÎwÈÔJP8&­øÎaPèáÈhÃ\r:X¤°ÄDÈ0i!BÒÃ]Ã¬Ó	\0¨Å¬ï:T­\rÖB2ÉïßRg\n)} ,(YKCX«È¬Dr´uTÖC5µkXûÇRê¥eD³Ñ®$åò \\)ª|tFpØ9âPÄ/9`ã©\raÓ\rC3ú>¿ûOÍhüYÁU¥TÐZñ¸@@9öfK§QÚ(1~¤Ñ3ÜyøÔÚ2rT¢p6¦¬:ÔÿeNK`×¸zÆ,Êv´Çû§SqÅò©ÜºY°hlYàp²«&§¨¶Ý°aJÇÀÈ7{ÉåÛîü»©Â±A0`0]°ÀÛR,a\\÷3ú6Ë\r31I28/:C>¢K¯Cë ×2G=CÍô©¦újn¾GÇù\ZÞq(~yõÊ§C^³c¶øÓ¾\nA80\ZH4.¯txzÎàÑþv¢½ó!Ä7ÎzLð>¯ØAT:#\"ITkfw­ÑABM?0ÀnBä»ÍýfUÜz÷A\0À§_qÓ@æ@Âñ!Þ,T¦\0£ZH`LÁQD±dO\ZªÂ¡B	&/\n¦,ÚJÐ±Pª^À½fkèo >f©oí¼E¢\Zeê>d÷¬¬4Ï¼ÉÓS¬ªPm:Åò¸ì ;\ni\'´Óô4©uf¸;ÆÖÁ	D_S¨ãNì`f2°¥HFy¦BÈi\\æATaÉ8¹{x÷»¹=¡ø0L+#	f\\!¤BrX$iÐØp6rWz]ªó-cAB¸J2(D.kl9¸Sx;\rrÂi¨ÈÏ©Ð(LA ù¢¥Ø×Eô`_pÏ¿°Æzæ]sDr\'è>|Þr$(²æÐA@Ö(¿ÀÊyËÚÚ ÇÇÉ	°¡È,âîH§\n?Ã\"à', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 130, NULL, NULL, 'f3uh1590603132', 'Pending', 'junajunnun@gmail.com', 'john doe', 'United States', '34534534', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-05-27 13:12:12', '2020-05-27 13:12:12', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(3, NULL, 'BZh91AY&SY9+5\0*ßP\0Xø;ïü¿ÿÿú`]ñ @\0 æÓ0\0\0\0L#b`#LÂ0\0\00i0#À\0\0Â0\nROHh4õ\Ziµ\r4õ\0Ð4@b`#LÂ0\0\00¢d\Z¥6J=O#)êzCM\r\rMI\"¹\\®eW;\rô0|#öü¿SóÅ*2ZüF#ÿ|H»¬~#%\Z|¢*GëX	6¬Q=ß6dÔÚÕ\"æ7Ò5¬fÿ´\Z\\d9LÉ$¦ûÈídQlïóäµÓ\rÅq{\Z;;ñ&5*µµ;¯²¶;NR£yòu;\rJÞc[AêtNCcVûbAP:ÏQ_´b[Ó¹	g6äV¦¦î`Ì$I$ ¸ÈÔìÞM\\1´4kmwP«.h+ípÆ\nì4¥uÈh\\Eª@ÅbV0TX¢`@Ø5M¦ñKàôÉR\\k(`u%QS¨ª$kI\\cÀÞP°´c 8ËÚfþÃ%©g3o¸0A©P¡.IK$7B¡QI<¬&ÓX f(â·¨R1(&^ní[U*^ÔMI.d)kÖ¢¶%RáJ:®­jUUùú×Í!v)]t¢!&Qj6²`¿1m±lhÐcBÖ[tØ«%¬XJmbFXÄI\r¨à÷ð\'hHYH\0ø[Lqcf)R´8ZH¡9ËãÆ¤Ío¦º4\ZÚ\rFjÍf53I3lVG\n0³	Íô¥æ*k¦ZÆºf2³7KZ^#ZAbEØ~óÌv°,lTï2Í´=ÇÀõî0­Æ#yB¸ê<\"ë!o{8Xª^6>éù÷\ZnÕAÌ÷ãã÷Á©Ô¤ÃØ½(¤äGSf.°6=¥NÕ¨×ÞB<F-\nÝ¨#³ÖÇc¦àbýêw£QÝ,0hl_PÿGZ4Ð;.xÎ%¸pªV?#!Ô2F.hþÉîïw~îÍI Á%Z`mª\nøV;I~æó¡a¦h)&Gçq	dg%×Ìôm¡#CÑô©»6	·>Óóý»C\ròO\0c=4SÐßÌÜíË¸gO\n<AM\rëM¯Ä½yã+ý,8¶Úâ¥FCQ¡ËÆÄù`g*H=ITk]/[¢>¤ÿ!àû« ÙË¥º\"úÁ\0À§>ó¦ÁÌâCì*S\0Q­Ò0TQ$ê²OmFUaP¡$aR`rÎ«(KÐ)+¨aò5K\\zÅ¢\Zeê?|l4Ïòe÷õGOäÞs,©Í/èæBïmµS¼Úm­\Z[ÃÎ6²3½3¼ë&û³²a\r-FV©×TÈPx\r+Äqm¸7<:ôðÞÉÅTÙÅ¹Y%,R¬¤ÅKb¤d¥Ù¡ºé¼úöÂ+Å/0ª½%¬n  £%Ü·Á¡ßÜàUMàófnc	¤hl c ,hi=S P\nY-ÃîEJ«_Øú´à3Ýó9v<æasDwô>²\0üâB!Í¢9±³U÷§¬^K¢Ðxøù!ZZKþ.äp¡ rVk<', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '-2', 3584, NULL, NULL, 'pvtt1591012687', 'Pending', 'hello@wirelesswavestx.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 06:58:07', '2020-06-01 06:58:07', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(4, NULL, 'BZh91AY&SY¼Çä\0%_P\0Xø;ïü¿ÿÿú`_rQ(\n æÓ0\0\0\0L#b`#LÂ0\0\00 Òj¨A \021¤Ó Qé\Z\0Úh\0\0\0\0\09¦&4À#\0\0\0ÀHM	 &2)ä¦õ=#)úIA#õIKGÀóÏ¡É\0Å³¨ûeÄPä»ncz\rÃÇCì¢:ªMhÏ·¥ÌÉº5ÅBbòÞI³ªïñRic ±¼ðe Õ&\ZÎ2Hm-¨(I÷ìÖ~çV£VAõÂ¥»;Ä è	gC¡óæu£1OÐÜoÄ!è`Aænäy(yä}¥Ä¹&%	vP]§\\bÞZ±\'3W f	 XdfhH@ÁhbÎgB¬±\\d¯´a¬c\ZVCÇ\ZepÄ°\ZÀHñ¼«Ü¬´&Ð6\rLVkc¼ÅP¹¡«¢Ä#4-áÉvB©LâÇ$Áá5(@Öìe{Ð\rc6\Z-YyÒJ\rRÎfßp\\B$B rÉ ¢Ì	!CI4P¤ÕL9iâ»\n.2!òdræÞªÂ42¼&v^d.¨ld\nYXABu%`­jªªýæºÑp %\r!°HS GÉ¨j ÅEQ0ëi¸C³	11¡ L)D\r!É\0ÐÇ´Øõì\'HHX¤\0|-&72ìt©Z-*cÝº¤Ímy4ÚÆfÌíÒLmbJó±pÃçdâ ´FÌóÍ`sMæX¬fÈÎE¯U.£çèvMçpXÐ©ÌÅ2=çÞõÞ\\>½EãYB¨õñÄ-èÍÁN53°bãQGèÛ=óæ8A¦Øºh9¸ù~GøfmRGQí^ÚRo\ræc©£\0h{Vc_ð²*{´ ¯i\0hx46j/ê§4f<ÄÉCbøÀà²¬Y¸¶ÍraJÇêbFHÅÉùø¾pðxuô¦5$ä.\\.Ihaq¶¨,.¬q$gôk7\ZfBdpa:K$/¡ì Ó\"G6\'Ò¦­ÔÐ&Ü¸·ò5ÔÆ\Z=	àpÜ¤óC.¼jøì÷G8<H£³×<ã§¶ì^`ÏUä	/bb!òãC<gì\rèo;H<5¨Öya\\A¤ÿÐ¸lBþN+ Ù¿¶ë¹4D0ñ^¥.\ZÕ!q2åENkxÔeUÕ\nI1Ü^F(¦,ØJ°ÂD¾¡HàX­£4¶vwdQs§ø#V,t!A0g±|C¤¨?ô4)ªªs$÷\ZgdQ£Xv¬FsLæp\'\rGª¡\r}Y¬)Ri î\ZXdn0;sös·{í~HüÁ^p\"áLÒÁ@ÄÚ6\0CII6¤ÚdvçÆ]Év\n«ØZÆ¡BÀ%\"Z×G>·ªnça¦3{¦¢±õEN@*b±­CëEJÑ_éz°0Ø3ßôÓô;ä]rDuý\0ød+62ý¦\ZÆ«îO1x®_Ø ððñBl\n §ø»)Âæ?$Ø', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 700, NULL, NULL, 'yaDw1591013386', 'Pending', 'hello@wirelesswavestx.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 07:09:46', '2020-06-01 07:09:46', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(5, NULL, 'BZh91AY&SY>¼au\0%ßP\0Xø;ïü¿ÿÿú`]ÏC -H#b`#LÂ0\0\00i0#À\0\0Â0SÊÀ4j\0\0ÐM¢R¦Ò\0Äz\ZSÔ@h\0\0æÓ0\0\0\0L#\"!2&ÈÉG¦¨zz¦ÒyA¦Ò6z54$sW#Þªû0ì@a?§¹éu GTòCKÄjYq!GC:èHÞòÀæ24\"áì¦AêLm-UÚ1	e²Wê\\¾Ac*AmHäÉ\rÅ®h$ømÔüÍÌÃëÌ§BÜÄ;	Aâ	gÀÔùöGUQì|[îkc´Ðô·Mqµ¿âm+¬eBKÄþ©oê1/Ä®BBã²4ê:it6\ZÓ H Xdhm$FÓÐÅ²éB¬±Æ2J=£\r£Ò¹<bàÀY*\rdñ¼«äT¦	´\rSØú-°k!®BzQRhÉhx +ZLÛ!;\Z,&ÞôhÎâÀCBe«3o°õ$ Õ,æmöÈ3`aÈ aJbJ,8h2A(8ìÒI/,0¥B-4êÈCS\\åÂÜ)OK3A)ÉhEG]Â-S\"ZàPH bHZ²Vµ*ªûWÉ!q_ã@J\"\" b)øYlÌecIÈÆ4S]âYJbÂSLHÆ¦HÊÄ6>ãsÛ¸a!a â1k1½c¥JÐÝiT#{÷Ô­³Ó2FKA Í¥£C±%dxÈ QN2&ÑT#Lc°Ìó(Ëábl)ZõYÁ\"ä|½çÞp<Æ¥Kõ733î:ð96Í¢A÷ÞjLn5TIE@µ6\"\0ÁðÁósBàÕ(ÆÉï=ÇÇï>fr9«Ö¥ êjÃ\Zâ§zÐkï!ceOv´ËÔ5=öíSª4I`ÍCbø\ZÀâóYfÓymÛ \"äÃ*ø`;HÅÑÃó<úÃÉåÏµ1RH.IbåÂäjÊêÇy#?ciÀÌ°Ó3#)ØBXì$É~g¡¹8Ü<ßj7ÓPtï?ïê5È85*{É PÏp²FSKhÅmôÙË½¡PÑx¶&÷Rm+ý6N=×T¨Èp9«Æz{,OjáCä`Ï`I*iUÉô$Ð\'÷ä/Ôï]ÎíÜoÒ:u;jeP¯Rl¸r¢\'E{ê2ªê$ò/#\nSn%Xa@¢_X¤p,.áèhïoâ,ÐÓ2ª.vùä±a¦{Ì#èíûNqô:%ü:±¶ªu$ût²(Ñ´<FÖuLêq\'-7Â\Zö±h2°¥HF&BÈids |Aã§§]]ËÃvG)U6â1nVIK%«\"©1RØ Ë%,Ù£yÏpô5änoKÀU^¬l  QBæ¶Á×U7sÀ×{¦ª±òTÂÀ]l4T ]\Z5üÑáÛô\Zjy%Ð!**(ºèæOÌ}8ÄæHQatbÆ_ÄÔXªûFSì±vBù??b`Ø²\'ü]ÉáB@úñÔ', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 600, NULL, NULL, 'Ss0C1591013646', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 07:14:06', '2020-06-01 07:14:06', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(6, NULL, 'BZh91AY&SYrØô\0%ßP\0Xø;ïü¿ÿÿú`]÷M\0\nÈm@4ÄÀF`\0\0a	SSôTÐÐ Ð\0\0\0\0Ó\Z`F\0\0	`¢4Ù#ÔÓÓPô©ê 4\04ÄÀF`\0\0a	FFIèýMLÑ¦SôÇ¨dÑ=M¦BGtÐÂºWÝL|Ï×ä~Ø±rFK_ Äoó\"î²Aú|#Þja¨ò\\¯d jÌÃI«È®·yDæ±_-QTàx¬$Öo¸ÖHuÈÔ(Iïß¸øx­u5Ì<s)Ü[!èp6>#=g¸¥Qí|ÜÐÃõ7\r¬þ6Ó´ÀT$°O°¯°b\\Ó¹	KN âS8¡¨l\\Id °ÈÐØ\ZLcC´ÏJ*²ÅòÉ(°a¸c\ZRD!å3ÈÈ,®D@ÅbV.T¦	´\rSØô²mF®2VB1J*n*KCÈ­i3k±ÞlP°+´c u\Z-Y}§ÜIAªYÌÛïfA(@ËARå\r	!Pª%LT©&ÓH eèâ¼*Æ$Àð©A2ówZÜ*¨¡Rö¢jIs!K^µ°)**QÔukPÉUWÖ¾)ý×\ZQ b)\n\0~m@ÛQ46PU6A;ZnÈ²T«0\"Ð¶\"ÑõÞíäí		\0cÓlÃ*VûJ ±	ÆiÒ×°ÂH/E.!t/E¹ÄRa\"£ Q$É¥\\[ÄCÐBÖ(k#J^0Qgañò;Ãà5*c¡¹ãð}æ{×©ÜP jNcúÏºÈ-äÎ°«SAkÆÂþ5Ïqø ÏX½TÏ#Úyþ\'ÌÐêRGaì_uJ)8AÔÙ6=¥NÕ ×âB<F,ÊÝ¨#³í \rQcy ÅüÔèt²3`ÐØ½æï8£<Ã²ç¸ë-¿|`eR±úPÉ¹£ú~#\'HwwåßLªI	,`À`ÐÃmPWÂ±ÚHÏäÜp3,4ÌÅ$Èà¼êBYú.¿sÔA¶du7ßS^ºln}§íü\rvÅO\"CÀz;\'ÆfjRúbö©Åº 1ulLÜsi]Sa$×¼Õ*1FúÅkx>«Ü8PàxxêJ£ZgzÝô$ÐÉý7Î8©kÃ»:°ÁF^^f®#Ä¤X©LFµÀ¢\'E=µUBLxJ)Ë7¬0 Q/ð)%Ô0ù\Z%¿Ð.¾\"Í\r2õFÿ>+\r3ÈÉ{ú£¿ÿeyô9¥ýÈ:1¶ªt$÷ieFàô\r¬tLèq&úNÊuÂëih2°¥HF&BÀi\\äA\\AsÑ§«¥½/MÙ%TÛan+RÉe*ÈªL*[´²RÉ--=mÌòw\n«ÔZÆ¢pdP\\è3:rp*¦ðwe71Ò36P133È©Þ¦K ÂÔ|Rh×ù_F÷ÿ_ ôi±à0¨¨£#?1óâ@g\"B%Í.&VpnO2²íNyª\'ð½|ñ--LRâÅÜN$¶&=\0', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 120, NULL, NULL, 'SS1P1591013979', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 07:19:39', '2020-06-01 07:19:39', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(7, NULL, 'BZh91AY&SY>Èï:\0ªßP\0Xø;ßü¿ÿÿú`ÿ}\0\0(I@ID! 9¦&4À#\0\0\0ÀæÓ0\0\0\0L#b`#LÂ0\0\00i0#À\0\0Â0$A=$Ñä§ä¡íI¦\Zbl¦a\0© &)yOS#Å2diÍ@ÓÔºP7iý(ù\"Y-#à\'Ð?{Î[ØÚ¹í<m!yfÎT¼?Ed³ü®´eFØa.ì|Y¹«\'6,Â5lèÃ	vì©»WúXÇ\Z®¥ËË§·Æ},:7<\r\"ÒvÆ¯öºÏ ÊÇâò¾§Gèy¹»&ï½Ñ^$Ò=h#Ðs>G¼÷åy^¥ø;]Ç£Áªç¨ÀËïl=çÄîZY_H¤ø¦CVy£´ò7V\"=	D\"1åX³µcV<1´z¦¢I¢*,MÔt;¥ÐÂTQE¤»eæì­477(¥Ó©ir¥äÊ¢lXÝP¼ÂÎeí[¦­¥9IJ.JQQ¹Ie7]s*nÕ7[VëË$¥2N¢Ór(RnÕvì²ö»\nIcY*Ñ\rY³ÍØí.Ñ¤¦ÆÍ%(]]æâí¤¢ÊFZ.Ã.¹Ð£ò6J©WYf­GÁOG\\.ºÿòË,Èf\\\\fQÎÖ\ZlÞÜÓ[6Í%Óº^Y,f:¨ÀË6T:a¹F×e)ºÌ6dav¬7aºóstÃ©ªí©£-FS	dÃV%&¥Øa­d70ºa¨±f\\â89\rãyç¡Ö%ë.@h¢(Â\0¸°Ò°÷D !E4D\"¡yfE)Nå%Rñ1d²¢R(¤¢(¢ËD,¨E@B,X\Z:ÍÐé6éeC5T©\'V\'m8lÔÑÂÍÌNÖîJnË&®MXäpZTG&Vh°ÙF68p¹ÃF¦G\rK¥Ô.¤º2Y%5´Y»*8,£­Ôrrn³Ví´ÃSï>_t÷?î_º~MÕ¤ösü\":{§QïàÑoÙÖe=Ï\'aèuôøÕÐhûÔw¥ÙXS* Ap1(Ü ÒÜû¤?vF£aF%ãOÖ÷¾ýïÕÔè6hë}	è/n=KÔ\Z¥2sPòö;ÎR£òÖQ8>Ç6\">À°siÎu;üfzQ©\"äæA®`zÃ©r2ÚìtpttXY´ºÊI¡ú¥K	?òÉIw{ÐYªÍ]ïK\rÍ&®¹¡±°Úh¥\rRäÕ²k?åÒu9¸æ\re85´.L~öÎ#óà§aYÇFâ^ë7ÆóÆüíÚ}èzÜºaã9¼Ó;V(ñzÑvÓdç=Å%å<ë¿y:<S¬Xù§0Uí/x!!¸^äýÇicxs;ÓÙ7·`¼¼aQÉÃVZ£§Ù/9%ß°ØuDÝñx\'¤¥Ï<äÕä;d|Ö,$ÃÌôÏAæ)5Yqê&ÌÌ6);¦ÆBËË¹&òïÌÖbÙÄs6£*`6ÂÄz[&!@^7ö©fc£\0ïu¡Ö£Ä#x½½±Èr0 ÇÒä&GìhQÄüÍ®÷ié|æ}«½óB\reËÑØÐR:§rÞe%¥JQÖY³òÏ¡ëBøRN%L­Kæ¡ äÁ1|4Ãu8¼\rG3°ÄäC°B\0ÆO9\Z\011\n\n4A`ÅUts{\rG¡/7ÀÇí1va:ÂfÜÑCå8u¼)KÆ¹é\\.Õ³h©S¬ZQEIÄâï±ÌÝ7xfa6%\'ÔO«e(û³dõ¤óDÂa,Õ<ð³ùÎSÁÝ-$üNòéÅSJ<ÊSlÁÈ¸p`\\yAî;}§ÌQçÜsD()?ñw$S	ìó ', NULL, 'Cash On Delivery', NULL, NULL, '4', 0, NULL, NULL, 'Ocjm1591349941', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'customer name', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-05 04:39:01', '2020-06-05 04:39:01', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(8, NULL, 'BZh91AY&SY³\0©_P\0Xø;ßü¿ÿÿú`ÿ}\0\0()@ID(\"¢i0#À\0\0Â09¦&4À#\0\0\0ÀæÓ0\0\0\0L#b`#LÂ0\0\00IÊM5#!\04\Z\Z\0\0© &!MS\'¦S&G¤ÈÍ ÓÔºP7qøQò,;âD²ZG¨OØ|þáÖv0qnèch&+°êq´Í?©y8?Ed³ö]hÊ90Â]Ôù(ÅÔÜÕ¡É«0¶sa»vTÝ«ý,pQ©bÓCC°þ©L96:w4IÖv\Z¿Úë:¬wkês~{w\'SnüÙòä@Qì#Ôiò<NGÈûqÜQG¢|]nÃ´£¹¢ç¤ÀËðl=g¼ìZY_`¤÷¦CVq£¬ø7f\"=	DK\"+Ì±¸º´,±Þx=3T4EAråÔs.RTQE¤»eæì­4377(¦\r©È¼ÁRáÈ©\'ÆÊä­[efí[¦¬8ÉJ.JQQ¹Ie7]su7l5n¤S(¤ó¢MÊXJR¡I»eÛ²ËØu2ê*IcY*Ñ\rY³ÍÆeÙ55j©)Bì¬²ì¶¥4F©h»¹ªç2#¬±Âo\"Ê©,Ù»cG½OGL.ºþöíYÌ¹s4ÖÝØÓCM·i-s5K§l¼²XÌtQl©6sÃr®ÊSulÈÂíXnÃuææéSUÛSFZ¦É­JMK°Ã-Z2É4n(atÃQc3\nÌºj4bnL½ÉòsöÎÄÊH¥ArÃJÃåÑEå,\\¡jÉT¼LZPTJERR±EÖYP!a1aµÔk6ñ³m\r*£?9RNJ£víZfn³c­Øâ¦ì²jâÕ\'Ò¢8²´ËEÊ4)±ÁÁsF¦G¥ÒêP»%ØqY£Y»E²©ÁÀ²­Ôqqn²ÍÚ²RÓ\r#¸ù}óÚ÷NÙÑôÏÉ±ú´A÷¸ÏCçOlè>Î~î)íuùl<]&¼å=çÐ³©9ä»+ÊeE\\J7Ô\Z[¾×icìÀõ¹Q¤ÖQ iî=/ÁëâhÙ£´ùÌhn6w.àÒÃ\'%19q¸ã*?)h}eÉö¹1ç{Ë)ï6§IÁÔ(?ã3Ée\"¹Ò#!ì<Î·k°æ°³iu2Cõ7\n)òå=îç ³U»L764.ºæÆÃi¢6)JKVÉ¬ø(øNs¶pp1Ì6,Êq,jh\\÷Û8à9ÂfafS¬3!Ü4:òæµ±Äp?SòÄß<æêz¥tÃÓ9<\'4v,Qéz@»i²rÒò+¿9½)Ö,|ãÚI\'­ã\n%DYÄIêCÖÒ]à9ÅÝÏuÉ§Ô%ÈLÌM\"]¦^qK¿q°è»àïO\"v¼g®³²GÍbÉRL<SÒæ<%MV\\z³3\r\nN¥ÆÄ©±²òî)¼»½óu1lâ9\rÚ6£*`l6)ï6LM%S)=/)hpN¹CñRu¼äâóTTÕOKñzvJtÝ¿£­g¥ðsH\'y8?égï<\'Ö¸ô¡¬¹sá9º)\'DíSÑ,¤£±ätlå<ÓÎÃu¡Iü©\'	S+EÒâÌóHÐÐr°L^a¿N/¤è9NR<Hub2yÐ¤H±4@PQ¢(Èk/:ç	èqw,»x)í>-g\"];a3nÑCà8ÇmO)ÆBÐ0CÊtp]«fÑRgH´¢&áwzÌÏXÀfn©Ofa6\'Ô>t)GÚò8:§­\'´L&ÍSÆ.p7?Ì8MÍ+Ôa¹ç#¤!ÓllÁÈ¸àÀ¹àä÷G£èD()?âîH§\nöra`', NULL, 'Cash On Delivery', NULL, NULL, '4', 0, NULL, NULL, 'e8vz1591350215', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'shipping name', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-05 04:43:35', '2020-06-05 04:43:35', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(9, NULL, 'BZh91AY&SY^»L\0©_P\0Xø;ßü¿ÿÿú`ÿ}\0\0()@)E\n)Hi0#À\0\0Â09¦&4À#\0\0\0ÀæÓ0\0\0\0L#b`#LÂ0\0\00IjdOQ¦!¡4¦\r\0© &£MIµ2ze6¦ÐÍ&=K eqð?\">%$K%¤v	ÚoÐuìî\rÜÌm©ê68ZC1fñ*^N¢Ë2Yû.´eFØa.ê|TbênjÐæÕF[:Øa.Ý7jÿk(Ô±i¡¡Úzê&KÛ\n;ÚE¤ì;M_ºë:¬wséu¿CÌÝÍÔá7}Î¶|exzÈõ Z|Oñ>³Äï(£È²Äü]Ó¸£½ªç¤ÀËîl=§×=ÇròÊ÷\nL&CVi£°÷·~Dz9DWc-ru5hYcÌyÍ¬Õ\"MP\\¹`æueÐêJ(¢Trl¼Ý¦æå¹µ9*\\9#TÜ±Â¡y+eáL²³vª¦¬©BÄ¥Su×7Sv©³VêIe2O*(´Ü¥¥!J¶]»,½g.¢å(©%d«D6YfÌ/6zØvf\\ÔÕª¤¥²²Ë²Øó¤¢*FZ.Ã.µ\\ë(Â7ÍäR¡*aK6nØÕîSÞÑÓ®¿¹¼ºÁÇ\".d[ºhi³ví1Á³NgY¤ºwKË%ÇEfÊg^h]vR¬ÃfFjÃv¯77L0®Ú2Ôe0L5hÂRj]jÑI£qC¦\ZVeÓTÑ£tÊeíOò´V RE*$( \ZVQ(!QMD-,È±bå»J¥âbÐ²¢R(¤¢¥.´BÊ£A\rÇhÒj\r³Vå\r\n¢õJt`²U·jÐË3u×%7eW&¬r8-*#+L´Xl£B8\\á£EÓ#¥ÒêP»K°ä³F³v7eSppÕ³\ZNMÖY»VJZa¡w¶{ÙÝ:>ù¶?Vç>×)øÄ>TöNëàÑoáÒe=Ï¤×®SÜ|ë:¬hûw¥ÙXS*(¤5YÛ!-&\'ß;|eð,äæ³VF¥ú={ïs¡Ü4x^tç37:×XhasPòð2w¥Gç-¨¢q2}lDy^âÁÎ{§9ÒpêñâR\"¹Ò\Z×ðÞ7K¸ç5\Z6YS)4?SqÐ©ræÿ)ëw½¬ÕÞña¹±¤Áu×466M¡±JR\\¶Mgâ\\£ß:çtâhc6lYàXÚh\\Xù8`r:\\iS²S=,Î§kr]£Îï«÷§Á÷£è]0ôÎo<ä¦Õ=/S3çm6NsØR^SÁwó\'[Ò}BÇÊ=|ÌÏQ*\"Èv¤ÚOTùß3Iwsë.ë|-<fÐl\"h203	v>ÖÓ]ü\rDMÞ÷<JQÜð»Ù%%I0ó¼g¥Ö<òÅ&«.=DÙÁ\'RãbTØÈYyw$Þ]æ|Me!ÎÝ ³j1¦vÃbXÛdÀ4L¤õÈ¼¥¡ÂvJä±å\'\'8EMYI´ô¿)w©7h¤©ð£wöv(á:\rNû\ZvÝàr;Bç}æ,Brç¾uº)\'DîSÑ,¤£±âtlç<ÊÃu¡Iý)\'¦V¥Åp¡ â`;Í4Ã¿NÐrÈlÄ dõ 1Hc#h 0¡ £DQ%YyÙ8\'zÈËµ\'2É£ñk9è-,ÚK£eù¼²óªz\nZFRÜñ.Õ³h©S¤ZQEIÄâï2ÌÏ`37MÆÇT§30Cé*£ìxÍ:§ÌÀZ&	f©á0n·?Ì7m+°7BÃs \rò+L14±³ãsÎ!À#ó;^CBa¢¤ÿ¹\"(H/FÝ¦\0', NULL, 'Cash On Delivery', NULL, NULL, '4', 520, NULL, NULL, 'jKNF1591350507', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'shipping name', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'hello@wirelesswavestx.com', 'hello@wirelesswavestx.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-05 04:48:27', '2020-06-05 04:48:27', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(10, 13, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'DRAx1592300219', 'Pending', 'vendor@gmail.com', 'Vendor', 'Algeria', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, 'Algeria', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 04:36:59', '2020-06-16 04:36:59', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(11, 30, 'BZh91AY&SYa*Y-\0Ôß@\0Xø;ôD¿ïÿúPÙ½ 3\0Ps\0À\0L\0\0\0\0&Òjª£@h@\0\Z\0D4 4\r\0\0\r12\09`\0&\0\0\0\0	i	ÚM¤H\Z\Zé=L$n68ã´:öÒU}Uf9téssiÔm6+Vrª9níiUhÓwYzs&ÈfÄÈ¾ì-B`bº²\"ph¤^¡ª~,4&ìlIÆæÂ¦Í¥pd©¸ãØP±Ùî*µ¥ðiÈ§@;ÌwI\\î&áè9Î§¦aÐÃ2§´Kp¾²*JB³<ò¬e¼HFB5\ZKF¸4ÄÅ¥ñ&\"Ìù¤\"îÓK£È©D{!)±»^ô½DB`@Ø5IV²-ä ¢+i-RÅ¨Á°µb&Ø0TE\Z)Y¶Åäm¤µ©CBe_\ZOx`ÀÆÖ4´=+pPÁ(P$¥Ë Y,¤á6ÐfCÒ1{<ªTâa/¯lTÝ6wpæ Ðr¤	 9{¡FrvOá5	j-£Y÷}µ1m3d¶mLl±)Ù²ÜUK\"ÂTÖ!¤¶BÙx]{½¾àßÉuVúfî9Á¥êV¦µ7ðÕéBÚ\ZkkRÄI fÐZøÖÌ¼uÞä2²!ë¹¦#J³9!×¥¹Ä^£¸ÉÀ|Oçà}GØl$©ø-ç¤yÍÞ¦¦MÄþø÷%êûÙ¢&°ª5.\ZV¼m<Í³òð?Ò\rvÅê æ}æÄý¾\'ÌØoRG#ÞºT¢´2j:w@ÜTäµ\Zæ~¤¥§¿m9062m1 0<êw#AÝ`ÑCbó;;\râÛvÀF	U3ÑÚ~d>#SÛÈÅøó¥ôÁÉ+Wtî°|O3\'%FI281:<	.pxvGÒ¦»é°&Üø/ðk\'èu=\ry;v(:wù(ÛqÝnôÅâñ§àpÙóÈ¶E¨NgcH¦C:ÏÐ]²vÃ:¨ÖtýÌ[¨ÏBNA=\\×xÙÃ¸½w£8Nç®QDEz¸k\\\n\nNW3©NY.AuRBT09c\rä­Å\nRö\nGØ¸?ãbáÞ-y9Az¥Ãþ\'ªÅí\ZÓîß½§B©{É; þùÃFÙrÍ@£[¸m3©¡Þ¸úÚo3É2F¦BÀi\\äAçv¿GKx>Üäb`1¿]bRÉe*Ä«#[$eBl$Þdï×\"¼è*¯¨µ  £BBõn!ëõ8\n·¡·I¹&j±3Ê*PÂÀ]j>h©@²2×´F°Ù÷wÎÃÖ¹%EE\\Ñô>Ò\0a\nâl8µ²2­Eæ2ÓÆçD/0A×¯¥©¹?âîH§\n%K% ', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 159.99, NULL, NULL, 'KzTW1592301344', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 04:55:44', '2020-06-16 04:55:44', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(12, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, '9ZXW1592301780', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:03:00', '2020-06-16 05:03:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(13, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'slAq1592303830', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:37:10', '2020-06-16 05:37:10', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(14, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'LMze1592303876', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:37:56', '2020-06-16 05:37:56', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(15, 30, 'BZh91AY&SY^¯5\0ëß@\0Xø;ö¿ïÿú`?2M°Í`4\0Ç4ÄÀ``\0\00JFD\0\0\0\0\0\0P	M@PÑ£M\0\0 b`00\0	\0sLL\0&\0&\00\0FFLI\ZzÑ£&Ò4=\'©{Ç`HÜ{Ã­^_ÉýL<ùj<|`±r£%¯ÄGðEÉÌdôeOuYNb¼dÄPÆ^DëÞMi¥£áKÅLiA<Iä)RäÌ¦NÉ¡©YfïG@#´`æ4@Ðº	5D@sê3èfcXaÜ<P ð!#À%ëj´­HB1¡6\n Ó´R¶öF/pÑA¥@¡YÅ-\Z<a£ÀlbnøÅ$Â©ÄP&Ð6\rZ{ÚiaGD4Ù¯3LzAT*p;´ZDPyz°É 6ÉqJë5,1µk»Ò0PÌ¥H*`±!¬2ÊKÍ°Dd%V1 ;CÙìÅ´É!\rTà\'\'Éâ**<CÍ¦D«B\"ÀÄ²:Y* ¹%&ã¸>©ÿ(b@Ø@Ùã\nÚ!¤ÚGY$KMÂ\ZcÒhöP1Rh\0rBA\r:\næ­q°èÙ	\nÂH ÚÇ.ù^¥*dãÐéÛ¢YÊT*øL ¬0QJEX£-KÝÂQ/²2ÝÆ|\ZµZÏ$/qP¾UâOz³AÂBJI2»2óPÈiQâ«Âe9)¨Tñ7ºÉ.³nÄÓ¿R!¦²\\4¤µç`©äg?ÐÃà¤ëª¨5.33âuØÖmRGYë\\jQIÔ\Z\Z*Ó¬\rMëA¯%ó%,ÊítHÞÀÖ2Æf³:ÈwY0hlZÂ÷\r\reµ-Pa¢©5ÀÍ§¸jvõx¾îº_),|ah3+CÝ,##iQ¦f)&G\'BÈg2K£JftÒ<ÝYSNÐJØ¼öú/Ìt8Æ8GÄhá3ÖµQ[J;Âr2\"òôï=éj¡÷¡%ò=89¡\"\0cÜd43¼û½6\'·)7qN$ª5_#Â*3Ì¬\'¡zä6mì/^ófÐ)ÇÎ\rC´Ê=½J\\\n5¦C@+5¼§UFUaP¡	S8KaE¨¡@ª]ÂÀ²]øÑtvMÀf)2Õ,¿By¬Xi=£Y{ºc³yT ØTÞäGØà1281±î1@£Zèq!¦v¬Îkpñ¨ÜuSl!âÀÌeaL®HuÈ7D8ß¿½Í¡|L7àÈLHØ\0ØØIö6Ýpp[r\\ÅeÈµ  #\"¤Bìé0íìpo#R®cià`ÈÏ©Cu ûRdfÑøõa³\rþF3Ôr[Å	QQu½ÖJÿ¿q\00q6u4AlÆ@Õ¨¼ÆSÔwñB±ñåÝÞØ6(`þ®ÅÜN$«Ía\0', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '2', 86, NULL, NULL, 'yVNy1592303936', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:38:56', '2020-06-16 05:38:56', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(16, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'cNNV1592304048', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:40:48', '2020-06-16 05:40:48', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(17, 30, 'BZh91AY&SY^¯5\0ëß@\0Xø;ö¿ïÿú`?2M°Í`4\0Ç4ÄÀ``\0\00JFD\0\0\0\0\0\0P	M@PÑ£M\0\0 b`00\0	\0sLL\0&\0&\00\0FFLI\ZzÑ£&Ò4=\'©{Ç`HÜ{Ã­^_ÉýL<ùj<|`±r£%¯ÄGðEÉÌdôeOuYNb¼dÄPÆ^DëÞMi¥£áKÅLiA<Iä)RäÌ¦NÉ¡©YfïG@#´`æ4@Ðº	5D@sê3èfcXaÜ<P ð!#À%ëj´­HB1¡6\n Ó´R¶öF/pÑA¥@¡YÅ-\Z<a£ÀlbnøÅ$Â©ÄP&Ð6\rZ{ÚiaGD4Ù¯3LzAT*p;´ZDPyz°É 6ÉqJë5,1µk»Ò0PÌ¥H*`±!¬2ÊKÍ°Dd%V1 ;CÙìÅ´É!\rTà\'\'Éâ**<CÍ¦D«B\"ÀÄ²:Y* ¹%&ã¸>©ÿ(b@Ø@Ùã\nÚ!¤ÚGY$KMÂ\ZcÒhöP1Rh\0rBA\r:\næ­q°èÙ	\nÂH ÚÇ.ù^¥*dãÐéÛ¢YÊT*øL ¬0QJEX£-KÝÂQ/²2ÝÆ|\ZµZÏ$/qP¾UâOz³AÂBJI2»2óPÈiQâ«Âe9)¨Tñ7ºÉ.³nÄÓ¿R!¦²\\4¤µç`©äg?ÐÃà¤ëª¨5.33âuØÖmRGYë\\jQIÔ\Z\Z*Ó¬\rMëA¯%ó%,ÊítHÞÀÖ2Æf³:ÈwY0hlZÂ÷\r\reµ-Pa¢©5ÀÍ§¸jvõx¾îº_),|ah3+CÝ,##iQ¦f)&G\'BÈg2K£JftÒ<ÝYSNÐJØ¼öú/Ìt8Æ8GÄhá3ÖµQ[J;Âr2\"òôï=éj¡÷¡%ò=89¡\"\0cÜd43¼û½6\'·)7qN$ª5_#Â*3Ì¬\'¡zä6mì/^ófÐ)ÇÎ\rC´Ê=½J\\\n5¦C@+5¼§UFUaP¡	S8KaE¨¡@ª]ÂÀ²]øÑtvMÀf)2Õ,¿By¬Xi=£Y{ºc³yT ØTÞäGØà1281±î1@£Zèq!¦v¬Îkpñ¨ÜuSl!âÀÌeaL®HuÈ7D8ß¿½Í¡|L7àÈLHØ\0ØØIö6Ýpp[r\\ÅeÈµ  #\"¤Bìé0íìpo#R®cià`ÈÏ©Cu ûRdfÑøõa³\rþF3Ôr[Å	QQu½ÖJÿ¿q\00q6u4AlÆ@Õ¨¼ÆSÔwñB±ñåÝÞØ6(`þ®ÅÜN$«Ía\0', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '2', 86, NULL, NULL, 'tPL71592304149', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:42:29', '2020-06-16 05:42:29', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(18, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'pDaI1592304551', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:49:11', '2020-06-16 05:49:11', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(19, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'I0Ko1592304752', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:52:32', '2020-06-16 05:52:32', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(20, NULL, 'BZh91AY&SYg%j\0Õß@\0Xø;ôD¿ïÿúPÙ½ÔÍ«µ\ZÝ(!)	©£h\0\0ÐÐ\0\0M¤ÓJS#Ô\0\0\0Ð\0DP\0\0\0h\0\0Ì\0\00\0\0\0\0HH<#LCA \0ô¥åH\\\nhAª\"ûÙa<HFv=Ì,a0´qM\"°Ë¬QÆg¥ª{Ö\ZdÀü4t9ÜQedä¥ÆuÊEËÝ~¨j	9ÜØTÙ´®7÷,o÷8ì®43Ð§4 E¡²^¡ägbö\n(ã!ä@yJRâÈ¸£!vùB¥äDÔi&ÁX\ZbhÅ¯&2ZHù!Rwtº=åJ#ÜI	H\rMÚ÷¥ê\"ÚÁªLÒµp!(Z&k¡jØ©ICajÄM°`¨*4R³m¼âÊ¸µödÑç ê¤[8î×·ÚÑ6#F\"¹$.XÐÊLM f,¿\Z­\\=²C9Q§ MUÎØY©³`à¡	É\rM¢ÚZ|s³Ëð6AI\0KU5Ê<ìÑ$j#«,@Ø,R ÁL h6³nã~êÊBÈä8ç/7R)/\Z3\'KH\Zá¥ÂL$­ µñ%¢0g2f°YÖÊÂ3?	¡^¢ÓÙ74²Ü7¬\\Ðâæ;Jb)È\r6ùyLÏ©Ã\n#6\n¶ù/P¿Å\"k\n£Rá¥kÆÑCÌÛ?ÀÏ¼ô,tK}¨²CY@å1ð.±,´yT¦Þ¨\rÅNkQ®èJY*|6Ñ#ic&Ó\ZÎ§j4Ö\r46/3xcÆ¦óa¸¶Ý°aLÁ´fr>D?PÔòæEñ]N²«!³ENjÝ\rTÚ9Ù³²FI281:<	.pxo­GFô ÉÒU:ùAp/-´C\\S£E­ç\Z<el³,7µÂ<fé¥\Z?\"DW!Áy¶i ÐÏ#Ùë±=EÊNG°Á¯PU\ZÎ±aèIÌ\'zEÜ6qí/^èÎ S¯qßaD\"½J\\\n5®C@\'+¡âSK]eT¡âP¤æ%LXÃ+qbT½bÀ¶.#øØ¸÷^`eA^©`ðÿñX°Ó0}£Z|xG~Ó©T¾²MåÀþù£#Ll¹f¢(öhÚg¡Ü¸úNTáaïbÈÊÂ#MS!Aà4®s As·_g[x>¯ÜÈÄÀc@EDa¢\"¬µDÚ\'\'v½!âQU}%¬l!\\%\Z\"fè2ý­àêmÒnc	¤dÚ c ,dÌó0°Z¢*P,µíÑì6|{LgaÞº%EE]ÉôND\0ÂÄÙêhÙV¢óOiåsª òñòBl\Z)ÿrE8Pg%j', NULL, 'PayPal Express', NULL, NULL, '4', 433, NULL, NULL, 'KQda1592405340', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '78701', 'dsd', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-17 09:49:00', '2020-06-17 09:49:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(21, 33, 'BZh91AY&SY+£sU\0_@\0Xø;«ôD¿ïÿú`ô\0 \0T\n©\09L\0\0À\0L\0`\0&\00\0\0À	\0L\0À\0æ0`\0\00\0H	¦ÓÔôSzjDi \0Ð*D HLI¡¤1=A hddb80GØØKR#ÅöÕO½üÞéÿhüE¤³îÃð>÷ß¼ÌÔ©>éRO\'i¾*b\\û¥LI§úajx-q¦)zL;íFpäÝ±ÞÝ²<e0å¥9nüVtÝµb[Ïõy¹\r<£N¸Tî0x¼_ñ¦ôÎÍßC_bl{]ÍÎMÛ³òJ÷\0|åååsa20$i9ÍËó13Új;gÜD­Hõ¥¦)ñoé\nt¢4aíQñefTiêwÌDR÷$ì©!E)\'\nKT7¨J%IÓX;ÙRíõ?6PÒj2©I[¸Zô¦\\\'Å§sPø²°É6U¨©¦îeÃG½Iß)¥&©R©BR¥\nM0ËvGDñjE²ä0¶164ÙÌëfÎ!J8ZØeÜîaÛM¤0¤lÃ\rÚx½lÎ;ÜÂÕ	S¿°åÒÚj[¿we&Ó¤ã$8Ë72d2hÑ©\r¬Ù4mkf¦%Ã.Í£6£Þl¾(Ë\"-»gC,7eË.Xå2Ê*nÃi¦ãI´ËvÌ¥%°Ã-´Ñ6r(eËqf¦U©Ý6lÌå4|aøIÿ$bDR¥D¥A¼1$µÅ¿Î\"ÊR×-R)QmE*Yf\nRSÍIT¹¹jmHµ\nI!(\\µAJ)H)IOÜïx§$`ó*(õ2µ0W.[´e©ÊÜO-O%»)Ë&8vpÏfÄÄGfË7_\n6l·N:n¶ê4²·2akL*-S¦S³Mæ\\¸aË*:-GKSfìì8[Ý²2É}²t¯Ãæ²}O\\úg^RçÆçÅAÎzHÜó\ZÛa\ZgY?\"Àï±G#\r-4RaKPlÝnø%©ààLû0ì$^`H° ÉÞo2{ÛÏ\'ÏÉþºc6G+½2(Ô3q\\Á{I¬ä	àhô;JsüLDêhþöa=ói±ÔïËOF¢äâ{ÑÚS¤ây;a-©ü=rté=ç®z§ÙêvX·TÒSÚåâTä£µirZ2kÐ¶ënó{oÌÁ{LZÎá¼ËKR7)J2M7N\'÷?)ó§®vçq2ÔM92öm`Z±&LdQ¼â*3¬ÊÎSê~LAAÌpï¨ú:ÉÊÜvò\nÎ!l6ò¶`39íFlÀ{YÄ@È=Ð\0W¸ê-v!2ã¥ë:Ù¥8ÉºÞ¶ÖOQxfÅ¥e¢QÑÌÒ)æ=å(§Ý7iã\'þSÊIÈ÷ÏoIe&Ëað&íL·©;8&g¶&\'ø2öºo.n&¦ek38ur¦-J<¦cÀÔ±ÒOÕ#äò?IÜOy;=¤u\n`©svâ|Ïó3hÚV@Ãhp`n<\rDvûºZÏAñ-ùÏt¥-ô¾J)Iô¹:£³Áh@AÉ¤ÎíX.48¼ObáHøÒN¥M.0Án{EKK>H±ædÉ69ÂÃ aà|ÆB!ÞC `aHHbTd-*@ªH¤	ULOÔùkFI>Ri>¹³iÜZÞL#kOGÎ¤ùÞ\\ÆËSäpåÌTiá(¢ÓiÔëËjfp\rÎÒpÔÊm´?õÁÂ7ºpéÝ>Tó)¶éçx½³/×)æöKR\\pRl©.Q²K2¤£/¬ø7×½óHGÁô!JJTªKRUü]ÉáB@®ÍT', NULL, 'Cash On Delivery', 'shipto', 'Azampur', '2', 319.98, NULL, NULL, 'os9l1592576769', 'Pending', 'shayanhaider666@gmail.com', 'full test name', 'Austria', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '20132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-19 09:26:09', '2020-06-19 09:26:09', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(22, 33, 'BZh91AY&SY»ÿÖ\0_@\0Xø;«ô¿ïÿú`=÷ ÷+@PRAÀ0\0\0\0\0\0¦¦§©?J\0\0\0\0\0S5CÒ\0\0\0\0\0Ð\0*%)¡ÐS i Ó \Z\0ÐÀ0\0\0\0\0\04M4Â\n~FÔõ<£Ðz§©ºÐ]\r°Å$è{ßWµàî­[håô>;(Tdµò\ZwY ù?ñ¬²ZÆ X5)ËgeB×{9Gr×zh_³3qAÛÀÁ»cbË<Ù¸ÀÓ÷(E\n*r×]t0pêWª¡4äj`ädêllo>Þh/y¿ß#¶¶R]é¡`%ý\ZáH(Ìå@%¨Ð¸B	\ZVL,ZòaÕõ D^Ã(1¨á¡ÝfÐU17[R¢©ðË\rY±¶	`Õï4½~ý 4: ÄÞÖ)d6µæiVbJÃ¼È ÆHEi($¢Ñ,ª`·Ä©©*P$¦\ZB ÙVÔ^b\n4JÚïwÁÂ¹w\nlEÍÔ¹FÌÌ®Ø¾\0X@u0cå$sò\n@¡l3^ÌY4[fc,[6¦5KXÆUW¹e¹%©d²*%F¤¶BÙxìÖßÚÉv$JV÷	¬4HÈ{I*X$H°B®ª[@1øÌa´`ÎdÍï*°;Ü K	j^d­nSLNfrC2­y²×8¨àèéâv=çøYá<F6\0ù0Ã#\0ÒÔØD9ôt´+\rKø¼ÎÂcï;ÿÑv;{ù¹ªcàø·ö=¯çò\ZÝé£66N}M©£âj::¼¡â²5üùÃ2Xû·àªK£yrá©¼Ý57:ÁäGÈÝRBÚâÅ&N!ÈZÇ±Þ[l@F	UxÆM)Í÷\rO.d^/Ë¥4 ÅT0Â£m\\U²¹úÁèMK\r}7\n$pfw£;]ðj8Àz?*÷ÓpMºr>_°× $gSã?2=Ò¦¦î¦]¸(<ý>%J=Á:\ZW\Z4VÎT×Ç9ç*ØãdUd8[£Iì={X¨¸ÉÞzvÔ$F³¥ëtPgÔNÈ^ê632õî«8NOÆ1^Ü\n5­ÅBåADÈ\nªÂ¡B	&/\n¦\n\"(ÎâqR¡T½#h´;bWwAkÄPÓ/T>Äù-,5[®uglÞq5#\ZÜGåzÝöt¬ð­:[ð{o¸ªÑpci·bä=69éákàÒ22° F¦BÔi\\êA(ç¾nM¿%ä¼Ñï*¥E[é¬¢ÊÊÅ%Y&(´¤d¥i£ÙrkÎ^ô¼ÅUâZÆâEÂQ¡B!tÚ§GàZ¶g&©²±3¬T¡ºÔ|ÑRde¯´_ùÛ\r}ÝLgqæ¹%EE\\ÑE=Ãçà@!`2ÎF,HÕ¨¾£E>Ó·ðy!{Ý½PÅ¹?âîH§\núÀ', NULL, 'Mobile Money', 'shipto', 'Azampur', '1', 159.99, 'ukljhjklh', NULL, 'wIdn1592631215', 'Pending', 'shayanhaider666@gmail.com', 'full test name', 'American Samoa', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '20132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-20 00:33:35', '2020-06-20 00:33:35', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(23, 31, 'BZh91AY&SY°5\'>\0ë_@\0Xø/kô¿ïÿúPûÜC\0hÐd#\0&\0`\0\0\0\0\nhC@4\0\0hÐ4\0B=C@46¡êh\0&hs\0À\0L\0\0\0\0$LCÒ\Zji¦ÔÓ#õ&	°ÐÉ¼\n{©¾p½ü5Aô0æÐê|þr,L É5ò,é\"ÉuJXd]ñû}Ô\\Fû\'\"UÊÙ·`êSW\n9ÍfÄo4VI8Ür~Ë8í9Vò}¢.	eMg#¿ÓØ0:%Ä³Ï¼Å¯U 8=PL$ºÒM4Àa©Þ/yVKÀ`H&8C8£cÀ`¨ct­gZÆ°j+Me(!´Ä±Yø^\nå,AT\ZpïUxþÃ¥ª*Lí$\nòÓ3²l@Ão ,q,¡Ãã	Ì=#Æ)>ÖøÆÈ¹HDbK-Õ¤#$UB*3»ÌÕÔytBÈéiU¨êºÝ~QèQü?¡ÐF¤(M3£íÎaS£4 à,± 8bXFµb¡ä¡Ar8)É¾L¢ Ò(¦ùââÂòß\Z¢^ìÔdÅu¹oR^HÓÐ¬@©¼9VØ5§\"¥L5BéÞÍÖÃÖ0¦´2ÑxMÛ²ÍlR¡:Ç°ôC¬|NáÒ|;\'ÑÐ_âPÖxØ´h`?xþº¾¦ÅaæÞ§\"pXU\Z¯ãaSîlRê3È±zâÔPiÈ÷ðHÚ¤GåBjFàÜd:\Z½&ð58¬Æ·~âhÖ2º÷lÀ¢GÂÆfÃ<sPÑÊæ\Z]/¡°-ZLÍet¼\\2Ø||Æ¥»nß;a\"¦áX¤1Xmà+Ý`yc#iO©LÌT%!Á´!,t$Ypf¬çC>Ùè®<¿Q­áäLò}§|*yÍ;{N-F³4Ølá;ñ©Ã¤ÜR {Ê.rC\ZD ì&0ýûúA#¶ÈÚwtÈ$IM¬pgdLgRFà¤.ã{m8¦°ü³`ãÜr7aXæ+LYXS,ÇÒvëä¡¢ÓID8í tx\'c\rÖ	óC`dl =Î\"Ëp¡È¨Î^Â\\Õê4ËÑ¬<vG-%A¨¡¼>ûæ4ÆÊ´¾UmhÃ`Ï6\'5´vÌÚnd!<+\nd³<Ó!Aç\ZW8A¶ ±Ë>ë©ò~°>!ÈEyã\0`%0h%F´;gXÛ×ïó\Z&÷uv«j,F$ÈÇ\\>.Üäi¬^é¤bh c *bc-ñBeÕÂË!ðE	Q^°?5]±¯ß¸¾9eÀ!)©¨ºÞí%øý¤\0Â6.&ÍÌ,ädH«Mé$ÕÊqî6£ý98ù ¡È,âîH§\n¤çÀ', NULL, 'Cheque Payment', 'shipto', 'Azampur', '2', 71, '123', NULL, 'DtRp1592667960', 'Pending', 'shayanhaider666@gmail.com', 'shayan haider', 'Argentina', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '78701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-20 10:46:00', '2020-06-20 10:46:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(24, NULL, 'BZh91AY&SYát\0ë_@\0Xø/kô¿ïÿúPûÝwaÃ×h.gA¡(Õje\rIé4y@\0Í P(ÊOP\0Ð\0Ð\0\0DýSõOSM=L\"y©ê\0 Ì\0\00\0\0\0\0H #Qª~&TÓÔ¡ ÐÈò!`RF SØ}Mg Ú?³¡G]ãñå\n¾·ôEÝdà2BL·L?dvÙ¡ÙFF×\rN7E\\ÊæÝÉ^A\'4ÔÈ¶%MÇåüËW\ZX:±Ñ\"\n(óëàe	TC#u2È¹GL(¼4¢ÁeAH£A¤@iÅï&@Òð!f¨\rMÖÖ¥ªf4L\ZÁªÒkjî+T6Ûi¼WÀ±%ö 0+D6\\fi(¤Ú)4­_¡ÄqBE@bé¹$Tb`crRÇÊ7a2R9¤xÅ\"áãaÌaÌ$F$¹ÂÝXJB2ET\"£;¸,Í]HPHÀ1$,Pw\\:µGîþFÂ2\"	j¤$:©¢F5ED³EÒ,@ä¥$]°¥Ä¤¤*ÅQB@fû©Í´: DAä(¦ÓÉÅé/*¢ìÔd9kÜÉX#,b8P¬@)¼9LØ5§5,RËª\ZÉ!CC5)¼>aMhe5¢ñ¬cxÑâ`cæw&¢ÓÜÖ<y1 åè$æ:,¶ØÈ¾Á[û¼CÛð«È-2PÔ¸i_÷l (=\rø÷ Ó|^ª\rm=¹ö£JQ£\"E-r4¨* ,ÅPXæ´\ZâzwFñÔg»lJ¤r`l\\ÐØÓ-5F#ºÀÅCk±¥älÇµfòÚáªf¦&;àÔjxr\"ñ~i|d±Àfd%V®6ñ,Oyæfvö$Jf©282HK1ä\\»¼p2¡f\\ò+tà×Í¡RÀÀqÖ\\ÄA?1~T£Ë¾å\rálLChÝ¤ç<£ühH&äRð ËçÂAÙca~a$ª54.îñ$à¹¸âÚ{r/]áófÀSqÐâ1£¨¯Bn\ZÎâ¡r@gIÕ©?¦qÛ,(@èìDQ7Z (KÔ)4ÃbÖJÛ³àHrõK§¬«\r3Ø5·hé©Ì¨78üÎDu\Zce°Ã;Fµá°g×Õvú§\nvBx1uÌÐ¨H6e/DÈPz\";b4ïëmÏ£ü\0ýÂbÏ\n0!\0!¢²ÔX\Z8ÌÝî^²É«bîK¡{\Z.ø2¼Ü[ÀèkÜÃÒ25P1221R\0ºÌ|Rá©2ì,d#íÚc³ñ-ÛµÊõòÒ\0a0kWrfIW¢ù/ÒpÐÜðÏhÈ\r@ÿÅÜN$\0 ¸]\0', NULL, 'PayPal Express', NULL, NULL, '4', 433, NULL, NULL, 'CnSG1592668076', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'dsd', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-20 10:47:56', '2020-06-20 10:47:56', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(25, 29, 'BZh91AY&SYÇ×Í\0,_@\0Xø++ü>¿ïÿú`ÜÏ´hÔ\0ÐÀ&È`\0&	\0\0	\nm@\0Ð\0¡ \0À&È`\0&	\0\0(JhÐi \0\0\0\0i ÐÀ&È`\0&	\0\0\"hÄÄÔÔÔx)¶!4zz¦oëO¡¾|Îñû6:È¢xÃðB}CpÄF}\rýØn9FÄi<D\'\'ÌkYq¨£âì¿d]zÂ>ïÂäßPÐãB9%³nÁúSW\nu¬ÛyXk8û\r%õæ+²Cg3»¸NeG¨!6!_ñ&$)#ÀwHã\r#kÖmÀ¡Ð©äv=d{À¨Ð`½äÐ¢TÉB$á)\\KÜy[X ÃHÒ!`iÅ27V?Áé¡\nPµ·)Ï\0 â-v½)zT>#¥0Lh¯i½ë(LnEb¡X)#m6ØR¥nh*\n­©CiHÍB³2\Z\"±8ÉÒE!!å¦^tA7@Â8ÄPá¸qM ­Z<áÌ#SvùÆ\\É¸ÂÌØJB2E¨Îî3WR+ä)**QÔukPÉUVÇ\0þû7É\n\\Å/lãVcèá³GL47::ÍbJKC ÊDB¸@¡M <÷?0ès±A%9Å7±XX¾EãTSÝ¥uFÍy¢Þ³¨VM©¶0¥ZsTÅ,°ºaÇ[tÍ&ÍVf[2¢Ðêk%â-66*;Áçô<FÓæPò<SÛGp<x0Û\r>{ ç9@¿p²ó¢Ç.ò8s_Öä¤!Ú¥ÃJBø¼Ï¸@P.z\ZýF.EÈ4l¢ª\r]33ÿü¾gæI¹I­J);CA¡TÖ©¼\re+H×SüäQ[PÏ-¹HàÀÚ0¹¤ÚiÏH4 l©Éë#&\r\r­Í-¡l`+S¡\ZËiF #Ã*0iOÔîàEâû·ÒùX ÌÌäÂeeãn¢½ÖÔó3;ÊB1%(82$%Î¤]ßb]¼=Fí«XÕÛ]a(½·_ìkz¼<&S°ÐÎNû g.t(Tð	ÈÈ7gâ/Z9\r­Aü%\"ÙáírÎªD¦Âl@EÙ5N½ Ü7D4¨Öy~-TgîIÜ±ýÎ-¶nï1nFàô;@¯:80´Ià+Ô¥À£Z0*uH¸R\n#5Àî*ÕFo,³U*B$Ê\Z\nT±V4¥¢¤©Hu\"ÌÐ¸ga!÷/iEÙÈZ·¤iCA¥ÑbCÁbÃLÁï\ZËápíEA¬©Äø° d}CFØÃR¼ÏM£ZÃØ3¯¡¼Òu\\³jn5îi\ZQ%á*\r$S!`$$%ÞAlAË¿fÃ£!Ñ}M¯4`@L1D4 6$¡¤Á¡6m39èß(¯b\\eÐæP/sY#¢3*D.; Àtâà*ÞG#<¥1Ff¸2Æfs¾*PÂÀ]h6Á*F°F0Ø×üï1£¢à%ED£#y(óÒ¡ @NÐfØw3M5z/¢ìõ|Nh\\`ú\0QôÜ ´4âîH§\náúù ', NULL, NULL, '0', 'Azampur', '1', 20, NULL, NULL, 'EjtW1594625238', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Albania', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:27:18', '2020-07-13 05:27:18', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(26, 29, 'BZh91AY&SYsô}M\0vß@@Xø+Kü¿ïÿú`|©H¢¤9M0	À\0L\0\0Ó\0\0Á0\0\09M0	À\0L\0\0&¤M&$§êÊze&`Aêm\ZÓ\0\0Á0\0\0\n¡i´\"Ôa©! =ORÁ!Hh¹;\rç°ú<pô>AÜ$?Òþ0DCàQñ±ñÅ32Ìíãö(íÁcN³óªp6§&òÛw%hg¿«Æó½âÛbvÒÔ~ßM1Qw~¬s_FSÛñsfSÕíå-v}~)ïGGQâºâ=IC	DqHP0PNÐØCN.\'jNõÜÞæí9BGõ0ýåYä³&Ä\0Å½³Í	z3´	:¡ÖT\'{RQ\rã$A!Ä04JØ<¡bjD¢ÜfÎZ«2¢(±\'×3³D3\Zsµ÷2ån(©4B°ZÆWÅe6\ZÐ0h\rD\"0×[k¶.v1¡;2uÞÛV²Ãm³«c\rÒI-±ÛÁ£Òø¡F¦QS	,â­ü5;4M´(å+\\1ÍÉvÊQNL.¤µYL5ecSe·ÆR¾4Ù¤Vi{´/m¯µõ³fÑuÑ--¬ç\ZÅEôfñ1VµÆ3¦p6$½¢úÂó£HÎnÚ1Ã ;¤\'Î}IbE°ªö2j¥F-(¼SÂ¹r,Å¨$ýAH20#\" Ô\n@@X®Gc«j§_:F\"?4¹Òý´éÙ=¹Ë6ÓF÷¦ÌeÛ¾8­sÛ]oÆSK#Ó:SYf«S]lkr¡Ös¢àµ¶qsMÕ¦Hî´·¦t¾cõ­0¶PõÏÁ÷;^UÃñ>3÷:#ÇF»«ìË.õþ)ÀýzÏÁë¼ÃÀ6öçû6/PqS,Ó%=ÖôøÃGÍÎ-þRq×ZCN~/½»òoöÿ7þYì+£¤zax³¸áÄaÕôÝÞ:xG$ÇÏÈò^ig_wfÌD:HìIý|6u=¼uIAÁË\rtØÄ`ÁgèÉQ;áÛÐìg5¢µZ¥y¶sKd»ETú&-ßä­+Nïé¶WK\rªõÍ¥\Z¦g×XÙø¿Véö7bÑ5nZÉ¦¶áQ%ÉfÝ.X:÷Kµ<Lc]·æZã¹ý?DÇ2ÏIoóiÏ]%áãuØyÕ©Â).ÿ?l<K83û,|qa\0ë1?mN8#!@ Áw0)§¢}þ´³Èÿ­ïï=üË-åÇs\\ë%ýÖt-ÎîxÄ§òwyµÏ¹ì>NáK½TñRLÕôiô^£Qv¬D*É-\rã¢þÜ%ÞÌmaP²Õ­$v°¼^¬Jb~lGS*:2iûtÒ8r=>¨ûWï4sðq	¢¦¸Ùéú¬zÆ¹LKWÉ1·Ë¶½y¼BL<_|?Â¿G¹\"n93]øìüPN}NXîNÏmûjû$w4¨ÂÄËÊ%Q±a£à¨Þ\nSÙTÙç¯î÷gy<ëëGäJHÀEô¥\0ADÄH²\0¤a2?3Sj¡zN2btg.o5 YmMÃvQá×N?	£;<íhh×Xnê¤£-ÛÛ¾°»XÔÒ8OHas0Þb>#ç\'V³)]ù=ÑÐTEâñ¬t}ÉÑv5x{_ÁaE½¤¹\"dxFÇÅûcLO¸Ã:|æg`è?ÁéêPRR\rOÀ»)Â£êh', NULL, NULL, '0', 'Azampur', '1', 35.99, NULL, NULL, 'dm4a1594625360', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Afghanistan', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:29:20', '2020-07-13 05:29:20', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(27, 29, 'BZh91AY&SY:S°ª\0uß@@Xø+Kü¿ïÿú`÷-(ÉØ£M 0	¦2\0	`\0\0%\0BSSM4i õ \0\Z\0s\0`!\0&\0\0§¨ô5=@\0P4hhs\0`!\0&\0\0DhM654õCLFÑ©êdèÃØ»Ús\Z´GÐùRê@}>$9:ÁRâ$×Èb2N®r ú89eéfIYÙxöü(Ù.N\n/¢ó+vÊ¤¹T±mÄYaÔ:üÆâõ.õ·&ZóÐ×Ä{Å¡¨8&í\"yÉ2BA	§t¸okÙ°Ú0ÞLÌì=aÌÚ!À=E`@x\"ELD·`à¹¯m .P½Hö·À¬	\ra.7ÃÈà)cYX±Ä	ÛF° Bapá*îìÔÉ±* hn+IÖ(±Çä/	c@Ø5k>30Rl@¨*¨¸Óå°¤ÈAX2dPU\n333!fÀÁ@¨]\\K`¨¡¡°ZDWÄ¼°l&+æALp3$ÖUº-ELÌ@ABdÀã`¡ÃaÆ%}M\nDÞKF¦\"|ÆcdÊA)o©°d*QÜf®¤KÈ(C¤`GKJ¨2\'9TO0äF¤)ã[§XÃJràÑ£²\Zhh`3XHxL,,,AZa@¡:Øj¾ ×$*$>3{`s»cÛZ×\Z2MZE¾qÚaÄÚ«c\nS6\riÍK²Âée±]KSNÖlÖåa³*)­¦«Mµ¹Å7<ç¤â;¦ñÅ$è@NeôlsYm£©ç¢7^±d×sø\n^Ûûêä2L*:²¢8^Ðk@ ,þþÂÊXÆÜ-hJ1»ö\Z=Æ½óú9ÖGä]&¤nLU\rHÿ\ràj*pY\ryòDÑ¬esùl¼¢F\rì»¾óQÔk¦ i@ÝÔèGeTÐÐÚëiBgPVÖ\nÐäbrE \"Ä¡MËÌÆ^3qè@@Cì\ZþC[_,^hM§Q0*¬&Nê`>£N\n	HpZXã2$\\·3*Üc1GT¡ÙæEqÜ>CYÆºøïÝuÀÎfL¡Ì%bÁ fþñ}èâ6¶ÏÌÉö8ÊD)©ø÷÷âD¦\0Hëpv»¶	ì^A½7æ$Ù)µ;VÈ Ï#@h],kÞnæZ½ ô8ÀìÆ9F)ï	rE¡`H0JDHa$`´\'×AÊ«Õ\n@ÀÚP1¤PøZ\Zû1À±2L:R÷[¹>	¡¢IÔJó³êH;Uª4Ë#WøíÜÎEA¨¡ÄýL>Dy2;FØÂÇiÄÃ*6µ!°gvE¸wdn:ç¶ÏÅ¼:bHº	f2d+Â@\\w:¢Îvü:WãÀ^ðcB+ÓI$bLCJbJ\ZL\ZaCaË\r$mKª´+S3$X.	£Bá®CFï9_$\\ZÉ4\rP0dL%¾(L²°\\±¡0ª0izÀóaªÍxñ-GE ÔÒ-ìhQÌg´ëd&ÃFk²m2`ñæ5êyÉ5hrwã:&àÄ}Æ ²eÿ¹\"(H)ØU\0', NULL, NULL, '0', 'Azampur', '1', 35.99, NULL, NULL, 'zf8X1594625400', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Albania', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:30:00', '2020-07-13 05:30:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0);
INSERT INTO `orders` (`id`, `user_id`, `cart`, `website`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(28, 29, 'BZh91AY&SY×òó4\0,_@\0Xø++ü>¿ïÿú`Üø\0Pæ4À&C\00L\0\0`Ld0\0À\0\0æ4À&C\00L\0\0¢Ié=OSCCAê\0\0Ð\r\0Ó\0\0Á0\0\0!@A ¢zM¦Qå&hõ4õL3\re$õGÉ±ño~:ñÌµgØûª>¶¤ªs°úäÙ«\rNÊÎuGïô#ÒAú^©üD+;aît§oîÈÐÅ×¨¤c;êmig¦&Ñâ»fâ°`7ßi´»ýOÝuC3¡êpð;y¸àx>¡Q¢¤å¤&çp_cM{v·+´Ã¸õ:fÍJÀï±âf8î&BOß\n¦Ò8 ÷ÊX$ûî!uAK#j,¢§C_NÒ¯­ø\\EÇa«ìHÂàZ bnøªÅX>FL\ZÁ¬^qJCh«Qh*FÚm°ª´Í[ÂÀ©¢ö½cÔâj3 ®Í¡È´LgJVdÕM¢ÐZÚd\r¥@ËAcÉ\r¤Yl¢úáfL¨¼N¥i&¦+.ÖÀYRX/I¨vµñ{\Z\nY,S±+\n÷£EekÃøH^è\"_	Q1\"û,Ä¶ãÅ³)×TÉªfÑmÙn\"È²Tª,%UBçb²Ë75íÆ.$+$?1 ®¨cê½ËÎ0kZ+ZÜÙÒæ&úf+eÇeà¾ Ë.<ë&·îÐ=Tð9kY#1hÒÌfºÍt1ÂÙlÆl<Éé÷qÄèz|O#\'âXöÇÄî&×[^8??7Í¼ps.ùÆ{Ûú-Wq·ðÇ±ÉPxW\Z\r)fÉ¯h ÁênSóº ÙÂ0(7x>F§ý5ÿ_#ñ$ëRG3ó±JNÀØlV7£æjrys¹m\Zó?®#Ëîñã¡dl#N&ÝvJË ð´4`ÐÚëiqç!kÍÞ_j3&dÑhÌâ|~S×ÌF:ùV4¹C52<	,\r»Y?CÐÔê5,5\n\rEýCIÚBZó$Êíúîáõhg`öµmæîËo	F/Èùÿ#\\áå2FÆs4xá:xQE \r f¿X½è6¸5ìo[y\ZN>×*ÈÀ¥ãh¢¯+8yøÁ&EÖ:Ï#B$ªk]?37Ê,3õ$í	àösiõ÷¿C¬=bÀ-áGqÄBÅÀÖÌÀ;$`*W3´³VÈºÕX±cHl,R¨-cHàRÜX ²Rb¡Èµ6&uê$?²öººw$¨rÈ3d´<Qô$<n4Ìá­=øÉñÜt.\"\råãàÂù#ètÈó\Zccdy®ÛMo°g©ÈÚy®cÓyÌ½uÂ\ZûGàÜIJÄfÉdÈY	ÖBZävD\Z3ìò¾¬<Yöï18U¾ZÀ¥Tª¨ÅªÈÄ44&ÂN&§ÈRu%ÐW^\'AÈ@A#RÄBîáCÇ¹ÀY½ºJ0g)4Mð0dÍMgX£+!°àG4\\°a\Z_h¬8e±¯ñÞg]Çæ%J®hD£Ð|û<AaÚÍw&ÆL,àÎoXÏÂîú\\±;*ÖæÝs.þôKKS¹2ñw$S	\r/3@', NULL, NULL, '0', 'Azampur', '1', 20, NULL, NULL, 'nt3C1594625452', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Afghanistan', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:30:52', '2020-07-13 05:30:52', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(29, 29, 'BZh91AY&SY¬¥º\0uß@@Xø+Kü¿ïÿú`b$xÂM\0)£À&È`\0&	\0\0Ñ1	MÔôõê© Ó4s\0`!\0&\0\0\"£i¦42a4i¦ i£ÌiL\0`\0\0I#I$\r\Zh	LÉ=M\rI¦§ê.HÐþR}?îÅß>ò§êµ#ó8e±Ï<iÊ,Ø!fT±Ák¢{ÚTs,4qý!ô\\ÕÃXÃF¥jáÃÞÚ.S`ìÉAÇ#XsyÈÊÑ=Ó¯jg£ÙÓÍ]ÉÉÐsbÄOBu$Ê(b¢t¼ý\rTiÃ­GsàxºÍ¢yï]÷>GMRq2±1³uOìÆ°Ò\'¾>å¢uïK#eI(¤pT4*Kj|yR&\ZålÖÊkdXÀ£å*ª:K,NÂ\nWÊl1Çià8î$IêqO-À&4/xP9 NÈ	 MÆu8lÁ0Hw\Z|¢Mh0VM1h\n53³p iL]\\bø¥ª*2½°ËæÑÔÄÝ34ljb?JeºÙÞü1h²Å.Åb2@ä(àÁÉh#sB17¢ÙéÌ`°Äk\ZÆ`Ñ¦ÁqSa-vÅLñ\nf®¤kÐ Ä62I$5µV\r4´ÉõõG­ÿB4\r!Hå!~\'XÃJrpfÑêó30AÖ K ËBxCJ6ºN};ifPNc©Åé]UEEÙ¼9¢hêÜðùª.5ã7å[E;äÞàÜ¹J\\[£+´\'{8zòú#{!r¯Üo/r+1Ü{¼AÔ|Çq$ô°7zôQ¸;ì·ÙÏøQG;¯ÈRÃ <îb@qáBÂ§jBtÍy9¾±ÀYþ<D7ict=±,ç.Óó6~¦üÿ±þy\Zò¹O;±8µtGêÞîóeO¯àÆ:TËÇ«Eär¡Ô£ón÷hèv:oÐTUfVO(ÕYÍYJ©ÛRZS°Ë<Ì®ðkL¶ì[6¥åGG4SÙbÅ«ÍSÏ0ö÷×ÛÕÁÁ¡òaÙl)9màl^CdÃ\'hg63s1ÀìÚÌAm×M®r=Zâ§Zµ©{¶ëÇe¯÷ý8<êv÷µî4¬ºl§<X®ñ0ÍªYNïr}ÍSæ#ô{Dã~ûCØ) 8®¡J³Í^¾`ð?ëEÑwsÜÕg¯\ZkÅYÅÔÿ,8Dòs¥OÉÇÅ^NÃÚÖqóÅè³Ê2¶TÝv;ÚkbÍyÁF¾rcÛu;M%×Z0alì¡Ö»µéR.ú/:,rdnø&*²jÚQØ±ïÇÅ8wÈÖ*0K3¼<ÿv¤Ï%Jfùªióë·§è³¡w7ãF\'è·ìðRôT¥RÞÛöÈcS ðUõÞòU»gn=v}ôM-X7Z]T×¶´ÐÀn{{½e³Go[èñ·É?\"d\"¸ã\0A	2DCi©$µIEDª.êoxkc~¹9¦S,, ÁfãÞºÖý6jx÷ÕÕhóÔ1frÌî C7&¶BÛr±2õ\'À}hèÎ©SçÍýS´Ìd¶s[Ùý£ê·kâÀXÃ´¦ÉTr¦¥ÊùñiöTeíóêÜúûá_äh(qà.äp¡!YKt,', NULL, NULL, '0', 'Azampur', '1', 35.99, NULL, NULL, 'RbrF1594625485', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '78701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:31:25', '2020-07-13 05:31:25', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(30, 29, 'BZh91AY&SYÜ4\0Lß@@Pÿø;Kýn¿ïÿú`¿\0\0Ðª\0\0\0L`Âa4À\0dÓ@ÃDÉé	@õ@\0\0d@\0a0		Ó\0\0	MÂ`\0&	¦\0\0&9À&\0L&L\0\0&M4$DBb1\ZbÐPl§4ôÌ©²=RA\"¼>çÌõ\'È:Áû\rIä10üãèIö¡öûAsFKZ\">YÌ}FHIÃÕªêjKè§O&FEêjµ\"¾5Òr«Êóû³;Ñ­(pÁmZÊ`ÀWaßë5cèÔEÕðyë\'©j$Ü4y\"	4.ÐHÔ}C3´ædt<ÐÂð8ðÚ{ÉímFd¡KB^ö­h(¿¡¾00hYHap#A¥ ÀL-2ÎMXh46´.\\¨	D\"æ5;\n\0ØÄÜ^ô¼QpiPmc@Ø5ZÍmY(\rÂLÂaï,³(^I) @HVØMm3J²áA0%\"µi|AxB»BfX¾V\n+´fÃÜXXÉ5³ëSö50AEJ3TÁbCBHETj^\nÚ`¢ÁM\nNTÊ#%JQ\rE3&GèdîÀ)&ÂE@PU0T£©*êÖ¡ª­x¤/%÷MlCb&!ÿ­@Û!41©PQ6uHPg`27(i\r&Æ!M1Ä4iiÈ¶ØÎtQ!Ä(Íò\nçAo$W¨Úå¤æV\ZébÒÐZð°ËD`Ï93¬uç)ØY{\nÒUQk	(±HbZ¤ÂÀPÄ&0ÂywÓÈÞ~F\n±òH^Çû¡ÈÔ{îô=FfyÌýOH_à=fàøàÙð¿©ÉHC´+\rKÄüLø\nÏàÖ{OÈÁ­±UÎãô3>fOø$*Här_J):CESbò38°±Öµ\r~92 ÚÆ|6äU#hË¦¼õJÊÈwY0hmoiBØÀV§#CamKaLÁ¨fm<È[¾cS¿oáKåb3X40R-,Xu0²?ø37<D©5\n¤ÈàÎu;É.¸³eM§y¥tGVTÕÓ:ÂU´àxþ Ü×³>¶@Î¾Ê(;@¦\ræCÔ)Ãò ëFÖ½$SóBØwdu¡\"\0cê3Ü>þø$ìR¤ê2 îÐ$F³Ëq°ý:z¹A¹®Ðófà)×Øs â@ÂÑ\'p¯RiPÁTp¢Ñr+ÂÃ8Yª c(P0R(ÆÒaRT»Å#f³7­ÝÓ©#49I^©`çø$;V,4Ï1á¬ýÛ£·aÈ¨:\nRñ#ÚrÈí\Zcc.0öÎ Q­AÖ6ïÈæ·úôÛgsÐð «4z!tÄ%¡À\rñgf=íëpÏI¬õ¥ñ14oØÈ@Ó0Lh bl66Ò`Ð\nÍEµAÊQ]év\nËk@AFEHË¢Ã\'Vò;òsCHÌ×@XÌÎxEJX­Qb¡thÑëÍF\Z÷vÏYÜ¸*\"0¸¢8¿ÀøôÚ$ÇAK,ÍÉ°¼Ïýî½Eêz»Ð¬|DÒ½>¤&Á±Cú¿ø»)Âà	¤`', NULL, NULL, '0', 'Azampur', '1', 24.99, NULL, NULL, 'WiVP1594625684', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Afghanistan', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:34:44', '2020-07-13 05:34:44', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(31, 29, 'BZh91AY&SYÈÒ¿\0Lß@@Pÿø;Kýn¿ïÿú`¿\0\0\0\0\0\0Ì&0a0`\02i cL`Âa4À\0dÓ@Ç0À	Âi\0É¦a0		Ó\0\0	MÂ`\0&	¦\0\0&\"Ò$ÚhL£ð(<SÊ1¦eMê	pû3Òw ê/ð5\'ÄÃðTLaÿ¡\'ÚÛíÌ-lúg2Aõ!&ódB«ÙaIrè§_Fêlµ\"¾6Òt«Òóü37£ZÐãÛ6Á®ã»Òm.ÇÍ°«àñ+ÔOB8ØI	#´iñ\ZD h^PHØ}C\'ì49 ¡ô0½GÜÏq#=\rèÉ\0¨RåP¹«Z\nbt`a)\0Ð²\r!ÂF£A	Zé5ua¨Ð¡µ©pàØ5@¸èJ!ï4¬¬¹` \rMÅïKÀUØ&4\rU¬ÖÕÐ\\$ÈL1½åeÉ%5	\nÐ	­¦iV\\(0 d	H£E­Z_! h ÐWhLÓ.úX%X=5Ð¨BÆ­-Í\n¿ÜÚPÁ\Z(@ÌAS\rI!PNkRðVÓ^\nn¸Rt¦Ä\Z*PL¼áÚ·\nª(T½¨­\niIkZö¨¯ 0¤ª` ©GRUÕ­CEU[Dw¤/÷MlCb&!ý·@44Ô¨(:	$ Î°e\ZnPÒ\ZMCc hÒÓo1¹Ûi)\n¢BØfØ+ÂKo|-bÓ{tÐ­Õâµ-:^u¨àÈµ´¼,2Ñ33X,ëÊäÁ,½ÍÊëy/ÒéÉÍæË\\â1QÜdØ^ñ\Zðí=Î~F\n±òH^·üó°÷]êzÄ3õ<áõ\rÜây·|wÂþ%!Ð¬5.\ZR_ñ3Ä@P.&ÓÚ:w>F6o*¨7vó\'ÌÏÓô?¢O\"9çRNÔÕTÜ¼ n,u-_Àì(9-´gÃ~R:XÆ\\Øo6çh4 l©Öu¡£×;HÞÆµ9\ZlXªf\r03yâBÜä%ö\rO?Isñ¥ô±AX50R-,Xu0´?£ù2s*xRlIÁÚBZî$ºéfêÎãZòWVTÙä¡*Úñ;ÿPpkQ&·@Î®º(< S9 Æt\nFqüæ:½¯9\'ÔüÆÐõºHcHúIíwtu¡Iä:\r;u	%Q¬éÀÅBØE~ä<È]H3\"õÞ,à:ºÎÂ;Ez¸k\\\n\n¤Dû«^6ÒYeT©\Z@Â©F4·[\n¥Ü)+\'ÔJáÖ-z2)2Õ,CÊ±a¦xÐögÛÂ<»EDAÌTéJC¼aÈc#Ê4ÆÆ\\aì1F¶PØ3»C±põ8ôßg©Ð;5$¼(*ÍdÄÈ\\ÆJÒÔâA<A¯nËeõ8gÚzRø7ëd\0Á i&416@i0hMNsamrW.±Yr-cq\"ábÐ©¹sAìäà*ÞY%\\ÆÒ2m ,dÌñ0°Z9\"ÅBèÕ£Òl65íë1§jé	TDat¢8¿Ðú|\0Â7yiÄÔRÃ¥rn$¯ÔÓm×¬h½OGry×ÐØ6(`ÿRWÿrE8PÈÒ¿', NULL, NULL, '0', 'Azampur', '1', 24.99, NULL, NULL, 'aIZC1594625719', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Albania', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:35:19', '2020-07-13 05:35:19', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(32, NULL, 'BZh91AY&SYÐÊ=\0ßP\0Xø+þ¿ÿÿú`=lT¦MH#À	\0L\0À\0H	\"Ð\0¤õ\0`\0&\00\0\0\0¨4FA (h4\0Ð 49L\0\0À\0L\0 cJzSÔõM¨Ð\Z41©µ556ÇñÐéí\Z+ãújcðð£E2Zý#ó:ápôÌ?Þ![\"ßs¥=¼Y«;²â­÷Õ)å¾j`?Òc°:97.-·ªÉ¡~ds3Èî[ªËð\'>$q gp$3a´ê0eRdÐA,ÞB!/¼&\nøAsñ7	\nFÆÁh\Z²ÚærKxÐ[jàc	CC*.«2Wøha\064£{­ïÆHÐÓÆ°jnªó%#¼r2SH\\³EÉ2RMaWULº(4DÕ])bL«Rjnâîð¸AE3DY£	 PZºX¬ËÜ§½Nê¨/+É4qrìLÝqêö©B­å&¢fAÝæ²ìhÖÁÊ½S²VÖeV¯È/Õ!x/¥!6¶¢ØZæÉ3.¬eaªcIj±N+-Ò-XJ«	gp£$\rÀ6ÐÁíÜ=$,D%Ù ÷g«zÝãÖ~¼èÆejÎ«¢ðí×°õvfÊæë¶Û ÍA¶dFÎº«ÀúÑ3AL¦uWhÂn9lg]Î¤ÍV.vy±Øc.cõÈÔÞt=d=¢JOHñ0<nxlÙ? ü{|ç¾ÁÏF¼î¨pD8p­|{t2u÷ü\n~æã78\\ ø4æ{OÈ¿oqæJÞ¤MW¶¥°u7P9SUÃãÔl+2øí¼¨6edfl4TäeyF(\rëï®9l¾/&3å¸ÌÈÄgÐa@Ôjxq\"Ñn=Ö®¤¸*&©*4ÝwK+Ïñ38þÂ÷mJHàÊs!,}D]8A»pàesUÖ6ñ®a8eÌÞPtk¦:îíMyn3©yØ)#5Y° Î}âÎ­x\nÔØFÝ÷Z*¤ÈuÍ%mißÝ:ÎòòÙ¨Ö8Z¶Ezp	Íû;£c\rç2û·£7N§1\";\nÔ§@S]´*5ÞSDö^Ñ>I,×A$°HÂSE3dyL²ý$£Î2ÉHª8Ky yQÔ\\;X¡Ë µD¯;yÞ¯¸÷IRb{²òáú\ZãÐ¯Ty¶³rÊ¶7z\ZÆ8N5¥¤Ý8lç\no´¢[Hí¡AF];!¦BÔØì±Æc}ÇÃãÛ½Õ«{ÕÊ9©QVùë$R)U%X¢ÔÉ&T1&Àq×.J]éj*­Kíca$àEJÄ%tÝÀõtpo©¦3bûÓPdÆ&3Î*P²¼,²DT \\ZûÀú4³½±¯.¥øì;.@õJT`µDr\'è>|H-áä`)reì$¡´¡%5Î9[vÄþääñD´µ2\'ü]ÉáBCC(ô|', NULL, NULL, '10', 'Azampur', '1', 535, NULL, NULL, 'KrTu1606377113', 'Pending', 'wazuxuviro@mailinator.com', 'Keefe Bradley', 'Sri Lanka', '+1 (427) 444-9986', 'Officiis placeat ab', 'Distinctio Deleniti', '12647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Labore ea consequatu', NULL, NULL, 'pending', '2020-11-26 13:51:53', '2020-11-26 13:51:53', NULL, NULL, '$', 1, 10, 0, 0, 0, NULL, 0, 0),
(33, NULL, 'BZh91AY&SYYX\0\0V_@\0Xø+ö¿ÿÿú`÷A§¤´âÝ\02s\0\0\0\0\0\0\0\04	% \0dÐ\0\0\0\0%é4R\Z\0\0\0\0\0\0	\nJ\Z\0\r\r£@\r\r\00\0\0\0\0\0\0\0D@¦4Ó TßªSi¨ô£#Ój(	LÆô\\mî(/±_>z(Tdµî1¢vtqdk0ÿxV%!±.=Ì!HÈÄ\"úÄ³cTäèª£õ0m5+mYÊHÆÍ\ræp\rzïÚzLñTÂÝÌ·$!@HfÂàw\ZÄqÔê`-Lº`ÉK°d{ äÒM	_XZÍ$¼d»¨v6h ö?\rL`Pb*1\\[\Z®YÉ£³0Xi@u$C\n%iKVú	s\"Ã¥Å1¬£TL\ZÁ©R¶·bûÂÛ	#QqãTCH](iR\n¬ù¨¢vÂid ìQB ÈH	ÄzÄÚðm/¤Õox½kbF3q(@ËARå\r¤²,+µ+u6,Ô¦ZËÄ¤´lDåÖbá¯dÎÔ­ji´\r7VW*QÔukPÉUWîH_Uû%(I±hÚÃÂ(RGt#Cb¹B±aÅ¦è&0&i4	¤4¢`D1&ÄÄ!(Þe¶7¦ä@ü«<i& ;E´­Cy°{ÓvÃ@äE@ðæÅRwR²¨\n¬c`\Z]eU1T\"í+)!ªN¯âv7È:? Ãìr3;\\¼fdPÿm?óÂ\ZmÂéAÄë\"eF£¥kæCGô×hxôê ¡¡KÃPË¬t²¹<;ÒVå$s9®µ(¤î\r©¯û39»ä`V£·(ÞVj3éÃD¹°8/°ài:ó!%6<ð²3Û	´w´ ÆBùd¹ÐÚo-·c|LÈÔfC\0ô~cS»ÄÅûùÓ#ffæ`¢¼0ÀÛUüòY\'ý4;Í}ÁÂf¡283¤%ÏbK®|`Ý¸p2Î»g­®mî®¡9lð7¸×)ýxµáÐÓ·Ðä>´7X¹æÌÌ:-¬$g?ä.ÆªÍAð{\r¤ùx7¡Jp<ÊzúA\'1qê`Ó`I*g¹aõ$ï	ÜÿÔlaÄñ1nðÎ S§SÈDb½U.\ZÙPÆÐvIM	Ø¼	ï¨ÎEj¥H$Ä(J)cEAÜõ(JAÀ²3L8æQwÐ]ÒÍPÈ/Q,_b|Ö,4ÌA¬¿¶øòÚs*\"\rOÅ¹AýcÔm,IÈËK\n\r¡â6\rgUÄwÐâwS}å)Bë°ð¨H6Zì1¦EcJóh6$,bÃÉ¿]F7âb`ßK!\ZDLÀÒ Ð\r¡(i0`\r&ó3¦ÎPðKÄU^&/sBFQBBðÖÃ§«x<M¹MÌa4Íª2Æfsß(a`.¶*P,Úì/»A®\Zús1UÈ=	Jªª2^DýÇáÜ@#33 {0ÔhI#X¢ø\Z)ØõùP¿AëëìØ6(`âÿrE8PYX\0', NULL, NULL, '0', 'Azampur', '1', 900, NULL, NULL, '46JT1606385510', 'Pending', 'fexoku@mailinator.com', 'Chloe Holmes', 'Samoa', '+1 (749) 106-7579', 'Quis dolore ut eos', 'Ut laborum Deleniti', '15900', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Illum minima asperi', NULL, NULL, 'pending', '2020-11-26 16:11:50', '2020-11-26 20:45:49', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(34, 103, 'BZh91AY&SY(Þ2\0þ_@\0Pø+ô¿ÿÿúPò#lÖÚh;ª[MCMM3\"d`C\0&iÔÂQ4ÈÔ¡´Ôh\0\0\0\0Ð¨ÔM1=M)¦@Æ@\0@\0\0\0\0HhÐSS56¦ÐTò$ýPô¤\"@HØy\0\"ýZÀc\\:FH|ÄØññ£b-xGî<u$$$ßù}\"²-ôt§s#sW#émÖ©Nö÷ÔÀ{ß	àÆ&ì7Ð¡²VªJbPº»È-¡Ý®df ñ&PcÊjv9^BhK°×|L%Ï{I\nhbCZÔ-kRZxÏ8ô4»Tm\ZªÍ¾²°CÒÍjµ¬m bÁ1 l\Zª¼Ï²h!¤\"ò¼Ü(ÂÆp2X6µUm\"±-WWb$Ë¹%DÝÅÝÁ=D}¥@ÍAf$9¢#£ð¥ËEî\Z­u{Ö ÷pP¬ÖX±).â)zÊÙîJ%oEJ30+¼ZK@ar«pÊvJÙfQ¥jüx$/ØRI6±\r «PTC!¤Ú&Æ@Ã¹¦éIÀcMi\r hAaPÁ\r y½¹fä&Ðo\n)ìõ(´ÅañB\ZÎMe Ã%Èi³sÁI(ç ÍA³0{q\'f@øY4S%qd×8hß#}3gf«ü½£V;ÔD²óêy\Zu&gò\n|G¡\"Û\\Þ?w>³¦Å¾oÃ^TD4Óy°C¶Ya\0ÉÂÆñ.¯è)é ¦l\Za(è>Gr0ý{ä*DTÑzÌ±Hà\r_*Xh¸{üÄ£¡p3]ìÀê3F¹:\' 4LÑ¦Q``Á¦ÑK ¶ûÂs8\rÅ»i% µx10%ÿù\rKd]KhÙ³ÌNH+En%·~LQM#\Zf\")å°¯ÐgðXQi¾\rØ,3Û-m¸¯	î	_¿A\Z5îc{»%Àå­m.:¬$3am^gøy>A´m$07¡J\0cà&t,íÖ	\Z9vÄ$IXÖÝ;`Î§ÁaÄ#âÔú1,gsÔùeû	ÀL¦cÆ¡jè\nk\n\nÒQ$ð¼Äý>s1rp°²	(©ÞYJ ¦|ÄR¡%è)¼Èó$±æ/ÒX!É]1*¾ÄºªZ4ÊÆ¯ïS)s©Ìâ\\\"FG±n÷4#\ZcfÄØÙSmÉj¡¸\Zx¯¡±ÜLu7;WvÒG±ôt Ô()=a¦B¨Ò¡âAã]í«ÕÈ?`cÆý1cAI±\0Ø%\r&I±205ÇÆ?¹\nkK®6(ID.5\0×t<·ÊâCHàê c 0àâ{El¶\r.GäE\ZÒS¶5¯	,ÖÌjQQPPNIPèÞ@!\\°àË\\f%¯)jø\Z\'ø|¾À§N¨Mbþ.äp¡!.Q¼d', NULL, NULL, '0', 'Azampur', '1', 765, NULL, NULL, 'JkEc1606485573', 'Pending', 'rumowibepo@mailinator.com', 'Solomon Browning', 'Serbia', '+1 (973) 951-6554', 'Alias ratione volupt', 'Aut sunt voluptas cu', '78985', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ea aperiam mollit ve', NULL, NULL, 'pending', '2020-11-27 19:59:33', '2020-11-27 19:59:33', NULL, NULL, '$', 1, 0, 15, 0, 0, NULL, 0, 0),
(35, 103, 'BZh91AY&SYh¾ê@\0ß@\0Xø+ü¿ÿÿúPà ìmK P.!)	¤i¦A£Ò\0\0\0 ¦A(\Z\0Ä4i \0\04OJ©<IôÐhdz`\0\0\0\0\0\0\0A	=3M!hÈÐ\0ÈÓ&J	ìë9:À¸£ëbomÌÌú	pÞÅ\ZD°XÌîµ<*¦«>íý²41´YÏUR,ôÄÀ|©\0éÉÞ»2n`Âê©z$âgÜ\\³ë³3¡;öÚ gp$3s¨Ø%/\"®&Ð^SxÁäì{ÂC  Ð4²ÔÛ2eÝXb40¢bª¯yõâ77Y\n2gÅñcCñÁª«ºjø½d÷q£ ÕHc6£,¡6Â¢mUV\n$ TÑ5UfXM`i«I I	ÚÑkZä@Aì(¢`.HjIÈ42îÖpv;b¨-\nÒ´E¥¬&f´x¶BÊ*Íé5&fwCØT+`PvG°Hrjã%R\"eDÑá\'$ãy-!H°ÀS²¥ªmAL¥%àf¶h*ª## À 0½¤B\nà3ãä>Å!h	 ý3{bö!ÇÊp]¬XÒ´/AobÎEIÚ¬OCa\"b\'*»ÄvëfL×B+k+]\'Zc6ØlÄRpÁbUqh(¬Ä(¡ SÛà1Hs¬DOåÌXÌëaÎ.|4ê:Ü+ÉL«9$%\nä\"8LXÌ{\r°døær4wþ{GÅ\nÊe\'Ñgq¤vÍFÁpñÞN1#FpäÒÑå\'0î8Ç*Ð%ÏBØañô:HÎ?-îXK½ÄÁ\'	à\r%9£AádÑ¡¦ÑÉ¥_9Üæln_`#$Æð]r765óPCóOgiy»±m$p°ÕC\r7[¦OÜøæ6-+êÎ±285¢Ôg&>¸8êq¡ëmçÂø:»-¸Nw\"_aä¼ZK1\n,&Ê^|¥ z`ó­ØHÎX·³^³èzÒ@zpqC ä\ZLôåãâë¬ò2Aã°I*×L[¡BNÀÐ¾jè6uÌßzÙÖwô(<cDzS)­°*0Y%N«¸Û²Ê¢$Q*`¨¦nG¤²e	%BÀ²²q >D­ûÅË±%ª²x 8É\nDIK\\kuBÐÑÃeCÙ)[F\"Ã°Ó¨Õ4$°lù¦±ËI¬å¾¼­\n:\rÚJ2©EÓÕLJyFNñÌQG-Q³Wn9v&ð>@¾© É(H É%)0bM¨h©á·r[­.²èg82(ÀÏ7fô:ë83Ò67P155è±FVCaóE£V½@|Ú[å±¯oC:ð<ëPJMR®hÒ~cîì ¹Üh)v³,$£DfËè0¾ónØOè@éèéPRR\rOø»)ÂE÷R\0', NULL, NULL, '10', 'Azampur', '1', 259, NULL, NULL, 'koMh1606485712', 'Pending', 'duqorawaho@mailinator.com', 'Alisa Reed', 'Bangladesh', '+1 (817) 676-2678', 'Sit est consectetur', 'Ullamco deserunt vol', '76308', 'Maya Boyd', 'Sierra Leone', NULL, '+1 (482) 864-1333', 'Officiis velit qui v', 'Officia et rerum ten', '89773', 'Officia quis et modi', NULL, NULL, 'pending', '2020-11-27 20:01:52', '2020-11-27 20:01:52', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(36, 103, 'BZh91AY&SY(Þ2\0þ_@\0Pø+ô¿ÿÿúPò#lÖÚh;ª[MCMM3\"d`C\0&iÔÂQ4ÈÔ¡´Ôh\0\0\0\0Ð¨ÔM1=M)¦@Æ@\0@\0\0\0\0HhÐSS56¦ÐTò$ýPô¤\"@HØy\0\"ýZÀc\\:FH|ÄØññ£b-xGî<u$$$ßù}\"²-ôt§s#sW#émÖ©Nö÷ÔÀ{ß	àÆ&ì7Ð¡²VªJbPº»È-¡Ý®df ñ&PcÊjv9^BhK°×|L%Ï{I\nhbCZÔ-kRZxÏ8ô4»Tm\ZªÍ¾²°CÒÍjµ¬m bÁ1 l\Zª¼Ï²h!¤\"ò¼Ü(ÂÆp2X6µUm\"±-WWb$Ë¹%DÝÅÝÁ=D}¥@ÍAf$9¢#£ð¥ËEî\Z­u{Ö ÷pP¬ÖX±).â)zÊÙîJ%oEJ30+¼ZK@ar«pÊvJÙfQ¥jüx$/ØRI6±\r «PTC!¤Ú&Æ@Ã¹¦éIÀcMi\r hAaPÁ\r y½¹fä&Ðo\n)ìõ(´ÅañB\ZÎMe Ã%Èi³sÁI(ç ÍA³0{q\'f@øY4S%qd×8hß#}3gf«ü½£V;ÔD²óêy\Zu&gò\n|G¡\"Û\\Þ?w>³¦Å¾oÃ^TD4Óy°C¶Ya\0ÉÂÆñ.¯è)é ¦l\Za(è>Gr0ý{ä*DTÑzÌ±Hà\r_*Xh¸{üÄ£¡p3]ìÀê3F¹:\' 4LÑ¦Q``Á¦ÑK ¶ûÂs8\rÅ»i% µx10%ÿù\rKd]KhÙ³ÌNH+En%·~LQM#\Zf\")å°¯ÐgðXQi¾\rØ,3Û-m¸¯	î	_¿A\Z5îc{»%Àå­m.:¬$3am^gøy>A´m$07¡J\0cà&t,íÖ	\Z9vÄ$IXÖÝ;`Î§ÁaÄ#âÔú1,gsÔùeû	ÀL¦cÆ¡jè\nk\n\nÒQ$ð¼Äý>s1rp°²	(©ÞYJ ¦|ÄR¡%è)¼Èó$±æ/ÒX!É]1*¾ÄºªZ4ÊÆ¯ïS)s©Ìâ\\\"FG±n÷4#\ZcfÄØÙSmÉj¡¸\Zx¯¡±ÜLu7;WvÒG±ôt Ô()=a¦B¨Ò¡âAã]í«ÕÈ?`cÆý1cAI±\0Ø%\r&I±205ÇÆ?¹\nkK®6(ID.5\0×t<·ÊâCHàê c 0àâ{El¶\r.GäE\ZÒS¶5¯	,ÖÌjQQPPNIPèÞ@!\\°àË\\f%¯)jø\Z\'ø|¾À§N¨Mbþ.äp¡!.Q¼d', NULL, NULL, '0', 'Azampur', '1', 750, NULL, NULL, 'DDOT1606486697', 'Pending', 'mahmedsaleem60@gmail.com', 'Ahmed', 'Saudi Arabia', '+1 (355) 694-9416', 'Ahmed', 'Ahmed', '11111111111', 'Amanda Best', 'Kuwait', NULL, '+1 (983) 774-9926', 'Enim nulla ipsa pra', 'Veniam eveniet est', '26463', 'Distinctio Ut autem', NULL, NULL, 'pending', '2020-11-27 20:18:17', '2020-11-27 20:18:17', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(37, NULL, 'BZh91AY&SYÐÊ=\0ßP\0Xø+þ¿ÿÿú`=lT¦MH#À	\0L\0À\0H	\"Ð\0¤õ\0`\0&\00\0\0\0¨4FA (h4\0Ð 49L\0\0À\0L\0 cJzSÔõM¨Ð\Z41©µ556ÇñÐéí\Z+ãújcðð£E2Zý#ó:ápôÌ?Þ![\"ßs¥=¼Y«;²â­÷Õ)å¾j`?Òc°:97.-·ªÉ¡~ds3Èî[ªËð\'>$q gp$3a´ê0eRdÐA,ÞB!/¼&\nøAsñ7	\nFÆÁh\Z²ÚærKxÐ[jàc	CC*.«2Wøha\064£{­ïÆHÐÓÆ°jnªó%#¼r2SH\\³EÉ2RMaWULº(4DÕ])bL«Rjnâîð¸AE3DY£	 PZºX¬ËÜ§½Nê¨/+É4qrìLÝqêö©B­å&¢fAÝæ²ìhÖÁÊ½S²VÖeV¯È/Õ!x/¥!6¶¢ØZæÉ3.¬eaªcIj±N+-Ò-XJ«	gp£$\rÀ6ÐÁíÜ=$,D%Ù ÷g«zÝãÖ~¼èÆejÎ«¢ðí×°õvfÊæë¶Û ÍA¶dFÎº«ÀúÑ3AL¦uWhÂn9lg]Î¤ÍV.vy±Øc.cõÈÔÞt=d=¢JOHñ0<nxlÙ? ü{|ç¾ÁÏF¼î¨pD8p­|{t2u÷ü\n~æã78\\ ø4æ{OÈ¿oqæJÞ¤MW¶¥°u7P9SUÃãÔl+2øí¼¨6edfl4TäeyF(\rëï®9l¾/&3å¸ÌÈÄgÐa@Ôjxq\"Ñn=Ö®¤¸*&©*4ÝwK+Ïñ38þÂ÷mJHàÊs!,}D]8A»pàesUÖ6ñ®a8eÌÞPtk¦:îíMyn3©yØ)#5Y° Î}âÎ­x\nÔØFÝ÷Z*¤ÈuÍ%mißÝ:ÎòòÙ¨Ö8Z¶Ezp	Íû;£c\rç2û·£7N§1\";\nÔ§@S]´*5ÞSDö^Ñ>I,×A$°HÂSE3dyL²ý$£Î2ÉHª8Ky yQÔ\\;X¡Ë µD¯;yÞ¯¸÷IRb{²òáú\ZãÐ¯Ty¶³rÊ¶7z\ZÆ8N5¥¤Ý8lç\no´¢[Hí¡AF];!¦BÔØì±Æc}ÇÃãÛ½Õ«{ÕÊ9©QVùë$R)U%X¢ÔÉ&T1&Àq×.J]éj*­Kíca$àEJÄ%tÝÀõtpo©¦3bûÓPdÆ&3Î*P²¼,²DT \\ZûÀú4³½±¯.¥øì;.@õJT`µDr\'è>|H-áä`)reì$¡´¡%5Î9[vÄþääñD´µ2\'ü]ÉáBCC(ô|', NULL, NULL, '0', 'Kamarpara', '1', 525, NULL, NULL, 'z4Wu1606488101', 'Pending', 'test@gmail.com', 'John Smith', 'Australia', '123456789012', 'Taxes', 'Queens land', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-27 20:41:41', '2020-11-27 20:41:41', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(38, NULL, 'BZh91AY&SY(Þ2\0þ_@\0Pø+ô¿ÿÿúPò#lÖÚh;ª[MCMM3\"d`C\0&iÔÂQ4ÈÔ¡´Ôh\0\0\0\0Ð¨ÔM1=M)¦@Æ@\0@\0\0\0\0HhÐSS56¦ÐTò$ýPô¤\"@HØy\0\"ýZÀc\\:FH|ÄØññ£b-xGî<u$$$ßù}\"²-ôt§s#sW#émÖ©Nö÷ÔÀ{ß	àÆ&ì7Ð¡²VªJbPº»È-¡Ý®df ñ&PcÊjv9^BhK°×|L%Ï{I\nhbCZÔ-kRZxÏ8ô4»Tm\ZªÍ¾²°CÒÍjµ¬m bÁ1 l\Zª¼Ï²h!¤\"ò¼Ü(ÂÆp2X6µUm\"±-WWb$Ë¹%DÝÅÝÁ=D}¥@ÍAf$9¢#£ð¥ËEî\Z­u{Ö ÷pP¬ÖX±).â)zÊÙîJ%oEJ30+¼ZK@ar«pÊvJÙfQ¥jüx$/ØRI6±\r «PTC!¤Ú&Æ@Ã¹¦éIÀcMi\r hAaPÁ\r y½¹fä&Ðo\n)ìõ(´ÅañB\ZÎMe Ã%Èi³sÁI(ç ÍA³0{q\'f@øY4S%qd×8hß#}3gf«ü½£V;ÔD²óêy\Zu&gò\n|G¡\"Û\\Þ?w>³¦Å¾oÃ^TD4Óy°C¶Ya\0ÉÂÆñ.¯è)é ¦l\Za(è>Gr0ý{ä*DTÑzÌ±Hà\r_*Xh¸{üÄ£¡p3]ìÀê3F¹:\' 4LÑ¦Q``Á¦ÑK ¶ûÂs8\rÅ»i% µx10%ÿù\rKd]KhÙ³ÌNH+En%·~LQM#\Zf\")å°¯ÐgðXQi¾\rØ,3Û-m¸¯	î	_¿A\Z5îc{»%Àå­m.:¬$3am^gøy>A´m$07¡J\0cà&t,íÖ	\Z9vÄ$IXÖÝ;`Î§ÁaÄ#âÔú1,gsÔùeû	ÀL¦cÆ¡jè\nk\n\nÒQ$ð¼Äý>s1rp°²	(©ÞYJ ¦|ÄR¡%è)¼Èó$±æ/ÒX!É]1*¾ÄºªZ4ÊÆ¯ïS)s©Ìâ\\\"FG±n÷4#\ZcfÄØÙSmÉj¡¸\Zx¯¡±ÜLu7;WvÒG±ôt Ô()=a¦B¨Ò¡âAã]í«ÕÈ?`cÆý1cAI±\0Ø%\r&I±205ÇÆ?¹\nkK®6(ID.5\0×t<·ÊâCHàê c 0àâ{El¶\r.GäE\ZÒS¶5¯	,ÖÌjQQPPNIPèÞ@!\\°àË\\f%¯)jø\Z\'ø|¾À§N¨Mbþ.äp¡!.Q¼d', NULL, NULL, '0', 'Azampur', '1', 750, NULL, NULL, 'IZb91606490653', 'Pending', 'hassanasrf97@gmail.com', 'Ubanto', 'Pakistan', '123456789012', 'test', 'test', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-27 21:24:13', '2020-11-27 21:24:13', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(39, 103, 'BZh91AY&SYª8+1\0ß@\0Xø+þ¿ÿÿú`;îX\0\n9`\0&\0\0\0\0\rO@M%#\0\0\0\0\0\0=%@Ð\0\0\0Ð\r\0\0RMLCC@\Z\0\0&\0`\0\0\0\0!\ZLSõ	ªx§©±04Ñ¦Õ$$}±ª\0âÈx¢L¶$¹-,2ZüÆ#ò$Ad~ÓøUaæ²åêBP3!Ñì&ÿæk-9©TÜâÄ;£²îÁ©e¿¬p1¸¡fýbãVc\"uæG0BwC2576*<#Ì,ÄÐ8UIY<ä@æ	{Ð	f1dbEph\ZCY5¦uGf¨ÄtCC\nR.õ,%4{FØÒ¾1L]Y¸Æ°j­-{J\\¨M Í]â$¤¢ *]Ûw`ÁZ6n¤(VH%9³¦V%7Q.«c0\rÅ\n2äT¹bCBHA£²×ÊÉyféÀ 55(V§qb.ÔeîTP©Q52µµí\\d+*¹(êJÂµ¨]UW¸¤/Õ|ÒÚI´ Q\n`)¥ê(¦ÉE¸¢Pô´4ÔDYdBÁ\r¤ø=üG»BÈBPÐÿc¬Ú\\!LÓ² iqMVg§pÕvÊAÖÊDÝDq(Ä è$*°CÙ$		NAÀR1BnÓfÈÝ.àÈ ª þ~¿qàr=e*|D£öÚ¼.`xþz|½/eüÝ(8C´B±ñ?iÄ@Pa>Æaò é(ÀÒ4bÙRzcØ:æÿßø}I\\±²øÔ¢ t7§\ZP5*l³}<	Fâ°3A~üêÀÔ¹}\rMÓ¸î°dÁ¦ÑÁ¥À-¬t45-»cX,¸û(ö\Zîd^/Ï¾õÊ\nRTi»Ý®°GÐÔæhWô	Bdpg;ÈK1D]yAÃ3àewÎÖ¹¿ººå§Có\Z«]ýø¸.âN§oaµÎ2<BÁ²àÂFvò\nµì=1´lxÜà1(AÄ04âS×å¡rë0Aå I*gëtPg¡\'pN¨_3½zÁ#©qFr½Ec¨Æ\"½Ü\n5¥ÅBûÁÙ%4\'EÐúªÉP¡Â/#\n­#wPQ ÏÑw`JÂ¹,KÈàR6wzK49d¨©>K\Zf¸k/ñÞv*\"\rOB¾¤FÄx\r1´XøiP(Å¸;\rAå²ä;èr;©Êò=í#m/\n\n2Óº\Zd/ÈÌidwAÎ Á¶þMý-]äÖ9ÀïHEéJA\"H\0I)@´r4<7uP÷%°ª¶1{È@A	E\nÌ%vãáÙÀU¼sÂi\Z\Z¨ÈÏX©Cu û\"¥ÈÍ¯òÙ¥®\Zûö1ãÅu\nÈ¢Pª£%ÙÐ°úó ¹ÈÈR0èÌ0òb«Ðh§¼ððBúØ@ÉAIH5?ÅÜN$*\nÌ@', NULL, NULL, '0', 'Azampur', '1', 130, NULL, NULL, 'gQIm1606490669', 'Pending', 'mahmedsaleem60@gmail.com', 'Ahmed', 'Saudi Arabia', '+13556949416', 'Ahmed', 'Ahmed', '11111111111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-27 21:24:29', '2020-11-27 21:24:29', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(40, 105, 'BZh91AY&SYiæÝ\0Tß@\0Xø+ö¿ÿÿú`¯Û7A@ä2s\0\0\0\0\0\0\0\04&\ZPA¡b\0&@	 MA@\0FOP\0R&zC#j\ZhÂ\0À\0\0\0\0\0\0\0	M\Zjbh\Z!¨õ$h2Ôò		³ Ü~9â\'ìc¡tþ®¬\Z4ÌìHá½;°XØoÚÅTÑ{JÜXu¢èßyEçÊSjáÃòÛ&\ZÆAàPÄÈ¹27²0ÀÌ¬^âw\nÒðÚó>EÜH4`\\*gÀ°Ä7äÀidP^}O°KZh®ÊR²EÒ6È¹Ì;âBPÒI\"cÀÄÁ5©©ßY;Y1ÇJÆIÁè`ÁOÆdKÎX42Vs|å ËÄ\"0k^úc¦¹ºËFPÖFá«ÒO&A0É6ùYÁo¢,íÅ,$ÅÓÆ/*NvPJL¯Z}UYuÇØG²(³Z0,\"¨f\"`a¦ða;õ¢Ú®d+T©Ô+4µT&3:k¬)N§8Éâ \Z«ä&ä¢ZV±4©WyvÒJâ$00ÀPøÔ¢õM¨¦\nKJ/Jï±o#@dFFd D1&Ä´!(Ì¿Ì5¤*¤>Ååg9_>	Ü2¼ÅV¤Â#Ú³\r@´[04DVdè(aÐDHC4hJJS0f­*©\"Ñ\\Xê\n 9H`}¿±ÄBgÝö7;\\]/,#ÈÖç&ÐÙÏ¯½Æ£¨M%(\\×1½ìl\nñ=þÃ=	cRj¸eO*þ¿CØÖ¤GÒeGpj0Ì¿¦ð5ù¨E\rC^8ÏM&%Å°¸»Q°ÂXò!%+N¨è¯+&Ñ¹¥m¯¼.¸æbf[ \"£8-L¼ÈeãÀï}F[½å+Z×É}FÅ7³%æ´Ef»­ê§#ü07\ZýAÂf°e	Tg\"åÇl\Zêk°u2ÒÛ{§Jý\\Ïìk|¿aáÌÃaWnÅÌÞ=,5Ú\\u*T9¬XHg»+ZÜñIäu¡Ä¡À¼i3©g|8l§BýA\"JÆ«Ô¥´DÆ}ZÐ¿Ðla´äRÝîÍ YÏC¡<])°%ìLðÂJ$ËºGr9-9>\\`9/,0hiR0OÀµ(^U0Ø@kbî%Ì]ÒIUHdLJOR]U-\ZeA«þyÇLN$ÄAîZ[Ä&Ôâ1\Z\r¥CBAæ_¢±r­MÑÝ´î³;¤)!i¨èV1m´Ä©G É¢\rQ©Å·ÓÇô^$æÈ\"HíF¤( ¡l@6¡¤Á6ÊõoOb\\5È¥×(EBá|Û¡ÈÆù\\R¤TÅ@Æ@ZT¬·DË\n*ËP÷¢ej*×a{4Q±¯N%+¢Þä×Fò^ÃáÜ@#`2¥á\rÌ£		´±a¼ÿ/Ênè8á?vïD\"jÅÜN$\Zy÷@', NULL, NULL, '0', 'Azampur', '1', 900, NULL, NULL, 'NXSX1606565097', 'Pending', 'mahmedsaleem60@gmail.com', 'Ahmed', 'Western Sahara', '3556949416', 'Ahmed', 'karachi', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-28 18:04:57', '2020-11-28 18:04:57', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(41, 105, 'BZh91AY&SY¹´=§\0Tß@\0Xø+ö¿ÿÿú`÷A ÓMY(À\0\0\0\0\0\0\0\r4SÒI  \0\0\0\0À\0\0\0\0\0\0\0	\nH dfè¨OD4aPÓhs\0\0\0\0\0\0\0\0$D	© &\Z\n~¤ÑÏH#ÓÔ(	\ZOpÿb@l¨ÏCWð×®¥Ê-{Gæ$Üd¦ñªÅs²[r(KÊÈ\'\rdµ9©²òn;©i¸­·2;Ùjp3\0×¯\r§ õÎ4\nßC~B2ÀÍK3yõ63Èò4 Î£L¹\0ÆH{¹ì¨%Á4BWÝtÒKÉÝ%ÝCë5	\0>£ØÁ¤\"£ÁkcUÓ2jìÊXi@eIÂ¦IZRÕ¾_À@°àiF1La« ¨ÕÆ°jf­­ì_[a$na7ÌjiÖ+­\rjAPÜ´\"\râÔ¥)z*\"JÒ¼ë&\0«f-|@I´¾TI«cÖäfò2P¡.IK$6BÐ°¬$Ô,Ö¹m%å§\0ÔÕdINâf)£½pTP©Q52µµíQ_\0Â©¥IWVµ\rUz®äó_ºRM6±\r l?QB;¡\ZÊM\r7DÀiÒhHiDÀbMBQÀÓlp#]é)Èb¢K¢è+#ÈÔ/}3â0«ÆÉ-×°ðg2fÖXnLÐ(Ê3.±!ª-LEîÜN\r(/¥ ÂBk$\rZLNæC9îmxfc¥	Ø=i9¯2<.sºPq:ÈQ¨ã#c¯fo¨`þJñ0ëóò*A·bª\rÝ¸É÷ùù­êHêu]ªQIÜ\rGSwêdïØ@áÓÈp+7ùqÁQ.¬%Ëì8Î }¤$¦Ç2<-íÚ9´ Æúh¹âm8ÛÁ1Â&hn ÅÄ=Ôïð\"ñ~}i¡É3&¬ÁExa¶ª+ùè²xôÔæl+î5	ÁÚBYý.ºò~Mãk¶{ZæÞêî	ÓgCü\rwÏæ3Á®&¼L»q^\'xûPßbçS&CÅma#:ôyU¨m:Ò@v÷c>i3Ì§¯¤u)9¦=6¨Öt÷1l\"£>dÂw¡~ÇEØlaÈð1n!ý3ñìyu ahÌWª¥À£[0*ÚÉ)¡;ByÔgyeR¤Lb%L1¢ Ãàõ(JAÀ´2q 7ÚQwâ.éIePÈ/Q,_B|Ö,4Ì!­>þå´êTDè±n¡PC¨Æ@vKbCî4ÖÂ£hx\rAç²ä;êr;©Âò²P»l ¼*\r»Ó\"Ði^q\\:s×yò³­°ccf8Ó00P`\r¡(i0`\r\'\'ÎøEx¥à*¯¹©#(¡B!tÝCÇ£«x<\rºMÌa4TdqRë`ûÑRde¯a|4°Ø×Ë©êv]áèJUUQ¢èï\'à};qdÐ aÍa#0..\Z¾hÑLM¦t/ø Ñ£¡	°lQÜ]ÉáBBæÐö', NULL, NULL, '10', 'Azampur', '1', 925, NULL, NULL, 'oo2V1606565362', 'Pending', 'lyxevysa@mailinator.com', 'Susan Alexander', 'Guinea', '+1 (489) 107-5834', 'Fugit velit debiti', 'Ut dignissimos qui e', '73503', 'Len Melton', 'Fiji', NULL, '+1 (918) 728-9327', 'Blanditiis laudantiu', 'Aperiam eum voluptat', '23261', 'Quidem omnis tempori', NULL, NULL, 'pending', '2020-11-28 18:09:22', '2020-11-28 18:09:22', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(42, 105, 'BZh91AY&SY·Í\0Tß@\0Xø+ö¿ÿÿú`÷A Ó-V(À\0\0\0\0\0\0\0\r4SÒI  \0\0\0\0À\0\0\0\0\0\0\0	4©ªx¤zh&¦PC@i¡Ì\0\0\0\0\0\0\0\0&¦4h(ª=FÉ\Z4ý(Èõ?JPÀHn:NÙü2<D~IIý¦ûþ®¬Ë*2Z÷Õ;:I¸É	47L?Þ!Uç!d¶>ä*P!N\ZÉjsSeäÝ22(vSÓq[ndtda©ÀÌ^¼6×8Ð*~:<÷¡<È;C5.,ÍçâPÚÏ#ÈÐ:2ä!ì\Zç² Ð\r	_t-ÓI/\'tu¡ àL`nW&	­WLÉ«³*RÉ ÉA3-×<cLõÁ\'¤Ð\n\ZcÆ\Z²\nQ0Lh¦iJÚÞÅø¶FáyÆ¨=bºÐÖ¤\rÉëB(ÐØ^-JR²±bÂ*Ñµ+È2\\\n1&^±6¼KåID¶1­nHÆo aB¹$.XÚIBÂ°XkPH³XZ`B*å´`SUE%;¦õÀUQB¦-DÔLÈ:Ö×µE|\nJ¦\nu%]ZÔ4UUê»Õ~ÉJI6\0Ø\nA`(|¬¦-r¥`¤ÊSD¢,\\HÁ@a0\"bCäp4Û×zBÊ@ôehèº\'JÈò!¥Å5$ßLÅ8\0jÁ±`²Ke µàÃ,<Ìµ¥V4\n2Ël`HjS{\"÷Jâih0 ÚÉëýcÔ±Sæû¿c¡Úåã&ùö×qæÔÞvþí9`ls¶&c+R¨MoAÚø8\nüW_¨Ï¡R\r¼#Pnï>fOÌÿÐø%oRGSªíRNàØj:¿Ñ þAöÁÙn}¼G°3pÏ§êÀâ\\¾Ã¬êäBJly##ÂÐÎØM£J	LaÈ/¦{&Óm¸ ²fá\\CÑùNÿ/çÖXc2`þ&\n+Ã\rµQ_ÏEÀÿ§3a_pp¨¨LÎÒÈÏÀë¯(7äÞ8Aæ»gµ®mî®à6wÜk¤þ£<\Zïñ5âeÛñ:µ\rö.y2d<VÖ3¯19Î	)Ør$ÔrjpÁK¼ã#<ÊzúA\'Qrê`Ó`I*gOsÂ*3êIÌ\'zû;×a±#ÀÅ¸Ý§cÈ©Db½U.\ZÙPÆÐvIM	Ø»ÉçQ,ª c0¡*`¤Q4©@²P*¡Ã¼?\"¸wJK(rAz`òûæ±a¦`ú\riùð-§R¢ Ü}Ëêö:da´°v$>fØT`m°h<òv\\}NGu8^Qò.Û/\nå²±,§dÔð§~ÓCg7|¼IÑ\"DÖ Ä\0D%&ÄhJ\ZL`IÀÉã³¤\"¼RðWÜÔP¡»÷Añïpo·I¹&j±3Î*PÂÀ]lDT YkØ_\rì65ôêc:@ô%*ª¨Ñw¢:ð>þâ\0aA4sfHkØ0Ç9·ØqÂu\r»yaÏø»)ÂÌý¾h', NULL, NULL, '10', 'Azampur', '1', 925, NULL, NULL, 'qVP01606571377', 'Pending', 'doxup@mailinator.com', 'Beau Keller', 'Botswana', '+1 (381) 674-3541', 'Aut pariatur Cum es', 'Dolores velit ea dic', '36244', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Voluptatem ipsum co', NULL, NULL, 'pending', '2020-11-28 19:49:37', '2020-11-28 19:49:37', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(43, 105, 'BZh91AY&SYÉÍ·\0Tß@\0Xø+ö¿ÿÿú`÷A Ó-V(À\0\0\0\0\0\0\0\r4SÒI  \0\0\0\0À\0\0\0\0\0\0\0	4©ªx¤zh&¦PC@i¡Ì\0\0\0\0\0\0\0\0&¦F¦%7ª=FÉOÒSô¥	ã¤íÃ#ÄHäÑ:aO¿èêêÌ¹B£%¯aý³¤{CtÃþ\"X®rKcîB¥	qYá Ì§56P>MÐ# `ô0m7¶æG6@Æ\ZÀ5éÃiæ=s§ÇC~B\"ÀÍKy¼MC¸ìv0A¦Á¦\\c$=CCØü·¦hJû¡nIy9IwPõ5	\0>\'àB`ÒI\rÃQàÄÁ5±ªé5veJY4(&e°C3zçi¸$ôCAÀÒbÃVAQª&	`ÔÍ)[[Ô¿\0¶ÂHÜ2Ã0o#¼jiÖ+­\rv+X7\'JFÂñjR½JF¥i^AàQ2õµà$ÚebJ$Õ±ÅkbF3y\n eÉ ©rÄÒHZÄZEÀÓW-¤¼³\0ôà\Z¬))ÜLÅ4w®ª1j&¢fAÖ¶½ª+àRU0T£©*êÖ¡¢ª¯UÜ¾«÷JRI°ÀRCåe1k+&R%ábâFÈ È2	Ä £¦ØàF»ÒRÄ#+DEÑ:VG\r.)© ^úf)Ä`V\r[-¯aàÎdÍ­*°<Ü Qf\\+cCTZÜ)¸\ZP_KAÖH_ìz¤0ÝúÌ·/4(GÈ¾»6¦ó°7÷iÈãc±3ZDkz×¹Ä@T0â¼L:ýF}\nmáªwCædüÏëùä­êHêu]ªQIÜ\rGSwú2sÈ>Á;-Ã·Àp+7úqÁQ.¬%Ëì8Î ~$$¦ÇÁvÀBmí(%1 ¾îxN¶â0LpÉh1q7ä5;ü¼_¿­409 °ÆdÁüÌWj¢¿\'ÿ\rNóa_`p¨¨LÎÒÈÏ%×^PoÉ¼p2ÍvÏk\\ÛÝ]Á:lèp?®sûðk§¯.ÜWÌ}¨o±sÈ)!â¶°yÎpLÙNÃY £SXàßäSÓÎ	:C{ITk:{¶QRNðè_ìè»\r9-Ä>ìä<{¤-y\nõT¸kfCAÙ%4\'bèO}Fs,²ªTI@Â©F0ÐtT{É@¨8S$ðüJ.â|EÝ),¡Êê%áö\'ÉbÃLÁô\ZÓòá\r§R¢ Ü}Ëêö:da´°v$>fØT`m°h<²v\\}NGu8^Qò.Û/\nå²±,§dÔð§~ÓCg7|¼IÑ\"DÖ Ä\0D%&ÄhJ\ZL`IÀÉã³\"¼RðWÜÔP¡ºn!ãÑÀU¼Ý&æ0FMª2ÆLÏ|T¡ºØ>h©@²2×¨½Ú\rØlkéÔÆu;.aæJUUQ¢èæO¸úwâÉ @Ã½a\"l22®\'`ÃæßaÇ	Ö6íåPRT?âîH§\n9²¶à', NULL, NULL, '0', 'Azampur', '1', 915, NULL, NULL, '5Dtl1606572603', 'Pending', 'ronyb@mailinator.com', 'Maya Goodman', 'Zambia', '+1 (974) 606-1328', 'Sapiente deserunt eu', 'Dolore sunt necessi', '81193', 'Rinah Kaufman', 'Togo', NULL, '+1 (644) 355-1516', 'Rerum officia volupt', 'Quaerat quidem et il', '39446', 'Culpa quia velit no', NULL, NULL, 'pending', '2020-11-28 20:10:03', '2020-11-28 20:10:03', NULL, NULL, '$', 1, 0, 15, 0, 0, NULL, 0, 0),
(44, 105, 'BZh91AY&SY½úî\0Tß@\0Xø+ö¿ÿÿú`÷@\0°\0 H!Ì\0\0\0\0\0\0\0\0Ð&L@\0\0\0Ì\0\0\0\0\0\0\0\0(§iê©O$Ú¨=C Ñ@z0\0\0\0\0\0\0\0D@#M\ZhÞ¤õ¤z?J\Z=OÔ	Eæ£xû$2	´P/Á¿fÉ*2Z÷Õ;:I¸É	26´B«ÇØæ¤EGdb}bY±Ì*òtFA¡ê`ÔØ­¶dsda¡ÀÎ¯^\ZÓ<d6ÈßÐ¿4!$3BâÀl|4ã©ÔÁ[rösðTÞ¡+íi¤uc@ä~ 7\r$ÀØb*1\\&·5\\³GfT¸Òe0±Xµ){b_p .8\ZQSjÈ*5DÁ1 l\Z¥+k{à·FÃ,3ò;Æ¨=\"ºPÒ¤\rÒQ¡°¼Z¥/R¥DQ¢)ZWf\\\n1&^±6¼-LID¶1­lHÆo qB¹$.XÔE`±$Ö£dÒF:^ÅFÓw[Ò\\ª)2s%j&b;×UE\nµQ3 ë[^Õð)**QÔukPÉUW¢îH_eþÒl´\rmaõ\"Á\r\rTM¢M`4i4	¤4¢`D1&ÄÈ!(àe¬p#Mé4Ôbf<4MJÐ¢ÂZIÅV¡¼Ø=Ài»aÁ r\"Ã xAsâ©E;©YÔV1°FÃ\r.²Æ*ªvÂU\']åÇÃpð`î2§\'Fé)£ÛMælz_Îý(8NÑ	\ZÆ+â?ÁÄ@T0â¼L:ý}\nkÂ1UÞæf~çýþ§Á+z:ZRwãAÔÛüÀßó¸F«aßÌp+6ôã¢]KÜq4\0üIM4f<,õÚ;ÚPJcA|²Üò58×cLÈØfCô}§÷ô¦F$ÌÌðÃmTWíÌñ?á¡Þn+î4	ÁêBYù]tåüÍãuÖzÚæ½ÕØ\'-Þö\Zç?¨Ï¼<8»q^G1õ¡¾ÅÎÁLÌÃÉjÂFtþö6VjÜù\r¤ù°pC â\r&v)ëéÊNG©MÁ$ª5^æ-TgØIÂðr«ÆÅq¨	k°ÌA¢NÂ½U.\ZÝPÆ ì¹xßQË,ÕJI1P0R(ÆÔ Y(ÈÍ0â@oÈ¢î\'È]ÒÍPÈ/Q,r{,Xi>Y~#ÏS¡Ql~åt\nû2¨ÚX:\ZØ4³:®C¾#º/(ùÊ]ÄBA²i)\nãJDÓÓdÜn¶v#ÐÄÄßs$@0\Z`Æ\0Æ\n	±\0Ú\0Øp3<·sW^\"ªñ1{0\"Ðf^­àñ5Ênc	¤fj c ,fg=ñRëpù¢¥ÈÍ¯a|4a±¯§Cèu\\ÃÐªª£%àæOÀü;qfd0ïfHÍ		¨`4Kqoybè ¶Þ&Á±Cü]ÉáBB÷ëº8', NULL, NULL, '10', 'Azampur', '1', 925, NULL, NULL, 'tWNF1606572878', 'Pending', 'myvybygo@mailinator.com', 'Jamalia Riddle', 'France', '+1 (327) 202-9483', 'Consectetur velit as', 'In amet ut beatae e', '40432', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Laborum Ea in venia', NULL, NULL, 'pending', '2020-11-28 20:14:38', '2020-11-28 20:14:38', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(45, 105, 'BZh91AY&SYm¨Ìô\0Tß@\0Xø+ö¿ÿÿú`÷A¦ËU \0À\0\0\0\0\0\0\0\r	èSÒ) \0\0\0\0\0Ì\0\0\0\0\0\0\0\0J§ª~¨C@hõ4\Z2d\0À\0\0\0\0\0\0\0	jhÓFSz£Ôxý(É´ò¥	Ó¨Þ?F£ÄHÜ±:¡NÏÁÛÛå\n½#õNÎAì2BM\rÓ÷Uc:ä¥-ºJã(,PNÄ¥©Ö¦ÌÊÝ,\nÌÁ´ÜVÛjp3\0×\r§õÎ4\nÁ¿~HBðHf¥Å¬g¡&¡Öt: Ó`Ó.@1¡¡ì~\n[Ó@4%}Ð·M$¼R]Ô=MB@Cð!°i$Æá¨Åpb`ØÕtÌ»2¥,%°C&åÎ1¦uàÖh\rJ1czeÐXj©c@Ø53ZZöõ/¶ÂHÜ2Ã0o#°jiÖ+­\rjAP3\"äC@ª´L¸páT\\¾F$ËÖ&×ÑPÚb\n$Õ±ÅkbF3y\n eÉ ©rÄÒHZÄW\n¢ÆÁBÍÒ¯$ôà(Y$RSÔLÅ4w®ª1j&¢fAÖ¶½ª+àRU0T£©*êÖ¡¢ª¯5Ö¾ËöJRI°¤MÅ®ªV\nMR%ð±q#@dFFdÀbM¨!(ài¶8®ôä03tlõÐ²<2â$ßEbF\0\ZÀãÁdËAkÁXx33kJ¬7&hewq ÔZÜ2ÜNP_¦ Dk%Ûü§B§ÐðýNFO[#ä?ÛfÓÉ¬ÜvsnWÛ#+R¨MwÏ¹Ä@T0%xuûú ÛÂ1UîÓèdüIÏéùä­êHæs]*QIÖ\rGSwü2rÈ>á:-Ã¿(àVnõã¢\\ØKØq5@ùÈð´3¶hìiA):úh¹Þm8ÛÁ1Â&hn ÅÄ<Ôïî\"ñ~ÎtÐÀäÃõ0Q^`mªþ:,Çþ5;\r}Âf¢¡283;HK#=	.¹õA¿&ñÀÊ5Û=-so]wé³´à~ã\\§õÜ×oy¯.ÜWyÈ}(o±sÄ)!Þ¶°ÿ ½MÊÍAðz\r¤ù88!Jq\r<JyùA\'1uIÔy òØJ£YÓØÅ°ûvïBÿ§jè60ê;[|3¨\nwô<90´Iâ+ÕRàQ­mdÐ´ÊäYeT©	S\"a è¨0÷<ÊPp-¦H\ráó(ºÉï\\¤²(d¨¹>+\Zf¨ÖðÚs*\"\rÇÁbÜÂ þç1è6ÐÓ[\n\r¡Ü6\rN¨wÔê:éÂò¡tØAxT$Î­e9Mg	JoÚhrìôxóÞEåNx|Á$¾Ôl ! `\r¡(i0`\r\'\'~ÎPñK¸U]Æ/sRFQBBíÝC¿µÀU¼ÆÝ&æ0FMª2ÆLÏdT¡ºØ>H©@²2×¨½Ú\rØlkëÌÆu:.AäJUUQ¢íDr\'Ü}½d\0Â82h0ìfHÍI$k_E=O?Ñù~~Mb?âîH§\n\rµ', NULL, NULL, '0', 'Azampur', '1', 900, NULL, NULL, 'nfmn1606573238', 'Pending', 'datoqiv@mailinator.com', 'Cathleen Merritt', 'Ecuador', '+1 (487) 479-5543', 'Rem labore deleniti', 'Neque aut quae ut do', '91203', 'Blossom Spence', 'Belgium', NULL, '+1 (969) 533-5821', 'Nam fuga Sit est s', 'Exercitationem sapie', '29030', 'Dolor odit est quib', NULL, NULL, 'pending', '2020-11-28 20:20:38', '2020-11-28 20:20:38', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(46, 105, 'BZh91AY&SYé}¡\0TßP\0Xø+ü¿ÿÿú`þ|\0	P( (\"¨¨¨æ\0	\0\0\0\0\0s\0À\0L\0\0\0\09`\0&\0\0\0\0À0\0\0\0\0\0H\rÚ§¨Ñ4¨ýIµ\r$\"hóSÔò$ÀÔôC@£51,@2ySÈë@A-ù+\'îÐaÇAè3çC3ÅôqsÜ¦WÒ¥ä¹Ìø®²Ë-Qs%Ý_ñEàÙv§sfVe\ZnìÃ	v8R6]aù)Ê^ÅaLOIââvnå¼ì¼»Lw¾N­Ëî§{ÜñzÙ­¬§ãL=Jùåyó@©*z]ìa¡ôø}/°ÌGX%\"3ö,}í\r4Ùy¥.TTHÜ¤\ZJI7ÙcfÍ4´§¹fÊIÂ¤\n´n»fiºÿZ¶RÐûTp¥0á¦JP²PÄPR¡IuØaÒ>ËWþRÊNå&Ò¢åg,º3:0ûQÂS\n¡eÃaÃ2¨¥°Ë¯3¨¥I2ÂË²ÊòáK©+,¬Ë,¬ÓsM\r>÷\Zc¶îfÇ¶£%p³{iµSf·JiÕvêe³·dPÃ/6+áQSvSL·L%\rÚa),ºëe8lÒÊFî®p,faYMÓM169G±uCÐ:Á´RX@DCÎBÊJ\"[F\0¢Å¹â²ek-Zb¤HºJ\nAB ´A,¡\nl¤Z %()á8xvQN½òC5P>RÌ¶eWZÛw²¾Y²nÌÝ§.^¥4áMØpÝv:,ÑeåÃ£KM6YºÜåÊç-,i«ir°]K°R\\å+º631ÃK¸aR¹.£Óf4:)ºËV\rÙÚZYEq§½ìz×1oêò@\r?j?)sZUÊo-ðüÝ_Õ¹9¾æÏr`«*¥)¥ME²M¬ü_[¾Bzõ\'ôx)ÆQê|&0q605ÆìÌQÒzßõhä?\'öm9FòÏ	é{3	yã\'ÖS\'f\"v2zÊäöËÃ©:J?¹¹æPvÚlé;N«ºI,ÌñGÞæYºs9RRRà¨iÃË3æg|Ó¹ºÂÍåÖw­4£±ÒrQyúKIe=%%Þ·°³e=Ì¸]y©,úTÔºee\r¦äÙæù98Jè¼ïÉÒd¤úÖIä§Qg+BêcÜÃ@úÍ¥;\ra±Ç:2ì»ÚÓiÙìeß%Ü8tyNÓòÂåÇâ¿¢lý¸è¦ÏZx{o¡©ûÏ¤aÖYÑ=#½IG±õ³*Oyò}¯E³íDVAPï5=(@4mXAcÞààxØoS¨·sÁÐÓÀÖ¶*9pÙÈÄ©õ>3æL;Ñ=ÏÌóO ¥3p~£i¤ö nWiÜà;ÃØÀEQ6b.Ø0¤èØ¤^]Ê|òï\'£RÓIÑÊjfX»]Åå³0Se#±£C&±È\\)l)\rC¤64c¬ÞA¸WR²4ç2Ðóóghè3c <X:OØY¼æwnuv>óO$~sÊYç%%©9m.=ó«³¹´PyL Cy­ìCAµ²WK¸ÃÖÝxY\nCÚë-6Z,aFw-QM\rkÐõ4Ó\r´ç{C¼Îs!Âææ1@HÉî#Bb1\"Æ(Æ*PE!D,£$UóÂs=%©\'2swûnÞw SF¢ÄÉ¦¡cê6SÈ»ÏQ\n\\SÎw¹]³vòTIßQdÔæswÌ³3tÜltòfPåIöÂ~µ©E\'Þó¹uÔò/	féå\n76ì:­\r\ZÈì©[`n é).aâí31):º6OÚR2÷o4OÑ\"wwâe\'Ü]ÉáBC¦}ö', NULL, NULL, '0', 'Azampur', '6', 3545, NULL, NULL, 'wtL51606573480', 'Pending', 'sonyxybet@mailinator.com', 'Heather Berry', 'Andorra', '+1 (138) 638-3531', 'Magnam rerum obcaeca', 'Et reprehenderit bea', '90936', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Cillum corporis null', NULL, NULL, 'pending', '2020-11-28 20:24:40', '2020-11-28 20:24:40', NULL, NULL, '$', 1, 0, 15, 0, 0, NULL, 0, 0),
(47, 105, 'BZh91AY&SY3,ß\r\0]ß@\0Xø+ü¿ÿÿú`ÿ}\0A(P(9\0\0\0\0\0\0\09\0\0\0\0\0\0\09\0\0\0\0\0\0\09\0\0\0\0\0\0\0$I¢jõTõÓDiÐ*HDÐ¦SÙ&$ÃFi\Z&K aâýß¥üÑyñQQø§ô@°z\\(>AêzÝÉ»± z1_oðãiÒ¥ä¹ÌüYNå¨Êîå0uÕ»eRÍYi»³%Ü2§\rX~rSaÒÌ©ë³w-çeæUæ]æïÉÕ±}Ôïôz&v¥8t>úbx¼¥¼¤\ny TË.Vx»×;.÷6CÞ¦\":Ä¢|ê%ô¬},ßªÙy¥,ahµA6ùÜib¢%H¤Ùc\r)¥L>eßcñpºOZ¤n*\'¹]nÃN_S	¢hÅO­K(iL¤©EDÝÃiÃe§äXaOB\nj(J)\nP¤ÃvSw\r0æQ:©/*!Mi³eß[©Ò¬Ò)uF%(l³.a12¨¥°Ë³*D©Ö]L/IJEd¹FûhiàÝ»LqlÓ±±8fduåwÙ½7láÂSNn¦[°ÀËvE8aÂócáQSvSf[¦Éí0]uÅ2ËM6iÂÂÂWL830¬Ë¦é¦&S/82CÀð%âE(¬ \"!úÒ~\rÅ¹ME,ÑB0( 2BÔÑ`°`\"!¢(ØÒ-B°Yá8w<\nu·|æDMT$t©GfeWZÖïa|<³J4Ãfg859rñSN4Ý\rØèÉ¢ËËFl³u\Z)¹ËÎZXÓVÒå9`ºa0¢ô©Ë%Øt[FÓpÊ¥9r]G+:6nÎÉË*n³fJaMX«Q>¾N¯KÜì,ò6:y 8þlú¥ÍiVs;,â_á)ö;iÜàÊï{gµL0UIRÒË&\"ÊYKJ&ÍÖ}§/¡á!=:ûM<ËþJ<_	£L?ÓÐñS¶KOK>G®}Óïu~ülÆÍ©ä`Ülèºæ1pûì\'y£Øt\'èöËÃÜìiréGÜïpfDóPxJm6u¹wp¤ÌÏ$}®e§3%%)\"¢xÉ§,Ï9ÖwÍ;¬,Þ]gzÓIâv:NJ/?Éi,§¶R]èz6Y³ÒòlË×ÏRL¬¡¢Ü>Wèäá*~Åç|ôÎ%æ\re;KköÜ¬ê)ç3;åZTÄ§F]Ë¾VNÏC.ù.áÑê§ç),y)=/9ûº<#6x\' »ÖúÞÆ§{3yîuÎ;Ôy>rveIï?WÐóQì/B*¾p,fðBQ Cöç#À¦ÁÚ¦NM<À³d¹QËÌ¶F%Oêþf\'aÞî~§Ê1J±Ä;aÀÚEú8«s¼ðní`A¢ 1\rÃ*N­É¸ÊIeåÝ×.ó{\ZN®SS2ÅØ¢î/(]ÂÒë0©ã,úfX]2/)hrvK@x¶ÎÐz}BÄiÄÀ\'ÿ¥ß2nÒ¦çÆRpû½®ÓÚõM¤/<\'ÀÓÉ¬òzä¤¢\'-¥ÇÂuvw60\"ÉÎG!Úfê?##bî×sd)|W°ÜÓIArÂÆò¢F¹dèÓL8S©¸ñ;Nb!©÷=ÀCò4)#,bb¥F@$AhÁ2%@nô¹½ãÚ&Pºêäbbï\n)£%¢Â]¦aë:)Í{²¸§®vr»fíä©´ZQESÍÞ¥¦ãc¤§30*O|GñPìÝJ)?³×7rë=ê/	féê-ùSJÁ¤è3j¶`ñ\"dD°\\ÐÞàÝýÌêÁ6jj#þ>ï Â!DAÿ¹\"(Ho', NULL, NULL, '10', 'Azampur', '2', 935, NULL, NULL, 'ETtV1606573714', 'Pending', 'pavocamig@mailinator.com', 'Alana Ballard', 'Seychelles', '+1 (367) 869-6153', 'Sint natus iure itaq', 'Ut sapiente eligendi', '69951', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Enim aut aliqua Mod', NULL, NULL, 'pending', '2020-11-28 20:28:34', '2020-11-28 20:28:34', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(48, 105, 'BZh91AY&SYÛÁBè\0ßP\0Xø+ü¿ÿÿúPiÑ-th Ó ¤Ò£ \Z\Z\0 4\0\0%H)M\04Ä\0\0hS)êÔ7ª1¤ÓÍI  \0¡Ì\0\00\0\0\0\0H6&FêhLMAp\"³Y\0w` §&w9û¨\\¡a×´b6öwY Ü2BLÏ|ÃøÄ$ä\'âzié}V¡1ôêtXÃíîNË»¥Ë­ð³-BM|fkâr\\ªÌdNÝæ gPHf¦fñÒ@ÐÄ]#¦T[}\ZMCJ\"!jHjÍV¶îÌc440±JRÖ.ÏXÕ0cKÅ1k%Qª&	`Õ&­­?iï2$ù]®@óÒt2¦L xvÂ\"nª® jv&)d4¸ã8ª%)$	!(I`ÌA¢Á$ÁdH\ZÁ£/l]gùÜáÖ*ejµ&Ói!ÍiídSCVnÚ&d+Å­Æt\nYh E:%²×ta»|Çº£÷+ÈHÆ-U2 ¨!EQ@ÎT±H-BT\0@\'|Ëî¦É Iî1Öö©6|&åj^¦«B³{ë>/%k2W	áp^3%ÄdÞäÚÝ@ûÁ3ANS²+V³:§ÞÉfj×,´xTwæK1ÏçÌìq<T÷\nð\ZÍ/5+ÌO×oÒsñ âéÇDµI-¬B!ÃÅ|Gæl *?Â»uøöæ@Tc_xU²Hm)MãRh±¦Ã¸u8P:©S²Ìaòð%ÀÍ~ðTK£Råô57Nài(©ÍëL\Zm4¶cZÍ\rKnÄ`Ö.¦c?ÄM¤rãÌ¬V3ðâúò2uÈÚ \\zÝ`þ©è4*5ê Ü*Áï!,Æy]tãÙPy×|öµÍü«¨NY\Zs=Çô5Ü0?/=§¿fíÉwxw68ÔÁé\nhA s» X{eãå8\Z*FòBHp\r&yõz`¨¹IÄõ ôèJ£YåzÝî$äªÄî]ãg¡pù@§^òØè1G¯E7iqP¹TI9®d÷TeV\nI,\"ò0¡*`¤QäU2¤R ¤p,,2V½EÃK49d¨Ä~ÆÛG¦FÆ#òUXéRøtHy\"ÐÓE3M*·Q°h<2;.#¾8ÞQÔÒ;hAxPQÐÓ!Aâ4°z\n(ãª0sw7mÕ²sC¤xBuB\0@Eh \"­EÄPÑ\'3¶Ô\"¼RïWyiÈ@A¢¥ô®¼ Ü\\[ÈìmÜÆHÐÙ@Æ@XÌÎzEJX­Õ(FmzÀÿZZá±¯gcî<WP JMQFTGq?èùò\0@F8Üf)w3&PÞP¬ª¸-ÔmÚôß»z2P%?ñw$S	\r¼.', NULL, NULL, '0', 'Azampur', '1', 280, NULL, NULL, '399B1606573820', 'Pending', 'gaqywaqev@mailinator.com', 'Megan Merrill', 'Singapore', '+1 (934) 158-4894', 'Beatae ad qui except', 'Minima dolorem a mag', '95091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sit architecto et m', NULL, NULL, 'pending', '2020-11-28 20:30:20', '2020-11-28 20:30:20', NULL, NULL, '$', 1, 0, 15, 0, 0, NULL, 0, 0),
(49, 105, 'BZh91AY&SYv½þ\0Uß@\0Xø+ö¿ÿÿú`÷@5mZ\0\0HÌ\0\0\0\0\0\0\0\0Ñ6=$\02\0\0\0\0Ì\0\0\0\0\0\0\0\0¨§ª\Zh\r\r£Ô\r4\0s\0\0\0\0\0\0\0\0$Da\Z&ªOÕ$ô£ÓÊ+Ìæ±ôTn¤P_c3¢ß(Tdµì1¢vtadm¼B«!(Øû¨²P Ø 4	F§Z°+tP­*à	:¡Si[mdrda¡¼Î¯Mú!é2\nüóByuh\\X8CPâyFDè4Ëd±ìj¨%¹4BWÛlÒKÉÝ%ÝC± ~%\0ÀìBA¤\"ÃkFW=kh*A	\\i@2ÄBX¬Z½·,ª%¡ 08\ZQTË&¬£TL\ZÁ©R¶·bù¶FÑqF¨=\"ºPÒ¤\r©éB(ÐØ^-JR©R¢(Ñ­+Ü3bL½bmx2T51jØN&¶\"H¸\rJ2äT¹bCRHYÄîC\nÌK[+J¼Ó \Z4É\"*ÔLÅ2w®ª1j&¢fAÖ¶½ª+àRU0T£©*êÖ¡ª¯EÞ¾Ëþ%)$ØhÚÃ4P®GT#Cb©JÐÈ0àÓtI¡Á&44UF$ñHEÁ¯0)U AP2tm¤PÈ.(Ò@½ôÖ)Ä`¬08,Ùh-x0ËyÉ­*°<îLÐ(Ê1w8Àj\ZÜYEî\'(/Õ¦`,²¸ÀûS±¼èAÔ©ô|>Ç#3µËÆfEõ^q\\j:íãuÖ8&X5dlb¶éÏÜà *?¼:ý}Jk¾1UÞgÐÌüÉÏýÜ¹I­J);Ãa êmýÎ@nõ¸F«hÃïäJ7F}x`¨Fåö\r\'@=HIM$f<,õÚ8´ ÆÁ|²Üñ57×c|LÈÚ3!|Ôîð\"ñ~=)É33â`¢¼0ÀÛUüòYý48\nûÍBdpg:c>d]; Ý¸p2ÎºÏ[\\×¾»BrÙÌÞ~ã\\§ôà×?NnÜÈ}hn±sÌ)x­XHÎ»Uàù¤õ<ðoC à\r&yôùA\'AwIÜz ùl	%Q¬òö1l\"£>ÄBw!³ê60î<[|3¸\nxõ<:0´Iæ+ÕRàQ­jÉ)¡;2xÔg\"Ë5R¤Lb%L1C¢ ÃÜô(JAÀ²3L8ò(»Éñ|¤³C2ÔKÜ5\r3Ôk/Ï|yjt*\"\r§ÁbÝ þçAê6¤ÐËKF-CÀl\Z<Î«¸wÐî;é¾òYBë°ð¨H6YU#LÆáb´ÇNn6Þ6eG11¾¶8Ó00P0MÐ40Àyã³\"¼ðWÜÐP¡¹í0ñæà*Þ\\¦æ0Ffª2ÆfsÆ*PÂÀ]l$T YµØvÜ65õèc=«|Jªª2\\Ñ÷>ò\0aâÌ1HÍ	$k_E;Õø4&Á±Cø»)Â´\rïð', NULL, NULL, '0', 'Azampur', '9', 8100, NULL, NULL, 'musg1606574145', 'Pending', 'netenufyx@mailinator.com', 'Lacey Stephens', 'Cape Verde', '+1 (707) 381-3837', 'Voluptas at ut in eu', 'Quo sint rem autem', '99366', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ut qui voluptatem en', NULL, NULL, 'pending', '2020-11-28 20:35:45', '2020-11-28 20:35:45', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(50, NULL, 'BZh91AY&SY+éÉâ\0Ýß@\0Pø+ô¿ÿÿúPøòCdt9\0J!	DÔ` õ4Äi£504L&EM¦FM\00F4\0  hzi )ê\0\r\0\0\r\0\0\0\0\"BdÒdFÒi¥6¦È@4¦)\0 î;À%Ì_³°LgÈcæ2N4}Î\\¤Ð¢ÆK\\F#è<:#$$×í0÷D+d[ÜéNÞlLÜv.-ÓµT§[zæ`?bc¬ÚÊDÎÛ6JÅÍ9*iÚA=ä÷¡ 5(Tcäu9bBK¨Öø©.K{HBÈÐ\rg0³Éiá#Y\Z:*CC\r\'8ÓÎR617æ³6Æ1J`Ð6\rEÕ^1ÕM4Ò¼Q7,fÁÁ°Åi3ZH¬`KEÕÕØ¡2æ&®ICDÝÅÝÁ  ë(¢d2`ÚILH%´T7\r¡TXYYAQ%,ÀBêózªP«LRj&dÞ3©MAÊ­B)Ù+EQjü)qH\\RÚÄLBñjÜA\r&Ô¨)6AèiºCi0Ó@CH\Z&G\0(`Ð÷¼ô6ç	\nà£@ôÍ)ÙIDá>\'péZ,ÚæòÁÒj°qDÞëAYÍ¨Õa)+0E»zMY{ËÁgÇ\'h­QúùæãRÈ\0÷~F%(à±QüGýßÐçbáo:ôs!`5\'\r*Ú=è &bztúÚz\'¥ ÙcF*=Mºâñ¨ôhÈ§¤C¥A¼Ø:\ZjÎåq¯HØR\\gË;HÕÊÈÌÙ-ÀQæ\\+`ÓhÜÒÜcRq°Úa \"Ä£l,Ëì=|¥¿R+×ë3 Ì	)ÃnÂ¯/RágØ´ÔÈ Ó2Hpc-%ÏO¡Æ³Á(ñNÛcQP\rõýX©)xúIVJR£Æ×ó<,H(4R¼âÎ|ôøFçÚI6¨cH÷CIüzÁ#µ©âXÇ %6¯iTLgSÜÜÚý§%Ôlý|ï1aîÃP+QÀéÃÖ,ÒMmÈ¨È¬v/!>?)-¦ÅÈ$¢§XIe*bÝ\"jc	¥ä)u¸øYtf ]L´JÆß¹.ªØ\r2ÇÐk¦étÐèw#qèaÅñÄCLl¹RAèlÐÎ Q­iB3¦G5þ7ÌMg¾ÒD6#¦Â\nÂlÂYÃLQ¥câ\nöytÃF7Í?PcÆû\00`4Á\Z\rPÒ`ÄÅÎypS±.B¢äZµ6,I ]ÚApçÜà(Ýó<eRÖCH¹À¹yk&YX*²P`µøìÃK65óâZûkBSSQeÁ©/aðí ª&Ã{0r52&HkØ/q¢¯zÜxxuBl0p¿ñw$S	¾ ', NULL, NULL, '0', 'Azampur', '1', 750, NULL, NULL, 'Fq4M1606578811', 'Pending', 'test@gmail.com', 'test test', 'Austria', '067090413', '3102 Saba 1 Tower, Jumeirah Lake tower', 'Dubai', '65444', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-28 21:53:31', '2020-11-28 21:53:31', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(51, NULL, 'BZh91AY&SY+éÉâ\0Ýß@\0Pø+ô¿ÿÿúPøòCdt9\0J!	DÔ` õ4Äi£504L&EM¦FM\00F4\0  hzi )ê\0\r\0\0\r\0\0\0\0\"BdÒdFÒi¥6¦È@4¦)\0 î;À%Ì_³°LgÈcæ2N4}Î\\¤Ð¢ÆK\\F#è<:#$$×í0÷D+d[ÜéNÞlLÜv.-ÓµT§[zæ`?bc¬ÚÊDÎÛ6JÅÍ9*iÚA=ä÷¡ 5(Tcäu9bBK¨Öø©.K{HBÈÐ\rg0³Éiá#Y\Z:*CC\r\'8ÓÎR617æ³6Æ1J`Ð6\rEÕ^1ÕM4Ò¼Q7,fÁÁ°Åi3ZH¬`KEÕÕØ¡2æ&®ICDÝÅÝÁ  ë(¢d2`ÚILH%´T7\r¡TXYYAQ%,ÀBêózªP«LRj&dÞ3©MAÊ­B)Ù+EQjü)qH\\RÚÄLBñjÜA\r&Ô¨)6AèiºCi0Ó@CH\Z&G\0(`Ð÷¼ô6ç	\nà£@ôÍ)ÙIDá>\'péZ,ÚæòÁÒj°qDÞëAYÍ¨Õa)+0E»zMY{ËÁgÇ\'h­QúùæãRÈ\0÷~F%(à±QüGýßÐçbáo:ôs!`5\'\r*Ú=è &bztúÚz\'¥ ÙcF*=Mºâñ¨ôhÈ§¤C¥A¼Ø:\ZjÎåq¯HØR\\gË;HÕÊÈÌÙ-ÀQæ\\+`ÓhÜÒÜcRq°Úa \"Ä£l,Ëì=|¥¿R+×ë3 Ì	)ÃnÂ¯/RágØ´ÔÈ Ó2Hpc-%ÏO¡Æ³Á(ñNÛcQP\rõýX©)xúIVJR£Æ×ó<,H(4R¼âÎ|ôøFçÚI6¨cH÷CIüzÁ#µ©âXÇ %6¯iTLgSÜÜÚý§%Ôlý|ï1aîÃP+QÀéÃÖ,ÒMmÈ¨È¬v/!>?)-¦ÅÈ$¢§XIe*bÝ\"jc	¥ä)u¸øYtf ]L´JÆß¹.ªØ\r2ÇÐk¦étÐèw#qèaÅñÄCLl¹RAèlÐÎ Q­iB3¦G5þ7ÌMg¾ÒD6#¦Â\nÂlÂYÃLQ¥câ\nöytÃF7Í?PcÆû\00`4Á\Z\rPÒ`ÄÅÎypS±.B¢äZµ6,I ]ÚApçÜà(Ýó<eRÖCH¹À¹yk&YX*²P`µøìÃK65óâZûkBSSQeÁ©/aðí ª&Ã{0r52&HkØ/q¢¯zÜxxuBl0p¿ñw$S	¾ ', NULL, NULL, '0', 'Azampur', '1', 750, NULL, NULL, '2hAv1606578906', 'Pending', 'test@gmail.com', 'test test', 'Austria', '067090413', '3102 Saba 1 Tower, Jumeirah Lake tower', 'Dubai', '65444', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-28 21:55:06', '2020-11-28 21:55:06', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(52, 105, 'BZh91AY&SY\Zï.Ý\0$ß@\0Xø+þ¿ÿÿú`_4*¤( \0\0À0\0\0\0\0\0¡0H&É \r\0\0\00\0L\0À\0\0\0\0&\0`\0\0\0Ì\0\00\0\0\0\0HÐ&$hò¦©§ê§¢=Oj	Ðùsô8ò*A² ùÉò}>,PÀdµüFIàé$õvd\Z73¦6û(Ö¢Ô¥ÙÐ¬ïwL\\-Ö?$Ç`trt]×40Zî«$FÂøF×6Q¹:¸½@Î ËÆ¢æq^&ÃÃÄú	kBhJðIÕø0ka	2H\\\ZÕÚÊ, ¡¡gwÁ2Ê:S`dTTIªyÁ1 l\ZÒÃJ¥ÕHâ×¡ 1	Ì(Ä2¨KM;¼{Ê$ØÜd¬VøÖ¬*&ÄY¥d\ZÖ+Zæ)4`cÆ8ã\nQ(£Ä YÑQyE¸1Kî·¼Z\\@eím[U*_\n&¢fAÖ¸[\nÐhµÁeVáJ:°XaCU\\ÅõH[Ø	HM¤@ØÐ6zÂ¸)$´6\n¡0îiº! c4	¤4¢`D0Á\röëÚ=TØ±zf´ÀÊÜ(%ê{I5®Ur=åöXM¥¦2¤ZÁ©bV]Q8:¦ gÙ¤¢å®SZ0¦¬¬½#ì¹i,(¸Á*(¾¾G3ÌÜu $©ò^Ïàb|*ñ.P¹þC÷çäuÄ«Ì1øÛÞéAÂ&+^<¶\n?Cð)cä)Ô@ÇÀâr..Q)ÉÔyÆÏ×Ðû·)#Í~u(¤âMC©¶æØ`tY?¯S±DyúÚ:Ø3ÓveDº°7-¡¼Ù:\0ÒQS¢1ÌxD´ É8]ÑÎn8¿âliÁôa¸±ïÇ)Èoþ(ê5=Ý´[[WR`«0\Znâ·ºëþÑ¡ÄÌ¯Ì=ÄJf±TÎ%ÏÈË¦è6äm ò®á|+ N9÷°ÔãÌË`]á±o$çÞtØXfÌN¶\ZÃQ¨9­$gÄ[*×ìx¤ëcjÄ¡ð¸ÒgR ·I¸ð.AË0U\ZËVÈ ÏbMá:!}åÐla¸ï/Ðöfà)Ë@ìvcä1ÄÔS`(ÖvkJhNk=ÕU¡B	%ZF%L2äxL©ø$ `\nb±6\ZÜ¢C©Ô\\8¤µ!Ë ÆÂY¾äâ¯Ó.z\rcúîßÁ¡ÌÀDcÿÇB;\r1´`@Vu\ZÃ Ø4ìNËxí¼áMÖAæÒ;fAhPQN¨i ÷+ ßXé«ã×\rß6p?¹æÌÄÄ1¿ÍÁÓ1&4(\rÒJ\ZLj\Z*o5uòPðK ªµµd  ¢à¹m0ëÉÀU¼NMÝ4ÍdFSß(]\\,³$T `òAøbÒí|ºËQÙw©D¡UF+#?¨ûø[äb)qeØICYBF¯Uì4SÈëÕð<}þ(MbÿrE8P\Zï.Ý', NULL, NULL, '0', 'Azampur', '1', 18000, NULL, NULL, 'VDDu1606578953', 'Pending', 'nunocamej@mailinator.com', 'Raphael Moss', 'Zimbabwe', '+1 (383) 915-2501', 'Voluptate delectus', 'Qui adipisci minim s', '93571', 'Anastasia Hammond', 'Cayman Islands', NULL, '+1 (597) 341-4212', 'Voluptates et offici', 'Mollit deserunt volu', '16882', 'Culpa velit asperi', NULL, NULL, 'pending', '2020-11-28 21:55:53', '2020-11-28 21:55:53', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(53, NULL, 'BZh91AY&SYÆÃ\0NßP\0Xø+öD¿ÿÿú`|øU\0\0G0	\0\0\0	Ì`À\0&\0`\0s\00\0	\0\0	5\"ôG¤d5\Zi 4ÓFFF0	\0\0\0	\0A4(õ4l¦FÐ2	C Èè@;	Dï¦~;N¾¼L	fu	¸âÞÅBX,f~mOmTÁ0ÓzS¿æÈÓ,p½Ea¶xÕ)Õ±0£Ù1àdaî2r3/×}!ÔmÌv,XÝn\'V6Û&Ô2ë«ÄÇ!ByDèÁÿ#¬gQÀæ2hiû¦@HHÉG@øâFB)a*Ö/cåÎUf%ZB*{H\0¡¡2\\b`ÄÒ\ZÓ5IµáC1%AX!s\ZÂ÷Ë+nÅIÉÌP$Õg7ÀþPèc`Ð6\rEíWÅàÈZÎÄCJ÷,JA8ª.¸KQ#Á°ÌcUUª>ÂÀ©¢jÕx8&$ËÚ&×\\KÉt¾*1k~DàA\"Å3$XÉrC$*,-ÜÁ*òÆN1FTgNØªjÀÂµZ­a35§d,©B¬ÞQ3 íkâö2*Ud ±NÄ¬+Þ++x­Òzù¥)	´hòSéeU)d¦\nKJÂ	Caò1h`Â¤# É (AduºµÐI!øg*¾_UuÒ¾LVlm[+í»¾¬^u½Ç¤·¼ÄeÇm¤Ú÷hØ(* )Í¬Eo\næ\r^5f3mf×X­nók7\rí2?\\æÓY°¢çÈ	àñæg³°Ðf|Â½ãûÕÐpæ`êþq7¸ÔqªDÄ©MU\"3#oS°@X2~Å»»~C>âÄz£6Prè|ðNßóï?rW5$t:/K¤í\rÎ±ÊÇpÃÛÄp-7öíÂ]Ì&nq8O\0\ZJ,tFL\ZmÚP.a|ä>Ó<3u\Z&9ÁtÍhbë£ò\ZÞâ1îï¬jä`ØÁ*¡Ú°¯ã£ñ?s´Ü·ââ9Lâ+#y±	l3ØIá×=c=­Æ|ïe¹ë~ó¨ùv0ûÎ§ÄþG³gµêyúQÎælh<3ÃÚ/y²bÊ<X²BCõn\Zà%@ÞÆF{\nöúÁ\'»$ì=¦H=w	%S[ký¾QaRNàh_aù ØÃ´ò5Àú³´\nóô=$/zU)­ò*3ÄÒSDîºßaZTQ¯!Ø¥PZ)4:T0úâé@¨p-ÇYÌ?z³¹%²¡bÂY:¿}Vn4Ì!­|:£ÓàXGèIÌ¹~@ÿs Æ@y\r¥âg{\nÞxÁ öly®ÁýÆ¸§uºó(þÒéÀ ¦^Ú©R1pà(£~¨ÈÙ§.ÄæÔ\"A*A \"2J\nH\0)@±¬ÐpîðT0Þa0Fq#!(¢áÊ\rÓÁÀY¼\'Nå4*2æÆÓÝ(ÊÈan>ôX º6kà/£AË-~vày®ðøix\";ÉúÃ´BÑÌ0ähÜF¾&æÀa¸Û²¼ qñíPRR\rOø»)Â®4ö', NULL, NULL, '10', 'Azampur', '3', 1360, NULL, NULL, 'QdyV1606578970', 'Pending', 'hassanasrf97@gmail.com', 'Ubanto', 'Pakistan', '123456789012', 'test', 'test', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-28 21:56:10', '2020-11-28 21:56:10', NULL, NULL, '$', 1, 10, 0, 0, 0, NULL, 0, 0),
(54, 105, 'BZh91AY&SY.ý\0Tß@\0Xø+ö¿ÿÿú`÷A¨²ãºà\Z 0\0\0\0\0\0\0\0@2R\0M\0\0\0\0PJ2d\0 4\0h\0	5)©â¤~©£F&¦Q 44À\0\0\0\0\0\0\0	\"dÀL\ZhhÓÒ\Z=OQA!`ÜuÓøhxÉ\0üýª}ÿG_]Ë*2Zóx:IÉ	15Ì?¬B«ÇØæ¤EGdb}bY±Ì*òtFAÁä\\ÐÖW\rl,36@5å·CÄyå|B¥®læ[¼¨$32Â¹Àg°0àv; ÇPÓ,@1àÄó=ª[@4%mpµÍ$´ä³¨{Â@aí!¨i$Æ±¨Å`b`ÔÕqÊLÞ©EI.UA*ö¶/zA¡³g.\"bÆlD`(3M6Zø¿¸¶ÐÃQ$kËî\Z¢\Z@óçC:T1¢éÀÐ¨*ZIA¢buCé`*Ä|\"mx	4/¤Õmhµk#°\"R£IR!ÆKAÇGX­\n#67ªÍbo ¡ºTº^dE-ZÍR-HEFwpYV%l\n**QÔe1UUæ¸$/¢þl`)¡ò©Eª(¦\nM%%p1m#@dFFdÀbMxBQ´ÇHÚF{IÁÑg±&é©Z´XKI8ªÔ7¸\r7l84DXt.aÁÜU(§u+ :ªÀ11VØq¥ÖXÅPCB.ÐB°r@@j¥A@ÑÒf2C¤ÀOåÌlªU°.@Üâûë¸óäØêÛ±ÈÚÃQ½TK¦I×Aé@$,~²ÎN²F1dkJ1dÎ`víò=I[Ìæ»T¢j3MÁÄ\rðô¹Ùkzw¥`f±×*%Í¸±mFã9Ì	)ÀïFCºÄËHM£¹¥¦0ÞÇµ¦Ó\r/&6Áf&±[Åø\rNÎZ-ÝÎ`1>Òå¡ÆÚ¨­áÈèfgq¨¯8LÌT&GS¡	d3ØIeÏ|26PyWIí8WXN:¹O¨×ùè×.¦{SûPÙcÀ)uZ0^bsòì£´åH¬äÉà$7&xòñNbß&óÈ¹ U\ZË2ø]ô$î	Øüa±ó¡|7üÍàS¯c¼\"OZªj¸¨_@x$¦ê\\î¨Î&%R¤L^%L1#¢ ÃÔò(% àX&\rð(¸Ô\\%%¡Z¢W;ý	ðWÀi>#Xþ[c¿CQk?ãaP~1ì6ÎÄâc£@è6\rGe¼vÌÞp¦ÛJ=òÛQ¡PRúkLJzL%xjZóqß}:!óH\"û©\n ! `\r¡(i0`\r&Ó#®®0îK ªºµÈ@A	E\n¸2¼[¹ÐÓ±{¦¢ÀÈÊ{¢¥«¡ñEJÉ¯p½Z\rwlkãÌ¾Yñ%*ª¨ÅrDq\'Ô|¸ÜÈÄ aÜË°544NÑ¹Í¾Ó° mÛË ¤¤\Zñw$S	áÑÐ', NULL, NULL, '10', 'Azampur', '1', 925, NULL, NULL, 'nbWz1606579032', 'Pending', 'cojakyky@mailinator.com', 'Dorothy Noble', 'Chile', '+1 (697) 963-7584', 'Dolor veniam non qu', 'Harum rem aperiam ea', '52546', 'Dawn May', 'St. Helena', NULL, '+1 (751) 177-7807', 'Expedita omnis delec', 'In earum dolorem ut', '20860', 'Est voluptatibus co', NULL, NULL, 'pending', '2020-11-28 21:57:12', '2020-11-28 21:57:12', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0);
INSERT INTO `orders` (`id`, `user_id`, `cart`, `website`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(55, NULL, 'BZh91AY&SY¥~¿\0[ßP\0Xø;ö¿ÿÿú`h\0Å@èÐêA\rD©¡´ M4`ddÐJdQG©êM¡ê\04Ð4\ZÓ\0		\0L\0Á&¥¡£CA¡ Ð\0\0\0Í10\0\0\0À\0L! @É¢dÕ<\0\r¨Óõ4H$X4?©ü¡ô;ÍJ:$° ^ã0:,èèÁD(êâ|~cRËs:kìì¿fE÷Jw}C7Þ¢ðªS¥üõLxrö2p3¿1Àç­Ã9\ZQ\'Ëó©Ý¶\r´6C×eÙÔÏ@B<Á\"øp2#ØfãÍC¸ò?qmãó@@Æv!ëæ%ÚB^|æ\nû;dÆ¦Ã$AI0Hk-ilÉvPÐdgM6$ÔC\r\rcZ«kÈ<á&0÷´B\r+kU®·âÍFHLh¸ÆÁ1 l\Zâ°gCSHÞ&bí]Q@Ë\rÁ°µUU¾gØP`¸+´c«@XRÄj«9$DªµDÌØ9¸aä0¥1%62A(d(gÀ°m¥\Zò)pe\Zííqq5 ¡¹FS3\")q©°dÄ\"£;¸,Íñ{*²X§bVïFÊÞòH]W¹,$R\nA`)ßRj(¦\nK%ÅÁ18YUVA\" !H0¶R\0¡À]~°»BÕ AýÆ3_GÈØ/X±¥hZæµ½jdÅ 0Æ)ÔÃ5Ó:Kª \"!RSA6eE5¡ÔÖÂ®)5+meH¬3ª`P;gßå7¤Iì:â4QÍK£$\rÀ_ß9ß²Whgr,@¬.B`c1øò=ÊäHb-þÅ=¦6îlÊ1Öâ=fëåï?£¦äq¸Î4ïIçplnÁà$âXü¸aùõ%xq1#7®9,%Í¸ÁÇtäÜa	)¹Õ\Z+CV\r6Æb/ît68ÝÉ1¾¦ho ÅÉWä5=¾b1ÝÎ±$ S&	U06Õ\nýr´=Ïæ~fã¼Ü_ûÍâ¢dpm<Ka¤];`å©Èp2­·Ê\r2kbàO)õ¤Hl)5 *ÈP¦dÂÂc V¦¡Õoa#:{äpWjÔö÷Æ%;A¤Ï^¾IÐ]²v¦H<ö	%SZéò3|¢Ã?É\'xO,ÍCciÝ3_C²Jvýex9Ä(S9Ê%ÊEMoÈ¨ÏwIM½zKxàfÊ cHQ*`¨¦0Ôt¨aúÅÒPàZ-â~(ø4¨rA	dõý	ø¬Üi?ÖËzï:ï._ X>${#¶KbìûÄµøl\nw3Lq±ÚwW,J ôi[bËÎèi²Iè5.0Q§Tf²òlåh\"âKÇ;A$yR¤ª¢\"Ò`ÄF§\\xB-Ø¬¼LãâFBQEÌ£§\Z6.åáÁÔá¬àÎSHØà c .jk>b¬ÃæF­|Åõh8e±¯Åx×qæ¹y)YX¡²t\\Ñ<\núq\00ÑaÞÌ9MHæ]³øanCs\'Ø@ÇrAIH5?ÅÜN$)Aß¯À', NULL, NULL, '10', 'Azampur', '1', 340, NULL, NULL, 'm4nM1606692950', 'Pending', 'dan700402@gmail.com', 'Dan Ny', 'Malaysia', '01121760171', '11 , Jalan setia 6/31 ,, Taman setia indah', 'Johor Bahru', '81100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-30 05:35:50', '2020-11-30 05:35:50', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(56, NULL, 'BZh91AY&SYþX¢\0Z_P\0Xø;ö¿ÿÿú`nÀ6Æè@\09¦&\0\0\0\0\0	SIB¢zPÐ\rõ\0i§¨\0A¨*6¡ \r\Z\0\0\0\0\0R¡¦õ\0\Z\0\0æ\0L\0L\0`\0&	&&&&jjbÐ\r2y5$	F\'ê~ÈùË8¤ÀP/!D0pár$EucáÒ=)°çP1c_K:û»ÁÓ`Òc³7C\nKÉ¶tÉ¦½aÀøk¦:rñ,j-¥£Pø2HÌfÂá\"dÉ½ÍÕ2±Pbuö-½\nò ð gx$Aý }\r#2:ÎãÖTËA÷Å Ú3Ä¢öÄ¶¦ÐV½²|6È­ÔCÈ´Ó&0HjÍc¤UÑ\ZãD¨0hm¡%âóØ¥Õ&0ò±\ZX^s½é­eLh ÆÁ1 l\Z«:­`¦0$@åH¢QA\04 H,ó<Ç9EÆ.ªR0%XËáPPPT9iith,npsD8,9%,ÐK\na=ÐkÔgÆ-|DÔÌ^EÍÒ¦íU`æmI,^\"\0d©Z k`	6ÂÈA©¥$\\¢µ© ìp`Â,H)§J-ªl¢),[J¹ÀÊª°Y(P²dhE,ó;F¯{NBi:D#d^¥Ðýu´±4^*LÆI¹­nU8±<m<¨8( ¥`,ÊDX½ä_\nIa½¦ö\'b3wÌ£Z\Zâ+M©X|\"qìâúï9ÇÄ°üºCE´d÷ÉÇÎwðJãï¯\nºq;ºõ\ZÚ0~ÈÚX)çaqÌõÆ8(4í>Ïy+ü3ÌÖ¤Gç5#pdf<\r^!Ú$k0>%ÃéÌ=Á¬¬h2zì`%ÁR¹Ìåc0cIJ4\\vX`Óhìi@v\"°R#QLí%\ZADÌMb1lGÕô\ZÞÒ+ÝÂu\Z ¹b¤áFÚ©ÎËÈý¡¼Ì§Ä&h)àÊZK!I\\vÁ²æÁÀÉøi.©¦ì5#Ì´äm>cPskC>ÂÎyÓºf*xî\\9­HgzÍJAèxöö6!Jv#IäúøA#¶ÈÚu,AÝHWÇÖZF?Fð´*áÁÆ-§#\ZþæÐôfà>g½ÝçÈV$u°S¨kK\neµ¢JS%¢êds0åQY) (Æ$¥âlaqÍLaèx\n%àX¬MÄÀó&£¼Z¸$®$2\nà%c¯/Rµcñ\ZÇòÙt8Qî(S`¢<CÍ1±b@{ÌsÀS`hÁ ðÄè¶¹MÓÙY\"Ó\"\nÂl¤³\nÄ¥¼Fivf=¼½$[(äp\"D/2T@TADa(`(XL1&À°¹Î½°;ä,\"Õ©\"Á$L»uÌÈ:qp)f«Ê¥¬FF¥BååÛ,¬Y0&EÚöêÐj³c_Èµó;\0ÞI,ÆÉb¸\"0;IýGÃq\00£.\röUÉ±D\rVkÐan3ÐaP\nJA©þ.äp¡!%ü±D', NULL, NULL, '10', 'Azampur', '1', 340, NULL, NULL, '6LJN1606693112', 'Pending', 'dan700402@gmail.com', 'Dan Ny', 'Malaysia', '01121760171', '11 , Jalan setia 6/31 ,, Taman setia indah', 'Johor Bahru', '81100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-30 05:38:32', '2020-11-30 05:38:32', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(57, 108, 'BZh91AY&SYv½n¿\0ïß@\0Xø+ü¿ÿÿúPä­²®°$)£	JSÔCCM1@ i	M&&B \0\0\0\0\0SD¦i\Zz@4hh\0 Ì\0\0\0\0\0\0\0\0!4M\0Ñ¢¨Ú¨h?Q=C©\00Hú¦ä½#ÔLgî1¨?©ÇnÒ2`±×Èb8|ItCe¾µÞEløÞqu2lfqérÑwã\n¶·¶jáîLuPHÛSQCLê\\LÀå°aÈÚ¶ÍÜc¼¦ô$8C04\Z	ø\'Q+Åà4¬$¼w®Ñ¥hÏ9µLhF Ó«jôÍ=FSæA§Ö°h6C¡\0K7quG`ipÆ°j²u¶Él\\Æ¤@©ê`§Î ¬b+ I ÐxibIR±tÆÖ/îòQÒBAÉÉ2Qä08ÎKofYÌÎ­¯¦âbãQ¬4Ú\ZY	hghºLøÆLÖjlL[ÃEB\rÓL+WØ\"BÞºI	°M lEH³ßr \"Q¬(Zl+Má\rI¤4	¤4¢ À6®m«TB¹! ùfÙdÈ¤µ½XY*ÔÑ;áV¤«#WICàÀÜSÁB³{Áºr¸·\nèÜ=5O©B7³ÜYÕ]{º. ê2R,ÎÞã3aàXH¡ñ]Þf­¤cÆ/·\'íhÀ_=Õ;NÔ&ÃmCG©¬@P*Å1-tì3âPù6¹]Yq^Æp9\n·Ö2hâ,Sãé½TXè:4Ta®aÐ2Ã5xÃüèI\Z@Í#=p´¢FLJ¼ÄÓ- ÀQpê­.`ÓhÖÒÄ*ì&Ns®Kp{0üÎSr4#¢G]áªåàLÌöy±¨Ð,j£¼NæV§èüÍÅåx¤JCéi!+u$Up×\Z&=Ó.VTÕ²®¿yÛØj-oÈÑZìÅm$eï9ØaBÓ NòóÌg5	ËÄXÑ¬3¹ä6$KMcHBá¤Îü:Á#·HÚxu¼$IM­VDÆw$n	`ìpY2-³XwfÀ\'Ë2aÐâ1GQVjUm_Q`É`J+ræ+µªÁS	`¥PÄÃ7´Ë PZXU©È@ý°ÈZö¡L´JÓÔjÛiè5w»\\sÔp(\"å8£ý8ÈiûõFµCÒó5¸vê7çº²Dlï ¬(&Ë%¦\Zd(:+No*sÓã1¼Ùù`{ALCéd¦bLh Àl\0m$¡¤Á6¡ÌØ^uÓÅB)µ,ÅEÌ¶µ5-	\"dÈÜYkðë´vNmã%Û&cB(Ù¼s¼[Xe^<Bab45Ð»nlkòæ]~£¢È&I©¨µdàKî>;-è\\)0àË\\I\Zk]ÆûÌóBû/$&Á±Cü]ÉáBAÚõºü', NULL, NULL, '10', 'Azampur', '1', 200, NULL, NULL, 'nP4Y1606728467', 'Pending', 'wykos@mailinator.com', 'Shelby Strickland', 'Gabon', '+1 (324) 201-8146', 'Obcaecati quo tempor', 'Ab harum quo et quas', '55483', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Distinctio Voluptat', NULL, NULL, 'pending', '2020-11-30 15:27:47', '2020-11-30 15:27:47', NULL, NULL, '$', 1, 10, 0, 0, 0, NULL, 0, 0),
(58, 108, 'BZh91AY&SYv½n¿\0ïß@\0Xø+ü¿ÿÿúPä­²®°$)£	JSÔCCM1@ i	M&&B \0\0\0\0\0SD¦i\Zz@4hh\0 Ì\0\0\0\0\0\0\0\0!4M\0Ñ¢¨Ú¨h?Q=C©\00Hú¦ä½#ÔLgî1¨?©ÇnÒ2`±×Èb8|ItCe¾µÞEløÞqu2lfqérÑwã\n¶·¶jáîLuPHÛSQCLê\\LÀå°aÈÚ¶ÍÜc¼¦ô$8C04\Z	ø\'Q+Åà4¬$¼w®Ñ¥hÏ9µLhF Ó«jôÍ=FSæA§Ö°h6C¡\0K7quG`ipÆ°j²u¶Él\\Æ¤@©ê`§Î ¬b+ I ÐxibIR±tÆÖ/îòQÒBAÉÉ2Qä08ÎKofYÌÎ­¯¦âbãQ¬4Ú\ZY	hghºLøÆLÖjlL[ÃEB\rÓL+WØ\"BÞºI	°M lEH³ßr \"Q¬(Zl+Má\rI¤4	¤4¢ À6®m«TB¹! ùfÙdÈ¤µ½XY*ÔÑ;áV¤«#WICàÀÜSÁB³{Áºr¸·\nèÜ=5O©B7³ÜYÕ]{º. ê2R,ÎÞã3aàXH¡ñ]Þf­¤cÆ/·\'íhÀ_=Õ;NÔ&ÃmCG©¬@P*Å1-tì3âPù6¹]Yq^Æp9\n·Ö2hâ,Sãé½TXè:4Ta®aÐ2Ã5xÃüèI\Z@Í#=p´¢FLJ¼ÄÓ- ÀQpê­.`ÓhÖÒÄ*ì&Ns®Kp{0üÎSr4#¢G]áªåàLÌöy±¨Ð,j£¼NæV§èüÍÅåx¤JCéi!+u$Up×\Z&=Ó.VTÕ²®¿yÛØj-oÈÑZìÅm$eï9ØaBÓ NòóÌg5	ËÄXÑ¬3¹ä6$KMcHBá¤Îü:Á#·HÚxu¼$IM­VDÆw$n	`ìpY2-³XwfÀ\'Ë2aÐâ1GQVjUm_Q`É`J+ræ+µªÁS	`¥PÄÃ7´Ë PZXU©È@ý°ÈZö¡L´JÓÔjÛiè5w»\\sÔp(\"å8£ý8ÈiûõFµCÒó5¸vê7çº²Dlï ¬(&Ë%¦\Zd(:+No*sÓã1¼Ùù`{ALCéd¦bLh Àl\0m$¡¤Á6¡ÌØ^uÓÅB)µ,ÅEÌ¶µ5-	\"dÈÜYkðë´vNmã%Û&cB(Ù¼s¼[Xe^<Bab45Ð»nlkòæ]~£¢È&I©¨µdàKî>;-è\\)0àË\\I\Zk]ÆûÌóBû/$&Á±Cü]ÉáBAÚõºü', NULL, NULL, '0', 'Azampur', '1', 190, NULL, NULL, 'iBSm1606728515', 'Pending', 'zolitus@mailinator.com', 'Freya Carney', 'Mayotte', '+1 (652) 525-8262', 'Lorem dolorem veniam', 'Asperiores do invent', '15924', 'Oscar Brady', 'East Timor', NULL, '+1 (734) 961-3737', 'Consequatur Distinc', 'Nemo rerum sed adipi', '66687', 'Veniam ducimus qui', NULL, NULL, 'pending', '2020-11-30 15:28:35', '2020-11-30 15:28:35', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(59, 108, 'BZh91AY&SYv½n¿\0ïß@\0Xø+ü¿ÿÿúPä­²®°$)£	JSÔCCM1@ i	M&&B \0\0\0\0\0SD¦i\Zz@4hh\0 Ì\0\0\0\0\0\0\0\0!4M\0Ñ¢¨Ú¨h?Q=C©\00Hú¦ä½#ÔLgî1¨?©ÇnÒ2`±×Èb8|ItCe¾µÞEløÞqu2lfqérÑwã\n¶·¶jáîLuPHÛSQCLê\\LÀå°aÈÚ¶ÍÜc¼¦ô$8C04\Z	ø\'Q+Åà4¬$¼w®Ñ¥hÏ9µLhF Ó«jôÍ=FSæA§Ö°h6C¡\0K7quG`ipÆ°j²u¶Él\\Æ¤@©ê`§Î ¬b+ I ÐxibIR±tÆÖ/îòQÒBAÉÉ2Qä08ÎKofYÌÎ­¯¦âbãQ¬4Ú\ZY	hghºLøÆLÖjlL[ÃEB\rÓL+WØ\"BÞºI	°M lEH³ßr \"Q¬(Zl+Má\rI¤4	¤4¢ À6®m«TB¹! ùfÙdÈ¤µ½XY*ÔÑ;áV¤«#WICàÀÜSÁB³{Áºr¸·\nèÜ=5O©B7³ÜYÕ]{º. ê2R,ÎÞã3aàXH¡ñ]Þf­¤cÆ/·\'íhÀ_=Õ;NÔ&ÃmCG©¬@P*Å1-tì3âPù6¹]Yq^Æp9\n·Ö2hâ,Sãé½TXè:4Ta®aÐ2Ã5xÃüèI\Z@Í#=p´¢FLJ¼ÄÓ- ÀQpê­.`ÓhÖÒÄ*ì&Ns®Kp{0üÎSr4#¢G]áªåàLÌöy±¨Ð,j£¼NæV§èüÍÅåx¤JCéi!+u$Up×\Z&=Ó.VTÕ²®¿yÛØj-oÈÑZìÅm$eï9ØaBÓ NòóÌg5	ËÄXÑ¬3¹ä6$KMcHBá¤Îü:Á#·HÚxu¼$IM­VDÆw$n	`ìpY2-³XwfÀ\'Ë2aÐâ1GQVjUm_Q`É`J+ræ+µªÁS	`¥PÄÃ7´Ë PZXU©È@ý°ÈZö¡L´JÓÔjÛiè5w»\\sÔp(\"å8£ý8ÈiûõFµCÒó5¸vê7çº²Dlï ¬(&Ë%¦\Zd(:+No*sÓã1¼Ùù`{ALCéd¦bLh Àl\0m$¡¤Á6¡ÌØ^uÓÅB)µ,ÅEÌ¶µ5-	\"dÈÜYkðë´vNmã%Û&cB(Ù¼s¼[Xe^<Bab45Ð»nlkòæ]~£¢È&I©¨µdàKî>;-è\\)0àË\\I\Zk]ÆûÌóBû/$&Á±Cü]ÉáBAÚõºü', NULL, NULL, '10', 'Azampur', '1', 200, NULL, NULL, 'HzF11606728788', 'Pending', 'giluca@mailinator.com', 'Felix Joyce', 'Jamaica', '+1 (382) 728-1519', 'Rerum autem voluptat', 'Ut explicabo Totam', '17845', 'Oscar Brady', 'East Timor', NULL, '+1 (734) 961-3737', 'Consequatur Distinc', 'Nemo rerum sed adipi', '66687', 'Pariatur Tempore v', NULL, NULL, 'pending', '2020-11-30 15:33:08', '2020-11-30 15:33:08', NULL, NULL, '$', 1, 10, 0, 0, 0, NULL, 0, 0),
(60, 108, 'BZh91AY&SYv½n¿\0ïß@\0Xø+ü¿ÿÿúPä­²®°$)£	JSÔCCM1@ i	M&&B \0\0\0\0\0SD¦i\Zz@4hh\0 Ì\0\0\0\0\0\0\0\0!4M\0Ñ¢¨Ú¨h?Q=C©\00Hú¦ä½#ÔLgî1¨?©ÇnÒ2`±×Èb8|ItCe¾µÞEløÞqu2lfqérÑwã\n¶·¶jáîLuPHÛSQCLê\\LÀå°aÈÚ¶ÍÜc¼¦ô$8C04\Z	ø\'Q+Åà4¬$¼w®Ñ¥hÏ9µLhF Ó«jôÍ=FSæA§Ö°h6C¡\0K7quG`ipÆ°j²u¶Él\\Æ¤@©ê`§Î ¬b+ I ÐxibIR±tÆÖ/îòQÒBAÉÉ2Qä08ÎKofYÌÎ­¯¦âbãQ¬4Ú\ZY	hghºLøÆLÖjlL[ÃEB\rÓL+WØ\"BÞºI	°M lEH³ßr \"Q¬(Zl+Má\rI¤4	¤4¢ À6®m«TB¹! ùfÙdÈ¤µ½XY*ÔÑ;áV¤«#WICàÀÜSÁB³{Áºr¸·\nèÜ=5O©B7³ÜYÕ]{º. ê2R,ÎÞã3aàXH¡ñ]Þf­¤cÆ/·\'íhÀ_=Õ;NÔ&ÃmCG©¬@P*Å1-tì3âPù6¹]Yq^Æp9\n·Ö2hâ,Sãé½TXè:4Ta®aÐ2Ã5xÃüèI\Z@Í#=p´¢FLJ¼ÄÓ- ÀQpê­.`ÓhÖÒÄ*ì&Ns®Kp{0üÎSr4#¢G]áªåàLÌöy±¨Ð,j£¼NæV§èüÍÅåx¤JCéi!+u$Up×\Z&=Ó.VTÕ²®¿yÛØj-oÈÑZìÅm$eï9ØaBÓ NòóÌg5	ËÄXÑ¬3¹ä6$KMcHBá¤Îü:Á#·HÚxu¼$IM­VDÆw$n	`ìpY2-³XwfÀ\'Ë2aÐâ1GQVjUm_Q`É`J+ræ+µªÁS	`¥PÄÃ7´Ë PZXU©È@ý°ÈZö¡L´JÓÔjÛiè5w»\\sÔp(\"å8£ý8ÈiûõFµCÒó5¸vê7çº²Dlï ¬(&Ë%¦\Zd(:+No*sÓã1¼Ùù`{ALCéd¦bLh Àl\0m$¡¤Á6¡ÌØ^uÓÅB)µ,ÅEÌ¶µ5-	\"dÈÜYkðë´vNmã%Û&cB(Ù¼s¼[Xe^<Bab45Ð»nlkòæ]~£¢È&I©¨µdàKî>;-è\\)0àË\\I\Zk]ÆûÌóBû/$&Á±Cü]ÉáBAÚõºü', NULL, NULL, '10', 'Azampur', '1', 215, NULL, NULL, 'mur51606729164', 'Pending', 'bidoxim@mailinator.com', 'Colette Flores', 'Korea, Republic of', '+1 (314) 307-2805', 'Ea reprehenderit de', 'Sed esse doloremque', '91620', 'Oscar Brady', 'Antigua and Barbuda', NULL, '+1 (734) 961-3737', 'Consequatur Distinc', 'Nemo rerum sed adipi', '66687', 'Veritatis pariatur', NULL, NULL, 'pending', '2020-11-30 15:39:24', '2020-11-30 15:39:24', NULL, NULL, '$', 1, 10, 15, 0, 0, NULL, 0, 0),
(61, 108, 'BZh91AY&SY-)\0Tß@\0Xø+ö¿ÿÿú`Ýðwh8´TÈÌ\0\0\0\0\0\0\0\0Ð&L@\0\0\0\0$ÉPÈ\0\0\r\0\0©©èê\0\r\0\Z=@\r\r\00\0\0\0\0\0\0\0RDLh4h%ª=G\Z(L¤FcDú,6÷ØÌÈ¯Á=LXÉk¤b>IáÔtcÃüD+bXòãÜÂ°ÈìB/ ÌK69@>NÁÂY©ìÍ½ÁÎ·},¥\Z¹4±SÛ÷ªµÓ=¦/m®^,¼\"!è³©\"²LÝÎ/ó¹æómY¦ªÉb¹îm}[æ	8Ê¢L¸Zp¾eweÙV\'»Ràø?jÐnTR#&E%¦êíÒíke1!´i@eIÂ¦)ZRÚó¬x¡h28\ZQ­V´ÖXÕ&	`ÔÍUã6\\[ÍÅnåL\"¤+[c®\rqYàR¦Ã1ª¬Ùe¦««ëã@[f±@I¼ÌWÆnðHÆr@AÈQDÉ$dÁ!¼ÆÁ$ÝR1ÖpaDp»ÍP^Ö+jÚVM×bf«gÐZ¥\nµMDÌ»ÆqbÎJ­S²VV1FÊÕñ]ô_t¥$\0mb@ØxE\nHìhlV(Q62(çR«	*)B¤QJ¡*EHT^Èµ$ªH®ÂÒ[nûr[^2&ý#\Z,ðv$Ò=5+@v	i\'Zó`÷¦íÈáÌ8;¥î¥dPX0Æ*Á 4ºËªb¨EÚVRC\rTCíü^îN:±|Äü×»½£ß&VÑµß½p7î1;Ãð:Pq:ÈQ¨ã#c·L¦áÍ&oÕ6uÙO£ù[<e<6ä¾Ûô~eÖèé:âÂ]Únj¬\\?ËGxãñ>å³u}ü×Ll§>ób¥lnsk}GÉi%ö<ãEg6´ßbÒª;ªK/)GaÝ¦Y<Ü7çbÙ¯nVlÚà¦Õ\'3Ö½\rO/lÆy¸ÖÆ$ÍÆ!¢aÚ´ËÓlÑâÿ­]ÍÌ~¥ZSTÁ{ªÍ/½i4Sà»)Ó²Î:8ªÊ`­1ß~»2oíÇ}»¼ñS¾ÿÝO\ZMy´­ç½]pqØÉèa£CÊo¢êtýîá6T³òø*¤âôÍÊ)I-fÕIOFÞ¶]Ñ;.ì{3Yë¸ºó\nmú³Ùb§Ùwq~1?ÓÂuU(ìx³ÙÌüÓ°aåÕæ³¢ÃDÍªÈ×&F·ÂJhEÚ\'ÆvL-ÊË cP0¢TÁQLa°éPÃú{06IdÁVM­%Ö8&µ$í¼HrAÑÏû\'¡kLÑë\ZÛÙÕ}çÄAÁùllèbWÝÑJXêª7UÏn»\nÏQPôÑÖv+-]Ü9exøÞ\']Ë2´Áq²¶_\ZdPá\ZW$!¯!iþMêé±¼Êcc},q\"¦``¢Tª$µIEP»GîûF<äñLg<²j´[2ñ­<8Y¡åáV1ªÍâß¶ù3ÎTå¥\Z_ºØ°g32ß°6F=ÓñPáR§Ó£=5uçªòc1Û<\"ÝëþáÚ°¢ÜÊhÚYGu3¢êjºêá?*=Þß\'X©ooJ¢©-@âÿrE8P-)', NULL, NULL, '0', 'Azampur', '1', 900, NULL, NULL, 'pfTT1606729548', 'Pending', 'DonBhai@gmail.com', 'Don Bhai', 'Afghanistan', '7491067579', 'Don', 'Ut laborum Deleniti', 'DonBhai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-11-30 15:45:48', '2020-11-30 15:45:48', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(62, 108, 'BZh91AY&SYÆo\0Tß@\0Xø+ö¿ÿÿú`÷@=%ÕÆ\0\09\0\0\0\0\0\0\0\ZÉ\02h\0\0\0\0ô*\r\0 \0\0\0\0HTMõ14hhd\r\0ÓÔdÉ¦C\0\0\0\0\0\0\0\" DÐ&4*oRz¥TH$xÍãì Â$±A~Îiú5j¨¹B£%¯aýS³¤{#dÃýâX<Ä¸÷0\",2;#è3ÍaP¢0p2(u3³a[ldqda¡¸Î¯=ÚÏ!é2\n¬yâ ñ è	Ô\\X8BÀêu2 ÏA¦\\c$=C#Øü·&hJû!lIy6ÉwPõ4	\0=È¡¤\"£ÁkSUË94veH?5)`iH2äB\\´^Å² EÀ¸àiF1La« ¨ÕÆ°jf­­ê_p[Q$la6Þ5D4éÒ ¨dgÎE°îK$Á\'biÛ0¸bLÅ¢o	5Rj·¼^µ±#´\rE\n2äT¹bCY$,\nÁbI­Grª£¦ÅÉYx1\0à°±V\rºÃÌ\\!QìÚ­Aâ-6¦ààªÔÀAR¤««ZJª¼×÷_²RM6±\r l,ÈèhlT(RÚn41Ò1¤Ð&Ò#lHx%/òù+¼·RF4YàìI¤zjVíÒN*µ\ræÁîMÛ\rÂpwJ)ÝJÈ *°aU6@iu1TÅP´¬¤\Z©8`}þg©¸æAÔ©õ|?SërñB>Cý´ù8*ô¾wìéAÂvL°Ô8d61_üÌýÍâ¡ø+¼Ã¯ÜgØ©½Ñ¨6r>¦gèNçûä­ªHæs]jQIÀ5\Z¦ÏöfqoÈ?:­Ç(ÜVlöß¢\\ØËÔo4\0ú(ÌxYëÚ;ÚPJcà¾Yît5kÄ`Ý26ÈbÞO°Ôíð\"ñ~þtÈÀäÃó0Q^`mªý²Yý4;ÍE}Âf¡283d%ÏBK®}ÐmÌÚ8Aç]sÖ×5ð®Àµr7¸×ýFx5Ë¡¦ó7më¡Ä}hm±s°S30èµ°ÿ½MÍAðz\r¤ù°nC Þ\r&v)çåÅÝ\'qæ`ËPI*g±a÷$ï	ÚÿK¨ØÃ¸ð1máðÎà)Ó©âAÌ¢NÂ½U.\ZÕPÆ°vIM	Ô¹ßQK,ÕJI1P0R(ÆsÌ Y(ÈÍ0Þ@m¡EÀá),ÐåõÁãø\'²Åì5éº<uÊaðX·0¨?ÁÌc :¥©!õ2ÒÂ£Xx\rAÛ3ªîô;ÝyGÊPºê ¼*\rQ\\iCÒ¸q	f*ú¹ªºÆñ3*=LI÷±Ä11Øm	CI\0l	75qWz^ªð1{0\"-f98\n·À×ÜÆHÌÖ c ,fg=ñRëPø¢¥ÈÍ¯Q{´0Ø×Ûô:®!äJUUQäâO¸ùp ¼@Ã½a#4$¬Q|\rõ<þT/àyùú!6\r8¿ÅÜN$1ÂÛÀ', NULL, NULL, '0', 'Azampur', '1', 915, NULL, NULL, 'YKPR1606729599', 'Pending', 'fyva@mailinator.com', 'Basia Glenn', 'Madagascar', '+1 (156) 639-8141', 'Dolor omnis dolores', 'Quibusdam totam est', '27994', 'Blair Mooney', 'Guinea', NULL, '+1 (519) 737-2717', 'Sequi nesciunt aute', 'Est velit aut eu exc', '98034', 'Quisquam quas expedi', NULL, NULL, 'pending', '2020-11-30 15:46:39', '2020-11-30 15:46:39', NULL, NULL, '$', 1, 0, 15, 0, 0, NULL, 0, 0),
(63, 108, 'BZh91AY&SY²HpV\0Tß@\0Xø+ö¿ÿÿú`ðÒYhÐ\0C\0\0\0\0\0\0\0\0\0\0\0\0\0\0(D)\r\0\0\0\0\0\0TSÓRoTÓj\ZODÓG¨¡\r`\0\0\0\0\0\0\05224DÒ£d\'éG¢zzR!!¸ê8ÙÆHi)?uBÉ××rFK^£û\'gI õ!&ùûÄ*²*õtS»êÈZ©­©Eñµ(§J½/0ÃÊc¸È ¡Ô=æ\råm½ÞÈÃS½üv=Ã×8Ð*sÐãÐ¿4!$3RâÁÜp?ÁÜXê`MÃL¹\0ÆHz©ýU¸&hJûáoIy;$»©ðLø\0èzÔi$Æ¨Åpb`Õªçt;2¤Æ,D!Å©KÛÔ¸\r(Æ)5d\Z¢`Ð6\rLÒµ½ñn$ã,3;¨=bºÐÖ¤¬ù¨¢vÂÔ´@Âv(¡F]²¬I´Mñ&Åà­ï­lHÆp qB¹$.XØ`±$Örª£­$µ)¶x²ApXX«XÃb g.°ö2j2¼¦v¥kPx´ÀÖ¸8)TÀAR¤««Zª½jBú/ªRM, °>vS\\)X)0Æ(C[\" 20# È&ClH}	GM£\ZðHYH~cÌªlðÔÏ}\n´Xh0°ÒdÉ¢ó&à±u#É-Ö×°ðg2fÖXnLÐ(Ê35\"¬\\¦(Ìäg%ñK£;¡BäeX`}?Áèq:u*|_wèw=.^2hPþºn<ùÎ¿»W+#c±2ZD³½&}B¡ù+ÈÃ¯ÐgÌ©ÜcPoæ|9ý3ØÁI­J);Cq¨êoÿNð8|CìªÞ0ûy%`fñ>X*%ÑÈ¹}Ç#YÔÄØòFG¡ !6æÃ°/¦{&ÇÛb0LqÉÆh1rsó\Z^/ÝÒXc2`ø(¯06ÕE=Oø5;Å}AÂf¢¡283;F~]; áàeí=msnÚï	Ów3ûwÏì3Á®~&¼»r^\'xúÐábçS&CÅlÂFyyÉÐpÌNãY ³3X\ZÃ|bgOº	:²NÃÞ`Ý¸$F³§©aô$î	àÿNk¨ØÃ°ð1nA÷g`ñêyt ahÌWª¥À£[°*ØSBw.d÷TgyeR¤Lb%L1¢ ÃØ÷% àZL9ñ(»Iñl¤²(d¨/±>k\ZfÖòØèTDÏ¹bÝ þÇAê6¤ÈÓ[\n\rÀl\Z<W`ï©ØvÓåBë¸ð¨H)\ZV%ô2ðÚj6iÏÉ\n.ÄéÐ\"HìFÈP@b\"E 	+\"ÄwvÚÒðWÜÔP¡¹ï!ãÍÀU¼ÚMÌa4(È3=ÑRëpûÑRde¯A{4ðØ×Ï¡êu]áî%*ª¨ÑsDwì>}¤\0Â92h0îfHÏÜ0Ç Ûí9!?¡nÞhE%A³þ.äp¡!dà¬', NULL, NULL, '0', 'Azampur', '1', 900, NULL, NULL, 'F1Jj1606729826', 'Pending', 'mahmedsaleem60@gmail.com', 'Ahmed', 'Pakistan', '03152475697', 'lyari', 'karachi', '12345', 'Ahmed', 'Pakistan', NULL, '031234567890', 'lyari ka Don', 'karachi', '12345', NULL, NULL, NULL, 'pending', '2020-11-30 15:50:26', '2020-11-30 15:50:26', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(64, 108, 'BZh91AY&SYb\ZR,\0Tß@\0Xø+ö¿ÿÿú`÷@=%ÕÆ\0\09\0\0\0\0\0\0\0\ZÉ\02h\0\0\0\0ô*\r\0 \0\0\0\0HTMõ14hhd\r\0ÓÔdÉ¦C\0\0\0\0\0\0\0\" DÐ&ýRz¤éåH	GÞ>Ê\"@kàÌè£Vª*2ZõÕ;:I¨É	26L?æ!UcÈlKsR\"Ã#²1¾1,Øæ\0ù:# Pô0k6¶ÆG@Æ\Zà\ZôÝ¬óã ©·n(BÍEÀàl>©ÔÁZý°H2(3Ôì¨%¹4BXÛlÒKÉÂKºâk	\0?±\rCI&06EF+Ö¦«rhìÊBW\ZP±+¥/lPKip .8\ZQSjÈ*5DÁ1 l\Z¥+kv/¸-¨6°ÌHï\Z¢\Z@ôéCJT23ç\"Øw	¥J`±E\n4Âí\\\n±&bÑ7ÄËÁ)5[Þ/ZØÚA¢rH*\\±!¬E`±$Ö£¹UQÇSbÍJd¬¼JApXX«IÄ@Î]aæ.É¨ÊöLíJÖ ñ@ÓppUj` ©GRUÕ­C%U^kBû/á)I&À@ØÐ6E\ndtB46*)M\rí7DÀiÒhHiDÁ6$<ÍÍù|ÞH[©#\Z,ðv$Ò=5+@v	i\'Zó`÷¦íÈáÌ8;¥î¥dPX0Æ*Á 4ºËªb¨EÚVRC\rT0>ß#±¸æAÔ©ô{¾Ç3µËÆfEøøÓäxà«Ð2ù_ÉÒía¨pÈlb¾#ûûÄCôWy_°Ï©R\r{£Plä}ÏÈÿßù=[TÌæºÔ¢j4M±Ä\r¿û`ê¶>þ$£qX°g×~\ns`o._Q¼ÒtæBJlx£1ádg®hïiA);ùd¹ÐÖn-¯ctLÈØ3!xy¿!©ÛàEâýüéÉ33â`¢¼0ÀÛUü²Yý4;ÍE}AÂf¡283d%Ï%×>è6æm ó®¹ëkøW`NZ¹äkþ£<\ZåÐÓy¶õÐâ>´6Ø¹äÌÌ:-l$g?À]ÍAî|Ò@|O,Æ%7CIE=<à»¤î=yê	%Q¬òõ1l\"£>Äá;P¿sê60î<[x{³¸\ntêxs ahÈWª¥À£Z°*ÖÉ)¡:\"{ê3e©R	&1\n¦\nEÃ!ÑPaìz% àY¦È\r¡ó(¸Ð\\%%¡^¢X<~äù,Xi>£Y~[£ÇYÌ¨6ås\nûÆ2¨ÚX:C-,*05Ø4YWpï¡Üp¦ëÊ>2×QáPlªãLÆÃ0H[1WÕÍUÖ7QìbbLo½$@`4Á&ÄhJ\ZL`I¸Ìé«\"»ÒðWÜÐP¡¹l0éÉÀU¼¼¦æ0Ffµc39ï0°ZÅ(Fmv³A³\r}yÏCªâd¥UU.H$û\0aÁ;Ù3BI\ZÅ¸ÑNÇ§ÌêýOO`Ø¡ü]ÉáBAiH°', NULL, NULL, '0', 'Azampur', '1', 900, NULL, NULL, 'WSOu1606738212', 'Pending', 'xawy@mailinator.com', 'Kiona Church', 'Aruba', '+1 (419) 564-2383', 'In dolore suscipit e', 'Sunt laboris vero o', '94577', 'Kristen Steele', 'Portugal', NULL, '+1 (578) 453-7047', 'Dolor ut pariatur A', 'Officia quam repelle', '70507', 'Distinctio Qui blan', NULL, NULL, 'pending', '2020-11-30 18:10:12', '2020-11-30 18:10:12', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(65, NULL, 'BZh91AY&SYÛÁBè\0ßP\0Xø+ü¿ÿÿúPiÑ-th Ó ¤Ò£ \Z\Z\0 4\0\0%H)M\04Ä\0\0hS)êÔ7ª1¤ÓÍI  \0¡Ì\0\00\0\0\0\0H6&FêhLMAp\"³Y\0w` §&w9û¨\\¡a×´b6öwY Ü2BLÏ|ÃøÄ$ä\'âzié}V¡1ôêtXÃíîNË»¥Ë­ð³-BM|fkâr\\ªÌdNÝæ gPHf¦fñÒ@ÐÄ]#¦T[}\ZMCJ\"!jHjÍV¶îÌc440±JRÖ.ÏXÕ0cKÅ1k%Qª&	`Õ&­­?iï2$ù]®@óÒt2¦L xvÂ\"nª® jv&)d4¸ã8ª%)$	!(I`ÌA¢Á$ÁdH\ZÁ£/l]gùÜáÖ*ejµ&Ói!ÍiídSCVnÚ&d+Å­Æt\nYh E:%²×ta»|Çº£÷+ÈHÆ-U2 ¨!EQ@ÎT±H-BT\0@\'|Ëî¦É Iî1Öö©6|&åj^¦«B³{ë>/%k2W	áp^3%ÄdÞäÚÝ@ûÁ3ANS²+V³:§ÞÉfj×,´xTwæK1ÏçÌìq<T÷\nð\ZÍ/5+ÌO×oÒsñ âéÇDµI-¬B!ÃÅ|Gæl *?Â»uøöæ@Tc_xU²Hm)MãRh±¦Ã¸u8P:©S²Ìaòð%ÀÍ~ðTK£Råô57Nài(©ÍëL\Zm4¶cZÍ\rKnÄ`Ö.¦c?ÄM¤rãÌ¬V3ðâúò2uÈÚ \\zÝ`þ©è4*5ê Ü*Áï!,Æy]tãÙPy×|öµÍü«¨NY\Zs=Çô5Ü0?/=§¿fíÉwxw68ÔÁé\nhA s» X{eãå8\Z*FòBHp\r&yõz`¨¹IÄõ ôèJ£YåzÝî$äªÄî]ãg¡pù@§^òØè1G¯E7iqP¹TI9®d÷TeV\nI,\"ò0¡*`¤QäU2¤R ¤p,,2V½EÃK49d¨Ä~ÆÛG¦FÆ#òUXéRøtHy\"ÐÓE3M*·Q°h<2;.#¾8ÞQÔÒ;hAxPQÐÓ!Aâ4°z\n(ãª0sw7mÕ²sC¤xBuB\0@Eh \"­EÄPÑ\'3¶Ô\"¼RïWyiÈ@A¢¥ô®¼ Ü\\[ÈìmÜÆHÐÙ@Æ@XÌÎzEJX­Õ(FmzÀÿZZá±¯gcî<WP JMQFTGq?èùò\0@F8Üf)w3&PÞP¬ª¸-ÔmÚôß»z2P%?ñw$S	\r¼.', NULL, NULL, '0', 'Azampur', '1', 265, NULL, NULL, '6tQR1606896333', 'Pending', 'Hostndomainreseller@gmail.com', 'Darrel Wilson', 'United States', '7033680454', '3180, Golf Course Drive,', 'Manassas', '22110', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-02 14:05:33', '2020-12-02 14:05:33', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(66, NULL, 'BZh91AY&SY$tÿz\08_@\0Xø+ô¿ÿÿú`\\ø\0á\0B\0\0\0\0\0\0\0\0JMÐ =A \0\0\0\0\0\0\0\0\0\0BOP4\Z\Z\r\r\r\0\0\0\0\0\0\0\0\0H\Z&4Å4z¦¡¡ \Z¦OSÊ@H¨$uÜÜP_c3¢M\Z-.*X2yDÎK](CÌe¡ëHôÈ¬d±ït§wÑ£,o½E#Ú©Nzb`?GªcÀÈ;óbæZÙ¤HÓ8n{ñÔüïÝ¬2µõ/ÍAØØ\0Íã=®42ÊWe|B%á­¦xî ¹ò/ ÒAc1(Ön­dÝrKÜd\r&\r6^-UJË(h.+â«jâ°Æ2Æ°jfª×½è<!p¸Ë¡¤OÞ5eS,ÚÅbª°Ê(J\Z\"j­C,)bL«d6ÐÕ±Å­©\'($Æ\ZBÐ¸J¥ÈÉ¬áN]±TÍiZJÁ¢ÒÖ3Z<[!eJfôk_°±aR«!v%a^ôh¬­Ñ~iü¯¢R\0mb@ØxÅ\nÈéÒl\n(U6294Ý\0ØÁ1&Ð&ÒÈ J!´¦ÑäFþ)TõÎ|;%ÖL­)Ð¤Ë=Á{ë¥\'2*j¾ V\ZD!H¥XZ»4¦¢^3 è^| qíV³¡{Ô<JØ%V&\"Í(>) QÎ%üExJ-Ë	Î\'¶M&L]4ãu¨âvÅ¤QÈ¢°Nü\"¡qôW	s³@ÎÒÂ\ZÛ3\ZøÂó¸¥ùü²ePÆòÇaJO@ÜoÀáómXÔöLaúu%?P¸F&v8NÀÒQc²4M4Ú94¿yäau`êlr/Ã0cLÐâ3A£Íìgð\ZèF#ý©\r.eÔÐÉJðÃ#mP±ßE©ì~¦ãÐÜXi$ÈàÒw=É0½| á©Àp2­·ÏkàÛ¸é»ýø5øÏ÷Õ®ü]ù.gnôq¹â©¨v[0Þó8+µÜð6ñ=ÿ3CÄ¥j4ñ+ÇÊ	:ÒOCÁàU5®¾QarNa<P¾EÜläyM1ævz]»\rH^$÷,«\0S[²*2Y%N«¡<ì2Ë* c0¢TÁQLaÀt¨aI|E#ed\\H©+Qyy¤µC2XK\'©?3\'ä5§òòÛ¥AÀû/Ð,êtÈ£idðHM÷06£`Ð|5;¯1ãyæzW%ºP»î Ä*$/;á¦BÜidæAA¶ÿïÁövdg11o¥$@`4Ál\0m	F$Ø0îÅnIuS8Á¼QDBéÂ\rC·Gfòu6Òpg)¤jl c .jk<âÅY-ÃõE£V¾bû42Ø×åìg]çuê*[ÉÑz¢?ÑêWØ}=-9²îFb­ªþ\Z+ËðdÉ	°lQü]ÉáB@Óýè', NULL, NULL, '0', 'Azampur', '1', 795, NULL, NULL, 'CELN1606999555', 'Pending', 'devmrmsoft@gmail.com', 'MAJID KHAN', 'United States', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'Houston', '77036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-03 18:45:55', '2020-12-03 18:45:55', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(67, NULL, 'BZh91AY&SYðú:\0Tß@\0Xø+ö¿ÿÿú`÷A Ó-V(À\0\0\0\0\0\0\0\r4SÒI  \0\0\0\0À\0\0\0\0\0\0\0	4©ªx¤z\Z&¦PC@i¡Ì\0\0\0\0\0\0\0\0&¦4h)7ª=FÉ\Z4ý(Èõ?JPÀHn:NÙü2<D~IIý¦ûþ®¬Ë*2Z÷Õ;:I¸É	47L?Þ!Uç!d¶>ä*P!N\ZÉjsSeäÝ22(vSÓq[ndtda©ÀÌ^¼6×8Ð*{hpð/Þ ó ì	Ô¸°s7À¡´9G¡u\ZeÈ2C`h{ ôÐ\r	_t-ÓI/\').êÆ¡ à~$ 6\r$ÀÜ1®L[\Z®WfT¥Af[37®xÆëOI 4\r(Æ)5d\Z¢`Ð6\rLÒµ½ðl$Ã,3ò9Q\r zÅu¡­H*ÖQ¡°¼Z¥/R¥ÕU¢kj[d¸bL½bm}BÓJM[V¶$c7@A©B¹$.XÉ$-\nÁbI­A\"Í`Ai«Ò^Yzp\rMVIî&b;×UE\nµQ3 ë[^Õð)**QÔukPÑUWªîH_Uû%)$Ø`)¡ò²µÊ)LEp±q#@dFFdÀbMBQÀÓlp#]é)Ðb¢K¢è+#ÈÔ/}3â0«ÆÉ-×°ðg2fÖXnLÐ(Ê3.±!ª-LEîÜN\r(/¥ ÂBk$¯ô=RÅOîýOk#ä?Û]ÇSyØû´äq±ÎØ­J¢M5½kàâ *?ñ^&~£>H6ðUA»¼ù?2sþ?Cà½IN«µJ);a¨ênÿFNoùØ#e¸aöò%\nÀÍÃ>pTK«rû&³¨	)±äC;`!6m(%1 ¾îxN¶â0LpÉh1qGæ5;ü¼_Zh`rAaÉø(¯06ÕE=Oþ\ZÍ}ÁÂf¢¡283;HK#?K®¼ ßxàeíÖ¹·º»tÙÞp?q®úðk¿Ä×n+Äè>Ô7Ø¹æÉñ[XHÎ¼Äç8&l§aÊ,QÉ©Þ$|7ÆFyõôN¢å\'#ÔÁ¦ÀU\ZÎæ-TgÔNô/öw®ÃcGq»9OÇR<Åzª\\\n5³¡ ì±wÎ£:YU*A$Æ aBTÁH£h:*>Rd TC)x~Eq>\"îPåõÁåö\'ÍbÃLÁô\ZÓóá[N¥DA¸û-Ô*ìuÈÃi`ìH|Í5°¨ÀÚ`Ðyäì¹úêp¼£å(]¶^	3ËebYO8É©á)NM-Üyð\"ñ\'D>`D_Z6BD`\r¡(i0`\r\'\'ÎñKÀU^/sRFQBBïÝCÇ½ÀU¼Ý&æ0FMª2ÆLÏ8©Cu°}Rde¯a|4°Ø×Ó©êv]Ðªª£EÞèOÀûûqdÐ aÍa\"l22®\'`ÃæßaÇ	Ö6íåPRT?âîH§\nBG@', NULL, NULL, '10', 'Azampur', '1', 910, NULL, NULL, 'qlXh1607030065', 'Pending', 'hello@wirelesswavestx.com', 'MAJID KHAN', 'United States', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'Houston', '77036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-04 03:14:25', '2020-12-04 03:14:25', NULL, NULL, '$', 1, 10, 0, 0, 0, NULL, 0, 0),
(68, 108, 'BZh91AY&SY(Þ2\0þ_@\0Pø+ô¿ÿÿúPò#lÖÚh;ª[MCMM3\"d`C\0&iÔÂQ4ÈÔ¡´Ôh\0\0\0\0Ð¨ÔM1=M)¦@Æ@\0@\0\0\0\0HhÐSS56¦ÐTò$ýPô¤\"@HØy\0\"ýZÀc\\:FH|ÄØññ£b-xGî<u$$$ßù}\"²-ôt§s#sW#émÖ©Nö÷ÔÀ{ß	àÆ&ì7Ð¡²VªJbPº»È-¡Ý®df ñ&PcÊjv9^BhK°×|L%Ï{I\nhbCZÔ-kRZxÏ8ô4»Tm\ZªÍ¾²°CÒÍjµ¬m bÁ1 l\Zª¼Ï²h!¤\"ò¼Ü(ÂÆp2X6µUm\"±-WWb$Ë¹%DÝÅÝÁ=D}¥@ÍAf$9¢#£ð¥ËEî\Z­u{Ö ÷pP¬ÖX±).â)zÊÙîJ%oEJ30+¼ZK@ar«pÊvJÙfQ¥jüx$/ØRI6±\r «PTC!¤Ú&Æ@Ã¹¦éIÀcMi\r hAaPÁ\r y½¹fä&Ðo\n)ìõ(´ÅañB\ZÎMe Ã%Èi³sÁI(ç ÍA³0{q\'f@øY4S%qd×8hß#}3gf«ü½£V;ÔD²óêy\Zu&gò\n|G¡\"Û\\Þ?w>³¦Å¾oÃ^TD4Óy°C¶Ya\0ÉÂÆñ.¯è)é ¦l\Za(è>Gr0ý{ä*DTÑzÌ±Hà\r_*Xh¸{üÄ£¡p3]ìÀê3F¹:\' 4LÑ¦Q``Á¦ÑK ¶ûÂs8\rÅ»i% µx10%ÿù\rKd]KhÙ³ÌNH+En%·~LQM#\Zf\")å°¯ÐgðXQi¾\rØ,3Û-m¸¯	î	_¿A\Z5îc{»%Àå­m.:¬$3am^gøy>A´m$07¡J\0cà&t,íÖ	\Z9vÄ$IXÖÝ;`Î§ÁaÄ#âÔú1,gsÔùeû	ÀL¦cÆ¡jè\nk\n\nÒQ$ð¼Äý>s1rp°²	(©ÞYJ ¦|ÄR¡%è)¼Èó$±æ/ÒX!É]1*¾ÄºªZ4ÊÆ¯ïS)s©Ìâ\\\"FG±n÷4#\ZcfÄØÙSmÉj¡¸\Zx¯¡±ÜLu7;WvÒG±ôt Ô()=a¦B¨Ò¡âAã]í«ÕÈ?`cÆý1cAI±\0Ø%\r&I±205ÇÆ?¹\nkK®6(ID.5\0×t<·ÊâCHàê c 0àâ{El¶\r.GäE\ZÒS¶5¯	,ÖÌjQQPPNIPèÞ@!\\°àË\\f%¯)jø\Z\'ø|¾À§N¨Mbþ.äp¡!.Q¼d', NULL, NULL, '0', 'Azampur', '1', 750, NULL, NULL, '4T7a1607331364', 'Pending', 'admin@gmail.com', 'Ubanto', 'Pakistan', '123456789012', 'test', 'test', '123', 'Ubanto', 'Pakistan', NULL, '123456789012', 'test', 'test', '123', NULL, NULL, NULL, 'pending', '2020-12-07 14:56:04', '2020-12-07 14:56:04', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(69, 108, 'BZh91AY&SY(Þ2\0þ_@\0Pø+ô¿ÿÿúPò#lÖÚh;ª[MCMM3\"d`C\0&iÔÂQ4ÈÔ¡´Ôh\0\0\0\0Ð¨ÔM1=M)¦@Æ@\0@\0\0\0\0HhÐSS56¦ÐTò$ýPô¤\"@HØy\0\"ýZÀc\\:FH|ÄØññ£b-xGî<u$$$ßù}\"²-ôt§s#sW#émÖ©Nö÷ÔÀ{ß	àÆ&ì7Ð¡²VªJbPº»È-¡Ý®df ñ&PcÊjv9^BhK°×|L%Ï{I\nhbCZÔ-kRZxÏ8ô4»Tm\ZªÍ¾²°CÒÍjµ¬m bÁ1 l\Zª¼Ï²h!¤\"ò¼Ü(ÂÆp2X6µUm\"±-WWb$Ë¹%DÝÅÝÁ=D}¥@ÍAf$9¢#£ð¥ËEî\Z­u{Ö ÷pP¬ÖX±).â)zÊÙîJ%oEJ30+¼ZK@ar«pÊvJÙfQ¥jüx$/ØRI6±\r «PTC!¤Ú&Æ@Ã¹¦éIÀcMi\r hAaPÁ\r y½¹fä&Ðo\n)ìõ(´ÅañB\ZÎMe Ã%Èi³sÁI(ç ÍA³0{q\'f@øY4S%qd×8hß#}3gf«ü½£V;ÔD²óêy\Zu&gò\n|G¡\"Û\\Þ?w>³¦Å¾oÃ^TD4Óy°C¶Ya\0ÉÂÆñ.¯è)é ¦l\Za(è>Gr0ý{ä*DTÑzÌ±Hà\r_*Xh¸{üÄ£¡p3]ìÀê3F¹:\' 4LÑ¦Q``Á¦ÑK ¶ûÂs8\rÅ»i% µx10%ÿù\rKd]KhÙ³ÌNH+En%·~LQM#\Zf\")å°¯ÐgðXQi¾\rØ,3Û-m¸¯	î	_¿A\Z5îc{»%Àå­m.:¬$3am^gøy>A´m$07¡J\0cà&t,íÖ	\Z9vÄ$IXÖÝ;`Î§ÁaÄ#âÔú1,gsÔùeû	ÀL¦cÆ¡jè\nk\n\nÒQ$ð¼Äý>s1rp°²	(©ÞYJ ¦|ÄR¡%è)¼Èó$±æ/ÒX!É]1*¾ÄºªZ4ÊÆ¯ïS)s©Ìâ\\\"FG±n÷4#\ZcfÄØÙSmÉj¡¸\Zx¯¡±ÜLu7;WvÒG±ôt Ô()=a¦B¨Ò¡âAã]í«ÕÈ?`cÆý1cAI±\0Ø%\r&I±205ÇÆ?¹\nkK®6(ID.5\0×t<·ÊâCHàê c 0àâ{El¶\r.GäE\ZÒS¶5¯	,ÖÌjQQPPNIPèÞ@!\\°àË\\f%¯)jø\Z\'ø|¾À§N¨Mbþ.äp¡!.Q¼d', NULL, NULL, '0', 'Azampur', '1', 765, NULL, NULL, 'g0yJ1607334923', 'Pending', 'admin@gmail.com', 'Ubanto', 'Andorra', '123456789012', 'test', 'test', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-07 15:55:23', '2020-12-07 15:55:23', NULL, NULL, '$', 1, 0, 15, 0, 0, NULL, 0, 0),
(70, NULL, 'BZh91AY&SYÿ\0ößP\0Xø+þ¿ÿÿúPòQA+k\Z% Põ4\0\Z\0Ð4\0\0\0iÀÀ\0&\0`D©½SÊ1M \0M¨Ú æ\0L\0L\0`\0&	 ÔôÔdÍM\0\04zR##ï?ÃÈyRðÏÔcPôÑàw÷Ñ²ÌM~Ãæôo(Qtýê?	2cêíWÔÉËðë«ñìÝªç;¨Ëèí6 Ûcq\\Ä¡ÙReM2:wSM9íB@lA¨$3#3©àr,Ã©rbY	¡yÈ»î¸fCº$È1ÀgB$´\r0\ZÓ\\fèáé£4*CC\nu)Ö²³pv±»^ó½êh1LcL\ZÁ«Ö^·ª>Q£Ä5X4Dï¥\\ápÎ ÍC	{ª½ÙÞ#0Éw¥IR¯?CÊ@Ã¬Þæfggi¦  Ì±2bH%IÊ¦_|Ú³§ÎVÞnì2ø¾o6r¹Ì8¾^óÅj+ãVT3[ÖXÑ¾RÎ[Âµg+uzBí^¡RBlhR l?8 Û\rªP´Ù*;g!cM&Ð&Ò#lØ>èoÎHX$$ÆiN5Æµ++XÆx©9;RnÂóÊ£ eX+kÈ¬:QÓ!§}6UBËdgMý¢Îxã1ÄcB¶<#\nÃ%\"¢ý¾§Àó$PúDü¨9O%\r%Wh#,¼r Å&ÚÑhME¡ªq¥¾\'Ôtì\Z?²û\r¼ôû!è(ÇÈEëÁ»YìbQF#pÔaT9CÃÌ5ySeÃåÔ3)7øér\Z°7-¼Ý-À0\"D~&$]`bÁ¦ÑÅ¥©Ä-!k7Êçx¹(ßVf&=£Ö@j¼~RnoÉî^ù£AU°mØUÚÊçø|Ã\" 8LÜ)àÆY#=.`ß¼p2cÆËjØÏ4	a3ò5F»u1ÞuÞ¹5ï6©¥á<¹hÂC:xJ5â|ç©ä6$}ÎcHC¤Îò~>Hî)ä)µ¥1¤À_ÁÍl60ã©zðVq}Û§A\"<i©X	µ2Ù¨,2]´Ed(¦t0²Bå³yÌLÂ¬¢Â©@¦8 zQÜ.=b&Aj%s¯¡.õz2çÀkë#3¡ Ðõ)ª=\rHØi <Ì² kpj6ïÞbl¸û§ÆÒDæÙZeeº\Zd(<ÎÂ9D;·ym]ßs=ç¸¸ÄÄ1¿{ 11 °´%\ZLjÎ&G]Ý\"ØT[µÈ@A	\"dÈì	-xAuÕÀQ¼\r1Ý4dLLeÒ(Lº¸Yd=QBaTb×´ùlkéØ¾;õÐ&I¥\n0Z¢9ÿÓ°Bâfl»32d¯í*½FHëÕÐy{<Å/ü]ÉáBBN<ü', NULL, NULL, '10', 'Azampur', '1', 285, NULL, NULL, 'r1Ag1608569592', 'Pending', 'mlwqman40@gmail.com', 'Mhamad Lwqman', 'United States', '+9647714553060', '2410 Cruzen St. Suite S592', 'Nashville', '37211', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-21 22:53:12', '2020-12-21 22:53:12', NULL, NULL, '$', 1, 10, 0, 0, 0, NULL, 0, 0),
(71, NULL, 'BZh91AY&SYÿ\0ößP\0Xø+þ¿ÿÿúPòQA+k\Z% Põ4\0\Z\0Ð4\0\0\0iÀÀ\0&\0`D©½SÊ1M \0M¨Ú æ\0L\0L\0`\0&	 ÔôÔdÍM\0\04zR##ï?ÃÈyRðÏÔcPôÑàw÷Ñ²ÌM~Ãæôo(Qtýê?	2cêíWÔÉËðë«ñìÝªç;¨Ëèí6 Ûcq\\Ä¡ÙReM2:wSM9íB@lA¨$3#3©àr,Ã©rbY	¡yÈ»î¸fCº$È1ÀgB$´\r0\ZÓ\\fèáé£4*CC\nu)Ö²³pv±»^ó½êh1LcL\ZÁ«Ö^·ª>Q£Ä5X4Dï¥\\ápÎ ÍC	{ª½ÙÞ#0Éw¥IR¯?CÊ@Ã¬Þæfggi¦  Ì±2bH%IÊ¦_|Ú³§ÎVÞnì2ø¾o6r¹Ì8¾^óÅj+ãVT3[ÖXÑ¾RÎ[Âµg+uzBí^¡RBlhR l?8 Û\rªP´Ù*;g!cM&Ð&Ò#lØ>èoÎHX$$ÆiN5Æµ++XÆx©9;RnÂóÊ£ eX+kÈ¬:QÓ!§}6UBËdgMý¢Îxã1ÄcB¶<#\nÃ%\"¢ý¾§Àó$PúDü¨9O%\r%Wh#,¼r Å&ÚÑhME¡ªq¥¾\'Ôtì\Z?²û\r¼ôû!è(ÇÈEëÁ»YìbQF#pÔaT9CÃÌ5ySeÃåÔ3)7øér\Z°7-¼Ý-À0\"D~&$]`bÁ¦ÑÅ¥©Ä-!k7Êçx¹(ßVf&=£Ö@j¼~RnoÉî^ù£AU°mØUÚÊçø|Ã\" 8LÜ)àÆY#=.`ß¼p2cÆËjØÏ4	a3ò5F»u1ÞuÞ¹5ï6©¥á<¹hÂC:xJ5â|ç©ä6$}ÎcHC¤Îò~>Hî)ä)µ¥1¤À_ÁÍl60ã©zðVq}Û§A\"<i©X	µ2Ù¨,2]´Ed(¦t0²Bå³yÌLÂ¬¢Â©@¦8 zQÜ.=b&Aj%s¯¡.õz2çÀkë#3¡ Ðõ)ª=\rHØi <Ì² kpj6ïÞbl¸û§ÆÒDæÙZeeº\Zd(<ÎÂ9D;·ym]ßs=ç¸¸ÄÄ1¿{ 11 °´%\ZLjÎ&G]Ý\"ØT[µÈ@A	\"dÈì	-xAuÕÀQ¼\r1Ý4dLLeÒ(Lº¸Yd=QBaTb×´ùlkéØ¾;õÐ&I¥\n0Z¢9ÿÓ°Bâfl»32d¯í*½FHëÕÐy{<Å/ü]ÉáBBN<ü', NULL, NULL, '10', 'Azampur', '1', 275, NULL, NULL, 'mZV31608569616', 'Pending', 'mlwqman40@gmail.com', 'Mhamad Lwqman', 'United States', '+9647714553060', '2410 Cruzen St. Suite S592', 'Nashville', '37211', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-21 22:53:36', '2020-12-21 22:53:36', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(72, NULL, 'BZh91AY&SYÿ\0ößP\0Xø+þ¿ÿÿúPòQA+k\Z% Põ4\0\Z\0Ð4\0\0\0iÀÀ\0&\0`D©½SÊ1M \0M¨Ú æ\0L\0L\0`\0&	 ÔôÔdÍM\0\04zR##ï?ÃÈyRðÏÔcPôÑàw÷Ñ²ÌM~Ãæôo(Qtýê?	2cêíWÔÉËðë«ñìÝªç;¨Ëèí6 Ûcq\\Ä¡ÙReM2:wSM9íB@lA¨$3#3©àr,Ã©rbY	¡yÈ»î¸fCº$È1ÀgB$´\r0\ZÓ\\fèáé£4*CC\nu)Ö²³pv±»^ó½êh1LcL\ZÁ«Ö^·ª>Q£Ä5X4Dï¥\\ápÎ ÍC	{ª½ÙÞ#0Éw¥IR¯?CÊ@Ã¬Þæfggi¦  Ì±2bH%IÊ¦_|Ú³§ÎVÞnì2ø¾o6r¹Ì8¾^óÅj+ãVT3[ÖXÑ¾RÎ[Âµg+uzBí^¡RBlhR l?8 Û\rªP´Ù*;g!cM&Ð&Ò#lØ>èoÎHX$$ÆiN5Æµ++XÆx©9;RnÂóÊ£ eX+kÈ¬:QÓ!§}6UBËdgMý¢Îxã1ÄcB¶<#\nÃ%\"¢ý¾§Àó$PúDü¨9O%\r%Wh#,¼r Å&ÚÑhME¡ªq¥¾\'Ôtì\Z?²û\r¼ôû!è(ÇÈEëÁ»YìbQF#pÔaT9CÃÌ5ySeÃåÔ3)7øér\Z°7-¼Ý-À0\"D~&$]`bÁ¦ÑÅ¥©Ä-!k7Êçx¹(ßVf&=£Ö@j¼~RnoÉî^ù£AU°mØUÚÊçø|Ã\" 8LÜ)àÆY#=.`ß¼p2cÆËjØÏ4	a3ò5F»u1ÞuÞ¹5ï6©¥á<¹hÂC:xJ5â|ç©ä6$}ÎcHC¤Îò~>Hî)ä)µ¥1¤À_ÁÍl60ã©zðVq}Û§A\"<i©X	µ2Ù¨,2]´Ed(¦t0²Bå³yÌLÂ¬¢Â©@¦8 zQÜ.=b&Aj%s¯¡.õz2çÀkë#3¡ Ðõ)ª=\rHØi <Ì² kpj6ïÞbl¸û§ÆÒDæÙZeeº\Zd(<ÎÂ9D;·ym]ßs=ç¸¸ÄÄ1¿{ 11 °´%\ZLjÎ&G]Ý\"ØT[µÈ@A	\"dÈì	-xAuÕÀQ¼\r1Ý4dLLeÒ(Lº¸Yd=QBaTb×´ùlkéØ¾;õÐ&I¥\n0Z¢9ÿÓ°Bâfl»32d¯í*½FHëÕÐy{<Å/ü]ÉáBBN<ü', NULL, NULL, '10', 'Azampur', '1', 275, NULL, NULL, 'okPh1608569616', 'Pending', 'mlwqman40@gmail.com', 'Mhamad Lwqman', 'United States', '+9647714553060', '2410 Cruzen St. Suite S592', 'Nashville', '37211', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-21 22:53:36', '2020-12-21 22:53:36', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(73, NULL, 'BZh91AY&SYÿ\0ößP\0Xø+þ¿ÿÿúPòQA+k\Z% Põ4\0\Z\0Ð4\0\0\0iÀÀ\0&\0`D©½SÊ1M \0M¨Ú æ\0L\0L\0`\0&	 ÔôÔdÍM\0\04zR##ï?ÃÈyRðÏÔcPôÑàw÷Ñ²ÌM~Ãæôo(Qtýê?	2cêíWÔÉËðë«ñìÝªç;¨Ëèí6 Ûcq\\Ä¡ÙReM2:wSM9íB@lA¨$3#3©àr,Ã©rbY	¡yÈ»î¸fCº$È1ÀgB$´\r0\ZÓ\\fèáé£4*CC\nu)Ö²³pv±»^ó½êh1LcL\ZÁ«Ö^·ª>Q£Ä5X4Dï¥\\ápÎ ÍC	{ª½ÙÞ#0Éw¥IR¯?CÊ@Ã¬Þæfggi¦  Ì±2bH%IÊ¦_|Ú³§ÎVÞnì2ø¾o6r¹Ì8¾^óÅj+ãVT3[ÖXÑ¾RÎ[Âµg+uzBí^¡RBlhR l?8 Û\rªP´Ù*;g!cM&Ð&Ò#lØ>èoÎHX$$ÆiN5Æµ++XÆx©9;RnÂóÊ£ eX+kÈ¬:QÓ!§}6UBËdgMý¢Îxã1ÄcB¶<#\nÃ%\"¢ý¾§Àó$PúDü¨9O%\r%Wh#,¼r Å&ÚÑhME¡ªq¥¾\'Ôtì\Z?²û\r¼ôû!è(ÇÈEëÁ»YìbQF#pÔaT9CÃÌ5ySeÃåÔ3)7øér\Z°7-¼Ý-À0\"D~&$]`bÁ¦ÑÅ¥©Ä-!k7Êçx¹(ßVf&=£Ö@j¼~RnoÉî^ù£AU°mØUÚÊçø|Ã\" 8LÜ)àÆY#=.`ß¼p2cÆËjØÏ4	a3ò5F»u1ÞuÞ¹5ï6©¥á<¹hÂC:xJ5â|ç©ä6$}ÎcHC¤Îò~>Hî)ä)µ¥1¤À_ÁÍl60ã©zðVq}Û§A\"<i©X	µ2Ù¨,2]´Ed(¦t0²Bå³yÌLÂ¬¢Â©@¦8 zQÜ.=b&Aj%s¯¡.õz2çÀkë#3¡ Ðõ)ª=\rHØi <Ì² kpj6ïÞbl¸û§ÆÒDæÙZeeº\Zd(<ÎÂ9D;·ym]ßs=ç¸¸ÄÄ1¿{ 11 °´%\ZLjÎ&G]Ý\"ØT[µÈ@A	\"dÈì	-xAuÕÀQ¼\r1Ý4dLLeÒ(Lº¸Yd=QBaTb×´ùlkéØ¾;õÐ&I¥\n0Z¢9ÿÓ°Bâfl»32d¯í*½FHëÕÐy{<Å/ü]ÉáBBN<ü', NULL, NULL, '0', 'Azampur', '1', 275, NULL, NULL, 'SvcW1608569760', 'Pending', 'mlwqman40@gmail.com', 'Mhamad Lwqman', 'United States', '+9647503329601', '2410 Cruzen St. Suite S592', 'Nashville', '37211', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-21 22:56:00', '2020-12-21 22:56:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(74, NULL, 'BZh91AY&SYiUä[\0Z_P\0Xø;ö¿ÿÿú`nÀ6Æ9@\09¦&\0\0\0\0\0	Q©z\0Ð\0\0i§¨\0A¨*6¡ \r\Z\0\0\0\0\0R¡¦õ\0\Z\0\0æ\0L\0L\0`\0&	&&À¦¦MD\0\r5$	F\'ä|ñ;9$ÀP/Ê¢98ñ¢È$sÜã|9o8Î90kéÿwr:l\ZLbö`æâ¨aIy\"¶Î1t×¬8\rtÇQo^EE´´jIØ\\$L#×³C¡¾¦V2ÊNÍmèS!;Á\"äÐÚÃ3ÌyÔúh>ñÀ¤z/`LKrhM	xµìú÷H­ÔCÈ´Ó&0HjÍc¤UÑ\ZåD¨0hm¡%âóØ\nªLaò±\ZX^s½é­eLh ÆÁ1 l\Z«:­`¦0$@åH¢QÉÃ\nb\"\'Æy(bµ)Î2ÔXÂâhØc If ÑrÌÊ0Ì	 6ò Tã¬ì9¸2Ë9Æ^³r´^EÍÒ¦íU`æmI,^\"\0d©Z k`	6ÂÈA©¥$\\¢µ© ìp`Â,H)§J-ªl¢),[J¹ÀÊª°Y(P²dhE,ó;F¯{NBi9ßr]\'à`*.M£bhÖ¹æJÉf7Pè¦u\"­\\¤²kX55>¬AÐúÄîLYÔ¶5¡9­Åæ-µ´X®$Õúó=Ó±#àô~Ór§¶®æ$ÈóYÖÆ0ÇÎ¾s£\'I8à^W¨×³öGjx{\n¸ÇysÌÿ?ÀùZÔâq]0&¤oÇ«È8\0É\ZÌyÍf0ÿzG¸5ÍO]¸°3*W3Y¬fa	)PèËì\ZmZPQKX)C¨¦v ¢f&1¶#èúKw+ßÆu\Z ¹b¤áFÚ©ÒËä~\'úfwüÁÂf)¥¨²ØUËt.lïëJoÃQñ>de§3qþ\rAÑ®]LötÚ»½ó4¡SÄ\'ráÑhÂC9yÖjTjCÈlHiácbÄ¡ÐÄi3Ào$réÅ;ò	SjøúËRÈÀgîHî	kB©ýQÔlbÜs1¯ênFoü8ïð<H&@Â±#°­@ZXS-¬R-c#¡:ªÉLF00%(\'cjcCÈQ(ÇÅbo 6ÌjâºÈ+ß2^¥j\r2ÇÜ5ß²;hr0£ÜP§ À=D|cí1±b@yç¦ÀÐ9AãÕnr7ç²²D-#®DÙIg\r2#K¸Fivf=¼½$[(äp\"D/2T@TADa(`(XL1&À°¹Ò¼!mK°\\ËV¦d  2dBï&r×3 ëÉÀP¤8:\Z¯*²i\ZdÀe`ªÈ|Q0¢.×°_FU\Z÷®e¯Þ¸q$°X%âÀàOè>;Ènpl;W&Æ$5Y¯A¸ÌWB}8qB())§ø»)ÂJ¯\"Ø', NULL, NULL, '20', 'Azampur', '1', 335, NULL, NULL, 'rrN81610810888', 'Pending', 'mirihb@yahoo.com', 'nadmir Qerreti', 'Albania', '0696546896', 'skenderbeh, skenderbeg', 'shkoder', '4001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2021-01-16 21:28:08', '2021-01-16 21:28:08', NULL, NULL, '$', 1, 20, 0, 0, 0, NULL, 0, 0),
(75, NULL, 'BZh91AY&SYÃÂ©N\0L_P\0Xø+öD¿ÿÿú`}÷C@ÓRæ0`\0\00\0\Zi0§¤\0\0\0\0\0æ0`\0\00\0jQ2zòOPÚÓ\n\0Í@z¨`\0&\00\0\0\" Dh0L #CLL4ÍKC Hp18¢@;	Dä\'Q÷í:úð1(Tdµñìé$d|æÖ!UW²[b2ã$2²	Ã1-Njl |¡A èSÃÑì\ZÖHví\n$ò×CJÜÏ:sÏswy~@ÈÐ þ1ÚA¸Ðì3\ZgÃ $PsI	-¡4Ù#YÄÉI=(z[X$&Ù¡\02c´ 	HÈ2Ì¶[\\fpd,\"¦m(Z+J^ó©6Ð¿2àX\ZWÅ)5a¨tØ&4\rQjÒ×´	£LY\r*Ô±)Þ)JZI,Q#`Øb/)L³U=EU¢kj^\reDÄjÄÖÒg¡hRY&­zÅë_¡&±@A¡RÁ$0XØI\nE\\¹me%³»	E³`SUJwF\rF_UE\nµQ3 ë[^Õð)**QÔukPÉUWÔ¸/ªRI6±\rÀx²ê¹)éEñD¡¸Dt±jF0dI¨Td Âú$¤B#¤³£HçïÈLÁ¤DNkâK¢çû#ÈÔ/}â-SA±TY1*0\"À^ö²:y2Ä1p­ \"¡iÈ½ÂxI´%bp\\¸*\r¬ÏI¼Òk(¼ø?ÎÓ±èÁì3>!^áýhàtXÈèucµ¾ñ¨áTRªDr×6ÃÂ *>Å{L:ý{iÙªg#Úf~Äçñ÷ñ+j9ZRw£Xêl©¼\rNK1Û(ÖVjëÏD·°4._Q¡®uÒQS2Ö4Ú6´ [BØÀzËÜâk6Ó(ÈÛ26ÈbÜÞ5=ÛÈ¼_\n\\+As2äªCµQ[GÈþMGq¨¯È å3AT\Z¦¤%Ï%×ÐmÌÚ8Aç]\'¥®iÛ]9jàvQ®âFó©æy{Ý÷/Ó­\r¶.X)w­HÎ>A~GEf ð?!a\"¨ÙÓI*,2&ÂýÛh¸æ.Ù;O! U\ZÏ/aàI¼\'j¨þÎK¨ØÃ¸ï2¿hx3¸\ntêx90´Iãê©p(Ö¬\n4dÐK<*2«%BL^»ÇRV(ÆÊP,\nf³7CîQGQvïIf(d¨Ï¹>5\r3´k/GduÐâTGøI´±nA@| AæLJ£/{!/@ÎsÊR\rhýÆZÆúîÄ£ô.ºÈ/\n\n2Ó¤4ÈPn2(çª15çôôaÌ­=°ù$H\"ô¥H Ä$D$Ò`ÄNÃ3®®îK ª»Ì^æ²FQBBã²Ã¯Vðs4Ênc	¤fh c ,fg;â¥,Ö¡ðEJ^~\Z\rlkõäc=gEÀ=¥UU.(þÇ¸BÈÀ@Ã{.Éj$¯a¯£\'°ß®@Ý»|\"jÅÜN$0ðªS', NULL, NULL, '20', 'Azampur', '1', 470, NULL, NULL, 'yLJy1612608727', 'Pending', 'admin@gmail.com', 'my full name', 'Pakistan', '123456789012', 'test', 'test', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2021-02-06 16:52:07', '2021-02-06 16:52:07', NULL, NULL, '$', 1, 20, 0, 0, 0, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_tracks`
--

CREATE TABLE `order_tracks` (
  `id` int(191) NOT NULL,
  `order_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tracks`
--

INSERT INTO `order_tracks` (`id`, `order_id`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 2, 'Pending', 'You have successfully placed your pending order.', '2020-05-27 13:12:12', '2020-05-28 00:59:13'),
(2, 1, 'about to', 'testing', '2020-05-28 00:45:27', '2020-05-28 00:45:27'),
(3, 3, 'Pending', 'You have successfully placed your order.', '2020-06-01 06:58:08', '2020-06-01 06:58:08'),
(4, 4, 'Pending', 'You have successfully placed your order.', '2020-06-01 07:09:46', '2020-06-01 07:09:46'),
(5, 5, 'Pending', 'You have successfully placed your order.', '2020-06-01 07:14:06', '2020-06-01 07:14:06'),
(6, 6, 'Pending', 'You have successfully placed your order.', '2020-06-01 07:19:39', '2020-06-01 07:19:39'),
(7, 7, 'Pending', 'You have successfully placed your order.', '2020-06-05 04:39:02', '2020-06-05 04:39:02'),
(8, 8, 'Pending', 'You have successfully placed your order.', '2020-06-05 04:43:35', '2020-06-05 04:43:35'),
(9, 9, 'Pending', 'You have successfully placed your order.', '2020-06-05 04:48:27', '2020-06-05 04:48:27'),
(10, 10, 'Pending', 'You have successfully placed your order.', '2020-06-16 04:36:59', '2020-06-16 04:36:59'),
(11, 11, 'Pending', 'You have successfully placed your order.', '2020-06-16 04:55:44', '2020-06-16 04:55:44'),
(12, 12, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:03:00', '2020-06-16 05:03:00'),
(13, 13, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:37:10', '2020-06-16 05:37:10'),
(14, 14, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:37:56', '2020-06-16 05:37:56'),
(15, 15, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:38:56', '2020-06-16 05:38:56'),
(16, 16, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:40:48', '2020-06-16 05:40:48'),
(17, 17, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:42:30', '2020-06-16 05:42:30'),
(18, 18, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:49:11', '2020-06-16 05:49:11'),
(19, 19, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:52:33', '2020-06-16 05:52:33'),
(20, 20, 'Pending', 'You have successfully placed your order.', '2020-06-17 09:49:00', '2020-06-17 09:49:00'),
(21, 21, 'Pending', 'You have successfully placed your order.', '2020-06-19 09:26:09', '2020-06-19 09:26:09'),
(22, 22, 'Pending', 'You have successfully placed your order.', '2020-06-20 00:33:35', '2020-06-20 00:33:35'),
(23, 23, 'about to deliver', 'order details', '2020-06-20 10:46:00', '2020-06-20 10:49:14'),
(24, 24, 'Pending', 'You have successfully placed your order.', '2020-06-20 10:47:56', '2020-06-20 10:47:56');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `user_id`, `title`, `subtitle`, `price`) VALUES
(1, 0, 'Default Packaging', 'Default packaging by store', 0),
(2, 0, 'Gift Packaging', 'Exclusive Gift packaging', 15);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(191) NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `header` tinyint(1) NOT NULL DEFAULT '0',
  `footer` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `details`, `meta_tag`, `meta_description`, `header`, `footer`) VALUES
(1, 'About Us', 'about', '<div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 1</font><br></h2><p><span style=\"font-weight: 700;\">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 2</font><br></h2><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><br helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 3</font><br></h2><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><h2 helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-weight:=\"\" 700;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px=\"\" 15px;=\"\" font-size:=\"\" 30px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" style=\"font-family: \" 51);\"=\"\"><font size=\"6\">Title number 9</font><br></h2><p helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', NULL, NULL, 1, 0),
(2, 'Privacy & Policy', 'privacy', '<div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><img src=\"https://i.imgur.com/BobQuyA.jpg\" width=\"591\"></h2><h2><font size=\"6\">Title number 1</font></h2><p><span style=\"font-weight: 700;\">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 2</font><br></h2><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><br helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 3</font><br></h2><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><h2 helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-weight:=\"\" 700;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px=\"\" 15px;=\"\" font-size:=\"\" 30px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" 51);\"=\"\" style=\"font-family: \"><font size=\"6\">Title number 9</font><br></h2><p helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'test,test1,test2,test3', 'Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 0, 1),
(3, 'Terms & Condition', 'terms', '<div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 1</font><br></h2><p><span style=\"font-weight: 700;\">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 2</font><br></h2><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><br helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 3</font><br></h2><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><h2 helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-weight:=\"\" 700;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px=\"\" 15px;=\"\" font-size:=\"\" 30px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" 51);\"=\"\" style=\"font-family: \"><font size=\"6\">Title number 9</font><br></h2><p helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 't1,t2,t3,t4', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pagesettings`
--

CREATE TABLE `pagesettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `contact_success` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_title` text COLLATE utf8mb4_unicode_ci,
  `contact_text` text COLLATE utf8mb4_unicode_ci,
  `side_title` text COLLATE utf8mb4_unicode_ci,
  `side_text` text COLLATE utf8mb4_unicode_ci,
  `street` text COLLATE utf8mb4_unicode_ci,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `fax` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `site` text COLLATE utf8mb4_unicode_ci,
  `slider` tinyint(1) NOT NULL DEFAULT '1',
  `service` tinyint(1) NOT NULL DEFAULT '1',
  `featured` tinyint(1) NOT NULL DEFAULT '1',
  `small_banner` tinyint(1) NOT NULL DEFAULT '1',
  `best` tinyint(1) NOT NULL DEFAULT '1',
  `top_rated` tinyint(1) NOT NULL DEFAULT '1',
  `large_banner` tinyint(1) NOT NULL DEFAULT '1',
  `big` tinyint(1) NOT NULL DEFAULT '1',
  `hot_sale` tinyint(1) NOT NULL DEFAULT '1',
  `partners` tinyint(1) NOT NULL DEFAULT '0',
  `review_blog` tinyint(1) NOT NULL DEFAULT '1',
  `best_seller_banner` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link` text COLLATE utf8mb4_unicode_ci,
  `bottom_small` tinyint(1) NOT NULL DEFAULT '0',
  `flash_deal` tinyint(1) NOT NULL DEFAULT '0',
  `best_seller_banner1` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `featured_category` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pagesettings`
--

INSERT INTO `pagesettings` (`id`, `contact_success`, `contact_email`, `contact_title`, `contact_text`, `side_title`, `side_text`, `street`, `phone`, `fax`, `email`, `site`, `slider`, `service`, `featured`, `small_banner`, `best`, `top_rated`, `large_banner`, `big`, `hot_sale`, `partners`, `review_blog`, `best_seller_banner`, `best_seller_banner_link`, `big_save_banner`, `big_save_banner_link`, `bottom_small`, `flash_deal`, `best_seller_banner1`, `best_seller_banner_link1`, `big_save_banner1`, `big_save_banner_link1`, `featured_category`) VALUES
(1, 'Success! Thanks for contacting us, we will get back to you shortly.', 'admin@geniusocean.com', '<h4 class=\"subtitle\" style=\"margin-bottom: 6px; font-weight: 600; line-height: 28px; font-size: 28px; text-transform: uppercase;\">WE\'D LOVE TO</h4><h2 class=\"title\" style=\"margin-bottom: 13px;font-weight: 600;line-height: 50px;font-size: 40px;color: #0f78f2;text-transform: uppercase;\">HEAR FROM YOU</h2>', '<span style=\"color: rgb(119, 119, 119);\">Send us a message and we\' ll respond as soon as possible</span><br>', '<h4 class=\"title\" style=\"margin-bottom: 10px; font-weight: 600; line-height: 28px; font-size: 28px;\">Let\'s Connect</h4>', '<span style=\"color: rgb(51, 51, 51);\">Get in touch with us</span>', '3584 Hickory Heights Drive ,Hanover MD 21076, USA', '00 000 000 000', '00 000 000 000', 'admin@geniusocean.com', 'https://geniusocean.com/', 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, '1568889138banner1.jpg', 'http://google.com', '1565150264banner3.jpg', 'http://google.com', 0, 0, '1568889138banner2.jpg', 'http://google.com', '1565150264banner4.jpg', 'http://google.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(191) NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `photo`, `link`) VALUES
(1, '1591616422brand6.jpg', 'https://www.braven.com/'),
(2, '1591616413brand5.jpg', 'https://www.bosbosshop.com/'),
(3, '1591616401brand4.jpg', 'http://www.baseus.com/'),
(4, '1591616393brand3.jpg', 'https://ballistic.com/'),
(5, '1591616377brand2.jpg', 'http://www.adata.com/en/'),
(6, '1591616368brand1.jpg', 'https://www.acellories.com/'),
(7, '1591616432brand7.jpg', 'https://bumpboxx.com/'),
(8, '1591616439brand8.jpg', 'https://www.cellhelmet.com/'),
(9, '1591616457brand9.jpg', 'https://www.zizowireless.com/');

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateways`
--

CREATE TABLE `payment_gateways` (
  `id` int(191) NOT NULL,
  `subtitle` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(199) DEFAULT NULL,
  `status` tinyint(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_gateways`
--

INSERT INTO `payment_gateways` (`id`, `subtitle`, `title`, `details`, `type`, `status`) VALUES
(47, 'Paypal Express', 'Paypal Express', '<font size=\"3\"><b style=\"\">Paypal Express</b><br></font>', 'secure', 1),
(48, 'Cash On Delivery', 'Cash On Delivery', '<font size=\"3\"><b style=\"\">Cash On Delivery</b><br></font>', 'nonsecure', 1),
(49, 'Bank Transfer', 'Bank Transfer', '<font size=\"3\"><b style=\"\">Bank Transfer</b><b>&nbsp;No: 6528068515</b><br><br></font>', 'secure', 1),
(50, 'Money Order / Cheque Payment', 'Cheque Payment', '<font size=\"3\"><b style=\"\">Cheque Payment</b><br></font>', 'secure', 1),
(52, 'Wire Transfer', 'Wire Transfer', '<font size=\"3\"><b style=\"\">Wire Transfer</b><br></font>', 'secure', 1),
(53, 'Company Cheque', 'Company Cheque', '<font size=\"3\"><b style=\"\">Company Cheque</b><br></font>', 'nonsecure', 1),
(54, 'Open Shipment', 'Open Shipment', '<font size=\"3\"><b style=\"\">Open Shipment<br></font>', 'nonsecure', 1),
(55, 'Personal Cheque', 'Personal Cheque', '<font size=\"3\"><b style=\"\">Personal Cheque<br></font>', 'nonsecure', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pickups`
--

CREATE TABLE `pickups` (
  `id` int(191) UNSIGNED NOT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickups`
--

INSERT INTO `pickups` (`id`, `location`) VALUES
(2, 'Azampur'),
(3, 'Dhaka'),
(4, 'Kazipara'),
(5, 'Kamarpara'),
(6, 'Uttara');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(191) UNSIGNED NOT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `product_type` enum('normal','affiliate') NOT NULL DEFAULT 'normal',
  `affiliate_link` text,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `category_id` int(191) UNSIGNED NOT NULL,
  `subcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `childcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `attributes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_qty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `color_qty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color_price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `previous_price` double DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `stock` int(191) DEFAULT NULL,
  `policy` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `views` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text,
  `colors` text,
  `product_condition` tinyint(1) NOT NULL DEFAULT '0',
  `ship` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_meta` tinyint(1) NOT NULL DEFAULT '0',
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `youtube` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Physical','Digital','License') NOT NULL,
  `license` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `license_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `platform` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `licence_type` varchar(255) DEFAULT NULL,
  `measure` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `best` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `top` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `hot` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `latest` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `big` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `trending` tinyint(1) NOT NULL DEFAULT '0',
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_date` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_discount` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_catalog` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_id` int(191) NOT NULL DEFAULT '0',
  `color_gallery` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_prices` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `pro_type_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `pro_type_child` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `acc_brand_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `acc_type_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `carrier_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `cases` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `headphones` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `chargers` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cables` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `color_qty`, `color_price`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `color_gallery`, `vendor_prices`, `pro_type_id`, `pro_type_child`, `acc_brand_id`, `acc_type_id`, `carrier_id`, `cases`, `headphones`, `chargers`, `cables`) VALUES
(95, 'pr495jsv', 'normal', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 95', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027732dTwHda8l.png', '1568027751AidGUyJv.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 395, 'watch', '1', NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 07:36:06', '2021-02-15 20:36:09', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(116, 'pr496jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 116', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027684whVhJDrR.png', '1568027717gm0tFzeb.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', 'Keyword1,Keyword 2', '#ff1a1a,#0fbcd4', 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:15:17', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(117, 'pr497jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 117', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027658Up0FIXsW.png', '1568027670dTA7gQml.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', '0', NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:14:30', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(118, 'pr498jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 118', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027631cnmEylRa.png', '1568027643PgYviwVK.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', '0', NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:14:03', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(119, 'pr499jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 1', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027603i5UAZiKB.png', '1568027616O1coe3aV.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:13:36', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(120, 'pr500jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 120', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027558gLSECTIh.png', '1568027591b1oUIo7Q.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:53:33', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(121, 'pr501jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 121', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027534O1QEBPpR.png', '1568027543P8eoamtf.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:12:23', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(122, 'pr502jsv', 'affiliate', 'https://www.amazon.com/Alex-Vando-Shirts-Regular-Sleeve/dp/B072QYBXYV?pf_rd_r=9GJCSWTVBD8RT6JMEF0T&pf_rd_p=ad021af6-b7b0-4b98-946a-3a6865266868&pd_rd_r=611e6c6d-7326-449e-a8cf-ce8c1ba118cb&pd_rd_w=wk7Xs&pd_rd_wg=s8T3g&ref_=pd_gw_unk', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 122', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-122-pr502jsv', '1568027493eLqHNoZP.png', '1590041187dngDcz0K.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 78, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2021-02-15 02:48:45', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(300, 'stV0323qQA', 'normal', NULL, 0, 44, 95, NULL, NULL, 'Apple iPhone 6 Plus, Gold, Unlocked', 'apple-iphone-6-plus-gold-unlocked-stv0323qqa', '16056407221.jpeg', '1605640722BXkND4mx.jpg', NULL, '16,64,128', '100,100,100', '0,10,30', '#edd5bd', '0', '0', 150, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85);\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">CARRIER - This phone is locked to Simple Mobile from Tracfone, which means this device can only be used on the Simple Mobile wireless network.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">PLANS - Simple Mobile offers a variety of coverage plans, including 30-Day Unlimited Talk, Text &amp; Data. No activation fees, no credit checks, and no hassles on a nationwide lightning-fast network. For more information or plan options, please visit the Simple Mobile website.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">ACTIVATION - You’ll receive a Simple Mobile SIM kit with this iPhone. Follow the instructions to get the service activated with the Simple Mobile plan of your choice.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">5.5-inch Retina HD display</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">12MP camera and 4K video</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">5MP FaceTime HD camera with Retina Flash</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">Touch ID for secure authentication and Apple Pay</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">A9 chip</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">iOS 12 with Group FaceTime, Screen Time, and even faster performance</font></span></li></ul><div class=\"a-row a-expander-container a-expander-inline-container\" aria-live=\"polite\" style=\"color: rgb(85, 85, 85);\"><div class=\"a-expander-content a-expander-extend-content a-expander-content-expanded\" aria-expanded=\"true\" style=\"\"><ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\"><br></li><li style=\"margin: 0px; padding: 0px;\"><br></li></ul></div></div>', NULL, '<br>', 1, 56, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 01:18:42', '2021-02-13 22:24:54', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(301, 'fg81558rL4', 'normal', NULL, 0, 44, 96, NULL, NULL, 'Apple iPhone 6s, Gold, Unlocked', 'apple-iphone-6s-gold-unlocked-fg81558rl4', '16056417791.jpeg', '1605641779yIW2fUv9.jpg', NULL, '16,32,64', '100,100,100', '0,30,50', '#edd5bd', '0', '0', 130, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85);\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">CARRIER - This phone is locked&nbsp;to Simple Mobile from Trachoma, &nbsp;which means&nbsp;this device can&nbsp;only be used on the Simple&nbsp;Mobile wireless network.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">PLANS - Simple Mobile offers a&nbsp;variety of coverage plans, &nbsp;including 30-Day Unlimited Talk, &nbsp;Text &amp; Data. No activation fees, &nbsp;no credit checks, and no&nbsp;hassles&nbsp;on a nationwide lightning-fast&nbsp;network. For more information or&nbsp;plan&nbsp;options, please visit the&nbsp;Simple Mobile website.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">ACTIVATION - You’ll receive a&nbsp;Simple Mobile SIM kit with this&nbsp;iPhone. &nbsp;Follow the instructions to&nbsp;get the service activated with the&nbsp;Simple Mobile plan of&nbsp;your&nbsp;choice.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">4. 7-Inch Retina HD display</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">12MP camera and 4K video</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">5MP FaceTime HD camera with Retina Flash</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">Touch ID for secure authentication and Apple Pay</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">A9 chip</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">Ios 12 with Group FaceTime, Screen Time, and even faster performance</font></span></li></ul><div><br></div>', NULL, '<br>', 1, 58, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 01:36:19', '2021-02-13 22:24:53', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', 'Select Accessory', '', NULL, 0, 0, 0, 0),
(302, '9If2064rBC', 'normal', NULL, 0, 44, 97, NULL, NULL, 'Apple iPhone 6S Plus,  White Box', 'apple-iphone-6s-plus-white-box-9if2064rbc', '16056427641.jpg', '1605642764dvm0OW2J.jpg', NULL, '16,32,64,128', '100,100,100,100', '0,20,40,50', '#c0c1c6,#ebc2bc,#dddee0,#d5c3af', '0,0,0,0', '0,0,0,0', 175, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.5-inch Retina HD display</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12MP camera and 4K video</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5MP FaceTime HD camera with Retina Flash</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Touch ID for secure authentication</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">A9 chip</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">iOS 12 with Group FaceTime, Screen Time, and even faster performance4</span></li></ul>', NULL, '<br>', 1, 70, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 01:52:44', '2021-02-13 22:24:25', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Space Grey\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427644.jpg\",\"16056427645.jpg\",\"16056427646.jpg\"]},{\"key\":1,\"color\":\"Rose Gold\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427644.jpg\",\"16056427645.jpg\",\"16056427646.jpg\"]},{\"key\":2,\"color\":\"Silver\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427643.png\",\"16056427644.jpg\"]},{\"key\":3,\"color\":\"Gold\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427644.jpg\",\"16056427645.jpg\"]}]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(303, 'ZvT3089IdC', 'normal', NULL, 0, 44, 98, NULL, NULL, 'Apple iPhone 7  White Box', 'apple-iphone-7-white-box-zvt3089idc', '16056434763.jpg', '1605643476N5JngEdB.jpg', NULL, '32,128', '100,100', '0,25', '#000000,#ebc2bc,#dddee0,#d5c3af', '0,0,0,0', '0,0,0,0', 190, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.).</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic, in which case it will be UL or Mfi (Made for iPhone) Certified.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arm\'s length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 60, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 02:04:36', '2021-02-14 09:34:15', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16056434761.jpg\",\"16056434763.jpg\",\"16056434764.jpg\",\"16056434765.jpg\",\"16056434766.jpg\"]},{\"key\":1,\"color\":\"Rose Gold\",\"images\":[\"16056434761.jpg\",\"16056434762.jpg\",\"16056434763.jpg\"]},{\"key\":2,\"color\":\"Silver\",\"images\":[\"16056434762.jpg\",\"16056434763.jpg\",\"16056434764.jpg\"]},{\"key\":3,\"color\":\"Gold\",\"images\":[\"16056434762.jpg\",\"16056434764.jpg\",\"16056434765.jpg\",\"16056434766.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(304, 'SUX3609Ku0', 'normal', NULL, 0, 44, 99, NULL, NULL, 'Apple iPhone 7 Plus Jet Black  Unlocked', 'apple-iphone-7-plus-jet-black-unlocked-sux3609ku0', '16056444743.jpg', '160564447456rVG8Zp.jpg', NULL, '32,128', '100,97', '0,25', '#26272b,#ebc2bc,#000000', '0,0,0', '0,0,0', 290, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.).</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic, in which case it will be UL or Mfi (Made for iPhone) Certified.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arms length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 63, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 02:21:14', '2021-02-14 01:34:49', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(305, 'WCC5121qo2', 'normal', NULL, 0, 44, 101, NULL, NULL, 'Apple iPhone 8 Plus  White Box', 'apple-iphone-8-plus-white-box-wcc5121qo2', '16056454354.jpg', '1605645435aaePGk0T.jpg', NULL, '64,256', '100,100', '0,55', '#e6e7eb,#ebc2bc,#000000', '0,0,0', '0,0,0', 360, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.5-inch Retina HD display</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">IP67 water and dust resistant (maximum depth of 1 meter up to 30 minutes)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12MP dual cameras with OIS, Portrait mode, Portrait Lighting, and 4K video</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">7MP FaceTime HD camera with Retina Flash</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Touch ID for secure authentication</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">A11 Bionic with Neural Engine</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">iOS 12 with Screen Time, Group FaceTime, and even faster performance</span></li></ul>', NULL, '<br>', 1, 73, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 02:37:15', '2021-02-13 22:24:12', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Silver\",\"images\":[\"16056454352.jpg\",\"16056454353.jpg\",\"16056454354.jpg\",\"16056454355.jpg\"]},{\"key\":1,\"color\":\"Rose Gold\",\"images\":[\"16056454352.jpg\"]},{\"key\":2,\"color\":\"Black\",\"images\":[\"16056454352.jpg\",\"16056454353.jpg\",\"16056454354.jpg\"]}]', NULL, '19', '', '', '', '1,2,4,5,6,7', 0, 0, 0, 0);
INSERT INTO `products` (`id`, `sku`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `color_qty`, `color_price`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `color_gallery`, `vendor_prices`, `pro_type_id`, `pro_type_child`, `acc_brand_id`, `acc_type_id`, `carrier_id`, `cases`, `headphones`, `chargers`, `cables`) VALUES
(306, 'jcQ5469UFl', 'normal', NULL, 0, 44, 69, NULL, NULL, 'Apple iPhone X Unlocked', 'apple-iphone-x-unlocked-jcq5469ufl', '16056457181.jpg', '1605645718cB1APRJT.jpg', NULL, '64', '100', '0', '#000000,#e4e4e2', '0,0', '0,0', 425, 0, '<div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Originally released September 2017</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Unlocked, SIM-Free, Model A1865<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">5.8-inch Super Retina HD display with OLED technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">A11 Bionic chip with embedded M11 motion coprocessor</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Talk time (wireless) up to 21 hours</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">LTE and 802.11ac Wi‑Fi with MIMO</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Bluetooth 5.0 wireless technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">NFC with reader mode</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">12MP wide-angle and telephoto cameras</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Digital zoom up to 10x</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">1080p HD video recording</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">7MP TrueDepth camera with Portrait mode</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Face ID</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Siri</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Apple Pay</p></div><div class=\"para-list as-pdp-lastparalist\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">6.14 ounces and 0.30 inch</p></div>', NULL, '<br>', 1, 161, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 0, 0, 0, '2020-11-18 02:41:58', '2021-02-15 22:17:33', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16056457182.jpg\",\"16056457183.jpg\"]},{\"key\":1,\"color\":\"Silver\",\"images\":[\"16056457182.jpg\",\"16056457183.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(307, 'EPW0157SuU', 'normal', NULL, 0, 44, 70, NULL, NULL, 'Apple iPhone Xs Max  Unlocked', 'apple-iphone-xs-max-unlocked-epw0157suu', '16056504493.jpg', '1605650449obK23hKx.jpg', NULL, '64,256', '100,100', '0,25', '#f9dbc1,#000000,#e5e5e3', '0,0,0', '0,0,0', 575, 0, '<div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Originally released September 2018</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Unlocked, SIM-Free, Model A1921¹</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">6.5-inch Super Retina HD display with OLED technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">A12 Bionic chip with next-generation Neural Engine</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Talk time (wireless) up to 25 hours</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Gigabit-class LTE with 4x4 MIMO and LAA</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">802.11ac Wi‑Fi with 2x2 MIMO</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Bluetooth 5.0 wireless technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">NFC with reader mode</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Dual 12MP wide-angle and telephoto cameras</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">2x optical zoom; digital zoom up to 10x</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">4K video recording, 1080p HD video recording</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">7MP TrueDepth camera with Portrait mode and lighting</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Face ID</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Siri</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Apple Pay</p></div><div class=\"para-list as-pdp-lastparalist\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">7.34 ounces and 0.30 inch</p></div>', NULL, '<br>', 1, 65, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 04:00:49', '2021-02-13 22:24:51', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Rose Gold\",\"images\":[\"16056504492.jpg\",\"16056504493.jpg\"]},{\"key\":1,\"color\":\"Black\",\"images\":[\"16056504491.jpg\",\"16056504492.jpg\"]},{\"key\":2,\"color\":\"Silver\",\"images\":[\"16056504491.jpg\",\"16056504492.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(308, 'zVx0521NLO', 'normal', NULL, 0, 44, 71, NULL, NULL, 'Apple iPhone 11 Unlocked', 'apple-iphone-11-unlocked-zvx0521nlo', '16056508352.jpg', '1605650836UHDk6Mmx.jpg', NULL, '64,128', '100,100', '0,50', '#ded9e0,#ceeee1,#d62a44,#000000', '0,0,0,0', '0,0,0,0', 575, 0, '<table border=\"0\" width=\"1151\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-spacing: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><tbody><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">3G bands</td><td class=\"xl67\" width=\"872\" style=\"padding: 0px;\">HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100</td></tr><tr data-spec-optional=\"\"><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">4G bands</td><td class=\"xl71\" style=\"padding: 0px;\">LTE</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"24\" style=\"padding: 0px;\">Screen Size</td><td class=\"xl68\" width=\"872\" style=\"padding: 0px;\">6.1‑inch</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Resolution</td><td class=\"xl65\" style=\"padding: 0px;\">828 x 1792 pixels</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Dimensions</td><td class=\"xl65\" style=\"padding: 0px;\">150.9 x 75.7 x 8.3 mm (5.94 x 2.98 x 0.33 in)</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Weight</td><td class=\"xl65\" style=\"padding: 0px;\">194 g</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">SIM</td><td style=\"padding: 0px;\">Dual SIM (Physically)</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">OS</td><td class=\"xl65\" style=\"padding: 0px;\">iOS 13</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"23\" style=\"padding: 0px;\">Chipset</td><td class=\"xl73\" width=\"872\" style=\"padding: 0px;\">A13 Bionic chip</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Rom</td><td class=\"xl64\" width=\"872\" style=\"padding: 0px;\">64GB</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Ram</td><td class=\"xl64\" width=\"872\" style=\"padding: 0px;\">4GB</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"23\" style=\"padding: 0px;\">Primary</td><td class=\"xl72\" style=\"padding: 0px;\">Dual 12 MP+12 MP</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"23\" style=\"padding: 0px;\">Secondary Camera</td><td class=\"xl72\" style=\"padding: 0px;\">12MP</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"22\" style=\"padding: 0px;\">Video</td><td class=\"xl70\" width=\"872\" style=\"padding: 0px;\">160p@24/30/60fps,&nbsp;1080p@30/60/120/240fps</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"22\" style=\"padding: 0px;\">Wifi</td><td class=\"xl69\" width=\"872\" style=\"padding: 0px;\">Wi-Fi 802.11 a/b/g/n/ac/ax, dual-band, hotspot</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Bluetooth</td><td class=\"xl74\" style=\"padding: 0px;\">5.0</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">GPS</td><td class=\"xl66\" width=\"872\" style=\"padding: 0px;\">Yes</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">NFC</td><td class=\"xl64\" width=\"872\" style=\"padding: 0px;\">Yes</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"24\" style=\"padding: 0px;\">USB</td><td class=\"xl68\" width=\"872\" style=\"padding: 0px;\">USB 2.0</td></tr></tbody></table>', NULL, '<br>', 1, 72, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 04:07:15', '2021-02-13 22:24:52', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Purple\",\"images\":[\"16056508351.jpg\",\"16056508353.jpg\",\"16056508354.jpg\"]},{\"key\":1,\"color\":\"Green\",\"images\":[\"16056508352.jpg\",\"16056508353.jpg\",\"16056508354.jpg\"]},{\"key\":2,\"color\":\"Red\",\"images\":[\"16056508351.jpg\",\"16056508352.jpg\",\"16056508353.jpg\"]},{\"key\":3,\"color\":\"Black\",\"images\":[\"16056508351.jpg\",\"16056508352.jpg\",\"16056508354.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(309, 'xZa0839cOG', 'normal', NULL, 0, 44, 73, NULL, NULL, 'Apple iPhone 11 Pro Max Unlocked', 'apple-iphone-11-pro-max-unlocked-xza0839cog', '16056515471.jpg', '16056515478OqBnrmL.jpg', NULL, '64,256,512', '78,100,100', '0,95,195', '#fbd7bd,#4e5752', '0,0', '0,0', 900, 0, '<ul class=\"a-unordered-list a-vertical\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">OFFER INCLUDES: An Apple iPhone and a wireless plan with unlimited data/talk/text</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">WIRELESS PLAN: Unlimited talk, text, and data with mobile hotspot, nationwide coverage, and international reach. No long-term contract required.</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">PROGRAM DETAILS: When you add this offer to cart, it will reflect 3 items: the iPhone, SIM kit, and carrier subscription</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">6.5-inch Super Retina XDR OLED display, water and dust resistant, with Face ID</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Triple-camera system with 12MP Ultra Wide camera, 12MP TrueDepth front camera with Portrait mode</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fast-charge and wireless charging capable</span></li></ul>', NULL, '<br>', 1, 222, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 0, 0, 0, '2020-11-18 04:19:07', '2021-02-15 20:56:24', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Rose Gold\",\"images\":[\"16056515471.jpg\",\"16056515472.jpg\",\"16056515473.jpg\"]},{\"key\":1,\"color\":\"Midnight Green\",\"images\":[\"16056515472.jpg\",\"16056515473.jpg\",\"16056515474.jpg\"]}]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(310, '75j1685yfn', 'normal', NULL, 0, 44, 102, NULL, NULL, 'Apple iPhone XR, Unlocked', 'apple-iphone-xr-unlocked-75j1685yfn', '1605711939red.jpeg', '1605711939EYM3bH5S.jpg', NULL, '64,128', '96,100', '0,25', '#950310,#45a9db,#ffffff', '0,0,0', '0,0,0', 450, 0, '<ul class=\"a-unordered-list a-vertical\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85);\"><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">Get the iPhone XR for as low as $6.63/month - With 24-month financing through an eligible credit card, your monthly payment amount will be $16.63, and you will receive $10 in Amazon.com gift card credit every month you maintain the wireless plan subscription.</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">OFFER INCLUDES: An Apple iPhone and a wireless plan with unlimited data/talk/text</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">WIRELESS PLAN: Unlimited talk, text, and data with mobile hotspot, nationwide coverage, and international reach. No long-term contract required.</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">PROGRAM DETAILS: When you add this offer to cart, it will reflect 3 items: the iPhone, SIM kit, and carrier subscription</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">6.1-inch Liquid Retina display, water, and dust resistant, with Face ID</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">12MP dual cameras with Portrait mode, 7MP TrueDepth front camera</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">Wireless charging</font></span></li></ul>', NULL, '<br>', 1, 163, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 0, 0, 0, '2020-11-18 21:05:39', '2021-02-15 18:14:35', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Red\",\"images\":[\"16057121831.jpg\",\"16057121832.jpg\",\"16057121833.jpg\",\"16057121834.jpg\",\"16057121835.jpg\",\"16057121836.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057121831.jpg\",\"16057121832.jpg\",\"16057121833.jpg\",\"16057121834.jpg\",\"16057121835.jpg\",\"16057121836.jpg\"]},{\"key\":2,\"color\":\"White\",\"images\":[\"16057119391.jpg\",\"16057119392.jpg\",\"16057119393.jpg\",\"16057119394.jpg\",\"16057119395.jpg\",\"16057119396.jpg\"]}]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(311, 'IBk2316uUs', 'normal', NULL, 0, 44, 72, NULL, NULL, 'Apple iPhone 11 Pro', 'apple-iphone-11-pro-ibk2316uus', '16057125481.jpg', '1605712548p9c7Nx3I.jpg', NULL, '64', '99', '0', '#fcd8be,#545d58', '0,0', '0,0', 795, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">6.5-inch Super Retina XDR OLED display</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Triple-camera system with 12MP Ultra Wide, Wide, and Telephoto cameras; Night mode, Portrait mode, and 4K video up to 60fps</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12MP TrueDepth front camera with Portrait Mode, 4K video, and Slo-Motion</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Water and dust resistant (4 meters for up to 30 minutes, IP68)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">90 Day Warranty - comes with a generic charger. Does not include headphones, SIM-card or original packaging</span></li></ul>', NULL, '<br>', 1, 91, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:15:48', '2021-02-13 22:24:30', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Rose Gold\",\"images\":[\"16057125481.jpg\",\"16057125482.jpg\",\"16057125483.jpg\"]},{\"key\":1,\"color\":\"Midnight Green\",\"images\":[\"16057125482.jpg\",\"16057125483.jpg\",\"16057125484.jpg\"]}]', NULL, '19', '21', '25', '74', '1,3,4,5,6,7', 0, 0, 0, 0),
(312, 'bS6316774p', 'normal', NULL, 0, 45, NULL, NULL, NULL, 'Samsung Galaxy S7 Edge Gold Unlocked', 'samsung-galaxy-s7-edge-gold-unlocked-bs6316774p', '16057133335.jpg', '1605713333Wqt9a2Cd.jpg', NULL, NULL, NULL, NULL, '#c5b09d', '0', '0', 180, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.5-inch QHD AMOLED Capacitive Touchscreen, 2560 x 1440 pixel resolution (534 PPI pixel density) + Corning Gorilla Glass 4 (Back Panel) w/ Always-on Display, TouchWiz UI, Curved Edge Screen</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Android OS, v6.0 (Marshmallow), Chipset: Exynos 8890 Octa, Processor: Octa-Core (Quad-Core 2.3 GHz Mongoose &amp; Quad-Core 1.6 GHz Cortex-A53), GPU: Mali-T880 MP12</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12 Megapixel Camera with Dual Pixel Autofocus, OIS, F/1.7, 26mm, phase detection autofocus, LED Flash, 1/2.5\" sensor size, 1.4 m pixel size, geo-tagging, simultaneous 4K video, and 9MP image recording, touch focus, face/smile detection, Auto HDR, panorama + 5 Megapixel Front Camera with F/1.7, Selfie Flash, 22mm Lens, Dual Video Call, Auto HDR</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Internal Memory: 32GB, 4GB RAM - microSD Up to 256GB. This cell phone is compatible with GSM carriers like ATT and also with GSM SIM need cards (e.g. H20, Straight Talk, and select prepaid carriers) need to contact the service provider to check fully compatibility of the phone. This cellphone will NOT work with CDMA Carriers like Verizon, T-mobile, Sprint, Boost mobile or Virgin mobile.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">WLAN: Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot; Bluetooth: v4.2, A2DP, LE, apt-X; GPS: with A-GPS, GLONASS, BDS NFC; USB: microUSB v2.0, USB Host</span></li></ul>', NULL, '<br>', 1, 40, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:28:53', '2021-02-14 00:28:41', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(313, 'H3S3369BwE', 'normal', NULL, 0, 45, 104, NULL, NULL, 'Samsung Galaxy S8  Unlocked', 'samsung-galaxy-s8-unlocked-h3s3369bwe', '16057137971.jpg', '1605713797r85bw7Ig.jpg', NULL, NULL, NULL, NULL, '#67657b,#749dcb,#000000', '0,0,0', '0,0,0', 225, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Screen: Super AMOLED capacitive touchscreen, 16M colors, Size: 5.8 inches, Resolution: 1440 x 2960 pixels, Protection: Corning Gorilla Glass 5. OS: Android 7.0 (Nougat), upgradable to Android 8.0 (Oreo), Chipset: Exynos 8895 Octa. Memory: 64 GB, 4 GB RAM. Camera: 12MP, Front: 8 MP. Sensors: Iris scanner, fingerprint (rear-mounted), accelerometer, gyro, proximity, compass, barometer, heart rate, SpO2.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Unlocked cell phones are compatible with GSM carrier such as AT&amp;T and T-Mobile, but are not compatible with CDMA carriers such as Verizon and Sprint.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Please check if your GSM cellular carrier supports the bands for this model before purchasing, LTE may not be available in all regions: 2G bands: GSM 850 / 900 / 1800 / 1900 - SIM 1 &amp; SIM 2, 3G bands: HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100, LTE bands: 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 12(700), 13(700), 17(700), 18(800), 19(800), 20(800), 25(1900), 26(850), 28(700), 32(1500), 66(1700/2100), 38(2600), 39(1900), 40(2300), 41(2500).</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">This device may not include a US warranty as some manufacturers do not honor warranties for international items. Please contact the seller for specific warranty information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The box contains: Your new device, USB cable, Earphones, Charger (may be foreign), Documentation.</span></li></ul>', NULL, '<br>', 1, 40, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:36:37', '2021-02-13 22:25:34', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Orchid Grey\",\"images\":[\"16057137972.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057137972.jpg\",\"16057137973.jpg\",\"16057137974.jpg\",\"16057137975.jpg\"]},{\"key\":2,\"color\":\"Black\",\"images\":[\"16057137972.jpg\",\"16057137973.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(314, 'gtN3823YdR', 'normal', NULL, 0, 45, 105, NULL, NULL, 'Samsung S8 Plus Brand New', 'samsung-s8-plus-brand-new-gtn3823ydr', '16057142171.jpg', '16057142178ivwgu5n.jpg', NULL, NULL, NULL, NULL, '#a3a4a8,#000000', '0,0', '0,0', 285, 0, '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">The Galaxy S8+ expansive display stretches from edge to edge, giving you the most amount of screen in the least amount of space. And the Galaxy S8+ is even more expansive—our biggest screen yet.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">The Galaxy S8 camera still takes amazing photos in low light, and now has an enhanced front-facing camera so you can take better clearer selfies.</p>', NULL, '<br>', 1, 56, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:43:37', '2021-02-15 01:28:13', 0, NULL, NULL, NULL, 0, 0, '{\"2\":{\"key\":2,\"color\":\"Silver\",\"images\":[\"16057142172.jpg\"]},\"3\":{\"key\":3,\"color\":\"Black\",\"images\":[\"16057142172.jpg\"]}}', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(315, 'cRD4259HyP', 'normal', NULL, 0, 45, 74, NULL, NULL, 'Samsung Galaxy S9 Unlocked', 'samsung-galaxy-s9-unlocked-crd4259hyp', '16057148521.jpg', '1605714852dbVvPipP.jpg', NULL, NULL, NULL, NULL, '#9b7396,#000000', '0,0', '0,0', 275, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Dual Nano-SIM (4G + 3G) ; 3G WCDMA B1(2100) / B2(1900) / B4(AWS) / B5(850) / B8(900) TD-SCDMA B34(2010) / B39(1880) ; 4G LTE B1 / B2 / B3 / B4 / B5 / B7 / B8 / B12 / B13 / B17 / B18 / B19 / B20 / B25 / B26 / B28 / B32 / B66 / B38 / B39 / B40 / B41 ; 4G LTE &amp; 3G work with AT&amp;T and T-Mobile ; DOES NOT work with Sprint, Verizon, U.S. Cellular and all other CDMA carriers ; LTE compatibility: This is international stock, varies per carrier (ensure to check with your carrier before purchase)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">10nm, 64-bit, Octa-Core (2.7 GHz Quad + 1.7 GHz Quad) CPU ; 64GB ROM, 4GB RAM ; Supports microSD, up to 400 GB (uses SIM 2 slot) ; 3000 mAh battery.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Main Camera: 12 MP (f/1.5-2.4, 1/2.55\", 1.4 µm, Dual Pixel PDAF), phase detection autofocus, OIS, LED flash; Front Camera: 8 MP (f/1.7, 1/3.6\", 1.22 µm), autofocus, Auto HDR.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.8 inches, Super AMOLED capacitive touchscreen, 16M colors.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Package Content: Samsung Galaxy S9 (SM-G960F/DS), USB Cable, Earphone, Ejection pin, USB power adapter, Quick start guide, USB connector (USB Type-C), Micro USB connector, Clear view cover. PLEASE NOTE: this is an international version of the phone that comes with no warranty in the US.</span></li></ul>', NULL, '<br>', 1, 48, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:54:12', '2021-02-13 22:25:28', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Purple\",\"images\":[\"16057148522.jpg\",\"16057148523.jpg\",\"16057148524.jpg\"]},{\"key\":1,\"color\":\"Black\",\"images\":[\"16057148522.jpg\",\"16057148523.jpg\",\"16057148524.jpg\",\"16057148525.jpg\",\"16057148526.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(316, 'p0658366FM', 'normal', NULL, 0, 45, 106, NULL, NULL, 'Samsung Galaxy S9 Plus Unlocked', 'samsung-galaxy-s9-plus-unlocked-p0658366fm', '16057162491.jpg', '1605716249YZuQ0DJp.jpg', NULL, NULL, NULL, NULL, '#000000,#947192', '0,0', '0,0', 300, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Dual Nano-SIM (4G + 3G) ; 3G WCDMA B1(2100) / B2(1900) / B4(AWS) / B5(850) / B8(900) TD-SCDMA B34(2010) / B39(1880) ; 4G LTE B1 / B2 / B3 / B4 / B5 / B7 / B8 / B12 / B13 / B17 / B18 / B19 / B20 / B25 / B26 / B28 / B32 / B66 / B38 / B39 / B40 / B41 ; 4G LTE &amp; 3G work with AT&amp;T and T-Mobile ; DOES NOT work with Sprint, Verizon, U.S. Cellular and all other CDMA carriers ; LTE compatibility: This is international stock, varies per carrier (ensure to check with your carrier before purchase)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">10nm, 64-bit, Octa-Core (2.7 GHz Quad + 1.7 GHz Quad) CPU ; 64GB ROM, 4GB RAM ; Supports microSD, up to 400 GB (uses SIM 2 slot) ; 3000 mAh battery.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Main Camera: 12 MP (f/1.5-2.4, 1/2.55\", 1.4 µm, Dual Pixel PDAF), phase detection autofocus, OIS, LED flash; Front Camera: 8 MP (f/1.7, 1/3.6\", 1.22 µm), autofocus, Auto HDR.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.8 inches, Super AMOLED capacitive touchscreen, 16M colors.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Package Content: Samsung Galaxy S9 (SM-G960F/DS), USB Cable, Earphone, Ejection pin, USB power adapter, Quick start guide, USB connector (USB Type-C), Micro USB connector, Clear view cover. PLEASE NOTE: this is an international version of the phone that comes with no warranty in the US.</span></li></ul>', NULL, '<br>', 1, 54, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 22:17:29', '2021-02-13 22:25:30', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057162492.jpg\",\"16057162493.jpg\",\"16057162494.jpg\",\"16057162495.jpg\",\"16057162496.jpg\"]},{\"key\":1,\"color\":\"Purple\",\"images\":[\"16057162492.jpg\",\"16057162493.jpg\",\"16057162494.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(317, 'c386443qwO', 'normal', NULL, 0, 45, 107, NULL, NULL, 'Samsung Galaxy S10e Unlocked', 'samsung-galaxy-s10e-unlocked-c386443qwo', '16057174801.jpg', '16057174811wnnQV7X.jpg', NULL, NULL, NULL, NULL, '#000000,#b7d3dc,#45607e', '0,0,0', '0,0,0', 360, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">For USA Buyers: This Smartphone is compatible/will work with any GSM Networks such as AT&amp;T, T-Mobile. For exact 2G GSM, 3G, 4G/LTE compatibility, please check with your network provider in advance prior to your purchase. This phone WILL NOT WORK with any CDMA Networks such as VERIZON, SPRINT, US CELLULAR.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Hybrid/Dual-SIM (Nano-SIM), Network Compatibility : SIM CARD 1 [ 2G GSM and/or 3G 850(B5) / 900(B8) / 1700|2100(B4) / 1900(B2) / 2100(B1) and/or 3G TD-SCDMA : 2000 / 1900 and/or 4G LTE : 700(B12) / 700(B13) / 700(B17) / 700(B28) / 800(B18) / 800(B19) / 800(B20) / 850(B5) / 850(B26) / 900(B8) /1500(B32) / 1700|2100(B4) / 1700|2100 (B66) / 1800(B3) / 1900(B2) / 1900(B25) / 2100(B1) / 2600(B7) and/or : TD-LTE : 1900(B39) / 2300(B40) / 2500(B41) / 2600(B38) / 3500(B42) ] and SIM CARD 2 [ 2G GSM ]</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.8 inches, Dynamic AMOLED capacitive touchscreen, 16M colors, 1080 x 2280 pixels, 19:9 ratio (~438 ppi density), Corning Gorilla Glass 5</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">128GB Storage, 6GB RAM, Up to 512GB microSD Card slot (uses SIM 2 slot)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Android 9.0 (Pie), Exynos 9820 Octa (8 nm), Octa-core (2x2.73 GHz Mongoose M4 &amp; 2x2.31 GHz Cortex-A75 &amp; 4x1.95 GHz Cortex-A55), DualCamera 12MP, f/1.5-2.4, 16 MP, f/2.2, LED flash, auto-HDR, panorama, 2160p@60fps, 1080p@240fps, 720p@960fps, HDR, dual-video rec, Front Camera : 10MP, f/1.9, 26mm (wide), Dual video call, Auto-HDR, 2160p@30fps, 1080p@30fps</span></li></ul>', NULL, '<br>', 1, 40, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 22:38:00', '2021-02-14 07:27:14', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057174802.jpg\",\"16057174803.jpg\",\"16057174804.jpg\",\"16057174805.jpg\",\"16057174806.jpg\",\"16057174807.jpg\"]},{\"key\":1,\"color\":\"white\",\"images\":[\"16057174802 (1).jpg\",\"16057174803 (1).jpg\",\"16057174804 (1).jpg\",\"16057174805 (1).jpg\",\"16057174806 (1).jpg\",\"16057174807 (1).jpg\"]},{\"key\":2,\"color\":\"Blue\",\"images\":[\"16057174802.jpg\",\"16057174803.jpg\",\"16057174804.jpg\",\"16057174805.jpg\",\"16057174806.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(318, 'oTd7485L3j', 'normal', NULL, 0, 45, 108, NULL, NULL, 'Samsung Galaxy S10 Unlocked', 'samsung-galaxy-s10-unlocked-otd7485l3j', '16057215111.jpg', '1605721511nzRT3jrl.jpg', NULL, NULL, NULL, NULL, '#000000,#33597d', '0,0', '0,0', 450, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fully Unlocked: Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.), both domestically and internationally.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arm\'s length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 44, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 23:45:11', '2021-02-13 22:25:26', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057215112.jpg\",\"16057215113.jpg\",\"16057215114.jpg\",\"16057215115.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057215112.jpg\",\"16057215113.jpg\",\"16057215114.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(319, 'RDX1534Df1', 'normal', NULL, 0, 45, 109, NULL, NULL, 'Samsung Galaxy S10+  Unlocked', 'samsung-galaxy-s10-unlocked-rdx1534df1', '16057217521.jpg', '1605721752ou6cXsmc.jpg', NULL, NULL, NULL, NULL, '#000000,#33597d', '0,0', '0,0', 495, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The octa-core processor and plenty of RAM deliver outstanding overall performance for opening and running applications, flipping through menus, and more</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Adapts to you and the way you use your phone, learning your preferences as you go. Your experience gets better over time, and it keeps things running smoother and longer</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Provides fast Web connection for downloading apps, streaming content, and staying connected on social media</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Its nearly frameless cinematic Infinity Display offers more detail and clarity, more immersive and uninterrupted content, in a slim, balanced form</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Sees what you see. The pro-grade camera effortlessly captures epic, pro-quality images of the world as you see it.</span></li></ul>', NULL, '<br>', 1, 50, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 23:49:12', '2021-02-13 22:25:28', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057217522.jpg\",\"16057217523.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057217521.jpg\",\"16057217522.jpg\",\"16057217523.jpg\",\"16057217524.jpg\",\"16057217525.jpg\",\"16057217526.jpg\"]}]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(320, '4EN74823uw', 'normal', NULL, 0, 45, 110, NULL, NULL, 'Samsung Galaxy S20 Unlocked', 'samsung-galaxy-s20-unlocked-4en74823uw', '16057281251.jpg', '1605728125tnH00KfA.jpg', NULL, NULL, NULL, NULL, '#e9bdca', '0', '0', 650, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); margin: 0px; padding: 0px; border: 0px; font-size: 1rem; vertical-align: baseline; float: left; text-align: justify; width: 511.984px; display: inline-block; font-family: SamsungOneLatinWeb400, arial; line-height: 1.75;\">Getting work done on your smartphone should be easy. The Galaxy S20 integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); margin: 0px; padding: 0px 0px 0px 30px; border: 0px; font-size: 1rem; vertical-align: baseline; float: right; width: 511.984px; text-align: justify; display: inline-block; font-family: SamsungOneLatinWeb400, arial; line-height: 1.75;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; vertical-align: baseline;\"><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20’s productivity experience<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">1</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">2</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">5</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">3</span></li></ul></div>', NULL, '<br>', 1, 51, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:35:25', '2021-02-13 22:25:27', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Pink\",\"images\":[\"16057281252.jpg\",\"16057281253.jpg\",\"16057281254.jpg\",\"16057281255.jpg\",\"16057281256.jpg\",\"1605728125no 1.webp\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(321, 'NlL8173PC0', 'normal', NULL, 0, 45, 111, NULL, NULL, 'Samsung Galaxy S20 Plus 5G Unlocked', 'samsung-galaxy-s20-plus-5g-unlocked-nll8173pc0', '16057285811.jpg', '1605728581qP4ZxbRy.jpg', NULL, NULL, NULL, NULL, '#68696b,#c3e3fa,#000000', '0,0,0', '0,0,0', 725, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">Getting work done on your smartphone should be easy. The Galaxy S20+ integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20+’s productivity experience<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></li><li style=\"margin: 0px; padding: 0px;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">5</span></li><li style=\"margin: 0px; padding: 0px;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li></ul></div>', NULL, '<br>', 1, 58, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:43:01', '2021-02-13 22:25:29', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Space Grey\",\"images\":[\"16057285812.jpg\",\"16057285813.jpg\",\"16057285814.jpg\",\"16057285815.jpg\",\"16057285816.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057285812.jpg\",\"16057285813.jpg\",\"16057285814.jpg\",\"16057285815.jpg\",\"16057285816.jpg\"]},{\"key\":2,\"color\":\"Black\",\"images\":[\"16057285812.jpg\",\"16057285813.jpg\",\"16057285814.jpg\",\"16057285815.jpg\",\"16057285816.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0);
INSERT INTO `products` (`id`, `sku`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `color_qty`, `color_price`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `color_gallery`, `vendor_prices`, `pro_type_id`, `pro_type_child`, `acc_brand_id`, `acc_type_id`, `carrier_id`, `cases`, `headphones`, `chargers`, `cables`) VALUES
(322, 'DK58604JJI', 'normal', NULL, 0, 45, 112, NULL, NULL, 'Samsung Galaxy Note20 Ultra Unlocked', 'samsung-galaxy-note20-ultra-unlocked-dk58604jji', '16057288421.jpg', '1605728843MR3QjVcD.jpg', NULL, NULL, NULL, NULL, '#000000,#f0efed', '0,0', '0,0', 800, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">The Galaxy Note20 Ultra 5G is the phone for a business that works seamlessly with popular business tools, and lets you work however is best for you. A powerful battery with Super Fast Charging saves time and lasts all day.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;The S Pen is the most natural way to capture your thoughts and now gives new ways to present your ideas. And you can keep your business data and personal data protected thanks to a Secure Processor and the Samsung Knox security platform.</div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Responsive S Pen that feels like you\'re using a real pen and lets you save notes across your Galaxy devices while on the go</li><li style=\"margin: 0px; padding: 0px;\">Seamless integration with Microsoft makes your work accessible across devices, from your PC to your phone<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Transform working from home by turning your TV into your monitor wirelessly with Samsung DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li><li style=\"margin: 0px; padding: 0px;\">5G support to take advantage of the latest advancements in mobile networks<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></li></ul></div>', NULL, '<br>', 1, 53, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:47:22', '2021-02-13 22:25:29', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057288422.jpg\",\"16057288423.jpg\",\"16057288424.jpg\",\"16057288425.jpg\",\"16057288426.jpg\"]},{\"key\":1,\"color\":\"white\",\"images\":[\"16057288422.jpg\",\"16057288423.jpg\",\"16057288424.jpg\",\"16057288425.jpg\",\"16057288426.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(323, 'D3W8845pBE', 'normal', NULL, 0, 45, 113, NULL, NULL, 'Samsung Note 5', 'samsung-note-5-d3w8845pbe', '16057292541.jpg', '1605729254rJK4JsVG.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 179.99, 0, '<span style=\"color: rgb(15, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\">Service Provider:<span style=\"font-weight: 700;\">Verizon</span>&nbsp;&nbsp;|&nbsp; Color:<span style=\"font-weight: 700;\">Black</span>&nbsp;&nbsp;|&nbsp; Size:<span style=\"font-weight: 700;\">32 GB</span></span><span style=\"color: rgb(15, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"></span><div class=\"a-row a-spacing-top-base\" style=\"width: 1464px; color: rgb(15, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; margin-top: 12px !important;\"><div class=\"a-column a-span6\" style=\"margin-right: 29.2656px; float: left; min-height: 1px; overflow: visible; width: 716.594px;\"><div class=\"a-row a-spacing-base\" style=\"width: 716.594px; margin-bottom: 12px !important;\"><div class=\"a-section table-padding\" style=\"margin-bottom: 0px; margin-left: 12px;\"><table id=\"productDetails_detailBullets_sections1\" class=\"a-keyvalue prodDetTable\" role=\"presentation\" style=\"margin-bottom: 22px; width: 704px; border-bottom: 1px solid rgb(231, 231, 231); table-layout: fixed; border-spacing: 0px; padding: 0px;\"><tbody><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Product Dimensions</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">2.99 x 0.29 x 6.03 inches</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Item Weight</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">6 ounces</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">ASIN</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">B013XAPPIK</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Item model number</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Galaxy Note 5</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Batteries</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">1 Lithium Polymer batteries required. (included)</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\"><br></th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\"></td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Best Sellers Rank</th><td style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial;\">#44,271 in Cell Phones &amp; Accessories (<a href=\"https://www.amazon.com/gp/bestsellers/wireless/ref=pd_zg_ts_wireless\" style=\"color: rgb(0, 113, 133);\">See Top 100 in Cell Phones &amp; Accessories</a>)<br>#584 in&nbsp;<a href=\"https://www.amazon.com/gp/bestsellers/wireless/2407749011/ref=pd_zg_hrsr_wireless\" style=\"color: rgb(0, 113, 133);\">Unlocked Cell Phones</a><br></td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Is Discontinued By Manufacturer</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">No</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">OS</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Android</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">RAM</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">32 GB</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Additional Features</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Touchscreen, Built-in-GPS, Bluetooth-enabled, Dual-camera, LTE, Smartphone</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Display resolution</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">2560 x 1440 pixels</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Other display features</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Wireless Phone</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Device interface - primary</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Touchscreen with Stylus Support</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Other camera features</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">16 MP</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Form Factor</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Bar</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Colour</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Black</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Phone Standy Time (with data)</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">336 hours</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Included Components</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Pre-Installed SIM Card, Wall Charger, Samsung Note 5, USB Cable</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Manufacturer</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Samsung</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Date First Available</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">August 21, 2015</td></tr></tbody></table></div></div></div></div>', NULL, '<br>', 1, 48, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:54:14', '2021-02-13 22:25:26', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057292542.jpg\",\"16057292543.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(324, 'tch9393Boc', 'normal', NULL, 0, 45, 114, NULL, NULL, 'Samsung Galaxy Note 8 Unlocked', 'samsung-galaxy-note-8-unlocked-tch9393boc', '16057299091.jpg', '1605729909BQ5JKyXw.jpg', NULL, NULL, NULL, NULL, '#000000,#797081', '0,0', '0,0', 290, 0, '<p class=\"product-details__info-description--line product-details__info-description--line1\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Features a smarter S Pen, our largest Infinity Display and our best camera yet.</p><p class=\"product-details__info-description--line product-details__info-description--line2\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">With Galaxy Note8, you can create, share, and express your ideas more than ever before.</p>', NULL, '<br>', 1, 48, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:05:09', '2021-02-13 22:25:24', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057299092.jpg\"]},{\"key\":1,\"color\":\"Purple\",\"images\":[\"16057299092.jpg\",\"16057299093.jpg\",\"16057299094.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(325, '4Vr0120ueD', 'normal', NULL, 0, 45, 115, NULL, NULL, 'Samsung Galaxy Note 9  Unlocked', 'samsung-galaxy-note-9-unlocked-4vr0120ued', '16057313341.jpg', '1605731334DZxSvb8v.jpg', NULL, NULL, NULL, NULL, '#000000,#1f2c4d,#825b78', '0,0,0', '0,0,0', 390, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The largest battery in a note, ever When you have a long-lasting battery, you really can go all day and all night</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The Note9 has twice as much storage as the Note8, which means more music, more videos, more pictures, and less worry when it comes to space on your phone</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The Note9 gives you a quick network connection for incredibly fast streaming and downloading, so you can do more, uninterrupted. One Nano SIM and one MicroSD slot (up to 512GB)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Still amazing on screen, but now the S pen has more power off Screen Remotely control different applications and use the S pen to capture shots from far away, scroll, and play music</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">At 64 Inches, the Note9 has the largest screen of any Galaxy phone Perfect for gaming and streaming, our Super AMOLED display is bigger than ever before</span></li></ul>', NULL, '<br>', 1, 51, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:28:54', '2021-02-13 22:25:31', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057313342.jpg\",\"16057313343.jpg\",\"16057313344.jpg\",\"16057313345.jpg\",\"16057313346.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057313342.jpg\",\"16057313343.jpg\",\"16057313344.jpg\",\"16057313345.jpg\",\"16057313346.jpg\"]},{\"key\":2,\"color\":\"purple\",\"images\":[\"16057313342.jpg\",\"16057313343.jpg\",\"16057313344.jpg\",\"16057313345.jpg\",\"16057313346.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(326, 'Eh313435sM', 'normal', NULL, 0, 45, 77, NULL, NULL, 'Samsung Galaxy Note10 Unlocked', 'samsung-galaxy-note10-unlocked-eh313435sm', '16057314581.jpg', '1605731458HrPa0pXc.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 525, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(17, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Fully Unlocked: Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.), both domestically and internationally.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arm\'s length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 89, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:30:58', '2021-02-14 14:11:15', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057314582.jpg\",\"16057314583.jpg\",\"16057314584.jpg\",\"16057314585.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(327, 'NHU1461NI5', 'normal', NULL, 0, 45, 116, NULL, NULL, 'Samsung Galaxy Note10+ Unlocked', 'samsung-galaxy-note10-unlocked-nhu1461ni5', '16057315721.jpg', '1605731572yYcO1TOf.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 650, 0, '<br>', NULL, '<br>', 1, 69, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:32:52', '2021-02-14 08:51:00', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057315722.jpg\",\"16057315723.jpg\",\"16057315724.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(328, 'clF2061OAg', 'normal', NULL, 0, 45, 110, NULL, NULL, 'Samsung Galaxy S20 Unlocked', 'samsung-galaxy-s20-unlocked-clf2061oag', '16057322021.jpg', '1605732202SVXfYP5Y.jpg', NULL, NULL, NULL, NULL, '#e9bdca', '0', '0', 900, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">Getting work done on your smartphone should be easy. The Galaxy S20 integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20’s productivity experience<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></li><li style=\"margin: 0px; padding: 0px;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">5</span></li><li style=\"margin: 0px; padding: 0px;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li></ul></div>', NULL, '<br>', 1, 66, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:43:22', '2021-02-14 01:23:36', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Pink\",\"images\":[\"16057322022.jpg\",\"16057322023.jpg\",\"16057322024.jpg\",\"16057322025.jpg\",\"16057322026.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(329, 'wkE2204yBE', 'normal', NULL, 0, 45, 112, NULL, NULL, 'Samsung Galaxy S20 Ultra Unlocked', 'samsung-galaxy-s20-ultra-unlocked-wke2204ybe', '16057325281.jpg', '1605732528KB5iLVsm.jpg', NULL, NULL, NULL, NULL, '#000000,#c3e3fa,#68696b', '0,0,0', '0,0,0', 1050, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">Getting work done on your smartphone should be easy. The Galaxy S20+ integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20+’s productivity experience<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></li><li style=\"margin: 0px; padding: 0px;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">5</span></li><li style=\"margin: 0px; padding: 0px;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li></ul></div>', NULL, '<br>', 1, 85, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:48:48', '2021-02-13 22:23:59', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057325282.jpg\",\"16057325283.jpg\",\"16057325284.jpg\",\"16057325285.jpg\",\"16057325286.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057325282.jpg\",\"16057325283.jpg\",\"16057325284.jpg\",\"16057325285.jpg\",\"16057325286.jpg\"]},{\"key\":2,\"color\":\"Gray\",\"images\":[\"16057325282.jpg\",\"16057325283.jpg\",\"16057325284.jpg\",\"16057325285.jpg\",\"16057325286.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(330, 'Bgh2538Xla', 'normal', NULL, 0, 45, 117, NULL, NULL, 'Samsung Galaxy A01', 'samsung-galaxy-a01-bgh2538xla', '16057327561.jpg', '1605732756SNWt95z8.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 110, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Unlocked cell phones are compatible with GSM carriers such as AT&amp;T and T-Mobile, but are not compatible with CDMA carriers such as Verizon and Sprint.</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Please check if your GSM cellular carrier supports the bands for this model before purchasing, LTE may not be available in all areas:</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">This device may not include a US warranty as some manufacturers do not honor warranties for international items.</span></li></ul>', NULL, '<br>', 1, 64, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:52:36', '2021-02-13 22:23:59', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057327562.jpg\",\"16057327563.jpg\",\"16057327564.jpg\",\"16057327565.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(331, '4hV2836NbR', 'normal', NULL, 0, 45, 118, NULL, NULL, 'Samsung Galaxy A10e', 'samsung-galaxy-a10e-4hv2836nbr', '16057330531.jpg', '1605733053X202zho3.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 110, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">With a long-lasting battery, the Galaxy A10e gives you more time to post, talk, text, and share with friends and family</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Keep more with 32GB of built-in memory; Expand your memory up to 512GB with a Micro SD card</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Enjoy an edge to edge view on a 5. 83\" Infinity display</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Capture crisp, clear photos with an 8MP rear camera and a 5MP front Camera</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Wireless voice, data, and messaging services compatible with most major U.S. GSM and CDMA networks; Support for certain features and services such as VoWiFi and hotspot, vary by the wireless service provider</span></li></ul>', NULL, '<br>', 1, 64, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:57:33', '2021-02-13 22:24:01', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057330532.jpg\",\"16057330533.jpg\",\"16057330534.jpg\",\"16057330535.jpg\",\"16057330536.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(332, '5Zg3056nnK', 'normal', NULL, 0, 45, 119, NULL, NULL, 'Samsung Galaxy A10s Unlocked', 'samsung-galaxy-a10s-unlocked-5zg3056nnk', '16057340101.jpg', '1605734010HdMbOTQj.jpg', NULL, NULL, NULL, NULL, '#192849', '0', '0', 130, 0, '<span style=\"color: rgb(98, 96, 96); letter-spacing: 0.2px;\">6.2 inches of HD+ TFT screen for a phone you\'ll love to watch. Whether you\'re into sitcoms or MMORPGs, Galaxy A10s\'s Infinity-V Display changes the way you experience them by putting you right in the action. See how far the experience takes you on the v-cut screen.</span><br>', NULL, '<br>', 1, 59, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:13:30', '2021-02-13 22:23:58', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Blue\",\"images\":[\"16057340102.jpg\",\"16057340103.jpg\",\"16057340104.jpg\",\"16057340105.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(333, '1mD42031Y3', 'normal', NULL, 0, 45, 120, NULL, NULL, 'Samsung Galaxy A11', 'samsung-galaxy-a11-1md42031y3', '16057343051.jpg', '1605734305jYwXzPTc.jpg', NULL, NULL, NULL, NULL, '#68696b', '0', '0', 135, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: \" amazon=\"\" ember\",=\"\" arial,=\"\" sans-serif;\"=\"\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">6.4\" HD+ (720x1560) TFT LCD Infinity O, Dual SIM , 4000 mAh battery, Fingerprint (rear-mounted)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Internal Storage - 32GB, RAM - 2GB, Snapdragon 450, Octa-core 1.8 GHz, Android 10</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Triple Rear Camera: 13 MP, f/1.8, 28mm (wide), AF, 5 MP, f/2.2 (ultrawide), 2 MP, f/2.4, (depth), Front Camera: 8 MP, f/2.0</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">2G Bands: 850, 900, 1800, 1900MHz, 3G Bands: 850, 900, 1700, 1900, 4G LTE Bands: 1, 2, 3, 4, 5, 7, 8, 12, 17, 20, 28, 38, 40, 41</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">International Model Compatible with Most GSM Carriers like T-Mobile, AT&amp;T, MetroPCS, etc. Will NOT work with CDMA Carriers Such as Verizon, Sprint, Boost</span></li></ul>', NULL, '<br>', 1, 72, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:18:25', '2021-02-15 19:35:03', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Gray\",\"images\":[\"16057343052.jpg\"]}]', NULL, '19', '21', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(334, '3Li47638yC', 'normal', NULL, 0, 45, 121, NULL, NULL, 'Samsung Galaxy A20S', 'samsung-galaxy-a20s-3li47638yc', '16057349061.jpg', '1605734906AA2ea0wQ.jpg', NULL, NULL, NULL, NULL, '#131b42', '0', '0', 160, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">International Model - No Warranty in the US. Compatible with Most GSM Carriers like T-Mobile, AT&amp;T, MetroPCS, etc. Will NOT work with CDMA Carriers Such as Verizon, Sprint, Boost</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">RAM 3GB , ROM 32GB Internal Memory ; MicroSD (Up to 512GB), Android 9.0 (Pie), Qualcomm SDM450 Snapdragon 450 (14 nm), Octa-core 1.8 GHz Cortex-A53, Adreno 506</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Main Rear Camera: 13.0 MP + 8.0 MP + 5.0 MP , F1.8 , F2.2 , F2.2 ; Front Camera: 8.0 MP , F2.0, Non-removable Li-Po 4000 mAh battery</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">3G HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100, 4G LTE band 1(2100), 2(1900), 3(1800), 5(850), 7(2600), 8(900), 20(800), 28(700), 34(2000), 38(2600), 39(1900), 40(2300), 41(2500)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">6.5 inches, 103.7 cm2 (~81.9% screen-to-body ratio) 720 x 1560 pixels (HD+), 19.5:9 ratio (~264 ppi density) IPS LCD capacitive touchscreen, 16M colors</span></li></ul>', NULL, '<br>', 1, 70, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:28:26', '2021-02-13 22:24:00', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Blue\",\"images\":[\"16057349062.jpg\",\"16057349063.jpg\",\"16057349064.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(335, 'HQR4971TYH', 'normal', NULL, 0, 45, 122, NULL, NULL, 'Samsung Galaxy A21S', 'samsung-galaxy-a21s-hqr4971tyh', '16057350612.jpg', '1605735061s3J3HkfN.jpg', NULL, NULL, NULL, NULL, '#131b42', '0', '0', 190, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Display: 6.5 inches PLS TFT capacitive touchscreen, Resolution: 720 x 1600 pixels</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Memory: 64GB 4GB RAM, MicroSD up to 512GB - Dual SIM (Nano-SIM, dual stand-by)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Main Camera: 48 MP + 8 MP + 2 MP + 2 MP w/ LED flash, panorama, HDR - Selfie Camera: 13 MP</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Android -- Exynos 850 -- Mali-G52 -- Octa-core (4x2.0 GHz Cortex-A55 &amp; 4x2.0 GHz Cortex-A55)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Non-removable Li-Po 5000 mAh battery, Fast charging 15W</span></li></ul>', NULL, '<br>', 1, 74, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:31:01', '2021-02-15 18:24:25', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"white\",\"images\":[\"16057350611.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(336, 'OQB51840aC', 'normal', NULL, 0, 45, 123, NULL, NULL, 'Samsung Galaxy A31', 'samsung-galaxy-a31-oqb51840ac', '16057352751.jpg', '1605735275yZbT4Mbl.jpg', NULL, NULL, NULL, NULL, '#f0efed', '0', '0', 234, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Display: 6.4 inches Super AMOLED capacitive touchscreen, Resolution: 1080 x 2400 pixels</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Memory: 128GB, 4G RAM - MicroSD up to 512GB</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Android OS -- Mediatek MT6768 Helio P65 -- Octa-core (2x2.0 GHz &amp; 6x1.7 GHz) -- ARM Mali-G52</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Non-removable Li-Po 5000 mAh battery, Fast charging 15W</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Dimensions: 6.27 x 2.88 x 0.34 in, Weight: 6.53 oz</span></li></ul>', NULL, '<br>', 1, 82, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:34:35', '2021-02-13 22:24:05', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"white\",\"images\":[\"16057352752.jpg\",\"16057352753.jpg\",\"16057352754.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(337, 'eGx5311sew', 'normal', NULL, 0, 45, 124, NULL, NULL, 'Samsung Galaxy A51', 'samsung-galaxy-a51-egx5311sew', '16057354751.jpg', '1605735475TaFDocak.jpg', NULL, NULL, NULL, NULL, '#ffffff', '0', '0', 265, 0, '<br>', NULL, '<br>', 1, 187, 'Samsung Galaxy A51', 'Samsung Galaxy A51', '#000000', 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 0, 0, 0, '2020-11-19 03:37:55', '2021-02-15 00:55:14', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Prism Crush Black\",\"images\":[\"16057354752.jpg\",\"16057354753.jpg\",\"16057354754.jpg\",\"16057354755.jpg\"]}]', NULL, '19', '21', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(340, 'eXv9623uRE', 'normal', NULL, 0, 53, NULL, NULL, NULL, 'Nokia X', 'nokia-x-exv9623ure', '1606399800image.jpeg', '1606399800GkO7t6we.jpg', NULL, NULL, NULL, NULL, '#db1f1f', '0', '0', 299, 0, '<div style=\"text-align: right;\">lorem</div>', NULL, '<hr>', 1, 52, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-26 20:10:00', '2021-02-13 23:18:29', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":null,\"images\":[\"1606399800max16056515471.jpg\"]}]', NULL, '20', '18', '28', '', '2,3', 0, 0, 0, 0),
(341, 'JFM2025Cr', 'normal', NULL, 0, 52, NULL, NULL, NULL, 'RealMi 5i', 'realmi-5i-jfm2025cr', '1606462139realme-5i-pakistan-priceoye-2kepy.jpg', '1606462139wq8xXZPH.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 750, 0, '<br>', NULL, '<br>', 1, 187, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 0, 0, 0, '2020-11-27 13:28:59', '2021-02-14 20:36:32', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '25', '33', '', '1,3', 0, 0, 0, 0),
(342, 'G8J60546rS', 'normal', NULL, 0, 44, 70, NULL, NULL, 'Test Inks Product', 'test-inks-product-g8j60546rs', '1612976289Inkns.jpeg', '1612976289aHIT3RF3.jpg', NULL, NULL, NULL, NULL, '#191818,#1200db', '10,10', '1,2', 21, 0, 'Test product for Inks', 20, 'No', 1, 6, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-10 22:58:09', '2021-02-13 22:24:15', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '20', '14', '25', '', '1,2,3', 0, 0, 0, 0),
(343, 'Ino5062oCq', 'normal', NULL, 0, 44, 70, NULL, NULL, 'Test Talha 1', 'test-talha-1-ino5062ocq', '1612985201Boost .jpeg', '1612985201FFl0yA44.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 'Test&nbsp;', 20, 'Test', 1, 5, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2021-02-11 01:26:41', '2021-02-13 22:24:16', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '20', '13', '25', '', '1', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_clicks`
--

CREATE TABLE `product_clicks` (
  `id` int(191) NOT NULL,
  `product_id` int(191) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_clicks`
--

INSERT INTO `product_clicks` (`id`, `product_id`, `date`) VALUES
(20, 95, '2020-05-21'),
(21, 95, '2020-05-21'),
(22, 95, '2020-05-21'),
(23, 95, '2020-05-21'),
(24, 95, '2020-05-21'),
(25, 95, '2020-05-21'),
(26, 95, '2020-05-21'),
(27, 95, '2020-05-21'),
(28, 95, '2020-05-21'),
(29, 122, '2020-05-21'),
(320, 95, '2020-07-01'),
(340, 95, '2020-07-21'),
(341, 122, '2020-07-21'),
(484, 95, '2020-08-25'),
(485, 122, '2020-08-25'),
(491, 95, '2020-08-25'),
(492, 122, '2020-08-25'),
(567, 95, '2020-08-27'),
(576, 122, '2020-08-27'),
(728, 95, '2020-09-05'),
(767, 95, '2020-09-07'),
(798, 95, '2020-09-07'),
(799, 122, '2020-09-07'),
(817, 95, '2020-09-08'),
(819, 122, '2020-09-08'),
(906, 122, '2020-09-10'),
(949, 122, '2020-09-12'),
(966, 95, '2020-09-13'),
(982, 95, '2020-09-13'),
(986, 122, '2020-09-13'),
(1030, 122, '2020-09-13'),
(1127, 95, '2020-09-17'),
(1128, 122, '2020-09-17'),
(1129, 122, '2020-09-17'),
(1130, 95, '2020-09-17'),
(1133, 95, '2020-09-17'),
(1134, 122, '2020-09-17'),
(1136, 95, '2020-09-17'),
(1137, 122, '2020-09-17'),
(1139, 122, '2020-09-17'),
(1140, 95, '2020-09-17'),
(1187, 95, '2020-09-17'),
(1188, 122, '2020-09-17'),
(1192, 95, '2020-09-17'),
(1193, 95, '2020-09-17'),
(1194, 122, '2020-09-17'),
(1195, 122, '2020-09-17'),
(1197, 95, '2020-09-17'),
(1198, 122, '2020-09-17'),
(1199, 95, '2020-09-17'),
(1200, 122, '2020-09-17'),
(1205, 95, '2020-09-18'),
(1206, 122, '2020-09-18'),
(1355, 95, '2020-09-23'),
(1576, 122, '2020-09-30'),
(1577, 95, '2020-09-30'),
(1592, 122, '2020-09-30'),
(1639, 122, '2020-09-30'),
(1640, 95, '2020-09-30'),
(1647, 122, '2020-10-01'),
(1750, 122, '2020-10-02'),
(1751, 122, '2020-10-02'),
(1752, 95, '2020-10-02'),
(1753, 95, '2020-10-02'),
(1800, 95, '2020-10-03'),
(1889, 122, '2020-10-05'),
(1962, 122, '2020-10-07'),
(2018, 95, '2020-10-09'),
(2051, 95, '2020-10-10'),
(2093, 95, '2020-10-12'),
(2125, 95, '2020-10-13'),
(2147, 122, '2020-10-14'),
(2183, 95, '2020-10-15'),
(2185, 122, '2020-10-15'),
(2199, 122, '2020-10-16'),
(2229, 95, '2020-10-17'),
(2290, 95, '2020-10-19'),
(2295, 122, '2020-10-19'),
(2298, 95, '2020-10-19'),
(2300, 95, '2020-10-19'),
(2329, 122, '2020-10-20'),
(2447, 122, '2020-10-22'),
(2501, 122, '2020-10-24'),
(2577, 95, '2020-10-25'),
(2581, 122, '2020-10-25'),
(2723, 122, '2020-10-29'),
(2724, 95, '2020-10-29'),
(2756, 122, '2020-11-01'),
(2776, 95, '2020-11-01'),
(2892, 95, '2020-11-08'),
(2896, 122, '2020-11-08'),
(3007, 95, '2020-11-13'),
(3012, 95, '2020-11-13'),
(3024, 122, '2020-11-13'),
(3026, 122, '2020-11-13'),
(3032, 122, '2020-11-14'),
(3042, 95, '2020-11-14'),
(3043, 122, '2020-11-14'),
(3090, 95, '2020-11-17'),
(3105, 300, '2020-11-17'),
(3106, 300, '2020-11-17'),
(3107, 300, '2020-11-17'),
(3108, 300, '2020-11-17'),
(3109, 301, '2020-11-17'),
(3110, 301, '2020-11-17'),
(3111, 302, '2020-11-17'),
(3112, 303, '2020-11-17'),
(3113, 302, '2020-11-17'),
(3114, 303, '2020-11-17'),
(3115, 301, '2020-11-17'),
(3116, 304, '2020-11-17'),
(3117, 305, '2020-11-17'),
(3118, 303, '2020-11-18'),
(3119, 310, '2020-11-18'),
(3120, 310, '2020-11-18'),
(3121, 310, '2020-11-18'),
(3122, 310, '2020-11-18'),
(3123, 310, '2020-11-18'),
(3124, 301, '2020-11-18'),
(3125, 302, '2020-11-18'),
(3126, 304, '2020-11-18'),
(3127, 303, '2020-11-18'),
(3128, 308, '2020-11-18'),
(3129, 319, '2020-11-18'),
(3130, 309, '2020-11-18'),
(3131, 308, '2020-11-18'),
(3132, 306, '2020-11-18'),
(3133, 300, '2020-11-18'),
(3134, 301, '2020-11-18'),
(3135, 302, '2020-11-18'),
(3136, 316, '2020-11-18'),
(3137, 304, '2020-11-18'),
(3138, 303, '2020-11-18'),
(3139, 321, '2020-11-18'),
(3140, 305, '2020-11-18'),
(3141, 310, '2020-11-18'),
(3142, 331, '2020-11-18'),
(3143, 332, '2020-11-18'),
(3144, 324, '2020-11-18'),
(3145, 321, '2020-11-18'),
(3146, 330, '2020-11-18'),
(3147, 320, '2020-11-18'),
(3148, 328, '2020-11-18'),
(3149, 325, '2020-11-18'),
(3150, 336, '2020-11-18'),
(3151, 337, '2020-11-18'),
(3152, 319, '2020-11-18'),
(3153, 334, '2020-11-18'),
(3154, 323, '2020-11-18'),
(3155, 333, '2020-11-19'),
(3156, 329, '2020-11-19'),
(3157, 322, '2020-11-19'),
(3158, 335, '2020-11-19'),
(3159, 326, '2020-11-19'),
(3160, 304, '2020-11-19'),
(3161, 314, '2020-11-19'),
(3162, 316, '2020-11-19'),
(3163, 317, '2020-11-19'),
(3164, 311, '2020-11-19'),
(3165, 300, '2020-11-19'),
(3166, 309, '2020-11-19'),
(3167, 311, '2020-11-19'),
(3168, 306, '2020-11-19'),
(3169, 308, '2020-11-19'),
(3170, 307, '2020-11-19'),
(3171, 306, '2020-11-19'),
(3172, 303, '2020-11-19'),
(3173, 310, '2020-11-19'),
(3174, 317, '2020-11-19'),
(3175, 313, '2020-11-19'),
(3176, 316, '2020-11-19'),
(3177, 314, '2020-11-19'),
(3178, 333, '2020-11-19'),
(3179, 334, '2020-11-19'),
(3180, 335, '2020-11-19'),
(3181, 336, '2020-11-19'),
(3182, 310, '2020-11-19'),
(3183, 337, '2020-11-19'),
(3184, 304, '2020-11-19'),
(3185, 317, '2020-11-19'),
(3186, 313, '2020-11-19'),
(3187, 308, '2020-11-19'),
(3188, 301, '2020-11-19'),
(3189, 316, '2020-11-19'),
(3190, 314, '2020-11-19'),
(3191, 306, '2020-11-19'),
(3192, 307, '2020-11-19'),
(3193, 333, '2020-11-19'),
(3194, 334, '2020-11-19'),
(3195, 335, '2020-11-19'),
(3196, 307, '2020-11-19'),
(3197, 336, '2020-11-19'),
(3198, 337, '2020-11-19'),
(3199, 318, '2020-11-19'),
(3200, 319, '2020-11-19'),
(3201, 317, '2020-11-19'),
(3202, 313, '2020-11-19'),
(3203, 316, '2020-11-19'),
(3204, 333, '2020-11-19'),
(3205, 334, '2020-11-19'),
(3206, 335, '2020-11-19'),
(3207, 336, '2020-11-19'),
(3208, 314, '2020-11-19'),
(3209, 337, '2020-11-19'),
(3210, 302, '2020-11-19'),
(3211, 302, '2020-11-19'),
(3212, 302, '2020-11-19'),
(3213, 337, '2020-11-19'),
(3214, 302, '2020-11-19'),
(3215, 302, '2020-11-19'),
(3216, 302, '2020-11-19'),
(3217, 302, '2020-11-19'),
(3218, 309, '2020-11-19'),
(3219, 324, '2020-11-19'),
(3220, 323, '2020-11-19'),
(3221, 311, '2020-11-19'),
(3222, 312, '2020-11-19'),
(3223, 337, '2020-11-19'),
(3224, 337, '2020-11-19'),
(3225, 325, '2020-11-19'),
(3226, 321, '2020-11-19'),
(3227, 315, '2020-11-19'),
(3228, 322, '2020-11-19'),
(3229, 329, '2020-11-19'),
(3230, 320, '2020-11-19'),
(3231, 337, '2020-11-19'),
(3232, 328, '2020-11-19'),
(3233, 337, '2020-11-19'),
(3234, 336, '2020-11-19'),
(3235, 326, '2020-11-19'),
(3236, 311, '2020-11-20'),
(3237, 310, '2020-11-20'),
(3238, 301, '2020-11-20'),
(3239, 330, '2020-11-20'),
(3240, 331, '2020-11-20'),
(3241, 322, '2020-11-20'),
(3242, 332, '2020-11-20'),
(3243, 327, '2020-11-20'),
(3244, 329, '2020-11-20'),
(3245, 305, '2020-11-20'),
(3246, 326, '2020-11-20'),
(3247, 328, '2020-11-20'),
(3248, 309, '2020-11-20'),
(3249, 302, '2020-11-20'),
(3250, 308, '2020-11-20'),
(3251, 300, '2020-11-20'),
(3252, 301, '2020-11-20'),
(3253, 302, '2020-11-20'),
(3254, 304, '2020-11-20'),
(3255, 303, '2020-11-20'),
(3256, 303, '2020-11-20'),
(3257, 305, '2020-11-20'),
(3258, 306, '2020-11-20'),
(3259, 309, '2020-11-20'),
(3260, 309, '2020-11-20'),
(3261, 309, '2020-11-20'),
(3262, 300, '2020-11-20'),
(3263, 307, '2020-11-20'),
(3264, 310, '2020-11-20'),
(3265, 331, '2020-11-20'),
(3266, 332, '2020-11-20'),
(3267, 311, '2020-11-20'),
(3268, 312, '2020-11-20'),
(3269, 324, '2020-11-20'),
(3270, 325, '2020-11-20'),
(3271, 322, '2020-11-20'),
(3272, 305, '2020-11-20'),
(3273, 318, '2020-11-20'),
(3274, 308, '2020-11-20'),
(3275, 319, '2020-11-20'),
(3276, 305, '2020-11-20'),
(3277, 310, '2020-11-20'),
(3278, 315, '2020-11-21'),
(3279, 319, '2020-11-21'),
(3280, 321, '2020-11-21'),
(3281, 320, '2020-11-21'),
(3282, 306, '2020-11-21'),
(3283, 315, '2020-11-21'),
(3284, 323, '2020-11-21'),
(3285, 336, '2020-11-21'),
(3286, 335, '2020-11-21'),
(3287, 337, '2020-11-21'),
(3288, 310, '2020-11-21'),
(3289, 305, '2020-11-21'),
(3290, 326, '2020-11-21'),
(3291, 330, '2020-11-21'),
(3292, 329, '2020-11-21'),
(3293, 328, '2020-11-21'),
(3294, 334, '2020-11-21'),
(3295, 333, '2020-11-21'),
(3296, 327, '2020-11-21'),
(3297, 331, '2020-11-21'),
(3298, 312, '2020-11-21'),
(3299, 305, '2020-11-21'),
(3300, 336, '2020-11-21'),
(3301, 336, '2020-11-21'),
(3302, 333, '2020-11-21'),
(3303, 327, '2020-11-21'),
(3304, 334, '2020-11-21'),
(3305, 314, '2020-11-21'),
(3306, 95, '2020-11-21'),
(3307, 309, '2020-11-21'),
(3308, 308, '2020-11-21'),
(3309, 309, '2020-11-21'),
(3310, 122, '2020-11-22'),
(3311, 303, '2020-11-22'),
(3312, 311, '2020-11-22'),
(3313, 309, '2020-11-22'),
(3314, 328, '2020-11-22'),
(3315, 334, '2020-11-22'),
(3316, 309, '2020-11-22'),
(3317, 331, '2020-11-22'),
(3318, 122, '2020-11-22'),
(3319, 330, '2020-11-22'),
(3320, 323, '2020-11-22'),
(3321, 303, '2020-11-22'),
(3322, 324, '2020-11-22'),
(3323, 332, '2020-11-22'),
(3324, 326, '2020-11-22'),
(3325, 333, '2020-11-22'),
(3326, 337, '2020-11-22'),
(3327, 330, '2020-11-22'),
(3328, 331, '2020-11-22'),
(3329, 329, '2020-11-22'),
(3330, 327, '2020-11-22'),
(3331, 336, '2020-11-22'),
(3332, 326, '2020-11-22'),
(3333, 335, '2020-11-23'),
(3334, 334, '2020-11-23'),
(3335, 316, '2020-11-23'),
(3336, 329, '2020-11-23'),
(3337, 306, '2020-11-23'),
(3338, 329, '2020-11-23'),
(3339, 95, '2020-11-23'),
(3340, 337, '2020-11-23'),
(3341, 337, '2020-11-23'),
(3342, 303, '2020-11-23'),
(3343, 303, '2020-11-23'),
(3344, 325, '2020-11-23'),
(3345, 337, '2020-11-23'),
(3346, 310, '2020-11-23'),
(3347, 316, '2020-11-23'),
(3348, 307, '2020-11-23'),
(3349, 311, '2020-11-23'),
(3350, 328, '2020-11-24'),
(3351, 337, '2020-11-24'),
(3352, 329, '2020-11-24'),
(3353, 308, '2020-11-24'),
(3354, 329, '2020-11-24'),
(3355, 308, '2020-11-24'),
(3356, 324, '2020-11-24'),
(3357, 322, '2020-11-24'),
(3358, 332, '2020-11-24'),
(3359, 323, '2020-11-24'),
(3360, 335, '2020-11-24'),
(3361, 337, '2020-11-24'),
(3362, 321, '2020-11-25'),
(3363, 305, '2020-11-25'),
(3364, 337, '2020-11-25'),
(3365, 329, '2020-11-25'),
(3366, 309, '2020-11-25'),
(3367, 307, '2020-11-25'),
(3368, 313, '2020-11-25'),
(3369, 304, '2020-11-25'),
(3370, 332, '2020-11-25'),
(3371, 337, '2020-11-25'),
(3372, 326, '2020-11-25'),
(3374, 329, '2020-11-25'),
(3375, 314, '2020-11-25'),
(3376, 337, '2020-11-25'),
(3377, 337, '2020-11-25'),
(3378, 309, '2020-11-25'),
(3379, 309, '2020-11-25'),
(3380, 309, '2020-11-25'),
(3381, 337, '2020-11-25'),
(3382, 326, '2020-11-25'),
(3383, 309, '2020-11-25'),
(3384, 312, '2020-11-25'),
(3385, 305, '2020-11-25'),
(3386, 309, '2020-11-25'),
(3387, 313, '2020-11-25'),
(3389, 335, '2020-11-25'),
(3390, 312, '2020-11-25'),
(3391, 326, '2020-11-25'),
(3392, 336, '2020-11-25'),
(3393, 304, '2020-11-26'),
(3394, 337, '2020-11-26'),
(3395, 337, '2020-11-26'),
(3396, 334, '2020-11-26'),
(3397, 337, '2020-11-26'),
(3398, 329, '2020-11-26'),
(3399, 335, '2020-11-26'),
(3400, 309, '2020-11-26'),
(3401, 309, '2020-11-26'),
(3402, 309, '2020-11-26'),
(3403, 326, '2020-11-26'),
(3404, 309, '2020-11-26'),
(3405, 337, '2020-11-26'),
(3406, 309, '2020-11-26'),
(3407, 326, '2020-11-26'),
(3408, 309, '2020-11-26'),
(3409, 309, '2020-11-26'),
(3410, 309, '2020-11-26'),
(3411, 309, '2020-11-26'),
(3412, 309, '2020-11-26'),
(3413, 309, '2020-11-26'),
(3414, 311, '2020-11-26'),
(3415, 309, '2020-11-26'),
(3416, 309, '2020-11-26'),
(3417, 309, '2020-11-26'),
(3418, 309, '2020-11-26'),
(3419, 309, '2020-11-26'),
(3420, 309, '2020-11-26'),
(3421, 309, '2020-11-26'),
(3422, 309, '2020-11-26'),
(3423, 309, '2020-11-26'),
(3424, 309, '2020-11-26'),
(3425, 311, '2020-11-26'),
(3426, 311, '2020-11-26'),
(3427, 95, '2020-11-26'),
(3428, 95, '2020-11-26'),
(3429, 95, '2020-11-26'),
(3430, 95, '2020-11-26'),
(3431, 95, '2020-11-26'),
(3432, 337, '2020-11-26'),
(3433, 95, '2020-11-26'),
(3434, 95, '2020-11-26'),
(3435, 337, '2020-11-26'),
(3436, 335, '2020-11-26'),
(3437, 95, '2020-11-26'),
(3438, 330, '2020-11-26'),
(3439, 336, '2020-11-26'),
(3440, 95, '2020-11-26'),
(3441, 331, '2020-11-26'),
(3442, 337, '2020-11-26'),
(3443, 330, '2020-11-26'),
(3444, 95, '2020-11-26'),
(3445, 95, '2020-11-26'),
(3446, 334, '2020-11-26'),
(3447, 316, '2020-11-26'),
(3448, 95, '2020-11-26'),
(3449, 95, '2020-11-26'),
(3450, 95, '2020-11-26'),
(3451, 340, '2020-11-26'),
(3452, 95, '2020-11-26'),
(3453, 95, '2020-11-26'),
(3454, 95, '2020-11-26'),
(3455, 309, '2020-11-26'),
(3456, 306, '2020-11-26'),
(3457, 319, '2020-11-26'),
(3458, 310, '2020-11-26'),
(3459, 317, '2020-11-26'),
(3460, 330, '2020-11-27'),
(3461, 333, '2020-11-27'),
(3462, 95, '2020-11-27'),
(3463, 331, '2020-11-27'),
(3464, 337, '2020-11-27'),
(3465, 329, '2020-11-27'),
(3466, 319, '2020-11-27'),
(3467, 95, '2020-11-27'),
(3468, 95, '2020-11-27'),
(3469, 95, '2020-11-27'),
(3470, 95, '2020-11-27'),
(3471, 317, '2020-11-27'),
(3472, 305, '2020-11-27'),
(3473, 341, '2020-11-27'),
(3474, 325, '2020-11-27'),
(3475, 337, '2020-11-27'),
(3476, 337, '2020-11-27'),
(3477, 95, '2020-11-27'),
(3478, 336, '2020-11-27'),
(3479, 95, '2020-11-27'),
(3480, 95, '2020-11-27'),
(3481, 341, '2020-11-27'),
(3482, 329, '2020-11-27'),
(3483, 95, '2020-11-27'),
(3484, 95, '2020-11-27'),
(3485, 95, '2020-11-27'),
(3486, 95, '2020-11-27'),
(3487, 95, '2020-11-27'),
(3488, 95, '2020-11-27'),
(3489, 95, '2020-11-27'),
(3490, 95, '2020-11-27'),
(3491, 95, '2020-11-27'),
(3492, 95, '2020-11-27'),
(3493, 95, '2020-11-27'),
(3494, 95, '2020-11-27'),
(3495, 95, '2020-11-27'),
(3496, 314, '2020-11-27'),
(3497, 325, '2020-11-27'),
(3498, 341, '2020-11-27'),
(3499, 309, '2020-11-27'),
(3500, 336, '2020-11-27'),
(3501, 341, '2020-11-27'),
(3502, 315, '2020-11-27'),
(3503, 326, '2020-11-27'),
(3504, 302, '2020-11-27'),
(3505, 95, '2020-11-27'),
(3506, 95, '2020-11-27'),
(3507, 341, '2020-11-27'),
(3508, 332, '2020-11-27'),
(3509, 95, '2020-11-27'),
(3510, 341, '2020-11-27'),
(3511, 340, '2020-11-27'),
(3512, 95, '2020-11-27'),
(3513, 95, '2020-11-27'),
(3514, 95, '2020-11-27'),
(3515, 95, '2020-11-27'),
(3516, 95, '2020-11-27'),
(3517, 95, '2020-11-27'),
(3518, 95, '2020-11-27'),
(3519, 95, '2020-11-27'),
(3520, 95, '2020-11-27'),
(3521, 95, '2020-11-27'),
(3522, 95, '2020-11-27'),
(3523, 95, '2020-11-27'),
(3524, 95, '2020-11-27'),
(3525, 95, '2020-11-27'),
(3526, 300, '2020-11-27'),
(3527, 95, '2020-11-27'),
(3528, 95, '2020-11-27'),
(3529, 95, '2020-11-27'),
(3530, 95, '2020-11-27'),
(3531, 95, '2020-11-27'),
(3532, 95, '2020-11-27'),
(3533, 95, '2020-11-27'),
(3534, 341, '2020-11-27'),
(3535, 95, '2020-11-27'),
(3536, 95, '2020-11-27'),
(3537, 95, '2020-11-27'),
(3538, 316, '2020-11-27'),
(3539, 341, '2020-11-27'),
(3540, 341, '2020-11-27'),
(3541, 95, '2020-11-27'),
(3542, 309, '2020-11-27'),
(3543, 337, '2020-11-28'),
(3544, 335, '2020-11-28'),
(3545, 327, '2020-11-28'),
(3546, 311, '2020-11-28'),
(3547, 95, '2020-11-28'),
(3548, 95, '2020-11-28'),
(3549, 95, '2020-11-28'),
(3550, 95, '2020-11-28'),
(3551, 95, '2020-11-28'),
(3552, 95, '2020-11-28'),
(3553, 95, '2020-11-28'),
(3554, 95, '2020-11-28'),
(3555, 95, '2020-11-28'),
(3556, 95, '2020-11-28'),
(3557, 95, '2020-11-28'),
(3558, 95, '2020-11-28'),
(3559, 95, '2020-11-28'),
(3560, 95, '2020-11-28'),
(3561, 95, '2020-11-28'),
(3562, 95, '2020-11-28'),
(3563, 95, '2020-11-28'),
(3564, 95, '2020-11-28'),
(3565, 95, '2020-11-28'),
(3566, 95, '2020-11-28'),
(3567, 341, '2020-11-28'),
(3568, 95, '2020-11-28'),
(3569, 95, '2020-11-28'),
(3570, 341, '2020-11-28'),
(3571, 337, '2020-11-28'),
(3572, 95, '2020-11-28'),
(3573, 341, '2020-11-28'),
(3574, 95, '2020-11-28'),
(3575, 95, '2020-11-28'),
(3576, 95, '2020-11-28'),
(3577, 95, '2020-11-28'),
(3578, 95, '2020-11-28'),
(3579, 329, '2020-11-28'),
(3580, 95, '2020-11-28'),
(3581, 95, '2020-11-28'),
(3582, 95, '2020-11-28'),
(3583, 95, '2020-11-28'),
(3584, 95, '2020-11-28'),
(3585, 95, '2020-11-28'),
(3586, 95, '2020-11-28'),
(3587, 341, '2020-11-28'),
(3588, 340, '2020-11-28'),
(3589, 337, '2020-11-28'),
(3590, 340, '2020-11-28'),
(3591, 341, '2020-11-28'),
(3592, 95, '2020-11-28'),
(3593, 95, '2020-11-28'),
(3594, 95, '2020-11-28'),
(3595, 95, '2020-11-28'),
(3596, 95, '2020-11-28'),
(3597, 95, '2020-11-28'),
(3598, 95, '2020-11-28'),
(3599, 95, '2020-11-28'),
(3600, 122, '2020-11-28'),
(3601, 95, '2020-11-28'),
(3602, 95, '2020-11-28'),
(3603, 95, '2020-11-28'),
(3604, 95, '2020-11-28'),
(3605, 95, '2020-11-28'),
(3606, 95, '2020-11-28'),
(3607, 95, '2020-11-28'),
(3608, 95, '2020-11-28'),
(3609, 309, '2020-11-28'),
(3610, 309, '2020-11-28'),
(3611, 95, '2020-11-28'),
(3612, 329, '2020-11-28'),
(3613, 314, '2020-11-28'),
(3614, 95, '2020-11-28'),
(3615, 309, '2020-11-28'),
(3616, 309, '2020-11-28'),
(3617, 309, '2020-11-28'),
(3618, 309, '2020-11-28'),
(3619, 309, '2020-11-28'),
(3620, 341, '2020-11-28'),
(3621, 341, '2020-11-28'),
(3622, 341, '2020-11-28'),
(3623, 337, '2020-11-28'),
(3624, 337, '2020-11-28'),
(3625, 334, '2020-11-28'),
(3626, 334, '2020-11-28'),
(3627, 341, '2020-11-28'),
(3628, 334, '2020-11-28'),
(3629, 334, '2020-11-28'),
(3630, 337, '2020-11-28'),
(3631, 309, '2020-11-28'),
(3632, 95, '2020-11-28'),
(3633, 341, '2020-11-28'),
(3634, 341, '2020-11-28'),
(3635, 337, '2020-11-28'),
(3636, 328, '2020-11-28'),
(3637, 322, '2020-11-28'),
(3638, 314, '2020-11-28'),
(3639, 314, '2020-11-28'),
(3640, 312, '2020-11-28'),
(3641, 331, '2020-11-28'),
(3642, 302, '2020-11-28'),
(3643, 331, '2020-11-28'),
(3644, 311, '2020-11-28'),
(3645, 95, '2020-11-28'),
(3646, 305, '2020-11-28'),
(3647, 310, '2020-11-28'),
(3648, 309, '2020-11-28'),
(3649, 309, '2020-11-28'),
(3650, 341, '2020-11-28'),
(3651, 322, '2020-11-28'),
(3652, 95, '2020-11-28'),
(3653, 324, '2020-11-28'),
(3654, 310, '2020-11-28'),
(3655, 333, '2020-11-28'),
(3656, 328, '2020-11-28'),
(3657, 331, '2020-11-28'),
(3658, 305, '2020-11-28'),
(3659, 95, '2020-11-28'),
(3660, 330, '2020-11-28'),
(3661, 122, '2020-11-28'),
(3662, 300, '2020-11-29'),
(3663, 306, '2020-11-29'),
(3664, 337, '2020-11-29'),
(3665, 95, '2020-11-29'),
(3666, 323, '2020-11-29'),
(3667, 329, '2020-11-29'),
(3668, 306, '2020-11-29'),
(3669, 314, '2020-11-29'),
(3670, 315, '2020-11-29'),
(3671, 306, '2020-11-29'),
(3672, 321, '2020-11-29'),
(3673, 332, '2020-11-29'),
(3674, 324, '2020-11-29'),
(3675, 336, '2020-11-29'),
(3676, 326, '2020-11-29'),
(3677, 315, '2020-11-29'),
(3678, 327, '2020-11-29'),
(3679, 328, '2020-11-29'),
(3680, 334, '2020-11-29'),
(3681, 303, '2020-11-29'),
(3682, 302, '2020-11-29'),
(3683, 308, '2020-11-29'),
(3684, 305, '2020-11-29'),
(3685, 307, '2020-11-29'),
(3686, 300, '2020-11-29'),
(3687, 310, '2020-11-29'),
(3688, 311, '2020-11-29'),
(3689, 301, '2020-11-29'),
(3690, 304, '2020-11-29'),
(3691, 311, '2020-11-29'),
(3692, 327, '2020-11-29'),
(3693, 309, '2020-11-29'),
(3694, 311, '2020-11-29'),
(3695, 311, '2020-11-29'),
(3696, 311, '2020-11-29'),
(3697, 302, '2020-11-29'),
(3698, 329, '2020-11-29'),
(3699, 321, '2020-11-29'),
(3700, 304, '2020-11-29'),
(3701, 304, '2020-11-29'),
(3702, 308, '2020-11-30'),
(3703, 337, '2020-11-30'),
(3704, 301, '2020-11-30'),
(3705, 337, '2020-11-30'),
(3706, 320, '2020-11-30'),
(3707, 95, '2020-11-30'),
(3708, 309, '2020-11-30'),
(3709, 309, '2020-11-30'),
(3710, 329, '2020-11-30'),
(3711, 341, '2020-11-30'),
(3712, 317, '2020-11-30'),
(3713, 309, '2020-11-30'),
(3714, 309, '2020-11-30'),
(3715, 309, '2020-11-30'),
(3716, 309, '2020-11-30'),
(3717, 309, '2020-11-30'),
(3718, 337, '2020-11-30'),
(3719, 311, '2020-11-30'),
(3720, 337, '2020-11-30'),
(3721, 341, '2020-11-30'),
(3722, 311, '2020-11-30'),
(3723, 337, '2020-11-30'),
(3724, 337, '2020-11-30'),
(3725, 95, '2020-11-30'),
(3726, 309, '2020-11-30'),
(3727, 323, '2020-11-30'),
(3728, 95, '2020-11-30'),
(3729, 309, '2020-11-30'),
(3730, 305, '2020-11-30'),
(3731, 306, '2020-11-30'),
(3732, 336, '2020-11-30'),
(3733, 333, '2020-11-30'),
(3734, 336, '2020-11-30'),
(3735, 321, '2020-12-01'),
(3736, 307, '2020-12-01'),
(3737, 337, '2020-12-01'),
(3738, 320, '2020-12-01'),
(3739, 326, '2020-12-01'),
(3740, 316, '2020-12-01'),
(3741, 309, '2020-12-01'),
(3742, 341, '2020-12-01'),
(3743, 329, '2020-12-01'),
(3744, 309, '2020-12-01'),
(3745, 309, '2020-12-01'),
(3746, 309, '2020-12-01'),
(3747, 340, '2020-12-01'),
(3748, 341, '2020-12-01'),
(3749, 95, '2020-12-01'),
(3750, 122, '2020-12-01'),
(3751, 95, '2020-12-01'),
(3752, 95, '2020-12-01'),
(3753, 310, '2020-12-01'),
(3754, 329, '2020-12-02'),
(3755, 341, '2020-12-02'),
(3756, 325, '2020-12-02'),
(3757, 319, '2020-12-02'),
(3758, 95, '2020-12-02'),
(3759, 340, '2020-12-02'),
(3760, 337, '2020-12-02'),
(3761, 307, '2020-12-02'),
(3762, 337, '2020-12-02'),
(3763, 309, '2020-12-02'),
(3764, 305, '2020-12-02'),
(3765, 329, '2020-12-02'),
(3766, 340, '2020-12-02'),
(3767, 303, '2020-12-02'),
(3768, 311, '2020-12-02'),
(3769, 302, '2020-12-02'),
(3770, 310, '2020-12-02'),
(3771, 320, '2020-12-02'),
(3772, 340, '2020-12-02'),
(3773, 341, '2020-12-02'),
(3774, 330, '2020-12-02'),
(3775, 331, '2020-12-02'),
(3776, 332, '2020-12-02'),
(3777, 326, '2020-12-02'),
(3778, 327, '2020-12-02'),
(3779, 329, '2020-12-02'),
(3780, 328, '2020-12-02'),
(3781, 340, '2020-12-02'),
(3782, 341, '2020-12-02'),
(3783, 320, '2020-12-02'),
(3784, 340, '2020-12-02'),
(3785, 95, '2020-12-02'),
(3786, 311, '2020-12-02'),
(3787, 310, '2020-12-02'),
(3788, 340, '2020-12-02'),
(3789, 337, '2020-12-02'),
(3790, 311, '2020-12-02'),
(3791, 328, '2020-12-03'),
(3792, 311, '2020-12-03'),
(3793, 307, '2020-12-03'),
(3794, 308, '2020-12-03'),
(3795, 337, '2020-12-03'),
(3796, 311, '2020-12-03'),
(3797, 309, '2020-12-03'),
(3798, 308, '2020-12-03'),
(3799, 300, '2020-12-03'),
(3800, 306, '2020-12-03'),
(3801, 307, '2020-12-03'),
(3802, 324, '2020-12-03'),
(3803, 325, '2020-12-03'),
(3804, 322, '2020-12-03'),
(3805, 318, '2020-12-03'),
(3806, 319, '2020-12-03'),
(3807, 321, '2020-12-03'),
(3808, 320, '2020-12-03'),
(3809, 312, '2020-12-03'),
(3810, 315, '2020-12-03'),
(3811, 323, '2020-12-03'),
(3812, 312, '2020-12-03'),
(3813, 310, '2020-12-03'),
(3814, 326, '2020-12-03'),
(3815, 329, '2020-12-03'),
(3816, 95, '2020-12-03'),
(3817, 329, '2020-12-03'),
(3818, 95, '2020-12-03'),
(3819, 95, '2020-12-03'),
(3820, 95, '2020-12-03'),
(3821, 311, '2020-12-03'),
(3822, 95, '2020-12-03'),
(3823, 95, '2020-12-03'),
(3824, 95, '2020-12-03'),
(3825, 308, '2020-12-03'),
(3826, 337, '2020-12-03'),
(3827, 95, '2020-12-03'),
(3828, 301, '2020-12-03'),
(3829, 341, '2020-12-03'),
(3830, 316, '2020-12-03'),
(3834, 308, '2020-12-03'),
(3836, 310, '2020-12-03'),
(3837, 328, '2020-12-03'),
(3838, 95, '2020-12-03'),
(3839, 306, '2020-12-03'),
(3840, 332, '2020-12-03'),
(3841, 326, '2020-12-03'),
(3842, 327, '2020-12-03'),
(3843, 335, '2020-12-03'),
(3844, 330, '2020-12-03'),
(3845, 333, '2020-12-03'),
(3846, 334, '2020-12-03'),
(3847, 336, '2020-12-03'),
(3848, 333, '2020-12-03'),
(3849, 331, '2020-12-03'),
(3850, 337, '2020-12-03'),
(3851, 301, '2020-12-03'),
(3852, 334, '2020-12-03'),
(3853, 329, '2020-12-03'),
(3854, 307, '2020-12-03'),
(3855, 340, '2020-12-03'),
(3856, 335, '2020-12-03'),
(3857, 309, '2020-12-03'),
(3858, 303, '2020-12-03'),
(3859, 336, '2020-12-03'),
(3860, 311, '2020-12-03'),
(3861, 308, '2020-12-03'),
(3862, 305, '2020-12-03'),
(3863, 301, '2020-12-03'),
(3864, 337, '2020-12-03'),
(3865, 311, '2020-12-03'),
(3866, 341, '2020-12-03'),
(3867, 300, '2020-12-03'),
(3868, 337, '2020-12-03'),
(3869, 337, '2020-12-03'),
(3870, 309, '2020-12-03'),
(3871, 302, '2020-12-03'),
(3872, 304, '2020-12-03'),
(3873, 337, '2020-12-04'),
(3874, 305, '2020-12-04'),
(3875, 337, '2020-12-04'),
(3876, 329, '2020-12-04'),
(3877, 331, '2020-12-04'),
(3878, 310, '2020-12-04'),
(3879, 309, '2020-12-04'),
(3880, 329, '2020-12-04'),
(3881, 330, '2020-12-04'),
(3882, 306, '2020-12-04'),
(3883, 337, '2020-12-04'),
(3884, 331, '2020-12-04'),
(3885, 332, '2020-12-04'),
(3886, 327, '2020-12-04'),
(3887, 95, '2020-12-04'),
(3888, 341, '2020-12-04'),
(3889, 306, '2020-12-04'),
(3890, 309, '2020-12-04'),
(3891, 310, '2020-12-04'),
(3892, 337, '2020-12-04'),
(3893, 310, '2020-12-04'),
(3894, 315, '2020-12-04'),
(3895, 325, '2020-12-04'),
(3896, 306, '2020-12-04'),
(3897, 332, '2020-12-04'),
(3898, 326, '2020-12-04'),
(3899, 327, '2020-12-04'),
(3900, 335, '2020-12-04'),
(3901, 330, '2020-12-04'),
(3902, 334, '2020-12-04'),
(3903, 336, '2020-12-04'),
(3904, 333, '2020-12-04'),
(3905, 331, '2020-12-04'),
(3906, 301, '2020-12-04'),
(3907, 329, '2020-12-04'),
(3908, 307, '2020-12-04'),
(3909, 340, '2020-12-04'),
(3910, 303, '2020-12-04'),
(3911, 311, '2020-12-04'),
(3912, 305, '2020-12-04'),
(3913, 341, '2020-12-04'),
(3914, 300, '2020-12-04'),
(3915, 309, '2020-12-04'),
(3916, 302, '2020-12-04'),
(3917, 304, '2020-12-04'),
(3918, 307, '2020-12-05'),
(3919, 308, '2020-12-05'),
(3920, 304, '2020-12-05'),
(3921, 327, '2020-12-05'),
(3922, 335, '2020-12-05'),
(3923, 310, '2020-12-05'),
(3924, 332, '2020-12-05'),
(3925, 315, '2020-12-05'),
(3926, 336, '2020-12-05'),
(3927, 337, '2020-12-05'),
(3928, 341, '2020-12-05'),
(3929, 341, '2020-12-05'),
(3930, 301, '2020-12-05'),
(3931, 318, '2020-12-05'),
(3932, 341, '2020-12-05'),
(3933, 95, '2020-12-05'),
(3934, 341, '2020-12-05'),
(3935, 95, '2020-12-05'),
(3936, 95, '2020-12-05'),
(3937, 95, '2020-12-05'),
(3938, 95, '2020-12-05'),
(3939, 341, '2020-12-05'),
(3940, 337, '2020-12-05'),
(3941, 310, '2020-12-05'),
(3942, 337, '2020-12-05'),
(3943, 331, '2020-12-05'),
(3944, 337, '2020-12-05'),
(3945, 326, '2020-12-05'),
(3946, 300, '2020-12-05'),
(3947, 301, '2020-12-05'),
(3948, 301, '2020-12-05'),
(3949, 337, '2020-12-05'),
(3950, 322, '2020-12-05'),
(3951, 312, '2020-12-05'),
(3952, 330, '2020-12-05'),
(3953, 313, '2020-12-05'),
(3954, 318, '2020-12-05'),
(3955, 320, '2020-12-05'),
(3956, 303, '2020-12-05'),
(3957, 319, '2020-12-05'),
(3958, 317, '2020-12-05'),
(3959, 315, '2020-12-05'),
(3960, 323, '2020-12-05'),
(3961, 316, '2020-12-05'),
(3962, 322, '2020-12-05'),
(3963, 324, '2020-12-05'),
(3964, 304, '2020-12-05'),
(3965, 336, '2020-12-05'),
(3966, 311, '2020-12-05'),
(3967, 306, '2020-12-05'),
(3968, 324, '2020-12-06'),
(3969, 334, '2020-12-06'),
(3970, 309, '2020-12-06'),
(3971, 337, '2020-12-06'),
(3972, 122, '2020-12-06'),
(3973, 330, '2020-12-06'),
(3974, 300, '2020-12-06'),
(3975, 324, '2020-12-06'),
(3976, 323, '2020-12-06'),
(3977, 316, '2020-12-06'),
(3978, 322, '2020-12-06'),
(3979, 122, '2020-12-06'),
(3980, 95, '2020-12-06'),
(3981, 341, '2020-12-06'),
(3982, 306, '2020-12-06'),
(3983, 309, '2020-12-06'),
(3984, 314, '2020-12-06'),
(3985, 312, '2020-12-06'),
(3986, 321, '2020-12-06'),
(3987, 325, '2020-12-06'),
(3988, 300, '2020-12-06'),
(3989, 95, '2020-12-06'),
(3990, 318, '2020-12-06'),
(3991, 321, '2020-12-07'),
(3992, 311, '2020-12-07'),
(3993, 309, '2020-12-07'),
(3994, 308, '2020-12-07'),
(3995, 311, '2020-12-07'),
(3996, 313, '2020-12-07'),
(3997, 317, '2020-12-07'),
(3998, 341, '2020-12-07'),
(3999, 95, '2020-12-07'),
(4000, 95, '2020-12-07'),
(4001, 341, '2020-12-07'),
(4002, 319, '2020-12-07'),
(4003, 337, '2020-12-07'),
(4004, 95, '2020-12-07'),
(4005, 337, '2020-12-07'),
(4006, 337, '2020-12-07'),
(4007, 307, '2020-12-07'),
(4008, 95, '2020-12-07'),
(4009, 95, '2020-12-07'),
(4010, 95, '2020-12-07'),
(4011, 327, '2020-12-07'),
(4012, 309, '2020-12-07'),
(4013, 95, '2020-12-07'),
(4014, 95, '2020-12-07'),
(4015, 312, '2020-12-07'),
(4016, 321, '2020-12-07'),
(4017, 325, '2020-12-07'),
(4018, 323, '2020-12-07'),
(4019, 306, '2020-12-07'),
(4020, 311, '2020-12-07'),
(4021, 330, '2020-12-07'),
(4022, 340, '2020-12-07'),
(4023, 303, '2020-12-07'),
(4024, 341, '2020-12-07'),
(4025, 95, '2020-12-07'),
(4026, 310, '2020-12-07'),
(4027, 341, '2020-12-07'),
(4028, 308, '2020-12-07'),
(4029, 310, '2020-12-07'),
(4030, 314, '2020-12-08'),
(4031, 307, '2020-12-08'),
(4032, 341, '2020-12-08'),
(4033, 337, '2020-12-08'),
(4034, 341, '2020-12-08'),
(4035, 337, '2020-12-08'),
(4036, 326, '2020-12-08'),
(4037, 335, '2020-12-08'),
(4038, 329, '2020-12-08'),
(4039, 314, '2020-12-08'),
(4040, 306, '2020-12-08'),
(4041, 323, '2020-12-08'),
(4042, 322, '2020-12-08'),
(4043, 327, '2020-12-08'),
(4044, 322, '2020-12-08'),
(4045, 309, '2020-12-08'),
(4046, 95, '2020-12-08'),
(4047, 95, '2020-12-08'),
(4048, 320, '2020-12-09'),
(4049, 300, '2020-12-09'),
(4050, 302, '2020-12-09'),
(4051, 326, '2020-12-09'),
(4052, 305, '2020-12-09'),
(4053, 304, '2020-12-09'),
(4054, 301, '2020-12-09'),
(4055, 303, '2020-12-09'),
(4056, 308, '2020-12-09'),
(4057, 305, '2020-12-09'),
(4058, 309, '2020-12-09'),
(4059, 308, '2020-12-09'),
(4060, 331, '2020-12-09'),
(4061, 329, '2020-12-09'),
(4062, 318, '2020-12-09'),
(4063, 329, '2020-12-09'),
(4064, 325, '2020-12-09'),
(4065, 330, '2020-12-09'),
(4066, 322, '2020-12-09'),
(4067, 341, '2020-12-09'),
(4068, 303, '2020-12-09'),
(4069, 316, '2020-12-09'),
(4070, 316, '2020-12-09'),
(4071, 327, '2020-12-09'),
(4072, 306, '2020-12-09'),
(4073, 303, '2020-12-09'),
(4074, 337, '2020-12-09'),
(4075, 332, '2020-12-09'),
(4076, 334, '2020-12-09'),
(4077, 333, '2020-12-09'),
(4078, 334, '2020-12-09'),
(4079, 95, '2020-12-09'),
(4080, 328, '2020-12-09'),
(4081, 335, '2020-12-09'),
(4082, 336, '2020-12-09'),
(4083, 308, '2020-12-09'),
(4084, 341, '2020-12-09'),
(4085, 321, '2020-12-09'),
(4086, 305, '2020-12-09'),
(4087, 336, '2020-12-09'),
(4088, 324, '2020-12-09'),
(4089, 308, '2020-12-09'),
(4090, 321, '2020-12-09'),
(4091, 310, '2020-12-09'),
(4092, 312, '2020-12-09'),
(4093, 305, '2020-12-09'),
(4094, 313, '2020-12-09'),
(4095, 310, '2020-12-09'),
(4096, 317, '2020-12-09'),
(4097, 327, '2020-12-09'),
(4098, 316, '2020-12-10'),
(4099, 311, '2020-12-10'),
(4100, 317, '2020-12-10'),
(4101, 337, '2020-12-10'),
(4102, 313, '2020-12-10'),
(4103, 335, '2020-12-10'),
(4104, 329, '2020-12-10'),
(4105, 328, '2020-12-10'),
(4106, 304, '2020-12-10'),
(4107, 302, '2020-12-10'),
(4108, 300, '2020-12-10'),
(4109, 306, '2020-12-10'),
(4110, 307, '2020-12-10'),
(4111, 308, '2020-12-10'),
(4112, 316, '2020-12-10'),
(4113, 311, '2020-12-10'),
(4114, 318, '2020-12-10'),
(4115, 319, '2020-12-10'),
(4116, 308, '2020-12-10'),
(4117, 324, '2020-12-10'),
(4118, 323, '2020-12-10'),
(4119, 301, '2020-12-10'),
(4120, 325, '2020-12-10'),
(4121, 321, '2020-12-10'),
(4122, 327, '2020-12-10'),
(4123, 309, '2020-12-10'),
(4124, 317, '2020-12-10'),
(4125, 322, '2020-12-10'),
(4126, 320, '2020-12-10'),
(4127, 314, '2020-12-10'),
(4128, 313, '2020-12-10'),
(4129, 328, '2020-12-10'),
(4130, 331, '2020-12-10'),
(4131, 307, '2020-12-10'),
(4132, 332, '2020-12-10'),
(4133, 310, '2020-12-10'),
(4134, 307, '2020-12-10'),
(4135, 337, '2020-12-10'),
(4136, 332, '2020-12-10'),
(4137, 314, '2020-12-10'),
(4138, 303, '2020-12-10'),
(4139, 341, '2020-12-10'),
(4140, 301, '2020-12-10'),
(4141, 329, '2020-12-10'),
(4142, 310, '2020-12-10'),
(4143, 311, '2020-12-11'),
(4144, 310, '2020-12-11'),
(4145, 310, '2020-12-11'),
(4146, 334, '2020-12-11'),
(4147, 334, '2020-12-11'),
(4148, 304, '2020-12-11'),
(4149, 303, '2020-12-11'),
(4150, 95, '2020-12-11'),
(4151, 329, '2020-12-11'),
(4152, 336, '2020-12-11'),
(4153, 328, '2020-12-11'),
(4154, 300, '2020-12-11'),
(4155, 321, '2020-12-11'),
(4156, 308, '2020-12-11'),
(4157, 307, '2020-12-11'),
(4158, 334, '2020-12-11'),
(4159, 319, '2020-12-11'),
(4160, 122, '2020-12-11'),
(4161, 309, '2020-12-11'),
(4162, 334, '2020-12-11'),
(4163, 320, '2020-12-11'),
(4164, 95, '2020-12-11'),
(4165, 314, '2020-12-11'),
(4166, 314, '2020-12-11'),
(4167, 330, '2020-12-11'),
(4168, 326, '2020-12-11'),
(4169, 311, '2020-12-11'),
(4170, 318, '2020-12-11'),
(4171, 315, '2020-12-12'),
(4172, 337, '2020-12-12'),
(4173, 324, '2020-12-12'),
(4174, 319, '2020-12-12'),
(4175, 327, '2020-12-12'),
(4176, 307, '2020-12-12'),
(4177, 309, '2020-12-12'),
(4178, 334, '2020-12-12'),
(4179, 310, '2020-12-12'),
(4180, 321, '2020-12-12'),
(4181, 315, '2020-12-12'),
(4182, 310, '2020-12-12'),
(4183, 325, '2020-12-12'),
(4184, 304, '2020-12-12'),
(4185, 321, '2020-12-12'),
(4186, 313, '2020-12-12'),
(4187, 322, '2020-12-12'),
(4188, 309, '2020-12-12'),
(4189, 309, '2020-12-12'),
(4190, 320, '2020-12-12'),
(4191, 305, '2020-12-12'),
(4192, 324, '2020-12-12'),
(4193, 312, '2020-12-12'),
(4194, 300, '2020-12-12'),
(4195, 323, '2020-12-12'),
(4196, 312, '2020-12-12'),
(4197, 328, '2020-12-12'),
(4198, 313, '2020-12-12'),
(4199, 319, '2020-12-12'),
(4200, 330, '2020-12-12'),
(4201, 315, '2020-12-12'),
(4202, 336, '2020-12-12'),
(4203, 331, '2020-12-12'),
(4204, 333, '2020-12-12'),
(4205, 337, '2020-12-12'),
(4206, 318, '2020-12-12'),
(4207, 306, '2020-12-13'),
(4208, 314, '2020-12-13'),
(4209, 328, '2020-12-13'),
(4210, 326, '2020-12-13'),
(4211, 326, '2020-12-13'),
(4212, 326, '2020-12-13'),
(4213, 95, '2020-12-13'),
(4214, 122, '2020-12-13'),
(4215, 306, '2020-12-13'),
(4216, 308, '2020-12-13'),
(4217, 302, '2020-12-13'),
(4218, 334, '2020-12-13'),
(4219, 315, '2020-12-13'),
(4220, 321, '2020-12-13'),
(4221, 328, '2020-12-13'),
(4222, 329, '2020-12-13'),
(4223, 320, '2020-12-13'),
(4224, 320, '2020-12-13'),
(4225, 320, '2020-12-13'),
(4226, 319, '2020-12-13'),
(4227, 335, '2020-12-13'),
(4228, 122, '2020-12-13'),
(4229, 308, '2020-12-13'),
(4230, 304, '2020-12-13'),
(4231, 301, '2020-12-13'),
(4232, 300, '2020-12-13'),
(4233, 336, '2020-12-13'),
(4234, 310, '2020-12-13'),
(4235, 336, '2020-12-13'),
(4236, 329, '2020-12-14'),
(4237, 337, '2020-12-14'),
(4238, 95, '2020-12-14'),
(4239, 341, '2020-12-14'),
(4240, 306, '2020-12-14'),
(4241, 341, '2020-12-14'),
(4242, 303, '2020-12-14'),
(4243, 305, '2020-12-14'),
(4244, 333, '2020-12-14'),
(4245, 302, '2020-12-14'),
(4246, 327, '2020-12-14'),
(4247, 341, '2020-12-14'),
(4248, 306, '2020-12-14'),
(4249, 95, '2020-12-14'),
(4250, 329, '2020-12-14'),
(4251, 327, '2020-12-14'),
(4252, 333, '2020-12-15'),
(4253, 337, '2020-12-15'),
(4254, 330, '2020-12-15'),
(4255, 312, '2020-12-15'),
(4256, 307, '2020-12-15'),
(4257, 325, '2020-12-15'),
(4258, 306, '2020-12-15'),
(4259, 307, '2020-12-15'),
(4260, 337, '2020-12-15'),
(4261, 325, '2020-12-15'),
(4262, 317, '2020-12-16'),
(4263, 301, '2020-12-16'),
(4264, 310, '2020-12-16'),
(4265, 316, '2020-12-16'),
(4266, 319, '2020-12-16'),
(4267, 307, '2020-12-16'),
(4268, 324, '2020-12-16'),
(4269, 321, '2020-12-16'),
(4270, 310, '2020-12-16'),
(4271, 316, '2020-12-16'),
(4272, 341, '2020-12-16'),
(4273, 326, '2020-12-16'),
(4274, 326, '2020-12-16'),
(4275, 309, '2020-12-16'),
(4276, 322, '2020-12-16'),
(4277, 95, '2020-12-16'),
(4278, 320, '2020-12-16'),
(4279, 326, '2020-12-16'),
(4280, 323, '2020-12-16'),
(4281, 341, '2020-12-16'),
(4282, 309, '2020-12-17'),
(4283, 311, '2020-12-17'),
(4284, 307, '2020-12-17'),
(4285, 319, '2020-12-17'),
(4286, 326, '2020-12-17'),
(4287, 326, '2020-12-17'),
(4288, 326, '2020-12-17'),
(4289, 326, '2020-12-17'),
(4290, 314, '2020-12-17'),
(4291, 327, '2020-12-17'),
(4292, 335, '2020-12-17'),
(4293, 313, '2020-12-17'),
(4294, 326, '2020-12-17'),
(4295, 311, '2020-12-17'),
(4296, 333, '2020-12-17'),
(4297, 95, '2020-12-17'),
(4298, 307, '2020-12-18'),
(4299, 334, '2020-12-18'),
(4300, 335, '2020-12-18'),
(4301, 336, '2020-12-18'),
(4302, 300, '2020-12-18'),
(4303, 309, '2020-12-18'),
(4304, 341, '2020-12-18'),
(4305, 328, '2020-12-18'),
(4306, 318, '2020-12-18'),
(4307, 341, '2020-12-18'),
(4308, 95, '2020-12-18'),
(4309, 340, '2020-12-18'),
(4310, 310, '2020-12-18'),
(4311, 307, '2020-12-18'),
(4312, 331, '2020-12-18'),
(4313, 330, '2020-12-18'),
(4314, 323, '2020-12-18'),
(4315, 330, '2020-12-18'),
(4316, 326, '2020-12-18'),
(4317, 331, '2020-12-18'),
(4318, 95, '2020-12-18'),
(4319, 332, '2020-12-18'),
(4320, 328, '2020-12-18'),
(4321, 306, '2020-12-18'),
(4322, 325, '2020-12-18'),
(4323, 336, '2020-12-18'),
(4324, 320, '2020-12-19'),
(4325, 328, '2020-12-19'),
(4326, 329, '2020-12-19'),
(4327, 309, '2020-12-19'),
(4328, 316, '2020-12-19'),
(4329, 336, '2020-12-19'),
(4330, 310, '2020-12-19'),
(4331, 328, '2020-12-19'),
(4332, 329, '2020-12-19'),
(4333, 95, '2020-12-19'),
(4334, 122, '2020-12-19'),
(4335, 311, '2020-12-19'),
(4336, 309, '2020-12-19'),
(4337, 308, '2020-12-19'),
(4338, 300, '2020-12-19'),
(4339, 301, '2020-12-19'),
(4340, 302, '2020-12-19'),
(4341, 304, '2020-12-19'),
(4342, 303, '2020-12-19'),
(4343, 305, '2020-12-19'),
(4344, 306, '2020-12-19'),
(4345, 307, '2020-12-19'),
(4346, 340, '2020-12-19'),
(4347, 341, '2020-12-19'),
(4348, 330, '2020-12-19'),
(4349, 331, '2020-12-19'),
(4350, 332, '2020-12-19'),
(4351, 333, '2020-12-19'),
(4352, 334, '2020-12-19'),
(4353, 335, '2020-12-19'),
(4354, 336, '2020-12-19'),
(4355, 337, '2020-12-19'),
(4356, 324, '2020-12-19'),
(4357, 325, '2020-12-19'),
(4358, 326, '2020-12-19'),
(4359, 327, '2020-12-19'),
(4360, 322, '2020-12-19'),
(4361, 318, '2020-12-19'),
(4362, 319, '2020-12-19'),
(4363, 317, '2020-12-19'),
(4364, 321, '2020-12-19'),
(4365, 329, '2020-12-19'),
(4366, 320, '2020-12-19'),
(4367, 328, '2020-12-19'),
(4368, 312, '2020-12-19'),
(4369, 313, '2020-12-19'),
(4370, 316, '2020-12-19'),
(4371, 315, '2020-12-19'),
(4372, 323, '2020-12-19'),
(4373, 314, '2020-12-19'),
(4374, 329, '2020-12-19'),
(4375, 337, '2020-12-19'),
(4376, 336, '2020-12-20'),
(4377, 306, '2020-12-20'),
(4378, 316, '2020-12-20'),
(4379, 328, '2020-12-20'),
(4380, 95, '2020-12-20'),
(4381, 95, '2020-12-20'),
(4382, 336, '2020-12-20'),
(4383, 332, '2020-12-20'),
(4384, 332, '2020-12-20'),
(4385, 337, '2020-12-20'),
(4386, 309, '2020-12-20'),
(4387, 336, '2020-12-21'),
(4388, 340, '2020-12-21'),
(4389, 329, '2020-12-21'),
(4390, 95, '2020-12-21'),
(4391, 306, '2020-12-21'),
(4392, 306, '2020-12-21'),
(4393, 341, '2020-12-21'),
(4394, 341, '2020-12-21'),
(4395, 341, '2020-12-21'),
(4396, 311, '2020-12-21'),
(4397, 304, '2020-12-21'),
(4398, 310, '2020-12-21'),
(4399, 315, '2020-12-21'),
(4400, 320, '2020-12-21'),
(4401, 314, '2020-12-21'),
(4402, 337, '2020-12-21'),
(4403, 332, '2020-12-21'),
(4404, 308, '2020-12-21'),
(4405, 122, '2020-12-22'),
(4406, 305, '2020-12-22'),
(4407, 336, '2020-12-22'),
(4408, 335, '2020-12-22'),
(4409, 309, '2020-12-22'),
(4410, 309, '2020-12-22'),
(4411, 301, '2020-12-22'),
(4412, 336, '2020-12-22'),
(4413, 302, '2020-12-22'),
(4414, 337, '2020-12-22'),
(4415, 122, '2020-12-22'),
(4416, 305, '2020-12-22'),
(4417, 303, '2020-12-22'),
(4418, 326, '2020-12-22'),
(4419, 327, '2020-12-22'),
(4420, 315, '2020-12-22'),
(4421, 95, '2020-12-22'),
(4422, 307, '2020-12-22'),
(4423, 327, '2020-12-22'),
(4424, 95, '2020-12-22'),
(4425, 306, '2020-12-22'),
(4426, 306, '2020-12-23'),
(4427, 340, '2020-12-23'),
(4428, 332, '2020-12-23'),
(4429, 322, '2020-12-23'),
(4430, 330, '2020-12-23'),
(4431, 310, '2020-12-23'),
(4432, 318, '2020-12-23'),
(4433, 307, '2020-12-23'),
(4434, 333, '2020-12-23'),
(4435, 122, '2020-12-23'),
(4436, 306, '2020-12-23'),
(4437, 95, '2020-12-23'),
(4438, 300, '2020-12-23'),
(4439, 328, '2020-12-23'),
(4440, 320, '2020-12-23'),
(4441, 335, '2020-12-23'),
(4442, 331, '2020-12-23'),
(4443, 323, '2020-12-23'),
(4444, 326, '2020-12-23'),
(4445, 314, '2020-12-23'),
(4446, 322, '2020-12-23'),
(4447, 341, '2020-12-23'),
(4448, 323, '2020-12-23'),
(4449, 306, '2020-12-23'),
(4450, 311, '2020-12-24'),
(4451, 304, '2020-12-24'),
(4452, 321, '2020-12-24'),
(4453, 313, '2020-12-24'),
(4454, 324, '2020-12-24'),
(4455, 337, '2020-12-24'),
(4456, 341, '2020-12-24'),
(4457, 337, '2020-12-24'),
(4458, 337, '2020-12-24'),
(4459, 318, '2020-12-24'),
(4460, 310, '2020-12-24'),
(4461, 310, '2020-12-24'),
(4462, 336, '2020-12-24'),
(4463, 336, '2020-12-24'),
(4464, 326, '2020-12-24'),
(4465, 95, '2020-12-24'),
(4466, 341, '2020-12-24'),
(4467, 337, '2020-12-24'),
(4468, 95, '2020-12-24'),
(4469, 315, '2020-12-24'),
(4470, 327, '2020-12-24'),
(4471, 305, '2020-12-24'),
(4472, 95, '2020-12-25'),
(4473, 310, '2020-12-25'),
(4474, 310, '2020-12-25'),
(4475, 306, '2020-12-25'),
(4476, 309, '2020-12-25'),
(4477, 337, '2020-12-25'),
(4478, 341, '2020-12-25'),
(4479, 95, '2020-12-25'),
(4480, 310, '2020-12-25'),
(4481, 341, '2020-12-25'),
(4482, 309, '2020-12-25'),
(4483, 301, '2020-12-25'),
(4484, 302, '2020-12-25'),
(4485, 337, '2020-12-25'),
(4486, 95, '2020-12-25'),
(4487, 341, '2020-12-25'),
(4488, 341, '2020-12-25'),
(4489, 305, '2020-12-25'),
(4490, 304, '2020-12-25'),
(4491, 320, '2020-12-25'),
(4492, 300, '2020-12-25'),
(4493, 311, '2020-12-25'),
(4494, 122, '2020-12-25'),
(4495, 309, '2020-12-26'),
(4496, 337, '2020-12-26'),
(4497, 333, '2020-12-26'),
(4498, 341, '2020-12-26'),
(4499, 306, '2020-12-26'),
(4500, 306, '2020-12-26'),
(4501, 310, '2020-12-26'),
(4502, 325, '2020-12-26'),
(4503, 319, '2020-12-26'),
(4504, 309, '2020-12-26'),
(4505, 309, '2020-12-26'),
(4506, 305, '2020-12-26'),
(4507, 330, '2020-12-26'),
(4508, 303, '2020-12-26'),
(4509, 302, '2020-12-26'),
(4510, 337, '2020-12-26'),
(4511, 321, '2020-12-26'),
(4512, 316, '2020-12-26'),
(4513, 328, '2020-12-27'),
(4514, 328, '2020-12-27'),
(4515, 328, '2020-12-27'),
(4516, 306, '2020-12-27'),
(4517, 302, '2020-12-27'),
(4518, 310, '2020-12-27'),
(4519, 306, '2020-12-27'),
(4520, 302, '2020-12-27'),
(4521, 310, '2020-12-27'),
(4522, 306, '2020-12-27'),
(4523, 302, '2020-12-27'),
(4524, 310, '2020-12-27'),
(4525, 306, '2020-12-27'),
(4526, 302, '2020-12-27'),
(4527, 310, '2020-12-27'),
(4528, 306, '2020-12-27'),
(4529, 302, '2020-12-27'),
(4530, 306, '2020-12-27'),
(4531, 302, '2020-12-27'),
(4532, 310, '2020-12-27'),
(4533, 310, '2020-12-27'),
(4534, 306, '2020-12-27'),
(4535, 302, '2020-12-27'),
(4536, 310, '2020-12-27'),
(4537, 306, '2020-12-27'),
(4538, 302, '2020-12-27'),
(4539, 310, '2020-12-27'),
(4540, 306, '2020-12-27'),
(4541, 302, '2020-12-27'),
(4542, 310, '2020-12-27'),
(4543, 306, '2020-12-27'),
(4544, 302, '2020-12-27'),
(4545, 310, '2020-12-27'),
(4546, 328, '2020-12-27'),
(4547, 334, '2020-12-27'),
(4548, 330, '2020-12-27'),
(4549, 321, '2020-12-27'),
(4550, 309, '2020-12-27'),
(4551, 341, '2020-12-27'),
(4552, 306, '2020-12-27'),
(4553, 95, '2020-12-27'),
(4554, 306, '2020-12-27'),
(4555, 309, '2020-12-27'),
(4556, 314, '2020-12-27'),
(4557, 329, '2020-12-27'),
(4558, 307, '2020-12-27'),
(4559, 325, '2020-12-27'),
(4560, 308, '2020-12-27'),
(4561, 327, '2020-12-28'),
(4562, 341, '2020-12-28'),
(4563, 337, '2020-12-28'),
(4564, 327, '2020-12-28'),
(4565, 333, '2020-12-28'),
(4566, 337, '2020-12-28'),
(4567, 306, '2020-12-28'),
(4568, 310, '2020-12-28'),
(4569, 309, '2020-12-28'),
(4570, 323, '2020-12-28'),
(4571, 325, '2020-12-28'),
(4572, 95, '2020-12-28'),
(4573, 95, '2020-12-28'),
(4574, 306, '2020-12-28'),
(4575, 309, '2020-12-28'),
(4576, 310, '2020-12-28'),
(4577, 337, '2020-12-28'),
(4578, 341, '2020-12-28'),
(4579, 328, '2020-12-28'),
(4580, 306, '2020-12-28'),
(4581, 319, '2020-12-28'),
(4582, 309, '2020-12-28'),
(4583, 304, '2020-12-28'),
(4584, 305, '2020-12-28'),
(4585, 303, '2020-12-28'),
(4586, 306, '2020-12-28'),
(4587, 309, '2020-12-28'),
(4588, 330, '2020-12-28'),
(4589, 333, '2020-12-28'),
(4590, 326, '2020-12-28'),
(4591, 327, '2020-12-28'),
(4592, 336, '2020-12-28'),
(4593, 333, '2020-12-28'),
(4594, 335, '2020-12-28'),
(4595, 334, '2020-12-28'),
(4596, 336, '2020-12-28'),
(4597, 337, '2020-12-28'),
(4598, 306, '2020-12-28'),
(4599, 329, '2020-12-29'),
(4600, 302, '2020-12-29'),
(4601, 308, '2020-12-29'),
(4602, 304, '2020-12-29'),
(4603, 303, '2020-12-29'),
(4604, 327, '2020-12-29'),
(4605, 304, '2020-12-29'),
(4606, 329, '2020-12-29'),
(4607, 328, '2020-12-29'),
(4608, 335, '2020-12-29'),
(4609, 300, '2020-12-29'),
(4610, 305, '2020-12-29'),
(4611, 332, '2020-12-29'),
(4612, 326, '2020-12-29'),
(4613, 308, '2020-12-29'),
(4614, 311, '2020-12-29'),
(4615, 315, '2020-12-29'),
(4616, 305, '2020-12-29'),
(4617, 341, '2020-12-29'),
(4618, 336, '2020-12-29'),
(4619, 301, '2020-12-29'),
(4620, 318, '2020-12-29'),
(4621, 331, '2020-12-29'),
(4622, 331, '2020-12-29'),
(4623, 337, '2020-12-29'),
(4624, 322, '2020-12-29'),
(4625, 330, '2020-12-29'),
(4626, 332, '2020-12-29'),
(4627, 310, '2020-12-29'),
(4628, 305, '2020-12-29'),
(4629, 302, '2020-12-30'),
(4630, 304, '2020-12-30'),
(4631, 301, '2020-12-30'),
(4632, 337, '2020-12-30'),
(4633, 311, '2020-12-30'),
(4634, 300, '2020-12-30'),
(4635, 330, '2020-12-30'),
(4636, 95, '2020-12-30'),
(4637, 306, '2020-12-30'),
(4638, 309, '2020-12-30'),
(4639, 310, '2020-12-30'),
(4640, 337, '2020-12-30'),
(4641, 317, '2020-12-30'),
(4642, 331, '2020-12-30'),
(4643, 316, '2020-12-30'),
(4644, 307, '2020-12-30'),
(4645, 341, '2020-12-30'),
(4646, 310, '2020-12-30'),
(4647, 306, '2020-12-30'),
(4648, 308, '2020-12-30'),
(4649, 313, '2020-12-30'),
(4650, 303, '2020-12-30'),
(4651, 316, '2020-12-30'),
(4652, 318, '2020-12-30'),
(4653, 319, '2020-12-30'),
(4654, 319, '2020-12-30'),
(4655, 305, '2020-12-30'),
(4656, 324, '2020-12-30'),
(4657, 323, '2020-12-30'),
(4658, 341, '2020-12-30'),
(4659, 310, '2020-12-30'),
(4660, 306, '2020-12-30'),
(4661, 325, '2020-12-30'),
(4662, 321, '2020-12-30'),
(4663, 306, '2020-12-30'),
(4664, 317, '2020-12-30'),
(4665, 322, '2020-12-30'),
(4666, 314, '2020-12-30'),
(4667, 320, '2020-12-30'),
(4668, 313, '2020-12-30'),
(4669, 334, '2020-12-30'),
(4670, 301, '2020-12-30'),
(4671, 307, '2020-12-30'),
(4672, 341, '2020-12-30'),
(4673, 311, '2020-12-31'),
(4674, 320, '2020-12-31'),
(4675, 322, '2020-12-31'),
(4676, 341, '2020-12-31'),
(4677, 310, '2020-12-31'),
(4678, 306, '2020-12-31'),
(4679, 306, '2020-12-31'),
(4680, 300, '2020-12-31'),
(4681, 330, '2020-12-31'),
(4682, 314, '2020-12-31'),
(4683, 303, '2020-12-31'),
(4684, 301, '2020-12-31'),
(4685, 341, '2020-12-31'),
(4686, 310, '2020-12-31'),
(4687, 306, '2020-12-31'),
(4688, 308, '2020-12-31'),
(4689, 309, '2020-12-31'),
(4690, 310, '2020-12-31'),
(4691, 337, '2020-12-31'),
(4692, 341, '2020-12-31'),
(4693, 306, '2020-12-31'),
(4694, 310, '2020-12-31'),
(4695, 95, '2020-12-31'),
(4696, 309, '2020-12-31'),
(4697, 95, '2020-12-31'),
(4698, 337, '2020-12-31'),
(4699, 306, '2020-12-31'),
(4700, 336, '2020-12-31'),
(4701, 331, '2020-12-31'),
(4702, 335, '2020-12-31'),
(4703, 332, '2020-12-31'),
(4704, 328, '2020-12-31'),
(4705, 330, '2020-12-31'),
(4706, 329, '2020-12-31'),
(4707, 334, '2020-12-31'),
(4708, 333, '2020-12-31'),
(4709, 340, '2020-12-31'),
(4710, 341, '2020-12-31'),
(4711, 327, '2020-12-31'),
(4712, 332, '2020-12-31'),
(4713, 329, '2020-12-31'),
(4714, 330, '2020-12-31'),
(4715, 334, '2020-12-31'),
(4716, 326, '2020-12-31'),
(4717, 331, '2020-12-31'),
(4718, 326, '2020-12-31'),
(4719, 328, '2020-12-31'),
(4720, 327, '2020-12-31'),
(4721, 305, '2020-12-31'),
(4722, 333, '2020-12-31'),
(4723, 336, '2020-12-31'),
(4724, 335, '2020-12-31'),
(4725, 300, '2020-12-31'),
(4726, 301, '2020-12-31'),
(4727, 302, '2020-12-31'),
(4728, 308, '2020-12-31'),
(4729, 305, '2020-12-31'),
(4730, 307, '2020-12-31'),
(4731, 311, '2020-12-31'),
(4732, 303, '2020-12-31'),
(4733, 304, '2020-12-31'),
(4734, 302, '2020-12-31'),
(4735, 340, '2020-12-31'),
(4736, 311, '2020-12-31'),
(4737, 303, '2020-12-31'),
(4738, 307, '2020-12-31'),
(4739, 304, '2020-12-31'),
(4740, 308, '2020-12-31'),
(4741, 301, '2020-12-31'),
(4742, 300, '2020-12-31'),
(4743, 312, '2020-12-31'),
(4744, 314, '2020-12-31'),
(4745, 313, '2020-12-31'),
(4746, 315, '2020-12-31'),
(4747, 122, '2020-12-31'),
(4748, 325, '2020-12-31'),
(4749, 317, '2020-12-31'),
(4750, 319, '2020-12-31'),
(4751, 324, '2020-12-31'),
(4752, 316, '2020-12-31'),
(4753, 318, '2020-12-31'),
(4754, 322, '2020-12-31'),
(4755, 320, '2020-12-31'),
(4756, 323, '2020-12-31'),
(4757, 321, '2020-12-31'),
(4758, 317, '2020-12-31'),
(4759, 324, '2020-12-31'),
(4760, 323, '2020-12-31'),
(4761, 318, '2020-12-31'),
(4762, 320, '2020-12-31'),
(4763, 319, '2020-12-31'),
(4764, 315, '2020-12-31'),
(4765, 322, '2020-12-31'),
(4766, 321, '2020-12-31'),
(4767, 316, '2020-12-31'),
(4768, 314, '2020-12-31'),
(4769, 325, '2020-12-31'),
(4770, 313, '2020-12-31'),
(4771, 312, '2020-12-31'),
(4772, 122, '2020-12-31'),
(4773, 304, '2020-12-31'),
(4774, 311, '2020-12-31'),
(4775, 329, '2020-12-31'),
(4776, 327, '2020-12-31'),
(4777, 300, '2020-12-31'),
(4778, 335, '2020-12-31'),
(4779, 341, '2021-01-01'),
(4780, 310, '2021-01-01'),
(4781, 306, '2021-01-01'),
(4782, 332, '2021-01-01'),
(4783, 302, '2021-01-01'),
(4784, 327, '2021-01-01'),
(4785, 318, '2021-01-01'),
(4786, 315, '2021-01-01'),
(4787, 336, '2021-01-01'),
(4788, 341, '2021-01-01'),
(4789, 310, '2021-01-01'),
(4790, 306, '2021-01-01'),
(4791, 318, '2021-01-01'),
(4792, 95, '2021-01-01'),
(4793, 301, '2021-01-01'),
(4794, 315, '2021-01-01'),
(4795, 324, '2021-01-01'),
(4796, 331, '2021-01-01'),
(4797, 315, '2021-01-01'),
(4798, 321, '2021-01-01'),
(4799, 337, '2021-01-01'),
(4800, 325, '2021-01-01'),
(4801, 304, '2021-01-01'),
(4802, 322, '2021-01-01'),
(4803, 314, '2021-01-01'),
(4804, 301, '2021-01-01'),
(4805, 305, '2021-01-01'),
(4806, 310, '2021-01-01'),
(4807, 323, '2021-01-01'),
(4808, 320, '2021-01-01'),
(4809, 312, '2021-01-01'),
(4810, 330, '2021-01-01'),
(4811, 319, '2021-01-01'),
(4812, 307, '2021-01-01'),
(4813, 95, '2021-01-01'),
(4814, 323, '2021-01-01'),
(4815, 341, '2021-01-01'),
(4816, 341, '2021-01-01'),
(4817, 309, '2021-01-01'),
(4818, 309, '2021-01-01'),
(4819, 306, '2021-01-01'),
(4820, 309, '2021-01-01'),
(4821, 340, '2021-01-01'),
(4822, 330, '2021-01-01'),
(4823, 329, '2021-01-01'),
(4824, 310, '2021-01-01'),
(4825, 310, '2021-01-01'),
(4826, 341, '2021-01-02'),
(4827, 310, '2021-01-02'),
(4828, 306, '2021-01-02'),
(4829, 312, '2021-01-02'),
(4830, 321, '2021-01-02'),
(4831, 302, '2021-01-02'),
(4832, 95, '2021-01-02'),
(4833, 324, '2021-01-02'),
(4834, 309, '2021-01-02'),
(4835, 337, '2021-01-02'),
(4836, 310, '2021-01-02'),
(4837, 341, '2021-01-02'),
(4838, 310, '2021-01-02'),
(4839, 306, '2021-01-02'),
(4840, 323, '2021-01-02'),
(4841, 337, '2021-01-02'),
(4842, 95, '2021-01-02'),
(4843, 334, '2021-01-02'),
(4844, 335, '2021-01-02'),
(4845, 308, '2021-01-02'),
(4846, 309, '2021-01-02'),
(4847, 303, '2021-01-02'),
(4848, 306, '2021-01-02'),
(4849, 302, '2021-01-02'),
(4850, 304, '2021-01-03'),
(4851, 341, '2021-01-03'),
(4852, 310, '2021-01-03'),
(4853, 306, '2021-01-03'),
(4854, 307, '2021-01-03'),
(4855, 331, '2021-01-03'),
(4856, 326, '2021-01-03'),
(4857, 306, '2021-01-03'),
(4858, 341, '2021-01-03'),
(4859, 309, '2021-01-03'),
(4860, 310, '2021-01-03'),
(4861, 341, '2021-01-03'),
(4862, 334, '2021-01-04'),
(4863, 325, '2021-01-04'),
(4864, 309, '2021-01-04'),
(4865, 319, '2021-01-04'),
(4866, 303, '2021-01-04'),
(4867, 305, '2021-01-04'),
(4868, 315, '2021-01-04'),
(4869, 341, '2021-01-04'),
(4870, 310, '2021-01-04'),
(4871, 306, '2021-01-04'),
(4872, 95, '2021-01-04'),
(4873, 306, '2021-01-04'),
(4874, 309, '2021-01-04'),
(4875, 337, '2021-01-04'),
(4876, 341, '2021-01-04'),
(4877, 337, '2021-01-04'),
(4878, 95, '2021-01-04'),
(4879, 95, '2021-01-04'),
(4880, 341, '2021-01-04'),
(4881, 310, '2021-01-04'),
(4882, 306, '2021-01-04'),
(4883, 333, '2021-01-04'),
(4884, 334, '2021-01-04'),
(4885, 308, '2021-01-04'),
(4886, 306, '2021-01-04'),
(4887, 337, '2021-01-04'),
(4888, 341, '2021-01-04'),
(4889, 95, '2021-01-04'),
(4890, 309, '2021-01-04'),
(4891, 310, '2021-01-04'),
(4892, 334, '2021-01-04'),
(4893, 328, '2021-01-04'),
(4894, 329, '2021-01-04'),
(4895, 326, '2021-01-04'),
(4896, 330, '2021-01-04'),
(4897, 333, '2021-01-04'),
(4898, 335, '2021-01-04'),
(4899, 336, '2021-01-04'),
(4900, 332, '2021-01-04'),
(4901, 331, '2021-01-04'),
(4902, 327, '2021-01-04'),
(4903, 340, '2021-01-04'),
(4904, 304, '2021-01-04'),
(4905, 305, '2021-01-04'),
(4906, 302, '2021-01-04'),
(4907, 303, '2021-01-04'),
(4908, 300, '2021-01-04'),
(4909, 308, '2021-01-04'),
(4910, 301, '2021-01-04'),
(4911, 307, '2021-01-04'),
(4912, 311, '2021-01-04'),
(4913, 315, '2021-01-04'),
(4914, 333, '2021-01-04'),
(4915, 333, '2021-01-04'),
(4916, 309, '2021-01-04'),
(4917, 306, '2021-01-04'),
(4918, 311, '2021-01-04'),
(4919, 95, '2021-01-04'),
(4920, 334, '2021-01-05'),
(4921, 335, '2021-01-05'),
(4922, 341, '2021-01-05'),
(4923, 337, '2021-01-05'),
(4924, 310, '2021-01-05'),
(4925, 309, '2021-01-05'),
(4926, 306, '2021-01-05'),
(4927, 95, '2021-01-05'),
(4928, 336, '2021-01-05'),
(4929, 329, '2021-01-05'),
(4930, 341, '2021-01-05'),
(4931, 303, '2021-01-05'),
(4932, 341, '2021-01-05'),
(4933, 95, '2021-01-05'),
(4934, 341, '2021-01-05'),
(4935, 337, '2021-01-05'),
(4936, 309, '2021-01-05'),
(4937, 95, '2021-01-05'),
(4938, 306, '2021-01-05'),
(4939, 309, '2021-01-05'),
(4940, 341, '2021-01-05'),
(4941, 305, '2021-01-05'),
(4942, 327, '2021-01-05'),
(4943, 330, '2021-01-05'),
(4944, 331, '2021-01-05'),
(4945, 341, '2021-01-05'),
(4946, 95, '2021-01-05'),
(4947, 332, '2021-01-06'),
(4948, 324, '2021-01-06'),
(4949, 314, '2021-01-06'),
(4950, 314, '2021-01-06'),
(4951, 314, '2021-01-06'),
(4952, 325, '2021-01-06'),
(4953, 341, '2021-01-06'),
(4954, 310, '2021-01-06'),
(4955, 306, '2021-01-06'),
(4956, 320, '2021-01-06'),
(4957, 329, '2021-01-06'),
(4958, 319, '2021-01-06'),
(4959, 316, '2021-01-06'),
(4960, 305, '2021-01-06'),
(4961, 321, '2021-01-06'),
(4962, 337, '2021-01-06'),
(4963, 315, '2021-01-06'),
(4964, 309, '2021-01-06'),
(4965, 341, '2021-01-06'),
(4966, 310, '2021-01-06'),
(4967, 306, '2021-01-06'),
(4968, 322, '2021-01-06'),
(4969, 305, '2021-01-06'),
(4970, 341, '2021-01-06'),
(4971, 341, '2021-01-06'),
(4972, 321, '2021-01-06'),
(4973, 306, '2021-01-06'),
(4974, 311, '2021-01-06'),
(4975, 311, '2021-01-06'),
(4976, 306, '2021-01-06'),
(4977, 95, '2021-01-06'),
(4978, 302, '2021-01-06'),
(4979, 327, '2021-01-06'),
(4980, 309, '2021-01-06'),
(4981, 337, '2021-01-06'),
(4982, 337, '2021-01-06'),
(4983, 322, '2021-01-07'),
(4984, 311, '2021-01-07'),
(4985, 315, '2021-01-07'),
(4986, 341, '2021-01-07'),
(4987, 306, '2021-01-07'),
(4988, 319, '2021-01-07'),
(4989, 341, '2021-01-07'),
(4990, 310, '2021-01-07'),
(4991, 306, '2021-01-07'),
(4992, 341, '2021-01-07'),
(4993, 335, '2021-01-07'),
(4994, 306, '2021-01-07'),
(4995, 309, '2021-01-07'),
(4996, 95, '2021-01-07'),
(4997, 306, '2021-01-07'),
(4998, 341, '2021-01-07'),
(4999, 341, '2021-01-07'),
(5000, 309, '2021-01-07'),
(5001, 337, '2021-01-07'),
(5002, 321, '2021-01-07'),
(5003, 334, '2021-01-07'),
(5004, 306, '2021-01-07'),
(5005, 341, '2021-01-07'),
(5006, 310, '2021-01-07'),
(5007, 306, '2021-01-07'),
(5008, 329, '2021-01-07'),
(5009, 329, '2021-01-07'),
(5010, 95, '2021-01-07'),
(5011, 310, '2021-01-07'),
(5012, 331, '2021-01-07'),
(5013, 306, '2021-01-07'),
(5014, 337, '2021-01-07'),
(5015, 303, '2021-01-07'),
(5016, 302, '2021-01-07'),
(5017, 316, '2021-01-07'),
(5018, 95, '2021-01-07'),
(5019, 309, '2021-01-07'),
(5020, 310, '2021-01-07');
INSERT INTO `product_clicks` (`id`, `product_id`, `date`) VALUES
(5021, 309, '2021-01-07'),
(5022, 323, '2021-01-07'),
(5023, 305, '2021-01-07'),
(5024, 336, '2021-01-07'),
(5025, 341, '2021-01-08'),
(5026, 95, '2021-01-08'),
(5027, 310, '2021-01-08'),
(5028, 306, '2021-01-08'),
(5029, 333, '2021-01-08'),
(5030, 95, '2021-01-08'),
(5031, 341, '2021-01-08'),
(5032, 304, '2021-01-08'),
(5033, 327, '2021-01-08'),
(5034, 341, '2021-01-08'),
(5035, 310, '2021-01-08'),
(5036, 306, '2021-01-08'),
(5037, 306, '2021-01-08'),
(5038, 332, '2021-01-08'),
(5039, 308, '2021-01-08'),
(5040, 309, '2021-01-08'),
(5041, 318, '2021-01-08'),
(5042, 301, '2021-01-08'),
(5043, 331, '2021-01-08'),
(5044, 336, '2021-01-08'),
(5045, 310, '2021-01-08'),
(5046, 336, '2021-01-08'),
(5047, 331, '2021-01-08'),
(5048, 305, '2021-01-08'),
(5049, 302, '2021-01-08'),
(5050, 326, '2021-01-08'),
(5051, 309, '2021-01-08'),
(5052, 311, '2021-01-08'),
(5053, 309, '2021-01-08'),
(5054, 95, '2021-01-08'),
(5055, 329, '2021-01-08'),
(5056, 341, '2021-01-08'),
(5057, 95, '2021-01-08'),
(5058, 306, '2021-01-08'),
(5059, 95, '2021-01-08'),
(5060, 340, '2021-01-08'),
(5061, 309, '2021-01-09'),
(5062, 306, '2021-01-09'),
(5063, 313, '2021-01-09'),
(5064, 340, '2021-01-09'),
(5065, 341, '2021-01-09'),
(5066, 337, '2021-01-09'),
(5067, 331, '2021-01-09'),
(5068, 341, '2021-01-09'),
(5069, 310, '2021-01-09'),
(5070, 95, '2021-01-09'),
(5071, 341, '2021-01-09'),
(5072, 306, '2021-01-09'),
(5073, 309, '2021-01-09'),
(5074, 337, '2021-01-09'),
(5075, 306, '2021-01-09'),
(5076, 95, '2021-01-09'),
(5077, 337, '2021-01-09'),
(5078, 95, '2021-01-09'),
(5079, 306, '2021-01-09'),
(5080, 341, '2021-01-09'),
(5081, 95, '2021-01-09'),
(5082, 324, '2021-01-09'),
(5083, 309, '2021-01-09'),
(5084, 307, '2021-01-09'),
(5085, 310, '2021-01-09'),
(5086, 308, '2021-01-09'),
(5087, 307, '2021-01-09'),
(5088, 308, '2021-01-09'),
(5089, 310, '2021-01-09'),
(5090, 305, '2021-01-09'),
(5091, 309, '2021-01-09'),
(5092, 310, '2021-01-09'),
(5093, 337, '2021-01-09'),
(5094, 341, '2021-01-09'),
(5095, 306, '2021-01-09'),
(5096, 310, '2021-01-09'),
(5097, 95, '2021-01-09'),
(5098, 309, '2021-01-09'),
(5099, 95, '2021-01-09'),
(5100, 337, '2021-01-09'),
(5101, 306, '2021-01-09'),
(5102, 336, '2021-01-09'),
(5103, 331, '2021-01-09'),
(5104, 335, '2021-01-09'),
(5105, 332, '2021-01-09'),
(5106, 328, '2021-01-09'),
(5107, 330, '2021-01-09'),
(5108, 329, '2021-01-09'),
(5109, 334, '2021-01-09'),
(5110, 333, '2021-01-09'),
(5111, 340, '2021-01-09'),
(5112, 341, '2021-01-09'),
(5113, 327, '2021-01-09'),
(5114, 332, '2021-01-09'),
(5115, 329, '2021-01-09'),
(5116, 330, '2021-01-09'),
(5117, 334, '2021-01-09'),
(5118, 326, '2021-01-09'),
(5119, 331, '2021-01-09'),
(5120, 326, '2021-01-09'),
(5121, 328, '2021-01-09'),
(5122, 327, '2021-01-09'),
(5123, 333, '2021-01-09'),
(5124, 336, '2021-01-09'),
(5125, 305, '2021-01-09'),
(5126, 335, '2021-01-09'),
(5127, 300, '2021-01-09'),
(5128, 301, '2021-01-09'),
(5129, 302, '2021-01-09'),
(5130, 308, '2021-01-09'),
(5131, 305, '2021-01-09'),
(5132, 307, '2021-01-09'),
(5133, 311, '2021-01-09'),
(5134, 303, '2021-01-09'),
(5135, 304, '2021-01-09'),
(5136, 311, '2021-01-09'),
(5137, 340, '2021-01-09'),
(5138, 302, '2021-01-09'),
(5139, 303, '2021-01-09'),
(5140, 307, '2021-01-09'),
(5141, 304, '2021-01-09'),
(5142, 308, '2021-01-09'),
(5143, 301, '2021-01-09'),
(5144, 300, '2021-01-09'),
(5145, 312, '2021-01-09'),
(5146, 314, '2021-01-09'),
(5147, 313, '2021-01-09'),
(5148, 315, '2021-01-09'),
(5149, 325, '2021-01-09'),
(5150, 317, '2021-01-09'),
(5151, 319, '2021-01-09'),
(5152, 324, '2021-01-09'),
(5153, 316, '2021-01-09'),
(5154, 318, '2021-01-09'),
(5155, 322, '2021-01-09'),
(5156, 320, '2021-01-09'),
(5157, 323, '2021-01-09'),
(5158, 321, '2021-01-09'),
(5159, 317, '2021-01-09'),
(5160, 122, '2021-01-09'),
(5161, 324, '2021-01-09'),
(5162, 323, '2021-01-09'),
(5163, 318, '2021-01-09'),
(5164, 320, '2021-01-09'),
(5165, 319, '2021-01-09'),
(5166, 315, '2021-01-09'),
(5167, 322, '2021-01-09'),
(5168, 321, '2021-01-09'),
(5169, 316, '2021-01-09'),
(5170, 314, '2021-01-09'),
(5171, 325, '2021-01-09'),
(5172, 313, '2021-01-09'),
(5173, 312, '2021-01-09'),
(5174, 122, '2021-01-09'),
(5175, 309, '2021-01-10'),
(5176, 332, '2021-01-10'),
(5177, 306, '2021-01-10'),
(5178, 328, '2021-01-10'),
(5179, 341, '2021-01-10'),
(5180, 95, '2021-01-10'),
(5181, 332, '2021-01-10'),
(5182, 306, '2021-01-10'),
(5183, 95, '2021-01-10'),
(5184, 341, '2021-01-10'),
(5185, 309, '2021-01-10'),
(5186, 307, '2021-01-10'),
(5187, 95, '2021-01-10'),
(5188, 334, '2021-01-10'),
(5189, 326, '2021-01-10'),
(5190, 95, '2021-01-10'),
(5191, 313, '2021-01-10'),
(5192, 306, '2021-01-10'),
(5193, 306, '2021-01-10'),
(5194, 327, '2021-01-10'),
(5195, 335, '2021-01-10'),
(5196, 336, '2021-01-10'),
(5197, 341, '2021-01-10'),
(5198, 95, '2021-01-10'),
(5199, 318, '2021-01-10'),
(5200, 309, '2021-01-10'),
(5201, 306, '2021-01-10'),
(5202, 340, '2021-01-10'),
(5203, 300, '2021-01-10'),
(5204, 309, '2021-01-10'),
(5205, 310, '2021-01-10'),
(5206, 95, '2021-01-10'),
(5207, 95, '2021-01-10'),
(5208, 306, '2021-01-10'),
(5209, 301, '2021-01-10'),
(5210, 334, '2021-01-11'),
(5211, 304, '2021-01-11'),
(5212, 306, '2021-01-11'),
(5213, 341, '2021-01-11'),
(5214, 341, '2021-01-11'),
(5215, 309, '2021-01-11'),
(5216, 336, '2021-01-11'),
(5217, 95, '2021-01-11'),
(5218, 334, '2021-01-11'),
(5219, 308, '2021-01-11'),
(5220, 335, '2021-01-11'),
(5221, 335, '2021-01-11'),
(5222, 337, '2021-01-11'),
(5223, 95, '2021-01-11'),
(5224, 337, '2021-01-11'),
(5225, 309, '2021-01-11'),
(5226, 337, '2021-01-11'),
(5227, 341, '2021-01-11'),
(5228, 310, '2021-01-11'),
(5229, 95, '2021-01-11'),
(5230, 341, '2021-01-12'),
(5231, 340, '2021-01-12'),
(5232, 122, '2021-01-12'),
(5233, 333, '2021-01-12'),
(5234, 310, '2021-01-12'),
(5235, 307, '2021-01-12'),
(5236, 341, '2021-01-12'),
(5237, 122, '2021-01-12'),
(5238, 309, '2021-01-12'),
(5239, 309, '2021-01-12'),
(5240, 95, '2021-01-12'),
(5241, 306, '2021-01-12'),
(5242, 309, '2021-01-12'),
(5243, 310, '2021-01-12'),
(5244, 337, '2021-01-12'),
(5245, 341, '2021-01-12'),
(5246, 340, '2021-01-12'),
(5247, 307, '2021-01-12'),
(5248, 306, '2021-01-12'),
(5249, 337, '2021-01-12'),
(5250, 320, '2021-01-12'),
(5251, 315, '2021-01-12'),
(5252, 309, '2021-01-13'),
(5253, 331, '2021-01-13'),
(5254, 340, '2021-01-13'),
(5255, 311, '2021-01-13'),
(5256, 337, '2021-01-13'),
(5257, 336, '2021-01-13'),
(5258, 334, '2021-01-13'),
(5259, 335, '2021-01-13'),
(5260, 330, '2021-01-13'),
(5261, 310, '2021-01-13'),
(5262, 341, '2021-01-13'),
(5263, 341, '2021-01-13'),
(5264, 301, '2021-01-13'),
(5265, 341, '2021-01-13'),
(5266, 312, '2021-01-13'),
(5267, 326, '2021-01-13'),
(5268, 337, '2021-01-13'),
(5269, 329, '2021-01-13'),
(5270, 307, '2021-01-13'),
(5271, 95, '2021-01-13'),
(5272, 308, '2021-01-13'),
(5273, 327, '2021-01-13'),
(5274, 330, '2021-01-13'),
(5275, 322, '2021-01-13'),
(5276, 335, '2021-01-13'),
(5277, 341, '2021-01-13'),
(5278, 337, '2021-01-13'),
(5279, 310, '2021-01-13'),
(5280, 309, '2021-01-13'),
(5281, 306, '2021-01-13'),
(5282, 95, '2021-01-13'),
(5283, 340, '2021-01-13'),
(5284, 333, '2021-01-13'),
(5285, 311, '2021-01-13'),
(5286, 336, '2021-01-13'),
(5287, 335, '2021-01-13'),
(5288, 334, '2021-01-13'),
(5289, 332, '2021-01-13'),
(5290, 331, '2021-01-13'),
(5291, 330, '2021-01-13'),
(5292, 329, '2021-01-13'),
(5293, 328, '2021-01-13'),
(5294, 327, '2021-01-13'),
(5295, 308, '2021-01-13'),
(5296, 307, '2021-01-13'),
(5297, 305, '2021-01-13'),
(5298, 305, '2021-01-13'),
(5299, 304, '2021-01-13'),
(5300, 303, '2021-01-13'),
(5301, 302, '2021-01-13'),
(5302, 301, '2021-01-13'),
(5303, 300, '2021-01-13'),
(5304, 326, '2021-01-13'),
(5305, 337, '2021-01-13'),
(5306, 325, '2021-01-13'),
(5307, 324, '2021-01-13'),
(5308, 323, '2021-01-13'),
(5309, 322, '2021-01-13'),
(5310, 321, '2021-01-13'),
(5311, 320, '2021-01-13'),
(5312, 319, '2021-01-13'),
(5313, 318, '2021-01-13'),
(5314, 317, '2021-01-13'),
(5315, 316, '2021-01-13'),
(5316, 315, '2021-01-13'),
(5317, 314, '2021-01-13'),
(5318, 313, '2021-01-13'),
(5319, 312, '2021-01-13'),
(5320, 122, '2021-01-13'),
(5321, 327, '2021-01-13'),
(5322, 326, '2021-01-13'),
(5323, 310, '2021-01-13'),
(5324, 310, '2021-01-13'),
(5325, 306, '2021-01-13'),
(5326, 300, '2021-01-13'),
(5327, 311, '2021-01-14'),
(5328, 310, '2021-01-14'),
(5329, 341, '2021-01-14'),
(5330, 306, '2021-01-14'),
(5331, 309, '2021-01-14'),
(5332, 95, '2021-01-14'),
(5333, 304, '2021-01-14'),
(5334, 310, '2021-01-14'),
(5335, 308, '2021-01-14'),
(5336, 332, '2021-01-14'),
(5337, 330, '2021-01-14'),
(5338, 337, '2021-01-14'),
(5339, 321, '2021-01-14'),
(5340, 328, '2021-01-14'),
(5341, 95, '2021-01-14'),
(5342, 95, '2021-01-14'),
(5343, 337, '2021-01-14'),
(5344, 316, '2021-01-14'),
(5345, 325, '2021-01-14'),
(5346, 334, '2021-01-14'),
(5347, 336, '2021-01-14'),
(5348, 95, '2021-01-14'),
(5349, 328, '2021-01-14'),
(5350, 328, '2021-01-14'),
(5351, 95, '2021-01-14'),
(5352, 306, '2021-01-14'),
(5353, 309, '2021-01-14'),
(5354, 310, '2021-01-14'),
(5355, 337, '2021-01-14'),
(5356, 341, '2021-01-14'),
(5357, 332, '2021-01-15'),
(5358, 310, '2021-01-15'),
(5359, 333, '2021-01-15'),
(5360, 337, '2021-01-15'),
(5361, 337, '2021-01-15'),
(5362, 337, '2021-01-15'),
(5363, 328, '2021-01-15'),
(5364, 306, '2021-01-15'),
(5365, 95, '2021-01-15'),
(5366, 306, '2021-01-15'),
(5367, 309, '2021-01-15'),
(5368, 310, '2021-01-15'),
(5369, 337, '2021-01-15'),
(5370, 341, '2021-01-15'),
(5371, 302, '2021-01-15'),
(5372, 341, '2021-01-15'),
(5373, 331, '2021-01-15'),
(5374, 310, '2021-01-15'),
(5375, 329, '2021-01-15'),
(5376, 337, '2021-01-15'),
(5377, 335, '2021-01-15'),
(5378, 340, '2021-01-15'),
(5379, 301, '2021-01-15'),
(5380, 327, '2021-01-16'),
(5381, 327, '2021-01-16'),
(5382, 333, '2021-01-16'),
(5383, 335, '2021-01-16'),
(5384, 95, '2021-01-16'),
(5385, 334, '2021-01-16'),
(5386, 336, '2021-01-16'),
(5387, 327, '2021-01-16'),
(5388, 337, '2021-01-16'),
(5389, 336, '2021-01-16'),
(5390, 95, '2021-01-16'),
(5391, 323, '2021-01-16'),
(5392, 327, '2021-01-16'),
(5393, 304, '2021-01-16'),
(5394, 304, '2021-01-16'),
(5395, 321, '2021-01-16'),
(5396, 329, '2021-01-16'),
(5397, 331, '2021-01-16'),
(5398, 309, '2021-01-16'),
(5399, 326, '2021-01-16'),
(5400, 329, '2021-01-16'),
(5401, 334, '2021-01-16'),
(5402, 310, '2021-01-16'),
(5403, 321, '2021-01-16'),
(5404, 335, '2021-01-16'),
(5405, 328, '2021-01-16'),
(5406, 319, '2021-01-16'),
(5407, 308, '2021-01-16'),
(5408, 309, '2021-01-16'),
(5409, 310, '2021-01-16'),
(5410, 337, '2021-01-16'),
(5411, 341, '2021-01-16'),
(5412, 95, '2021-01-16'),
(5413, 306, '2021-01-16'),
(5414, 337, '2021-01-16'),
(5415, 306, '2021-01-16'),
(5416, 310, '2021-01-16'),
(5417, 336, '2021-01-16'),
(5418, 331, '2021-01-16'),
(5419, 335, '2021-01-16'),
(5420, 332, '2021-01-16'),
(5421, 328, '2021-01-16'),
(5422, 330, '2021-01-16'),
(5423, 329, '2021-01-16'),
(5424, 334, '2021-01-16'),
(5425, 309, '2021-01-16'),
(5426, 333, '2021-01-16'),
(5427, 340, '2021-01-16'),
(5428, 95, '2021-01-16'),
(5429, 327, '2021-01-16'),
(5430, 326, '2021-01-16'),
(5431, 305, '2021-01-16'),
(5432, 302, '2021-01-16'),
(5433, 311, '2021-01-16'),
(5434, 341, '2021-01-16'),
(5435, 332, '2021-01-16'),
(5436, 329, '2021-01-16'),
(5437, 330, '2021-01-16'),
(5438, 334, '2021-01-16'),
(5439, 326, '2021-01-16'),
(5440, 331, '2021-01-16'),
(5441, 328, '2021-01-16'),
(5442, 327, '2021-01-16'),
(5443, 333, '2021-01-16'),
(5444, 336, '2021-01-16'),
(5445, 303, '2021-01-16'),
(5446, 335, '2021-01-16'),
(5447, 300, '2021-01-16'),
(5448, 301, '2021-01-16'),
(5449, 302, '2021-01-16'),
(5450, 308, '2021-01-16'),
(5451, 305, '2021-01-16'),
(5452, 307, '2021-01-16'),
(5453, 311, '2021-01-16'),
(5454, 307, '2021-01-16'),
(5455, 303, '2021-01-16'),
(5456, 304, '2021-01-16'),
(5457, 304, '2021-01-16'),
(5458, 308, '2021-01-16'),
(5459, 301, '2021-01-16'),
(5460, 340, '2021-01-16'),
(5461, 300, '2021-01-16'),
(5462, 312, '2021-01-16'),
(5463, 314, '2021-01-16'),
(5464, 313, '2021-01-16'),
(5465, 315, '2021-01-16'),
(5466, 122, '2021-01-16'),
(5467, 325, '2021-01-16'),
(5468, 317, '2021-01-16'),
(5469, 319, '2021-01-16'),
(5470, 324, '2021-01-16'),
(5471, 316, '2021-01-16'),
(5472, 318, '2021-01-16'),
(5473, 322, '2021-01-16'),
(5474, 320, '2021-01-16'),
(5475, 323, '2021-01-16'),
(5476, 321, '2021-01-16'),
(5477, 317, '2021-01-16'),
(5478, 324, '2021-01-16'),
(5479, 323, '2021-01-16'),
(5480, 318, '2021-01-16'),
(5481, 320, '2021-01-16'),
(5482, 319, '2021-01-16'),
(5483, 315, '2021-01-16'),
(5484, 322, '2021-01-16'),
(5485, 321, '2021-01-16'),
(5486, 316, '2021-01-16'),
(5487, 314, '2021-01-16'),
(5488, 325, '2021-01-16'),
(5489, 313, '2021-01-16'),
(5490, 312, '2021-01-16'),
(5491, 122, '2021-01-16'),
(5492, 328, '2021-01-16'),
(5493, 301, '2021-01-16'),
(5494, 95, '2021-01-17'),
(5495, 306, '2021-01-17'),
(5496, 309, '2021-01-17'),
(5497, 310, '2021-01-17'),
(5498, 337, '2021-01-17'),
(5499, 341, '2021-01-17'),
(5500, 331, '2021-01-17'),
(5501, 317, '2021-01-17'),
(5502, 322, '2021-01-17'),
(5503, 330, '2021-01-17'),
(5504, 332, '2021-01-17'),
(5505, 311, '2021-01-17'),
(5506, 304, '2021-01-17'),
(5507, 306, '2021-01-17'),
(5508, 302, '2021-01-17'),
(5509, 306, '2021-01-17'),
(5510, 304, '2021-01-17'),
(5511, 323, '2021-01-17'),
(5512, 307, '2021-01-17'),
(5513, 336, '2021-01-17'),
(5514, 326, '2021-01-17'),
(5515, 303, '2021-01-17'),
(5516, 305, '2021-01-17'),
(5517, 321, '2021-01-17'),
(5518, 95, '2021-01-17'),
(5519, 333, '2021-01-17'),
(5520, 305, '2021-01-17'),
(5521, 300, '2021-01-18'),
(5522, 304, '2021-01-18'),
(5523, 332, '2021-01-18'),
(5524, 309, '2021-01-18'),
(5525, 341, '2021-01-18'),
(5526, 329, '2021-01-18'),
(5527, 336, '2021-01-18'),
(5528, 308, '2021-01-18'),
(5529, 337, '2021-01-18'),
(5530, 329, '2021-01-18'),
(5531, 331, '2021-01-18'),
(5532, 302, '2021-01-18'),
(5533, 325, '2021-01-18'),
(5534, 325, '2021-01-18'),
(5535, 304, '2021-01-18'),
(5536, 324, '2021-01-18'),
(5537, 322, '2021-01-18'),
(5538, 316, '2021-01-18'),
(5539, 327, '2021-01-18'),
(5540, 321, '2021-01-18'),
(5541, 308, '2021-01-18'),
(5542, 332, '2021-01-18'),
(5543, 327, '2021-01-18'),
(5544, 305, '2021-01-18'),
(5545, 314, '2021-01-18'),
(5546, 315, '2021-01-18'),
(5547, 319, '2021-01-18'),
(5548, 95, '2021-01-18'),
(5549, 306, '2021-01-18'),
(5550, 309, '2021-01-18'),
(5551, 310, '2021-01-18'),
(5552, 337, '2021-01-18'),
(5553, 341, '2021-01-18'),
(5554, 319, '2021-01-18'),
(5555, 303, '2021-01-19'),
(5556, 333, '2021-01-19'),
(5557, 311, '2021-01-19'),
(5558, 95, '2021-01-19'),
(5559, 306, '2021-01-19'),
(5560, 309, '2021-01-19'),
(5561, 310, '2021-01-19'),
(5562, 337, '2021-01-19'),
(5563, 341, '2021-01-19'),
(5564, 300, '2021-01-19'),
(5565, 313, '2021-01-19'),
(5566, 307, '2021-01-19'),
(5567, 324, '2021-01-19'),
(5568, 341, '2021-01-19'),
(5569, 341, '2021-01-19'),
(5570, 316, '2021-01-19'),
(5571, 318, '2021-01-19'),
(5572, 308, '2021-01-19'),
(5573, 319, '2021-01-19'),
(5574, 324, '2021-01-19'),
(5575, 311, '2021-01-19'),
(5576, 311, '2021-01-19'),
(5577, 336, '2021-01-19'),
(5578, 319, '2021-01-19'),
(5579, 309, '2021-01-19'),
(5580, 323, '2021-01-19'),
(5581, 325, '2021-01-19'),
(5582, 337, '2021-01-19'),
(5583, 324, '2021-01-19'),
(5584, 315, '2021-01-19'),
(5585, 321, '2021-01-19'),
(5586, 317, '2021-01-19'),
(5587, 317, '2021-01-19'),
(5588, 313, '2021-01-19'),
(5589, 316, '2021-01-19'),
(5590, 307, '2021-01-19'),
(5591, 333, '2021-01-19'),
(5592, 320, '2021-01-19'),
(5593, 314, '2021-01-19'),
(5594, 313, '2021-01-19'),
(5595, 322, '2021-01-19'),
(5596, 327, '2021-01-19'),
(5597, 329, '2021-01-19'),
(5598, 328, '2021-01-19'),
(5599, 329, '2021-01-19'),
(5600, 317, '2021-01-19'),
(5601, 317, '2021-01-19'),
(5602, 309, '2021-01-20'),
(5603, 310, '2021-01-20'),
(5604, 310, '2021-01-20'),
(5605, 335, '2021-01-20'),
(5606, 320, '2021-01-20'),
(5607, 301, '2021-01-20'),
(5608, 323, '2021-01-20'),
(5609, 341, '2021-01-20'),
(5610, 318, '2021-01-20'),
(5611, 311, '2021-01-20'),
(5612, 332, '2021-01-20'),
(5613, 301, '2021-01-20'),
(5614, 309, '2021-01-20'),
(5615, 314, '2021-01-20'),
(5616, 307, '2021-01-20'),
(5617, 306, '2021-01-20'),
(5618, 326, '2021-01-20'),
(5619, 95, '2021-01-20'),
(5620, 320, '2021-01-21'),
(5621, 341, '2021-01-21'),
(5622, 320, '2021-01-21'),
(5623, 306, '2021-01-21'),
(5624, 340, '2021-01-21'),
(5625, 310, '2021-01-21'),
(5626, 310, '2021-01-21'),
(5627, 337, '2021-01-21'),
(5628, 337, '2021-01-21'),
(5629, 315, '2021-01-21'),
(5630, 341, '2021-01-21'),
(5631, 318, '2021-01-21'),
(5632, 311, '2021-01-21'),
(5633, 309, '2021-01-21'),
(5634, 333, '2021-01-21'),
(5635, 317, '2021-01-21'),
(5636, 335, '2021-01-21'),
(5637, 332, '2021-01-21'),
(5638, 315, '2021-01-21'),
(5639, 321, '2021-01-21'),
(5640, 337, '2021-01-21'),
(5641, 325, '2021-01-21'),
(5642, 333, '2021-01-21'),
(5643, 322, '2021-01-21'),
(5644, 337, '2021-01-21'),
(5645, 324, '2021-01-21'),
(5646, 319, '2021-01-21'),
(5647, 310, '2021-01-21'),
(5648, 310, '2021-01-21'),
(5649, 312, '2021-01-21'),
(5650, 320, '2021-01-21'),
(5651, 323, '2021-01-21'),
(5652, 312, '2021-01-21'),
(5653, 95, '2021-01-21'),
(5654, 309, '2021-01-21'),
(5655, 340, '2021-01-21'),
(5656, 333, '2021-01-21'),
(5657, 309, '2021-01-21'),
(5658, 306, '2021-01-22'),
(5659, 335, '2021-01-22'),
(5660, 334, '2021-01-22'),
(5661, 316, '2021-01-22'),
(5662, 326, '2021-01-22'),
(5663, 310, '2021-01-22'),
(5664, 336, '2021-01-22'),
(5665, 301, '2021-01-22'),
(5666, 300, '2021-01-22'),
(5667, 304, '2021-01-22'),
(5668, 303, '2021-01-22'),
(5669, 330, '2021-01-22'),
(5670, 309, '2021-01-22'),
(5671, 341, '2021-01-23'),
(5672, 309, '2021-01-23'),
(5673, 330, '2021-01-23'),
(5674, 309, '2021-01-23'),
(5675, 331, '2021-01-23'),
(5676, 310, '2021-01-23'),
(5677, 332, '2021-01-23'),
(5678, 309, '2021-01-23'),
(5679, 310, '2021-01-23'),
(5680, 337, '2021-01-23'),
(5681, 341, '2021-01-23'),
(5682, 306, '2021-01-23'),
(5683, 95, '2021-01-23'),
(5684, 310, '2021-01-23'),
(5685, 309, '2021-01-23'),
(5686, 95, '2021-01-23'),
(5687, 337, '2021-01-23'),
(5688, 306, '2021-01-23'),
(5689, 336, '2021-01-23'),
(5690, 331, '2021-01-23'),
(5691, 335, '2021-01-23'),
(5692, 332, '2021-01-23'),
(5693, 328, '2021-01-23'),
(5694, 330, '2021-01-23'),
(5695, 329, '2021-01-23'),
(5696, 334, '2021-01-23'),
(5697, 333, '2021-01-23'),
(5698, 340, '2021-01-23'),
(5699, 327, '2021-01-23'),
(5700, 326, '2021-01-23'),
(5701, 341, '2021-01-23'),
(5702, 305, '2021-01-23'),
(5703, 332, '2021-01-23'),
(5704, 329, '2021-01-23'),
(5705, 330, '2021-01-23'),
(5706, 334, '2021-01-23'),
(5707, 326, '2021-01-23'),
(5708, 331, '2021-01-23'),
(5709, 328, '2021-01-23'),
(5710, 327, '2021-01-23'),
(5711, 333, '2021-01-23'),
(5712, 302, '2021-01-23'),
(5713, 336, '2021-01-23'),
(5714, 335, '2021-01-23'),
(5715, 300, '2021-01-23'),
(5716, 311, '2021-01-23'),
(5717, 301, '2021-01-23'),
(5718, 302, '2021-01-23'),
(5719, 308, '2021-01-23'),
(5720, 305, '2021-01-23'),
(5721, 307, '2021-01-23'),
(5722, 311, '2021-01-23'),
(5723, 303, '2021-01-23'),
(5724, 304, '2021-01-23'),
(5725, 340, '2021-01-23'),
(5726, 303, '2021-01-23'),
(5727, 307, '2021-01-23'),
(5728, 304, '2021-01-23'),
(5729, 308, '2021-01-23'),
(5730, 301, '2021-01-23'),
(5731, 300, '2021-01-23'),
(5732, 312, '2021-01-23'),
(5733, 314, '2021-01-23'),
(5734, 313, '2021-01-23'),
(5735, 315, '2021-01-23'),
(5736, 122, '2021-01-23'),
(5737, 325, '2021-01-23'),
(5738, 317, '2021-01-23'),
(5739, 319, '2021-01-23'),
(5740, 324, '2021-01-23'),
(5741, 316, '2021-01-23'),
(5742, 318, '2021-01-23'),
(5743, 322, '2021-01-23'),
(5744, 320, '2021-01-23'),
(5745, 323, '2021-01-23'),
(5746, 321, '2021-01-23'),
(5747, 317, '2021-01-23'),
(5748, 324, '2021-01-23'),
(5749, 323, '2021-01-23'),
(5750, 318, '2021-01-23'),
(5751, 320, '2021-01-23'),
(5752, 319, '2021-01-23'),
(5753, 315, '2021-01-23'),
(5754, 322, '2021-01-23'),
(5755, 321, '2021-01-23'),
(5756, 316, '2021-01-23'),
(5757, 314, '2021-01-23'),
(5758, 325, '2021-01-23'),
(5759, 313, '2021-01-23'),
(5760, 312, '2021-01-23'),
(5761, 122, '2021-01-23'),
(5762, 303, '2021-01-23'),
(5763, 306, '2021-01-23'),
(5764, 310, '2021-01-24'),
(5765, 306, '2021-01-24'),
(5766, 341, '2021-01-24'),
(5767, 95, '2021-01-24'),
(5768, 337, '2021-01-24'),
(5769, 300, '2021-01-24'),
(5770, 95, '2021-01-24'),
(5771, 312, '2021-01-24'),
(5772, 318, '2021-01-24'),
(5773, 337, '2021-01-25'),
(5774, 321, '2021-01-25'),
(5775, 311, '2021-01-25'),
(5776, 309, '2021-01-25'),
(5777, 310, '2021-01-25'),
(5778, 317, '2021-01-25'),
(5779, 335, '2021-01-25'),
(5780, 95, '2021-01-25'),
(5781, 341, '2021-01-25'),
(5782, 322, '2021-01-25'),
(5783, 310, '2021-01-25'),
(5784, 330, '2021-01-25'),
(5785, 310, '2021-01-25'),
(5786, 337, '2021-01-25'),
(5787, 300, '2021-01-26'),
(5788, 301, '2021-01-26'),
(5789, 341, '2021-01-26'),
(5790, 308, '2021-01-26'),
(5791, 306, '2021-01-26'),
(5792, 337, '2021-01-26'),
(5793, 310, '2021-01-26'),
(5794, 309, '2021-01-26'),
(5795, 340, '2021-01-26'),
(5796, 320, '2021-01-27'),
(5797, 322, '2021-01-27'),
(5798, 340, '2021-01-27'),
(5799, 308, '2021-01-27'),
(5800, 310, '2021-01-27'),
(5801, 309, '2021-01-27'),
(5802, 341, '2021-01-27'),
(5803, 341, '2021-01-27'),
(5804, 336, '2021-01-27'),
(5805, 310, '2021-01-27'),
(5806, 327, '2021-01-27'),
(5807, 326, '2021-01-27'),
(5808, 335, '2021-01-27'),
(5809, 310, '2021-01-27'),
(5810, 316, '2021-01-27'),
(5811, 310, '2021-01-27'),
(5812, 329, '2021-01-28'),
(5813, 326, '2021-01-28'),
(5814, 341, '2021-01-28'),
(5815, 330, '2021-01-28'),
(5816, 328, '2021-01-28'),
(5817, 308, '2021-01-28'),
(5818, 312, '2021-01-28'),
(5819, 329, '2021-01-28'),
(5820, 302, '2021-01-28'),
(5821, 340, '2021-01-28'),
(5822, 314, '2021-01-28'),
(5823, 323, '2021-01-28'),
(5824, 306, '2021-01-28'),
(5825, 321, '2021-01-28'),
(5826, 324, '2021-01-28'),
(5827, 314, '2021-01-28'),
(5828, 309, '2021-01-28'),
(5829, 330, '2021-01-28'),
(5830, 341, '2021-01-28'),
(5831, 95, '2021-01-29'),
(5832, 335, '2021-01-29'),
(5833, 309, '2021-01-29'),
(5834, 310, '2021-01-29'),
(5835, 341, '2021-01-29'),
(5836, 334, '2021-01-29'),
(5837, 311, '2021-01-29'),
(5838, 337, '2021-01-29'),
(5839, 337, '2021-01-29'),
(5840, 333, '2021-01-29'),
(5841, 309, '2021-01-30'),
(5842, 305, '2021-01-30'),
(5843, 309, '2021-01-30'),
(5844, 340, '2021-01-30'),
(5845, 309, '2021-01-30'),
(5846, 310, '2021-01-30'),
(5847, 337, '2021-01-30'),
(5848, 341, '2021-01-30'),
(5849, 306, '2021-01-30'),
(5850, 310, '2021-01-30'),
(5851, 95, '2021-01-30'),
(5852, 309, '2021-01-30'),
(5853, 95, '2021-01-30'),
(5854, 337, '2021-01-30'),
(5855, 306, '2021-01-30'),
(5856, 336, '2021-01-30'),
(5857, 331, '2021-01-30'),
(5858, 335, '2021-01-30'),
(5859, 332, '2021-01-30'),
(5860, 328, '2021-01-30'),
(5861, 330, '2021-01-30'),
(5862, 329, '2021-01-30'),
(5863, 334, '2021-01-30'),
(5864, 341, '2021-01-30'),
(5865, 333, '2021-01-30'),
(5866, 340, '2021-01-30'),
(5867, 333, '2021-01-30'),
(5868, 300, '2021-01-30'),
(5869, 301, '2021-01-30'),
(5870, 302, '2021-01-30'),
(5871, 308, '2021-01-30'),
(5872, 327, '2021-01-30'),
(5873, 305, '2021-01-30'),
(5874, 307, '2021-01-30'),
(5875, 311, '2021-01-30'),
(5876, 303, '2021-01-30'),
(5877, 304, '2021-01-30'),
(5878, 326, '2021-01-30'),
(5879, 305, '2021-01-30'),
(5880, 332, '2021-01-30'),
(5881, 329, '2021-01-30'),
(5882, 330, '2021-01-30'),
(5883, 334, '2021-01-30'),
(5884, 326, '2021-01-30'),
(5885, 331, '2021-01-30'),
(5886, 328, '2021-01-30'),
(5887, 327, '2021-01-30'),
(5888, 302, '2021-01-30'),
(5889, 336, '2021-01-30'),
(5890, 335, '2021-01-30'),
(5891, 340, '2021-01-30'),
(5892, 311, '2021-01-30'),
(5893, 303, '2021-01-30'),
(5894, 307, '2021-01-30'),
(5895, 304, '2021-01-30'),
(5896, 308, '2021-01-30'),
(5897, 301, '2021-01-30'),
(5898, 300, '2021-01-30'),
(5899, 312, '2021-01-30'),
(5900, 314, '2021-01-30'),
(5901, 313, '2021-01-30'),
(5902, 315, '2021-01-30'),
(5903, 122, '2021-01-30'),
(5904, 325, '2021-01-30'),
(5905, 317, '2021-01-30'),
(5906, 319, '2021-01-30'),
(5907, 324, '2021-01-30'),
(5908, 316, '2021-01-30'),
(5909, 318, '2021-01-30'),
(5910, 322, '2021-01-30'),
(5911, 320, '2021-01-30'),
(5912, 323, '2021-01-30'),
(5913, 321, '2021-01-30'),
(5914, 317, '2021-01-30'),
(5915, 324, '2021-01-30'),
(5916, 323, '2021-01-30'),
(5917, 318, '2021-01-30'),
(5918, 320, '2021-01-30'),
(5919, 319, '2021-01-30'),
(5920, 315, '2021-01-30'),
(5921, 322, '2021-01-30'),
(5922, 321, '2021-01-30'),
(5923, 316, '2021-01-30'),
(5924, 314, '2021-01-30'),
(5925, 325, '2021-01-30'),
(5926, 313, '2021-01-30'),
(5927, 312, '2021-01-30'),
(5928, 122, '2021-01-30'),
(5929, 311, '2021-01-30'),
(5930, 341, '2021-01-30'),
(5931, 335, '2021-01-30'),
(5932, 335, '2021-01-30'),
(5933, 335, '2021-01-30'),
(5934, 309, '2021-01-30'),
(5935, 340, '2021-01-30'),
(5936, 337, '2021-01-30'),
(5937, 306, '2021-01-30'),
(5938, 327, '2021-01-31'),
(5939, 95, '2021-01-31'),
(5940, 310, '2021-01-31'),
(5941, 341, '2021-01-31'),
(5942, 95, '2021-01-31'),
(5943, 309, '2021-01-31'),
(5944, 337, '2021-01-31'),
(5945, 303, '2021-01-31'),
(5946, 333, '2021-01-31'),
(5947, 309, '2021-01-31'),
(5948, 305, '2021-01-31'),
(5949, 325, '2021-01-31'),
(5950, 326, '2021-01-31'),
(5951, 335, '2021-01-31'),
(5952, 327, '2021-01-31'),
(5953, 319, '2021-01-31'),
(5954, 306, '2021-01-31'),
(5955, 95, '2021-01-31'),
(5956, 309, '2021-01-31'),
(5957, 341, '2021-01-31'),
(5958, 337, '2021-01-31'),
(5959, 309, '2021-01-31'),
(5960, 95, '2021-01-31'),
(5961, 337, '2021-01-31'),
(5962, 326, '2021-01-31'),
(5963, 326, '2021-01-31'),
(5964, 330, '2021-01-31'),
(5965, 331, '2021-02-01'),
(5966, 309, '2021-02-01'),
(5967, 337, '2021-02-01'),
(5968, 309, '2021-02-01'),
(5969, 341, '2021-02-01'),
(5970, 306, '2021-02-01'),
(5971, 309, '2021-02-01'),
(5972, 334, '2021-02-01'),
(5973, 337, '2021-02-01'),
(5974, 333, '2021-02-01'),
(5975, 328, '2021-02-01'),
(5976, 336, '2021-02-01'),
(5977, 95, '2021-02-01'),
(5978, 309, '2021-02-01'),
(5979, 336, '2021-02-01'),
(5980, 341, '2021-02-01'),
(5981, 337, '2021-02-01'),
(5982, 330, '2021-02-01'),
(5983, 306, '2021-02-01'),
(5984, 326, '2021-02-01'),
(5985, 341, '2021-02-01'),
(5986, 333, '2021-02-01'),
(5987, 331, '2021-02-01'),
(5988, 95, '2021-02-01'),
(5989, 310, '2021-02-01'),
(5990, 309, '2021-02-01'),
(5991, 311, '2021-02-01'),
(5992, 122, '2021-02-01'),
(5993, 309, '2021-02-01'),
(5994, 309, '2021-02-01'),
(5995, 337, '2021-02-01'),
(5996, 329, '2021-02-01'),
(5997, 328, '2021-02-01'),
(5998, 325, '2021-02-02'),
(5999, 306, '2021-02-02'),
(6000, 331, '2021-02-02'),
(6001, 333, '2021-02-02'),
(6002, 122, '2021-02-02'),
(6003, 309, '2021-02-02'),
(6004, 309, '2021-02-02'),
(6005, 309, '2021-02-02'),
(6006, 337, '2021-02-02'),
(6007, 307, '2021-02-02'),
(6008, 307, '2021-02-02'),
(6009, 337, '2021-02-02'),
(6010, 318, '2021-02-02'),
(6011, 310, '2021-02-02'),
(6012, 95, '2021-02-02'),
(6013, 326, '2021-02-02'),
(6014, 306, '2021-02-02'),
(6015, 306, '2021-02-02'),
(6016, 333, '2021-02-02'),
(6017, 309, '2021-02-02'),
(6018, 333, '2021-02-02'),
(6019, 310, '2021-02-02'),
(6020, 337, '2021-02-02'),
(6021, 341, '2021-02-02'),
(6022, 306, '2021-02-02'),
(6023, 310, '2021-02-02'),
(6024, 95, '2021-02-02'),
(6025, 341, '2021-02-02'),
(6026, 309, '2021-02-02'),
(6027, 337, '2021-02-02'),
(6028, 334, '2021-02-02'),
(6029, 336, '2021-02-02'),
(6030, 95, '2021-02-02'),
(6031, 322, '2021-02-02'),
(6032, 337, '2021-02-02'),
(6033, 306, '2021-02-02'),
(6034, 309, '2021-02-02'),
(6035, 309, '2021-02-02'),
(6036, 309, '2021-02-03'),
(6037, 309, '2021-02-03'),
(6038, 309, '2021-02-03'),
(6039, 327, '2021-02-03'),
(6040, 337, '2021-02-03'),
(6041, 335, '2021-02-03'),
(6042, 326, '2021-02-03'),
(6043, 326, '2021-02-03'),
(6044, 306, '2021-02-03'),
(6045, 335, '2021-02-03'),
(6046, 337, '2021-02-03'),
(6047, 333, '2021-02-03'),
(6048, 95, '2021-02-03'),
(6049, 315, '2021-02-03'),
(6050, 340, '2021-02-03'),
(6051, 95, '2021-02-03'),
(6052, 335, '2021-02-03'),
(6053, 309, '2021-02-03'),
(6054, 329, '2021-02-03'),
(6055, 304, '2021-02-03'),
(6056, 336, '2021-02-03'),
(6057, 328, '2021-02-03'),
(6058, 327, '2021-02-03'),
(6059, 331, '2021-02-03'),
(6060, 333, '2021-02-03'),
(6061, 95, '2021-02-03'),
(6062, 304, '2021-02-03'),
(6063, 320, '2021-02-03'),
(6064, 334, '2021-02-03'),
(6065, 341, '2021-02-03'),
(6066, 332, '2021-02-03'),
(6067, 95, '2021-02-03'),
(6068, 306, '2021-02-03'),
(6069, 336, '2021-02-03'),
(6070, 332, '2021-02-03'),
(6071, 333, '2021-02-03'),
(6072, 309, '2021-02-03'),
(6073, 95, '2021-02-03'),
(6074, 313, '2021-02-03'),
(6075, 337, '2021-02-03'),
(6076, 305, '2021-02-03'),
(6077, 314, '2021-02-03'),
(6078, 341, '2021-02-04'),
(6079, 341, '2021-02-04'),
(6080, 327, '2021-02-04'),
(6081, 333, '2021-02-04'),
(6082, 326, '2021-02-04'),
(6083, 309, '2021-02-04'),
(6084, 305, '2021-02-04'),
(6085, 327, '2021-02-04'),
(6086, 341, '2021-02-04'),
(6087, 321, '2021-02-04'),
(6088, 333, '2021-02-04'),
(6089, 309, '2021-02-04'),
(6090, 309, '2021-02-04'),
(6091, 336, '2021-02-04'),
(6092, 95, '2021-02-04'),
(6093, 341, '2021-02-04'),
(6094, 328, '2021-02-04'),
(6095, 326, '2021-02-04'),
(6096, 324, '2021-02-04'),
(6097, 306, '2021-02-04'),
(6098, 310, '2021-02-04'),
(6099, 95, '2021-02-04'),
(6100, 341, '2021-02-04'),
(6101, 309, '2021-02-04'),
(6102, 337, '2021-02-04'),
(6103, 334, '2021-02-04'),
(6104, 300, '2021-02-04'),
(6105, 309, '2021-02-05'),
(6106, 333, '2021-02-05'),
(6107, 310, '2021-02-05'),
(6108, 325, '2021-02-05'),
(6109, 302, '2021-02-05'),
(6110, 308, '2021-02-05'),
(6111, 327, '2021-02-05'),
(6112, 322, '2021-02-05'),
(6113, 311, '2021-02-05'),
(6114, 341, '2021-02-05'),
(6115, 326, '2021-02-05'),
(6116, 309, '2021-02-05'),
(6117, 333, '2021-02-05'),
(6118, 311, '2021-02-05'),
(6119, 311, '2021-02-05'),
(6120, 311, '2021-02-05'),
(6121, 341, '2021-02-05'),
(6122, 303, '2021-02-05'),
(6123, 302, '2021-02-05'),
(6124, 326, '2021-02-05'),
(6125, 326, '2021-02-05'),
(6126, 310, '2021-02-05'),
(6127, 314, '2021-02-05'),
(6128, 332, '2021-02-05'),
(6129, 95, '2021-02-05'),
(6130, 306, '2021-02-05'),
(6131, 340, '2021-02-05'),
(6132, 311, '2021-02-05'),
(6133, 332, '2021-02-05'),
(6134, 310, '2021-02-05'),
(6135, 335, '2021-02-05'),
(6136, 318, '2021-02-05'),
(6137, 321, '2021-02-05'),
(6138, 335, '2021-02-05'),
(6139, 307, '2021-02-05'),
(6140, 301, '2021-02-05'),
(6141, 326, '2021-02-05'),
(6142, 303, '2021-02-05'),
(6143, 323, '2021-02-05'),
(6144, 331, '2021-02-06'),
(6145, 305, '2021-02-06'),
(6146, 335, '2021-02-06'),
(6147, 308, '2021-02-06'),
(6148, 315, '2021-02-06'),
(6149, 316, '2021-02-06'),
(6150, 324, '2021-02-06'),
(6151, 325, '2021-02-06'),
(6152, 300, '2021-02-06'),
(6153, 312, '2021-02-06'),
(6154, 316, '2021-02-06'),
(6155, 308, '2021-02-06'),
(6156, 95, '2021-02-06'),
(6157, 301, '2021-02-06'),
(6158, 302, '2021-02-06'),
(6159, 304, '2021-02-06'),
(6160, 303, '2021-02-06'),
(6161, 320, '2021-02-06'),
(6162, 307, '2021-02-06'),
(6163, 331, '2021-02-06'),
(6164, 302, '2021-02-06'),
(6165, 305, '2021-02-06'),
(6166, 304, '2021-02-06'),
(6167, 95, '2021-02-06'),
(6168, 306, '2021-02-06'),
(6169, 309, '2021-02-06'),
(6170, 336, '2021-02-06'),
(6171, 300, '2021-02-06'),
(6172, 310, '2021-02-06'),
(6173, 322, '2021-02-06'),
(6174, 311, '2021-02-06'),
(6175, 341, '2021-02-06'),
(6176, 328, '2021-02-06'),
(6177, 302, '2021-02-06'),
(6178, 305, '2021-02-06'),
(6179, 334, '2021-02-06'),
(6180, 303, '2021-02-06'),
(6181, 310, '2021-02-06'),
(6182, 330, '2021-02-06'),
(6183, 332, '2021-02-06'),
(6184, 329, '2021-02-06'),
(6185, 308, '2021-02-06'),
(6186, 309, '2021-02-06'),
(6187, 304, '2021-02-06'),
(6188, 308, '2021-02-06'),
(6189, 330, '2021-02-06'),
(6190, 306, '2021-02-06'),
(6191, 302, '2021-02-06'),
(6192, 329, '2021-02-06'),
(6193, 304, '2021-02-06'),
(6194, 314, '2021-02-07'),
(6195, 318, '2021-02-07'),
(6196, 323, '2021-02-07'),
(6197, 333, '2021-02-07'),
(6198, 334, '2021-02-07'),
(6199, 335, '2021-02-07'),
(6200, 336, '2021-02-07'),
(6201, 317, '2021-02-07'),
(6202, 313, '2021-02-07'),
(6203, 316, '2021-02-07'),
(6204, 314, '2021-02-07'),
(6205, 310, '2021-02-07'),
(6206, 337, '2021-02-07'),
(6207, 341, '2021-02-07'),
(6208, 337, '2021-02-07'),
(6209, 306, '2021-02-07'),
(6210, 310, '2021-02-07'),
(6211, 309, '2021-02-07'),
(6212, 95, '2021-02-07'),
(6213, 336, '2021-02-07'),
(6214, 330, '2021-02-07'),
(6215, 335, '2021-02-07'),
(6216, 331, '2021-02-07'),
(6217, 334, '2021-02-07'),
(6218, 328, '2021-02-07'),
(6219, 332, '2021-02-07'),
(6220, 320, '2021-02-07'),
(6221, 321, '2021-02-07'),
(6222, 307, '2021-02-07'),
(6223, 311, '2021-02-07'),
(6224, 303, '2021-02-07'),
(6225, 341, '2021-02-07'),
(6226, 327, '2021-02-07'),
(6227, 337, '2021-02-07'),
(6228, 333, '2021-02-07'),
(6229, 300, '2021-02-07'),
(6230, 327, '2021-02-07'),
(6231, 310, '2021-02-07'),
(6232, 316, '2021-02-08'),
(6233, 318, '2021-02-08'),
(6234, 322, '2021-02-08'),
(6235, 95, '2021-02-08'),
(6236, 300, '2021-02-08'),
(6237, 335, '2021-02-08'),
(6238, 319, '2021-02-08'),
(6239, 334, '2021-02-08'),
(6240, 324, '2021-02-08'),
(6241, 323, '2021-02-08'),
(6242, 325, '2021-02-08'),
(6243, 336, '2021-02-08'),
(6244, 313, '2021-02-08'),
(6245, 314, '2021-02-08'),
(6246, 308, '2021-02-08'),
(6247, 321, '2021-02-08'),
(6248, 317, '2021-02-08'),
(6249, 311, '2021-02-08'),
(6250, 320, '2021-02-08'),
(6251, 307, '2021-02-08'),
(6252, 322, '2021-02-08'),
(6253, 325, '2021-02-08'),
(6254, 324, '2021-02-08'),
(6255, 326, '2021-02-08'),
(6256, 308, '2021-02-08'),
(6257, 316, '2021-02-08'),
(6258, 313, '2021-02-09'),
(6259, 317, '2021-02-09'),
(6260, 341, '2021-02-09'),
(6261, 316, '2021-02-09'),
(6262, 326, '2021-02-09'),
(6263, 337, '2021-02-09'),
(6264, 309, '2021-02-09'),
(6265, 301, '2021-02-09'),
(6266, 337, '2021-02-09'),
(6267, 341, '2021-02-09'),
(6268, 333, '2021-02-09'),
(6269, 301, '2021-02-09'),
(6270, 309, '2021-02-09'),
(6271, 314, '2021-02-09'),
(6272, 309, '2021-02-09'),
(6273, 308, '2021-02-09'),
(6274, 315, '2021-02-09'),
(6275, 95, '2021-02-09'),
(6276, 95, '2021-02-09'),
(6277, 95, '2021-02-09'),
(6278, 326, '2021-02-09'),
(6279, 330, '2021-02-09'),
(6280, 331, '2021-02-09'),
(6281, 326, '2021-02-09'),
(6282, 332, '2021-02-09'),
(6283, 330, '2021-02-09'),
(6284, 337, '2021-02-09'),
(6285, 314, '2021-02-09'),
(6286, 318, '2021-02-09'),
(6287, 334, '2021-02-10'),
(6288, 329, '2021-02-10'),
(6289, 335, '2021-02-10'),
(6290, 326, '2021-02-10'),
(6291, 309, '2021-02-10'),
(6292, 341, '2021-02-10'),
(6293, 318, '2021-02-10'),
(6294, 341, '2021-02-10'),
(6295, 342, '2021-02-10'),
(6296, 306, '2021-02-10'),
(6297, 312, '2021-02-10'),
(6298, 322, '2021-02-10'),
(6299, 315, '2021-02-10'),
(6300, 341, '2021-02-10'),
(6301, 321, '2021-02-10'),
(6302, 309, '2021-02-10'),
(6303, 325, '2021-02-10'),
(6304, 322, '2021-02-10'),
(6305, 324, '2021-02-10'),
(6306, 319, '2021-02-10'),
(6307, 320, '2021-02-10'),
(6308, 312, '2021-02-11'),
(6309, 323, '2021-02-11'),
(6310, 304, '2021-02-11'),
(6311, 305, '2021-02-11'),
(6312, 337, '2021-02-11'),
(6313, 95, '2021-02-11'),
(6314, 340, '2021-02-11'),
(6315, 306, '2021-02-11'),
(6316, 316, '2021-02-11'),
(6317, 342, '2021-02-11'),
(6318, 343, '2021-02-11'),
(6319, 95, '2021-02-11'),
(6320, 310, '2021-02-11'),
(6321, 305, '2021-02-11'),
(6322, 95, '2021-02-11'),
(6323, 329, '2021-02-11'),
(6324, 309, '2021-02-11'),
(6325, 326, '2021-02-12'),
(6326, 310, '2021-02-12'),
(6327, 95, '2021-02-12'),
(6328, 309, '2021-02-12'),
(6329, 341, '2021-02-12'),
(6330, 341, '2021-02-12'),
(6331, 310, '2021-02-12'),
(6332, 318, '2021-02-12'),
(6333, 337, '2021-02-12'),
(6334, 95, '2021-02-12'),
(6335, 326, '2021-02-12'),
(6336, 326, '2021-02-12'),
(6337, 316, '2021-02-12'),
(6338, 311, '2021-02-12'),
(6339, 311, '2021-02-12'),
(6340, 311, '2021-02-12'),
(6341, 311, '2021-02-12'),
(6342, 311, '2021-02-12'),
(6343, 328, '2021-02-12'),
(6344, 329, '2021-02-12'),
(6345, 328, '2021-02-12'),
(6346, 329, '2021-02-12'),
(6347, 342, '2021-02-12'),
(6348, 319, '2021-02-12'),
(6349, 313, '2021-02-12'),
(6350, 341, '2021-02-12'),
(6351, 337, '2021-02-12'),
(6352, 306, '2021-02-12'),
(6353, 310, '2021-02-12'),
(6354, 309, '2021-02-12'),
(6355, 95, '2021-02-12'),
(6356, 336, '2021-02-12'),
(6357, 333, '2021-02-12'),
(6358, 330, '2021-02-12'),
(6359, 335, '2021-02-12'),
(6360, 331, '2021-02-12'),
(6361, 334, '2021-02-12'),
(6362, 328, '2021-02-12'),
(6363, 332, '2021-02-12'),
(6364, 325, '2021-02-12'),
(6365, 343, '2021-02-12'),
(6366, 305, '2021-02-12'),
(6367, 343, '2021-02-13'),
(6368, 342, '2021-02-13'),
(6369, 326, '2021-02-13'),
(6370, 309, '2021-02-13'),
(6371, 311, '2021-02-13'),
(6372, 324, '2021-02-13'),
(6373, 305, '2021-02-13'),
(6374, 326, '2021-02-13'),
(6375, 306, '2021-02-13'),
(6376, 328, '2021-02-13'),
(6377, 340, '2021-02-13'),
(6378, 309, '2021-02-13'),
(6379, 331, '2021-02-13'),
(6380, 321, '2021-02-13'),
(6381, 309, '2021-02-13'),
(6382, 310, '2021-02-13'),
(6383, 337, '2021-02-13'),
(6384, 306, '2021-02-13'),
(6385, 341, '2021-02-13'),
(6386, 310, '2021-02-13'),
(6387, 95, '2021-02-13'),
(6388, 309, '2021-02-13'),
(6389, 95, '2021-02-13'),
(6390, 337, '2021-02-13'),
(6391, 306, '2021-02-13'),
(6392, 343, '2021-02-13'),
(6393, 336, '2021-02-13'),
(6394, 331, '2021-02-13'),
(6395, 335, '2021-02-13'),
(6396, 332, '2021-02-13'),
(6397, 330, '2021-02-13'),
(6398, 334, '2021-02-13'),
(6399, 342, '2021-02-13'),
(6400, 333, '2021-02-13'),
(6401, 340, '2021-02-13'),
(6402, 341, '2021-02-13'),
(6403, 332, '2021-02-13'),
(6404, 329, '2021-02-13'),
(6405, 329, '2021-02-13'),
(6406, 330, '2021-02-13'),
(6407, 334, '2021-02-13'),
(6408, 326, '2021-02-13'),
(6409, 331, '2021-02-13'),
(6410, 328, '2021-02-13'),
(6411, 327, '2021-02-13'),
(6412, 333, '2021-02-13'),
(6413, 336, '2021-02-13'),
(6414, 335, '2021-02-13'),
(6415, 300, '2021-02-13'),
(6416, 301, '2021-02-13'),
(6417, 302, '2021-02-13'),
(6418, 308, '2021-02-13'),
(6419, 326, '2021-02-13'),
(6420, 305, '2021-02-13'),
(6421, 328, '2021-02-13'),
(6422, 307, '2021-02-13'),
(6423, 311, '2021-02-13'),
(6424, 327, '2021-02-13'),
(6425, 303, '2021-02-13'),
(6426, 304, '2021-02-13'),
(6427, 305, '2021-02-13'),
(6428, 340, '2021-02-13'),
(6429, 342, '2021-02-13'),
(6430, 343, '2021-02-13'),
(6431, 302, '2021-02-13'),
(6432, 311, '2021-02-13'),
(6433, 303, '2021-02-13'),
(6434, 307, '2021-02-13'),
(6435, 304, '2021-02-13'),
(6436, 308, '2021-02-13'),
(6437, 301, '2021-02-13'),
(6438, 300, '2021-02-13'),
(6439, 317, '2021-02-13'),
(6440, 314, '2021-02-13'),
(6441, 313, '2021-02-13'),
(6442, 315, '2021-02-13'),
(6443, 316, '2021-02-13'),
(6444, 312, '2021-02-13'),
(6445, 122, '2021-02-13'),
(6446, 325, '2021-02-13'),
(6447, 319, '2021-02-13'),
(6448, 324, '2021-02-13'),
(6449, 318, '2021-02-13'),
(6450, 322, '2021-02-13'),
(6451, 320, '2021-02-13'),
(6452, 323, '2021-02-13'),
(6453, 317, '2021-02-13'),
(6454, 321, '2021-02-13'),
(6455, 324, '2021-02-13'),
(6456, 323, '2021-02-13'),
(6457, 318, '2021-02-13'),
(6458, 320, '2021-02-13'),
(6459, 319, '2021-02-13'),
(6460, 315, '2021-02-13'),
(6461, 322, '2021-02-13'),
(6462, 321, '2021-02-13'),
(6463, 316, '2021-02-13'),
(6464, 314, '2021-02-13'),
(6465, 325, '2021-02-13'),
(6466, 313, '2021-02-13'),
(6467, 312, '2021-02-13'),
(6468, 122, '2021-02-13'),
(6469, 340, '2021-02-13'),
(6470, 306, '2021-02-13'),
(6471, 312, '2021-02-13'),
(6472, 328, '2021-02-13'),
(6473, 304, '2021-02-13'),
(6474, 95, '2021-02-14'),
(6475, 317, '2021-02-14'),
(6476, 327, '2021-02-14'),
(6477, 303, '2021-02-14'),
(6478, 326, '2021-02-14'),
(6479, 341, '2021-02-14'),
(6480, 337, '2021-02-14'),
(6481, 309, '2021-02-14'),
(6482, 333, '2021-02-14'),
(6483, 337, '2021-02-14'),
(6484, 314, '2021-02-14'),
(6485, 95, '2021-02-14'),
(6486, 122, '2021-02-14'),
(6487, 310, '2021-02-14'),
(6488, 95, '2021-02-14'),
(6489, 95, '2021-02-15'),
(6490, 310, '2021-02-15'),
(6491, 333, '2021-02-15'),
(6492, 309, '2021-02-15'),
(6493, 309, '2021-02-15'),
(6494, 306, '2021-02-15'),
(6495, 310, '2021-02-15'),
(6496, 335, '2021-02-15'),
(6497, 333, '2021-02-15'),
(6498, 95, '2021-02-15'),
(6499, 95, '2021-02-15'),
(6500, 306, '2021-02-15'),
(6501, 95, '2021-02-15'),
(6502, 309, '2021-02-15'),
(6503, 306, '2021-02-15');

-- --------------------------------------------------------

--
-- Table structure for table `product_discounts`
--

CREATE TABLE `product_discounts` (
  `id` int(11) NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `discount_id` int(10) UNSIGNED NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_discounts`
--

INSERT INTO `product_discounts` (`id`, `product_id`, `discount_id`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(138, 319, 1, '154', '2020-11-18 18:03:20', '2020-11-18 18:03:20', NULL),
(139, 319, 5, '10', '2020-11-18 18:03:20', '2020-11-18 18:03:20', NULL),
(140, 341, 1, '700', '2020-12-03 16:10:32', '2020-12-03 16:10:32', NULL),
(141, 341, 5, '650', '2020-12-03 16:10:32', '2020-12-03 16:10:32', NULL),
(142, 341, 6, '600', '2020-12-03 16:10:32', '2020-12-03 16:10:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `product_id` int(191) NOT NULL,
  `review` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rating` tinyint(2) NOT NULL,
  `review_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(11) NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `comment_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `product_id` int(192) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `photo`, `title`, `subtitle`, `details`) VALUES
(4, '1557343012img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.'),
(5, '1557343018img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.'),
(6, '1557343024img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `section`) VALUES
(16, 'Manager', 'orders , products , affilate_products , customers , vendors , vendor_subscription_plans , categories , bulk_product_upload , product_discussion , set_coupons , blog , messages , general_settings , home_page_settings , menu_page_settings , emails_settings , payment_settings , social_settings , language_settings , seo_tools , subscribers'),
(17, 'Moderator', 'orders , products , customers , vendors , categories , blog , messages , home_page_settings , payment_settings , social_settings , language_settings , seo_tools , subscribers'),
(18, 'Staff', 'orders , products , vendors , vendor_subscription_plans , categories , blog , home_page_settings , menu_page_settings , language_settings , seo_tools , subscribers');

-- --------------------------------------------------------

--
-- Table structure for table `seotools`
--

CREATE TABLE `seotools` (
  `id` int(10) UNSIGNED NOT NULL,
  `google_analytics` text COLLATE utf8mb4_unicode_ci,
  `meta_keys` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seotools`
--

INSERT INTO `seotools` (`id`, `google_analytics`, `meta_keys`) VALUES
(1, '<script>//Google Analytics Scriptfffffffffffffffffffffffssssfffffs</script>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `user_id`, `title`, `details`, `photo`) VALUES
(2, 0, 'FREE SHIPPING', 'Free Shipping All Order', '1561348133service1.png'),
(3, 0, 'PAYMENT METHOD', 'Secure Payment', '1561348138service2.png'),
(4, 0, '30 DAY RETURNS', '30-Day Return Policy', '1561348143service3.png'),
(5, 0, 'HELP CENTER', '24/7 Support System', '1561348147service4.png'),
(6, 13, 'FREE SHIPPING', 'Free Shipping All Order', '1563247602brand1.png'),
(7, 13, 'PAYMENT METHOD', 'Secure Payment', '1563247614brand2.png'),
(8, 13, '30 DAY RETURNS', '30-Day Return Policy', '1563247620brand3.png'),
(9, 13, 'HELP CENTER', '24/7 Support System', '1563247670brand4.png');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` int(11) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text,
  `subtitle` text,
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `user_id`, `title`, `subtitle`, `price`) VALUES
(1, 0, 'Free Shipping', '(10 - 12 days)', 0),
(2, 0, 'Express Shipping', '(5 - 6 days)', 20);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(191) UNSIGNED NOT NULL,
  `subtitle_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle_size` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_color` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_anime` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `title_size` varchar(50) DEFAULT NULL,
  `title_color` varchar(50) DEFAULT NULL,
  `title_anime` varchar(50) DEFAULT NULL,
  `details_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details_size` varchar(50) DEFAULT NULL,
  `details_color` varchar(50) DEFAULT NULL,
  `details_anime` varchar(50) DEFAULT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `subtitle_text`, `subtitle_size`, `subtitle_color`, `subtitle_anime`, `title_text`, `title_size`, `title_color`, `title_anime`, `details_text`, `details_size`, `details_color`, `details_anime`, `photo`, `position`, `link`) VALUES
(11, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723943406.jpg', 'slide-one', '#'),
(12, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723947402.jpg', 'slide-one', '#'),
(13, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723990903.jpg', 'slide-one', '#'),
(14, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723992904.jpg', 'slide-one', '#'),
(15, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723995205.jpg', 'slide-one', '#'),
(17, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723999301.jpg', 'slide-one', '#');

-- --------------------------------------------------------

--
-- Table structure for table `socialsettings`
--

CREATE TABLE `socialsettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gplus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dribble` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_status` tinyint(4) NOT NULL DEFAULT '1',
  `g_status` tinyint(4) NOT NULL DEFAULT '1',
  `t_status` tinyint(4) NOT NULL DEFAULT '1',
  `l_status` tinyint(4) NOT NULL DEFAULT '1',
  `d_status` tinyint(4) NOT NULL DEFAULT '1',
  `f_check` tinyint(10) DEFAULT NULL,
  `g_check` tinyint(10) DEFAULT NULL,
  `fclient_id` text COLLATE utf8mb4_unicode_ci,
  `fclient_secret` text COLLATE utf8mb4_unicode_ci,
  `fredirect` text COLLATE utf8mb4_unicode_ci,
  `gclient_id` text COLLATE utf8mb4_unicode_ci,
  `gclient_secret` text COLLATE utf8mb4_unicode_ci,
  `gredirect` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socialsettings`
--

INSERT INTO `socialsettings` (`id`, `facebook`, `gplus`, `twitter`, `linkedin`, `dribble`, `f_status`, `g_status`, `t_status`, `l_status`, `d_status`, `f_check`, `g_check`, `fclient_id`, `fclient_secret`, `fredirect`, `gclient_id`, `gclient_secret`, `gredirect`) VALUES
(1, 'https://www.facebook.com/', 'https://plus.google.com/', 'https://twitter.com/', 'https://www.linkedin.com/', 'https://dribbble.com/', 1, 1, 1, 1, 1, 0, 1, '503140663460329', 'f66cd670ec43d14209a2728af26dcc43', 'https://localhost/wireless/auth/facebook/callback', '536622682580-n50patemn5m6knmmvjl9dgqot0qhtf5o.apps.googleusercontent.com', 'LvTUnxm9JnyWwguZCgL_UlRl', 'http://localhost/wireless/auth/google/callback');

-- --------------------------------------------------------

--
-- Table structure for table `social_providers`
--

CREATE TABLE `social_providers` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `provider_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(191) UNSIGNED NOT NULL,
  `category_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` text,
  `is_featured` tinyint(4) DEFAULT '0',
  `is_newest` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `name`, `slug`, `status`, `photo`, `is_featured`, `is_newest`) VALUES
(69, 44, 'iPhone X', 'iphone-x', 1, '1597262283xs-max.png', 1, 1),
(70, 44, 'iPhone XS Max', 'iphone-xs-max', 1, '1597262291xs-max.png', 1, 1),
(71, 44, 'Iphone 11', 'iphone-11', 1, '159726230011.png', 1, 1),
(72, 44, 'Iphone 11 Pro', 'iphone-11-pro', 1, '159726230711-pro.png', 1, 1),
(73, 44, 'IPhone 11 Pro Max', 'iphone-11-pro-max', 1, '159726233911-pro.png', 1, 1),
(74, 45, 'Galaxy S9', 'galaxy-s9', 1, '159726261911.png', 1, 1),
(75, 45, 'Galaxy Note 9', 'galaxy-note-9', 1, '1597323275note-9.png', 1, 1),
(76, 45, 'Galaxy S10', 'galaxy-s10', 1, '1597323340s10.png', 1, 1),
(77, 45, 'Galaxy Note 10', 'galaxy-note-10', 1, '1597323332note-10.png', 1, 1),
(78, 45, 'Galaxy Note 20 Ultra', 'galaxy-note-20-ultra', 1, '1597323215s20-ultra.png', 1, 1),
(79, 48, 'Google Pixel 4A', 'google-pixel-4a', 1, '1597326831Google-Pixel-4a.png', 1, 0),
(80, 48, 'Google Pixel 2 XL', 'google-pixel-2-xl', 1, '1597326765Google-Pixel-2-XL.png', 0, 0),
(81, 48, 'Google Pixel 3a XL', 'google-pixel-3a-xl', 1, '1597326467Google-Pixel-3a-XL.png', 0, 0),
(82, 48, 'Google Pixel 3 XL', 'google-pixel-3-xl', 1, '1597326421Google-Pixel-3-XL.png', 1, 0),
(83, 48, 'Google Pixel 4 XL', 'google-pixel-4-xl', 1, '1597326372Google-Pixel-4-XL.png', 1, 0),
(84, 47, 'Motorola One', 'motorola-one', 1, '1597325070Motorola-One.png', 0, 0),
(85, 47, 'Moto G7 Power', 'moto-g7-power', 1, '1597325046Moto-G7-Power.png', 0, 0),
(86, 47, 'Motorola One Hyper', 'motorola-one-hyper', 1, '1597325010Motorola-One-Hyper.png', 1, 0),
(87, 47, 'Moto G Stylus', 'moto-g-stylus', 1, '1597324959Moto-G-Stylus.png', 1, 0),
(88, 47, 'Motorola One Action', 'motorola-one-action', 1, '1597324942Motorola-One-Action.png', 0, 0),
(89, 46, 'LG V50 ThinQ 5G', 'lg-v50-thinq-5g', 1, '1597324414LG-V50-ThinQ-5G.png', 1, 0),
(90, 46, 'LG G8 ThinQ', 'lg-g8-thinq', 1, '1597324406LG-G8-ThinQ.png', 1, 0),
(91, 46, 'LG G8X ThinQ', 'lg-g8x-thinq', 1, '1597324398LG-G8X-ThinQ.png', 1, 0),
(92, 46, 'LG G7 ThinQ', 'lg-g7-thinq', 1, '1597324390LG-G7-ThinQ.png', 0, 0),
(93, 46, 'LG V30', 'lg-v30', 1, '1597324381lg-v30.png', 0, 0),
(94, 49, 'All', 'all-models', 1, NULL, 0, 0),
(95, 44, 'iPhone 6 Plus', 'iphone-6-plus', 1, '160450643951Ykvmxr8QL._AC_.jpg', 0, 0),
(96, 44, 'iPhone 6s', 'iphone-6s', 1, '16056403181.jpeg', 0, 0),
(97, 44, 'iPhone 6s Plus', 'iphone-6s-plus', 1, '16056429461.jpg', 0, 0),
(98, 44, 'iPhone 7', 'iphone-7', 1, '16056431741.jpg', 0, 0),
(99, 44, 'iPhone 7 Plus', 'iphone-7-plus', 1, '16056441991.jpg', 0, 0),
(100, 44, 'iPhone 8', 'iphone-8', 1, '16056446014.jpg', 0, 0),
(101, 44, 'iPhone 8 Plus', 'iphone-8-plus', 1, '16056452034.jpg', 0, 0),
(102, 44, 'iPhone XR', 'iphone-xr', 1, '16056460214.jpg', 0, 0),
(103, 45, 'Samsung S7 Edge', 'Samsung-S7-edge', 1, '16057132233.jpg', 0, 0),
(104, 45, 'Samsung S8', 'samsung-s8', 1, '16057134612.jpg', 0, 0),
(105, 45, 'Samsung S8 Plus', 'samsung-s8-plus', 1, '16057139102.jpg', 0, 0),
(106, 45, 'Samsung S9 Plus', 'Samsung-S9-Plus', 1, '16057159842.jpg', 0, 0),
(107, 45, 'Samsung S10e', 'Samsung-S10e', 1, '16057166173.jpg', 0, 0),
(108, 45, 'Samsung S10', 'Samsung-S10', 1, '16057214143.jpg', 0, 0),
(109, 45, 'Samsung S10 Plus', 'Samsung-S10-Plus', 1, '16057216603.jpg', 0, 0),
(110, 45, 'Samsung S20', 'Samsung-S20', 1, '16057279963.jpg', 0, 0),
(111, 45, 'Samsung S20 Plus 5G', 'Samsung-S20-Plus-5G', 1, '16057282463.jpg', 0, 0),
(112, 45, 'Samsung S20 Ultra', 'Samsung-S20-Ultra', 1, '16057287253.jpg', 0, 0),
(113, 45, 'Samsung Note 5', 'Samsung-Note-5', 1, '16057291693.jpg', 0, 0),
(114, 45, 'Samsung Note 8', 'Samsung-note-8', 1, '16057294712.jpg', 0, 0),
(115, 45, 'Samsung Note 9', 'Samsung-Note-9', 1, '16057302836.jpg', 0, 0),
(116, 45, 'Samsung Note 10 Plus', 'Samsung-Note-10Plus', 1, '16057315322.jpg', 0, 0),
(117, 45, 'Samsung A01', 'Samsung-A01', 1, '16057327103.jpg', 0, 0),
(118, 45, 'Samsung A10e', 'Samsung-A10e', 1, '16057329973.jpg', 0, 0),
(119, 45, 'Samsung A10s', 'Samsung-A10s', 1, '16057332462.jpg', 0, 0),
(120, 45, 'Samsung A11', 'Samsung-A11', 1, '16057342481.jpg', 0, 0),
(121, 45, 'Samsung A20s', 'Samsung-A20s', 1, '16057348552.jpg', 0, 0),
(122, 45, 'Samsung A21s', 'Samsung-A21s', 1, '16057350101.jpg', 0, 0),
(123, 45, 'Samsung A31', 'Samsung-A31', 1, '16057352362.jpg', 0, 0),
(124, 45, 'Samsung A51', 'Samsung-A51', 1, '16057353973.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(191) NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`) VALUES
(1, 'hellothisis@mymy.com'),
(2, 'vm.lafayette@gmail.com'),
(3, 'turkiyeteknoloji@yahoo.com'),
(4, 'orfibnina7@gmail.com'),
(5, 'testo@gmail.com'),
(6, 'majeed@gmail.com'),
(7, 'watchcenter2001@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `title`, `currency`, `currency_code`, `price`, `days`, `allowed_products`, `details`) VALUES
(5, 'Standard', '$', 'NGN', 60, 45, 25, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>'),
(6, 'Premium', '$', 'USD', 120, 90, 90, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(7, 'Unlimited', '$', 'USD', 250, 365, 0, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_provider` tinyint(10) NOT NULL DEFAULT '0',
  `status` tinyint(10) NOT NULL DEFAULT '0',
  `verification_link` text COLLATE utf8mb4_unicode_ci,
  `email_verified` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `affilate_code` text COLLATE utf8mb4_unicode_ci,
  `affilate_income` double NOT NULL DEFAULT '0',
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `owner_name` text COLLATE utf8mb4_unicode_ci,
  `shop_number` text COLLATE utf8mb4_unicode_ci,
  `shop_address` text COLLATE utf8mb4_unicode_ci,
  `reg_number` text COLLATE utf8mb4_unicode_ci,
  `shop_message` text COLLATE utf8mb4_unicode_ci,
  `shop_details` text COLLATE utf8mb4_unicode_ci,
  `shop_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personal_id` text COLLATE utf8mb4_unicode_ci,
  `tax_id` text COLLATE utf8mb4_unicode_ci,
  `f_url` text COLLATE utf8mb4_unicode_ci,
  `g_url` text COLLATE utf8mb4_unicode_ci,
  `t_url` text COLLATE utf8mb4_unicode_ci,
  `l_url` text COLLATE utf8mb4_unicode_ci,
  `is_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `f_check` tinyint(1) NOT NULL DEFAULT '0',
  `g_check` tinyint(1) NOT NULL DEFAULT '0',
  `t_check` tinyint(1) NOT NULL DEFAULT '0',
  `l_check` tinyint(1) NOT NULL DEFAULT '0',
  `mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `current_balance` double NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  `discount_list` int(10) UNSIGNED DEFAULT NULL,
  `credit_limit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `credit_line` text COLLATE utf8mb4_unicode_ci,
  `credit_usage` int(11) DEFAULT '0',
  `usage_date` date DEFAULT NULL,
  `last_usage_date` date DEFAULT NULL,
  `secure` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nonsecure` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `zip`, `city`, `country`, `address`, `phone`, `fax`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_provider`, `status`, `verification_link`, `email_verified`, `affilate_code`, `affilate_income`, `shop_name`, `owner_name`, `shop_number`, `shop_address`, `reg_number`, `shop_message`, `shop_details`, `shop_image`, `personal_id`, `tax_id`, `f_url`, `g_url`, `t_url`, `l_url`, `is_vendor`, `f_check`, `g_check`, `t_check`, `l_check`, `mail_sent`, `shipping_cost`, `current_balance`, `date`, `ban`, `discount_list`, `credit_limit`, `credit_line`, `credit_usage`, `usage_date`, `last_usage_date`, `secure`, `nonsecure`) VALUES
(13, 'Vendor', '1557677677bouquet_PNG62.png', '1234', 'Washington, DC', 'Algeria', 'Space Needle 400 Broad St, Seattles', '3453453345453411', '23123121', 'vendor@gmail.com', '$2y$10$.4NrvXAeyToa4x07EkFvS.XIUEc/aXGsxe1onkQ.Udms4Sl2W9ZYq', 'kqxYb8q3ZpHpJbjCzBea5rX3wgtdIyCyxEkAlxtcUzKmjXcaGz3j5BoYGPWV', '2018-03-07 18:05:44', '2020-06-17 03:16:38', 0, 2, '$2y$10$oIf1at.0LwscVwaX/8h.WuSwMKEAAsn8EJ.9P7mWzNUFIcEBQs8ry', 'Yes', '$2y$10$oIf1at.0LwscVwaX/8h.WuSwMKEAAsn8EJ.9P7mWzNUFIcEBQs8rysdfsdfds', 5000, 'Test Stores', 'User', '43543534', 'Space Needle 400 Broad St, Seattles', 'asdasd', 'sdf', '<br>', '1591356383beautyworld-vendor-banner.jpg', '1591358775iphone7.png', '1591358775featured_3.jpg', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 4978.02, '2019-11-24', 0, 5, '2000', '30', 0, NULL, NULL, '47,48', ''),
(22, 'Users', NULL, '1231', 'Test City', 'United States', 'Test Address', '34534534534', '34534534534', 'user@gmail.com', '$2y$10$.4NrvXAeyToa4x07EkFvS.XIUEc/aXGsxe1onkQ.Udms4Sl2W9ZYq', 'pktEyMBKOBTjaURr9LVq7V6Evyrl2YZNyCE3e3xE2TtfWq29rkt46mZV9J6z', '2019-06-20 12:26:24', '2020-05-28 08:39:33', 0, 0, '1edae93935fba69d9542192fb854a80a', 'Yes', '8f09b9691613ecb8c3f7e36e34b97b80', 4963.6900000000005, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(27, 'khan', NULL, '23456', 'karachi', 'Pakistan', 'Space Needle 400 Broad St, Seattles', '34534534', NULL, 'shahrukh@gmail.com', '$2y$12$iANezsvkPhCmK22ENhU8XeC93ApgkorryUHoNUxkdIIKvj8O7Noum', 'oCYVkijy6vGAYJpYYAg1wxEoYaRlcIeDM2bOEciS2PZjMMIYGCZ8Dx5DlJMd', '2019-10-05 04:15:08', '2020-06-20 10:36:15', 0, 0, '0521bba4c819528b6a18a581a5842f17', 'Yes', 'bb9d23401cd70f11998fe36ea7677797', 0, 'Test Store', 'User', '43543534', 'Space Needle 400 Broad St, Seattles', 'asdasd', 'ds', 'dfhgdfgd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2019-11-24', 0, 1, '1500', '10', 0, NULL, NULL, '47,49,52', ''),
(28, 'User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'junnun@gmail.com', '$2y$10$YDfElg7O3K6eQK5enu.TBOyo.8TIr6Ynf9hFQ8dsIDeWAfmmg6hA.', 'vHXrRc3RAqweqyTlHqTF08P0tV3cE4PxS4Appj1m9xgv2FWTVvJqZItcLFFv', '2019-10-13 05:39:13', '2019-10-13 05:39:13', 0, 0, '8036978c6d71501e893ba7d3f3ecc15d', 'Yes', '33899bafa30292165430cb90b545728a', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(29, 'john doe', '1591772541834-8345234_happy-customer-clipart-png-student-cartoon-gif-png.png', NULL, NULL, NULL, 'asdfsdfs', '8328311884', NULL, 'devmrmsoft@gmail.com', '$2y$10$p35S2FczpEfpbe41CX4j4.XE548tHBtF5weGLPxZ56MX5dsOFtaCC', 'gG16lGioAyehAPjm01ey1i3Hrh9AdeOfWfF6oo8PLVjgYqGMNoWn14p8zOAQ', '2020-05-15 21:52:47', '2020-06-10 02:02:21', 0, 0, 'da3f643a31172039ee73c06186414eaa', 'Yes', '58a1689a0482240c973ef347382be897', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(30, 'Shahrukh Khan', NULL, '61602', 'Peoria', 'US', '3982  Coburn Hollow Road', '03453305509', NULL, 'shahrukh.khan7991@gmail.com', '$2y$12$c73PMgdzEX3krfjSfeTM0um33Lad49lbXUTcuHQu563jGOnhZPSEK', '0dyfDnrgB6WoHTmDMuezQ2WJR0FTi9xW7I1w3b90JrqdbP5SriA569cNVpFm', '2020-06-16 06:11:58', '2020-06-16 06:11:58', 0, 0, '51027db67f7fe0ed2ea9fdb8c8f0bf6e', 'Yes', 'a8a2c7858303dc7682a47c721ab7d50d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(31, 'shayan haider', NULL, '78701', 'austin', '12', '7800 Harwin Drive, Suit A4 Houston, TX 7703', '8328311884', NULL, 'shayanhaider666@gmail.com', '$2y$10$p35S2FczpEfpbe41CX4j4.XE548tHBtF5weGLPxZ56MX5dsOFtaCC', '2bxMzYY8j6pRPSp6YUve33BHLEpBk8lVdwCIIKzvSuVcyYb20sKa7fHGjhRQ', '2020-06-20 10:16:59', '2020-10-30 23:23:13', 0, 0, '015e44d4996124a340edbefa3a8a2ae4', 'Yes', '39ff94f3cec2d8c5ad4c9fc948db0c29', 0, 'shayan horizon wireless', 'owner', '090078601', '7800 Harwin Drive, Suit A4 Houston, TX 7703', NULL, NULL, '<br>', NULL, '159266621901.png', '159266621914.png', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '1400', '20', 0, NULL, NULL, '47,49,50', ''),
(32, 'Talha Vaid', NULL, NULL, NULL, NULL, '7250 Harwin Dr Ste K', '8326144581', NULL, 'talhavaid.ae@gmail.com', '$2y$10$rZ4S5OL0AsKBJnrVKH1/OO29bFur3XYb3cWCkbM215G7UCsq1yYMy', NULL, '2020-08-23 04:29:35', '2020-08-23 04:29:35', 0, 0, '64fe0bccc363b4126421a09411aa1406', 'No', 'f04e38216c6ff3edee24b72344dda72e', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(33, 'richard strubin', NULL, NULL, NULL, NULL, '160 arrowhead rd cadiz,ky', '2709240821', NULL, 'richardstrubin@mediacombb.net', '$2y$10$2xAZ.5lAhaHHSA8QbgFIQuYoEB3F4LU/125D0EACuPLNsbL/cjVWy', NULL, '2020-08-24 22:01:35', '2020-08-24 22:01:35', 0, 0, 'e5998b07e1bc82b368f4cec5ce3aa5d2', 'No', 'c25ec34b9c482bd40da02b0fddc9105a', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(34, 'habeeb ahmed', NULL, NULL, NULL, NULL, '233 BUCK CREEK ROAD.  APT 102', '5022990980', NULL, 'indiausa517@gmail.com', '$2y$10$WUXp7Exk2Ji3Mc041N7ArOYrx1/jyNFBaJldmtbev01W2WDuXJ7XK', NULL, '2020-08-31 06:49:44', '2020-09-03 03:35:39', 0, 0, 'f5f337973c37d96be0bbd8bf75d90fe5', 'No', 'ac190616d1e1f9bb03d8aba1b3172157', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(35, 'habeeb ahmed', NULL, NULL, NULL, NULL, '233 BUCK CREEK ROAD.  APT 102', '5022990980', NULL, 'indiausa02@gmail.com', '$2y$10$Zvrio4iju8Z7KeIQN07TZubbPD5zVz3tjCFC8vUr0Pwdd6mM2JXca', NULL, '2020-08-31 07:00:55', '2020-08-31 07:00:55', 0, 0, '012db56d26fc5e1b2eaf9332d2d1d43e', 'No', '5e46dd35093f091c551327b75aae8347', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(36, 'Fawaz Qazi', NULL, NULL, NULL, NULL, '14317 Walters Road #101', '8323751104', NULL, 'unlimitedprepaidwireless@gmail.com', '$2y$10$9yz6uGv9F0tz0WDVqc54euBvsKTov7vBOn1lndQy7e608dxbVmBGG', NULL, '2020-09-02 05:30:39', '2020-09-02 05:35:50', 0, 0, 'b955a8af1d11f2be2bd28d048a8c8aa6', 'No', '9371e0cd10b22ca7cd4d3382e6bc1faf', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(37, 'Damon P Coleman', NULL, NULL, NULL, NULL, '11811 North Freeway ste 508', '8322306817', NULL, 'Yourchoicewireless2@gmail.com', '$2y$10$sGOTnL7c9jL.H98CTvpK0O/gAwokvMYHwKgmWff.yzvNFCNekMt0C', NULL, '2020-09-07 02:19:15', '2020-09-07 02:19:15', 0, 0, 'd97527086412ec23458158615b545daa', 'No', '084c36c30a1945afa5ce5e54745bcb85', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(38, 'amsallem thierry', NULL, NULL, NULL, NULL, '99 rue de courcelles 75017 paris', '+33612040404', NULL, 'vm.lafayette@gmail.com', '$2y$10$BDB3Nj91W4H3Jw7LezncFePJdRwEjrFK.vGAHkEkFOKSqksoDRvlW', NULL, '2020-09-10 20:24:10', '2020-09-10 20:24:10', 0, 0, 'fc389527dee4189a575c5fbec45a2158', 'No', '24bd9e46989d5bcec16dab4bf8377335', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(39, 'amsallem thierry', NULL, NULL, NULL, NULL, '99 rue de courcelles 75017 paris', '+33612040404', NULL, 'thierry@mobileone.fr', '$2y$10$GrMrCw84bcHXvxbTmYc4gOa3BBaP.LNHx/QTMDlSde82n4Zy96.NG', NULL, '2020-09-10 20:30:06', '2020-09-10 20:30:06', 0, 0, 'ea310a1dce77625db62014665ab5c51d', 'No', '43e8d1b5e04e72eb777f6e42d4ea040f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(40, 'robert pineda', NULL, NULL, NULL, NULL, '1221 n tustin st', '5628107628', NULL, 'rpwireless2012@yahoo.com', '$2y$10$m73xbFj8492gXRJXkxSK6usOL4kqBHQYF/DyAfg/qbaiXr69RQ6.i', NULL, '2020-09-16 21:26:25', '2020-09-16 21:26:25', 0, 0, '7ed889327976fc2340e9638bb690e735', 'No', '2d6fc3c5ca61f37639ee38129d6d0d73', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(41, 'Don Richard', NULL, NULL, NULL, NULL, '1755 Georgian Dr', '7736093210', NULL, 'admin@my1solution.com', '$2y$10$ferFRIqQLCS1Gx56Ym8HU.pEgUi961a/Q3DFNUqOE4W9dGdI6Xppy', NULL, '2020-09-17 20:18:30', '2020-09-17 20:18:30', 0, 0, 'b1c9edcec5e77e99fa5adccee381e7b7', 'No', 'bc1ffdda7476a43d39edf05a7aa673a5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(42, 'valentin bautista', NULL, NULL, NULL, NULL, '1104 nw park st', '8632615835', NULL, 'elprimowireless@yahoo.com', '$2y$10$jA.na.sLrTvw85oOkYbD8eWWUF/4bUyvPZiOpcA6U9qu3uwjKIDoK', 'XZSjZ1AXeDFDvCclbUwxuPpp7ZrVt4UjVMOKiO3tskziLNq8g54a2c6LXAfp', '2020-09-18 23:12:46', '2020-09-18 23:12:46', 0, 0, 'd94a1e7967a6f2178e33756728e443d1', 'No', 'c5908ef05b0433e0839c59a0fb76aa9f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(43, 'Fabian Carpio', NULL, NULL, NULL, NULL, '4555 E Charleston Blvd., Suite 107', '7024329500', NULL, 'Fabiancarpio@aol.com', '$2y$10$CfHPKj7ZOT7nrQ/k/AmfseCbVG5WkfGSkIzVjOSaXt4HPsKphFSd6', '5Tqlmfk2ksq0b7fGgYUxVAXoMqzE3O9c8kOi5wVFttd7uelO1FWDU1fqldK4', '2020-09-23 00:00:50', '2020-09-25 00:05:05', 0, 0, '64c41e9d4b149eaffe9e49b11368ee9b', 'No', 'f585074e7b3baf2e18d2af5ebac1faf6', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(44, 'Bashirat Kareem', NULL, NULL, NULL, NULL, '11735 S GLEN DR APT 1309', '9175933547', NULL, 'bashonweb@yahoo.com', '$2y$10$TX.1mjfNyXekY.4isPINReWD07.QlmeYoEXQdsRvtuqc2lRJpafEK', 'zW9lxt94scCbZH0q4B3kGe323lJY1XoPjMHYMF6rJwgrNpfCMrwaex7EbXL0', '2020-09-25 06:13:00', '2020-09-25 06:13:00', 0, 0, '38011f7f5bf7f9759c1b23ed81ded419', 'No', 'c13dd37ce9b24343723f412f403da3d5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(45, 'Ngoc-noc nguyen', NULL, NULL, NULL, NULL, '1156 park oak ct  milpitas ca   95035', '4086806306', NULL, 'ezgoyr2@gmail.com', '$2y$10$7r6aFDmOXYzIhtO42aN0P.m.SWfxkpuV6G2OVc3hyiUC0KXSEF9nW', NULL, '2020-09-27 01:04:48', '2020-09-27 01:22:13', 0, 0, 'bcdd43a70c438b38cb4de41535887c32', 'No', '39167d108f2e615b0181baf57945989c', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(46, 'Ngoc-nga Nguyen', NULL, NULL, NULL, NULL, '1156 0ak park ct  Milpitas ca 95035', '4086806306', NULL, 'ejneedruru@gmail.com', '$2y$10$MvFMm2tsPRaMtM5Up7N0BulDf1NEp6gaVZrgiOJ462lgNup0HLRie', NULL, '2020-09-27 01:33:45', '2020-09-27 01:33:45', 0, 0, '8e124a9d250a939dc1ef3f996a1beaab', 'No', '0adabf6043327c965e1d6a3c47613d57', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(47, 'Carlos Cisneros', NULL, NULL, NULL, NULL, '104 east calton RD suite 103', '9565161111', NULL, 'ccisneros1997@gmail.com', '$2y$10$SxxuXZ/Il5mphIW1wvy7PeaGBTjUnvt6.9pfa52Y2OOtuIdanTBjm', NULL, '2020-09-29 20:19:27', '2020-09-29 20:19:27', 0, 0, '3d49c9291e5579e1229aaa0bca27cf8a', 'No', 'd24808d5dddc9b509042135ae3752758', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(48, 'Ken', NULL, NULL, NULL, NULL, '28219 Red Raven Rd', '2163893555', NULL, 'kingber41@aol.com', '$2y$10$NhisdYk9IU4oSVymZq5rV.kXgGFbY1l40itKO8CHko6HSobh01zsm', 'zI7OgByLxPvj4055CaQ5zdi7ypz9DmzdNriomoGIvtLQ7DPba6Mftv8NAejn', '2020-09-29 21:27:02', '2020-09-29 21:27:02', 0, 0, '601f01500686f82327b79edab033d98b', 'No', '1ccff936c3fc8b283fd11dd40e4c027f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(49, 'Zohaib Malik', NULL, '33612', 'TAMPA', 'United States', '2159k university square mall', '8134011165', NULL, 'simplemobile786@gmail.com', '$2y$10$UiQTTPDvHUsvL2k8/k59T.R22F1PVml7aUfSDHXv3ljlPe2cwZDOa', NULL, '2020-09-30 21:52:28', '2020-10-07 21:28:31', 0, 0, 'f23281ad941b3859449e8e97819a5ca3', 'No', '43efb59303a763c2910d7187945c16f5', 0, 'Totalwireless Inc', NULL, '8139771657', 'Www.mytotalwireless.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(50, 'Andrew Saucedo', NULL, NULL, NULL, NULL, '5551 S Monitor Ave', '7089275235', NULL, 'andysaucedo@comcast.net', '$2y$10$UAMDYP6s6fj0NACoA2dkw.sufLyl3o.72Ee9CTPufgmsnFg8RQwUy', NULL, '2020-10-06 21:15:21', '2020-10-06 21:15:21', 0, 0, 'bfd82b7e3909e74162ea879395b41531', 'No', 'e65b046dd30d506a725c5b8027aac254', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(51, 'roger johnson', NULL, NULL, NULL, NULL, '640 center ave #109 martinez,94553', '4088899662', NULL, 'noneedruru@gmail.com', '$2y$10$tM78no0D/OpcVje42M/aauKy3TXQx3TQqy0FU/Ba0L0o.pMz5vL8C', NULL, '2020-10-07 17:07:23', '2020-10-07 17:07:23', 0, 0, '46e5a1956ac2c3c91d2ab23472499fc6', 'No', '1b7124acb0890aa48f41120b9dba43ad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(52, 'Cristian Avilés', NULL, NULL, NULL, NULL, 'Urb Hacienda Boriquen calle Mabo D5, Toa Alta PR 00953', '7872287682', NULL, 'tresuelvo1@gmail.com', '$2y$10$IYFCniX5YVj/k0oS5GdXHud/uw7eod37eIcPteCCaOm126d9uErrO', 'POtoPHmDpRftg88MUMIgcK0urm3LgPdAGPgqRfITmvvAudvKxBF9v6YjSMvm', '2020-10-08 07:03:37', '2020-10-22 07:07:41', 0, 0, 'db2cdc095e73a0d1bf2d738dedc64a81', 'No', '060335b92164f4e6c11f1d3ff4924bf5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(53, 'wylder martinez', NULL, NULL, NULL, NULL, '1580 college view dr, Apartment 3', '2134351655', NULL, 'wyldermartinez@gmail.com', '$2y$10$d2WT5gUgUlLICJXCjnYo3uvl61S0A8jfITY5kh8dwOAyELYkKCrLa', 'ufNQAQRxrNJgRYnjU6BeQadseLdR2sDqZzHxsuXi0acNA2CNf5nyiOd5ScnK', '2020-10-10 04:10:06', '2020-10-10 04:10:06', 0, 0, 'c33cd43f942a46913da25d62abe56102', 'No', '17b188f1bc42b1a2aa554578f4298a48', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(54, 'speedy mobil tek', NULL, NULL, NULL, NULL, '10200 hawthorne blvd', '3104916052', NULL, 'speedytek0824@gmail.com', '$2y$10$KmXmzdy4kWQPz5mJhCoSduubVJM5kw64L2u5YNV5KGWhJqAu51L2O', NULL, '2020-10-11 03:24:50', '2020-10-11 03:24:50', 0, 0, 'd61fd56bbee0b930cfd58ad98065c01e', 'No', 'e6e5bc3d4ff5dcf7a6030a0df01ff045', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(55, 'Iqbal Hossain', NULL, '06611', 'Trumbull', 'United States', '5065 Main Street #193', '2033720060', NULL, 'hossainp24@yahoo.com', '$2y$10$YjNBd27Dt6twHAynl8yRx.CdHZL2k.PXv69MW7wDlevqcgaAxLUgG', 'eyhuuYI20FHB3FvpWj4TfoFA3pFpJgdlzqrt3couLq09UzpnVKiwQuVkOJlk', '2020-10-12 21:29:02', '2020-10-12 21:29:02', 0, 0, '052603025f9ed710389dd86805256223', 'No', '3050d1583f475b45e4266ebae8dc0e52', 0, 'cell phone world', NULL, '2033720060', '(cell phone world) 5065 main st', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(58, 'shahrukh khan', NULL, '27591', 'Wendell', 'United States', '3204  Dola Mine Road', '321456987', NULL, 'vforce2008@gmail.com', '$2y$10$aL7vMmg2T1vGw5hKhQBKdOVdOhagKASpY3ZNDdwF/153EobUQ.45i', '7gQKuGVUQGRj7ecPEtoqORUYyWrWN1IYdQ0H0CDLqsBjOB5WNrUJg4XWq6UT', '2020-10-14 23:05:36', '2020-10-14 23:19:03', 0, 0, '8b7dece3362b894a3cfeccd8b80936ce', 'Yes', '0facd6947d46441db60efb38381a34c8', 0, 'dev', NULL, '1234567989', '3204  Dola Mine Road', NULL, NULL, NULL, NULL, '1602698736Bangamary .png', '1602698736banner.png', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2020-11-13', 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(59, 'nazmul islam', NULL, NULL, NULL, NULL, '148 west Plumtree ln apt#24k Midvale ut 84047.', '5188198684', NULL, 'sixbrosenterpriseinc@gmail.com', '$2y$10$px.bkVQLj/oWw9wyFX3ZNeYj45qrVvexCeHvKvevMsa62YjoTYTWi', 'KZbOihSydWKWvGul1fOOSeJRnsucMn81Jxr9krYpY0kRglUymhI7J6vcSsmn', '2020-10-16 12:00:32', '2020-10-16 12:07:09', 0, 0, 'e10b7125cec85a7241cef6f026bed74a', 'No', '22b2ab4c94340c63c85b782cf53c5094', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(60, 'nazmul', NULL, NULL, NULL, NULL, '148 west Plumtree ln apt 24k,midvale ut 84047.', '5188198684', NULL, 'nazmul1989r@gmail.com', '$2y$10$Nix4iWWQGrWHkipY925BH.2QomSbYkQ9kUyJ3gZL92kPKn3zVXFte', NULL, '2020-10-16 12:12:17', '2020-10-16 12:12:17', 0, 0, '40c5498ac3e1e5a8ef46fb553e3c4545', 'No', 'c6bac6d46da68f0ef5ad68f1d893448e', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(61, 'sami baradie', NULL, NULL, NULL, NULL, '1359 fulton st', '7188579161', NULL, 'asphotocell@optimum.net', '$2y$10$OMMfehZ.4W9hImAKejc6AeYh/1YNKzPnjXJn7n7TvP.q/u2KkKduS', NULL, '2020-10-16 20:20:32', '2020-10-27 21:10:29', 0, 0, '18ff79390f479cf98413863275bf52f4', 'No', '85df2e6d2473c2dd9fbda94e8494cccc', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(62, 'Federico Tarragona', NULL, NULL, NULL, NULL, 'C/ GREGORIO ORDOÑEZ N° 12', '+34961412695', NULL, 'federico@cirkuitplanet.com', '$2y$10$OdeFcorjFOchAdb1Q3jirO4hmCm4xSK7skZGkFeJzGrNk6y.eifZa', NULL, '2020-10-19 17:55:11', '2020-10-19 17:55:11', 0, 0, 'c16b5cffb97e7615ab474049742eed94', 'No', 'bf16fe87dbfd63c706a2e52ba5c11d1d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(63, 'Kwan Park', NULL, NULL, NULL, NULL, '23830 HIGHWAY 99 STE 201', '4256974886', NULL, 'kpark@intercomws.com', '$2y$10$4uhWTMv7hbaMMude7PmeWe5td3DbFcAmx57kup1yuWoG4FNKxKbzq', NULL, '2020-10-21 00:45:49', '2020-10-21 00:47:23', 0, 0, '46023001ecbdf611582a826ff9fa1901', 'No', '7fac49cd4a581b52c97e8f76b8123af7', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(64, 'Kwan Park', NULL, NULL, NULL, NULL, '23830 HIGHWAY 99 STE 201', '4256974886', NULL, 'office@intercomws.com', '$2y$10$0LqDrGevCg7vpHBMfsHbfOMTM1Yl.Hzrsr7vsYtRHShmJc7kld8dO', NULL, '2020-10-21 00:46:18', '2020-10-21 00:46:18', 0, 0, 'd413a6f3bc8e88e79624b9e934f5f4be', 'No', 'a92721277a3a264ad12146ea987768e3', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(65, 'Federico Tarragona', NULL, NULL, NULL, NULL, 'C/ GREGORIO ORDOÑEZ N° 12', '961412695', NULL, 'federico@framlogistics.com', '$2y$10$3mzDmZ7HtohCPQyGB0LPren.oByAuTOGEedvqnS.fzMgGGGrfe7Qm', NULL, '2020-10-21 14:07:38', '2020-10-21 14:07:38', 0, 0, 'cd502bdae27d22f04a4a3477a908c194', 'No', '26c3e210426ef6e8c2e1aac68a363787', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(66, 'Faycal Bendedouch', NULL, NULL, NULL, NULL, '9090 Av du Parc, #102, Montréal, QC H2N 1Y8', '438-881-4955', NULL, 'liq.electro@gmail.com', '$2y$10$evVOtBf66ebKr/H2B0PEzeL4VXuBstQoxLk85tBiVfGd6izOOpIQW', NULL, '2020-10-22 01:14:12', '2020-10-22 01:14:12', 0, 0, '05aea19c3000cf576e804cb89d17e079', 'No', '1621581dba54e857bee24783155e5deb', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(67, 'Mahdi', NULL, NULL, NULL, NULL, '2940 pillsbury ave s suite minneapolis mn 55408', '6123564502', NULL, 'aliomahdi@gmail.com', '$2y$10$77zCfFRNHiFqtQ08wyzHyu/nyE9QWqRK9kFG0PkZdApIthx399WSu', 'cJ9qOmFO8cKo2D8p6EQ01km406SHGNlalcl7RufPgVDjhJskAjPgnFTNdkcp', '2020-10-22 15:19:25', '2020-10-22 15:42:45', 0, 0, '94909bc41a5cc9559742494b0f1c0490', 'No', 'c3d19a649a2b17c3f66f77ad6f96ce6f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(68, 'Galaxia Esquivel', NULL, NULL, NULL, NULL, '5913 Glenmont Drive, Apt:420', '3463898683', NULL, 'galaxiaesquivel1@gmail.com', '$2y$10$LgNAXadfyBDlFplrfiEZeerVqxo9h3YP55sj9/gXP2bCbup/.kdmC', NULL, '2020-10-29 03:46:59', '2020-10-29 03:46:59', 0, 0, 'c43605122d7837e98417c99b33735fc0', 'No', '0e30c3b45452ae9903b13febffab2eb0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(69, 'Galaxia Esquivel', NULL, NULL, NULL, NULL, '5913 Glenmont Drive, Apt:420', '3463898683', NULL, 'galaxyesquivel1@gmail.com', '$2y$10$RwJ2p/7Lh8GtOoggchzok.4LsqkdoJ9Bmws1XFk2h0eWKH2gVjG4m', '5cmHIEyToA00EgTDCPn5SKIp1qe7MSArsU7RyL04baVgTDLR18sQRvYJtIqN', '2020-10-29 03:47:47', '2020-10-29 03:47:47', 0, 0, 'd7504da379eb450bb4c17c7fd7a434a8', 'No', '3b70048a72cbc148e19a688f01fa131d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(70, 'Gilberto Jimenez', NULL, '01843', 'Lawrence', 'United States', '191 S Broadway', '9786012883', NULL, 'jqcommunication@gmail.com', '$2y$10$/GenoL.DV74sF9TAqFQ0tOwDzhWY.CoKNJ9tF7PwCvac/qgY7f7F.', NULL, '2020-11-02 08:21:58', '2020-11-02 08:21:58', 0, 0, '19e863cae53db58b1992f1557ea0c94b', 'No', '92c7d82a33ff5bbafeed18662b256e8f', 0, 'JQ Communication', NULL, '9782080604', '191 s Broadway', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(71, 'richard a scholl', NULL, NULL, NULL, NULL, '111 w bay shore rd , bay shore NY 11706', '801 893 1876', NULL, 'mylife007821@gmail.com', '$2y$10$hMvQ.uuRDDv34HHdk5qwOuCkRDwHhD6nvgyDoZ9K9Nc9ytNKKzSQu', NULL, '2020-11-10 03:22:16', '2020-11-10 03:22:16', 0, 0, 'dbae4c70a015a0fc83ce1cb32b0507fa', 'No', '773a07e17fe4006ca9ca0244357c2aba', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(72, 'Raan Syd', NULL, NULL, NULL, NULL, 'Spring field mall', '7033986537', NULL, 'raansyd86@gmail.com', '$2y$10$IHz1VE1Dz3TvyfdzK5niduwNon9A8rE6V07mbNyH3PxSDgLmVdnv2', NULL, '2020-11-14 09:10:30', '2020-11-14 09:10:30', 0, 0, 'e8bf20372766daeff1736980bba5b3e6', 'No', 'cd164f7ce57de79b27ada6ac4f11353d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(73, 'Sandy Stephens', NULL, NULL, NULL, NULL, 'Po Box 369, Whitley City, KY  42653', '6063765602', NULL, 'sandy@stephensprop.com', '$2y$10$w7LmhtMEGJ4BvVUKnjAIheHHutuYO2vqiB2o6724Q1QpuBHMAyC6.', NULL, '2020-11-17 03:31:00', '2020-11-17 03:31:00', 0, 0, '24a013b42df72ab44e68ea88050b5698', 'No', '4e580ec875110795afa75736d68beef2', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(74, 'Shayan Haider', NULL, NULL, NULL, NULL, 'Near Quba Masjid House NO# E/78 in Defence View Phase 2', '03174711209', NULL, 'shayanhaider333@gmail.com', '$2y$10$Z.JU.G3XuCeWzk0gJNqKiOmc7MsF.7ToeNVIxHgx5fAZzSBuFiig2', NULL, '2020-11-18 23:15:12', '2020-11-18 23:15:12', 0, 0, 'a2454102c44919e8a7a0dc1fc8ac515f', 'No', 'b15a3f6f2696551ae2b7e13e81eed53a', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(75, 'Ammad', NULL, NULL, NULL, NULL, 'lahore', '03138486712', NULL, 'sajjadkhan2386@gmail.com', '$2y$10$2bQYpAKwpOSc6e/GnThBOuecVoklsAWIb6HXhlX0TZrcjmZF.ede2', 'v9wtcJT1YZkslcE1Mo6QkbnnS8QlgsY5e6EKwIfekHI1UVN5kkdRE9fS5jTP', '2020-11-19 08:00:53', '2020-11-19 08:00:53', 0, 0, '5b2c6aa433382823b08d35e8734c8abd', 'No', 'bbffc6162a113088bf87da0f8dbbee92', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(76, 'Ubanto', NULL, NULL, NULL, NULL, 'Taxes', '123456789012', NULL, 'ubanto@gmail.com', '$2y$10$R13ezslbsrT0fYmJrUuVd.8fs.aoEr1MbXJpG2Yv5Y3M6khcOptMC', 'XnJD28ymbBwfBp98yYUZ42yiROyrrD0rqAQIxgobHnYEcU4DAFJPcAzH8ulG', '2020-11-25 13:30:42', '2020-11-25 13:30:42', 0, 0, 'e1f8138bfbd98a9ac9a547b4325b4c19', 'No', 'dc6d2c1c63b2999b07d8eee5177579a5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(78, 'Oladipo Olayanju', NULL, '60626-1223', 'Chicago', 'United States', '1546 W Jonquil Ter Apt 5', '3124203967', NULL, 'drdipson@gmail.com', '$2y$10$Pk1vlrYzMbVRm4A78drchegb9cfMo4UI6g1CFuXZBzTTz.TtRTnju', 'Xs2gKCDeJVzoDsqOjuu2YEqxG72SiYni3lUXllayztHPSapDtf0nikEkxRBq', '2020-11-26 08:08:46', '2020-11-26 08:08:46', 0, 0, '11718ae29c5ab0bd770bab043bc68ecc', 'No', 'ad387f8c1de0fce53013d68fc5d3908c', 0, 'Afridext Integrated Services', NULL, '6927527', '501 Silverside Rd #345 Wilmigton,  DE, USA', NULL, NULL, NULL, NULL, NULL, '160635652616063561721295068300516284543660.jpg', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(81, 'Margaret Vega', NULL, NULL, NULL, NULL, 'Beatae magnam minim', '+1 (877) 465-6216', NULL, 'pumyq@mailinator.com', '$2y$10$Dey1QKJTUuVgUvflwYgLF.iF6/K0dsWOKNcOWaI7517l2FK8yU7lO', NULL, '2020-11-26 19:22:49', '2020-11-26 19:22:49', 0, 0, 'f204043f76cd5398ee2213d62c621933', 'No', '0947d2a6d4c882e107583889fb43ed53', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(82, 'Ivana Frye', NULL, NULL, NULL, NULL, 'Quia sint laborum N', '+1 (623) 417-1072', NULL, 'liqiw@mailinator.com', '$2y$10$0Mb7L.3Qouh7VInayKgGdu2MpPCrTFJxzNLgWOi3xNxPCXjIcIVb.', NULL, '2020-11-26 19:55:31', '2020-11-26 19:55:31', 0, 0, '10eb304f6796a02ee6599efbf7c94c80', 'No', 'dd4b0fdad7c77110d96b74ce11e0635f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(99, 'usama', NULL, NULL, NULL, NULL, 'Quis dolore ut eos', '7491067579', NULL, 'usama.farooq91@outlook.com', '$2y$10$1C7A.Em8dAUPxw6qZOVgu.PvhL9CvKJXNdYG5.QzFcOAa8RK64ATi', NULL, '2020-11-27 16:20:23', '2020-11-27 16:20:23', 0, 0, 'e99065b5f69c486c9a7fb567b6785088', 'No', '4822813d61822d6870bb7b0d6f243695', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(104, 'admin', NULL, NULL, NULL, NULL, 'Quis dolore ut eos', '7491067579', NULL, 'admin@gmail.com', '$2y$10$CATbNl0QjHJwoQZbakFmdu/a3JkAF8mk7x0d6sXfv5zAJ3BftdfbK', 'mgJmvqNJt7u35LNTL7xBCgboGLYedjsk3gxeI7MgoPzu8n2HdE16gzZjQ84y', '2020-11-27 21:48:19', '2020-11-27 21:48:19', 0, 0, '1723d5e11f86032d9b1f83cd60a9d326', 'No', 'a77c281efef36c5b8e7dbd6df6ca827f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(106, 'haresh chatlani', NULL, '39209', 'JACKSON', 'United States', '2596 ROBINSON STREET', '6013558187', NULL, 'hash27@aol.com', '$2y$10$0dhgtGFSvaKewUtss912x.4a5rz9QpMtbt2gTm3NUCOgmE.2KMYIK', NULL, '2020-11-28 05:25:21', '2020-11-28 05:55:50', 0, 0, 'ce1dea3b2e04f0ee5661d1e6815f1f44', 'Yes', '453d15fb8afc48a1bde55729d65dbdb7', 0, 'in style wireless', NULL, '6013558187', 'SUITE 39', NULL, NULL, NULL, NULL, '1606519521DRIVERSLICENSE.pdf', '1606519521SALESTAX.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(108, 'Ahmed', NULL, NULL, NULL, NULL, 'lyari', '03152475697', NULL, 'mahmedsaleem60@gmail.com', '$2y$10$LJQ1Zx.xN5O5wfV1S.6W..qmfBx158D7U5ZbntbqaO06T5jwm0nYC', 'xc6j2ci6vcihxRCKX8IYpmbcAFE0ZtX4XouTogurVJ9TdrbXFPKOy1i3JHqH', '2020-11-30 14:04:13', '2020-12-07 17:05:37', 0, 0, '20dfac4f5e33d9860542eab09ac102e9', 'Yes', '10e8e4cac1d7b18b50d7d5be7627d757', 0, 'Ubantu', 'Linux', '7756644', 'Hardive  ST. West', '545336', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2021-01-06', 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(110, 'Jeremiah Phillips', NULL, NULL, NULL, NULL, '130 STEPPING STONE LANE', '2293435257', NULL, 'pluggdn.biz@gmail.com', '$2y$10$pD/V3.DxTe8T1Df3GQrC8.naijyEMGyulEhtOOV0W6dB2VKb49V12', 'zT3jEfmHmZQb11EZ3qhdzX69ieCQX13GZGGCckQeYZutXuPjXb48t1L0w9Dy', '2020-12-03 14:03:32', '2020-12-11 07:23:03', 0, 0, '4ba1ce7b8dd3efd1136a3bde4b887a74', 'Yes', 'b16b43832f9324465a2a207a37d05b35', 0, 'Plugged-N', 'Jeremiah Phillips', '229-343-5257', '130 Stepping Stone Ln Albany, Ga 317211', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2021-01-10', 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(111, 'Murtaza Zaheer', NULL, '77001', 'houston', 'United States', '123 street', '326523652', NULL, 'murtazazaheer@gmail.com', '$2y$10$5Ni1s.JBPNf1on.mQRCvOejzztK4YatfRFpVYJqkw8I/3xYIjot/W', 'jjRKrG4GFVGBMI6iYRtpyBbskcJAXVqUXALNhyC84puKJzDAmgHMnEM0MeNT', '2020-12-03 19:17:19', '2020-12-03 19:30:17', 0, 0, '8f761a4c9d37365dc97df53c1966111e', 'Yes', '2f7a0a100fd1a4cf5dfb7ee278959392', 0, 'logozel', 'Murtaza', 'logozel', '123 street', NULL, NULL, '<br>', NULL, '1607001439120911655_160554459069236_2817189788991613115_n.jpg', '1607001439121164195_160559945735354_7518363871340081159_n.jpg', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 5, '5000', '30', 0, NULL, NULL, '', ''),
(112, 'Eusebio Figueredo', NULL, '70816', 'Baton Rouge', 'United States', '13315 old hammond hwy Suite A', '2252758889', NULL, 'joyawireless2@gmail.com', '$2y$10$lsPrvASYVSZ.d8/B6JMz1exye8.ph.i/CXjfcXWELQU5s2xRRlVF.', NULL, '2020-12-04 03:44:41', '2020-12-04 03:54:32', 0, 0, 'a2d239ae6661a0190008272a7fc4846f', 'Yes', '95f61b7ae44612a71727f8bf948a0129', 0, 'Joya Wireless LLC', NULL, '2252758889', 'suite A', NULL, NULL, NULL, NULL, '1607031881Golber ID.pdf', '1607031881JOYA EIN.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(113, 'Yamil marrero', NULL, NULL, NULL, NULL, 'Urb las praderas 1201 calle aguamarina', '7872399417', NULL, 'yamilmarrerogarcia@gmail.com', '$2y$10$GUeZ9wzUAEnV/ztsRI1aTu070fWF85aXQOJvqV3NhjhKS7i6/AWFy', 'fj4Ak2ZZHqcpU39O6mX69nPR4PlEkh5K8wt8iHmrjiUygBONChaeuH3HMLHI', '2020-12-14 00:40:42', '2020-12-14 00:42:59', 0, 0, '7178c61f3c87cd9286d8bb65fb2b4b7d', 'Yes', 'e7da12b96b7fcbbe625329ee981965ae', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(114, 'Brian M Dalton', NULL, '46250', 'indianapolis', 'United States', '6520 E 82ND ST', '3179089690', NULL, 'bdalton@indycellular4less.com', '$2y$10$DLchrN06QKiMp5S1r0y8lOgdFBaDzqbKcCeA3cjNbjPTGgBczM7DC', NULL, '2020-12-15 23:46:30', '2020-12-15 23:46:30', 0, 0, 'a19e4675d4b40828206741142fee5dec', 'No', 'e65a16a12938f4cd7dd8aa2a94f0eb6e', 0, 'Indy Cellular 4Less', NULL, '3175365455', 'STE 150', NULL, NULL, NULL, NULL, '1608054390Scan Apr 4, 2020 at 11.39 AM.pdf', '1608054390Scan Jan 3, 2018 at 2.46 PM.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(115, 'Ed', NULL, NULL, NULL, NULL, '6222 15th street ne', '2532256049', NULL, 'mcdonoughed@outlook.com', '$2y$10$I4qAJZzdxCqSrpMZFDpZJ.IXZccvW089cUg4tgPdn8b/1WvXkqJM2', 'eiyOiULe8mRmbM68vuzJw0xOb98DMNViVZ4Y6GkLchh0mLijeuJyMSVrvcrC', '2020-12-17 03:09:05', '2020-12-17 03:09:05', 0, 0, 'a2eb2239d1da06e559f07a21dfa0fcd6', 'No', 'dc23c90620ecfc8d2512bfb3053b72ff', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(116, 'khdsjdfskjdjk', NULL, '46000', 'oran', NULL, 'fsdfsd', '0692130926', NULL, 'xdz4631@mailnesia.com', '$2y$10$8g1OrwylEk986RTVOuy3L.iGSb02Jj/tvseEGfnxN7NUnj7n8mWRm', NULL, '2020-12-17 04:36:10', '2020-12-17 04:42:21', 0, 0, '6e0afa6943c2eb80ea4af67fc551f9f6', 'Yes', '9bb9082d81899ef5bf08eed081fc8a2b', 0, 'opôp', NULL, '222', 'sdfsdfdsf', NULL, NULL, NULL, '1608158541blanka.jpg', '1608158170st.php', '1608158170st.php', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2021-01-15', 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(117, 'taraneh kashefpour', NULL, NULL, NULL, NULL, '7180 E. Kierland blvd, unit 206, scottsdale, az, 85254', '6027031459', NULL, 'tara_kashef@yahoo.com', '$2y$10$ibSF3.kWyFufLZhkzhrWLOOBy3DtzgIfd6DWT27fGLDmzvoDjE2be', 'c2QZ3X7ij0060hNz2Pw2hsRe04hB9JaB51x24aOHQR1u5cJirun6kEc3Cf9c', '2020-12-19 00:45:28', '2020-12-19 00:51:15', 0, 0, '05b8f031fd18344d18381243219b4bb6', 'Yes', '6e489d8e880dc90d3dbce19ab0de91b1', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(118, 'Maurice Khoury', NULL, NULL, NULL, NULL, '19992 Ellen Dr', '2485508850', NULL, 'bigmoris@icloud.com', '$2y$10$u1NptECMPCPAorYRJnZJpO0pHM5XAfZXNKIh3fgwzRsRuOV6SgTOC', 'uObkVA1GG7VK3AaMrE0HNfTOlTP9ZjX20pYvlEEDl1ia0HT93Vk7ty3rqDH0', '2020-12-31 06:46:25', '2020-12-31 07:14:16', 0, 0, 'dadb80a50de4eeb8cfed2c980b59a977', 'Yes', '19f1cb78e4d6e9401359f59e214f88b4', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(119, 'test', NULL, '65444', 'Dubai', NULL, '3102 Saba 1 Tower, Jumeirah Lake tower', '067090413', NULL, 'usama.webewox@gmail.com', '$2y$10$LH2A.N9U2ujlxKlOlCjF7OeUZDqB0d4fY52OyaExuFs2XUDHYq8bu', NULL, '2021-01-05 18:06:38', '2021-01-05 18:50:56', 0, 0, 'ddcb2a85ff3e7fb8afbf7b086c785983', 'Yes', '34baf4a4374ef0f0c0e09410d8165cc2', 0, 'webewox', NULL, '4444', 'ssssss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2021-02-04', 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(120, 'Howard Williams', NULL, '23704', 'Portsmouth', 'United States', '3409 George Washington Hwy.', '7573925045', NULL, 'vaphonezone@gmail.com', '$2y$10$7huQbGLjRNz3zJFlAZd9AOlApUck/rl8fd6Lp.lVRhZsfrF00ONn6', 'hk0g9s3DMZ4K4HjQ1ZXdWK87o6hIuPCs94fcOSYiYqTvXSPo2LZwyls4Ja2F', '2021-01-08 23:20:01', '2021-01-08 23:21:24', 0, 0, '79d9a6ee2631b44a0d08891d7495217f', 'Yes', '755d05167632b8477a7a3a067ae93cfa', 0, 'Phone Zone LLC.', NULL, '7579526630', 'Suite B', NULL, NULL, NULL, NULL, '1610126401license.jpg', '1610126401SalesAndUseTaxCertificate.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(121, 'Gustavo Machado', NULL, NULL, NULL, NULL, 'Rua Newton prado, 50 - Araçatuba/SP -', '5518981776969', NULL, 'gustavo@seubaratao.com.br', '$2y$10$5ZRosEoPAkyLwkDfxnoThOPS1X5Bi3CeZVnUkiElA59IJBxyUQHbG', NULL, '2021-01-26 22:16:36', '2021-01-26 22:17:22', 0, 0, 'c410d7d305402be9f199d260dc40d11c', 'Yes', 'a4a239e3f85de7aa23b2d363de5cc9dd', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(122, 'Tekcell', NULL, '30071', 'NORCROSS', 'United States', '5161 Brook Hollow Pkwy', '4045475962', NULL, 'Purchase.cellairis@gmail.com', '$2y$10$UQN3feiNV5ielBRuufJvo.QfAGPZ6mpbZYfDqavHa9UUVsr8rg6Pi', 'xQjjEsOLTdC5OckuBqFaZiup6ZoYGJ1EX4FbcwL1ItSPc1wOlbiq4i165FQs', '2021-02-05 01:02:37', '2021-02-05 20:46:02', 0, 0, 'fe206859f4fc623319f9f506acaf9513', 'Yes', 'ed2a854cd20ecbc972b1abb485e5fc32', 0, 'Tekcell LLC', NULL, '6787642005', '205', NULL, NULL, NULL, NULL, NULL, '1612465357Sales Tax Cert.Tekcell,Cobb PKWY S.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(123, 'Dennis Pascal', NULL, NULL, NULL, NULL, '216 BT ky11601', '13459252355', NULL, 'dennis2355@hotmail.com', '$2y$10$RT.R.3bJ1Hdca39QrjT79eaq/VtBygvWkY7r5xBweK/pS5uohXXtu', '4t2GM2Iy4vrxXrKFdaiplLEYOKh3XhlbekNeg85j1CkEz3MkqsKMtJjAvF20', '2021-02-05 21:02:30', '2021-02-05 21:10:54', 0, 0, '62c312ba5dc46833d3af80a81ad369fa', 'Yes', '5cbcc7f5ebc0be317507405ab27e30f4', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(124, 'Supreme', NULL, '30071', 'NORCROSS', 'United States', '5161 Brook Hollow Pkwy', '4045475962', NULL, 'supremecellairis@gmail.com', '$2y$10$RwgWcE3ydQm77DHWdQAT6eTuS9GamP9kpARHD3YlXWsuDw6DhqX7i', NULL, '2021-02-05 21:06:01', '2021-02-05 21:06:19', 0, 0, '066ed5234fc08c1ee7f3aae4b61b6eba', 'Yes', 'd24b3b6d82824820f2c1c37c0c03add7', 0, 'Supreme Mobile LLC', NULL, '6787642005', 'Ste 205', NULL, NULL, NULL, NULL, NULL, '1612537561Sales Tax Cert.Tekcell,Cobb PKWY S.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(125, 'Sai Cell', NULL, '30071', 'NORCROSS', 'United States', '5161 Brook Hollow Pkwy', '4045475962', NULL, 'Purchase.saicell@gmail.com', '$2y$10$hpl9ALhHY9YEZyGMt7SNZexd/IHXWIC9JuFj3lmoQc7fffGtbMPzq', '8iFYDSws0qOCY19VDor6OFdHr5n72qnaeO1qzUFRy4xAzl6SHZrgUs1BG8M4', '2021-02-05 21:10:33', '2021-02-05 21:15:08', 0, 0, '3eddd5fd19f1e30294f4f26d4c03dffd', 'Yes', '3c88eb19ab9bf5cb91670524eae767c5', 0, 'Sai Cell LLC', NULL, '6787642005', 'Ste 205', NULL, NULL, NULL, NULL, NULL, '1612537833SAI CELL EIN.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(126, 'Apopka', NULL, '30071', 'NORCROSS', 'United States', '5161 Brook Hollow Pkwy', '4045475962', NULL, 'cellairis.apopka@gmail.com', '$2y$10$xAzdjQUUDQT0SGbTghdFpebE6cXwgkJe5CLSEDWD2XRu37RnsZMce', '6NXi07tWS7Ji5EppBDhcO5jI0js7MR6BaK9gJHRZmQCPtxG0gZXCpDXHhvZY', '2021-02-05 21:19:43', '2021-02-05 21:19:59', 0, 0, '5f8d1c57a760dc673d5164c265424ee3', 'Yes', '6fd96377f3cf6a98c311128c4fa49688', 0, 'Amanah Cell-Apopka', NULL, '4045780453', 'Ste 205', NULL, NULL, NULL, NULL, NULL, '1612538383EIN Letter Amanah Cell LLC.pdf', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(127, 'Hialeah', NULL, '30071', 'NORCROSS', 'United States', '5161 Brook Hollow Pkwy', '4045475962', NULL, 'cellairis.hialeah@gmail.com', '$2y$10$S4gYHWeNZP.7RrtQTxkgA.SAYhsLPwvTlw3pqUO25N1VQm7y/Uy66', NULL, '2021-02-05 21:22:31', '2021-02-05 21:26:59', 0, 0, '9b02101696307781842d1de5003ecbf2', 'Yes', 'b9273d06e7c41709a9785518accd13b8', 0, 'Amanah Cell-Hialeah', NULL, '6787642005', 'Ste 205', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(128, 'Citi Diamonds', NULL, '30071', 'NORCROSS', 'United States', '5161 Brook Hollow Pkwy', '4045475962', NULL, 'Purchase.citidiamond@gmail.com', '$2y$10$//LBy1wx1KMRcbdOFyCBmu6dMvUzIG4953aPVoNdAV9XByW.q/xJa', '4IHdsdGkbD5Tm9KHGdubZxaiwFBwzFre1BspMTfyGraS7a3B3C607cjKzzvj', '2021-02-05 21:30:44', '2021-02-05 21:35:16', 0, 0, '2d563c23892a59ddd5d73032d0716a7e', 'Yes', 'a9ad9c28a96a8aebd4b623198fd7f8f5', 0, 'Citi Diamonds', NULL, '6787642005', 'Ste 205', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(129, 'Bladimy Denival', NULL, NULL, NULL, NULL, '2959 Chapel Hill Rd', '4702530565', NULL, 'b2cool5@gmail.com', '$2y$10$oaQSrqQIH6msUrc1vYUwku7wGFNV4HettpKRsza5iwOzKEv5V4CWS', 'a1bpCBR1mkpdE5UMfbMKtefD8StoCoBQcDXlcu8JA50zSeR7M3J9A2l18e4D', '2021-02-09 01:17:18', '2021-02-09 01:19:22', 0, 0, 'eaa92e3188415ed518fe271a93338793', 'Yes', 'd210673ba6798643fbaeafdce8188dbd', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `order_number`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 13, '2csj1590602773', 1, '2020-05-27 13:06:13', '2020-06-05 06:26:54'),
(2, 13, 'f3uh1590603132', 1, '2020-05-27 13:12:12', '2020-06-05 06:26:54'),
(3, 13, 'pvtt1591012687', 1, '2020-06-01 06:58:08', '2020-06-05 06:26:54'),
(4, 13, 'yaDw1591013386', 1, '2020-06-01 07:09:46', '2020-06-05 06:26:54'),
(5, 13, 'Ss0C1591013646', 1, '2020-06-01 07:14:06', '2020-06-05 06:26:54'),
(6, 13, 'SS1P1591013979', 1, '2020-06-01 07:19:40', '2020-06-05 06:26:54'),
(7, 13, 'Ocjm1591349941', 1, '2020-06-05 04:39:02', '2020-06-05 06:26:54'),
(8, 13, 'e8vz1591350215', 1, '2020-06-05 04:43:36', '2020-06-05 06:26:54'),
(9, 13, 'jKNF1591350507', 1, '2020-06-05 04:48:27', '2020-06-05 06:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_subscriptions`
--

CREATE TABLE `user_subscriptions` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `subscription_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Free',
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payment_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_subscriptions`
--

INSERT INTO `user_subscriptions` (`id`, `user_id`, `subscription_id`, `title`, `currency`, `currency_code`, `price`, `days`, `allowed_products`, `details`, `method`, `txnid`, `charge_id`, `created_at`, `updated_at`, `status`, `payment_number`) VALUES
(81, 27, 5, 'Standard', '$', 'NGN', 60, 45, 25, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Paystack', '688094995', NULL, '2019-10-09 21:32:57', '2019-10-09 21:32:57', 1, NULL),
(84, 13, 5, 'Standard', '$', 'NGN', 60, 45, 500, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Paystack', '242099342', NULL, '2019-10-10 02:35:29', '2019-10-10 02:35:29', 1, NULL),
(85, 58, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-10-14 23:19:03', '2020-10-14 23:19:03', 1, NULL),
(86, 108, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-12-07 17:05:37', '2020-12-07 17:05:37', 1, NULL),
(87, 110, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-12-11 07:23:03', '2020-12-11 07:23:03', 1, NULL),
(88, 116, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-12-17 04:37:55', '2020-12-17 04:37:55', 1, NULL),
(89, 119, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2021-01-05 18:14:53', '2021-01-05 18:14:53', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_orders`
--

CREATE TABLE `vendor_orders` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `order_id` int(191) NOT NULL,
  `qty` int(191) NOT NULL,
  `price` double NOT NULL,
  `order_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_orders`
--

INSERT INTO `vendor_orders` (`id`, `user_id`, `order_id`, `qty`, `price`, `order_number`, `status`) VALUES
(1, 13, 1, 1, 130, '2csj1590602773', 'pending'),
(2, 13, 2, 1, 130, 'f3uh1590603132', 'pending'),
(3, 13, 3, 40, 3584, 'pvtt1591012687', 'pending'),
(4, 13, 4, 5, 700, 'yaDw1591013386', 'pending'),
(5, 13, 5, 5, 600, 'Ss0C1591013646', 'pending'),
(6, 13, 6, 1, 120, 'SS1P1591013979', 'pending'),
(7, 13, 7, 9, 1170, 'Ocjm1591349941', 'pending'),
(8, 13, 7, 9, 1170, 'Ocjm1591349941', 'pending'),
(9, 13, 8, 2, 260, 'e8vz1591350215', 'pending'),
(10, 13, 8, 2, 260, 'e8vz1591350215', 'pending'),
(11, 13, 9, 2, 260, 'jKNF1591350507', 'pending'),
(12, 13, 9, 2, 260, 'jKNF1591350507', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE `verifications` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `attachments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('Pending','Verified','Declined') DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `admin_warning` tinyint(1) NOT NULL DEFAULT '0',
  `warning_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`id`, `user_id`, `attachments`, `status`, `text`, `admin_warning`, `warning_reason`, `created_at`, `updated_at`) VALUES
(4, 13, '1573723849Baby.tux-800x800.png,1573723849Baby.tux-800x800.png', 'Declined', 'TEst', 0, NULL, '2019-11-14 03:30:49', '2020-06-16 07:12:09'),
(6, 30, NULL, NULL, NULL, 1, 'hello we didn\'t recieved it yet', '2020-06-16 06:07:47', '2020-06-16 06:07:47'),
(10, 31, '1592666391brand6.jpg,1592666391images.png', 'Verified', 'here it is', 0, 'hello send us your images', '2020-06-20 10:18:48', '2020-06-20 10:20:09'),
(11, 31, NULL, NULL, NULL, 1, 'hey send me your TAX ID.', '2020-07-24 14:23:47', '2020-07-24 14:23:47'),
(12, 108, NULL, NULL, NULL, 1, 'fgf', '2020-12-07 17:11:07', '2020-12-07 17:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(191) UNSIGNED NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`) VALUES
(12, 22, 119),
(13, 22, 118),
(14, 22, 117),
(15, 22, 116),
(22, 111, 301),
(23, 108, 337),
(24, 108, 336);

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

CREATE TABLE `withdraws` (
  `id` int(191) NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `swift` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` float DEFAULT NULL,
  `fee` float DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('pending','completed','rejected') NOT NULL DEFAULT 'pending',
  `type` enum('user','vendor') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessory_brands`
--
ALTER TABLE `accessory_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accessory_types`
--
ALTER TABLE `accessory_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_languages`
--
ALTER TABLE `admin_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user_conversations`
--
ALTER TABLE `admin_user_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user_messages`
--
ALTER TABLE `admin_user_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_types`
--
ALTER TABLE `cat_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_type_children`
--
ALTER TABLE `cat_type_children`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `childcategories`
--
ALTER TABLE `childcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counters`
--
ALTER TABLE `counters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite_sellers`
--
ALTER TABLE `favorite_sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generalsettings`
--
ALTER TABLE `generalsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_tracks`
--
ALTER TABLE `order_tracks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagesettings`
--
ALTER TABLE `pagesettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pickups`
--
ALTER TABLE `pickups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategory_id` (`subcategory_id`);
ALTER TABLE `products` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `products` ADD FULLTEXT KEY `attributes` (`attributes`);

--
-- Indexes for table `product_clicks`
--
ALTER TABLE `product_clicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `discounts_id` (`discount_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seotools`
--
ALTER TABLE `seotools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socialsettings`
--
ALTER TABLE `socialsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_providers`
--
ALTER TABLE `social_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_orders`
--
ALTER TABLE `vendor_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraws`
--
ALTER TABLE `withdraws`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessory_brands`
--
ALTER TABLE `accessory_brands`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `accessory_types`
--
ALTER TABLE `accessory_types`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `admin_languages`
--
ALTER TABLE `admin_languages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_user_conversations`
--
ALTER TABLE `admin_user_conversations`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin_user_messages`
--
ALTER TABLE `admin_user_messages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute_options`
--
ALTER TABLE `attribute_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `cat_types`
--
ALTER TABLE `cat_types`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cat_type_children`
--
ALTER TABLE `cat_type_children`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `childcategories`
--
ALTER TABLE `childcategories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `counters`
--
ALTER TABLE `counters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `favorite_sellers`
--
ALTER TABLE `favorite_sellers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=517;

--
-- AUTO_INCREMENT for table `generalsettings`
--
ALTER TABLE `generalsettings`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `order_tracks`
--
ALTER TABLE `order_tracks`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pagesettings`
--
ALTER TABLE `pagesettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `pickups`
--
ALTER TABLE `pickups`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=344;

--
-- AUTO_INCREMENT for table `product_clicks`
--
ALTER TABLE `product_clicks`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6504;

--
-- AUTO_INCREMENT for table `product_discounts`
--
ALTER TABLE `product_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `seotools`
--
ALTER TABLE `seotools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `socialsettings`
--
ALTER TABLE `socialsettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_providers`
--
ALTER TABLE `social_providers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `vendor_orders`
--
ALTER TABLE `vendor_orders`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `withdraws`
--
ALTER TABLE `withdraws`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `subcategory_id_foreign_key` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`);

--
-- Constraints for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD CONSTRAINT `product_discounts_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_discounts_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
