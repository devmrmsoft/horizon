$('.owl-carousel.__banner-section').owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: false,
    autoplayTimeout: 5000,
    animateOut: 'fadeOut',
    autoplay: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
        },
        600: {
            items: 1,
            nav: false,
        },
        1000: {
            items: 1
        }
    }
})
// Main Banner Slider

$('.owl-carousel.__featuredSlider').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 3000,
    dots: false,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 3
        },
        600: {
            items: 4
        },
        1000: {
            items: 8
        }
    }
})
//Featured Brands Slider


$('.owl-carousel.__shopbyDeviceSlider').owlCarousel({
    loop: true,
    dots: false,
    margin: 10,
    nav: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 3,
        },
        600: {
            items: 4
        },
        1000: {
            items: 8,
            nav: true
        }
    }
})
// Shop by Device Slider

$('.owl-carousel.__productNav').owlCarousel({
    center: true,
    items: 3,
    loop: false,
    margin: 0,
    dots: false,
    responsive: {
        600: {
            items: 4
        }
    }
});
//Product Nav For Mobile View

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
// Tooltip Toggle

$('.owl-carousel.__brandSlider-img').owlCarousel({
    items: 1,
    margin: 10,
    loop: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 5000,
    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 1
        },
        1000: {
            items: 1,
        }
    }
})
// Brands Slider 

$('.owl-carousel.__lowerSlider-img').owlCarousel({
    items: 1,
    margin: 10,
    loop: true,
    animateOut: 'fadeOutLeft',
    animateIn: 'fadeInRight',
    // autoplay: true,
    // autoplayTimeout: 5000,
    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 1
        },
        1000: {
            items: 1,
        }
    }
})
// Lower Slider 

$(window).scroll(function () {
    $('.main-header').toggleClass('scroll', $(this).scrollTop() > 90);
});
//Scroll Navbar in Desktop

$(window).scroll(function () {
    $('.mob-header').toggleClass('scroll', $(this).scrollTop() > 90);
});
//Scroll Navbar in Mobile

function openFilter() { 
    
    var Filters = document.getElementById("Filters");
    Filters.classList.add("filters");
    
    var resize = document.getElementById("resize");
     resize.classList.add("resize");
    
    var filtericon = document.getElementById("filter-icon");
     filtericon.classList.add("filtericon");
    
    
    var crossicon = document.getElementById("cross-icon");
     crossicon.classList.add("crossicon");

}

function closeFilter() {
    var Filters = document.getElementById("Filters");
    Filters.classList.remove("filters");
    
    var resize = document.getElementById("resize");
     resize.classList.remove("resize");
    
    var filtericon = document.getElementById("filter-icon");
     filtericon.classList.remove("filtericon");
    
    
    var crossicon = document.getElementById("cross-icon");
     crossicon.classList.remove("crossicon");
}

$(document).ready(function(){
    $("#filter-icon").click(function(){
        $(".remove-padding").addClass("filtered-products");
    });
});
$(document).ready(function(){
    $(".closebtn").click(function(){
        $(".remove-padding").removeClass("filtered-products");
    });
});

var $star_rating = $('.star-rating .fa');

var SetRatingStar = function () {
    return $star_rating.each(function () {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
};

$star_rating.on('click', function () {
    $star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
});

SetRatingStar();
$(document).ready(function () {

});
//Star Rating

$('.header').on('click', '.search-toggle', function (e) {
    var selector = $(this).data('selector');

    $(selector).toggleClass('show').find('.search-input').focus();
    $(this).toggleClass('active');

    e.preventDefault();
});
//Header Navigation Search Bar

$('.owl-carousel.featuredProduct-Slider').owlCarousel({
    loop: true,
    margin: 15,
    nav: true,
    autoplay: true,
    dots: false,
    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 3,
        },
        1000: {
            items: 4,
        }
    }
})
// Featured Products

$('.owl-carousel.__promo-slide-show').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    animateOut: 'fadeOutLeft',
    animateIn: 'fadeInRight',
    autoplay: true,
    autoplayTimeout: 5000,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})
// Lower Slider

function openNav() {
    document.getElementById("my-mob-nav").style.width = "250px";
}

function closeNav() {
    document.getElementById("my-mob-nav").style.width = "0";
}
// Mobile Responsive Header

$('#mobile-nav').on('click', '.search-toggle-1', function (e) {
    var selector = $(this).data('selector');

    $(selector).toggleClass('show').find('.search-input').focus();
    $(this).toggleClass('active');

    e.preventDefault();
});

$(document).ready(function () {
    // executes when HTML-Document is loaded and DOM is ready
    // breakpoint and up  
    $(window).resize(function () {
        if ($(window).width() >= 980) {
            // when you hover a toggle show its dropdown menu
            $(".navbar .dropdown-toggle").hover(function () {
                $(this).parent().toggleClass("show");
                $(this).parent().find(".dropdown-menu").toggleClass("show");
            });
            // hide the menu when the mouse leaves the dropdown
            $(".navbar .dropdown-menu").mouseleave(function () {
                $(this).removeClass("show");
            });
            // do something here
        }
    });
    // document ready  
});
$(function () {
    $('a[href="#search"]').on('click', function (event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });

    $('#search, #search button.close').on('click keyup', function (event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });


    //Do not include! This prevents the form from submitting for DEMO purposes only!

});
$('.owl-carousel.categori-brands-model').owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: false,
    autoplay: true,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 4
        },
        1000: {
            items: 5
        }
    }
});

//-----JS for Price Range slider-----//

$(function () {
    $("#slider-range").slider({
        range: true,
        min: 100,
        max: 5000,
        values: [100, 2500],
        slide: function (event, ui) {
            $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            $("#min, #min_price").val(ui.values[0]);
            $("#max, #max_price").val(ui.values[1]);
            filter();
        }
    });
    $("#amount").val("$" + $("#slider-range").slider("values", 0) +
        " - $" + $("#slider-range").slider("values", 1));
});