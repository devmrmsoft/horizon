<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::filter('nohttps', function() {
//   if (Request::secure()) {return Redirect::to('http://' . Request::url());}
// });
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cattype', 'Api\CategoryController@getCatType')->name('api.cattype');

Route::get('/category', 'Api\CategoryController@getcategory')->name('api.category');

Route::get('/carriers', 'Api\CategoryController@getcarriers')->name('api.carriers');

Route::get('/cattypechild', 'Api\CategoryController@getcatTypeChild')->name('api.cattypechild');

Route::get('/accessorybrand', 'Api\CategoryController@getaccessoryBrand')->name('api.accessorybrand');

Route::get('/accessorytype', 'Api\CategoryController@getaccessoryType')->name('api.accessorytype');

Route::get('/childcategory', 'Api\CategoryController@getChildcategory')->name('api.childcategory');

Route::get('/subcategory', 'Api\CategoryController@getSubcategory')->name('api.subcategory');

Route::get('/product', 'Api\ProductController@getProduct')->name('api.product');


Route::post('/storeOrderData', 'Api\ProductController@storeOrderData')->name('api.storeOrderData');
