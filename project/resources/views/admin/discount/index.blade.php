@extends('layouts.admin') 

@section('content')  
					<div class="content-area">
						<div class="mr-breadcrumb">
							<div class="row">
								<div class="col-lg-12">
									<h4 class="heading">{{ __("Discounts") }}</h4>
									<ul class="links">
										<li>
											<a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }} </a>
										</li>
										<li>
											<a href="javascript:;">{{ __("Discounts") }}</a>
										</li>
										<li>
											<a href="{{ route('admin-vendor-index') }}">{{ __("Discounts List") }}</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="product-area">
							<div class="row">
								<div class="col-lg-12">


									<div class="mr-table allproduct">
										@include('includes.admin.form-success') 
										<div class="table-responsiv">
											<table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th>Serial</th>
	                                                  <th>{{ __("Discount Name") }}</th>
	                                                  <th>{{ __("Status") }}</th>
													</tr>
												</thead>
												<tbody>
													@php $serial = 1; @endphp
													@foreach($discounts as $discount)
													<tr>
														<td> #{{ $serial }}</td>
														<td> {{ $discount->name }}</td>
														<td> <a href="{{ route('admin-discount-edit',[$discount->id]) }}"> <i class="fas fa-edit"></i> Edit</a> || <a href="{{ route('admin-discount-delete',[$discount->id]) }}"> <i class="fas fa-trash-alt"></i> Delete</a></td>
													</tr>
													@php $serial += 1; @endphp
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>




@endsection    

@section('scripts')


    <script type="text/javascript">

		var table = $('#geniustable').DataTable({
			   ordering: false,
               processing: true,
               serverSide: true,
               ajax: '{{ route('admin-discount-datatables') }}',
               columns: [
                        { data: 'shop_name', name: 'shop_name' },
                        { data: 'email', name: 'email' },
                        { data: 'shop_number', name: 'shop_number' },
                        { data: 'status', searchable: false, orderable: false},
            			{ data: 'action', searchable: false, orderable: false }
                     ],
               language : {
                	processing: '<img src="{{asset('assets/images/'.$gs->admin_loader)}}">'
                },
				drawCallback : function( settings ) {
	    				$('.select').niceSelect();	
				}
            });

	    				$('.select1').niceSelect();	
																
    </script>


<script type="text/javascript">


      	$(function() {
        $(".btn-area").append('<div class="col-sm-4 table-contents">'+
        	'<a class="add-btn" href="{{route('admin-discount-create')}}">'+
          '<i class="fas fa-plus"></i> <span class="remove-mobile">{{ __("Add New Discount List") }}<span>'+
          '</a>'+
          '</div>');
      });			

</script>

{{-- DATA TABLE --}}
    
@endsection   