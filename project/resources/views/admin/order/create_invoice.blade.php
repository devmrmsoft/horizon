@extends('layouts.admin')
     
@section('styles')

<style type="text/css">
    .order-table-wrap table#example2 {
    margin: 10px 20px;
}

</style>

@endsection


@section('content')
    <div class="content-area">
        <div class="mr-breadcrumb">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading">{{ __('Order Details') }} <a class="add-btn" href="javascript:history.back();"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
                    <ul class="links">
                        <li>
                            <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                        <li>
                            <a href="javascript:;">{{ __('Orders') }}</a>
                        </li>
                        <li>
                            <a href="javascript:;">{{ __('Order Details') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="order-table-wrap">
            <form method="POST" class="checkoutform" action="{{ route('cash.submit') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="special-box">
                            <div class="heading-area">
                                <h4 class="title">
                                {{ __('Order Details') }}
                                </h4>
                            </div>
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th class="45%" width="45%">{{ __('Order ID') }}</th>
                                            <td width="10%">:</td>
                                            <td class="45%" width="45%"><input type="text"  class="input-field" readonly="" name="order_number" value="<?php echo str_random(4).time(); ?>"></td>
                                        </tr>
                                        <tr>
                                            <th width="45%">{{ __('Total Product') }}</th>
                                            <td width="10%">:</td>
                                            <td width="45%"><input type="text"  class="input-field" name="totalQty" placeholder="e.g 1"></td>
                                        </tr>
                                        <tr>
                                            <th width="45%">{{ __('Total Cost') }}</th>
                                            <td width="10%">:</td>
                                            <td width="45%"><input type="text"  class="input-field" name="total" placeholder="e.g 1"></td>
                                        </tr>
                                        <tr>
                                            <th width="45%">{{ __('Ordered Date') }}</th>
                                            <td width="10%">:</td>
                                            <td width="45%">{{date('d-M-Y H:i:s a')}}</td>
                                        </tr>
                                        <tr>
                                            <th width="45%">{{ __('Payment Method') }}</th>
                                            <td width="10%">:</td>
                                            <td width="45%">
                                                <select name="method"  class="input-field">
                                                    <option value="PayPal Express" selected="">PayPal Express</option>
                                                    <option value="Credit Card">Credit Card</option>
                                                    <option value="Cash On Delivery">Cash On Delivery</option>
                                                </select>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="special-box">
                            <div class="heading-area">
                                <h4 class="title">
                                {{ __('Billing Details') }}
                                </h4>
                            </div>
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <tbody>
                                            <tr>
                                                <th width="45%">{{ __('Name') }}</th>
                                                <th width="10%">:</th>
                                                <td width="45%"><input type="text"  class="input-field" name="name" placeholder="e.g 1"></td>
                                            </tr>
                                            <tr>
                                                <th width="45%">{{ __('Email') }}</th>
                                                <th width="10%">:</th>
                                                <td width="45%"><input type="text"  class="input-field" name="email" placeholder="e.g 1"></td>
                                            </tr>
                                            <tr>
                                                <th width="45%">{{ __('Phone') }}</th>
                                                <th width="10%">:</th>
                                                <td width="45%"><input type="text"  class="input-field" name="phone" placeholder="e.g 1"></td>
                                            </tr>
                                            <tr>
                                                <th width="45%">{{ __('Address') }}</th>
                                                <th width="10%">:</th>
                                                <td width="45%"><input type="text"  class="input-field" name="address" placeholder="e.g 1"></td>
                                            </tr>
                                            <tr>
                                                <th width="45%">{{ __('Country') }}</th>
                                                <th width="10%">:</th>
                                                <td width="45%"><input type="text"  class="input-field" name="customer_country" placeholder="e.g 1"></td>
                                            </tr>
                                            <tr>
                                                <th width="45%">{{ __('City') }}</th>
                                                <th width="10%">:</th>
                                                <td width="45%"><input type="text"  class="input-field" name="city" placeholder="e.g 1"></td>
                                            </tr>
                                            <tr>
                                                <th width="45%">{{ __('Postal Code') }}</th>
                                                <th width="10%">:</th>
                                                <td width="45%"><input type="text"  class="input-field" name="zip" placeholder="e.g 1"></td>
                                            </tr>
               
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="special-box">
                            <div class="heading-area">
                                <h4 class="title">
                                {{ __('Shipping Details') }}
                                </h4>
                            </div>
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Name') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td><input type="text"  class="input-field" name="shipping_name" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Email') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"><input type="text"  class="input-field" name="shipping_email" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Phone') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"><input type="text"  class="input-field" name="shipping_phone" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Address') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"><input type="text"  class="input-field" name="shipping_address" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Country') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"><input type="text"  class="input-field" name="shipping_country" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('City') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"> <input type="text"  class="input-field" name="shipping_city" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Postal Code') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"> <input type="text"  class="input-field" name="shipping_zip" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Shippping Cost') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"> <input type="text"  class="input-field" name="shipping_cost" value="0"></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Packing Cost') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"> <input type="text"  class="input-field" name="packing_cost" value="0" ></td>
                                        </tr>
                                        <tr>
                                            <th width="45%"><strong>{{ __('Tax') }}:</strong></th>
                                            <th width="10%">:</th>
                                            <td width="45%"> <input type="text"  class="input-field" name="tax" value="0" >
                                                <input type="hidden" name="dp" value="0">
                                                <input type="hidden" name="vendor_shipping_id" value="0">
                                                <input type="hidden" name="vendor_packing_id" value="0">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="add-products-column">
                    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:;" id="add-products-btn" onclick="addMoreProduct()" class="add-more float-right"><i class="fas fa-plus"></i>{{ __('Add Product') }} </a>
                <input type="submit" class="add-more float-left" name="submit" value="CREATE">
                    </div>
                </div>
            </form>
        </div>
    </div>
                    <!-- Main Content Area End -->
{{-- LICENSE MODAL --}}




@endsection


@section('scripts')

<script type="text/javascript">
$('#example2').dataTable( {
  "ordering": false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false,
      'responsive'  : true
} );
</script>

    <script type="text/javascript">
        $(document).on('click','#license' , function(e){
            var id = $(this).parent().find('input[type=hidden]').val();
            var key = $(this).parent().parent().find('input[type=hidden]').val();
            $('#key').html(id);
            $('#license-key').val(key);
    });
        $(document).on('click','#license-edit' , function(e){
            $(this).hide();
            $('#edit-license').show();
            $('#license-cancel').show();
        });
        $(document).on('click','#license-cancel' , function(e){
            $(this).hide();
            $('#edit-license').hide();
            $('#license-edit').show();
        });

        $(document).on('submit','#edit-license' , function(e){
            e.preventDefault();
          $('button#license-btn').prop('disabled',true);
              $.ajax({
               method:"POST",
               url:$(this).prop('action'),
               data:new FormData(this),
               dataType:'JSON',
               contentType: false,
               cache: false,
               processData: false,
               success:function(data)
               {
                  if ((data.errors)) {
                    for(var error in data.errors)
                    {
                        $.notify('<li>'+ data.errors[error] +'</li>','error');
                    }
                  }
                  else
                  {
                    $.notify(data,'success');
                    $('button#license-btn').prop('disabled',false);
                    $('#confirm-delete').modal('toggle');

                   }
               }
                });
        });

        function addMoreProduct(){
            var leng =  $('.productscolumn').length;
            var token = $("input[name=_token]").val();
            
              $.ajax({
               method:"GET",
               url:'{{ route("admin_get_products_for_invoice") }}',
               data:{_token:token},
               dataType:'JSON',
               contentType: false,
               cache: false,
               processData: false,
               success:function(data)
               {
                $('#add-products-column').append(data.html);
                $('.js-example-basic-single').select2();
               }
                });
        }
        function removeThisRow(getThis){
            $('.cart-remove').click();
            $.get( $(this).data('href') , function( data ) {
                if(data == 0) {
                    $("#cart-count").html(data);
                   $('.cart-table').html('<h3 class="mt-1 pl-3 text-left">Cart is empty.</h3>');
                    $('#cart-items').html('<p class="mt-1 pl-3 text-left">Cart is empty.</p>');
                    $('.cartpage .col-lg-4').html('');
                  }
                else {
                   $('.cart-quantity').html(data[1]);
                   $('.cart-total').html(data[0]);
                   $('.coupon-total').val(data[0]);
                   $('.main-total').html(data[3]);
                  }

            });
            var getparentrow    =   $(getThis).parent().parent().remove();
        }
        function addSingleProduct(getThis){
                var mainparentDiv   =   $(getThis).parent().parent();

                var pid =   $(mainparentDiv).find('.prdouct_id').val();
                var qty =   $("input[name='product_qty']").val();
                var sizes       =   '';
                var colors      =   '';
                var size_qty    =   '';
                var size_price  =   '';
                var size_key    =   '';
                var keys        =   '';
                var values      =   '';
                var prices      =   '';
                    $.ajax({
                          type: "GET",
                          url:mainurl+"/addnumcart",
                          data:{id:pid,qty:qty,size:sizes,color:colors,size_qty:size_qty,size_price:size_price,size_key:size_key,keys:keys,values:values,prices:prices},
                          success:function(data){
                                console.log(data);
                                $(mainparentDiv).find('.remove-this-row').append('<span class="removecart cart-remove" data-class="cremove'+data.item.id+'S'+data.color+'" data-href="{{ url("/removecart/")}}/'+data.item.id+'S'+data.color+'"><i class="icofont-ui-delete"></i> </span>');                                
                             }
                          });
                    }



    $(document).on('click', '.cart-remove', function(){
        
      var $selector = $(this).data('class');
      $('.'+$selector).hide();
        $.get( $(this).data('href') , function( data ) {
            if(data == 0) {
                $("#cart-count").html(data);
               $('.cart-table').html('<h3 class="mt-1 pl-3 text-left">Cart is empty.</h3>');
                $('#cart-items').html('<p class="mt-1 pl-3 text-left">Cart is empty.</p>');
                $('.cartpage .col-lg-4').html('');
              }
            else {
               $('.cart-quantity').html(data[1]);
               $('.cart-total').html(data[0]);
               $('.coupon-total').val(data[0]);
               $('.main-total').html(data[3]);
              }

        });
    });


    </script>
    <script src="{{asset('assets/admin/js/custom.js')}}"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection