{{-- {!! $email_body !!} --}}

<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        @media screen {
            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 400;
                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 700;
                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 400;
                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 700;
                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
            }
        }

        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <!-- HIDDEN PREHEADER TEXT -->
  
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- LOGO -->
        <tr>
            <td bgcolor="#ff5500" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ff5500" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                            <img src="{{asset('assets/images/'.$gs->logo)}}" width="125" height="120" style="display: block; border: 0px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;">Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or  <a href="{{ url('user/register/verify/'.$token) }}" style='text-decoration: none;color: #ff5500;'>Simply click here to verify. </a></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>    
         <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;"><strong>Name:</strong> {{ $name }}</p>
                            <p style="margin: 0; line-height: 2;"><strong>Email:</strong> {{ $to }}</p>
                            <p style="margin: 0; line-height: 2;"><strong>Password:</strong> {{ $password }}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
          <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                          <p style="margin: 0; line-height: 2;">Thank you for registering with Horizon Wireless. Please allow us 1-2 business to process your account.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>  
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;">Please download and fill out the Texas State sales tax exemption certificates application to increase the approval time.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>   
           <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;">Texas State sales tax exemption certificates application - Download Link</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>    
         <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;">You can submit the form by replying this email or email the form to <a href="mailto:sales@horizonwirelesstx.com" style='text-decoration: none;color: #ff5500;'>sales@horizonwirelesstx.com</a></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
          <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;">You may also fax the form to xxx-xxx-xxxx</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>    
         <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;">Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
     <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">                    
                     <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 60px 40px 60px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0; line-height: 2;"><strong>Thanks,</strong></p>
                            <p style="margin: 0; line-height: 2;"><strong>Horizon Wireless</strong></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>  
  <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 30px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#14407d" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need help? Contact Us!</h2>
                            <p style="margin: 0;font-size: 16px;"><a href="tel:+1 713 988 6565" target="_blank" style="color: #ffffff;text-decoration: none;">+1 713 988 6565</a></p>
                            <p style="margin: 0;font-size: 16px;"><a href="mailto:info@horizonwirelesstx.com" target="_blank" style="color: #ffffff;text-decoration: none;">info@horizonwirelesstx.com</a></p>
                            <p style="margin: 0;font-size: 12px;color: #ffffff; ">© 2020 Horizon Wireless</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
</body>

</html>