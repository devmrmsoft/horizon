@extends('layouts.front')

@section('content')

<section class="login-signup">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <nav class="comment-log-reg-tabmenu">
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link login active" id="nav-log-tab" data-toggle="tab" href="#nav-log" role="tab"
              aria-controls="nav-log" aria-selected="true">
              {{ $langg->lang197 }}
            </a>
            <a class="nav-item nav-link" id="nav-reg-tab" data-toggle="tab" href="#nav-reg" role="tab"
              aria-controls="nav-reg" aria-selected="false">
              {{ $langg->lang198 }}
            </a>
          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-log" role="tabpanel" aria-labelledby="nav-log-tab">
            <div class="login-area">
              <div class="header-area">
                <h4 class="title">{{ $langg->lang172 }}</h4>
              </div>
              <div class="login-form signin-form">
                @include('includes.admin.form-login')
                <form class="mloginform" action="{{ route('user.login.submit') }}" method="POST">
                  {{ csrf_field() }}
                  <div class="form-input">
                    <input type="email" name="email" placeholder="{{ $langg->lang173 }}" required="">
                    <i class="icofont-user-alt-5"></i>
                  </div>
                  <div class="form-input">
                    <input type="password" class="Password" name="password" placeholder="{{ $langg->lang174 }}"
                      required="">
                    <i class="icofont-ui-password"></i>
                  </div>
                  <div class="form-forgot-pass">
                    <div class="left">
                      <input type="checkbox" name="remember" id="mrp" {{ old('remember') ? 'checked' : '' }}>
                      <label for="mrp">{{ $langg->lang175 }}</label>
                    </div>
                    <div class="right">
                      <a href="{{ route('user-forgot') }}">
                        {{ $langg->lang176 }}
                      </a>
                    </div>
                  </div>
                  <input type="hidden" name="modal" value="1">
                  <input class="mauthdata" type="hidden" value="{{ $langg->lang177 }}">
                  <button type="submit" class="submit-btn">{{ $langg->lang178 }}</button>
                  @if(App\Models\Socialsetting::find(1)->f_check == 1 || App\Models\Socialsetting::find(1)->g_check ==
                  1)
                  <div class="social-area">
                    <h3 class="title">{{ $langg->lang179 }}</h3>
                    <p class="text">{{ $langg->lang180 }}</p>
                    <ul class="social-links">
                      @if(App\Models\Socialsetting::find(1)->f_check == 1)
                      <li>
                        <a href="{{ route('social-provider','facebook') }}">
                          <i class="fab fa-facebook-f"></i>
                        </a>
                      </li>
                      @endif
                      @if(App\Models\Socialsetting::find(1)->g_check == 1)
                      <li>
                        <a href="{{ route('social-provider','google') }}">
                          <i class="fab fa-google-plus-g"></i>
                        </a>
                      </li>
                      @endif
                    </ul>
                  </div>
                  @endif
                </form>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-reg" role="tabpanel" aria-labelledby="nav-reg-tab">
            <div class="login-area signup-area">
              <div class="header-area">
                <h4 class="title">{{ $langg->lang181 }}</h4>
              </div>
              <div class="login-form signup-form">
                @include('includes.admin.form-login')
                <form class="mregisterform" action="{{route('user-register-submit')}}" method="POST">
                  {{ csrf_field() }}

                  <div class="form-input">
                    <input type="text" class="User Name" name="name" placeholder="{{ $langg->lang182 }}" required="">
                    <i class="icofont-user-alt-5"></i>
                  </div>

                  <div class="form-input">
                    <input type="email" class="User Name" name="email" placeholder="{{ $langg->lang183 }}" required="">
                    <i class="icofont-email"></i>
                  </div>

                  <div class="form-input">
                      <input type="text" class="User Name" name="shop_name" placeholder="Business Name" required="">
                      <i class="icofont-cart"></i>
                  </div>

                  <div class="form-input">
                      <input type="text" class="User Name" name="address" placeholder="Shipping Address" required="">
                      <i class="icofont-location-pin"></i>
                  </div>

                  <div class="form-input">
                      <input type="text" class="User Name" name="shop_address" placeholder="Business Address" required="">
                      <i class="icofont-opencart"></i>
                  </div>

                  <div class="form-input">
                      <input type="text" class="User Name" name="web_address" placeholder="Web Address" required="">
                      <i class="icofont-opencart"></i>
                  </div>
                  <div class="form-input">
                    <input type="text" class="User Name" name="phone" placeholder="Contact number" required="">
                    <i class="icofont-phone"></i>
                  </div>


                  <div class="form-input">
                    <input type="text" class="User Name" name="shop_number" placeholder="Business Number" required="">
                    <i class="icofont-shopping-cart"></i>
                  </div>


                <div class="form-input">
                    <input type="text" class="User Name" name="city" placeholder="City" required="">
                    <i class="icofont-ui-cart"></i>
                </div>

                <div class="form-input">
                    <input type="text" class="User Name" name="zip" placeholder="ZIP" required="">
                    <i class="icofont-ui-cart"></i>
                </div>
                <div class="form-input">
                  <span>Country :</span>
                    <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <option value="">{{ $langg->lang157 }}</option>
                        @foreach (DB::table('countries')->get() as $data)
                            <option value="{{ $data->country_name }}" >
                                {{ $data->country_name }}
                            </option>   
                         @endforeach
                    </select>
                </div>



                <div class="file-upload-area form-input">
                    <div class="upload-file">
                        <span class="User Name">Personal ID :</span>
                        <input type="file" name="personal_id" class="upload">
                    </div>
                </div>

                <div class="file-upload-area form-input">
                    <div class="upload-file">
                        <span class="User Name">Tax ID :</span>
                        <input type="file" name="tax_id" class="upload">
                    </div>
                </div>

<input type="hidden" name="vendor"  value="1">

                <div class="form-input">
                    <input type="text" class="User Name" name="number_of_locations" placeholder="Number of locations?" required="">
                    <i class="icofont-ui-cart"></i>
                </div>


                <div class="form-input">
                  <span>How did you hear about us?</span>
                    <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <option value="1">Email</option>
                        <option value="2">Event / Festival</option>
                        <option value="3">Family / Friend</option>
                        <option value="4">Sales Rep</option>
                        <option value="5">Internet Search</option>
                        <option value="6">Magazine Ad</option>
                        <option value="7">Facebook Ad</option>  
                        <option value="8">Social Media</option>
                        <option value="9">Other</option>
                    </select>
                </div>
                <div class="form-input">
                  <span>Estimated monthly accessory spending?</span>
                    <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <option value="1">$200 - $1,000</option>
                        <option value="2">$1,001 - $2,500</option>
                        <option value="3">$2,501 - $5,000</option>
                        <option value="4">$5,001 - $7,500</option>
                        <option value="5">$7,501 - $10,000</option>
                        <option value="6">$10,001 - $15,000</option>
                        <option value="7">$15,000 - $20,000</option>
                        <option value="8">$20,001 - $30,000</option>
                        <option value="9">$30,001 - $50,000</option>
                        <option value="10">$50,001 - $75,000</option>
                        <option value="11">$75,000+</option>
                    </select>
                </div>


                <div class="form-input">
                  <span>Sales Rep :</span>
                    <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <option value="21995415">Anton Silotang</option>
                        <option value="4059596">Bryan Martinez</option>
                        <option value="7">Drew Batta</option>
                        <option value="16685330">Eddie Wittner</option>
                        <option value="7049632">Henry Robles</option>
                        <option value="22452354">Jeremey Roberts</option>
                        <option value="-5">Rohit Batta</option>
                        <option value="22139811">Sandy Mawikere</option>
                        <option value="9">Sean Batta</option>
                        <option value="22503872">Will Ompi</option>
                    </select>
                </div>

                <div class="form-input">
                  <span>Business Type :</span>
                    <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <option value="5">Carrier Stores</option>
                        <option value="19">Flea Market / Swap Meet / Trade Shows</option>
                        <option value="6">International</option>
                        <option value="4">Online Seller (ex: Amazon, eBay, ecommerce website)</option>
                        <option value="17">Other</option>
                        <option value="1">Repair Shops &amp; Accessories</option>
                        <option value="2">Retailer (ex: Mall Cart, Kiosk, Storefront)</option>
                        <option value="3">Wholesale and Distribution</option>
                    </select>
                </div>

                <div class="form-input">
                  <span>Business Type :</span>
                    <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <option value="1">AT&amp;T</option>
                        <option value="7">Boost Mobile</option>
                        <option value="6">Cricket</option>
                        <option value="5">Metro by T-Mobile</option>
                        <option value="11">Others</option>
                        <option value="8">Sprint</option>
                        <option value="4">StraightTalk</option>
                        <option value="3">T-Mobile</option>
                        <option value="13">TotalWireless</option>
                        <option value="12">TracFone</option>
                        <option value="9">US Cellular</option>
                        <option value="2">Verizon</option>
                        <option value="10">Virgin Mobile</option>
                    </select>
                </div>


                  <div class="form-input">
                    <input type="password" class="Password" name="password" placeholder="{{ $langg->lang186 }}"
                      required="">
                    <i class="icofont-ui-password"></i>
                  </div>

                  <div class="form-input">
                    <input type="password" class="Password" name="password_confirmation"
                      placeholder="{{ $langg->lang187 }}" required="">
                    <i class="icofont-ui-password"></i>
                  </div>

                  @if($gs->is_capcha == 1)

                  <ul class="captcha-area">
                    <li>
                      <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i
                          class="fas fa-sync-alt pointer refresh_code "></i></p>
                    </li>
                  </ul>

                  <div class="form-input">
                    <input type="text" class="Password" name="codes" placeholder="{{ $langg->lang51 }}" required="">
                    <i class="icofont-refresh"></i>
                  </div>

                  @endif

                  <input class="mprocessdata" type="hidden" value="{{ $langg->lang188 }}">
                  <button type="submit" class="submit-btn">{{ $langg->lang189 }}</button>

                </form>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
</section>

@endsection