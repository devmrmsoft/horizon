@php
use App\Models\Subcategory;
$filterBrands = DB::table('categories')->where('status',1)->get();

@endphp
<div class="">
  <div class="left-area filter-column" id="Filters">
    <a class="closebtn" href="javascript:void(0)" onclick="closeFilter()"><i class="fas fa-times" aria-hidden="true"></i>
    </a>

    @if ((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) &&
    !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) &&
    !empty(json_decode($childcat->attributes, true))))

    <div class="tags-area">
      <div class="body-area">
        <form id="attrForm" action="{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">
          <ul class="filter">
            <div class="single-filter">
              <div class="mb-2">
                @if (!empty($cat) && !empty(json_decode($cat->attributes, true)))
                @foreach ($cat->attributes as $key => $attr)
                <a class="sub-title" data-toggle="collapse" href="#brands" role="button" aria-expanded="false" aria-controls="brands">
                  <span> {{$attr->name}}</span>
                </a>
                <div class="collapse show" id="brands">
                  <div class="card">
                    @if (!empty($attr->attribute_options))
                    @foreach ($attr->attribute_options as $key => $option)
                    <div class="form-check ml-0 pl-0">
                      <input name="{{$attr->input_name}}[]" class="form-check-input attribute-input" type="checkbox" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}">
                      <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                    </div>
                    @endforeach
                    @endif
                  </div>
                </div>
                @endforeach
                @endif
              </div>
{{-- 
              <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#brands" role="button" aria-expanded="false" aria-controls="brands">
                  <span>Brands</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div class="collapse show" id="brands">
                  <div class="card">
                    @isset($cat->id)
                    @php
                    $getCatModels = Subcategory::where('category_id',$cat->id)->get();
                    @endphp

                    @foreach($getCatModels as $filterBrand)
                    <div class="form-check ml-0 pl-0">
                      <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-{{ $filterBrand->id }}" value="{{ $filterBrand->id }}" onchange='brandFilterChange(this);'>
                      <label class="form-check-label" for="filter_brands-{{ $filterBrand->id }}">{{$filterBrand->name}} : Test</label>
                    </div>
                    @endforeach
                    @endisset
                  </div>
                </div>
              </div> --}}

       {{--        <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#model" role="button" aria-expanded="false" aria-controls="model">
                  <span> Model</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div id="model" class="collapse show" id="model">
                  <div class="card">
                    <div class="form-check ml-0 pl-0 " id="model">
                    </div>
                    @if (!empty($subcat) && !empty(json_decode($subcat->attributes, true)))
                    @foreach ($subcat->attributes as $key => $attr)
                  </div>
                </div>
              </div>

              <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#model" role="button" aria-expanded="false" aria-controls="model">
                  <span> {{$attr->name}}</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div id="model" class="collapse show" id="model">
                  <div class="card">
                    @if (!empty($attr->attribute_options))
                    @foreach ($attr->attribute_options as $key => $option)
                    <div class="form-check  ml-0 pl-0">
                      <input name="{{$attr->input_name}}[]" on class="form-check-input attribute-input" type="checkbox" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}">
                      <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                    </div>
                    @endforeach
                    @endif
                    @endforeach
                    @endif

                    @if (!empty($childcat) && !empty(json_decode($childcat->attributes, true)))
                    @foreach ($childcat->attributes as $key => $attr)
                  </div>
                </div>
              </div>

              <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#model" role="button" aria-expanded="false" aria-controls="model">
                  <span> {{$attr->name}}</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div id="model" class="collapse show" id="model">
                  <div class="card">
                    @if (!empty($attr->attribute_options))
                    @foreach ($attr->attribute_options as $key => $option)
                    <div class="form-check  ml-0 pl-0">
                      <input name="{{$attr->input_name}}[]" class="form-check-input attribute-input" type="checkbox" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}">
                      <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                    </div>
                    @endforeach
                    @endif
                    @endforeach
                    @endif
                  </div>
                </div>
              </div> --}}
            </div>
          </ul>
        </form>
      </div>
    </div>
    @else

    <div class="tags-area">
      <div class="body-area">
        <form id="attrForm" action="{{route('front.devices', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">
          <ul class="filter">
            <div class="single-filter">
        {{--       <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#brands" role="button" aria-expanded="false" aria-controls="brands">
                  <span>Brands</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div class="collapse show" id="brands">
                  <div class="card">
                    @php
                    if(isset($cat->id)){
                    $getCatModels = Subcategory::where('category_id',$cat->id)->get();
                    @endphp
                    @php } else{ @endphp
                    @isset($deviceCats)
                    @foreach($deviceCats as $filterBrand)
                    <div class="form-check ml-0 pl-0">
                      <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-{{ $filterBrand->id }}" value="{{ $filterBrand->id }}" onchange='brandFilterChange(this);'>
                      <label class="form-check-label" for="filter_brands-{{ $filterBrand->id }}">{{$filterBrand->name}}</label>
                    </div>
                    @endforeach
                    @endisset
                    @php } @endphp
                  </div>
                </div>
              </div> --}}
              <!-- Brands Filter -->

          {{--     <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#model" role="button" aria-expanded="false" aria-controls="model">
                  <span> Model</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div id="model" class="collapse show" id="model">
                  <div class="card">
                    @isset($cat->id)
                    @php
                    $getCatModels = Subcategory::where('category_id',$cat->id)->get();
                    if(isset($subcat->id)){
                    }else{
                    @endphp

                    @foreach($getCatModels as $filterBrand)
                    <div class="form-check ml-0 pl-0">
                      <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-{{ $filterBrand->id }}" value="{{ $filterBrand->id }}" onchange='brandFilterChange(this);'>
                      <label class="form-check-label" for="filter_brands-{{ $filterBrand->id }}">{{$filterBrand->name}}</label>
                    </div>
                    @endforeach
                    @php } @endphp
                    @endisset
                  </div>
                </div>
              </div> --}}
              <!-- Model Filter -->
            @if(strpos(Request::url(), 'carrier')== false)
              <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#carriers" role="button" aria-expanded="false" aria-controls="carriers">
                  <span> Carriers</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div class="collapse show" id="carriers">
                  <div class="card">
                    @isset($deviceCarriers)
                    @foreach($deviceCarriers as $filterBrand)
                    <div class="form-check ml-0 pl-0">
                      <input name="filter_device_carriers[]" class="form-check-input attribute-input filter_device_carriers" type="checkbox" id="filter_device_carriers-{{ $filterBrand->id }}" value="{{ $filterBrand->id }}">
                      <label class="form-check-label" for="filter_device_carriers-{{ $filterBrand->id }}">{{$filterBrand->name}}</label>
                    </div>
                    @endforeach
                    @endisset
                  </div>
                </div>
              </div>
              @endif
              <!-- Carriers Filter -->

              <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#storage" role="button" aria-expanded="false" aria-controls="storage">
                  <span> Storage</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div class="collapse show" id="storage">
                  <div class="card">
                    <?php
                    $storage  = 8;
                    for ($i = 0; $i < 7; $i++) {
                    ?>
                      <div class="form-check ml-0 pl-0">
                        <input name="filter_device_storage[]" class="form-check-input attribute-input filter_device_storage" type="checkbox" id="filter_device_storage-{{ $storage }}" value="{{ $storage }}">
                        <label class="form-check-label" for="filter_device_storage-{{ $storage }}">{{ $storage }} GB</label>
                      </div>
                    <?php
                      $storage  +=  $storage;
                    }
                    ?>
                  </div>
                </div>
              </div>
              <!-- Storage Filter -->

              <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#ph-color" role="button" aria-expanded="false" aria-controls="ph-color">
                  <span>Phone Color</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div class="collapse show" id="ph-color">
                  <div class="card">
                    <?php
                    $colors  = [0 => 'Red', 1 => 'Blue', 2 => 'Black', 3 => 'Silver'];
                    for ($i = 0; $i < count($colors); $i++) {

                    ?>
                      <div class="form-check ml-0 pl-0">
                        <input name="filter_device_color[]" class="form-check-input attribute-input" type="checkbox" id="filter_device_color-{{ $i }}" value="{{ $colors[$i] }}">
                        <label class="form-check-label" for="filter_device_color-{{ $i }}">{{
                                                $colors[$i] }}</label>
                      </div>
                    <?php
                      $storage  +=  $storage;
                    }
                    ?>
                  </div>
                </div>
              </div>
              <!-- Phone Color Filter -->

              <div class="mb-2">
                <a class="sub-title" data-toggle="collapse" href="#ph-condition" role="button" aria-expanded="false" aria-controls="ph-condition">
                  <span>Phone Conditions</span>
                  <i class="fas fa-caret-down"></i>
                </a>
                <div class="collapse show" id="ph-condition">
                  <div class="card">
                    <?php
                    $conditions  = [1 => 'A', 2 => 'A-', 3 => 'B', 4 => 'C', 5 => 'A/B'];
                    for ($i = 1; $i <= count($conditions); $i++) {
                    ?>
                      <div class="form-check ml-0 pl-0">
                        <input name="filter_device_conditions[]" class="form-check-input attribute-input" type="checkbox" id="filter_device_conditions-{{ $i }}" value="{{ $i }}">
                        <label class="form-check-label" for="filter_device_conditions-{{ $i }}">{{
                                              $conditions[$i] }}</label>
                      </div>
                    <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
              <!-- Phone Conditions Filter -->
            </div>
          </ul>
        </form>
      </div>
    </div>
    @endif

    <div class="filter-result-area">
      <div class="mb-0">
        <a class="sub-title" data-toggle="collapse" href="#price-filter" role="button" aria-expanded="false" aria-controls="price-filter">
          {{$langg->lang61}}
          <i class="fas fa-caret-down"></i>
        </a>
        <div class="collapse show" id="price-filter">
          <div class="card">
            <form id="catalogForm" action="{{route('front.accessories', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')]) }}" method="GET">
              @if (!empty(request()->input('search')))
              <input type="hidden" name="search" value="{{ request()->input('search') }}">
              @endif
              @if (!empty(request()->input('sort')))
              <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
              @endif
              <div class="price-range-block">
                <div id="slider-range" class="range-bar price-filter-range" name="rangeInput"></div>
                <p class="range-value livecount">
                  <input type="text" id="amount" readonly>
                  <input type="text" id="min_price" name="min" value="100" hidden>
                  <input type="text" id="max_price" name="max" value="2500" hidden>
                </p>
              </div>
              {{-- <button class="filter-btn" type="submit">{{$langg->lang58}}</button> --}}
            </form>
          </div>
        </div>
      </div>
    </div>

    @if(!isset($vendor))

    {{-- <div class="tags-area">
            <div class="header-area">
                <h4 class="title">
                    {{$langg->lang63}}
    </h4>
  </div>
  <div class="body-area">
    <ul class="taglist">
      @foreach(App\Models\Product::showTags() as $tag)
      @if(!empty($tag))
      <li>
        <a class="{{ isset($tags) ? ($tag == $tags ? 'active' : '') : ''}}" href="{{ route('front.tag',$tag) }}">
          {{ $tag }}
        </a>
      </li>
      @endif
      @endforeach
    </ul>
  </div>
</div> --}}

@else

<div class="service-center">
  <div class="header-area">
    <h4 class="title">
      {{ $langg->lang227 }}
    </h4>
  </div>
  <div class="body-area">
    <ul class="list">
      <li>
        <a href="javascript:;" data-toggle="modal" data-target="{{ Auth::guard('web')->check() ? '#vendorform1' : '#comment-log-reg' }}">
          <i class="icofont-email"></i> <span class="service-text">{{ $langg->lang228 }}</span>
        </a>
      </li>
      <li>
        <a href="tel:+{{$vendor->shop_number}}">
          <i class="icofont-phone"></i> <span class="service-text">{{$vendor->shop_number}}</span>
        </a>
      </li>
    </ul>
    <!-- Modal -->
  </div>

  <div class="footer-area">
    <p class="title">
      {{ $langg->lang229 }}
    </p>
    <ul class="list">
      @if($vendor->f_check != 0)
      <li><a href="{{$vendor->f_url}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
      @endif
      @if($vendor->g_check != 0)
      <li><a href="{{$vendor->g_url}}" target="_blank"><i class="fab fa-google"></i></a></li>
      @endif
      @if($vendor->t_check != 0)
      <li><a href="{{$vendor->t_url}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
      @endif
      @if($vendor->l_check != 0)
      <li><a href="{{$vendor->l_url}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
      @endif
    </ul>
  </div>
</div>

@endif

</div>
</div>

{{-- @section('scripts') --}}

{{-- <script>
  function openFilter() {
    document.getElementById("Filters").style.width = "300px";
  }

  function closeFilter() {
    document.getElementById("Filters").style.width = "0";
  }
</script> --}}

{{-- @endsection --}}