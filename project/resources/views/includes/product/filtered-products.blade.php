      @if (count($prods) > 0)
      @foreach ($prods as $key => $prod)
      <div class="col-lg-3 col-md-3 col-6 remove-padding">
        <div class="productTab">
          <div class="productTab2">
            <a href="{{ route('front.product', $prod->slug) }}"><img src="{{ $prod->photo ? asset('assets/images/products/'.$prod->photo):asset('assets/images/noimage.png') }}" class="img-fluid"></a>
          </div>
          <div class="productTab1">
            <a href="{{ route('front.product', $prod->slug) }}">{{substr($prod->showName(), 0 , 45)}} @if(strlen($prod->showName()) > 50)... @endif</a>
          </div>
          <div class="sku-upc">
            <p>sku: {{ $prod->sku }}</p>
            <p>upc/ean: {{ $prod->upc ?? 'Null' }}</p>
          </div>
          {{-- @auth --}}
          <div class="pricing">
            <p>MSRP: {{ $prod->msrp ?? 'Null' }}</p>
            <p>Your Price: {{ $prod->showPrice($prod->id) }}</p>
            <p>Master Case Qty: {{ $prod->master_case ?? 'Null' }}</p>
          </div>
          <div class="available d-flex justify-content-start align-items-center">
            <p class="mr-3">Available: {{ $prod->stock }}</p>
{{-- 
             @if($prod->product_type != "affiliate")
                      <li class="d-block count {{ $prod->type == 'Physical' ? '' : 'd-none' }}">
                        <div class="qty">
                          <ul>
                            <li>
                              <span class="qtminus">
                                <i class="icofont-minus"></i>
                              </span>
                            </li>
                            <li>
                              <span class="qttotal">1</span>
                            </li>
                            <li>
                              <span class="qtplus">
                                <i class="icofont-plus"></i>
                              </span>
                            </li>
                          </ul>
                        </div>
                      </li>
                      @endif --}}

            <div class="handle-counter" id="handleCounter">
              <a href="javascript:void(0)" class="counter-minus additem">-</a>
              <input type="text" class="counter-total" readonly value="0" min="0">
              <a href="javascript:void(0)" class="counter-plus additem">+</a>
            </div>



          </div>
                   <div class="productTab3 text-center">
                        {{-- @if(!empty($prod->size)) --}}

                  {{-- <input type="hidden" id="stock" value="{{ $prod->size_qty[0] }}"> --}}
                  {{-- @else --}}
                  @php
                  $stck = (string)$prod->stock;
                  @endphp
                  @if($stck != null)
                  <input type="hidden" id="stock" value="{{ $stck }}">
                  @elseif($prod->type != 'Physical')
                  <input type="hidden" id="stock" value="0">
                  @else
                  <input type="hidden" id="stock" value="">
                  @endif

                  {{-- @endif --}}
                  
                  <input type="hidden" id="actual_retail_price" value="{{ trim($prod->showPrice($prod->id),'$') }}">
                  <input type="hidden" id="product_price" value="{{ $prod->showPrice($prod->id) }}">
                  {{-- <input type="hidden" id="product_price" value="{{ round($prod->vendorPrice($prod->id) * $curr->value,2) }}"> --}}

                  <input type="hidden" id="product_id" value="{{ $prod->id }}">
                  {{-- <input type="hidden" id="curr_pos" value="{{ $gs->currency_format }}"> --}}
                  {{-- <input type="hidden" id="curr_sign" value="{{ $curr->sign }}"> --}}
            {{-- <a tabindex="0" data-toggle="tooltip" title="Add to Cart!" class="add-to-cart add-to-cart-btn" data-href="{{ route('product.cart.add',$prod->id) }}"> --}}
              {{-- <i class="fas fa-shopping-cart"></i> --}}
              {{-- Add to Cart --}}

            {{-- </a> --}}
                  </div>
          {{-- @endauth --}}
          @guest
          <div class="notice">
            <p>horizon PRODUCTS CANNOT BE SOLD TO ONLINE RESELLERS</p>
          </div>
          @endguest
        </div>
      </div>
      @endforeach
      <div class="col-lg-12">
        <div class="page-center mt-5">
          {!! $prods->appends(['search' => request()->input('search')])->links() !!}
        </div>
      </div>
      @else
      <div class="col-lg-12">
        <div class="page-center">
          <h4 class="text-center">{{ $langg->lang60 }}</h4>
        </div>
      </div>
      @endif


      @if(isset($ajax_check))


      <script type="text/javascript">
        // Tooltip Section


        $('[data-toggle="tooltip"]').tooltip({});
        $('[data-toggle="tooltip"]').on('click', function() {
          $(this).tooltip('hide');
        });




        $('[rel-toggle="tooltip"]').tooltip();

        $('[rel-toggle="tooltip"]').on('click', function() {
          $(this).tooltip('hide');
        });


        // Tooltip Section Ends
      </script>

      @endif