						<div class="item-filter">
                            <div class="sortingFilter">
                                <a id="filter-icon" href="javascript:void(0)" onclick="openFilter()"><i class="fas fa-filter" aria-hidden="true"></i></a>
    							<a id="cross-icon" class="closebtn" href="javascript:void(0)" onclick="closeFilter()"><i class="fas fa-times" aria-hidden="true"></i></a>
                            </div>
							<ul class="filter-list">
								<li class="item-short-area">
										<p>{{$langg->lang64}} :</p>
										<select id="sortby" name="sort" class="short-item">
	                    <option value="date_desc">{{$langg->lang65}}</option>
	                    <option value="date_asc">{{$langg->lang66}}</option>
	                    <option value="price_asc">{{$langg->lang67}}</option>
	                    <option value="price_desc">{{$langg->lang68}}</option>
										</select>
								</li>
							</ul>
						</div>
