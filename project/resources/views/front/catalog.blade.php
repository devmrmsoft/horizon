@extends('layouts.front')
@section('content')

<!-- Breadcrumb Area Start -->
	<div class="breadcrumb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<ul class="pages">
			            <li>
			            	<a href="{{route('front.index')}}">{{ $langg->lang17 }}</a>
			            </li>
			            <li>
			            	<a href="javascript:;">{{ ucfirst($cat_type) }}</a>
			            </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<!-- Breadcrumb Area End -->

<!-- SubCategori Area Start -->
	<section class="sub-categori">
		<div class="container">
			<div class="row">
                <div class="col-md-12">

                    @if ($cat_type == 'devices')
                    <img src="{{asset('assets/images/categories/'.$banner)}}" class="img-fluid"> 
					@foreach($model_years as $key => $value)

                    <div class="model-year">
                        <h6>{{ $key }} Models</h6>
                    </div>

    <div class="owl-carousel categori-brands-model owl-theme">
                       {{--  <div class="item">
                            <div class="brand-picture">
                                <img src="{{asset('assets/images/brands_models/Apple_iPhone_7_Plus.png')}}" class="img-fluid">
                            </div>
                            <p>Apple iPhone SE</p>
                        </div> --}}


						@foreach($value as $item)
						<div class="item">
                           <a title="{{ $item->name }}" href="{{ route('front.devices',['category' => $item->category->slug, 'subcategory' => $item->slug]) }}"><img alt="{{ $item->name }}" src="{{asset('assets/images/categories/'.$item->photo) }}"></a>
                            <p>{{ $item->name }}</p>
                        </div>

						{{-- <li>
							<a title="{{ $item->name }}" href="{{ route('front.devices',['category' => $item->category_id, 'subcategory' => $item->subcategory_id]) }}"><img alt="{{ $item->name }}" src="{{asset('assets/images/thumbnails/'.$item->thumbnail) }}"><h6 class="product-name">{{ $item->name }}</h6></a>
						</li> --}}
						@endforeach
                        
                        
                    </div>



					@endforeach
				@elseif($cat_type == 'carriers')
				<img src="{{asset('assets/front/images/carriers/'.$banner)}}" class="img-fluid"> 
				@foreach($model_years as $key => $value)
					<div class="model-year">
                        <h6>{{ $key }} Models</h6>
                    </div>
                        <div class="owl-carousel categori-brands-model owl-theme">

						@foreach($value as $item)
						<div class="item">
                           <a title="{{ $item->name }}" href="{{ route('front.carrier',['carrier_slug' => $carrier_slug, 'subcategory' => $item->slug, 'category' => $item->category->slug]) }}"><img alt="{{ $item->name }}" src="{{asset('assets/images/categories/'.$item->photo) }}"></a>
                            <p>{{ $item->name }}</p>
                        </div>



					{{-- 	<li>
							<a title="{{ $item->name }}" href="{{ route('front.carrier',['category' => $item->category_id, 'subcategory' => $item->subcategory_id,'carrier_id' => $carrier_id]) }}"><img alt="{{ $item->name }}" src="{{asset('assets/images/thumbnails/'.$item->thumbnail) }}"><h6 class="product-name">{{ $item->name }}</h6></a>
						</li> --}}
						@endforeach
						                    </div>

					@endforeach
				@endif
                   
                  
                </div>
			</div>
		</div>
	</section>
<!-- SubCategori Area End -->
@endsection

@section('scripts')

@endsection
