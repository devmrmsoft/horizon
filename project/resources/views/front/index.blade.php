@extends('layouts.front')

@section('content')
@php
$brands = json_decode( DB::table('categories')->select('id','name','slug','photo')->where('brands',1)->orderBy('id','asc')->take(10)->get());

	$partners = DB::table('partners')->get();
@endphp
	@if($ps->slider == 1)

		@if(count($sliders))
			@include('includes.slider-style')
		@endif
	@endif

	@if($ps->slider == 1)
		<!-- Hero Area Start -->
		<section class="hero-area">
			@if($ps->slider == 1)

				@if(count($sliders))
					<div class="hero-area-slider">
						<div class="slide-progress"></div>
						<div class="intro-carousel">
							@foreach($sliders as $data)
								<div class="intro-content {{$data->position}}" style="background-image: url({{asset('assets/images/sliders/'.$data->photo)}})">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
												<div class="slider-content">
													<!-- layer 1 -->
													<div class="layer-1">
														<h4 style="font-size: {{$data->subtitle_size}}px; color: {{$data->subtitle_color}}" class="subtitle subtitle{{$data->id}}" data-animation="animated {{$data->subtitle_anime}}">{{$data->subtitle_text}}</h4>
														<h2 style="font-size: {{$data->title_size}}px; color: {{$data->title_color}}" class="title title{{$data->id}}" data-animation="animated {{$data->title_anime}}">{{$data->title_text}}</h2>
													</div>
													<!-- layer 2 -->
													<div class="layer-2">
														<p style="font-size: {{$data->details_size}}px; color: {{$data->details_color}}"  class="text text{{$data->id}}" data-animation="animated {{$data->details_anime}}">{{$data->details_text}}</p>
													</div>
													<!-- layer 3 -->
													<div class="layer-3">
														{{--
														<a href="{{$data->link}}" target="_blank" class="mybtn1"><span>{{ $langg->lang25 }} <i class="fas fa-chevron-right"></i></span></a>
														--}}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				@endif

			@endif

		</section>
		<!-- Hero Area End -->
	@endif
    
    <section class="featuredBrand-section">
        <div class="__featuredBrand-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 __featuredBrand">
                        <div class="_featuredBrand">
                            <h1>Featured Brands</h1>                            
                        </div>
                    </div>
                    <div class="col-md-12 pt-3 __featuredBrand">
                        <div class="owl-carousel owl-theme __featuredSlider">
								@foreach($brands as $brand)
								<div class="item">
									<a href="{{ route('front.category',$brand->slug) }}">
										<img src="{{ asset('/assets/images/categories/') }}/{{ $brand->photo }}" alt="" class="brand-navigation-image">
									</a>
								</div>
							@endforeach
								{{-- @foreach($partners as $data)
									<div class="item">
										<a href="{{ route('front.category',$data->link) }}" target="_blank">
											<img src="{{asset('assets/images/partner/'.$data->photo)}}" alt="">
										</a>
									</div>
								@endforeach --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="">
        <div class="product" style="background:#ffffff;">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col">
                        <div class="product00">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
							  <li class="nav-item">
							    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Smart Phones</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="cases-tab" data-toggle="tab" href="#cases" role="tab" aria-controls="profile" aria-selected="false">Cases</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="headphones-tab" data-toggle="tab" href="#headphones" role="tab" aria-controls="contact" aria-selected="false">Headphones</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="chargers-tab" data-toggle="tab" href="#chargers" role="tab" aria-controls="contact" aria-selected="false">Chargers</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="screen-tab" data-toggle="tab" href="#screen" role="tab" aria-controls="contact" aria-selected="false">Screen</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="protectors-tab" data-toggle="tab" href="#protectors" role="tab" aria-controls="contact" aria-selected="false">Protectors</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="cables-tab" data-toggle="tab" href="#cables" role="tab" aria-controls="contact" aria-selected="false">Cables</a>
							  </li>
							</ul>
							<div class="tab-content" id="myTabContent">
						  		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						  			<div class="row">						  				
										@foreach($smart_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="cases" role="tabpanel" aria-labelledby="cases-tab">
									
						  			<div class="row">						  				
										@foreach($cases_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="headphones" role="tabpanel" aria-labelledby="headphones-tab">
									
						  			<div class="row">						  				
										@foreach($headphone_products as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="chargers" role="tabpanel" aria-labelledby="chargers-tab">
									
						  			<div class="row">						  				
										@foreach($chargers_products as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="screen" role="tabpanel" aria-labelledby="screen-tab">
									
						  			<div class="row">						  				
										@foreach($smart_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="protectors" role="tabpanel" aria-labelledby="protectors-tab">
									
						  			<div class="row">						  				
										@foreach($smart_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
							  	<div class="tab-pane fade" id="cables" role="tabpanel" aria-labelledby="cables-tab">
									
						  			<div class="row">						  				
										@foreach($cables_products as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
							</div>
                        </div>
                    </div>
                    <!--Product Column-->
                </div>
                <!--Product Row-->
            </div>
            <!--Product Container-->
        </div>
    </section>
    <!---Featured Brand Section-->
	@if($ps->featured == 1)
    <section class="featuredProduct">
        <div class="container">
            <div class="row">
                <div class="col-md-12 featuredProduct-col">
                    <div class="featuredTitle">
                        <h1>featured products</h1>
                    </div>
                </div>
                <div class="col-md-12 featuredProduct-col">
                    <div class="owl-carousel featuredProduct-Slider owl-theme">
    						@foreach($feature_products as $prod)
    							@include('includes.product.slider-product')
    						@endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!---Featured Product Section-->
	@if($ps->featured_category == 1)

	{{-- Slider buttom Category Start --}}
	{{-- 
	<section class="slider-buttom-category d-none d-md-block">
		<div class="container-fluid">
			<div class="row">
				@foreach($categories->where('is_featured','=',1) as $cat)
					<div class="col-xl-2 col-lg-3 col-md-4 sc-common-padding">
						<a href="{{ route('front.category',$cat->slug) }}" class="single-category">
							<div class="left">
								<h5 class="title">
									{{ $cat->name }}
								</h5>
								<p class="count">
									{{ count($cat->products) }} {{ $langg->lang4 }}
								</p>
							</div>
							<div class="right">
								<img src="{{asset('assets/images/categories/'.$cat->image) }}" alt="">
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</section>
	--}}
	{{-- Slider buttom banner End --}}

	@endif
{{--
	@if($ps->featured == 1)
		<!-- Trending Item Area Start -->
		<section  class="trending">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 remove-padding">
						<div class="section-top">
							<h2 class="section-title">
								{{ $langg->lang26 }}
							</h2>
							<!--  <a href="#" class="link">View All</a>  -->
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 remove-padding">
						<div class="trending-item-slider">
							@foreach($feature_products as $prod)
								@include('includes.product.slider-product')
							@endforeach
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- Tranding Item Area End -->
	@endif
--}}
	@if($ps->small_banner == 1)

		<!-- Banner Area One Start -->
		<section class="banner-section">
			<div class="container">
				@foreach($top_small_banners->chunk(2) as $chunk)
					<div class="row">
						@foreach($chunk as $img)
							<div class="col-lg-6 remove-padding">
								<div class="left">
									<a class="banner-effect" href="{{ $img->link }}" target="_blank">
										<img src="{{asset('assets/images/banners/'.$img->photo)}}" alt="">
									</a>
								</div>
							</div>
						@endforeach
					</div>
				@endforeach
			</div>
		</section>
		<!-- Banner Area One Start -->
	@endif

	<section id="extraData">
		<div class="text-center">
			<img src="{{asset('assets/images/'.$gs->loader)}}">
		</div>
	</section>


@endsection

@section('scripts')
	<script>
        $(window).on('load',function() {

            setTimeout(function(){

                $('#extraData').load('{{route('front.extraIndex')}}');

            }, 500);
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-prev').text('<');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-prev').addClass('adjust-arrow');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-next').text('>');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-next').addClass('adjust-arrow');
        });


	</script>
@endsection