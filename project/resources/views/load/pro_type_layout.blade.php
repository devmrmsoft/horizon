<select id="pro_type" name="pro_type_id[]" required="">
	<option value="">{{ __('Select Type') }}</option>
	@foreach($brands as $cat)
	<option data-href="{{ route('admin-catTypeChild-load',$cat->id) }}"
		value="{{ $cat->id }}" @isset($lastid) @if($lastid == $cat->id) selected @endif @endisset>{{$cat->name}}</option>
	@endforeach
	<option value="add-new-main-cat">Add New</option>
</select>