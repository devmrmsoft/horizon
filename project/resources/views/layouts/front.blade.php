<?php
use App\accessoryBrand as Brands;
use App\accessoryType as AType;
use App\Models\Category;
use App\Models\Subcategory as DeviceModel;

$brandsDropDown =   Brands::where('status',1)->get();

$devices = json_decode( DB::table('categories')->select('id','name','slug','photo')->where('devices',1)->orderBy('id','asc')->take(8)->get());
$phoneDeviceBrands  =   Category::where('status', 1)->orderBy('id','desc')->take(8)->get();
$brands = json_decode( DB::table('categories')->select('id','name','slug','photo')->where('brands',1)->orderBy('id','asc')->take(10)->get());
$carriers   =   DB::table('carriers')->select('id','name','status','slug','photo')->orderBy('id','asc')->take(8)->get();

$accessories = json_decode(DB::table('categories')->select('id','name','slug')->where('accessories',1)->orderBy('id','asc')->take(8)->get());
$arrivals = json_decode(DB::table('categories')->select('id','name','slug')->where('arrivals',1)->orderBy('id','asc')->take(8)->get());

$cat_type_children = DB::table('cat_type_children')->orderBy('id','asc')->get(); 
$img_array['0']="1";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(isset($page->meta_tag) && isset($page->meta_description))
        <meta name="keywords" content="{{ $page->meta_tag }}">
        <meta name="description" content="{{ $page->meta_description }}">
        <title>{{$gs->title}}</title>
    @elseif(isset($blog->meta_tag) && isset($blog->meta_description))
        <meta name="keywords" content="{{ $blog->meta_tag }}">
        <meta name="description" content="{{ $blog->meta_description }}">
        <title>{{$gs->title}}</title>
    @elseif(isset($productt))
        <meta name="keywords" content="{{ !empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): '' }}">
        <meta name="description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}">
        <meta property="og:title" content="{{$productt->name}}" />
        <meta property="og:description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}" />
        <meta property="og:image" content="{{asset('assets/images/thumbnails/'.$productt->thumbnail)}}" />
        <meta name="author" content="GeniusOcean">
        <title>{{substr($productt->name, 0,11)."-"}}{{$gs->title}}</title>
    @else
        <meta name="keywords" content="{{ $seo->meta_keys }}">
        <meta name="author" content="AurangZeb">
        <title>{{$gs->title}}</title>
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- favicon -->
    <link rel="icon"  type="image/x-icon" href="{{asset('assets/images/'.$gs->favicon)}}"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


@if($langg->rtl == "1")

    <!-- stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/front/css/rtl/all.css')}}">

    <!--Updated CSS-->
    <link rel="stylesheet" href="{{ asset('assets/front/css/rtl/styles.php?color='.str_replace('#','',$gs->colors).'&amp;'.'header_color='.str_replace('#','',$gs->header_color).'&amp;'.'footer_color='.str_replace('#','',$gs->footer_color).'&amp;'.'copyright_color='.str_replace('#','',$gs->copyright_color).'&amp;'.'menu_color='.str_replace('#','',$gs->menu_color).'&amp;'.'menu_hover_color='.str_replace('#','',$gs->menu_hover_color)) }}">

@else

    <!-- stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/front/css/all.css')}}">

    <!--Updated CSS-->
    <link rel="stylesheet" href="{{ asset('assets/front/css/styles.php?color='.str_replace('#','',$gs->colors).'&amp;'.'header_color='.str_replace('#','',$gs->header_color).'&amp;'.'footer_color='.str_replace('#','',$gs->footer_color).'&amp;'.'copyright_color='.str_replace('#','',$gs->copyright_color).'&amp;'.'menu_color='.str_replace('#','',$gs->menu_color).'&amp;'.'menu_hover_color='.str_replace('#','',$gs->menu_hover_color)) }}">

@endif



    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/assets/StyleSheet/Bootstrap4/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/assets/StyleSheet/Bootstrap4/owl.theme.default.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/assets/StyleSheet/Bootstrap4/animated.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/modified/style-modified.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/assets/StyleSheet/Bootstrap4/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/assets/fonts/impact.ttf')}}">



    @yield('styles')

</head>

<body>

@if($gs->is_loader == 1)
    <div class="preloader" id="preloader" style="background: url({{asset('assets/images/'.$gs->loader)}}) no-repeat scroll center center #FFF;"></div>
@endif

@if($gs->is_popup== 1)

@if(isset($visited))
    <div style="display:none">
        <img src="{{asset('assets/images/'.$gs->popup_background)}}">
    </div>

    <!--  Starting of subscribe-pre-loader Area   -->
    <div class="subscribe-preloader-wrap" id="subscriptionForm" style="display: none;">
        <div class="subscribePreloader__thumb" style="background-image: url({{asset('assets/images/'.$gs->popup_background)}});">
            <span class="preload-close"><i class="fas fa-times"></i></span>
            <div class="subscribePreloader__text text-center">
                <h1>{{$gs->popup_title}}</h1>
                <p>{{$gs->popup_text}}</p>
                <form action="{{route('front.subscribe')}}" id="subscribeform" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="email" name="email"  placeholder="{{ $langg->lang741 }}" required="">
                        <button id="sub-btn" type="submit">{{ $langg->lang742 }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--  Ending of subscribe-pre-loader Area   -->

@endif

@endif
    <section class="sticky-header">
            <div class="top-header logo-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 top-header-col d-flex align-items-center justify-content-start">
                            <div class="help-content">
                                <a href="#"><i class="fas fa-phone-alt"></i></a>
                                <a href="#">Help | 713 988 6565</a>
                            </div>
                        </div>
                        <div class="col-md-8 top-header-col d-flex align-items-end justify-content-end order-lg-last">
                            <div class="top-nav-bar helpful-links">
                                <ul class="nav helpful-links-inner">
                                    
                                            @if(!Auth::guard('web')->check())
                                            <li class="login">
                                                <a href="{{ route('user.login') }}" class="sign-log">
                                                    <div class="links">
                                                        <span class="sign-in">{{ $langg->lang12 }}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            @else
                                            <li class="login">
                                                @foreach($devices as $dev)
                                                <a href="{{ url('devices') }}" class="sign-log">
                                                    @endforeach
                                                    <div class="links">
                                                        <span class="join">On Sale</span>
                                                    </div>
                                                </a>
                                            </li>
                                                <li class="profilearea my-dropdown">
                                                    <a href="{{ url('/user/dashboard') }}" id="profile-icon" class="profile carticon">
                                                        <span class="text">
                                                            <i class="far fa-user"></i> {{ $langg->lang11 }} 
                                                            <i class="sign-log"></i>
                                                        </span>
                                                    </a>
                                                    <div class="my-dropdown-menu profile-dropdown">
                                                        <ul class="profile-links">
                                                            <li>
                                                                <a href="{{ route('user-dashboard') }}"><i class="fas fa-angle-double-right"></i> {{ $langg->lang221 }}</a>
                                                            </li>
                                                            @if(Auth::user()->IsVendor())
                                                            <li>
                                                                <a href="{{ route('vendor-dashboard') }}"><i class="fas fa-angle-double-right"></i> {{ $langg->lang222 }}</a>
                                                            </li>
                                                            @endif
        
                                                            <li>
                                                                <a href="{{ route('user-profile') }}"><i class="fas fa-angle-double-right"></i> {{ $langg->lang205 }}</a>
                                                            </li>
        
                                                            <li>
                                                                <a href="{{ route('user-logout') }}"><i class="fas fa-angle-double-right"></i> {{ $langg->lang223 }}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            @endif
        
                                    @if($gs->reg_vendor == 1)
                                        <li class="nav-item">
                                        @if(Auth::check())
                                            @if(Auth::guard('web')->user()->is_vendor == 2)
                                                <a href="{{ route('vendor-dashboard') }}" class="sell-btn">Retailer</a>
                                            @else
                                                <a href="{{ route('user-package') }}" class="sell-btn">Retailer</a>
                                            @endif
                                        </li>
                                        @else
                                        <li class="nav-item">
                                            <a href="{{ route('retailer-register') }}" class="sell-btn">Retailer</a>
                                        </li>
                                        @endif
                                    @endif
                                 {{--    <li class="nav-item wishlist"  data-toggle="tooltip" data-placement="top" title="{{ $langg->lang9 }}">
                                        @if(Auth::guard('web')->check())
                                            <a href="{{ route('user-wishlists') }}" class="wish">
                                                <i class="far fa-heart"></i>
                                                <span id="wishlist-count">{{ Auth::user()->wishlistCount() }}</span>
                                            </a>
                                        @else
                                            <a href="javascript:;" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" class="wish">
                                                <i class="far fa-heart"></i>
                                                <span id="wishlist-count">0</span>
                                            </a>
                                        @endif
                                    </li> 
                                    <li class="nav-item compare"  data-toggle="tooltip" data-placement="top" title="{{ $langg->lang10 }}">
                                        <a href="{{ route('product.compare') }}" class="wish compare-product">
                                                <i class="fas fa-exchange-alt"></i>
                                                <span id="compare-count">{{ Session::has('compare') ? count(Session::get('compare')->items) : '0' }}</span>
                                            <!-- <div class="icon">
                                            </div> -->
                                        </a>
                                    </li> --}}
        
                                    <li class="my-dropdown"  data-toggle="" data-placement="top" title="{{ $langg->lang3 }}">
                                        <a href="javascript:;" class="cart carticon">
                                            <div class="icon">
                                                <i class="icofont-cart"></i>
                                                <span class="cart-quantity" id="cart-count">{{ Session::has('cart') ? count(Session::get('cart')->items) : '0' }}</span>
                                            </div>
        
                                        </a>
                                        <div class="my-dropdown-menu" id="cart-items">
                                            @include('load.cart')
                                        </div>
                                    </li>
        
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Top Header Area End -->
    
        <header id="header-nav" class="main-header header sticky-top">
        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand web-logo" href="{{ route('front.index') }}""><img class="img-fluid" src="{{asset('assets/images/'.$gs->logo)}}" width="130" height="auto"></a>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto navigation">
                                    
                                     <!-- CATEGORY -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="{{ url('categories') }}" id="navbarDropdown"> Categories <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <div style="display:flex;">
                                                <ul class="nav sub-nav-lvl2 flex-column" style="width:80%;height:200px;padding-top:10px;">
                                                    @foreach($cat_type_children as $cat)
                                                    <li class="nav-item sub-cat-items-lvl2">
                                                        <?php 
                                                            $img_array[$cat->id]=$cat->image;
                                                        ?>
                                                        <a id="{{$cat->id}}" href="{{ route('front.accessories',$cat->slug) }}" class="sub-cat-link-lvl2 cat-img">{{$cat->name}}</a>
                                                        
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                <ul class="sub-nav-down">
                                                    <li class="nav-item">
                                                        <ul class="nav sub-nav-lvl2 flex-column">
                                                            <img id="cat-side-img" src="//horizonwirelesstx.com/assets/images/categories/1602603895cases.png" class="sub-new-img">
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="{{ url('categories') }}" type="button">see all Categories</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    
                                    
                                    <!-- DEVICES -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="{{route('front.catalog')}}" id="navbarDropdown"> Devices <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <ul class="nav sub-nav">
                                                @foreach($devices as $dev)
                                                <li class="nav-item sub-items">
                                                 {{--    @php
                                                        dd($dev->slug);
                                                        die;
                                                    @endphp --}}
                                                    <a href="{{route('front.catalog',['cat_type' => 'devices', 'category' => $dev->slug])}}" class="sub-link"><img id="" src="{{ asset('/assets/images/categories/') }}/{{ $dev->photo }}" class="brand-navigation-image"></a>
                                                </li>
                                                {{-- front.catalog --}}
                                                @endforeach
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="{{ url('devices') }}" type="button">see all devices</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    
                                    <!-- BRANDS -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="{{ url('all-brands') }}" id="navbarDropdown"> Brands <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <ul class="nav sub-nav">
                                                @foreach($brands as $brand)
                                                <li class="nav-item sub-items brands-sub-items">
                                                    <a href="{{ route('front.category',$brand->slug) }}">
                                                        <img id="" src="{{ asset('/assets/images/categories/') }}/{{ $brand->photo }}" class="brand-navigation-image">
                                                    </a>
                                                </li>
                                                @endforeach
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="{{ url('all-brands') }}" type="button">see all Brands</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- CARRIERS -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Carriers <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                           <ul class="nav sub-nav">
                                                @foreach($carriers as $carrier)

                                                <li class="nav-item sub-items carrier-sub-items">
                                                    <a href="{{route('front.catalog',['cat_type' => 'carriers', 'category' => $carrier->slug])}}" class="sub-link">
                                                        <img id="" src="{{ asset('/assets/front/images/carriers/') }}/{{ $carrier->photo }}" class="brand-navigation-image"></a>
                                                    
                                                </li>
                                                @endforeach
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="#" type="button">see all carriers</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown listing" style="display:none;">
                                        <a class="nav-link dropdown-toggle list-link" href="{{ route('front.accessories') }}" id="navbarDropdown"> Accessories <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <ul class="nav sub-nav">
                                                @foreach($accessories as $accessory)
                                                <li class="nav-item sub-items">
                                                    <a href="#" class="sub-link">{{$accessory->name}}</a>
                                                    <ul class="nav sub-nav-lvl2 flex-column">
                                                        <?php $subcats_accessories = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug')->where('subcategories.category_id',$accessory->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                        @foreach($subcats_accessories as $sub_accessory)
                                                            <li class="sub-items-lvl2">
                                                                <a href="{{url('/accessory/')}}/{{$accessory->slug}}/{{$sub_accessory->slug}}" class="sub-link-lvl2">{{$sub_accessory->name}}</a>
                                                            </li>
                                                        @endforeach
                                                        <a href="{{url('/accessory/')}}/{{$accessory->slug}}" type="button" class="see-all-btn">see all {{$accessory->name}}</a>
                                                    </ul>
                                                </li>
                                                @endforeach
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="{{ route('front.accessories') }}" type="button">see all Accessory</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item listing" style="display:none;">
                                        <a class="nav-link list-link" href="#">New Arrivals</a>
                                    </li>
                                </ul>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#search"><i class="fas fa-search"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div id="search">
            <button type="button" class="close">×</button>
            <form id="searchForm" class="search-form" action="{{ route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')]) }}" method="GET">
                <input type="search" placeholder="Search Your Product Here" id="prod_name" name="search" value="{{ request()->input('search') }}" autocomplete="off">
                <button type="submit" class="btn"><span>GO</span> <i class="fas fa-angle-double-right"></i></button>
            </form>
        </div>
    </header>
        
        <header id="mobile-nav" class="main-header header sticky-top .d-none .d-sm-flex d-lg-none">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mobile-nav-col">
                        <div class="search-button-1">
                            <a href="#" class="search-toggle-1" data-selector="#mobile-nav"></a>
                        </div>
                        <div id="my-mob-nav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            <ul class="siderbar-content">
                            <a class="nav-link" href="{{ url('categories') }}" data-toggle="collapse" aria-expanded="false" data-target="#Devices" aria-controls="Devices">Devices <i class="fas fa-chevron-down"></i>
                            </a>
                                <div id="Devices" class="collapse submenu">
                                    <ul class="nav flex-column first-submenu">
                                        @foreach($devices as $dev)
                                        <li class="nav-item">
                                            <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model{{$dev->id}}" aria-controls="Model">{{$dev->name}}<i class="fas fa-chevron-down"></i>
                                            </a>
                                            <div id="Model{{$dev->id}}" class="collapse submenu">
                                                <ul class="nav flex-column second-submenu">
                                                    <?php $subcats = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$dev->id)->where('products.acc_type_id','=','')->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                    @foreach($subcats as $sub)
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="/category/{{$dev->slug}}/{{$sub->slug}}" >{{$sub->name}}
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>     
                                        @endforeach
                                    </ul>
                                </div>
                            
                            
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Brands" aria-controls="Brands">Brands <i class="fas fa-chevron-down"></i>
                            </a>
                                <div id="Brands" class="collapse submenu">
                                    <ul class="nav flex-column first-submenu">
                                        @foreach($brands as $brand)
                                        <li class="nav-item">
                                            <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model{{$brand->id}}1" aria-controls="Model">{{$brand->name}}<i class="fas fa-chevron-down"></i>
                                            </a>
                                            <div id="Model{{$brand->id}}1" class="collapse submenu">
                                                <ul class="nav flex-column second-submenu">
                                                    <?php $subcats_brand = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$brand->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                    @foreach($subcats_brand as $sub_brand)
                                                    <li class="nav-item">
                                                       <a class="nav-link" href="/category/{{ $brand->slug }}/{{ $sub_brand->slug }}" >{{$sub_brand->name}}</a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>     
                                        @endforeach
                                    </ul>
                                </div>
                                
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Carriers" aria-controls="Carriers">Carriers <i class="fas fa-chevron-down"></i>
                            </a>
                            <div id="Carriers" class="collapse submenu">
                                    <ul class="nav flex-column first-submenu">
                                        @foreach($carriers as $carrier)
                                        <li class="nav-item">
                                            <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model{{$carrier->id}}12" aria-controls="Model">{{$carrier->name}}<i class="fas fa-chevron-down"></i>
                                            </a>
                                            <div id="Model{{$carrier->id}}12" class="collapse submenu">
                                                <ul class="nav flex-column second-submenu">
                                                    <?php $subcats_carrier = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$carrier->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                    @foreach($subcats_carrier as $sub_carrier)
                                                    <li class="nav-item">
                                                       <a class="nav-link" href="/category/{{ $carrier->slug }}/{{ $sub_carrier->slug }}" >{{$sub_carrier->name}}</a>
                                                       
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>     
                                        @endforeach
                                    </ul>
                                </div>
                            
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Accessories" aria-controls="Accessories">Accessories <i class="fas fa-chevron-down"></i>
                            </a>
                            <div id="Accessories" class="collapse submenu">
                                <ul class="nav flex-column first-submenu">
                                    @foreach($accessories as $accessory)
                                    <li class="nav-item">
                                        <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model{{$accessory->id}}123" aria-controls="Model">{{$accessory->name}}<i class="fas fa-chevron-down"></i>
                                        </a>
                                        <div id="Model{{$accessory->id}}123" class="collapse submenu">
                                            <ul class="nav flex-column second-submenu">
                                                <?php $subcats_accessory = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$accessory->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                @foreach($subcats_accessory as $sub_accessory)
                                                <li class="nav-item">
                                                   <a class="nav-link" href="/category/{{ $accessory->slug }}/{{ $sub_accessory->slug }}" >{{$sub_accessory->name}}</a>
                                                   
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>     
                                    @endforeach
                                </ul>
                            </div>
                            
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Arrivals" aria-controls="Arrivals">New Arrivals <i class="fas fa-chevron-down"></i>
                            </a>
                            <a href="#" class="nav-link">
                                About Us
                            </a>
                        </ul>
                        </div>
                        <span style="font-size:33px;cursor:pointer;display:inline-block;line-height:normal;"
                            onclick="openNav()">&#9776;</span>
                        <div class="site-logo">
                            <a href="{{ route('front.index') }}"><img src="{{asset('assets/images/'.$gs->logo)}}" class="img-fluid" width="135" height="auto"></a>
                        </div>
                        <form action="" class="search-box">
                            <input type="text" class="text search-input" placeholder="Type here to search..." />
                        </form>
                    </div>
                </div>
            </div>
        </header>
        <!---Mobile Responsive Header-->
    </section>

    <!--Main-Menu Area Start-->
    {{--
    <div class="mainmenu-area mainmenu-bb">
        <div class="container">
            <div class="row align-items-center mainmenu-area-innner">
                <div class="col-lg-3 col-md-6 categorimenu-wrapper remove-padding">
                    <!--categorie menu start-->
                    <!--categorie menu end-->
                </div>
                <div class="col-lg-9 col-md-6 mainmenu-wrapper remove-padding">
                    <nav hidden>
                        <div class="nav-header">
                            <button class="toggle-bar"><span class="fa fa-bars"></span></button>
                        </div>
                        <ul class="menu">
                            @if($gs->is_home == 1)
                            <li><a href="{{ route('front.index') }}">{{ $langg->lang17 }}</a></li>
                            @endif
                            <li><a href="{{ route('front.blog') }}">{{ $langg->lang18 }}</a></li>
                            @if($gs->is_faq == 1)
                            <li><a href="{{ route('front.faq') }}">{{ $langg->lang19 }}</a></li>
                            @endif
                            @foreach(DB::table('pages')->where('header','=',1)->get() as $data)
                                <li><a href="{{ route('front.page',$data->slug) }}">{{ $data->title }}</a></li>
                            @endforeach
                            @if($gs->is_contact == 1)
                            <li><a href="{{ route('front.contact') }}">{{ $langg->lang20 }}</a></li>
                            @endif
                            <li>
                                <a href="javascript:;" data-toggle="modal" data-target="#track-order-modal" class="track-btn">{{ $langg->lang16 }}</a>
                            </li>
                        </ul>

                    </nav>
                </div>
            </div>
        </div>
    </div>
    --}}
    <!--Main-Menu Area End-->

@yield('content')

{{--
    <!-- Footer Area Start -->
    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="footer-info-area">
                        <div class="footer-logo">
                            <a href="{{ route('front.index') }}" class="logo-link">
                                <img src="{{asset('assets/images/'.$gs->footer_logo)}}" alt="">
                            </a>
                        </div>
                        <div class="text">
                            <p>
                                    {!! $gs->footer !!}
                            </p>
                        </div>
                    </div>
                    <div class="fotter-social-links">
                        <ul>

                                     @if(App\Models\Socialsetting::find(1)->f_status == 1)
                                      <li>
                                        <a href="{{ App\Models\Socialsetting::find(1)->facebook }}" class="facebook" target="_blank">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                      </li>
                                      @endif

                                      @if(App\Models\Socialsetting::find(1)->g_status == 1)
                                      <li>
                                        <a href="{{ App\Models\Socialsetting::find(1)->gplus }}" class="google-plus" target="_blank">
                                            <i class="fab fa-google-plus-g"></i>
                                        </a>
                                      </li>
                                      @endif

                                      @if(App\Models\Socialsetting::find(1)->t_status == 1)
                                      <li>
                                        <a href="{{ App\Models\Socialsetting::find(1)->twitter }}" class="twitter" target="_blank">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                      </li>
                                      @endif

                                      @if(App\Models\Socialsetting::find(1)->l_status == 1)
                                      <li>
                                        <a href="{{ App\Models\Socialsetting::find(1)->linkedin }}" class="linkedin" target="_blank">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                      </li>
                                      @endif

                                      @if(App\Models\Socialsetting::find(1)->d_status == 1)
                                      <li>
                                        <a href="{{ App\Models\Socialsetting::find(1)->dribble }}" class="dribbble" target="_blank">
                                            <i class="fab fa-dribbble"></i>
                                        </a>
                                      </li>
                                      @endif

                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="footer-widget info-link-widget">
                        <h4 class="title">
                                {{ $langg->lang21 }}
                        </h4>
                        <ul class="link-list">
                            <li>
                                <a href="{{ route('front.index') }}">
                                    <i class="fas fa-angle-double-right"></i>{{ $langg->lang22 }}
                                </a>
                            </li>

                            @foreach(DB::table('pages')->where('footer','=',1)->get() as $data)
                            <li>
                                <a href="{{ route('front.page',$data->slug) }}">
                                    <i class="fas fa-angle-double-right"></i>{{ $data->title }}
                                </a>
                            </li>
                            @endforeach

                            <li>
                                <a href="{{ route('front.contact') }}">
                                    <i class="fas fa-angle-double-right"></i>{{ $langg->lang23 }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="footer-widget recent-post-widget">
                        <h4 class="title">
                            {{ $langg->lang24 }}
                        </h4>
                        <ul class="post-list">
                            @foreach (App\Models\Blog::orderBy('created_at', 'desc')->limit(3)->get() as $blog)
                            <li>
                                <div class="post">
                                  <div class="post-img">
                                    <img style="width: 73px; height: 59px;" src="{{ asset('assets/images/blogs/'.$blog->photo) }}" alt="">
                                  </div>
                                  <div class="post-details">
                                    <a href="{{ route('front.blogshow',$blog->id) }}">
                                        <h4 class="post-title">
                                            {{mb_strlen($blog->title,'utf-8') > 45 ? mb_substr($blog->title,0,45,'utf-8')." .." : $blog->title}}
                                        </h4>
                                    </a>
                                    <p class="date">
                                        {{ date('M d - Y',(strtotime($blog->created_at))) }}
                                    </p>
                                  </div>
                                </div>
                              </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="copy-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                            <div class="content">
                                <div class="content">
                                    <p>{!! $gs->copyright !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->

    <!--Site Footer-->
--}}
    <footer class="siteFooter">
        <div class="siteFooter1">
            <div class="container">
                <div class="row">
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>Support</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a href="#" class="nav-link"><i class="fas fa-phone"></i>
                                        713-988-6565</a></li>
                                <li class="nav-item"><a href="#" class="nav-link"><i class="far fa-envelope"></i>
                                        info@horizonwirelesstx.com</a></li>
                                <li class="nav-item"><a href="#" class="nav-link"><i
                                            class="fas fa-envelope-open-text"></i> Accessibility</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>Shop</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="#">New Arrivals</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Devices</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Carriers</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>information</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="#">About Us</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Contact Us</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">FAQs</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>My Account</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="#">My Account</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Sign In</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">View Cart</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Register</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>Social Links</h1>
                            <a href="#"><i class="fab fa-facebook-square"></i></a>
                            <a href="#"><i class="fab fa-twitter-square"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-youtube"></i></a>
                        </div>
                    </div>
                    <!---Column-->
                </div>
                <!--Row-->
            </div>
            <!--Container-->
        </div>
    </footer>
    <footer class="copyRight-footer">
        <div class="copyRight">
            <div class="container">
                <div class="row">
                    <div class="col-md d-flex align-items-center copyRight-col">
                        <div class="copyRight00">
                            <h4 style="margin-bottom:0;">&copy; 2020 Horizon Wireless, All Rights Reserved.</h4>
                        </div>
                        <div class="copyRight01">
                            <i class="fab fa-cc-paypal" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="Paypal!" style="color: #009CDE;"></i><span
                                class="sr-only">Paypal!</span>
                            <i class="fab fa-cc-visa" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="Visa" style="color: #00579F;"></i><span class="sr-only">Visa</span>
                            <i class="fab fa-cc-mastercard" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="MasterCard" style="color: #d9222a;"></i><span
                                class="sr-only">MasterCard</span>
                            <i class="fab fa-cc-amex" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="AmercianX" style="color: #016CCA;"></i><span
                                class="sr-only">AmercianX</span>
                        </div>
                        <div class="copyRight02">
                            <h4>Powered By</h4>
                            <a href="https://mrm-soft.com/" target="__blank"><img src="{{ asset('assets/front/assets/img/mrm logo-02.png') }}"
                                    width="50"></a>
                        </div>
                    </div>
                    <!--Column-->
                </div>
                <!---Row-->
            </div>
            <!--Container-->
        </div>
    </footer>
    <!--Copy Right Footer-->

    <!-- Back to Top Start -->
    <div class="bottomtotop">
        <i class="fas fa-chevron-right"></i>
    </div>
    <!-- Back to Top End -->

    <!-- LOGIN MODAL -->
    <div class="modal fade" id="comment-log-reg" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title"
        aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <nav class="comment-log-reg-tabmenu">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link login active" id="nav-log-tab1" data-toggle="tab" href="#nav-log1"
                                role="tab" aria-controls="nav-log" aria-selected="true">
                                {{ $langg->lang197 }}
                            </a>
                            <a class="nav-item nav-link" id="nav-reg-tab1" data-toggle="tab" href="#nav-reg1" role="tab"
                                aria-controls="nav-reg" aria-selected="false">
                                {{ $langg->lang198 }}
                            </a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-log1" role="tabpanel"
                            aria-labelledby="nav-log-tab1">
                            <div class="login-area">
                                <div class="header-area">
                                    <h4 class="title">{{ $langg->lang172 }}</h4>
                                </div>
                                <div class="login-form signin-form">
                                    @include('includes.admin.form-login')
                                    <form class="mloginform" action="{{ route('user.login.submit') }}" method="POST">
                                        {{ csrf_field() }}
                                        <div class="form-input">
                                            <input type="email" name="email" placeholder="{{ $langg->lang173 }}"
                                                required="">
                                            <i class="icofont-user-alt-5"></i>
                                        </div>
                                        <div class="form-input">
                                            <input type="password" class="Password" name="password"
                                                placeholder="{{ $langg->lang174 }}" required="">
                                            <i class="icofont-ui-password"></i>
                                        </div>
                                        <div class="form-forgot-pass">
                                            <div class="left">
                                                <input type="checkbox" name="remember" id="mrp"
                                                    {{ old('remember') ? 'checked' : '' }}>
                                                <label for="mrp">{{ $langg->lang175 }}</label>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:;" id="show-forgot">
                                                    {{ $langg->lang176 }}
                                                </a>
                                            </div>
                                        </div>
                                        <input type="hidden" name="modal" value="1">
                                        <input class="mauthdata" type="hidden" value="{{ $langg->lang177 }}">
                                        <button type="submit" class="submit-btn">{{ $langg->lang178 }}</button>
                                        @if(App\Models\Socialsetting::find(1)->f_check == 1 ||
                                        App\Models\Socialsetting::find(1)->g_check == 1)
                                        <div class="social-area">
                                            <h3 class="title">{{ $langg->lang179 }}</h3>
                                            <p class="text">{{ $langg->lang180 }}</p>
                                            <ul class="social-links">
                                                @if(App\Models\Socialsetting::find(1)->f_check == 1)
                                                <li>
                                                    <a href="{{ route('social-provider','facebook') }}">
                                                        <i class="fab fa-facebook-f"></i>
                                                    </a>
                                                </li>
                                                @endif
                                                @if(App\Models\Socialsetting::find(1)->g_check == 1)
                                                <li>
                                                    <a href="{{ route('social-provider','google') }}">
                                                        <i class="fab fa-google-plus-g"></i>
                                                    </a>
                                                </li>
                                                @endif
                                            </ul>
                                        </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-reg1" role="tabpanel" aria-labelledby="nav-reg-tab1">
                            <div class="login-area signup-area">
                                <div class="header-area">
                                    <h4 class="title">{{ $langg->lang181 }}</h4>
                                </div>
                                <div class="login-form signup-form">
                                    @include('includes.admin.form-login')
                                    <form class="mregisterform" action="{{route('user-register-submit')}}"
                                        method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-input">
                                            <input type="text" class="User Name" name="name"
                                                placeholder="{{ $langg->lang182 }}" required="">
                                            <i class="icofont-user-alt-5"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="email" class="User Name" name="email"
                                                placeholder="{{ $langg->lang183 }}" required="">
                                            <i class="icofont-email"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="text" class="User Name" name="phone"
                                                placeholder="{{ $langg->lang184 }}" required="">
                                            <i class="icofont-phone"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="text" class="User Name" name="address"
                                                placeholder="{{ $langg->lang185 }}" required="">
                                            <i class="icofont-location-pin"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="password" class="Password" name="password"
                                                placeholder="{{ $langg->lang186 }}" required="">
                                            <i class="icofont-ui-password"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="password" class="Password" name="password_confirmation"
                                                placeholder="{{ $langg->lang187 }}" required="">
                                            <i class="icofont-ui-password"></i>
                                        </div>


                                        @if($gs->is_capcha == 1)

                                        <ul class="captcha-area">
                                            <li>
                                                <p><img class="codeimg1"
                                                        src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i
                                                        class="fas fa-sync-alt pointer refresh_code "></i></p>
                                            </li>
                                        </ul>

                                        <div class="form-input">
                                            <input type="text" class="Password" name="codes"
                                                placeholder="{{ $langg->lang51 }}" required="">
                                            <i class="icofont-refresh"></i>
                                        </div>


                                        @endif

                                        <input class="mprocessdata" type="hidden" value="{{ $langg->lang188 }}">
                                        <button type="submit" class="submit-btn">{{ $langg->lang189 }}</button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGIN MODAL ENDS -->

    <!-- FORGOT MODAL -->
    <div class="modal fade" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title"
        aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="login-area">
                        <div class="header-area forgot-passwor-area">
                            <h4 class="title">{{ $langg->lang191 }} </h4>
                            <p class="text">{{ $langg->lang192 }} </p>
                        </div>
                        <div class="login-form">
                            @include('includes.admin.form-login')
                            <form id="mforgotform" action="{{route('user-forgot-submit')}}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-input">
                                    <input type="email" name="email" class="User Name"
                                        placeholder="{{ $langg->lang193 }}" required="">
                                    <i class="icofont-user-alt-5"></i>
                                </div>
                                <div class="to-login-page">
                                    <a href="javascript:;" id="show-login">
                                        {{ $langg->lang194 }}
                                    </a>
                                </div>
                                <input class="fauthdata" type="hidden" value="{{ $langg->lang195 }}">
                                <button type="submit" class="submit-btn">{{ $langg->lang196 }}</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- FORGOT MODAL ENDS -->


<!-- VENDOR LOGIN MODAL -->
    <div class="modal fade" id="vendor-login" tabindex="-1" role="dialog" aria-labelledby="vendor-login-Title" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" style="transition: .5s;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <nav class="comment-log-reg-tabmenu">
                    <div class="nav nav-tabs" id="nav-tab1" role="tablist">
                        <a class="nav-item nav-link login active" id="nav-log-tab11" data-toggle="tab" href="#nav-log11" role="tab" aria-controls="nav-log" aria-selected="true">
                            Retailer Login
                        </a>
                        <a class="nav-item nav-link" id="nav-reg-tab11" data-toggle="tab" href="#nav-reg11" role="tab" aria-controls="nav-reg" aria-selected="false">
                            Retailer Register
                        </a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-log11" role="tabpanel" aria-labelledby="nav-log-tab">
                        <div class="login-area">
                          <div class="login-form signin-form">
                                @include('includes.admin.form-login')
                            <form class="mloginform" action="{{ route('user.login.submit') }}" method="POST">
                              {{ csrf_field() }}
                              <div class="form-input">
                                <input type="email" name="email" placeholder="{{ $langg->lang173 }}" required="">
                                <i class="icofont-user-alt-5"></i>
                              </div>
                              <div class="form-input">
                                <input type="password" class="Password" name="password" placeholder="{{ $langg->lang174 }}" required="">
                                <i class="icofont-ui-password"></i>
                              </div>
                              <div class="form-forgot-pass">
                                <div class="left">
                                  <input type="checkbox" name="remember"  id="mrp1" {{ old('remember') ? 'checked' : '' }}>
                                  <label for="mrp1">{{ $langg->lang175 }}</label>
                                </div>
                                <div class="right">
                                  <a href="javascript:;" id="show-forgot1">
                                    {{ $langg->lang176 }}
                                  </a>
                                </div>
                              </div>
                              <input type="hidden" name="modal"  value="1">
                               <input type="hidden" name="vendor"  value="1">
                              <input class="mauthdata" type="hidden"  value="{{ $langg->lang177 }}">
                              <button type="submit" class="submit-btn">{{ $langg->lang178 }}</button>
                                  @if(App\Models\Socialsetting::find(1)->f_check == 1 || App\Models\Socialsetting::find(1)->g_check == 1)
                                  <div class="social-area">
                                      <h3 class="title">{{ $langg->lang179 }}</h3>
                                      <p class="text">{{ $langg->lang180 }}</p>
                                      <ul class="social-links">
                                        @if(App\Models\Socialsetting::find(1)->f_check == 1)
                                        <li>
                                          <a href="{{ route('social-provider','facebook') }}">
                                            <i class="fab fa-facebook-f"></i>
                                          </a>
                                        </li>
                                        @endif
                                        @if(App\Models\Socialsetting::find(1)->g_check == 1)
                                        <li>
                                          <a href="{{ route('social-provider','google') }}">
                                            <i class="fab fa-google-plus-g"></i>
                                          </a>
                                        </li>
                                        @endif
                                      </ul>
                                  </div>
                                  @endif
                            </form>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-reg11" role="tabpanel" aria-labelledby="nav-reg-tab">
                <div class="login-area signup-area">
                    <div class="login-form signup-form">
                       @include('includes.admin.form-login')
                        <form class="mregisterform" action="{{route('user-register-submit')}}" method="POST">
                          {{ csrf_field() }}

                          <div class="row">

                          <div class="col-lg-6">
                            <div class="form-input">
                                <input type="text" class="User Name" name="name" placeholder="{{ $langg->lang182 }}" required="">
                                <i class="icofont-user-alt-5"></i>
                                </div>
                           </div>

                           <div class="col-lg-6">
 <div class="form-input">
                                <input type="email" class="User Name" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                <i class="icofont-email"></i>
                            </div>

                            </div>

                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="shop_name" placeholder="Business Name" required="">
                                    <i class="icofont-cart"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="address" placeholder="Shipping Address" required="">
                                    <i class="icofont-location-pin"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="shop_address" placeholder="Business Address" required="">
                                    <i class="icofont-opencart"></i>
                                </div>
                            </div>

                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="shop_number" placeholder="Business Number" required="">
                                    <i class="icofont-shopping-cart"></i>
                                </div>
                            </div>

                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="phone" placeholder="Contact number" required="">
                                    <i class="icofont-phone"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="city" placeholder="City" required="">
                                    <i class="icofont-ui-cart"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="zip" placeholder="ZIP" required="">
                                    <i class="icofont-ui-cart"></i>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="file-upload-area">
                                    <div class="upload-file">
                                        <input type="file" name="personal_id" class="upload">
                                        <span>Personal ID</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="file-upload-area">
                                    <div class="upload-file">
                                        <input type="file" name="tax_id" class="upload">
                                        <span>Tax ID</span>
                                    </div>
                                </div>
                            </div>

                            {{-- 
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="shop_message" placeholder="{{ $langg->lang243 }}" required="">
                                    <i class="icofont-envelope"></i>
                                </div>
                            </div>

                            --}}

                           <div class="col-lg-6">
  <div class="form-input">
                                <input type="password" class="Password" name="password" placeholder="{{ $langg->lang186 }}" required="">
                                <i class="icofont-ui-password"></i>
                            </div>

                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                <input type="password" class="Password" name="password_confirmation" placeholder="{{ $langg->lang187 }}" required="">
                                <i class="icofont-ui-password"></i>
                                </div>
                            </div>

                            @if($gs->is_capcha == 1)

<div class="col-lg-6">


                            <ul class="captcha-area">
                                <li>
                                    <p>
                                        <img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i class="fas fa-sync-alt pointer refresh_code "></i>
                                    </p>

                                </li>
                            </ul>


</div>

<div class="col-lg-6">

 <div class="form-input">
                                <input type="text" class="Password" name="codes" placeholder="{{ $langg->lang51 }}" required="">
                                <i class="icofont-refresh"></i>

                            </div>



                          </div>

                          @endif

                            <input type="hidden" name="vendor"  value="1">
                            <input class="mprocessdata" type="hidden"  value="{{ $langg->lang188 }}">
                            <button type="submit" class="submit-btn">{{ $langg->lang189 }}</button>

                            </div>




                        </form>
                    </div>
                </div>
                    </div>
                </div>
      </div>
    </div>
  </div>
</div>
<!-- VENDOR LOGIN MODAL ENDS -->

<!-- Product Quick View Modal -->

      <div class="modal fade" id="quickview" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog quickview-modal modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="submit-loader">
                <img src="{{asset('assets/images/'.$gs->loader)}}" alt="">
            </div>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="container quick-view-modal">

                </div>
            </div>
          </div>
        </div>
      </div>
<!-- Product Quick View Modal -->

<!-- Order Tracking modal Start-->
    <div class="modal fade" id="track-order-modal" tabindex="-1" role="dialog" aria-labelledby="order-tracking-modal" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"> <b>{{ $langg->lang772 }}</b> </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                        <div class="order-tracking-content">
                            <form id="track-form" class="track-form">
                                {{ csrf_field() }}
                                <input type="text" id="track-code" placeholder="{{ $langg->lang773 }}" required="">
                                <button type="submit" class="mybtn1">{{ $langg->lang774 }}</button>
                                <a href="#"  data-toggle="modal" data-target="#order-tracking-modal"></a>
                            </form>
                        </div>

                        <div>
                            <div class="submit-loader d-none">
                                <img src="{{asset('assets/images/'.$gs->loader)}}" alt="">
                            </div>
                            <div id="track-order">

                            </div>
                        </div>

            </div>
            </div>
        </div>
    </div>
<!-- Order Tracking modal End -->

<script type="text/javascript">
  var mainurl = "{{url('/')}}";
  var gs      = {!! json_encode($gs) !!};
  var langg    = {!! json_encode($langg) !!};
</script>

    <!-- jquery -->
    {{-- <script src="{{asset('assets/front/js/all.js')}}"></script> --}}
    <script src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script src="{{asset('assets/front/js/vue.js')}}"></script>
    <script src="{{asset('assets/front/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- popper -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- plugin js-->
    <script src="{{asset('assets/front/js/plugin.js')}}"></script>

    <script src="{{asset('assets/front/js/xzoom.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.hammer.min.js')}}"></script>
    <script src="{{asset('assets/front/js/setup.js')}}"></script>

    <script src="{{asset('assets/front/js/toastr.js')}}"></script>
    <!-- main -->
    <script src="{{asset('assets/front/js/main.js')}}"></script>
    <!-- custom -->
    <script src="{{asset('assets/front/js/custom.js')}}"></script>

    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>

    {!! $seo->google_analytics !!}

    @if($gs->is_talkto == 1)
        <!--Start of Tawk.to Script-->
        {!! $gs->talkto !!}
        <!--End of Tawk.to Script-->
    @endif

    @yield('scripts')


    <script src="{{asset('assets/front/assets/JavaScript/main.js')}}"></script>
    
    <script>
        var img = <?php echo json_encode($img_array); ?>; 
        
        $('.cat-img').on('mouseover',function(){
            
            $('#cat-side-img').attr("src", "//horizonwirelesstx.com/assets/images/categories/"+img[$(this).attr('id')]);
            
        });
    </script>


</body>

</html>