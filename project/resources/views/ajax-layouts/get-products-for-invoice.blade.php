<div class="row silver-bg">
		
	<div class="col-md-4">
		<select class="js-example-basic-single prdouct_id" id="prdouct_id" name="prdouct_id">
		@foreach($products as $product)
		  <option value="{{ $product->id }}">{{ $product->name }}</option>.
		 @endforeach
		</select>
	</div>
	<div class="col-md-3">
		<input type="text" name="product_qty" class="input-field" placeholder="Qty">
	</div>
	<div class="col-md-3">
		<a href="javascript:;" onclick="addSingleProduct(this)" class="add-more float-right"><i class="fas fa-plus"></i>{{ __('Insert Product') }} </a>
	</div>
	<div class="col-md-2 remove-this-row">
		<button class="float-right " onclick="removeThisRow(this)"><span class="remove "><i class="fas fa-times"></i></span></button>
	</div>
</div>
