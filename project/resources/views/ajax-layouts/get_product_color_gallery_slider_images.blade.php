
                    <div id="carouselExampleIndicators" class="carousel slide" data-interval="false"
                        data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner " role="listbox">
                            <!-- Slide One - Set the background image for this slide in the line below -->
                            <div class="carousel-item active">
                                <a href="{{ asset('assets/images/color_galleries/') }}/{{$slider_images[0]}}"><img src="{{ asset('assets/images/color_galleries/') }}/{{$slider_images[0]}}" class="img-fluid"></a>
                            </div>

                            @foreach($slider_images as $gal)

                            <div class="carousel-item">
                                <a href="{{ asset('assets/images/color_galleries/') }}/{{$gal}}"><img src="{{ asset('assets/images/color_galleries/') }}/{{$gal}}" class="img-fluid"></a>
                            </div>

                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>