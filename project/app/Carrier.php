<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
   protected $fillable = ['name','slug','status',' photo', 'banner'];
   public $timestamps = false;
}
