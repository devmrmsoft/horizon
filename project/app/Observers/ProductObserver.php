<?php

namespace App\Observers;

use App\Models\Product;

class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        //
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    // public function updated(Product $product)
    // {
        // // dd('here');
        // // $productData = Product::with('productTypeData', 'productTypeChildData', 'categoryData', 'accessoryTypeData', 'accessoryBrandData', 'CarrierData')->where('id', $product->id)->first();
        // $productData = array('stock' => $product->stock, 'slug' => $product->slug);
        // // $productData = "{\n \"stock\": \"{$product->stock}\",  \"slug\": \"{$product->slug}\"\n}";
        // $url = 'http://localhost/inks-official/api/getProductData';
        // $handle = curl_init($url);
        // curl_setopt($handle, CURLOPT_POST, true);
        // curl_setopt($handle, CURLOPT_POSTFIELDS, $productData);
        // $response = curl_exec($handle);
        // curl_close($handle);
        // // print_r($response);die;
        // // print_r($product);die();
    // }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    // public function deleted(Product $product)
    // {
    //     //
    // }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    // public function restored(Product $product)
    // {
    //     //
    // }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    // public function forceDeleted(Product $product)
    // {
    //     //
    // }
}
