<?php

namespace App\Http\Controllers\Front;

use Datatables;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\catType;
use App\accessoryBrand;
use App\accessoryType;
use App\Carrier;



class CarrierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

  
    //*** GET Request
    public function index()
    {
        return view('admin.carriers.index');
    }

     public function datatables()
    {
         $datas = Carrier::orderBy('id','desc')->get();
         // dd($datas);
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('status', function(Carrier $data) {
                                $class = $data->status == 1 ? 'drop-success' : 'drop-danger';
                                $s = $data->status == 1 ? 'selected' : '';
                                $ns = $data->status == 0 ? 'selected' : '';
                                return '<div class="action-list"><select class="process select droplinks '.$class.'"><option data-val="1" value="'. route('carriers.status',['id1' => $data->id, 'id2' => 1]).'" '.$s.'>Activated</option><option data-val="0" value="'. route('carriers.status',['id1' => $data->id, 'id2' => 0]).'" '.$ns.'>Deactivated</option>/select></div>';
                            })
                            // ->addColumn('attributes', function(Category $data) {
                            //     $buttons = '<div class="action-list"><a data-href="' . route('admin-attr-createForCategory', $data->id) . '" class="attribute" data-toggle="modal" data-target="#attribute"> <i class="fas fa-edit"></i>Create</a>';
                            //     if ($data->attributes()->count() > 0) {
                            //       $buttons .= '<a href="' . route('admin-attr-manage', $data->id) .'?type=category' . '" class="edit"> <i class="fas fa-edit"></i>Manage</a>';
                            //     }
                            //     $buttons .= '</div>';

                            //     return $buttons;
                            // })
                            ->addColumn('action', function(Carrier $data) {
                                return '<div class="action-list"><a data-href="' . route('carriers.edit',$data->id) . '" class="edit" data-toggle="modal" data-target="#modal1"> <i class="fas fa-edit"></i>Edit</a><a href="javascript:;" data-href="' . route('carriers.delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>';
                            })
                            ->rawColumns(['status','attributes','action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

      //*** GET Request
    public function create()
    {
        // dd('here');
        if(isset($_GET['sts'])){
            $data   =   [
                            'sts'   =>  'true',

            ];
            return view('admin.carriers.create',$data);
        }
        return view('admin.carriers.create');
    }


   //*** GET Request Status
      public function status($id1,$id2)
      {
        // dd('carriers');
          $data = Carrier::findOrFail($id1);
          $data->status = $id2;
          $data->update();
      }


        //*** POST Request
    public function store(Request $request)
    {
        
        // dd('here');
        //--- Validation Section
        $rules = [
            'slug' => 'unique:carriers|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new Carrier();
        // $typedata = new catType();
        $input = $request->all();

        if ($file = $request->file('photo'))
         {
            $name = time().$file->getClientOriginalName();
            $file->move('assets/front/images/carriers',$name);
            $input['photo'] = $name;
        }

        if ($file = $request->file('banner'))
         {
            $name = time().$file->getClientOriginalName();
            $file->move('assets/front/images/carriers',$name);
            $input['banner'] = $name;
        }
        else {
                $input['is_featured'] = 1;
                //--- Validation Section
                //--- Validation Section Ends
                if ($file = $request->file('image'))
                {
                   $name = time().$file->getClientOriginalName();
                   $file->move('assets/front/images/carriers',$name);
                   $input['image'] = $name;
                }
        }
        
        $data->fill($input)->save();
        
        //--- Logic Section Ends
        if(isset($input['sts'])){
            if($input['sts'] == '1'){
                $msg = 'New Carrier Added';
                $carrier   =   Carrier::all();
                $lastid =   $data->id;
                $data   =   [
                                'carrier'      =>  $carrier,
                                'lastid'    =>  $lastid,
                 ];
                $returnHTML = view('load.pro_brand_layout',$data)->render();
                 return response()->json(array('carrier' => 'carrier', 'html'=>$returnHTML));
            }
        }
        //--- Redirect Section
        
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }


      //*** POST Request
    public function update(Request $request, $id)
    {
        // var_dump($id);
        // die;
        //--- Validation Section
        $rules = [
            'slug' => 'unique:carriers,slug,'.$id.'|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'photo.mimes' => 'Icon Type is Invalid.',
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = Carrier::findOrFail($id);
        $input = $request->all();
            if ($file = $request->file('photo'))
            {
                $name = time().$file->getClientOriginalName();
                $file->move('assets/front/images/carriers',$name);
                if($data->photo != null)
                {
                    if (file_exists(public_path().'/assets/front/images/carriers/'.$data->photo)) {
                        unlink(public_path().'/assets/front/images/carriers/'.$data->photo);
                    }
                }
            $input['photo'] = $name;
            }

            if ($file = $request->file('banner'))
            {
                $name = time().$file->getClientOriginalName();
                $file->move('assets/front/images/carriers',$name);
                if($data->banner != null)
                {
                    if (file_exists(public_path().'/assets/front/images/carriers/'.$data->banner)) {
                        unlink(public_path().'/assets/front/images/carriers/'.$data->banner);
                    }
                }
            $input['banner'] = $name;
            }

            // if ($request->is_featured == ""){
            //     $input['is_featured'] = 0;
            // }
            // else {
            //         $input['is_featured'] = 1;
            //         //--- Validation Section
            //         $rules = [
            //             'image' => 'mimes:jpeg,jpg,png,svg'
            //                 ];
            //         $customs = [
            //             'image.required' => 'Feature Image is required.'
            //                 ];
            //         $validator = Validator::make(Input::all(), $rules, $customs);

            //         if ($validator->fails()) {
            //         return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
            //         }
            //         //--- Validation Section Ends
            //         if ($file = $request->file('image'))
            //         {
            //            $name = time().$file->getClientOriginalName();
            //            $file->move('assets/front/images/carriers',$name);
            //            $input['image'] = $name;
            //         }
            // }

        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }



     //*** GET Request
    public function edit($id)
    {
        // dd($id);
        $data = Carrier::findOrFail($id);
        return view('admin.carriers.edit',compact('data'));
    }





    //*** GET Request Delete
    public function destroy($id)
    {
        $data = Carrier::findOrFail($id);

        //If Photo Doesn't Exist
        if($data->photo == null){
            $data->delete();
            //--- Redirect Section
            $msg = 'Data Deleted Successfully.';
            return response()->json($msg);
            //--- Redirect Section Ends
        }
        //If Photo Exist
        if($data->photo != null){

            if (file_exists(public_path().'/assets/front/images/carriers/'.$data->photo)) {
                unlink(public_path().'/assets/front/images/carriers/'.$data->photo);
            }
        } 
        //If Photo Exist
        if($data->banner != null){

            if (file_exists(public_path().'/assets/front/images/carriers/'.$data->banner)) {
                unlink(public_path().'/assets/front/images/carriers/'.$data->banner);
            }
        }
        if($data->image != null){

            if (file_exists(public_path().'/assets/front/images/carriers/'.$data->image)) {
                unlink(public_path().'/assets/front/images/carriers/'.$data->image);
            }
        }
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

   }


