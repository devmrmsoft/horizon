<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DB;
use App\catType;
use App\Models\Category;
use App\Carrier;
use App\catTypeChild;
use App\accessoryBrand;
use App\accessoryType;
use App\Models\Childcategory;
use App\Models\Subcategory;


class CategoryController extends Controller
{
	public function getCatType()
	{
		$catType = catType::get();
		echo json_encode(array('data'=> $catType));
	}

	public function getCategory()
	{
		$category = Category::with('CarrierData', 'accessoryTypeData', 'accessoryBrandData')->get();
		echo json_encode(array('data'=> $category));
	}

	public function getCarriers()
	{
		$carriers = Carrier::get();
		echo json_encode(array('data'=> $carriers));
	}

	public function getcatTypeChild()
	{
		$catTypeChild = catTypeChild::get();
		echo json_encode(array('data'=> $catTypeChild));
	}

	public function getaccessoryBrand()
	{
		$accessoryBrands = accessoryBrand::get();
		echo json_encode(array('data'=> $accessoryBrands));
	}

	public function getaccessoryType()
	{
		$accessoryType = accessoryType::get();
		echo json_encode(array('data'=> $accessoryType));
	}

	public function getChildcategory()
	{
		$childCategory = Childcategory::get();
		echo json_encode(array('data'=> $childCategory));
	}

	public function getSubcategory()
	{
		$subCategory = Subcategory::with('category')->get();
		echo json_encode(array('data'=> $subCategory));
	}
}