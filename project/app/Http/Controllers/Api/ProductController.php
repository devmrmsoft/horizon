<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Product;
use App\Models\Category;
use App\Models\Order;
use App\accessoryType;
use Illuminate\Http\Request;



class ProductController extends Controller
{
	public function getProduct()
	{
		$product = Product::with('productTypeData', 'productTypeChildData', 'categoryData', 'accessoryTypeData', 'accessoryBrandData', 'CarrierData')->get();
		echo json_encode(array('data'=> $product));
	}

	public function storeOrderData(Request $request)
	{
		// print_r($request->all());
		$data = $request->all();
		$data['website'] = 'INKS';
		$typedata = new Order();
		$typedata->fill($data)->save();
		// $data['cart'] = unserialize(bzdecompress(utf8_decode($data['cart'])));
		// print_r($data);
	}
}