<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use Illuminate\Http\Request;
use Datatables;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts  = Discount::orderBy('id','desc')->get();
        $data   =   [

                        'discounts' =>  $discounts,

                    ];
        //dd('index');
        return view('admin.discount.index', $data);
    }

    //*** JSON Request
    public function datatables()
    {
         $datas = Discount::orderBy('id','desc')->get();
         //dd($datas);
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('name', function(Discount $data) {
                                $details =  str_replace('_',' ',$data->name);
                                $details =  ucwords($details);
                                return  '<div>'.$details.'</div>';
                            })
                            ->addColumn('action', function(Discount $data) {
                                return '<div class="action-list"><a href="' . route('admin-role-edit',$data->id) . '"> <i class="fas fa-edit"></i>Edit</a><a href="javascript:;" data-href="' . route('admin-role-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>';
                            }) 
                            ->rawColumns(['section','action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.discount.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $discount   =   new Discount;
        $discount->name =   $request->name;
        $discount->save();

        //--- Redirect Section        
        $msg = 'New Data Added Successfully.'.'<a href="'.route("admin-discount-index").'">View Discount Lists</a>';
        return response()->json($msg);      
        //--- Redirect Section Ends   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function show(Discount $discount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount,$id)
    {
        $disc   =   Discount::find($id);
        $data   =   [

                    'discount'  =>  $disc,

                    ];
        return view('admin.discount.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discount $discount,$id)
    {
        //dd($request->all());
        Discount::where('id', $id)
       ->update([
           'name' => $request->name
        ]);
        $msg = 'Successfully Updated.'.'<a href="'.route("admin-discount-index").'">View Discount Lists</a>';
        return response()->json($msg); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount $discount,$id)
    {
        $data = Discount::findOrFail($id);
        $data->delete();
        $msg = 'Discount List Deleted Successfully.';
        return response()->json($msg);
    }
}
