<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Generalsetting;
use App\Models\User;
use App\Models\Verification;
use App\Classes\GeniusMailer;
use App\Models\Notification;
use Auth;
use Illuminate\Support\Facades\Input;
use Validator;

class RegisterController extends Controller
{

	public function showRetailerRegisterForm(){
		return view('user.register_retailer');
	}
	public function showRegisterForm(){
		return view('user.register_retailer');
	}
    public function register(Request $request)
    {
    	//dd($request->all());

        $rules = [
                ];

    	$gs = Generalsetting::findOrFail(1);

    	if($gs->is_capcha == 1)
    	{
	        $value = session('captcha_string');
	        if ($request->codes != $value){
	            return response()->json(array('errors' => [ 0 => 'Please enter Correct Capcha Code.' ]));    
	        }    		
    	}


        //--- Validation Section

        $rules = [
		        'email'   => 'required|email|unique:users',
		        'password' => 'required|confirmed'
                ];
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

	        $user = new User;
	        $input = $request->all();        
	        $input['password'] = bcrypt($request['password']);
	        $token = md5(time().$request->name.$request->email);
	        $input['verification_link'] = $token;
	        $input['affilate_code'] = md5($request->name.$request->email);
	        
			if ($file = $request->file('personal_id')) 
			 {      
			    $name = time().$file->getClientOriginalName();
			    $file->move('assets/images/vendorprofile',$name);           
			    $input['personal_id'] = $name;
			}
			if ($file = $request->file('tax_id')) 
			 {
			    $name = time().$file->getClientOriginalName();
			    $file->move('assets/images/vendorprofile',$name);           
			    $input['tax_id'] = $name;
			}
	          if(!empty($request->vendor))
	          {
					//--- Validation Section
					$rules = [
						'shop_name' => 'unique:users',
						'shop_number'  => 'max:10'
							];
					$customs = [
						'shop_name.unique' => 'This Shop Name has already been taken.',
						'shop_number.max'  => 'Shop Number Must Be Less Then 10 Digit.'
					];

					$validator = Validator::make(Input::all(), $rules, $customs);
					if ($validator->fails()) {
					return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
					}
					$input['is_vendor'] = 1;
					$input['owner_name'] = $request->name;

								$user->fill($input)->save();
						        $verification = new Verification;
						        $veriData = $request->all();
						        $veriData['status'] = 'Pending';        
						        $veriData['user_id'] = $user->id;
						        $veriData['admin_warning'] = 0;
								// $veriData['attachments'] =  $input['attachments'];				        
						        $verification->fill($veriData)->save();
			  }
			  
			$user->fill($input)->save();
	        if($gs->is_verification_email == 1)
	        {
	        if (!empty($request->vendor)) {
		        	$to = $request->email;
		        $name = $request->name;
		        $password = $request->password;
		        $subject = 'Verify your email address.';
		        $msg = "Dear Customer,<br> We noticed that you need to verify your email address. <a href=".url('user/register/verify/'.$token)." style='text-decoration: none;color: #ff5500;'><br>Simply click here to verify. </a>";
		        //Sending Email To Customer
				        if($gs->is_smtp == 1)
				        {
				        $data = [
				            'to' => $to,
				            'name' => $name,
				            'password' => $password,
				            'subject' => $subject,
				            'body' => $msg,
				            'token' => $token,
				        ];

		        $mailer = new GeniusMailer();
		        $mailer->sendRequiredApprovalMail($data);
		        }
		        else
		        {
			        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
			        $headers .= "MIME-Version: 1.0\r\n";
		                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
			        mail($to,$subject,$msg,$headers);
		        }
	          		return response()->json('We need to verify your email address. We have sent an email to '.$to.' to verify your email address. Please click link in that email to continue.');
		        }
		        else{
		        	  	$to = $request->email;
		        $name = $request->name;
		        $password = $request->password;
		        $subject = 'Verify your email address.';
		        $msg = "Dear Customer,<br> We noticed that you need to verify your email address. <a href=".url('user/register/verify/'.$token)." style='text-decoration: none;color: #ff5500;'><br>Simply click here to verify. </a>";
		        //Sending Email To Customer
				        if($gs->is_smtp == 1)
				        {
				        $data = [
				            'to' => $to,
				            'subject' => $subject,
				            'body' => $msg,
				        ];

		        $mailer = new GeniusMailer();
		        $mailer->sendCustomMail($data);
		        }
		        else
		        {
			        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
			        $headers .= "MIME-Version: 1.0\r\n";
		                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
			        mail($to,$subject,$msg,$headers);
		        }
	          		return response()->json('We need to verify your email address. We have sent an email to '.$to.' to verify your email address. Please click link in that email to continue.');
		        }

		        }
	        
	        else {

            $user->email_verified = 'Yes';
            $user->update();
	        $notification = new Notification;
	        $notification->user_id = $user->id;
	        $notification->save();
            Auth::guard('web')->login($user); 
          	return response()->json(1);
	        }

    }

    public function token($token)
    {
        $gs = Generalsetting::findOrFail(1);

        if($gs->is_verification_email == 1)
	        {    	
			        $user = User::where('verification_link','=',$token)->first();
			        if(isset($user) && $user->is_vendor == 1)
			        {
			        	if ($user->status == 1) {
				            $user->email_verified = 'Yes';
				            $user->update();
					        $notification = new Notification;
					        $notification->user_id = $user->id;
					        $notification->save();
				            Auth::guard('web')->login($user); 
		       				$subject = 'Account Approved!';
		        			$msg = "Your account has been approved!";
						        if($gs->is_smtp == 1)
						        {
						        $data = [
						            'to' => $user->email,
						            'name' => $user->name,
						            'subject' => $subject,
						            'body' => $msg,
						            'type' => 'vendor'
						        ];

					        $mailer = new GeniusMailer();
					        $mailer->sendApprovalMail($data);
					        }
					        else
					        {
						        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
						        $headers .= "MIME-Version: 1.0\r\n";
					                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
						        mail($to,$subject,$msg,$headers);
					        }	
					        return redirect()->route('user-dashboard')->with('success','Email Verified Successfully');
			        	}
			        	else{
			        		return redirect()->route('user.login')->with('notApproved','Your account has not been approved yet!');
			       		 }
			        }
			        else{
			        		$user->status = 1;
			        		$user->email_verified = 'Yes';
				            $user->update();
					        $notification = new Notification;
					        $notification->user_id = $user->id;
					        $notification->save();
				            Auth::guard('web')->login($user); 
				            $subject = 'Account Approved!';
		        			$msg = "Your account has been approved!";
						        if($gs->is_smtp == 1)
						        {
						        $data = [
						            'to' => $user->email,
						            'name' => $user->name,
						            'subject' => $subject,
						            'body' => $msg,
						            'type' => 'customer'
						        ];

					        $mailer = new GeniusMailer();
					        $mailer->sendApprovalMail($data);
					        }
					        else
					        {
						        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
						        $headers .= "MIME-Version: 1.0\r\n";
					                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
						        mail($to,$subject,$msg,$headers);
					        }
			            	return redirect()->route('user-dashboard')->with('success','Email Verified Successfully');
			        }
			        
    		}
    		else {
    		return redirect()->back();	
    		}
    }
}