-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 25, 2020 at 09:54 AM
-- Server version: 5.7.32
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `horizonw_wireless`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessory_brands`
--

CREATE TABLE `accessory_brands` (
  `id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `accessories` int(10) DEFAULT '0',
  `image` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accessory_brands`
--

INSERT INTO `accessory_brands` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `accessories`, `image`) VALUES
(25, 'ZIZO', 'zizo', 1, NULL, 0, 0, NULL),
(26, 'Incipio', 'incipio', 1, NULL, 0, 0, NULL),
(27, 'Kate Spade', 'kate-spade', 1, NULL, 0, 0, NULL),
(28, 'Speck', 'speck', 1, NULL, 0, 0, NULL),
(31, 'Zuve', 'zuve', 1, NULL, 0, 0, NULL),
(32, 'HAVIT', 'havit', 1, NULL, 0, 0, NULL),
(33, 'Aegis', 'aegis', 1, NULL, 0, 0, NULL),
(36, 'Max Power', 'max-power', 1, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accessory_types`
--

CREATE TABLE `accessory_types` (
  `id` int(191) NOT NULL,
  `accessory_brand_id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accessory_types`
--

INSERT INTO `accessory_types` (`id`, `accessory_brand_id`, `name`, `slug`, `status`) VALUES
(74, 25, 'Bolt Series', 'bolt-series', 1),
(75, 25, 'Transform Series', 'transform-series', 1),
(76, 25, 'Refine Series', 'refine-series', 1),
(77, 25, 'Pulse', 'pulse', 1),
(78, 32, 'E58P', 'e58p', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(191) NOT NULL DEFAULT '0',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `role_id`, `photo`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `shop_name`) VALUES
(1, 'Admin', 'admin@gmail.com', '713 988 6565', 0, '1556780563user.png', '$2y$10$p35S2FczpEfpbe41CX4j4.XE548tHBtF5weGLPxZ56MX5dsOFtaCC', 1, 'uPjgPr8rx6EpKHqKX0BmN7U0r0qUdScI47t5jvlCWYAMO6kuS0JEIarPKAQn', '2018-02-28 23:27:08', '2020-06-12 06:06:07', 'Horizon Wireless'),
(5, 'Mr Mamun', 'mamun@gmail.com', '34534534', 17, '1568803644User.png', '$2y$10$3AEjcvFBiQHECgtH9ivXTeQZfMf.rw318G820TtVBsYaCt7UNOwGC', 1, NULL, '2019-09-18 04:47:24', '2019-09-18 21:21:49', NULL),
(6, 'Mr. Manik', 'manik@gmail.com', '5079956958', 18, '1568863361user-admin.png', '$2y$10$Z3Jx5jHjV2m4HtZHzeaKMuwxkLAKfJ1AX3Ed5MPACvFJLFkEWN9L.', 1, NULL, '2019-09-18 21:22:41', '2019-09-18 21:22:41', NULL),
(7, 'Mr. Pratik', 'pratik@gmail.com', '34534534', 16, '1568863396user-admin.png', '$2y$10$u.93l4y6wOz6vq3BlAxvU.LuJ16/uBQ9s2yesRGTWUtLRiQSwoH1C', 1, 'iZPbEaxjSWBJMvncLqeMtAQsG7VoSirVMJ1EBfdJogvgXK2DM5mw236fBCOq', '2019-09-18 21:23:16', '2019-09-18 21:23:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_languages`
--

CREATE TABLE `admin_languages` (
  `id` int(191) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rtl` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_languages`
--

INSERT INTO `admin_languages` (`id`, `is_default`, `language`, `file`, `name`, `rtl`) VALUES
(1, 1, 'English', '1567232745AoOcvCtY.json', '1567232745AoOcvCtY', 0),
(2, 0, 'RTL English', '1584887310NzfWDhO8.json', '1584887310NzfWDhO8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_conversations`
--

CREATE TABLE `admin_user_conversations` (
  `id` int(191) NOT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` enum('Ticket','Dispute') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_conversations`
--

INSERT INTO `admin_user_conversations` (`id`, `subject`, `user_id`, `message`, `created_at`, `updated_at`, `type`, `order_number`) VALUES
(1, 'Order Confirmation', 22, 'rfgdfgfd', '2020-01-21 01:18:38', '2020-01-21 01:18:38', 'Ticket', NULL),
(2, 'test', 30, 'hello shayan', '2020-06-16 05:57:07', '2020-06-16 05:57:07', NULL, NULL),
(3, 'TEST', 32, 'TEST', '2020-11-11 06:47:33', '2020-11-11 06:47:33', NULL, NULL),
(4, 'Hello There', 31, 'lkb', '2020-11-18 23:34:38', '2020-11-18 23:34:38', NULL, NULL),
(5, 'Manager', 77, 'something', '2020-11-25 19:47:46', '2020-11-25 19:47:46', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_messages`
--

CREATE TABLE `admin_user_messages` (
  `id` int(191) NOT NULL,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_messages`
--

INSERT INTO `admin_user_messages` (`id`, `conversation_id`, `message`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'rfgdfgfd', 22, '2020-01-21 01:18:38', '2020-01-21 01:18:38'),
(2, 2, 'hello shayan', NULL, '2020-06-16 05:57:07', '2020-06-16 05:57:07'),
(3, 3, 'TEST', NULL, '2020-11-11 06:47:33', '2020-11-11 06:47:33'),
(4, 4, 'lkb', NULL, '2020-11-18 23:34:38', '2020-11-18 23:34:38'),
(5, 5, 'something', NULL, '2020-11-25 19:47:46', '2020-11-25 19:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `attributable_id` int(11) DEFAULT NULL,
  `attributable_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `input_name` varchar(255) DEFAULT NULL,
  `price_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `details_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(191) NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Large','TopSmall','BottomSmall') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `photo`, `link`, `type`) VALUES
(1, '1568889151top2.jpg', 'https://www.google.com/', 'TopSmall'),
(2, '1568889146top1.jpg', NULL, 'TopSmall'),
(3, '1568889164bottom1.jpg', 'https://www.google.com/', 'Large'),
(4, '1564398600side-triple3.jpg', 'https://www.google.com/', 'BottomSmall'),
(5, '1564398579side-triple2.jpg', 'https://www.google.com/', 'BottomSmall'),
(6, '1564398571side-triple1.jpg', 'https://www.google.com/', 'BottomSmall');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(191) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `meta_tag` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `title`, `details`, `photo`, `source`, `views`, `status`, `meta_tag`, `meta_description`, `tags`, `created_at`) VALUES
(9, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700986-min.jpg', 'www.geniusocean.com', 36, 1, 'b1,b2,b3', 'Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level.', 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-02-06 09:53:41'),
(10, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700902-min.jpg', 'www.geniusocean.com', 14, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-03-06 09:54:21'),
(12, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700821-min.jpg', 'www.geniusocean.com', 19, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-04-06 22:04:20'),
(13, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a \r\ndrifting tone like that of a not-quite-tuned-in radio station \r\n                                        rises and for a while drowns out\r\n the patter. These are the sounds encountered by NASA’s Cassini \r\nspacecraft as it dove \r\n                                        the gap between Saturn and its \r\ninnermost ring on April 26, the first of 22 such encounters before it \r\nwill plunge into \r\n                                        atmosphere in September. What \r\nCassini did not detect were many of the collisions of dust particles \r\nhitting the spacecraft\r\n                                        it passed through the plane of \r\nthe ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\">How its Works ?</h3>\r\n                                    <p align=\"justify\">\r\n                                        MIAMI — For decades, South \r\nFlorida schoolchildren and adults fascinated by far-off galaxies, \r\nearthly ecosystems, the proper\r\n                                        ties of light and sound and \r\nother wonders of science had only a quaint, antiquated museum here in \r\nwhich to explore their \r\n                                        interests. Now, with the \r\nlong-delayed opening of a vast new science museum downtown set for \r\nMonday, visitors will be able \r\n                                        to stand underneath a suspended,\r\n 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, \r\nmahi mahi, devil\r\n                                        rays and other creatures through\r\n a 60,000-pound oculus. <br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of\r\n a huge cocktail glass. And that’s just one of many\r\n                                        attractions and exhibits. \r\nOfficials at the $305 million Phillip and Patricia Frost Museum of \r\nScience promise that it will be a \r\n                                        vivid expression of modern \r\nscientific inquiry and exposition. Its opening follows a series of \r\nsetbacks and lawsuits and a \r\n                                        scramble to finish the \r\n250,000-square-foot structure. At one point, the project ran \r\nprecariously short of money. The museum\r\n                                        high-profile opening is \r\nespecially significant in a state s <br></p><p align=\"justify\"><br></p><h3 align=\"justify\">Top 5 reason to choose us</h3>\r\n                                    <p align=\"justify\">\r\n                                        Mauna Loa, the biggest volcano \r\non Earth — and one of the most active — covers half the Island of \r\nHawaii. Just 35 miles to the \r\n                                        northeast, Mauna Kea, known to \r\nnative Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea \r\nlevel. To them it repre\r\n                                        sents a spiritual connection \r\nbetween our planet and the heavens above. These volcanoes, which have \r\nbeguiled millions of \r\n                                        tourists visiting the Hawaiian \r\nislands, have also plagued scientists with a long-running mystery: If \r\nthey are so close together, \r\n                                        how did they develop in two \r\nparallel tracks along the Hawaiian-Emperor chain formed over the same \r\nhot spot in the Pacific \r\n                                        Ocean — and why are their \r\nchemical compositions so different? \"We knew this was related to \r\nsomething much deeper,\r\n                                        but we couldn’t see what,” said \r\nTim Jones.\r\n                                    </p>', '15542700676-min.jpg', 'www.geniusocean.com', 57, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-05-06 22:04:36'),
(14, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700595-min.jpg', 'www.geniusocean.com', 3, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-06-03 06:02:30'),
(15, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700464-min.jpg', 'www.geniusocean.com', 6, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-07-03 06:02:53'),
(16, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700383-min.jpg', 'www.geniusocean.com', 5, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-08-03 06:03:14'),
(17, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700322-min.jpg', 'www.geniusocean.com', 50, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:37'),
(18, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542700251-min.jpg', 'www.geniusocean.com', 151, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:59'),
(20, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542699136-min.jpg', 'www.geniusocean.com', 10, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-08-03 06:03:14'),
(21, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542699045-min.jpg', 'www.geniusocean.com', 36, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:37'),
(22, 2, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542698954-min.jpg', 'www.geniusocean.com', 68, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:59'),
(23, 7, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542698893-min.jpg', 'www.geniusocean.com', 5, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2018-08-03 06:03:14'),
(24, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15542698832-min.jpg', 'www.geniusocean.com', 34, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:37');
INSERT INTO `blogs` (`id`, `category_id`, `title`, `details`, `photo`, `source`, `views`, `status`, `meta_tag`, `meta_description`, `tags`, `created_at`) VALUES
(25, 3, 'How to design effective arts?', '<div align=\"justify\">The recording starts with the patter of a summer squall. Later, a drifting tone like that of a not-quite-tuned-in radio station rises and for a while drowns out the patter. These are the sounds encountered by NASA’s Cassini spacecraft as it dove the gap between Saturn and its innermost ring on April 26, the first of 22 such encounters before it will plunge into atmosphere in September. What Cassini did not detect were many of the collisions of dust particles hitting the spacecraft it passed through the plane of the ringsen the charged particles oscillate in unison.<br><br></div><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">How its Works ?</h3><p align=\"justify\">MIAMI — For decades, South Florida schoolchildren and adults fascinated by far-off galaxies, earthly ecosystems, the proper ties of light and sound and other wonders of science had only a quaint, antiquated museum here in which to explore their interests. Now, with the long-delayed opening of a vast new science museum downtown set for Monday, visitors will be able to stand underneath a suspended, 500,000-gallon aquarium tank and gaze at hammerhead and tiger sharks, mahi mahi, devil rays and other creatures through a 60,000-pound oculus.&nbsp;<br></p><p align=\"justify\">Lens that will give the impression of seeing the fish from the bottom of a huge cocktail glass. And that’s just one of many attractions and exhibits. Officials at the $305 million Phillip and Patricia Frost Museum of Science promise that it will be a vivid expression of modern scientific inquiry and exposition. Its opening follows a series of setbacks and lawsuits and a scramble to finish the 250,000-square-foot structure. At one point, the project ran precariously short of money. The museum high-profile opening is especially significant in a state s&nbsp;<br></p><p align=\"justify\"><br></p><h3 align=\"justify\" style=\"font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);\"=\"\">Top 5 reason to choose us</h3><p align=\"justify\">Mauna Loa, the biggest volcano on Earth — and one of the most active — covers half the Island of Hawaii. Just 35 miles to the northeast, Mauna Kea, known to native Hawaiians as Mauna a Wakea, rises nearly 14,000 feet above sea level. To them it repre sents a spiritual connection between our planet and the heavens above. These volcanoes, which have beguiled millions of tourists visiting the Hawaiian islands, have also plagued scientists with a long-running mystery: If they are so close together, how did they develop in two parallel tracks along the Hawaiian-Emperor chain formed over the same hot spot in the Pacific Ocean — and why are their chemical compositions so different? \"We knew this was related to something much deeper, but we couldn’t see what,” said Tim Jones.</p>', '15557542831-min.jpg', 'www.geniusocean.com', 40, 1, NULL, NULL, 'Business,Research,Mechanical,Process,Innovation,Engineering', '2019-01-03 06:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`) VALUES
(2, 'Oil & gas', 'oil-and-gas'),
(3, 'Manufacturing', 'manufacturing'),
(4, 'Chemical Research', 'chemical_research'),
(5, 'Agriculture', 'agriculture'),
(6, 'Mechanical', 'mechanical'),
(7, 'Entrepreneurs', 'entrepreneurs'),
(8, 'Technology', 'technology');

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `name`, `slug`, `status`, `photo`) VALUES
(1, 'Boost Mobile', 'boost-mobile', 1, 'boost_mobile.png'),
(2, 'Sprint', 'sprint', 1, 'sprint.png'),
(3, 'Verizon', 'verizon', 1, 'verizon.png'),
(4, 'AT&T', 'at-t', 1, 'at_and_t.png'),
(5, 'T-Mobile', 't-mobile', 1, 't_mobile.png'),
(6, 'Metro by T-Mobile', 'metro-by-t-mobile', 1, 'metro_by_t.png'),
(7, 'Cricket Wireless', 'cricket-wireless', 1, 'cricket_wireless.png');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT '0',
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devices` int(10) DEFAULT '0',
  `brands` int(10) DEFAULT '0',
  `carriers` int(10) DEFAULT '0',
  `accessories` int(10) DEFAULT '0',
  `arrivals` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `image`, `devices`, `brands`, `carriers`, `accessories`, `arrivals`) VALUES
(44, 'Apple', 'apple', 1, '1603990596device_32.png', 1, '159732661111-pro.png', 1, 1, 0, 1, 0),
(45, 'Samsung', 'samsung', 1, '1603992775device_6.png', 1, '1597326594s20-ultra.png', 1, 1, 0, 1, 0),
(46, 'LG', 'lg', 1, '1604002235device_4.png', 1, '1597326570LG-G8-ThinQ.png', 1, 1, 0, 1, 0),
(47, 'Motorola', 'motorola', 1, '1604002258motorola.png', 1, '1597326553Moto-G-Stylus.png', 1, 1, 0, 0, 0),
(48, 'Google', 'google', 1, '1603992691device_48.png', 1, '1597326523Google-Pixel-4-XL.png', 1, 1, 0, 0, 0),
(49, 'All', 'all', 1, NULL, 0, NULL, 0, 0, 0, 0, 0),
(50, 'Acellories', 'acellories', 1, '1604346656accellories.png', 0, NULL, 0, 1, 0, 0, 0),
(51, 'ADATA', 'adata', 1, '1604346710a_data.png', 0, NULL, 0, 1, 0, 0, 0),
(52, 'Airium', 'airium', 1, '1604346735airium.png', 0, NULL, 0, 1, 0, 0, 0),
(53, 'Cellhelmet', 'cellhelmet', 1, '1604346760cellhelmet.png', 0, NULL, 0, 1, 0, 0, 0),
(54, 'Axess', 'axess', 1, '1604346783axess.png', 0, NULL, 0, 1, 0, 0, 0),
(55, 'Valor', 'valor', 1, '1604346828valor.png', 0, NULL, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cat_types`
--

CREATE TABLE `cat_types` (
  `id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat_types`
--

INSERT INTO `cat_types` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `image`) VALUES
(19, 'Phones', 'phones', 1, NULL, 0, NULL),
(20, 'Accessories', 'accessories', 1, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cat_type_children`
--

CREATE TABLE `cat_type_children` (
  `id` int(191) NOT NULL,
  `cat_type_id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `accessories` int(10) DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat_type_children`
--

INSERT INTO `cat_type_children` (`id`, `cat_type_id`, `name`, `slug`, `status`, `accessories`, `image`) VALUES
(13, 20, 'Cases/Screen Protector', 'cases-screen-protector', 1, 1, '1602603895cases.png'),
(14, 20, 'Charger/Power', 'charger-power', 1, 1, '1602603880charger.png'),
(15, 20, 'Headphones', 'headphones', 1, 1, '1602603871bluetooth.png'),
(16, 20, 'Leather Pouches', 'leather-pouches', 1, 1, '1602603850pouches.png'),
(17, 20, 'Memory', 'memory', 1, 1, '1602603827memory.png'),
(18, 20, 'Apple watch', 'wearable', 1, 1, '1602603816smartwatch.png'),
(19, 20, 'Other Accessories', 'other-accessories', 1, 1, '1602603805others.png'),
(20, 20, 'Handsfree', 'handsfree', 1, 0, '1602603792handsfree.png'),
(21, 20, 'Phones', 'phones-accessory', 1, 0, '1603733402phones-accessory.png'),
(23, 20, 'Cables', 'cables-accessory', 1, 0, '1603735416power-cable.png'),
(25, 19, 'Mounts', 'mount-accessory', 1, 0, '1603738959mount-accessory.png'),
(26, 20, 'bluetooth', 'bluetooth-accessory', 1, 0, '1603739045bluetooth-accessory.png'),
(27, 20, 'APPLE AIRPODS', 'airpods-accessory', 1, 0, '1603741330airpods-512.png'),
(28, 20, 'TEMPERED GLASS PROTECTORS', 'tempered-glass-protectors-accessory', 1, 0, '1603741437telephone-cartoon-png-download-1024681-free-transparent-tempered-glass-png-900_600.png'),
(29, 20, 'TABLETS', 'tablets-accessory', 1, 0, '1603741593content-marketing.png'),
(30, 20, 'SPEAKERS', 'speakers-accessory', 1, 0, '1603741709speaker.png'),
(31, 20, 'POWER BANKS', 'power-banks-accessory', 1, 0, '1603742179power-bank.png'),
(32, 20, 'STANDS & GRIPS', 'stands-and-grips-accessory', 1, 0, '1603742283Phone_accessory-58-512.png'),
(33, 20, 'DISPLAYS', 'displays-accessory', 1, 0, '1605545546power-cable.png');

-- --------------------------------------------------------

--
-- Table structure for table `childcategories`
--

CREATE TABLE `childcategories` (
  `id` int(191) NOT NULL,
  `subcategory_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(191) NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(191) NOT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) NOT NULL,
  `recieved_user` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `counters`
--

CREATE TABLE `counters` (
  `id` int(11) NOT NULL,
  `type` enum('referral','browser') NOT NULL DEFAULT 'referral',
  `referral` varchar(255) DEFAULT NULL,
  `total_count` int(11) NOT NULL DEFAULT '0',
  `todays_count` int(11) NOT NULL DEFAULT '0',
  `today` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counters`
--

INSERT INTO `counters` (`id`, `type`, `referral`, `total_count`, `todays_count`, `today`) VALUES
(1, 'referral', 'www.facebook.com', 5, 0, NULL),
(2, 'referral', 'geniusocean.com', 2, 0, NULL),
(3, 'browser', 'Windows 10', 4029, 0, NULL),
(4, 'browser', 'Linux', 686, 0, NULL),
(5, 'browser', 'Unknown OS Platform', 9388, 0, NULL),
(6, 'browser', 'Windows 7', 1119, 0, NULL),
(7, 'referral', 'yandex.ru', 16, 0, NULL),
(8, 'browser', 'Windows 8.1', 671, 0, NULL),
(9, 'referral', 'www.google.com', 1069, 0, NULL),
(10, 'browser', 'Android', 1351, 0, NULL),
(11, 'browser', 'Mac OS X', 1032, 0, NULL),
(12, 'referral', 'l.facebook.com', 21, 0, NULL),
(13, 'referral', 'codecanyon.net', 6, 0, NULL),
(14, 'browser', 'Windows XP', 57, 0, NULL),
(15, 'browser', 'Windows 8', 25, 0, NULL),
(16, 'browser', 'iPad', 43, 0, NULL),
(17, 'browser', 'Ubuntu', 157, 0, NULL),
(18, 'browser', 'iPhone', 693, 0, NULL),
(19, 'referral', 'www.google.co.jp', 23, 0, NULL),
(20, 'referral', 'horizonwireless.myshopify.com', 8, 0, NULL),
(21, 'referral', 'duckduckgo.com', 10, 0, NULL),
(22, 'referral', 'anti-crisis-seo.com', 10, 0, NULL),
(23, 'referral', 'l.instagram.com', 9, 0, NULL),
(24, 'referral', 'www.bing.com', 50, 0, NULL),
(25, 'referral', 'mediacomtoday.com', 1, 0, NULL),
(26, 'referral', 'www.google.ca', 14, 0, NULL),
(27, 'referral', 'l.wl.co', 27, 0, NULL),
(28, 'referral', 'indyhealthnet.org', 11, 0, NULL),
(29, 'referral', 'www.groupon.com', 1, 0, NULL),
(30, 'referral', 'www.shcrose.com', 1, 0, NULL),
(31, 'browser', 'Windows Vista', 18, 0, NULL),
(32, 'referral', 'google.com', 95, 0, NULL),
(33, 'referral', 'www.beinewsecurities.com', 1, 0, NULL),
(34, 'referral', 'global.bing.com', 1, 0, NULL),
(35, 'referral', 'www.netcraft.com', 3, 0, NULL),
(36, 'referral', 'baidu.com', 31, 0, NULL),
(37, 'referral', 'www.lamberthome.top', 1, 0, NULL),
(38, 'browser', 'Windows 2000', 11, 0, NULL),
(39, 'referral', 'search.yahoo.com', 8, 0, NULL),
(40, 'referral', 'www.google.de', 4, 0, NULL),
(41, 'referral', 'lm.facebook.com', 2, 0, NULL),
(42, 'referral', 'www.imdpm.net', 1, 0, NULL),
(43, 'referral', NULL, 25, 0, NULL),
(44, 'referral', 'server.horizonwirelesstx.com', 3, 0, NULL),
(45, 'referral', 'www.google.com.au', 1, 0, NULL),
(46, 'referral', 'hnlmh.pctip.net', 1, 0, NULL),
(47, 'referral', 'objcj.rfesc.net', 1, 0, NULL),
(48, 'referral', 'buhba.hxfoot.com', 1, 0, NULL),
(49, 'referral', 'czapsp.hxfoot.com', 1, 0, NULL),
(50, 'referral', 'yqkg.meansung.com', 1, 0, NULL),
(51, 'referral', 'xhcp.meansung.com', 1, 0, NULL),
(52, 'referral', 'www.google.com.hk', 1, 0, NULL),
(53, 'referral', 'www.shopify.com', 2, 0, NULL),
(54, 'referral', 'search.aol.com', 1, 0, NULL),
(55, 'referral', 'www.linkedin.com', 3, 0, NULL),
(56, 'referral', 'r.search.yahoo.com', 3, 0, NULL),
(57, 'referral', 'pkw.lightenupnh.org', 1, 0, NULL),
(58, 'referral', 'acrhxp.lightenupnh.org', 1, 0, NULL),
(59, 'referral', 'www.google.com.pk', 1, 0, NULL),
(60, 'referral', 'odlr.prizesk.com', 1, 0, NULL),
(61, 'referral', 'sucuri.net', 2, 0, NULL),
(62, 'referral', 'sov.skfusion.com', 1, 0, NULL),
(63, 'referral', 'search.tb.ask.com', 1, 0, NULL),
(64, 'referral', 'www.horizonwirelesstx.com', 2, 0, NULL),
(65, 'referral', 'com.google.android.googlequicksearchbox', 1, 0, NULL),
(66, 'referral', 'searchassist.verizon.com', 1, 0, NULL),
(67, 'referral', 'brzeg-3009691323.w-poblizu.pl', 2, 0, NULL),
(68, 'referral', 'www.google.co.in', 3, 0, NULL),
(69, 'referral', 'www.google.com.pr', 1, 0, NULL),
(70, 'referral', 'search.earthlink.net', 2, 0, NULL),
(71, 'referral', 'www.ediblearrangements.com', 1, 0, NULL),
(72, 'referral', 'HORIZONWIRELESSTX.COM', 5, 0, NULL),
(73, 'referral', 'www.coralbruce.top', 1, 0, NULL),
(74, 'referral', 'www.google.com.sg', 1, 0, NULL),
(75, 'referral', 'vyolw.ammgm.org', 1, 0, NULL),
(76, 'referral', ' www.horizonwirelesstx.com', 4, 0, NULL),
(77, 'referral', ' horizonwirelesstx.com', 2, 0, NULL),
(78, 'referral', 'www.maaco.com', 1, 0, NULL),
(79, 'referral', 'www.google.cl', 2, 0, NULL),
(80, 'referral', 'hugwv.fxrst.com', 1, 0, NULL),
(81, 'referral', 'www.google.fr', 1, 0, NULL),
(82, 'referral', 'greencitizen.com', 1, 0, NULL),
(83, 'referral', 'bmbu.immobilieralgerie.net', 1, 0, NULL),
(84, 'referral', 'www.google.co.uk', 2, 0, NULL),
(85, 'referral', 'www.yahoo.com', 2, 0, NULL),
(86, 'referral', 'fbkwriter.com', 2, 0, NULL),
(87, 'referral', '67.200.159.253', 1, 0, NULL),
(88, 'referral', 'pfspvb.pjty9.com', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CD', 'Democratic Republic of the Congo'),
(50, 'CG', 'Republic of Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DK', 'Denmark'),
(58, 'DJ', 'Djibouti'),
(59, 'DM', 'Dominica'),
(60, 'DO', 'Dominican Republic'),
(61, 'TP', 'East Timor'),
(62, 'EC', 'Ecuador'),
(63, 'EG', 'Egypt'),
(64, 'SV', 'El Salvador'),
(65, 'GQ', 'Equatorial Guinea'),
(66, 'ER', 'Eritrea'),
(67, 'EE', 'Estonia'),
(68, 'ET', 'Ethiopia'),
(69, 'FK', 'Falkland Islands (Malvinas)'),
(70, 'FO', 'Faroe Islands'),
(71, 'FJ', 'Fiji'),
(72, 'FI', 'Finland'),
(73, 'FR', 'France'),
(74, 'FX', 'France, Metropolitan'),
(75, 'GF', 'French Guiana'),
(76, 'PF', 'French Polynesia'),
(77, 'TF', 'French Southern Territories'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germany'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GK', 'Guernsey'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'IM', 'Isle of Man'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran (Islamic Republic of)'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'CI', 'Ivory Coast'),
(109, 'JE', 'Jersey'),
(110, 'JM', 'Jamaica'),
(111, 'JP', 'Japan'),
(112, 'JO', 'Jordan'),
(113, 'KZ', 'Kazakhstan'),
(114, 'KE', 'Kenya'),
(115, 'KI', 'Kiribati'),
(116, 'KP', 'Korea, Democratic People\'s Republic of'),
(117, 'KR', 'Korea, Republic of'),
(118, 'XK', 'Kosovo'),
(119, 'KW', 'Kuwait'),
(120, 'KG', 'Kyrgyzstan'),
(121, 'LA', 'Lao People\'s Democratic Republic'),
(122, 'LV', 'Latvia'),
(123, 'LB', 'Lebanon'),
(124, 'LS', 'Lesotho'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libyan Arab Jamahiriya'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lithuania'),
(129, 'LU', 'Luxembourg'),
(130, 'MO', 'Macau'),
(131, 'MK', 'North Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MW', 'Malawi'),
(134, 'MY', 'Malaysia'),
(135, 'MV', 'Maldives'),
(136, 'ML', 'Mali'),
(137, 'MT', 'Malta'),
(138, 'MH', 'Marshall Islands'),
(139, 'MQ', 'Martinique'),
(140, 'MR', 'Mauritania'),
(141, 'MU', 'Mauritius'),
(142, 'TY', 'Mayotte'),
(143, 'MX', 'Mexico'),
(144, 'FM', 'Micronesia, Federated States of'),
(145, 'MD', 'Moldova, Republic of'),
(146, 'MC', 'Monaco'),
(147, 'MN', 'Mongolia'),
(148, 'ME', 'Montenegro'),
(149, 'MS', 'Montserrat'),
(150, 'MA', 'Morocco'),
(151, 'MZ', 'Mozambique'),
(152, 'MM', 'Myanmar'),
(153, 'NA', 'Namibia'),
(154, 'NR', 'Nauru'),
(155, 'NP', 'Nepal'),
(156, 'NL', 'Netherlands'),
(157, 'AN', 'Netherlands Antilles'),
(158, 'NC', 'New Caledonia'),
(159, 'NZ', 'New Zealand'),
(160, 'NI', 'Nicaragua'),
(161, 'NE', 'Niger'),
(162, 'NG', 'Nigeria'),
(163, 'NU', 'Niue'),
(164, 'NF', 'Norfolk Island'),
(165, 'MP', 'Northern Mariana Islands'),
(166, 'NO', 'Norway'),
(167, 'OM', 'Oman'),
(168, 'PK', 'Pakistan'),
(169, 'PW', 'Palau'),
(170, 'PS', 'Palestine'),
(171, 'PA', 'Panama'),
(172, 'PG', 'Papua New Guinea'),
(173, 'PY', 'Paraguay'),
(174, 'PE', 'Peru'),
(175, 'PH', 'Philippines'),
(176, 'PN', 'Pitcairn'),
(177, 'PL', 'Poland'),
(178, 'PT', 'Portugal'),
(179, 'PR', 'Puerto Rico'),
(180, 'QA', 'Qatar'),
(181, 'RE', 'Reunion'),
(182, 'RO', 'Romania'),
(183, 'RU', 'Russian Federation'),
(184, 'RW', 'Rwanda'),
(185, 'KN', 'Saint Kitts and Nevis'),
(186, 'LC', 'Saint Lucia'),
(187, 'VC', 'Saint Vincent and the Grenadines'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'Sao Tome and Principe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SK', 'Slovakia'),
(198, 'SI', 'Slovenia'),
(199, 'SB', 'Solomon Islands'),
(200, 'SO', 'Somalia'),
(201, 'ZA', 'South Africa'),
(202, 'GS', 'South Georgia South Sandwich Islands'),
(203, 'SS', 'South Sudan'),
(204, 'ES', 'Spain'),
(205, 'LK', 'Sri Lanka'),
(206, 'SH', 'St. Helena'),
(207, 'PM', 'St. Pierre and Miquelon'),
(208, 'SD', 'Sudan'),
(209, 'SR', 'Suriname'),
(210, 'SJ', 'Svalbard and Jan Mayen Islands'),
(211, 'SZ', 'Swaziland'),
(212, 'SE', 'Sweden'),
(213, 'CH', 'Switzerland'),
(214, 'SY', 'Syrian Arab Republic'),
(215, 'TW', 'Taiwan'),
(216, 'TJ', 'Tajikistan'),
(217, 'TZ', 'Tanzania, United Republic of'),
(218, 'TH', 'Thailand'),
(219, 'TG', 'Togo'),
(220, 'TK', 'Tokelau'),
(221, 'TO', 'Tonga'),
(222, 'TT', 'Trinidad and Tobago'),
(223, 'TN', 'Tunisia'),
(224, 'TR', 'Turkey'),
(225, 'TM', 'Turkmenistan'),
(226, 'TC', 'Turks and Caicos Islands'),
(227, 'TV', 'Tuvalu'),
(228, 'UG', 'Uganda'),
(229, 'UA', 'Ukraine'),
(230, 'AE', 'United Arab Emirates'),
(231, 'GB', 'United Kingdom'),
(232, 'US', 'United States'),
(233, 'UM', 'United States minor outlying islands'),
(234, 'UY', 'Uruguay'),
(235, 'UZ', 'Uzbekistan'),
(236, 'VU', 'Vanuatu'),
(237, 'VA', 'Vatican City State'),
(238, 'VE', 'Venezuela'),
(239, 'VN', 'Vietnam'),
(240, 'VG', 'Virgin Islands (British)'),
(241, 'VI', 'Virgin Islands (U.S.)'),
(242, 'WF', 'Wallis and Futuna Islands'),
(243, 'EH', 'Western Sahara'),
(244, 'YE', 'Yemen'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `price` double NOT NULL,
  `times` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `type`, `price`, `times`, `used`, `status`, `start_date`, `end_date`) VALUES
(1, 'eqwe', 1, 12.22, '990', 18, 1, '2019-01-15', '2026-08-20'),
(2, 'sdsdsasd', 0, 11, NULL, 2, 1, '2019-05-23', '2022-05-26'),
(3, 'werwd', 0, 22, NULL, 3, 1, '2019-05-23', '2023-06-08'),
(4, 'asdasd', 1, 23.5, NULL, 1, 1, '2019-05-23', '2020-05-28'),
(5, 'kopakopakopa', 0, 40, NULL, 3, 1, '2019-05-23', '2032-05-20'),
(6, 'rererere', 1, 9, '665', 1, 1, '2019-05-23', '2022-05-26');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(191) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `sign`, `value`, `is_default`) VALUES
(1, 'USD', '$', 1, 1),
(4, 'BDT', '৳', 84.63, 0),
(6, 'EUR', '€', 0.89, 0),
(8, 'INR', '₹', 68.95, 0),
(9, 'NGN', '₦', 363.919, 0);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(191) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Tier 1', '2020-05-28 03:44:34', '2020-06-23 16:39:06', NULL),
(5, 'Tier 2', '2020-05-30 02:11:55', '2020-06-18 09:09:13', NULL),
(6, 'Tier 3', '2020-11-24 00:07:10', '2020-11-24 00:07:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `email_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_subject` mediumtext COLLATE utf8_unicode_ci,
  `email_body` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_type`, `email_subject`, `email_body`, `status`) VALUES
(1, 'new_order', 'Your Order Placed Successfully', '<p>Hello {customer_name},<br>Your Order Number is {order_number}<br>Your order has been placed successfully</p>', 1),
(2, 'new_registration', 'Welcome To Royal Commerce', '<p>Hello {customer_name},<br>You have successfully registered to {website_title}, We wish you will have a wonderful experience using our service.</p><p>Thank You<br></p>', 1),
(3, 'vendor_accept', 'Your Vendor Account Activated', '<p>Hello {customer_name},<br>Your Vendor Account Activated Successfully. Please Login to your account and build your own shop.</p><p>Thank You<br></p>', 1),
(4, 'subscription_warning', 'Your subscrption plan will end after five days', '<p>Hello {customer_name},<br>Your subscription plan duration will end after five days. Please renew your plan otherwise all of your products will be deactivated.</p><p>Thank You<br></p>', 1),
(5, 'vendor_verification', 'Request for verification.', '<p>Hello {customer_name},<br>You are requested verify your account. Please send us photo of your passport.</p><p>Thank You<br></p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `details`, `status`) VALUES
(1, 'Right my front it wound cause fully', '<span style=\"color: rgb(70, 85, 65); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 1),
(3, 'Man particular insensible celebrated', '<span style=\"color: rgb(70, 85, 65); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 1),
(4, 'Civilly why how end viewing related', '<span style=\"color: rgb(70, 85, 65); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 0),
(5, 'Six started far placing saw respect', '<span style=\"color: rgb(70, 85, 65); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</span><br>', 0),
(6, 'She jointure goodness interest debat', '<div style=\"text-align: center;\"><div style=\"text-align: center;\"><br></div></div><div style=\"text-align: center;\"><span style=\"color: rgb(70, 85, 65); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\">Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.<br></span></div>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `favorite_sellers`
--

CREATE TABLE `favorite_sellers` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `vendor_id` int(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite_sellers`
--

INSERT INTO `favorite_sellers` (`id`, `user_id`, `vendor_id`) VALUES
(1, 22, 13);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `product_id`, `photo`) VALUES
(125, 122, '1568027503rFK94cnU.jpg'),
(126, 122, '1568027503i1X2FtIi.jpg'),
(127, 122, '156802750316jxawoZ.jpg'),
(128, 122, '1568027503QRBf290F.jpg'),
(129, 121, '1568027539SQqUc8Bu.jpg'),
(130, 121, '1568027539Zs5OTzjq.jpg'),
(131, 121, '1568027539C45VRZq1.jpg'),
(132, 121, '15680275398ovCzFnJ.jpg'),
(133, 120, '1568027565bJgX744G.jpg'),
(134, 120, '1568027565j0RPFUgX.jpg'),
(135, 120, '1568027565QGi6Lhyo.jpg'),
(136, 120, '15680275658MAY3VKp.jpg'),
(137, 119, '1568027610p9R6ivC6.jpg'),
(138, 119, '1568027610t2Aq7E5D.jpg'),
(139, 119, '1568027611ikz4n0fx.jpg'),
(140, 119, '15680276117BLgrCub.jpg'),
(141, 118, '156802763634t0c8tG.jpg'),
(142, 118, '1568027636fuJplSf3.jpg'),
(143, 118, '1568027636MXcgCQHU.jpg'),
(144, 118, '1568027636lfexGTpt.jpg'),
(145, 117, '1568027665rFHWlsAJ.jpg'),
(146, 117, '15680276655LPktA9k.jpg'),
(147, 117, '1568027665vcNWWq3u.jpg'),
(148, 117, '1568027665gQnqKhCw.jpg'),
(149, 116, '1568027692FPQpwtWN.jpg'),
(150, 116, '1568027692zBaGjOIC.jpg'),
(151, 116, '1568027692UXpDx63F.jpg'),
(152, 116, '1568027692KdIWbIGK.jpg'),
(153, 95, '1568027743xS8gHocM.jpg'),
(154, 95, '1568027743aVUOljdD.jpg'),
(155, 95, '156802774327OOA1Zj.jpg'),
(156, 95, '1568027743kGBx6mxa.jpg'),
(249, 186, '1591173870featured_1.jpg'),
(250, 186, '1591173870featured_2.jpg'),
(251, 186, '1591173870featured_3.jpg'),
(470, 300, '16056407222.jpeg'),
(471, 300, '16056407223.jpeg'),
(472, 300, '16056407224.jpeg'),
(473, 300, '16056407225.jpeg'),
(474, 301, '16056417792.jpeg'),
(475, 301, '16056417793.jpeg'),
(476, 301, '16056417794.jpeg'),
(477, 301, '16056417795.jpeg'),
(478, 302, '16056427641.jpg'),
(479, 303, '16056434761.jpg'),
(480, 303, '16056434762.jpg'),
(481, 303, '16056434765.jpg'),
(482, 304, '16056444741.jpg'),
(483, 304, '16056444742.jpg'),
(484, 305, '16056454351.jpg'),
(485, 305, '1605645435d.jpg'),
(486, 306, '16056457181.jpg'),
(487, 307, '16056504491.jpg'),
(488, 307, '16056504493.jpg'),
(489, 308, '16056508361.jpg'),
(490, 308, '16056508363.jpg'),
(491, 308, '16056508364.jpg'),
(492, 309, '16056515474.jpg'),
(493, 310, '1605711939blue.jpeg'),
(494, 310, '1605712047DTdz7X29.jpg'),
(495, 311, '16057125484.jpg'),
(496, 312, '16057133331.jpg'),
(497, 312, '16057133332.jpg'),
(498, 312, '16057133333.jpg'),
(499, 312, '16057133334.jpg'),
(500, 313, '16057137971.jpg'),
(501, 313, '160571379761-dEvsdZdL._AC_SL1500_.jpg'),
(502, 314, '16057142171.jpg'),
(503, 315, '16057148521.jpg'),
(504, 316, '16057162491.jpg'),
(505, 317, '16057174811 (1).jpg'),
(506, 317, '16057174811.jpg'),
(507, 318, '16057215111.jpg'),
(508, 319, '16057217527.jpg'),
(509, 321, '16057285811.jpg'),
(510, 321, '1605728581d.jpg'),
(511, 322, '16057288431.jpg'),
(512, 324, '16057299091.jpg'),
(513, 325, '16057313341.jpg'),
(514, 325, '1605731334d.jpg'),
(515, 329, '16057325281.jpg'),
(516, 329, '1605732528d.jpg'),
(517, 339, '16063146329QhfHh74.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `generalsettings`
--

CREATE TABLE `generalsettings` (
  `id` int(191) NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_email` text COLLATE utf8mb4_unicode_ci,
  `header_phone` text COLLATE utf8mb4_unicode_ci,
  `footer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `colors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loader` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_loader` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_talkto` tinyint(1) NOT NULL DEFAULT '1',
  `talkto` text COLLATE utf8mb4_unicode_ci,
  `is_language` tinyint(1) NOT NULL DEFAULT '1',
  `is_loader` tinyint(1) NOT NULL DEFAULT '1',
  `map_key` text COLLATE utf8mb4_unicode_ci,
  `is_disqus` tinyint(1) NOT NULL DEFAULT '0',
  `disqus` longtext COLLATE utf8mb4_unicode_ci,
  `is_contact` tinyint(1) NOT NULL DEFAULT '0',
  `is_faq` tinyint(1) NOT NULL DEFAULT '0',
  `guest_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_check` tinyint(1) NOT NULL DEFAULT '0',
  `cod_check` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_key` text COLLATE utf8mb4_unicode_ci,
  `stripe_secret` text COLLATE utf8mb4_unicode_ci,
  `currency_format` tinyint(1) NOT NULL DEFAULT '0',
  `withdraw_fee` double NOT NULL DEFAULT '0',
  `withdraw_charge` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `smtp_host` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_smtp` tinyint(1) NOT NULL DEFAULT '0',
  `is_comment` tinyint(1) NOT NULL DEFAULT '1',
  `is_currency` tinyint(1) NOT NULL DEFAULT '1',
  `add_cart` text COLLATE utf8mb4_unicode_ci,
  `out_stock` text COLLATE utf8mb4_unicode_ci,
  `add_wish` text COLLATE utf8mb4_unicode_ci,
  `already_wish` text COLLATE utf8mb4_unicode_ci,
  `wish_remove` text COLLATE utf8mb4_unicode_ci,
  `add_compare` text COLLATE utf8mb4_unicode_ci,
  `already_compare` text COLLATE utf8mb4_unicode_ci,
  `compare_remove` text COLLATE utf8mb4_unicode_ci,
  `color_change` text COLLATE utf8mb4_unicode_ci,
  `coupon_found` text COLLATE utf8mb4_unicode_ci,
  `no_coupon` text COLLATE utf8mb4_unicode_ci,
  `already_coupon` text COLLATE utf8mb4_unicode_ci,
  `order_title` text COLLATE utf8mb4_unicode_ci,
  `order_text` text COLLATE utf8mb4_unicode_ci,
  `is_affilate` tinyint(1) NOT NULL DEFAULT '1',
  `affilate_charge` int(100) NOT NULL DEFAULT '0',
  `affilate_banner` text COLLATE utf8mb4_unicode_ci,
  `already_cart` text COLLATE utf8mb4_unicode_ci,
  `fixed_commission` double NOT NULL DEFAULT '0',
  `percentage_commission` double NOT NULL DEFAULT '0',
  `multiple_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `multiple_packaging` tinyint(4) NOT NULL DEFAULT '0',
  `vendor_ship_info` tinyint(1) NOT NULL DEFAULT '0',
  `reg_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `cod_text` text COLLATE utf8mb4_unicode_ci,
  `paypal_text` text COLLATE utf8mb4_unicode_ci,
  `stripe_text` text COLLATE utf8mb4_unicode_ci,
  `header_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin_loader` tinyint(1) NOT NULL DEFAULT '0',
  `menu_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_hover_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `is_verification_email` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_text` text COLLATE utf8mb4_unicode_ci,
  `is_instamojo` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_sandbox` tinyint(1) NOT NULL DEFAULT '0',
  `is_paystack` tinyint(1) NOT NULL DEFAULT '0',
  `paystack_key` text COLLATE utf8mb4_unicode_ci,
  `paystack_email` text COLLATE utf8mb4_unicode_ci,
  `paystack_text` text COLLATE utf8mb4_unicode_ci,
  `wholesell` int(191) NOT NULL DEFAULT '0',
  `is_capcha` tinyint(1) NOT NULL DEFAULT '0',
  `error_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_popup` tinyint(1) NOT NULL DEFAULT '0',
  `popup_title` text COLLATE utf8mb4_unicode_ci,
  `popup_text` text COLLATE utf8mb4_unicode_ci,
  `popup_background` text COLLATE utf8mb4_unicode_ci,
  `invoice_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_secure` tinyint(1) NOT NULL DEFAULT '0',
  `allow_inks` tinyint(1) NOT NULL DEFAULT '0',
  `is_report` tinyint(1) NOT NULL,
  `paypal_check` tinyint(1) DEFAULT '0',
  `paypal_business` text COLLATE utf8mb4_unicode_ci,
  `footer_logo` text COLLATE utf8mb4_unicode_ci,
  `email_encryption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paytm_merchant` text COLLATE utf8mb4_unicode_ci,
  `paytm_secret` text COLLATE utf8mb4_unicode_ci,
  `paytm_website` text COLLATE utf8mb4_unicode_ci,
  `paytm_industry` text COLLATE utf8mb4_unicode_ci,
  `is_paytm` int(11) NOT NULL DEFAULT '1',
  `paytm_text` text COLLATE utf8mb4_unicode_ci,
  `paytm_mode` enum('sandbox','live') CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `is_molly` tinyint(1) NOT NULL DEFAULT '0',
  `molly_key` text COLLATE utf8mb4_unicode_ci,
  `molly_text` text COLLATE utf8mb4_unicode_ci,
  `is_razorpay` int(11) NOT NULL DEFAULT '1',
  `razorpay_key` text COLLATE utf8mb4_unicode_ci,
  `razorpay_secret` text COLLATE utf8mb4_unicode_ci,
  `razorpay_text` text COLLATE utf8mb4_unicode_ci,
  `show_stock` tinyint(1) NOT NULL DEFAULT '0',
  `is_maintain` tinyint(1) NOT NULL DEFAULT '0',
  `maintain_text` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generalsettings`
--

INSERT INTO `generalsettings` (`id`, `logo`, `favicon`, `title`, `header_email`, `header_phone`, `footer`, `copyright`, `colors`, `loader`, `admin_loader`, `is_talkto`, `talkto`, `is_language`, `is_loader`, `map_key`, `is_disqus`, `disqus`, `is_contact`, `is_faq`, `guest_checkout`, `stripe_check`, `cod_check`, `stripe_key`, `stripe_secret`, `currency_format`, `withdraw_fee`, `withdraw_charge`, `tax`, `shipping_cost`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `from_email`, `from_name`, `is_smtp`, `is_comment`, `is_currency`, `add_cart`, `out_stock`, `add_wish`, `already_wish`, `wish_remove`, `add_compare`, `already_compare`, `compare_remove`, `color_change`, `coupon_found`, `no_coupon`, `already_coupon`, `order_title`, `order_text`, `is_affilate`, `affilate_charge`, `affilate_banner`, `already_cart`, `fixed_commission`, `percentage_commission`, `multiple_shipping`, `multiple_packaging`, `vendor_ship_info`, `reg_vendor`, `cod_text`, `paypal_text`, `stripe_text`, `header_color`, `footer_color`, `copyright_color`, `is_admin_loader`, `menu_color`, `menu_hover_color`, `is_home`, `is_verification_email`, `instamojo_key`, `instamojo_token`, `instamojo_text`, `is_instamojo`, `instamojo_sandbox`, `is_paystack`, `paystack_key`, `paystack_email`, `paystack_text`, `wholesell`, `is_capcha`, `error_banner`, `is_popup`, `popup_title`, `popup_text`, `popup_background`, `invoice_logo`, `user_image`, `vendor_color`, `is_secure`, `allow_inks`, `is_report`, `paypal_check`, `paypal_business`, `footer_logo`, `email_encryption`, `paytm_merchant`, `paytm_secret`, `paytm_website`, `paytm_industry`, `is_paytm`, `paytm_text`, `paytm_mode`, `is_molly`, `molly_key`, `molly_text`, `is_razorpay`, `razorpay_key`, `razorpay_secret`, `razorpay_text`, `show_stock`, `is_maintain`, `maintain_text`) VALUES
(1, '1589597927logo.png', '1589598859logo.png', 'Horizon Wireless', 'smtp', '0123 456789', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae', 'COPYRIGHT © 2020. All Rights Reserved By <a href=\"https://mrm-soft.com/\" title=\"MRM-Soft\" target=\"_blank\">MRM-Soft.com</a>', '#14407d', '1592224445loading.gif', '1592224443loading.gif', 0, '<script type=\"text/javascript\">\r\nvar Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n(function(){\r\nvar s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];\r\ns1.async=true;\r\ns1.src=\'https://embed.tawk.to/5bc2019c61d0b77092512d03/default\';\r\ns1.charset=\'UTF-8\';\r\ns1.setAttribute(\'crossorigin\',\'*\');\r\ns0.parentNode.insertBefore(s1,s0);\r\n})();\r\n</script>', 1, 0, 'AIzaSyB1GpE4qeoJ__70UZxvX9CTMUTZRZNHcu8', 0, '<div id=\"disqus_thread\">         \r\n    <script>\r\n    /**\r\n    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.\r\n    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/\r\n    /*\r\n    var disqus_config = function () {\r\n    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page\'s canonical URL variable\r\n    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page\'s unique identifier variable\r\n    };\r\n    */\r\n    (function() { // DON\'T EDIT BELOW THIS LINE\r\n    var d = document, s = d.createElement(\'script\');\r\n    s.src = \'https://junnun.disqus.com/embed.js\';\r\n    s.setAttribute(\'data-timestamp\', +new Date());\r\n    (d.head || d.body).appendChild(s);\r\n    })();\r\n    </script>\r\n    <noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>\r\n    </div>', 1, 1, 1, 0, 1, 'pk_test_UnU1Coi1p5qFGwtpjZMRMgJM', 'sk_test_QQcg3vGsKRPlW6T3dXcNJsor', 0, 5, 5, 0, 5, 'mail.horizonwirelesstx.com', '465', 'info@horizonwirelesstx.com', 'admin@123#@!', 'info@horizonwirelesstx.com', 'Horizon Wireless', 1, 1, 1, 'Successfully Added To Cart', 'Out Of Stock', 'Add To Wishlist', 'Already Added To Wishlist', 'Successfully Removed From The Wishlist', 'Successfully Added To Compare', 'Already Added To Compare', 'Successfully Removed From The Compare', 'Successfully Changed The Color', 'Coupon Found', 'No Coupon Found', 'Coupon Already Applied', 'THANK YOU FOR YOUR PURCHASE.', 'We\'ll email you an order confirmation with details and tracking info.', 1, 8, '15587771131554048228onepiece.jpeg', 'Already Added To Cart', 5, 5, 1, 1, 1, 1, 'Pay with cash upon delivery.', 'Pay via your PayPal account.', 'Pay via your Credit Card.', '#ffffff', '#232323', '#ededed', 1, '#ff5500', '#02020c', 0, 1, 'test_172371aa837ae5cad6047dc3052', 'test_4ac5a785e25fc596b67dbc5c267', 'Pay via your Instamojo account.', 0, 1, 0, 'pk_test_162a56d42131cbb01932ed0d2c48f9cb99d8e8e2', 'junnuns@gmail.com', 'Pay via your Paystack account.', 6, 1, '1566878455404.png', 0, 'NEWSLETTER', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita porro ipsa nulla, alias, ab minus dummy.', '1584934329adv-banner.jpg', '1589598709logo.png', '1567655174profile.jpg', '#666666', 1, 1, 1, 1, 'horizonwirelesstxonline@gmail.com', '1571567309footers.png', 'tls', 'tkogux49985047638244', 'LhNGUUKE9xCQ9xY8', 'WEBSTAGING', 'Retail', 0, 'Pay via your Paytm account.', 'sandbox', 0, 'test_5HcWVs9qc5pzy36H9Tu9mwAyats33J', 'Pay with Molly Payment.', 0, 'rzp_test_xDH74d48cwl8DF', 'cr0H1BiQ20hVzhpHfHuNbGri', 'Pay via your Razorpay account.', 1, 0, '<div style=\"text-align: center;\"><font size=\"5\"><br></font></div><h1 style=\"text-align: center;\"><font size=\"6\">UNDER MAINTENANCE</font></h1>');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(191) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `is_default`, `language`, `file`) VALUES
(1, 1, 'English', '1579926860LzpDa1Y7.json'),
(2, 0, 'RTL English', '1579927527QjLMUGyj.json');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(191) NOT NULL,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) DEFAULT NULL,
  `recieved_user` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(191) NOT NULL,
  `order_id` int(191) UNSIGNED DEFAULT NULL,
  `user_id` int(191) DEFAULT NULL,
  `vendor_id` int(191) DEFAULT NULL,
  `product_id` int(191) DEFAULT NULL,
  `conversation_id` int(191) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `order_id`, `user_id`, `vendor_id`, `product_id`, `conversation_id`, `is_read`, `created_at`, `updated_at`) VALUES
(1, NULL, 29, NULL, NULL, NULL, 1, '2020-05-15 21:52:47', '2020-06-16 06:06:40'),
(2, 2, NULL, NULL, NULL, NULL, 1, '2020-05-27 13:12:12', '2020-06-16 06:06:46'),
(3, 3, NULL, NULL, NULL, NULL, 1, '2020-06-01 06:58:08', '2020-06-16 06:06:46'),
(4, 4, NULL, NULL, NULL, NULL, 1, '2020-06-01 07:09:46', '2020-06-16 06:06:45'),
(5, 5, NULL, NULL, NULL, NULL, 1, '2020-06-01 07:14:06', '2020-06-16 06:06:45'),
(6, 6, NULL, NULL, NULL, NULL, 1, '2020-06-01 07:19:39', '2020-06-16 06:06:45'),
(7, 7, NULL, NULL, NULL, NULL, 1, '2020-06-05 04:39:02', '2020-06-16 06:06:45'),
(8, 8, NULL, NULL, NULL, NULL, 1, '2020-06-05 04:43:35', '2020-06-16 06:06:45'),
(9, 9, NULL, NULL, NULL, NULL, 1, '2020-06-05 04:48:27', '2020-06-16 06:06:45'),
(10, 10, NULL, NULL, NULL, NULL, 1, '2020-06-16 04:36:59', '2020-06-16 06:06:45'),
(11, NULL, 30, NULL, NULL, NULL, 1, '2020-06-16 04:54:29', '2020-06-16 06:06:40'),
(12, 11, NULL, NULL, NULL, NULL, 1, '2020-06-16 04:55:44', '2020-06-16 06:06:45'),
(13, 12, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:03:00', '2020-06-16 06:06:45'),
(14, 13, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:37:10', '2020-06-16 06:06:45'),
(15, 14, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:37:56', '2020-06-16 06:06:45'),
(16, 15, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:38:56', '2020-06-16 06:06:45'),
(17, 16, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:40:48', '2020-06-16 06:06:45'),
(18, 17, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:42:30', '2020-06-16 06:06:45'),
(19, 18, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:49:11', '2020-06-16 06:06:45'),
(20, 19, NULL, NULL, NULL, NULL, 1, '2020-06-16 05:52:33', '2020-06-16 06:06:45'),
(21, 20, NULL, NULL, NULL, NULL, 1, '2020-06-17 09:49:00', '2020-06-20 10:47:08'),
(22, NULL, 32, NULL, NULL, NULL, 1, '2020-06-19 09:11:58', '2020-07-06 08:43:25'),
(23, NULL, 33, NULL, NULL, NULL, 1, '2020-06-19 09:24:55', '2020-07-06 08:43:25'),
(24, 21, NULL, NULL, NULL, NULL, 1, '2020-06-19 09:26:09', '2020-06-20 10:47:08'),
(25, 22, NULL, NULL, NULL, NULL, 1, '2020-06-20 00:33:35', '2020-06-20 10:47:08'),
(26, NULL, 31, NULL, NULL, NULL, 1, '2020-06-20 10:17:24', '2020-07-06 08:43:24'),
(27, 23, NULL, NULL, NULL, NULL, 1, '2020-06-20 10:46:00', '2020-06-20 10:47:08'),
(28, 24, NULL, NULL, NULL, NULL, 1, '2020-06-20 10:47:56', '2020-07-06 08:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `cart` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pickup_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalQty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_amount` float NOT NULL,
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(255) NOT NULL,
  `customer_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `coupon_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_discount` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `affilate_user` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `affilate_charge` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_value` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `packing_cost` double NOT NULL DEFAULT '0',
  `tax` int(191) NOT NULL,
  `dp` tinyint(1) NOT NULL DEFAULT '0',
  `pay_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_shipping_id` int(191) NOT NULL DEFAULT '0',
  `vendor_packing_id` int(191) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `cart`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(1, 27, 'BZh91AY&SYÄµ\0ßPXø;oþ¿ïÿú`=÷@ æ\00M0\0\04Ð02hOEÐ\0\0Ð\0æ\00M0\0\04Ð0))§©ÈÑ\Zd\0\0\0\09À&\0L&L\0\0&M4$Dòb)íQ<MG Ô14ÙMêM<SP)´\nmëè=ÄGØÀìïOÙòó>±rFK_°ÄoýH»¬~Ã$3¡ñ=ä¡~D ·³ äi\"üä­g3Aå%z|)ZASÑdHÔ]l5Î¤óÐ_Ì{?NÃ7ewq^Ô xï<Îfí<N¶7d¤BCÔ{\raIë=A:úQrLK$-³6sAIq\'\'#CÔzÌ)ù	I ÆÆòDl\r1¦-ôÛ}Yc#$É^Ñ¥°Å`Á\0BYS<Vcâ-yW¹Q)Lh¦+5ð.³([Aª\n\n,|UWâq(\\±{°l)y¦D* Ñ­6a6ËVfÖ ©«®Ã0A¸¡B\\\n,H{!AU&eë1XªxÅxÄ0©A2ówjÜ*¨¡Rö¢h\nZõ¨­aIT¸AR¤««ZJª¾Åü$.ÈÑ¨ ,pMZ8ñ(,Ã$¡ÑÃÑ\Zu&°	Ia`Ó2B¥\n-ãvèît1Eö1k<%a+C}¥P²|8TÂµëf\" BÓRÂ´ÖÃ´Gã	DG Ã M!z5Z1gwÊÔd.x<i5â,V$ÈÂ\Zç¾#Ào1e(dÉ°r¹5°ÐÁ ÌþOoxÿ±ã­öqUiU4¼o>ÌøæÄd­å¤ ¦±î<²D]39äQIÄ8SVjr§5¸kò!hÅSîÖ9R\0Ôò0jo3Ð¾U;Ñ¸wK#6\r\r0÷C,²Ç¤ÔôÛhÁ0Ê¥càdÆHÅÐ?\'é<þ¨wwçßL±íÚc\nçæHÏäØâfXiIÁyÐ²õ]ya¹8èo­M8SPvó>?À×CÞxdªu5í7;p|(P©ä©ý4.Ïxz<(GþxÑ^fÄÖtÕ\'×é±=Á°3²aädAéÐ$F·gzÝô$Üóô.k¼láÚe]ç _t\nv÷t/ÅJ`\n5¤ÌI;Dò¨Ê¬* bð0¡*`rÍZ *KÄR8Úf¾æj¨Zòiª0uùà²°Ó>ó$eøqº¥Rc´_!þgqÞØTö{?SJViyÃÀmnâØEõ;Tã~Öc+\nTgd(<ÎvDÓfçþ×nß¹SÚÂ²1``XÂ\ZD(%F\rãlàÜç¯»Uë-cAB¸J2(D.h3:ôp*¦ðw\Zå71Ò35P133QS¨P\n,AôEJ¹®«èÀ¾ñqô:¥Ð!**(ºèæOÐ}8ès$(²èÐa¯F¯Sâ#W ñuMì?\049ü]ÉáB@Öx', 'Paypal', 'shipto', 'Azampur', '1', 130, NULL, NULL, '2csj1590602773', 'Pending', 'junajunnun@gmail.com', 'john doe', 'United States', '34534534', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-05-27 13:06:13', '2020-05-27 13:06:13', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(2, 27, 'BZh91AY&SYÉþ\0_PXø;oþ¿ïÿú`=÷@ÆÅ@h9À&\0L&L\0\0&M4%	¦MH4\0\0\0@M\09À&\0L&L\0\0&M4!JJi¦Ê\0È¨õ\0\0\0\0a0		Ó\0\0	M	&E<ADÞ¦QèdzOÔ\no­ôâ#ì`u\0÷§ìùzX¹B£%¯Øb7~]ÖH?aÐøÌÉ`Ê5tlÔ©*LÊ³¡K²ÝÒ³V¾Tày,\ZK­£\\ª@n=Eý´ïßîqÆÌ³0lì+Ö\0ÏÀ¡ÈÐô=¼cÓpÆAÖHs$<ÎÓXRyi\"aÊ¿aAj1.)`·83i×8´o8q4>ÃÌÂ\0Tm6´\ZcM[©·uYc#$É^Á¥´b°`!,©+±ñ¼«úÖ	Q0M l\Z¤Zmàal([Qª\n\n,|UWâu.X½Ø6¼ÌÓ\"JPhÖQXM¡2Õµ$jjæk¸f  ö(@ËARåi$(*²dÑ¢Ùf2«O8°R/(&^ní[U*^ÔM3 x¤æ$\\\n$zfI(U&áLÎwÈÔJP8&­øÎaPèáÈhÃ\r:X¤°ÄDÈ0i!BÒÃ]Ã¬Ó	\0¨Å¬ï:T­\rÖB2ÉïßRg\n)} ,(YKCX«È¬Dr´uTÖC5µkXûÇRê¥eD³Ñ®$åò \\)ª|tFpØ9âPÄ/9`ã©\raÓ\rC3ú>¿ûOÍhüYÁU¥TÐZñ¸@@9öfK§QÚ(1~¤Ñ3ÜyøÔÚ2rT¢p6¦¬:ÔÿeNK`×¸zÆ,Êv´Çû§SqÅò©ÜºY°hlYàp²«&§¨¶Ý°aJÇÀÈ7{ÉåÛîü»©Â±A0`0]°ÀÛR,a\\÷3ú6Ë\r31I28/:C>¢K¯Cë ×2G=CÍô©¦újn¾GÇù\ZÞq(~yõÊ§C^³c¶øÓ¾\nA80\ZH4.¯txzÎàÑþv¢½ó!Ä7ÎzLð>¯ØAT:#\"ITkfw­ÑABM?0ÀnBä»ÍýfUÜz÷A\0À§_qÓ@æ@Âñ!Þ,T¦\0£ZH`LÁQD±dO\ZªÂ¡B	&/\n¦,ÚJÐ±Pª^À½fkèo >f©oí¼E¢\Zeê>d÷¬¬4Ï¼ÉÓS¬ªPm:Åò¸ì ;\ni\'´Óô4©uf¸;ÆÖÁ	D_S¨ãNì`f2°¥HFy¦BÈi\\æATaÉ8¹{x÷»¹=¡ø0L+#	f\\!¤BrX$iÐØp6rWz]ªó-cAB¸J2(D.kl9¸Sx;\rrÂi¨ÈÏ©Ð(LA ù¢¥Ø×Eô`_pÏ¿°Æzæ]sDr\'è>|Þr$(²æÐA@Ö(¿ÀÊyËÚÚ ÇÇÉ	°¡È,âîH§\n?Ã\"à', 'Cash On Delivery', 'shipto', 'Azampur', '1', 130, NULL, NULL, 'f3uh1590603132', 'Pending', 'junajunnun@gmail.com', 'john doe', 'United States', '34534534', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-05-27 13:12:12', '2020-05-27 13:12:12', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(3, NULL, 'BZh91AY&SY9+5\0*ßP\0Xø;ïü¿ÿÿú`]ñ @\0 æÓ0\0\0\0L#b`#LÂ0\0\00i0#À\0\0Â0\nROHh4õ\Ziµ\r4õ\0Ð4@b`#LÂ0\0\00¢d\Z¥6J=O#)êzCM\r\rMI\"¹\\®eW;\rô0|#öü¿SóÅ*2ZüF#ÿ|H»¬~#%\Z|¢*GëX	6¬Q=ß6dÔÚÕ\"æ7Ò5¬fÿ´\Z\\d9LÉ$¦ûÈídQlïóäµÓ\rÅq{\Z;;ñ&5*µµ;¯²¶;NR£yòu;\rJÞc[AêtNCcVûbAP:ÏQ_´b[Ó¹	g6äV¦¦î`Ì$I$ ¸ÈÔìÞM\\1´4kmwP«.h+ípÆ\nì4¥uÈh\\Eª@ÅbV0TX¢`@Ø5M¦ñKàôÉR\\k(`u%QS¨ª$kI\\cÀÞP°´c 8ËÚfþÃ%©g3o¸0A©P¡.IK$7B¡QI<¬&ÓX f(â·¨R1(&^ní[U*^ÔMI.d)kÖ¢¶%RáJ:®­jUUùú×Í!v)]t¢!&Qj6²`¿1m±lhÐcBÖ[tØ«%¬XJmbFXÄI\r¨à÷ð\'hHYH\0ø[Lqcf)R´8ZH¡9ËãÆ¤Ío¦º4\ZÚ\rFjÍf53I3lVG\n0³	Íô¥æ*k¦ZÆºf2³7KZ^#ZAbEØ~óÌv°,lTï2Í´=ÇÀõî0­Æ#yB¸ê<\"ë!o{8Xª^6>éù÷\ZnÕAÌ÷ãã÷Á©Ô¤ÃØ½(¤äGSf.°6=¥NÕ¨×ÞB<F-\nÝ¨#³ÖÇc¦àbýêw£QÝ,0hl_PÿGZ4Ð;.xÎ%¸pªV?#!Ô2F.hþÉîïw~îÍI Á%Z`mª\nøV;I~æó¡a¦h)&Gçq	dg%×Ìôm¡#CÑô©»6	·>Óóý»C\ròO\0c=4SÐßÌÜíË¸gO\n<AM\rëM¯Ä½yã+ý,8¶Úâ¥FCQ¡ËÆÄù`g*H=ITk]/[¢>¤ÿ!àû« ÙË¥º\"úÁ\0À§>ó¦ÁÌâCì*S\0Q­Ò0TQ$ê²OmFUaP¡$aR`rÎ«(KÐ)+¨aò5K\\zÅ¢\Zeê?|l4Ïòe÷õGOäÞs,©Í/èæBïmµS¼Úm­\Z[ÃÎ6²3½3¼ë&û³²a\r-FV©×TÈPx\r+Äqm¸7<:ôðÞÉÅTÙÅ¹Y%,R¬¤ÅKb¤d¥Ù¡ºé¼úöÂ+Å/0ª½%¬n  £%Ü·Á¡ßÜàUMàófnc	¤hl c ,hi=S P\nY-ÃîEJ«_Øú´à3Ýó9v<æasDwô>²\0üâB!Í¢9±³U÷§¬^K¢Ðxøù!ZZKþ.äp¡ rVk<', 'Cash On Delivery', 'shipto', 'Azampur', '-2', 3584, NULL, NULL, 'pvtt1591012687', 'Pending', 'hello@wirelesswavestx.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 06:58:07', '2020-06-01 06:58:07', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(4, NULL, 'BZh91AY&SY¼Çä\0%_P\0Xø;ïü¿ÿÿú`_rQ(\n æÓ0\0\0\0L#b`#LÂ0\0\00 Òj¨A \021¤Ó Qé\Z\0Úh\0\0\0\0\09¦&4À#\0\0\0ÀHM	 &2)ä¦õ=#)úIA#õIKGÀóÏ¡É\0Å³¨ûeÄPä»ncz\rÃÇCì¢:ªMhÏ·¥ÌÉº5ÅBbòÞI³ªïñRic ±¼ðe Õ&\ZÎ2Hm-¨(I÷ìÖ~çV£VAõÂ¥»;Ä è	gC¡óæu£1OÐÜoÄ!è`Aænäy(yä}¥Ä¹&%	vP]§\\bÞZ±\'3W f	 XdfhH@ÁhbÎgB¬±\\d¯´a¬c\ZVCÇ\ZepÄ°\ZÀHñ¼«Ü¬´&Ð6\rLVkc¼ÅP¹¡«¢Ä#4-áÉvB©LâÇ$Áá5(@Öìe{Ð\rc6\Z-YyÒJ\rRÎfßp\\B$B rÉ ¢Ì	!CI4P¤ÕL9iâ»\n.2!òdræÞªÂ42¼&v^d.¨ld\nYXABu%`­jªªýæºÑp %\r!°HS GÉ¨j ÅEQ0ëi¸C³	11¡ L)D\r!É\0ÐÇ´Øõì\'HHX¤\0|-&72ìt©Z-*cÝº¤Ímy4ÚÆfÌíÒLmbJó±pÃçdâ ´FÌóÍ`sMæX¬fÈÎE¯U.£çèvMçpXÐ©ÌÅ2=çÞõÞ\\>½EãYB¨õñÄ-èÍÁN53°bãQGèÛ=óæ8A¦Øºh9¸ù~GøfmRGQí^ÚRo\ræc©£\0h{Vc_ð²*{´ ¯i\0hx46j/ê§4f<ÄÉCbøÀà²¬Y¸¶ÍraJÇêbFHÅÉùø¾pðxuô¦5$ä.\\.Ihaq¶¨,.¬q$gôk7\ZfBdpa:K$/¡ì Ó\"G6\'Ò¦­ÔÐ&Ü¸·ò5ÔÆ\Z=	àpÜ¤óC.¼jøì÷G8<H£³×<ã§¶ì^`ÏUä	/bb!òãC<gì\rèo;H<5¨Öya\\A¤ÿÐ¸lBþN+ Ù¿¶ë¹4D0ñ^¥.\ZÕ!q2åENkxÔeUÕ\nI1Ü^F(¦,ØJ°ÂD¾¡HàX­£4¶vwdQs§ø#V,t!A0g±|C¤¨?ô4)ªªs$÷\ZgdQ£Xv¬FsLæp\'\rGª¡\r}Y¬)Ri î\ZXdn0;sös·{í~HüÁ^p\"áLÒÁ@ÄÚ6\0CII6¤ÚdvçÆ]Év\n«ØZÆ¡BÀ%\"Z×G>·ªnça¦3{¦¢±õEN@*b±­CëEJÑ_éz°0Ø3ßôÓô;ä]rDuý\0ød+62ý¦\ZÆ«îO1x®_Ø ððñBl\n §ø»)Âæ?$Ø', 'Cash On Delivery', 'shipto', 'Azampur', '1', 700, NULL, NULL, 'yaDw1591013386', 'Pending', 'hello@wirelesswavestx.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 07:09:46', '2020-06-01 07:09:46', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(5, NULL, 'BZh91AY&SY>¼au\0%ßP\0Xø;ïü¿ÿÿú`]ÏC -H#b`#LÂ0\0\00i0#À\0\0Â0SÊÀ4j\0\0ÐM¢R¦Ò\0Äz\ZSÔ@h\0\0æÓ0\0\0\0L#\"!2&ÈÉG¦¨zz¦ÒyA¦Ò6z54$sW#Þªû0ì@a?§¹éu GTòCKÄjYq!GC:èHÞòÀæ24\"áì¦AêLm-UÚ1	e²Wê\\¾Ac*AmHäÉ\rÅ®h$ømÔüÍÌÃëÌ§BÜÄ;	Aâ	gÀÔùöGUQì|[îkc´Ðô·Mqµ¿âm+¬eBKÄþ©oê1/Ä®BBã²4ê:it6\ZÓ H Xdhm$FÓÐÅ²éB¬±Æ2J=£\r£Ò¹<bàÀY*\rdñ¼«äT¦	´\rSØú-°k!®BzQRhÉhx +ZLÛ!;\Z,&ÞôhÎâÀCBe«3o°õ$ Õ,æmöÈ3`aÈ aJbJ,8h2A(8ìÒI/,0¥B-4êÈCS\\åÂÜ)OK3A)ÉhEG]Â-S\"ZàPH bHZ²Vµ*ªûWÉ!q_ã@J\"\" b)øYlÌecIÈÆ4S]âYJbÂSLHÆ¦HÊÄ6>ãsÛ¸a!a â1k1½c¥JÐÝiT#{÷Ô­³Ó2FKA Í¥£C±%dxÈ QN2&ÑT#Lc°Ìó(Ëábl)ZõYÁ\"ä|½çÞp<Æ¥Kõ733î:ð96Í¢A÷ÞjLn5TIE@µ6\"\0ÁðÁósBàÕ(ÆÉï=ÇÇï>fr9«Ö¥ êjÃ\Zâ§zÐkï!ceOv´ËÔ5=öíSª4I`ÍCbø\ZÀâóYfÓymÛ \"äÃ*ø`;HÅÑÃó<úÃÉåÏµ1RH.IbåÂäjÊêÇy#?ciÀÌ°Ó3#)ØBXì$É~g¡¹8Ü<ßj7ÓPtï?ïê5È85*{É PÏp²FSKhÅmôÙË½¡PÑx¶&÷Rm+ý6N=×T¨Èp9«Æz{,OjáCä`Ï`I*iUÉô$Ð\'÷ä/Ôï]ÎíÜoÒ:u;jeP¯Rl¸r¢\'E{ê2ªê$ò/#\nSn%Xa@¢_X¤p,.áèhïoâ,ÐÓ2ª.vùä±a¦{Ì#èíûNqô:%ü:±¶ªu$ût²(Ñ´<FÖuLêq\'-7Â\Zö±h2°¥HF&BÈids |Aã§§]]ËÃvG)U6â1nVIK%«\"©1RØ Ë%,Ù£yÏpô5änoKÀU^¬l  QBæ¶Á×U7sÀ×{¦ª±òTÂÀ]l4T ]\Z5üÑáÛô\Zjy%Ð!**(ºèæOÌ}8ÄæHQatbÆ_ÄÔXªûFSì±vBù??b`Ø²\'ü]ÉáB@úñÔ', 'Cash On Delivery', 'shipto', 'Azampur', '1', 600, NULL, NULL, 'Ss0C1591013646', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 07:14:06', '2020-06-01 07:14:06', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(6, NULL, 'BZh91AY&SYrØô\0%ßP\0Xø;ïü¿ÿÿú`]÷M\0\nÈm@4ÄÀF`\0\0a	SSôTÐÐ Ð\0\0\0\0Ó\Z`F\0\0	`¢4Ù#ÔÓÓPô©ê 4\04ÄÀF`\0\0a	FFIèýMLÑ¦SôÇ¨dÑ=M¦BGtÐÂºWÝL|Ï×ä~Ø±rFK_ Äoó\"î²Aú|#Þja¨ò\\¯d jÌÃI«È®·yDæ±_-QTàx¬$Öo¸ÖHuÈÔ(Iïß¸øx­u5Ì<s)Ü[!èp6>#=g¸¥Qí|ÜÐÃõ7\r¬þ6Ó´ÀT$°O°¯°b\\Ó¹	KN âS8¡¨l\\Id °ÈÐØ\ZLcC´ÏJ*²ÅòÉ(°a¸c\ZRD!å3ÈÈ,®D@ÅbV.T¦	´\rSØô²mF®2VB1J*n*KCÈ­i3k±ÞlP°+´c u\Z-Y}§ÜIAªYÌÛïfA(@ËARå\r	!Pª%LT©&ÓH eèâ¼*Æ$Àð©A2ówZÜ*¨¡Rö¢jIs!K^µ°)**QÔukPÉUWÖ¾)ý×\ZQ b)\n\0~m@ÛQ46PU6A;ZnÈ²T«0\"Ð¶\"ÑõÞíäí		\0cÓlÃ*VûJ ±	ÆiÒ×°ÂH/E.!t/E¹ÄRa\"£ Q$É¥\\[ÄCÐBÖ(k#J^0Qgañò;Ãà5*c¡¹ãð}æ{×©ÜP jNcúÏºÈ-äÎ°«SAkÆÂþ5Ïqø ÏX½TÏ#Úyþ\'ÌÐêRGaì_uJ)8AÔÙ6=¥NÕ ×âB<F,ÊÝ¨#³í \rQcy ÅüÔèt²3`ÐØ½æï8£<Ã²ç¸ë-¿|`eR±úPÉ¹£ú~#\'HwwåßLªI	,`À`ÐÃmPWÂ±ÚHÏäÜp3,4ÌÅ$Èà¼êBYú.¿sÔA¶du7ßS^ºln}§íü\rvÅO\"CÀz;\'ÆfjRúbö©Åº 1ulLÜsi]Sa$×¼Õ*1FúÅkx>«Ü8PàxxêJ£ZgzÝô$ÐÉý7Î8©kÃ»:°ÁF^^f®#Ä¤X©LFµÀ¢\'E=µUBLxJ)Ë7¬0 Q/ð)%Ô0ù\Z%¿Ð.¾\"Í\r2õFÿ>+\r3ÈÉ{ú£¿ÿeyô9¥ýÈ:1¶ªt$÷ieFàô\r¬tLèq&úNÊuÂëih2°¥HF&BÀi\\äA\\AsÑ§«¥½/MÙ%TÛan+RÉe*ÈªL*[´²RÉ--=mÌòw\n«ÔZÆ¢pdP\\è3:rp*¦ðwe71Ò36P133È©Þ¦K ÂÔ|Rh×ù_F÷ÿ_ ôi±à0¨¨£#?1óâ@g\"B%Í.&VpnO2²íNyª\'ð½|ñ--LRâÅÜN$¶&=\0', 'Cash On Delivery', 'shipto', 'Azampur', '1', 120, NULL, NULL, 'SS1P1591013979', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-01 07:19:39', '2020-06-01 07:19:39', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(7, NULL, 'BZh91AY&SY>Èï:\0ªßP\0Xø;ßü¿ÿÿú`ÿ}\0\0(I@ID! 9¦&4À#\0\0\0ÀæÓ0\0\0\0L#b`#LÂ0\0\00i0#À\0\0Â0$A=$Ñä§ä¡íI¦\Zbl¦a\0© &)yOS#Å2diÍ@ÓÔºP7iý(ù\"Y-#à\'Ð?{Î[ØÚ¹í<m!yfÎT¼?Ed³ü®´eFØa.ì|Y¹«\'6,Â5lèÃ	vì©»WúXÇ\Z®¥ËË§·Æ},:7<\r\"ÒvÆ¯öºÏ ÊÇâò¾§Gèy¹»&ï½Ñ^$Ò=h#Ðs>G¼÷åy^¥ø;]Ç£Áªç¨ÀËïl=çÄîZY_H¤ø¦CVy£´ò7V\"=	D\"1åX³µcV<1´z¦¢I¢*,MÔt;¥ÐÂTQE¤»eæì­477(¥Ó©ir¥äÊ¢lXÝP¼ÂÎeí[¦­¥9IJ.JQQ¹Ie7]s*nÕ7[VëË$¥2N¢Ór(RnÕvì²ö»\nIcY*Ñ\rY³ÍØí.Ñ¤¦ÆÍ%(]]æâí¤¢ÊFZ.Ã.¹Ð£ò6J©WYf­GÁOG\\.ºÿòË,Èf\\\\fQÎÖ\ZlÞÜÓ[6Í%Óº^Y,f:¨ÀË6T:a¹F×e)ºÌ6dav¬7aºóstÃ©ªí©£-FS	dÃV%&¥Øa­d70ºa¨±f\\â89\rãyç¡Ö%ë.@h¢(Â\0¸°Ò°÷D !E4D\"¡yfE)Nå%Rñ1d²¢R(¤¢(¢ËD,¨E@B,X\Z:ÍÐé6éeC5T©\'V\'m8lÔÑÂÍÌNÖîJnË&®MXäpZTG&Vh°ÙF68p¹ÃF¦G\rK¥Ô.¤º2Y%5´Y»*8,£­Ôrrn³Ví´ÃSï>_t÷?î_º~MÕ¤ösü\":{§QïàÑoÙÖe=Ï\'aèuôøÕÐhûÔw¥ÙXS* Ap1(Ü ÒÜû¤?vF£aF%ãOÖ÷¾ýïÕÔè6hë}	è/n=KÔ\Z¥2sPòö;ÎR£òÖQ8>Ç6\">À°siÎu;üfzQ©\"äæA®`zÃ©r2ÚìtpttXY´ºÊI¡ú¥K	?òÉIw{ÐYªÍ]ïK\rÍ&®¹¡±°Úh¥\rRäÕ²k?åÒu9¸æ\re85´.L~öÎ#óà§aYÇFâ^ë7ÆóÆüíÚ}èzÜºaã9¼Ó;V(ñzÑvÓdç=Å%å<ë¿y:<S¬Xù§0Uí/x!!¸^äýÇicxs;ÓÙ7·`¼¼aQÉÃVZ£§Ù/9%ß°ØuDÝñx\'¤¥Ï<äÕä;d|Ö,$ÃÌôÏAæ)5Yqê&ÌÌ6);¦ÆBËË¹&òïÌÖbÙÄs6£*`6ÂÄz[&!@^7ö©fc£\0ïu¡Ö£Ä#x½½±Èr0 ÇÒä&GìhQÄüÍ®÷ié|æ}«½óB\reËÑØÐR:§rÞe%¥JQÖY³òÏ¡ëBøRN%L­Kæ¡ äÁ1|4Ãu8¼\rG3°ÄäC°B\0ÆO9\Z\011\n\n4A`ÅUts{\rG¡/7ÀÇí1va:ÂfÜÑCå8u¼)KÆ¹é\\.Õ³h©S¬ZQEIÄâï±ÌÝ7xfa6%\'ÔO«e(û³dõ¤óDÂa,Õ<ð³ùÎSÁÝ-$üNòéÅSJ<ÊSlÁÈ¸p`\\yAî;}§ÌQçÜsD()?ñw$S	ìó ', 'Cash On Delivery', NULL, NULL, '4', 0, NULL, NULL, 'Ocjm1591349941', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'customer name', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-05 04:39:01', '2020-06-05 04:39:01', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(8, NULL, 'BZh91AY&SY³\0©_P\0Xø;ßü¿ÿÿú`ÿ}\0\0()@ID(\"¢i0#À\0\0Â09¦&4À#\0\0\0ÀæÓ0\0\0\0L#b`#LÂ0\0\00IÊM5#!\04\Z\Z\0\0© &!MS\'¦S&G¤ÈÍ ÓÔºP7qøQò,;âD²ZG¨OØ|þáÖv0qnèch&+°êq´Í?©y8?Ed³ö]hÊ90Â]Ôù(ÅÔÜÕ¡É«0¶sa»vTÝ«ý,pQ©bÓCC°þ©L96:w4IÖv\Z¿Úë:¬wkês~{w\'SnüÙòä@Qì#Ôiò<NGÈûqÜQG¢|]nÃ´£¹¢ç¤ÀËðl=g¼ìZY_`¤÷¦CVq£¬ø7f\"=	DK\"+Ì±¸º´,±Þx=3T4EAråÔs.RTQE¤»eæì­4377(¦\r©È¼ÁRáÈ©\'ÆÊä­[efí[¦¬8ÉJ.JQQ¹Ie7]su7l5n¤S(¤ó¢MÊXJR¡I»eÛ²ËØu2ê*IcY*Ñ\rY³ÍÆeÙ55j©)Bì¬²ì¶¥4F©h»¹ªç2#¬±Âo\"Ê©,Ù»cG½OGL.ºþöíYÌ¹s4ÖÝØÓCM·i-s5K§l¼²XÌtQl©6sÃr®ÊSulÈÂíXnÃuææéSUÛSFZ¦É­JMK°Ã-Z2É4n(atÃQc3\nÌºj4bnL½ÉòsöÎÄÊH¥ArÃJÃåÑEå,\\¡jÉT¼LZPTJERR±EÖYP!a1aµÔk6ñ³m\r*£?9RNJ£víZfn³c­Øâ¦ì²jâÕ\'Ò¢8²´ËEÊ4)±ÁÁsF¦G¥ÒêP»%ØqY£Y»E²©ÁÀ²­Ôqqn²ÍÚ²RÓ\r#¸ù}óÚ÷NÙÑôÏÉ±ú´A÷¸ÏCçOlè>Î~î)íuùl<]&¼å=çÐ³©9ä»+ÊeE\\J7Ô\Z[¾×icìÀõ¹Q¤ÖQ iî=/ÁëâhÙ£´ùÌhn6w.àÒÃ\'%19q¸ã*?)h}eÉö¹1ç{Ë)ï6§IÁÔ(?ã3Ée\"¹Ò#!ì<Î·k°æ°³iu2Cõ7\n)òå=îç ³U»L764.ºæÆÃi¢6)JKVÉ¬ø(øNs¶pp1Ì6,Êq,jh\\÷Û8à9ÂfafS¬3!Ü4:òæµ±Äp?SòÄß<æêz¥tÃÓ9<\'4v,Qéz@»i²rÒò+¿9½)Ö,|ãÚI\'­ã\n%DYÄIêCÖÒ]à9ÅÝÏuÉ§Ô%ÈLÌM\"]¦^qK¿q°è»àïO\"v¼g®³²GÍbÉRL<SÒæ<%MV\\z³3\r\nN¥ÆÄ©±²òî)¼»½óu1lâ9\rÚ6£*`l6)ï6LM%S)=/)hpN¹CñRu¼äâóTTÕOKñzvJtÝ¿£­g¥ðsH\'y8?égï<\'Ö¸ô¡¬¹sá9º)\'DíSÑ,¤£±ätlå<ÓÎÃu¡Iü©\'	S+EÒâÌóHÐÐr°L^a¿N/¤è9NR<Hub2yÐ¤H±4@PQ¢(Èk/:ç	èqw,»x)í>-g\"];a3nÑCà8ÇmO)ÆBÐ0CÊtp]«fÑRgH´¢&áwzÌÏXÀfn©Ofa6\'Ô>t)GÚò8:§­\'´L&ÍSÆ.p7?Ì8MÍ+Ôa¹ç#¤!ÓllÁÈ¸àÀ¹àä÷G£èD()?âîH§\nöra`', 'Cash On Delivery', NULL, NULL, '4', 0, NULL, NULL, 'e8vz1591350215', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'shipping name', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-05 04:43:35', '2020-06-05 04:43:35', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(9, NULL, 'BZh91AY&SY^»L\0©_P\0Xø;ßü¿ÿÿú`ÿ}\0\0()@)E\n)Hi0#À\0\0Â09¦&4À#\0\0\0ÀæÓ0\0\0\0L#b`#LÂ0\0\00IjdOQ¦!¡4¦\r\0© &£MIµ2ze6¦ÐÍ&=K eqð?\">%$K%¤v	ÚoÐuìî\rÜÌm©ê68ZC1fñ*^N¢Ë2Yû.´eFØa.ê|TbênjÐæÕF[:Øa.Ý7jÿk(Ô±i¡¡Úzê&KÛ\n;ÚE¤ì;M_ºë:¬wséu¿CÌÝÍÔá7}Î¶|exzÈõ Z|Oñ>³Äï(£È²Äü]Ó¸£½ªç¤ÀËîl=§×=ÇròÊ÷\nL&CVi£°÷·~Dz9DWc-ru5hYcÌyÍ¬Õ\"MP\\¹`æueÐêJ(¢Trl¼Ý¦æå¹µ9*\\9#TÜ±Â¡y+eáL²³vª¦¬©BÄ¥Su×7Sv©³VêIe2O*(´Ü¥¥!J¶]»,½g.¢å(©%d«D6YfÌ/6zØvf\\ÔÕª¤¥²²Ë²Øó¤¢*FZ.Ã.µ\\ë(Â7ÍäR¡*aK6nØÕîSÞÑÓ®¿¹¼ºÁÇ\".d[ºhi³ví1Á³NgY¤ºwKË%ÇEfÊg^h]vR¬ÃfFjÃv¯77L0®Ú2Ôe0L5hÂRj]jÑI£qC¦\ZVeÓTÑ£tÊeíOò´V RE*$( \ZVQ(!QMD-,È±bå»J¥âbÐ²¢R(¤¢¥.´BÊ£A\rÇhÒj\r³Vå\r\n¢õJt`²U·jÐË3u×%7eW&¬r8-*#+L´Xl£B8\\á£EÓ#¥ÒêP»K°ä³F³v7eSppÕ³\ZNMÖY»VJZa¡w¶{ÙÝ:>ù¶?Vç>×)øÄ>TöNëàÑoáÒe=Ï¤×®SÜ|ë:¬hûw¥ÙXS*(¤5YÛ!-&\'ß;|eð,äæ³VF¥ú={ïs¡Ü4x^tç37:×XhasPòð2w¥Gç-¨¢q2}lDy^âÁÎ{§9ÒpêñâR\"¹Ò\Z×ðÞ7K¸ç5\Z6YS)4?SqÐ©ræÿ)ëw½¬ÕÞña¹±¤Áu×466M¡±JR\\¶Mgâ\\£ß:çtâhc6lYàXÚh\\Xù8`r:\\iS²S=,Î§kr]£Îï«÷§Á÷£è]0ôÎo<ä¦Õ=/S3çm6NsØR^SÁwó\'[Ò}BÇÊ=|ÌÏQ*\"Èv¤ÚOTùß3Iwsë.ë|-<fÐl\"h203	v>ÖÓ]ü\rDMÞ÷<JQÜð»Ù%%I0ó¼g¥Ö<òÅ&«.=DÙÁ\'RãbTØÈYyw$Þ]æ|Me!ÎÝ ³j1¦vÃbXÛdÀ4L¤õÈ¼¥¡ÂvJä±å\'\'8EMYI´ô¿)w©7h¤©ð£wöv(á:\rNû\ZvÝàr;Bç}æ,Brç¾uº)\'DîSÑ,¤£±âtlç<ÊÃu¡Iý)\'¦V¥Åp¡ â`;Í4Ã¿NÐrÈlÄ dõ 1Hc#h 0¡ £DQ%YyÙ8\'zÈËµ\'2É£ñk9è-,ÚK£eù¼²óªz\nZFRÜñ.Õ³h©S¤ZQEIÄâï2ÌÏ`37MÆÇT§30Cé*£ìxÍ:§ÌÀZ&	f©á0n·?Ì7m+°7BÃs \rò+L14±³ãsÎ!À#ó;^CBa¢¤ÿ¹\"(H/FÝ¦\0', 'Cash On Delivery', NULL, NULL, '4', 520, NULL, NULL, 'jKNF1591350507', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'shipping name', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'hello@wirelesswavestx.com', 'hello@wirelesswavestx.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-05 04:48:27', '2020-06-05 04:48:27', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(10, 13, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'DRAx1592300219', 'Pending', 'vendor@gmail.com', 'Vendor', 'Algeria', '3453453345453411', 'Space Needle 400 Broad St, Seattles', 'Washington, DC', '1234', NULL, 'Algeria', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 04:36:59', '2020-06-16 04:36:59', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(11, 30, 'BZh91AY&SYa*Y-\0Ôß@\0Xø;ôD¿ïÿúPÙ½ 3\0Ps\0À\0L\0\0\0\0&Òjª£@h@\0\Z\0D4 4\r\0\0\r12\09`\0&\0\0\0\0	i	ÚM¤H\Z\Zé=L$n68ã´:öÒU}Uf9téssiÔm6+Vrª9níiUhÓwYzs&ÈfÄÈ¾ì-B`bº²\"ph¤^¡ª~,4&ìlIÆæÂ¦Í¥pd©¸ãØP±Ùî*µ¥ðiÈ§@;ÌwI\\î&áè9Î§¦aÐÃ2§´Kp¾²*JB³<ò¬e¼HFB5\ZKF¸4ÄÅ¥ñ&\"Ìù¤\"îÓK£È©D{!)±»^ô½DB`@Ø5IV²-ä ¢+i-RÅ¨Á°µb&Ø0TE\Z)Y¶Åäm¤µ©CBe_\ZOx`ÀÆÖ4´=+pPÁ(P$¥Ë Y,¤á6ÐfCÒ1{<ªTâa/¯lTÝ6wpæ Ðr¤	 9{¡FrvOá5	j-£Y÷}µ1m3d¶mLl±)Ù²ÜUK\"ÂTÖ!¤¶BÙx]{½¾àßÉuVúfî9Á¥êV¦µ7ðÕéBÚ\ZkkRÄI fÐZøÖÌ¼uÞä2²!ë¹¦#J³9!×¥¹Ä^£¸ÉÀ|Oçà}GØl$©ø-ç¤yÍÞ¦¦MÄþø÷%êûÙ¢&°ª5.\ZV¼m<Í³òð?Ò\rvÅê æ}æÄý¾\'ÌØoRG#ÞºT¢´2j:w@ÜTäµ\Zæ~¤¥§¿m9062m1 0<êw#AÝ`ÑCbó;;\râÛvÀF	U3ÑÚ~d>#SÛÈÅøó¥ôÁÉ+Wtî°|O3\'%FI281:<	.pxvGÒ¦»é°&Üø/ðk\'èu=\ry;v(:wù(ÛqÝnôÅâñ§àpÙóÈ¶E¨NgcH¦C:ÏÐ]²vÃ:¨ÖtýÌ[¨ÏBNA=\\×xÙÃ¸½w£8Nç®QDEz¸k\\\n\nNW3©NY.AuRBT09c\rä­Å\nRö\nGØ¸?ãbáÞ-y9Az¥Ãþ\'ªÅí\ZÓîß½§B©{É; þùÃFÙrÍ@£[¸m3©¡Þ¸úÚo3É2F¦BÀi\\äAçv¿GKx>Üäb`1¿]bRÉe*Ä«#[$eBl$Þdï×\"¼è*¯¨µ  £BBõn!ëõ8\n·¡·I¹&j±3Ê*PÂÀ]j>h©@²2×´F°Ù÷wÎÃÖ¹%EE\\Ñô>Ò\0a\nâl8µ²2­Eæ2ÓÆçD/0A×¯¥©¹?âîH§\n%K% ', 'Cash On Delivery', 'shipto', 'Azampur', '1', 159.99, NULL, NULL, 'KzTW1592301344', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 04:55:44', '2020-06-16 04:55:44', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(12, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, '9ZXW1592301780', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:03:00', '2020-06-16 05:03:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(13, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'slAq1592303830', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:37:10', '2020-06-16 05:37:10', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(14, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'LMze1592303876', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:37:56', '2020-06-16 05:37:56', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(15, 30, 'BZh91AY&SY^¯5\0ëß@\0Xø;ö¿ïÿú`?2M°Í`4\0Ç4ÄÀ``\0\00JFD\0\0\0\0\0\0P	M@PÑ£M\0\0 b`00\0	\0sLL\0&\0&\00\0FFLI\ZzÑ£&Ò4=\'©{Ç`HÜ{Ã­^_ÉýL<ùj<|`±r£%¯ÄGðEÉÌdôeOuYNb¼dÄPÆ^DëÞMi¥£áKÅLiA<Iä)RäÌ¦NÉ¡©YfïG@#´`æ4@Ðº	5D@sê3èfcXaÜ<P ð!#À%ëj´­HB1¡6\n Ó´R¶öF/pÑA¥@¡YÅ-\Z<a£ÀlbnøÅ$Â©ÄP&Ð6\rZ{ÚiaGD4Ù¯3LzAT*p;´ZDPyz°É 6ÉqJë5,1µk»Ò0PÌ¥H*`±!¬2ÊKÍ°Dd%V1 ;CÙìÅ´É!\rTà\'\'Éâ**<CÍ¦D«B\"ÀÄ²:Y* ¹%&ã¸>©ÿ(b@Ø@Ùã\nÚ!¤ÚGY$KMÂ\ZcÒhöP1Rh\0rBA\r:\næ­q°èÙ	\nÂH ÚÇ.ù^¥*dãÐéÛ¢YÊT*øL ¬0QJEX£-KÝÂQ/²2ÝÆ|\ZµZÏ$/qP¾UâOz³AÂBJI2»2óPÈiQâ«Âe9)¨Tñ7ºÉ.³nÄÓ¿R!¦²\\4¤µç`©äg?ÐÃà¤ëª¨5.33âuØÖmRGYë\\jQIÔ\Z\Z*Ó¬\rMëA¯%ó%,ÊítHÞÀÖ2Æf³:ÈwY0hlZÂ÷\r\reµ-Pa¢©5ÀÍ§¸jvõx¾îº_),|ah3+CÝ,##iQ¦f)&G\'BÈg2K£JftÒ<ÝYSNÐJØ¼öú/Ìt8Æ8GÄhá3ÖµQ[J;Âr2\"òôï=éj¡÷¡%ò=89¡\"\0cÜd43¼û½6\'·)7qN$ª5_#Â*3Ì¬\'¡zä6mì/^ófÐ)ÇÎ\rC´Ê=½J\\\n5¦C@+5¼§UFUaP¡	S8KaE¨¡@ª]ÂÀ²]øÑtvMÀf)2Õ,¿By¬Xi=£Y{ºc³yT ØTÞäGØà1281±î1@£Zèq!¦v¬Îkpñ¨ÜuSl!âÀÌeaL®HuÈ7D8ß¿½Í¡|L7àÈLHØ\0ØØIö6Ýpp[r\\ÅeÈµ  #\"¤Bìé0íìpo#R®cià`ÈÏ©Cu ûRdfÑøõa³\rþF3Ôr[Å	QQu½ÖJÿ¿q\00q6u4AlÆ@Õ¨¼ÆSÔwñB±ñåÝÞØ6(`þ®ÅÜN$«Ía\0', 'Cash On Delivery', 'shipto', 'Azampur', '2', 86, NULL, NULL, 'yVNy1592303936', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:38:56', '2020-06-16 05:38:56', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(16, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'cNNV1592304048', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:40:48', '2020-06-16 05:40:48', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(17, 30, 'BZh91AY&SY^¯5\0ëß@\0Xø;ö¿ïÿú`?2M°Í`4\0Ç4ÄÀ``\0\00JFD\0\0\0\0\0\0P	M@PÑ£M\0\0 b`00\0	\0sLL\0&\0&\00\0FFLI\ZzÑ£&Ò4=\'©{Ç`HÜ{Ã­^_ÉýL<ùj<|`±r£%¯ÄGðEÉÌdôeOuYNb¼dÄPÆ^DëÞMi¥£áKÅLiA<Iä)RäÌ¦NÉ¡©YfïG@#´`æ4@Ðº	5D@sê3èfcXaÜ<P ð!#À%ëj´­HB1¡6\n Ó´R¶öF/pÑA¥@¡YÅ-\Z<a£ÀlbnøÅ$Â©ÄP&Ð6\rZ{ÚiaGD4Ù¯3LzAT*p;´ZDPyz°É 6ÉqJë5,1µk»Ò0PÌ¥H*`±!¬2ÊKÍ°Dd%V1 ;CÙìÅ´É!\rTà\'\'Éâ**<CÍ¦D«B\"ÀÄ²:Y* ¹%&ã¸>©ÿ(b@Ø@Ùã\nÚ!¤ÚGY$KMÂ\ZcÒhöP1Rh\0rBA\r:\næ­q°èÙ	\nÂH ÚÇ.ù^¥*dãÐéÛ¢YÊT*øL ¬0QJEX£-KÝÂQ/²2ÝÆ|\ZµZÏ$/qP¾UâOz³AÂBJI2»2óPÈiQâ«Âe9)¨Tñ7ºÉ.³nÄÓ¿R!¦²\\4¤µç`©äg?ÐÃà¤ëª¨5.33âuØÖmRGYë\\jQIÔ\Z\Z*Ó¬\rMëA¯%ó%,ÊítHÞÀÖ2Æf³:ÈwY0hlZÂ÷\r\reµ-Pa¢©5ÀÍ§¸jvõx¾îº_),|ah3+CÝ,##iQ¦f)&G\'BÈg2K£JftÒ<ÝYSNÐJØ¼öú/Ìt8Æ8GÄhá3ÖµQ[J;Âr2\"òôï=éj¡÷¡%ò=89¡\"\0cÜd43¼û½6\'·)7qN$ª5_#Â*3Ì¬\'¡zä6mì/^ófÐ)ÇÎ\rC´Ê=½J\\\n5¦C@+5¼§UFUaP¡	S8KaE¨¡@ª]ÂÀ²]øÑtvMÀf)2Õ,¿By¬Xi=£Y{ºc³yT ØTÞäGØà1281±î1@£Zèq!¦v¬Îkpñ¨ÜuSl!âÀÌeaL®HuÈ7D8ß¿½Í¡|L7àÈLHØ\0ØØIö6Ýpp[r\\ÅeÈµ  #\"¤Bìé0íìpo#R®cià`ÈÏ©Cu ûRdfÑøõa³\rþF3Ôr[Å	QQu½ÖJÿ¿q\00q6u4AlÆ@Õ¨¼ÆSÔwñB±ñåÝÞØ6(`þ®ÅÜN$«Ía\0', 'Cash On Delivery', 'shipto', 'Azampur', '2', 86, NULL, NULL, 'tPL71592304149', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:42:29', '2020-06-16 05:42:29', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(18, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'pDaI1592304551', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:49:11', '2020-06-16 05:49:11', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(19, 30, 'BZh91AY&SYk~G\0ëß@\0Xø;ö¿ïÿú`?;°4È\0\0\0ãb`00\0	\0%SÂ\0@\0\0\0\0Jz£Ò\0\Z\0\0\0\0\0Ó\0		\0L\0Ãb`00\0	\0$D14bLQ==\rQå?Ty¦êé=S	ùÀ´ùB»Æ¿søzöÔryùÁbåFK_q\",æH>ã$¿£²û³$ÈÃ²à°ì£\\´T4!lXâí$µ>M0cN±$cN¤2\ZJ1+&N2f®6Yd{\Z:Þ@#¬`ì4@Ð¹5DsÐgàÌ.Æ0°Ã°.wñ-!ÞBGy(KàÕiZcBlA¦/¥m%Ý#ÑA¥@¡YÒidB;ÏcÀlbwc\n¦c@`@Ø5jNeïi¥Òfzr¼ÑéP©fzsQThd¡2\\RºFemZîô¥4@AÌT¡0IL$5@fYIy¶l¶öÅÜ¸·H³âY¤#$Z¡)â&m2%\\!Ò,I#¥ª¤¤ÜÚDÒ¿JØÐ6\"b6yÂ¶¢i6¥ADÙÐI%ÓpÆ!¤Ãâ61BÅHH! l´´è+uF³\\$+	 <ÆkcÜÊõ+S\'2©Ï²RÎJoW¼Ãy©:UÂØXb&¤ÒJ$PvQFAPîÔÕk>D\n\\¥^<b1QÜdØG¼G§ûs¸æ(Xú_Üu\ZÏ´ÌGpOÐw\rKýzC?£.¬*K¼ë=LçøxÑz¢êª\r+¤äf}MÞGäÔlRGAð\\*QI¸4\ZMkaÐ²¦õ kÕ}ÉK2§ª$o`jc3QN¤d;¬46-A{5A¨¶¦0L4U3°ö!öNÍÄ^/·¢ÊK#0´Á\nÐÅ·Ac##ÔÈØfTiIÁÐBYäIth¡Òr3 ÎqæêÊ9çHJ¶ýÇïý\rnÈ¡ô<$²y\ZºÛºûÊ,vädãÜ/Ô÷#¨?ê6ØîÁÉiÓ!¡§éÝbxjiú îÐJ£Yåö1l\"£=:y·®#fÎ¢õçfl8 é j¦QîêRàQ­$d¬Öòª2«\n$¼(JÐÂZÊ-%\nRìæfÍÔ-@ÍRdªX8ý	ä±a¦`ù\reáÏ5Ê¥²¦ô¤R?\'HÄÈà4ÆÆ\\gê\ZÒ\r3­fr[G&Óu6Bû1¬)2Ð\näAî!, lAûxÛÜø¼Ùï1ÆûÙ`Ài\" i0\0PÒ`Ð	?&ÃI×ªE¶¥ÈV\\KXÖB¸X¢2*D.®x3¾§Vò8å*æ0ª±ôÅJX­êEJGøÑ¼6|øÏIÅo%EDEÖôGA+ûý¤\0ÂÄØnjf2­Eì2ó·¨v.ÎÔ&Á±CâJì]ÉáBA­øU', 'Cash On Delivery', 'shipto', 'Azampur', '1', 43, NULL, NULL, 'I0Ko1592304752', 'Pending', 'shayanhaider333@gmail.com', 'john doe', 'Pakistan', '8328311884', 'asdfsdfs', 'karachi', '75300', NULL, 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-16 05:52:32', '2020-06-16 05:52:32', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(20, NULL, 'BZh91AY&SYg%j\0Õß@\0Xø;ôD¿ïÿúPÙ½ÔÍ«µ\ZÝ(!)	©£h\0\0ÐÐ\0\0M¤ÓJS#Ô\0\0\0Ð\0DP\0\0\0h\0\0Ì\0\00\0\0\0\0HH<#LCA \0ô¥åH\\\nhAª\"ûÙa<HFv=Ì,a0´qM\"°Ë¬QÆg¥ª{Ö\ZdÀü4t9ÜQedä¥ÆuÊEËÝ~¨j	9ÜØTÙ´®7÷,o÷8ì®43Ð§4 E¡²^¡ägbö\n(ã!ä@yJRâÈ¸£!vùB¥äDÔi&ÁX\ZbhÅ¯&2ZHù!Rwtº=åJ#ÜI	H\rMÚ÷¥ê\"ÚÁªLÒµp!(Z&k¡jØ©ICajÄM°`¨*4R³m¼âÊ¸µödÑç ê¤[8î×·ÚÑ6#F\"¹$.XÐÊLM f,¿\Z­\\=²C9Q§ MUÎØY©³`à¡	É\rM¢ÚZ|s³Ëð6AI\0KU5Ê<ìÑ$j#«,@Ø,R ÁL h6³nã~êÊBÈä8ç/7R)/\Z3\'KH\Zá¥ÂL$­ µñ%¢0g2f°YÖÊÂ3?	¡^¢ÓÙ74²Ü7¬\\Ðâæ;Jb)È\r6ùyLÏ©Ã\n#6\n¶ù/P¿Å\"k\n£Rá¥kÆÑCÌÛ?ÀÏ¼ô,tK}¨²CY@å1ð.±,´yT¦Þ¨\rÅNkQ®èJY*|6Ñ#ic&Ó\ZÎ§j4Ö\r46/3xcÆ¦óa¸¶Ý°aLÁ´fr>D?PÔòæEñ]N²«!³ENjÝ\rTÚ9Ù³²FI281:<	.pxo­GFô ÉÒU:ùAp/-´C\\S£E­ç\Z<el³,7µÂ<fé¥\Z?\"DW!Áy¶i ÐÏ#Ùë±=EÊNG°Á¯PU\ZÎ±aèIÌ\'zEÜ6qí/^èÎ S¯qßaD\"½J\\\n5®C@\'+¡âSK]eT¡âP¤æ%LXÃ+qbT½bÀ¶.#øØ¸÷^`eA^©`ðÿñX°Ó0}£Z|xG~Ó©T¾²MåÀþù£#Ll¹f¢(öhÚg¡Ü¸úNTáaïbÈÊÂ#MS!Aà4®s As·_g[x>¯ÜÈÄÀc@EDa¢\"¬µDÚ\'\'v½!âQU}%¬l!\\%\Z\"fè2ý­àêmÒnc	¤dÚ c ,dÌó0°Z¢*P,µíÑì6|{LgaÞº%EE]ÉôND\0ÂÄÙêhÙV¢óOiåsª òñòBl\Z)ÿrE8Pg%j', 'PayPal Express', NULL, NULL, '4', 433, NULL, NULL, 'KQda1592405340', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '78701', 'dsd', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-17 09:49:00', '2020-06-17 09:49:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(21, 33, 'BZh91AY&SY+£sU\0_@\0Xø;«ôD¿ïÿú`ô\0 \0T\n©\09L\0\0À\0L\0`\0&\00\0\0À	\0L\0À\0æ0`\0\00\0H	¦ÓÔôSzjDi \0Ð*D HLI¡¤1=A hddb80GØØKR#ÅöÕO½üÞéÿhüE¤³îÃð>÷ß¼ÌÔ©>éRO\'i¾*b\\û¥LI§úajx-q¦)zL;íFpäÝ±ÞÝ²<e0å¥9nüVtÝµb[Ïõy¹\r<£N¸Tî0x¼_ñ¦ôÎÍßC_bl{]ÍÎMÛ³òJ÷\0|åååsa20$i9ÍËó13Új;gÜD­Hõ¥¦)ñoé\nt¢4aíQñefTiêwÌDR÷$ì©!E)\'\nKT7¨J%IÓX;ÙRíõ?6PÒj2©I[¸Zô¦\\\'Å§sPø²°É6U¨©¦îeÃG½Iß)¥&©R©BR¥\nM0ËvGDñjE²ä0¶164ÙÌëfÎ!J8ZØeÜîaÛM¤0¤lÃ\rÚx½lÎ;ÜÂÕ	S¿°åÒÚj[¿we&Ó¤ã$8Ë72d2hÑ©\r¬Ù4mkf¦%Ã.Í£6£Þl¾(Ë\"-»gC,7eË.Xå2Ê*nÃi¦ãI´ËvÌ¥%°Ã-´Ñ6r(eËqf¦U©Ý6lÌå4|aøIÿ$bDR¥D¥A¼1$µÅ¿Î\"ÊR×-R)QmE*Yf\nRSÍIT¹¹jmHµ\nI!(\\µAJ)H)IOÜïx§$`ó*(õ2µ0W.[´e©ÊÜO-O%»)Ë&8vpÏfÄÄGfË7_\n6l·N:n¶ê4²·2akL*-S¦S³Mæ\\¸aË*:-GKSfìì8[Ý²2É}²t¯Ãæ²}O\\úg^RçÆçÅAÎzHÜó\ZÛa\ZgY?\"Àï±G#\r-4RaKPlÝnø%©ààLû0ì$^`H° ÉÞo2{ÛÏ\'ÏÉþºc6G+½2(Ô3q\\Á{I¬ä	àhô;JsüLDêhþöa=ói±ÔïËOF¢äâ{ÑÚS¤ây;a-©ü=rté=ç®z§ÙêvX·TÒSÚåâTä£µirZ2kÐ¶ënó{oÌÁ{LZÎá¼ËKR7)J2M7N\'÷?)ó§®vçq2ÔM92öm`Z±&LdQ¼â*3¬ÊÎSê~LAAÌpï¨ú:ÉÊÜvò\nÎ!l6ò¶`39íFlÀ{YÄ@È=Ð\0W¸ê-v!2ã¥ë:Ù¥8ÉºÞ¶ÖOQxfÅ¥e¢QÑÌÒ)æ=å(§Ý7iã\'þSÊIÈ÷ÏoIe&Ëað&íL·©;8&g¶&\'ø2öºo.n&¦ek38ur¦-J<¦cÀÔ±ÒOÕ#äò?IÜOy;=¤u\n`©svâ|Ïó3hÚV@Ãhp`n<\rDvûºZÏAñ-ùÏt¥-ô¾J)Iô¹:£³Áh@AÉ¤ÎíX.48¼ObáHøÒN¥M.0Án{EKK>H±ædÉ69ÂÃ aà|ÆB!ÞC `aHHbTd-*@ªH¤	ULOÔùkFI>Ri>¹³iÜZÞL#kOGÎ¤ùÞ\\ÆËSäpåÌTiá(¢ÓiÔëËjfp\rÎÒpÔÊm´?õÁÂ7ºpéÝ>Tó)¶éçx½³/×)æöKR\\pRl©.Q²K2¤£/¬ø7×½óHGÁô!JJTªKRUü]ÉáB@®ÍT', 'Cash On Delivery', 'shipto', 'Azampur', '2', 319.98, NULL, NULL, 'os9l1592576769', 'Pending', 'shayanhaider666@gmail.com', 'full test name', 'Austria', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '20132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-19 09:26:09', '2020-06-19 09:26:09', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(22, 33, 'BZh91AY&SY»ÿÖ\0_@\0Xø;«ô¿ïÿú`=÷ ÷+@PRAÀ0\0\0\0\0\0¦¦§©?J\0\0\0\0\0S5CÒ\0\0\0\0\0Ð\0*%)¡ÐS i Ó \Z\0ÐÀ0\0\0\0\0\04M4Â\n~FÔõ<£Ðz§©ºÐ]\r°Å$è{ßWµàî­[håô>;(Tdµò\ZwY ù?ñ¬²ZÆ X5)ËgeB×{9Gr×zh_³3qAÛÀÁ»cbË<Ù¸ÀÓ÷(E\n*r×]t0pêWª¡4äj`ädêllo>Þh/y¿ß#¶¶R]é¡`%ý\ZáH(Ìå@%¨Ð¸B	\ZVL,ZòaÕõ D^Ã(1¨á¡ÝfÐU17[R¢©ðË\rY±¶	`Õï4½~ý 4: ÄÞÖ)d6µæiVbJÃ¼È ÆHEi($¢Ñ,ª`·Ä©©*P$¦\ZB ÙVÔ^b\n4JÚïwÁÂ¹w\nlEÍÔ¹FÌÌ®Ø¾\0X@u0cå$sò\n@¡l3^ÌY4[fc,[6¦5KXÆUW¹e¹%©d²*%F¤¶BÙxìÖßÚÉv$JV÷	¬4HÈ{I*X$H°B®ª[@1øÌa´`ÎdÍï*°;Ü K	j^d­nSLNfrC2­y²×8¨àèéâv=çøYá<F6\0ù0Ã#\0ÒÔØD9ôt´+\rKø¼ÎÂcï;ÿÑv;{ù¹ªcàø·ö=¯çò\ZÝé£66N}M©£âj::¼¡â²5üùÃ2Xû·àªK£yrá©¼Ý57:ÁäGÈÝRBÚâÅ&N!ÈZÇ±Þ[l@F	UxÆM)Í÷\rO.d^/Ë¥4 ÅT0Â£m\\U²¹úÁèMK\r}7\n$pfw£;]ðj8Àz?*÷ÓpMºr>_°× $gSã?2=Ò¦¦î¦]¸(<ý>%J=Á:\ZW\Z4VÎT×Ç9ç*ØãdUd8[£Iì={X¨¸ÉÞzvÔ$F³¥ëtPgÔNÈ^ê632õî«8NOÆ1^Ü\n5­ÅBåADÈ\nªÂ¡B	&/\n¦\n\"(ÎâqR¡T½#h´;bWwAkÄPÓ/T>Äù-,5[®uglÞq5#\ZÜGåzÝöt¬ð­:[ð{o¸ªÑpci·bä=69éákàÒ22° F¦BÔi\\êA(ç¾nM¿%ä¼Ñï*¥E[é¬¢ÊÊÅ%Y&(´¤d¥i£ÙrkÎ^ô¼ÅUâZÆâEÂQ¡B!tÚ§GàZ¶g&©²±3¬T¡ºÔ|ÑRde¯´_ùÛ\r}ÝLgqæ¹%EE\\ÑE=Ãçà@!`2ÎF,HÕ¨¾£E>Ó·ðy!{Ý½PÅ¹?âîH§\núÀ', 'Mobile Money', 'shipto', 'Azampur', '1', 159.99, 'ukljhjklh', NULL, 'wIdn1592631215', 'Pending', 'shayanhaider666@gmail.com', 'full test name', 'American Samoa', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '20132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-20 00:33:35', '2020-06-20 00:33:35', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(23, 31, 'BZh91AY&SY°5\'>\0ë_@\0Xø/kô¿ïÿúPûÜC\0hÐd#\0&\0`\0\0\0\0\nhC@4\0\0hÐ4\0B=C@46¡êh\0&hs\0À\0L\0\0\0\0$LCÒ\Zji¦ÔÓ#õ&	°ÐÉ¼\n{©¾p½ü5Aô0æÐê|þr,L É5ò,é\"ÉuJXd]ñû}Ô\\Fû\'\"UÊÙ·`êSW\n9ÍfÄo4VI8Ür~Ë8í9Vò}¢.	eMg#¿ÓØ0:%Ä³Ï¼Å¯U 8=PL$ºÒM4Àa©Þ/yVKÀ`H&8C8£cÀ`¨ct­gZÆ°j+Me(!´Ä±Yø^\nå,AT\ZpïUxþÃ¥ª*Lí$\nòÓ3²l@Ão ,q,¡Ãã	Ì=#Æ)>ÖøÆÈ¹HDbK-Õ¤#$UB*3»ÌÕÔytBÈéiU¨êºÝ~QèQü?¡ÐF¤(M3£íÎaS£4 à,± 8bXFµb¡ä¡Ar8)É¾L¢ Ò(¦ùââÂòß\Z¢^ìÔdÅu¹oR^HÓÐ¬@©¼9VØ5§\"¥L5BéÞÍÖÃÖ0¦´2ÑxMÛ²ÍlR¡:Ç°ôC¬|NáÒ|;\'ÑÐ_âPÖxØ´h`?xþº¾¦ÅaæÞ§\"pXU\Z¯ãaSîlRê3È±zâÔPiÈ÷ðHÚ¤GåBjFàÜd:\Z½&ð58¬Æ·~âhÖ2º÷lÀ¢GÂÆfÃ<sPÑÊæ\Z]/¡°-ZLÍet¼\\2Ø||Æ¥»nß;a\"¦áX¤1Xmà+Ý`yc#iO©LÌT%!Á´!,t$Ypf¬çC>Ùè®<¿Q­áäLò}§|*yÍ;{N-F³4Ølá;ñ©Ã¤ÜR {Ê.rC\ZD ì&0ýûúA#¶ÈÚwtÈ$IM¬pgdLgRFà¤.ã{m8¦°ü³`ãÜr7aXæ+LYXS,ÇÒvëä¡¢ÓID8í tx\'c\rÖ	óC`dl =Î\"Ëp¡È¨Î^Â\\Õê4ËÑ¬<vG-%A¨¡¼>ûæ4ÆÊ´¾UmhÃ`Ï6\'5´vÌÚnd!<+\nd³<Ó!Aç\ZW8A¶ ±Ë>ë©ò~°>!ÈEyã\0`%0h%F´;gXÛ×ïó\Z&÷uv«j,F$ÈÇ\\>.Üäi¬^é¤bh c *bc-ñBeÕÂË!ðE	Q^°?5]±¯ß¸¾9eÀ!)©¨ºÞí%øý¤\0Â6.&ÍÌ,ädH«Mé$ÕÊqî6£ý98ù ¡È,âîH§\n¤çÀ', 'Cheque Payment', 'shipto', 'Azampur', '2', 71, '123', NULL, 'DtRp1592667960', 'Pending', 'shayanhaider666@gmail.com', 'shayan haider', 'Argentina', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '78701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-06-20 10:46:00', '2020-06-20 10:46:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(24, NULL, 'BZh91AY&SYát\0ë_@\0Xø/kô¿ïÿúPûÝwaÃ×h.gA¡(Õje\rIé4y@\0Í P(ÊOP\0Ð\0Ð\0\0DýSõOSM=L\"y©ê\0 Ì\0\00\0\0\0\0H #Qª~&TÓÔ¡ ÐÈò!`RF SØ}Mg Ú?³¡G]ãñå\n¾·ôEÝdà2BL·L?dvÙ¡ÙFF×\rN7E\\ÊæÝÉ^A\'4ÔÈ¶%MÇåüËW\ZX:±Ñ\"\n(óëàe	TC#u2È¹GL(¼4¢ÁeAH£A¤@iÅï&@Òð!f¨\rMÖÖ¥ªf4L\ZÁªÒkjî+T6Ûi¼WÀ±%ö 0+D6\\fi(¤Ú)4­_¡ÄqBE@bé¹$Tb`crRÇÊ7a2R9¤xÅ\"áãaÌaÌ$F$¹ÂÝXJB2ET\"£;¸,Í]HPHÀ1$,Pw\\:µGîþFÂ2\"	j¤$:©¢F5ED³EÒ,@ä¥$]°¥Ä¤¤*ÅQB@fû©Í´: DAä(¦ÓÉÅé/*¢ìÔd9kÜÉX#,b8P¬@)¼9LØ5§5,RËª\ZÉ!CC5)¼>aMhe5¢ñ¬cxÑâ`cæw&¢ÓÜÖ<y1 åè$æ:,¶ØÈ¾Á[û¼CÛð«È-2PÔ¸i_÷l (=\rø÷ Ó|^ª\rm=¹ö£JQ£\"E-r4¨* ,ÅPXæ´\ZâzwFñÔg»lJ¤r`l\\ÐØÓ-5F#ºÀÅCk±¥älÇµfòÚáªf¦&;àÔjxr\"ñ~i|d±Àfd%V®6ñ,Oyæfvö$Jf©282HK1ä\\»¼p2¡f\\ò+tà×Í¡RÀÀqÖ\\ÄA?1~T£Ë¾å\rálLChÝ¤ç<£ühH&äRð ËçÂAÙca~a$ª54.îñ$à¹¸âÚ{r/]áófÀSqÐâ1£¨¯Bn\ZÎâ¡r@gIÕ©?¦qÛ,(@èìDQ7Z (KÔ)4ÃbÖJÛ³àHrõK§¬«\r3Ø5·hé©Ì¨78üÎDu\Zce°Ã;Fµá°g×Õvú§\nvBx1uÌÐ¨H6e/DÈPz\";b4ïëmÏ£ü\0ýÂbÏ\n0!\0!¢²ÔX\Z8ÌÝî^²É«bîK¡{\Z.ø2¼Ü[ÀèkÜÃÒ25P1221R\0ºÌ|Rá©2ì,d#íÚc³ñ-ÛµÊõòÒ\0a0kWrfIW¢ù/ÒpÐÜðÏhÈ\r@ÿÅÜN$\0 ¸]\0', 'PayPal Express', NULL, NULL, '4', 433, NULL, NULL, 'CnSG1592668076', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', 'asdfsdfs', 'las vegas', '90210', 'dsd', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'devmrmsoft@gmail.com', 'devmrmsoft@gmail.com', '8328311884', 'austin', '78701', NULL, NULL, NULL, 'pending', '2020-06-20 10:47:56', '2020-06-20 10:47:56', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(25, 29, 'BZh91AY&SYÇ×Í\0,_@\0Xø++ü>¿ïÿú`ÜÏ´hÔ\0ÐÀ&È`\0&	\0\0	\nm@\0Ð\0¡ \0À&È`\0&	\0\0(JhÐi \0\0\0\0i ÐÀ&È`\0&	\0\0\"hÄÄÔÔÔx)¶!4zz¦oëO¡¾|Îñû6:È¢xÃðB}CpÄF}\rýØn9FÄi<D\'\'ÌkYq¨£âì¿d]zÂ>ïÂäßPÐãB9%³nÁúSW\nu¬ÛyXk8û\r%õæ+²Cg3»¸NeG¨!6!_ñ&$)#ÀwHã\r#kÖmÀ¡Ð©äv=d{À¨Ð`½äÐ¢TÉB$á)\\KÜy[X ÃHÒ!`iÅ27V?Áé¡\nPµ·)Ï\0 â-v½)zT>#¥0Lh¯i½ë(LnEb¡X)#m6ØR¥nh*\n­©CiHÍB³2\Z\"±8ÉÒE!!å¦^tA7@Â8ÄPá¸qM ­Z<áÌ#SvùÆ\\É¸ÂÌØJB2E¨Îî3WR+ä)**QÔukPÉUVÇ\0þû7É\n\\Å/lãVcèá³GL47::ÍbJKC ÊDB¸@¡M <÷?0ès±A%9Å7±XX¾EãTSÝ¥uFÍy¢Þ³¨VM©¶0¥ZsTÅ,°ºaÇ[tÍ&ÍVf[2¢Ðêk%â-66*;Áçô<FÓæPò<SÛGp<x0Û\r>{ ç9@¿p²ó¢Ç.ò8s_Öä¤!Ú¥ÃJBø¼Ï¸@P.z\ZýF.EÈ4l¢ª\r]33ÿü¾gæI¹I­J);CA¡TÖ©¼\re+H×SüäQ[PÏ-¹HàÀÚ0¹¤ÚiÏH4 l©Éë#&\r\r­Í-¡l`+S¡\ZËiF #Ã*0iOÔîàEâû·ÒùX ÌÌäÂeeãn¢½ÖÔó3;ÊB1%(82$%Î¤]ßb]¼=Fí«XÕÛ]a(½·_ìkz¼<&S°ÐÎNû g.t(Tð	ÈÈ7gâ/Z9\r­Aü%\"ÙáírÎªD¦Âl@EÙ5N½ Ü7D4¨Öy~-TgîIÜ±ýÎ-¶nï1nFàô;@¯:80´Ià+Ô¥À£Z0*uH¸R\n#5Àî*ÕFo,³U*B$Ê\Z\nT±V4¥¢¤©Hu\"ÌÐ¸ga!÷/iEÙÈZ·¤iCA¥ÑbCÁbÃLÁï\ZËápíEA¬©Äø° d}CFØÃR¼ÏM£ZÃØ3¯¡¼Òu\\³jn5îi\ZQ%á*\r$S!`$$%ÞAlAË¿fÃ£!Ñ}M¯4`@L1D4 6$¡¤Á¡6m39èß(¯b\\eÐæP/sY#¢3*D.; Àtâà*ÞG#<¥1Ff¸2Æfs¾*PÂÀ]h6Á*F°F0Ø×üï1£¢à%ED£#y(óÒ¡ @NÐfØw3M5z/¢ìõ|Nh\\`ú\0QôÜ ´4âîH§\náúù ', NULL, '0', 'Azampur', '1', 20, NULL, NULL, 'EjtW1594625238', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Albania', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:27:18', '2020-07-13 05:27:18', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(26, 29, 'BZh91AY&SYsô}M\0vß@@Xø+Kü¿ïÿú`|©H¢¤9M0	À\0L\0\0Ó\0\0Á0\0\09M0	À\0L\0\0&¤M&$§êÊze&`Aêm\ZÓ\0\0Á0\0\0\n¡i´\"Ôa©! =ORÁ!Hh¹;\rç°ú<pô>AÜ$?Òþ0DCàQñ±ñÅ32Ìíãö(íÁcN³óªp6§&òÛw%hg¿«Æó½âÛbvÒÔ~ßM1Qw~¬s_FSÛñsfSÕíå-v}~)ïGGQâºâ=IC	DqHP0PNÐØCN.\'jNõÜÞæí9BGõ0ýåYä³&Ä\0Å½³Í	z3´	:¡ÖT\'{RQ\rã$A!Ä04JØ<¡bjD¢ÜfÎZ«2¢(±\'×3³D3\Zsµ÷2ån(©4B°ZÆWÅe6\ZÐ0h\rD\"0×[k¶.v1¡;2uÞÛV²Ãm³«c\rÒI-±ÛÁ£Òø¡F¦QS	,â­ü5;4M´(å+\\1ÍÉvÊQNL.¤µYL5ecSe·ÆR¾4Ù¤Vi{´/m¯µõ³fÑuÑ--¬ç\ZÅEôfñ1VµÆ3¦p6$½¢úÂó£HÎnÚ1Ã ;¤\'Î}IbE°ªö2j¥F-(¼SÂ¹r,Å¨$ýAH20#\" Ô\n@@X®Gc«j§_:F\"?4¹Òý´éÙ=¹Ë6ÓF÷¦ÌeÛ¾8­sÛ]oÆSK#Ó:SYf«S]lkr¡Ös¢àµ¶qsMÕ¦Hî´·¦t¾cõ­0¶PõÏÁ÷;^UÃñ>3÷:#ÇF»«ìË.õþ)ÀýzÏÁë¼ÃÀ6öçû6/PqS,Ó%=ÖôøÃGÍÎ-þRq×ZCN~/½»òoöÿ7þYì+£¤zax³¸áÄaÕôÝÞ:xG$ÇÏÈò^ig_wfÌD:HìIý|6u=¼uIAÁË\rtØÄ`ÁgèÉQ;áÛÐìg5¢µZ¥y¶sKd»ETú&-ßä­+Nïé¶WK\rªõÍ¥\Z¦g×XÙø¿Véö7bÑ5nZÉ¦¶áQ%ÉfÝ.X:÷Kµ<Lc]·æZã¹ý?DÇ2ÏIoóiÏ]%áãuØyÕ©Â).ÿ?l<K83û,|qa\0ë1?mN8#!@ Áw0)§¢}þ´³Èÿ­ïï=üË-åÇs\\ë%ýÖt-ÎîxÄ§òwyµÏ¹ì>NáK½TñRLÕôiô^£Qv¬D*É-\rã¢þÜ%ÞÌmaP²Õ­$v°¼^¬Jb~lGS*:2iûtÒ8r=>¨ûWï4sðq	¢¦¸Ùéú¬zÆ¹LKWÉ1·Ë¶½y¼BL<_|?Â¿G¹\"n93]øìüPN}NXîNÏmûjû$w4¨ÂÄËÊ%Q±a£à¨Þ\nSÙTÙç¯î÷gy<ëëGäJHÀEô¥\0ADÄH²\0¤a2?3Sj¡zN2btg.o5 YmMÃvQá×N?	£;<íhh×Xnê¤£-ÛÛ¾°»XÔÒ8OHas0Þb>#ç\'V³)]ù=ÑÐTEâñ¬t}ÉÑv5x{_ÁaE½¤¹\"dxFÇÅûcLO¸Ã:|æg`è?ÁéêPRR\rOÀ»)Â£êh', NULL, '0', 'Azampur', '1', 35.99, NULL, NULL, 'dm4a1594625360', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Afghanistan', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:29:20', '2020-07-13 05:29:20', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(27, 29, 'BZh91AY&SY:S°ª\0uß@@Xø+Kü¿ïÿú`÷-(ÉØ£M 0	¦2\0	`\0\0%\0BSSM4i õ \0\Z\0s\0`!\0&\0\0§¨ô5=@\0P4hhs\0`!\0&\0\0DhM654õCLFÑ©êdèÃØ»Ús\Z´GÐùRê@}>$9:ÁRâ$×Èb2N®r ú89eéfIYÙxöü(Ù.N\n/¢ó+vÊ¤¹T±mÄYaÔ:üÆâõ.õ·&ZóÐ×Ä{Å¡¨8&í\"yÉ2BA	§t¸okÙ°Ú0ÞLÌì=aÌÚ!À=E`@x\"ELD·`à¹¯m .P½Hö·À¬	\ra.7ÃÈà)cYX±Ä	ÛF° Bapá*îìÔÉ±* hn+IÖ(±Çä/	c@Ø5k>30Rl@¨*¨¸Óå°¤ÈAX2dPU\n333!fÀÁ@¨]\\K`¨¡¡°ZDWÄ¼°l&+æALp3$ÖUº-ELÌ@ABdÀã`¡ÃaÆ%}M\nDÞKF¦\"|ÆcdÊA)o©°d*QÜf®¤KÈ(C¤`GKJ¨2\'9TO0äF¤)ã[§XÃJràÑ£²\Zhh`3XHxL,,,AZa@¡:Øj¾ ×$*$>3{`s»cÛZ×\Z2MZE¾qÚaÄÚ«c\nS6\riÍK²Âée±]KSNÖlÖåa³*)­¦«Mµ¹Å7<ç¤â;¦ñÅ$è@NeôlsYm£©ç¢7^±d×sø\n^Ûûêä2L*:²¢8^Ðk@ ,þþÂÊXÆÜ-hJ1»ö\Z=Æ½óú9ÖGä]&¤nLU\rHÿ\ràj*pY\ryòDÑ¬esùl¼¢F\rì»¾óQÔk¦ i@ÝÔèGeTÐÐÚëiBgPVÖ\nÐäbrE \"Ä¡MËÌÆ^3qè@@Cì\ZþC[_,^hM§Q0*¬&Nê`>£N\n	HpZXã2$\\·3*Üc1GT¡ÙæEqÜ>CYÆºøïÝuÀÎfL¡Ì%bÁ fþñ}èâ6¶ÏÌÉö8ÊD)©ø÷÷âD¦\0Hëpv»¶	ì^A½7æ$Ù)µ;VÈ Ï#@h],kÞnæZ½ ô8ÀìÆ9F)ï	rE¡`H0JDHa$`´\'×AÊ«Õ\n@ÀÚP1¤PøZ\Zû1À±2L:R÷[¹>	¡¢IÔJó³êH;Uª4Ë#WøíÜÎEA¨¡ÄýL>Dy2;FØÂÇiÄÃ*6µ!°gvE¸wdn:ç¶ÏÅ¼:bHº	f2d+Â@\\w:¢Îvü:WãÀ^ðcB+ÓI$bLCJbJ\ZL\ZaCaË\r$mKª´+S3$X.	£Bá®CFï9_$\\ZÉ4\rP0dL%¾(L²°\\±¡0ª0izÀóaªÍxñ-GE ÔÒ-ìhQÌg´ëd&ÃFk²m2`ñæ5êyÉ5hrwã:&àÄ}Æ ²eÿ¹\"(H)ØU\0', NULL, '0', 'Azampur', '1', 35.99, NULL, NULL, 'zf8X1594625400', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Albania', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:30:00', '2020-07-13 05:30:00', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0);
INSERT INTO `orders` (`id`, `user_id`, `cart`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(28, 29, 'BZh91AY&SY×òó4\0,_@\0Xø++ü>¿ïÿú`Üø\0Pæ4À&C\00L\0\0`Ld0\0À\0\0æ4À&C\00L\0\0¢Ié=OSCCAê\0\0Ð\r\0Ó\0\0Á0\0\0!@A ¢zM¦Qå&hõ4õL3\re$õGÉ±ño~:ñÌµgØûª>¶¤ªs°úäÙ«\rNÊÎuGïô#ÒAú^©üD+;aît§oîÈÐÅ×¨¤c;êmig¦&Ñâ»fâ°`7ßi´»ýOÝuC3¡êpð;y¸àx>¡Q¢¤å¤&çp_cM{v·+´Ã¸õ:fÍJÀï±âf8î&BOß\n¦Ò8 ÷ÊX$ûî!uAK#j,¢§C_NÒ¯­ø\\EÇa«ìHÂàZ bnøªÅX>FL\ZÁ¬^qJCh«Qh*FÚm°ª´Í[ÂÀ©¢ö½cÔâj3 ®Í¡È´LgJVdÕM¢ÐZÚd\r¥@ËAcÉ\r¤Yl¢úáfL¨¼N¥i&¦+.ÖÀYRX/I¨vµñ{\Z\nY,S±+\n÷£EekÃøH^è\"_	Q1\"û,Ä¶ãÅ³)×TÉªfÑmÙn\"È²Tª,%UBçb²Ë75íÆ.$+$?1 ®¨cê½ËÎ0kZ+ZÜÙÒæ&úf+eÇeà¾ Ë.<ë&·îÐ=Tð9kY#1hÒÌfºÍt1ÂÙlÆl<Éé÷qÄèz|O#\'âXöÇÄî&×[^8??7Í¼ps.ùÆ{Ûú-Wq·ðÇ±ÉPxW\Z\r)fÉ¯h ÁênSóº ÙÂ0(7x>F§ý5ÿ_#ñ$ëRG3ó±JNÀØlV7£æjrys¹m\Zó?®#Ëîñã¡dl#N&ÝvJË ð´4`ÐÚëiqç!kÍÞ_j3&dÑhÌâ|~S×ÌF:ùV4¹C52<	,\r»Y?CÐÔê5,5\n\rEýCIÚBZó$Êíúîáõhg`öµmæîËo	F/Èùÿ#\\áå2FÆs4xá:xQE \r f¿X½è6¸5ìo[y\ZN>×*ÈÀ¥ãh¢¯+8yøÁ&EÖ:Ï#B$ªk]?37Ê,3õ$í	àösiõ÷¿C¬=bÀ-áGqÄBÅÀÖÌÀ;$`*W3´³VÈºÕX±cHl,R¨-cHàRÜX ²Rb¡Èµ6&uê$?²öººw$¨rÈ3d´<Qô$<n4Ìá­=øÉñÜt.\"\råãàÂù#ètÈó\Zccdy®ÛMo°g©ÈÚy®cÓyÌ½uÂ\ZûGàÜIJÄfÉdÈY	ÖBZävD\Z3ìò¾¬<Yöï18U¾ZÀ¥Tª¨ÅªÈÄ44&ÂN&§ÈRu%ÐW^\'AÈ@A#RÄBîáCÇ¹ÀY½ºJ0g)4Mð0dÍMgX£+!°àG4\\°a\Z_h¬8e±¯ñÞg]Çæ%J®hD£Ð|û<AaÚÍw&ÆL,àÎoXÏÂîú\\±;*ÖæÝs.þôKKS¹2ñw$S	\r/3@', NULL, '0', 'Azampur', '1', 20, NULL, NULL, 'nt3C1594625452', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Afghanistan', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:30:52', '2020-07-13 05:30:52', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(29, 29, 'BZh91AY&SY¬¥º\0uß@@Xø+Kü¿ïÿú`b$xÂM\0)£À&È`\0&	\0\0Ñ1	MÔôõê© Ó4s\0`!\0&\0\0\"£i¦42a4i¦ i£ÌiL\0`\0\0I#I$\r\Zh	LÉ=M\rI¦§ê.HÐþR}?îÅß>ò§êµ#ó8e±Ï<iÊ,Ø!fT±Ák¢{ÚTs,4qý!ô\\ÕÃXÃF¥jáÃÞÚ.S`ìÉAÇ#XsyÈÊÑ=Ó¯jg£ÙÓÍ]ÉÉÐsbÄOBu$Ê(b¢t¼ý\rTiÃ­GsàxºÍ¢yï]÷>GMRq2±1³uOìÆ°Ò\'¾>å¢uïK#eI(¤pT4*Kj|yR&\ZålÖÊkdXÀ£å*ª:K,NÂ\nWÊl1Çià8î$IêqO-À&4/xP9 NÈ	 MÆu8lÁ0Hw\Z|¢Mh0VM1h\n53³p iL]\\bø¥ª*2½°ËæÑÔÄÝ34ljb?JeºÙÞü1h²Å.Åb2@ä(àÁÉh#sB17¢ÙéÌ`°Äk\ZÆ`Ñ¦ÁqSa-vÅLñ\nf®¤kÐ Ä62I$5µV\r4´ÉõõG­ÿB4\r!Hå!~\'XÃJrpfÑêó30AÖ K ËBxCJ6ºN};ifPNc©Åé]UEEÙ¼9¢hêÜðùª.5ã7å[E;äÞàÜ¹J\\[£+´\'{8zòú#{!r¯Üo/r+1Ü{¼AÔ|Çq$ô°7zôQ¸;ì·ÙÏøQG;¯ÈRÃ <îb@qáBÂ§jBtÍy9¾±ÀYþ<D7ict=±,ç.Óó6~¦üÿ±þy\Zò¹O;±8µtGêÞîóeO¯àÆ:TËÇ«Eär¡Ô£ón÷hèv:oÐTUfVO(ÕYÍYJ©ÛRZS°Ë<Ì®ðkL¶ì[6¥åGG4SÙbÅ«ÍSÏ0ö÷×ÛÕÁÁ¡òaÙl)9màl^CdÃ\'hg63s1ÀìÚÌAm×M®r=Zâ§Zµ©{¶ëÇe¯÷ý8<êv÷µî4¬ºl§<X®ñ0ÍªYNïr}ÍSæ#ô{Dã~ûCØ) 8®¡J³Í^¾`ð?ëEÑwsÜÕg¯\ZkÅYÅÔÿ,8Dòs¥OÉÇÅ^NÃÚÖqóÅè³Ê2¶TÝv;ÚkbÍyÁF¾rcÛu;M%×Z0alì¡Ö»µéR.ú/:,rdnø&*²jÚQØ±ïÇÅ8wÈÖ*0K3¼<ÿv¤Ï%Jfùªióë·§è³¡w7ãF\'è·ìðRôT¥RÞÛöÈcS ðUõÞòU»gn=v}ôM-X7Z]T×¶´ÐÀn{{½e³Go[èñ·É?\"d\"¸ã\0A	2DCi©$µIEDª.êoxkc~¹9¦S,, ÁfãÞºÖý6jx÷ÕÕhóÔ1frÌî C7&¶BÛr±2õ\'À}hèÎ©SçÍýS´Ìd¶s[Ùý£ê·kâÀXÃ´¦ÉTr¦¥ÊùñiöTeíóêÜúûá_äh(qà.äp¡!YKt,', NULL, '0', 'Azampur', '1', 35.99, NULL, NULL, 'RbrF1594625485', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'United States', '8328311884', '7800 Harwin Drive, Suit A4 Houston, TX 7703', 'austin', '78701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:31:25', '2020-07-13 05:31:25', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(30, 29, 'BZh91AY&SYÜ4\0Lß@@Pÿø;Kýn¿ïÿú`¿\0\0Ðª\0\0\0L`Âa4À\0dÓ@ÃDÉé	@õ@\0\0d@\0a0		Ó\0\0	MÂ`\0&	¦\0\0&9À&\0L&L\0\0&M4$DBb1\ZbÐPl§4ôÌ©²=RA\"¼>çÌõ\'È:Áû\rIä10üãèIö¡öûAsFKZ\">YÌ}FHIÃÕªêjKè§O&FEêjµ\"¾5Òr«Êóû³;Ñ­(pÁmZÊ`ÀWaßë5cèÔEÕðyë\'©j$Ü4y\"	4.ÐHÔ}C3´ædt<ÐÂð8ðÚ{ÉímFd¡KB^ö­h(¿¡¾00hYHap#A¥ ÀL-2ÎMXh46´.\\¨	D\"æ5;\n\0ØÄÜ^ô¼QpiPmc@Ø5ZÍmY(\rÂLÂaï,³(^I) @HVØMm3J²áA0%\"µi|AxB»BfX¾V\n+´fÃÜXXÉ5³ëSö50AEJ3TÁbCBHETj^\nÚ`¢ÁM\nNTÊ#%JQ\rE3&GèdîÀ)&ÂE@PU0T£©*êÖ¡ª­x¤/%÷MlCb&!ÿ­@Û!41©PQ6uHPg`27(i\r&Æ!M1Ä4iiÈ¶ØÎtQ!Ä(Íò\nçAo$W¨Úå¤æV\ZébÒÐZð°ËD`Ï93¬uç)ØY{\nÒUQk	(±HbZ¤ÂÀPÄ&0ÂywÓÈÞ~F\n±òH^Çû¡ÈÔ{îô=FfyÌýOH_à=fàøàÙð¿©ÉHC´+\rKÄüLø\nÏàÖ{OÈÁ­±UÎãô3>fOø$*Här_J):CESbò38°±Öµ\r~92 ÚÆ|6äU#hË¦¼õJÊÈwY0hmoiBØÀV§#CamKaLÁ¨fm<È[¾cS¿oáKåb3X40R-,Xu0²?ø37<D©5\n¤ÈàÎu;É.¸³eM§y¥tGVTÕÓ:ÂU´àxþ Ü×³>¶@Î¾Ê(;@¦\ræCÔ)Ãò ëFÖ½$SóBØwdu¡\"\0cê3Ü>þø$ìR¤ê2 îÐ$F³Ëq°ý:z¹A¹®Ðófà)×Øs â@ÂÑ\'p¯RiPÁTp¢Ñr+ÂÃ8Yª c(P0R(ÆÒaRT»Å#f³7­ÝÓ©#49I^©`çø$;V,4Ï1á¬ýÛ£·aÈ¨:\nRñ#ÚrÈí\Zcc.0öÎ Q­AÖ6ïÈæ·úôÛgsÐð «4z!tÄ%¡À\rñgf=íëpÏI¬õ¥ñ14oØÈ@Ó0Lh bl66Ò`Ð\nÍEµAÊQ]év\nËk@AFEHË¢Ã\'Vò;òsCHÌ×@XÌÎxEJX­Qb¡thÑëÍF\Z÷vÏYÜ¸*\"0¸¢8¿ÀøôÚ$ÇAK,ÍÉ°¼Ïýî½Eêz»Ð¬|DÒ½>¤&Á±Cú¿ø»)Âà	¤`', NULL, '0', 'Azampur', '1', 24.99, NULL, NULL, 'WiVP1594625684', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Afghanistan', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:34:44', '2020-07-13 05:34:44', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0),
(31, 29, 'BZh91AY&SYÈÒ¿\0Lß@@Pÿø;Kýn¿ïÿú`¿\0\0\0\0\0\0Ì&0a0`\02i cL`Âa4À\0dÓ@Ç0À	Âi\0É¦a0		Ó\0\0	MÂ`\0&	¦\0\0&\"Ò$ÚhL£ð(<SÊ1¦eMê	pû3Òw ê/ð5\'ÄÃðTLaÿ¡\'ÚÛíÌ-lúg2Aõ!&ódB«ÙaIrè§_Fêlµ\"¾6Òt«Òóü37£ZÐãÛ6Á®ã»Òm.ÇÍ°«àñ+ÔOB8ØI	#´iñ\ZD h^PHØ}C\'ì49 ¡ô0½GÜÏq#=\rèÉ\0¨RåP¹«Z\nbt`a)\0Ð²\r!ÂF£A	Zé5ua¨Ð¡µ©pàØ5@¸èJ!ï4¬¬¹` \rMÅïKÀUØ&4\rU¬ÖÕÐ\\$ÈL1½åeÉ%5	\nÐ	­¦iV\\(0 d	H£E­Z_! h ÐWhLÓ.úX%X=5Ð¨BÆ­-Í\n¿ÜÚPÁ\Z(@ÌAS\rI!PNkRðVÓ^\nn¸Rt¦Ä\Z*PL¼áÚ·\nª(T½¨­\niIkZö¨¯ 0¤ª` ©GRUÕ­CEU[Dw¤/÷MlCb&!ý·@44Ô¨(:	$ Î°e\ZnPÒ\ZMCc hÒÓo1¹Ûi)\n¢BØfØ+ÂKo|-bÓ{tÐ­Õâµ-:^u¨àÈµ´¼,2Ñ33X,ëÊäÁ,½ÍÊëy/ÒéÉÍæË\\â1QÜdØ^ñ\Zðí=Î~F\n±òH^·üó°÷]êzÄ3õ<áõ\rÜây·|wÂþ%!Ð¬5.\ZR_ñ3Ä@P.&ÓÚ:w>F6o*¨7vó\'ÌÏÓô?¢O\"9çRNÔÕTÜ¼ n,u-_Àì(9-´gÃ~R:XÆ\\Øo6çh4 l©Öu¡£×;HÞÆµ9\ZlXªf\r03yâBÜä%ö\rO?Isñ¥ô±AX50R-,Xu0´?£ù2s*xRlIÁÚBZî$ºéfêÎãZòWVTÙä¡*Úñ;ÿPpkQ&·@Î®º(< S9 Æt\nFqüæ:½¯9\'ÔüÆÐõºHcHúIíwtu¡Iä:\r;u	%Q¬éÀÅBØE~ä<È]H3\"õÞ,à:ºÎÂ;Ez¸k\\\n\n¤Dû«^6ÒYeT©\Z@Â©F4·[\n¥Ü)+\'ÔJáÖ-z2)2Õ,CÊ±a¦xÐögÛÂ<»EDAÌTéJC¼aÈc#Ê4ÆÆ\\aì1F¶PØ3»C±põ8ôßg©Ð;5$¼(*ÍdÄÈ\\ÆJÒÔâA<A¯nËeõ8gÚzRø7ëd\0Á i&416@i0hMNsamrW.±Yr-cq\"ábÐ©¹sAìäà*ÞY%\\ÆÒ2m ,dÌñ0°Z9\"ÅBèÕ£Òl65íë1§jé	TDat¢8¿Ðú|\0Â7yiÄÔRÃ¥rn$¯ÔÓm×¬h½OGry×ÐØ6(`ÿRWÿrE8PÈÒ¿', NULL, '0', 'Azampur', '1', 24.99, NULL, NULL, 'aIZC1594625719', 'Pending', 'devmrmsoft@gmail.com', 'john doe', 'Albania', '8328311884', 'asdfsdfs', 'las vegas', '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-07-13 05:35:19', '2020-07-13 05:35:19', NULL, NULL, '$', 1, 0, 0, 0, 0, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_tracks`
--

CREATE TABLE `order_tracks` (
  `id` int(191) NOT NULL,
  `order_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tracks`
--

INSERT INTO `order_tracks` (`id`, `order_id`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 2, 'Pending', 'You have successfully placed your pending order.', '2020-05-27 13:12:12', '2020-05-28 00:59:13'),
(2, 1, 'about to', 'testing', '2020-05-28 00:45:27', '2020-05-28 00:45:27'),
(3, 3, 'Pending', 'You have successfully placed your order.', '2020-06-01 06:58:08', '2020-06-01 06:58:08'),
(4, 4, 'Pending', 'You have successfully placed your order.', '2020-06-01 07:09:46', '2020-06-01 07:09:46'),
(5, 5, 'Pending', 'You have successfully placed your order.', '2020-06-01 07:14:06', '2020-06-01 07:14:06'),
(6, 6, 'Pending', 'You have successfully placed your order.', '2020-06-01 07:19:39', '2020-06-01 07:19:39'),
(7, 7, 'Pending', 'You have successfully placed your order.', '2020-06-05 04:39:02', '2020-06-05 04:39:02'),
(8, 8, 'Pending', 'You have successfully placed your order.', '2020-06-05 04:43:35', '2020-06-05 04:43:35'),
(9, 9, 'Pending', 'You have successfully placed your order.', '2020-06-05 04:48:27', '2020-06-05 04:48:27'),
(10, 10, 'Pending', 'You have successfully placed your order.', '2020-06-16 04:36:59', '2020-06-16 04:36:59'),
(11, 11, 'Pending', 'You have successfully placed your order.', '2020-06-16 04:55:44', '2020-06-16 04:55:44'),
(12, 12, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:03:00', '2020-06-16 05:03:00'),
(13, 13, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:37:10', '2020-06-16 05:37:10'),
(14, 14, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:37:56', '2020-06-16 05:37:56'),
(15, 15, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:38:56', '2020-06-16 05:38:56'),
(16, 16, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:40:48', '2020-06-16 05:40:48'),
(17, 17, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:42:30', '2020-06-16 05:42:30'),
(18, 18, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:49:11', '2020-06-16 05:49:11'),
(19, 19, 'Pending', 'You have successfully placed your order.', '2020-06-16 05:52:33', '2020-06-16 05:52:33'),
(20, 20, 'Pending', 'You have successfully placed your order.', '2020-06-17 09:49:00', '2020-06-17 09:49:00'),
(21, 21, 'Pending', 'You have successfully placed your order.', '2020-06-19 09:26:09', '2020-06-19 09:26:09'),
(22, 22, 'Pending', 'You have successfully placed your order.', '2020-06-20 00:33:35', '2020-06-20 00:33:35'),
(23, 23, 'about to deliver', 'order details', '2020-06-20 10:46:00', '2020-06-20 10:49:14'),
(24, 24, 'Pending', 'You have successfully placed your order.', '2020-06-20 10:47:56', '2020-06-20 10:47:56');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `user_id`, `title`, `subtitle`, `price`) VALUES
(1, 0, 'Default Packaging', 'Default packaging by store', 0),
(2, 0, 'Gift Packaging', 'Exclusive Gift packaging', 15);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(191) NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `header` tinyint(1) NOT NULL DEFAULT '0',
  `footer` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `details`, `meta_tag`, `meta_description`, `header`, `footer`) VALUES
(1, 'About Us', 'about', '<div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 1</font><br></h2><p><span style=\"font-weight: 700;\">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 2</font><br></h2><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><br helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 3</font><br></h2><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><h2 helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-weight:=\"\" 700;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px=\"\" 15px;=\"\" font-size:=\"\" 30px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" style=\"font-family: \" 51);\"=\"\"><font size=\"6\">Title number 9</font><br></h2><p helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', NULL, NULL, 1, 0),
(2, 'Privacy & Policy', 'privacy', '<div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><img src=\"https://i.imgur.com/BobQuyA.jpg\" width=\"591\"></h2><h2><font size=\"6\">Title number 1</font></h2><p><span style=\"font-weight: 700;\">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 2</font><br></h2><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><br helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 3</font><br></h2><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><h2 helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-weight:=\"\" 700;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px=\"\" 15px;=\"\" font-size:=\"\" 30px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" 51);\"=\"\" style=\"font-family: \"><font size=\"6\">Title number 9</font><br></h2><p helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'test,test1,test2,test3', 'Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 0, 1),
(3, 'Terms & Condition', 'terms', '<div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 1</font><br></h2><p><span style=\"font-weight: 700;\">Lorem Ipsum</span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 2</font><br></h2><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><br helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h2><font size=\"6\">Title number 3</font><br></h2><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><h2 helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-weight:=\"\" 700;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px=\"\" 15px;=\"\" font-size:=\"\" 30px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" 51);\"=\"\" style=\"font-family: \"><font size=\"6\">Title number 9</font><br></h2><p helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 't1,t2,t3,t4', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pagesettings`
--

CREATE TABLE `pagesettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `contact_success` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_title` text COLLATE utf8mb4_unicode_ci,
  `contact_text` text COLLATE utf8mb4_unicode_ci,
  `side_title` text COLLATE utf8mb4_unicode_ci,
  `side_text` text COLLATE utf8mb4_unicode_ci,
  `street` text COLLATE utf8mb4_unicode_ci,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `fax` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `site` text COLLATE utf8mb4_unicode_ci,
  `slider` tinyint(1) NOT NULL DEFAULT '1',
  `service` tinyint(1) NOT NULL DEFAULT '1',
  `featured` tinyint(1) NOT NULL DEFAULT '1',
  `small_banner` tinyint(1) NOT NULL DEFAULT '1',
  `best` tinyint(1) NOT NULL DEFAULT '1',
  `top_rated` tinyint(1) NOT NULL DEFAULT '1',
  `large_banner` tinyint(1) NOT NULL DEFAULT '1',
  `big` tinyint(1) NOT NULL DEFAULT '1',
  `hot_sale` tinyint(1) NOT NULL DEFAULT '1',
  `partners` tinyint(1) NOT NULL DEFAULT '0',
  `review_blog` tinyint(1) NOT NULL DEFAULT '1',
  `best_seller_banner` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link` text COLLATE utf8mb4_unicode_ci,
  `bottom_small` tinyint(1) NOT NULL DEFAULT '0',
  `flash_deal` tinyint(1) NOT NULL DEFAULT '0',
  `best_seller_banner1` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `featured_category` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pagesettings`
--

INSERT INTO `pagesettings` (`id`, `contact_success`, `contact_email`, `contact_title`, `contact_text`, `side_title`, `side_text`, `street`, `phone`, `fax`, `email`, `site`, `slider`, `service`, `featured`, `small_banner`, `best`, `top_rated`, `large_banner`, `big`, `hot_sale`, `partners`, `review_blog`, `best_seller_banner`, `best_seller_banner_link`, `big_save_banner`, `big_save_banner_link`, `bottom_small`, `flash_deal`, `best_seller_banner1`, `best_seller_banner_link1`, `big_save_banner1`, `big_save_banner_link1`, `featured_category`) VALUES
(1, 'Success! Thanks for contacting us, we will get back to you shortly.', 'admin@geniusocean.com', '<h4 class=\"subtitle\" style=\"margin-bottom: 6px; font-weight: 600; line-height: 28px; font-size: 28px; text-transform: uppercase;\">WE\'D LOVE TO</h4><h2 class=\"title\" style=\"margin-bottom: 13px;font-weight: 600;line-height: 50px;font-size: 40px;color: #0f78f2;text-transform: uppercase;\">HEAR FROM YOU</h2>', '<span style=\"color: rgb(119, 119, 119);\">Send us a message and we\' ll respond as soon as possible</span><br>', '<h4 class=\"title\" style=\"margin-bottom: 10px; font-weight: 600; line-height: 28px; font-size: 28px;\">Let\'s Connect</h4>', '<span style=\"color: rgb(51, 51, 51);\">Get in touch with us</span>', '3584 Hickory Heights Drive ,Hanover MD 21076, USA', '00 000 000 000', '00 000 000 000', 'admin@geniusocean.com', 'https://geniusocean.com/', 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, '1568889138banner1.jpg', 'http://google.com', '1565150264banner3.jpg', 'http://google.com', 0, 0, '1568889138banner2.jpg', 'http://google.com', '1565150264banner4.jpg', 'http://google.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(191) NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `photo`, `link`) VALUES
(1, '1591616422brand6.jpg', 'https://www.braven.com/'),
(2, '1591616413brand5.jpg', 'https://www.bosbosshop.com/'),
(3, '1591616401brand4.jpg', 'http://www.baseus.com/'),
(4, '1591616393brand3.jpg', '#'),
(5, '1591616377brand2.jpg', 'http://www.adata.com/en/'),
(6, '1591616368brand1.jpg', 'https://www.acellories.com/'),
(7, '1591616432brand7.jpg', 'https://bumpboxx.com/'),
(8, '1591616439brand8.jpg', 'https://www.cellhelmet.com/'),
(9, '1591616457brand9.jpg', 'https://www.zizowireless.com/');

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateways`
--

CREATE TABLE `payment_gateways` (
  `id` int(191) NOT NULL,
  `subtitle` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(199) DEFAULT NULL,
  `status` tinyint(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_gateways`
--

INSERT INTO `payment_gateways` (`id`, `subtitle`, `title`, `details`, `type`, `status`) VALUES
(47, 'Paypal Express', 'Paypal Express', '<font size=\"3\"><b style=\"\">Paypal Express</b><br></font>', 'secure', 1),
(48, 'Cash On Delivery', 'Cash On Delivery', '<font size=\"3\"><b style=\"\">Cash On Delivery</b><br></font>', 'nonsecure', 1),
(49, 'Bank Transfer', 'Bank Transfer', '<font size=\"3\"><b style=\"\">Bank Transfer</b><b>&nbsp;No: 6528068515</b><br><br></font>', 'secure', 1),
(50, 'Money Order / Cheque Payment', 'Cheque Payment', '<font size=\"3\"><b style=\"\">Cheque Payment</b><br></font>', 'secure', 1),
(52, 'Wire Transfer', 'Wire Transfer', '<font size=\"3\"><b style=\"\">Wire Transfer</b><br></font>', 'secure', 1),
(53, 'Company Cheque', 'Company Cheque', '<font size=\"3\"><b style=\"\">Company Cheque</b><br></font>', 'nonsecure', 1),
(54, 'Open Shipment', 'Open Shipment', '<font size=\"3\"><b style=\"\">Open Shipment<br></font>', 'nonsecure', 1),
(55, 'Personal Cheque', 'Personal Cheque', '<font size=\"3\"><b style=\"\">Personal Cheque<br></font>', 'nonsecure', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pickups`
--

CREATE TABLE `pickups` (
  `id` int(191) UNSIGNED NOT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickups`
--

INSERT INTO `pickups` (`id`, `location`) VALUES
(2, 'Azampur'),
(3, 'Dhaka'),
(4, 'Kazipara'),
(5, 'Kamarpara'),
(6, 'Uttara');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(191) UNSIGNED NOT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `product_type` enum('normal','affiliate') NOT NULL DEFAULT 'normal',
  `affiliate_link` text,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `category_id` int(191) UNSIGNED NOT NULL,
  `subcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `childcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `attributes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_qty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `color_qty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color_price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `previous_price` double DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `stock` int(191) DEFAULT NULL,
  `policy` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `views` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text,
  `colors` text,
  `product_condition` tinyint(1) NOT NULL DEFAULT '0',
  `ship` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_meta` tinyint(1) NOT NULL DEFAULT '0',
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `youtube` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Physical','Digital','License') NOT NULL,
  `license` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `license_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `platform` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `licence_type` varchar(255) DEFAULT NULL,
  `measure` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `best` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `top` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `hot` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `latest` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `big` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `trending` tinyint(1) NOT NULL DEFAULT '0',
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_date` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_discount` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_catalog` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_id` int(191) NOT NULL DEFAULT '0',
  `color_gallery` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_prices` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `pro_type_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `pro_type_child` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `acc_brand_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `acc_type_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `carrier_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `cases` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `headphones` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `chargers` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cables` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `color_qty`, `color_price`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `color_gallery`, `vendor_prices`, `pro_type_id`, `pro_type_child`, `acc_brand_id`, `acc_type_id`, `carrier_id`, `cases`, `headphones`, `chargers`, `cables`) VALUES
(95, 'pr495jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 95', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027732dTwHda8l.png', '1568027751AidGUyJv.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 103, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, 0, 0, 1, '2019-09-09 07:36:06', '2020-11-23 19:49:51', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(116, 'pr496jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 116', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027684whVhJDrR.png', '1568027717gm0tFzeb.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', 'Keyword1,Keyword 2', '#ff1a1a,#0fbcd4', 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:15:17', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(117, 'pr497jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 117', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027658Up0FIXsW.png', '1568027670dTA7gQml.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:14:30', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(118, 'pr498jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 118', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027631cnmEylRa.png', '1568027643PgYviwVK.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:14:03', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(119, 'pr499jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 1', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027603i5UAZiKB.png', '1568027616O1coe3aV.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:13:36', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(120, 'pr500jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 120', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027558gLSECTIh.png', '1568027591b1oUIo7Q.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 1, 0, '2019-09-09 12:36:06', '2019-09-09 10:53:33', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(121, 'pr501jsv', 'affiliate', 'https://www.amazon.com/Rolex-Master-Automatic-self-Wind-Certified-Pre-Owned/dp/B07MHJ8SVQ/ref=lp_13779934011_1_2?s=apparel&ie=UTF8&qid=1565186470&sr=1-2&nodeID=13779934011&psd=1', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 121', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-1-pr495jsv', '1568027534O1QEBPpR.png', '1568027543P8eoamtf.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 0, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2019-09-09 10:12:23', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(122, 'pr502jsv', 'affiliate', 'https://www.amazon.com/Alex-Vando-Shirts-Regular-Sleeve/dp/B072QYBXYV?pf_rd_r=9GJCSWTVBD8RT6JMEF0T&pf_rd_p=ad021af6-b7b0-4b98-946a-3a6865266868&pd_rd_r=611e6c6d-7326-449e-a8cf-ce8c1ba118cb&pd_rd_w=wk7Xs&pd_rd_wg=s8T3g&ref_=pd_gw_unk', 13, 4, NULL, NULL, NULL, 'Affiliate Product Title will Be Here. Affiliate Product Title will Be Here 122', 'affiliate-product-title-will-be-here-affiliate-product-title-will-be-here-122-pr502jsv', '1568027493eLqHNoZP.png', '1590041187dngDcz0K.jpg', NULL, NULL, NULL, NULL, '#000000,#a33333,#d90b0b,#209125', NULL, NULL, 50, 100, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 55555, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 47, 'watch', NULL, NULL, 2, '5-7 days', 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2019-09-09 12:36:06', '2020-11-22 18:21:35', 1, '09/08/2021', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(300, 'stV0323qQA', 'normal', NULL, 0, 44, 95, NULL, NULL, 'Apple iPhone 6 Plus, Gold, Unlocked', 'apple-iphone-6-plus-gold-unlocked-stv0323qqa', '16056407221.jpeg', '1605640722BXkND4mx.jpg', NULL, '16,64,128', '100,100,100', '0,10,30', '#edd5bd', '0', '0', 150, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85);\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">CARRIER - This phone is locked to Simple Mobile from Tracfone, which means this device can only be used on the Simple Mobile wireless network.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">PLANS - Simple Mobile offers a variety of coverage plans, including 30-Day Unlimited Talk, Text &amp; Data. No activation fees, no credit checks, and no hassles on a nationwide lightning-fast network. For more information or plan options, please visit the Simple Mobile website.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">ACTIVATION - You’ll receive a Simple Mobile SIM kit with this iPhone. Follow the instructions to get the service activated with the Simple Mobile plan of your choice.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">5.5-inch Retina HD display</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">12MP camera and 4K video</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">5MP FaceTime HD camera with Retina Flash</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">Touch ID for secure authentication and Apple Pay</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">A9 chip</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">iOS 12 with Group FaceTime, Screen Time, and even faster performance</font></span></li></ul><div class=\"a-row a-expander-container a-expander-inline-container\" aria-live=\"polite\" style=\"color: rgb(85, 85, 85);\"><div class=\"a-expander-content a-expander-extend-content a-expander-content-expanded\" aria-expanded=\"true\" style=\"\"><ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\"><br></li><li style=\"margin: 0px; padding: 0px;\"><br></li></ul></div></div>', NULL, '<br>', 1, 8, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 01:18:42', '2020-11-20 14:44:16', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(301, 'fg81558rL4', 'normal', NULL, 0, 44, 96, NULL, NULL, 'Apple iPhone 6s, Gold, Unlocked', 'apple-iphone-6s-gold-unlocked-fg81558rl4', '16056417791.jpeg', '1605641779yIW2fUv9.jpg', NULL, '16,32,64', '100,100,100', '0,30,50', '#edd5bd', '0', '0', 130, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85);\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">CARRIER - This phone is locked&nbsp;to Simple Mobile from Trachoma, &nbsp;which means&nbsp;this device can&nbsp;only be used on the Simple&nbsp;Mobile wireless network.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">PLANS - Simple Mobile offers a&nbsp;variety of coverage plans, &nbsp;including 30-Day Unlimited Talk, &nbsp;Text &amp; Data. No activation fees, &nbsp;no credit checks, and no&nbsp;hassles&nbsp;on a nationwide lightning-fast&nbsp;network. For more information or&nbsp;plan&nbsp;options, please visit the&nbsp;Simple Mobile website.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">ACTIVATION - You’ll receive a&nbsp;Simple Mobile SIM kit with this&nbsp;iPhone. &nbsp;Follow the instructions to&nbsp;get the service activated with the&nbsp;Simple Mobile plan of&nbsp;your&nbsp;choice.</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">4. 7-Inch Retina HD display</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">12MP camera and 4K video</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">5MP FaceTime HD camera with Retina Flash</font></span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">Touch ID for secure authentication and Apple Pay</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">A9 chip</font></span></li><li style=\"margin: 0px; padding: 0px;\"><span class=\"a-list-item\"><font size=\"2\">Ios 12 with Group FaceTime, Screen Time, and even faster performance</font></span></li></ul><div><br></div>', NULL, '<br>', 1, 8, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 01:36:19', '2020-11-20 11:37:45', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', 'Select Accessory', '', NULL, 0, 0, 0, 0),
(302, '9If2064rBC', 'normal', NULL, 0, 44, 97, NULL, NULL, 'Apple iPhone 6S Plus,  White Box', 'apple-iphone-6s-plus-white-box-9if2064rbc', '16056427641.jpg', '1605642764dvm0OW2J.jpg', NULL, '16,32,64,128', '100,100,100,100', '0,20,40,50', '#c0c1c6,#ebc2bc,#dddee0,#d5c3af', '0,0,0,0', '0,0,0,0', 175, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.5-inch Retina HD display</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12MP camera and 4K video</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5MP FaceTime HD camera with Retina Flash</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Touch ID for secure authentication</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">A9 chip</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">iOS 12 with Group FaceTime, Screen Time, and even faster performance4</span></li></ul>', NULL, '<br>', 1, 13, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 01:52:44', '2020-11-20 11:56:02', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Space Grey\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427644.jpg\",\"16056427645.jpg\",\"16056427646.jpg\"]},{\"key\":1,\"color\":\"Rose Gold\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427644.jpg\",\"16056427645.jpg\",\"16056427646.jpg\"]},{\"key\":2,\"color\":\"Silver\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427643.png\",\"16056427644.jpg\"]},{\"key\":3,\"color\":\"Gold\",\"images\":[\"16056427642.jpg\",\"16056427643.jpg\",\"16056427644.jpg\",\"16056427645.jpg\"]}]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(303, 'ZvT3089IdC', 'normal', NULL, 0, 44, 98, NULL, NULL, 'Apple iPhone 7  White Box', 'apple-iphone-7-white-box-zvt3089idc', '16056434763.jpg', '1605643476N5JngEdB.jpg', NULL, '32,128', '100,100', '0,25', '#000000,#ebc2bc,#dddee0,#d5c3af', '0,0,0,0', '0,0,0,0', 190, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.).</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic, in which case it will be UL or Mfi (Made for iPhone) Certified.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arm\'s length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 12, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 02:04:36', '2020-11-23 22:46:17', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16056434761.jpg\",\"16056434763.jpg\",\"16056434764.jpg\",\"16056434765.jpg\",\"16056434766.jpg\"]},{\"key\":1,\"color\":\"Rose Gold\",\"images\":[\"16056434761.jpg\",\"16056434762.jpg\",\"16056434763.jpg\"]},{\"key\":2,\"color\":\"Silver\",\"images\":[\"16056434762.jpg\",\"16056434763.jpg\",\"16056434764.jpg\"]},{\"key\":3,\"color\":\"Gold\",\"images\":[\"16056434762.jpg\",\"16056434764.jpg\",\"16056434765.jpg\",\"16056434766.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(304, 'SUX3609Ku0', 'normal', NULL, 0, 44, 99, NULL, NULL, 'Apple iPhone 7 Plus Jet Black  Unlocked', 'apple-iphone-7-plus-jet-black-unlocked-sux3609ku0', '16056444743.jpg', '160564447456rVG8Zp.jpg', NULL, '32,128', '100,100', '0,25', '#26272b,#ebc2bc,#000000', '0,0,0', '0,0,0', 290, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.).</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic, in which case it will be UL or Mfi (Made for iPhone) Certified.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arms length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 7, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 02:21:14', '2020-11-25 14:15:12', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(305, 'WCC5121qo2', 'normal', NULL, 0, 44, 101, NULL, NULL, 'Apple iPhone 8 Plus  White Box', 'apple-iphone-8-plus-white-box-wcc5121qo2', '16056454354.jpg', '1605645435aaePGk0T.jpg', NULL, '64,256', '100,100', '0,55', '#e6e7eb,#ebc2bc,#000000', '0,0,0', '0,0,0', 360, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.5-inch Retina HD display</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">IP67 water and dust resistant (maximum depth of 1 meter up to 30 minutes)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12MP dual cameras with OIS, Portrait mode, Portrait Lighting, and 4K video</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">7MP FaceTime HD camera with Retina Flash</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Touch ID for secure authentication</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">A11 Bionic with Neural Engine</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">iOS 12 with Screen Time, Group FaceTime, and even faster performance</span></li></ul>', NULL, '<br>', 1, 10, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 02:37:15', '2020-11-25 21:46:33', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Silver\",\"images\":[\"16056454352.jpg\",\"16056454353.jpg\",\"16056454354.jpg\",\"16056454355.jpg\"]},{\"key\":1,\"color\":\"Rose Gold\",\"images\":[\"16056454352.jpg\"]},{\"key\":2,\"color\":\"Black\",\"images\":[\"16056454352.jpg\",\"16056454353.jpg\",\"16056454354.jpg\"]}]', NULL, '19', '', '', '', '1,2,4,5,6,7', 0, 0, 0, 0);
INSERT INTO `products` (`id`, `sku`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `color_qty`, `color_price`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `color_gallery`, `vendor_prices`, `pro_type_id`, `pro_type_child`, `acc_brand_id`, `acc_type_id`, `carrier_id`, `cases`, `headphones`, `chargers`, `cables`) VALUES
(306, 'jcQ5469UFl', 'normal', NULL, 0, 44, 69, NULL, NULL, 'Apple iPhone X Unlocked', 'apple-iphone-x-unlocked-jcq5469ufl', '16056457181.jpg', '1605645718cB1APRJT.jpg', NULL, '64', '100', '0', '#000000,#e4e4e2', '0,0', '0,0', 425, 0, '<div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Originally released September 2017</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Unlocked, SIM-Free, Model A1865<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">5.8-inch Super Retina HD display with OLED technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">A11 Bionic chip with embedded M11 motion coprocessor</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Talk time (wireless) up to 21 hours</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">LTE and 802.11ac Wi‑Fi with MIMO</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Bluetooth 5.0 wireless technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">NFC with reader mode</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">12MP wide-angle and telephoto cameras</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Digital zoom up to 10x</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">1080p HD video recording</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">7MP TrueDepth camera with Portrait mode</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Face ID</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Siri</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Apple Pay</p></div><div class=\"para-list as-pdp-lastparalist\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">6.14 ounces and 0.30 inch</p></div>', NULL, '<br>', 1, 7, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 02:41:58', '2020-11-23 15:11:28', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16056457182.jpg\",\"16056457183.jpg\"]},{\"key\":1,\"color\":\"Silver\",\"images\":[\"16056457182.jpg\",\"16056457183.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(307, 'EPW0157SuU', 'normal', NULL, 0, 44, 70, NULL, NULL, 'Apple iPhone Xs Max  Unlocked', 'apple-iphone-xs-max-unlocked-epw0157suu', '16056504493.jpg', '1605650449obK23hKx.jpg', NULL, '64,256', '100,100', '0,25', '#f9dbc1,#000000,#e5e5e3', '0,0,0', '0,0,0', 575, 0, '<div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Originally released September 2018</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Unlocked, SIM-Free, Model A1921¹</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">6.5-inch Super Retina HD display with OLED technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">A12 Bionic chip with next-generation Neural Engine</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Talk time (wireless) up to 25 hours</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Gigabit-class LTE with 4x4 MIMO and LAA</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">802.11ac Wi‑Fi with 2x2 MIMO</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Bluetooth 5.0 wireless technology</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">NFC with reader mode</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Dual 12MP wide-angle and telephoto cameras</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">2x optical zoom; digital zoom up to 10x</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">4K video recording, 1080p HD video recording</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">7MP TrueDepth camera with Portrait mode and lighting</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Face ID</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Siri</p></div><div class=\"para-list\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Apple Pay</p></div><div class=\"para-list as-pdp-lastparalist\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 16px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">7.34 ounces and 0.30 inch</p></div>', NULL, '<br>', 1, 6, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 04:00:49', '2020-11-25 12:02:10', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Rose Gold\",\"images\":[\"16056504492.jpg\",\"16056504493.jpg\"]},{\"key\":1,\"color\":\"Black\",\"images\":[\"16056504491.jpg\",\"16056504492.jpg\"]},{\"key\":2,\"color\":\"Silver\",\"images\":[\"16056504491.jpg\",\"16056504492.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(308, 'zVx0521NLO', 'normal', NULL, 0, 44, 71, NULL, NULL, 'Apple iPhone 11 Unlocked', 'apple-iphone-11-unlocked-zvx0521nlo', '16056508352.jpg', '1605650836UHDk6Mmx.jpg', NULL, '64,128', '100,100', '0,50', '#ded9e0,#ceeee1,#d62a44,#000000', '0,0,0,0', '0,0,0,0', 575, 0, '<table border=\"0\" width=\"1151\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-spacing: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><tbody><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">3G bands</td><td class=\"xl67\" width=\"872\" style=\"padding: 0px;\">HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100</td></tr><tr data-spec-optional=\"\"><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">4G bands</td><td class=\"xl71\" style=\"padding: 0px;\">LTE</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"24\" style=\"padding: 0px;\">Screen Size</td><td class=\"xl68\" width=\"872\" style=\"padding: 0px;\">6.1‑inch</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Resolution</td><td class=\"xl65\" style=\"padding: 0px;\">828 x 1792 pixels</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Dimensions</td><td class=\"xl65\" style=\"padding: 0px;\">150.9 x 75.7 x 8.3 mm (5.94 x 2.98 x 0.33 in)</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Weight</td><td class=\"xl65\" style=\"padding: 0px;\">194 g</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">SIM</td><td style=\"padding: 0px;\">Dual SIM (Physically)</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">OS</td><td class=\"xl65\" style=\"padding: 0px;\">iOS 13</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"23\" style=\"padding: 0px;\">Chipset</td><td class=\"xl73\" width=\"872\" style=\"padding: 0px;\">A13 Bionic chip</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Rom</td><td class=\"xl64\" width=\"872\" style=\"padding: 0px;\">64GB</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Ram</td><td class=\"xl64\" width=\"872\" style=\"padding: 0px;\">4GB</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"23\" style=\"padding: 0px;\">Primary</td><td class=\"xl72\" style=\"padding: 0px;\">Dual 12 MP+12 MP</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"23\" style=\"padding: 0px;\">Secondary Camera</td><td class=\"xl72\" style=\"padding: 0px;\">12MP</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"22\" style=\"padding: 0px;\">Video</td><td class=\"xl70\" width=\"872\" style=\"padding: 0px;\">160p@24/30/60fps,&nbsp;1080p@30/60/120/240fps</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"22\" style=\"padding: 0px;\">Wifi</td><td class=\"xl69\" width=\"872\" style=\"padding: 0px;\">Wi-Fi 802.11 a/b/g/n/ac/ax, dual-band, hotspot</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">Bluetooth</td><td class=\"xl74\" style=\"padding: 0px;\">5.0</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">GPS</td><td class=\"xl66\" width=\"872\" style=\"padding: 0px;\">Yes</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"21\" style=\"padding: 0px;\">NFC</td><td class=\"xl64\" width=\"872\" style=\"padding: 0px;\">Yes</td></tr><tr><td class=\"xl64\" width=\"279\" height=\"24\" style=\"padding: 0px;\">USB</td><td class=\"xl68\" width=\"872\" style=\"padding: 0px;\">USB 2.0</td></tr></tbody></table>', NULL, '<br>', 1, 9, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 04:07:15', '2020-11-24 19:06:47', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Purple\",\"images\":[\"16056508351.jpg\",\"16056508353.jpg\",\"16056508354.jpg\"]},{\"key\":1,\"color\":\"Green\",\"images\":[\"16056508352.jpg\",\"16056508353.jpg\",\"16056508354.jpg\"]},{\"key\":2,\"color\":\"Red\",\"images\":[\"16056508351.jpg\",\"16056508352.jpg\",\"16056508353.jpg\"]},{\"key\":3,\"color\":\"Black\",\"images\":[\"16056508351.jpg\",\"16056508352.jpg\",\"16056508354.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(309, 'xZa0839cOG', 'normal', NULL, 0, 44, 73, NULL, NULL, 'Apple iPhone 11 Pro Max Unlocked', 'apple-iphone-11-pro-max-unlocked-xza0839cog', '16056515471.jpg', '16056515478OqBnrmL.jpg', NULL, '64,256,512', '100,100,100', '0,95,195', '#fbd7bd,#4e5752', '0,0', '0,0', 900, 0, '<ul class=\"a-unordered-list a-vertical\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">OFFER INCLUDES: An Apple iPhone and a wireless plan with unlimited data/talk/text</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">WIRELESS PLAN: Unlimited talk, text, and data with mobile hotspot, nationwide coverage, and international reach. No long-term contract required.</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">PROGRAM DETAILS: When you add this offer to cart, it will reflect 3 items: the iPhone, SIM kit, and carrier subscription</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">6.5-inch Super Retina XDR OLED display, water and dust resistant, with Face ID</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Triple-camera system with 12MP Ultra Wide camera, 12MP TrueDepth front camera with Portrait mode</span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fast-charge and wireless charging capable</span></li></ul>', NULL, '<br>', 1, 16, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 04:19:07', '2020-11-25 20:12:47', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Rose Gold\",\"images\":[\"16056515471.jpg\",\"16056515472.jpg\",\"16056515473.jpg\"]},{\"key\":1,\"color\":\"Midnight Green\",\"images\":[\"16056515472.jpg\",\"16056515473.jpg\",\"16056515474.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(310, '75j1685yfn', 'normal', NULL, 0, 44, 102, NULL, NULL, 'Apple iPhone XR, Unlocked', 'apple-iphone-xr-unlocked-75j1685yfn', '1605711939red.jpeg', '1605711939EYM3bH5S.jpg', NULL, '64,128', '100,100', '0,25', '#950310,#45a9db,#ffffff', '0,0,0', '0,0,0', 450, 0, '<ul class=\"a-unordered-list a-vertical\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85);\"><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">Get the iPhone XR for as low as $6.63/month - With 24-month financing through an eligible credit card, your monthly payment amount will be $16.63, and you will receive $10 in Amazon.com gift card credit every month you maintain the wireless plan subscription.</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">OFFER INCLUDES: An Apple iPhone and a wireless plan with unlimited data/talk/text</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">WIRELESS PLAN: Unlimited talk, text, and data with mobile hotspot, nationwide coverage, and international reach. No long-term contract required.</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">PROGRAM DETAILS: When you add this offer to cart, it will reflect 3 items: the iPhone, SIM kit, and carrier subscription</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">6.1-inch Liquid Retina display, water, and dust resistant, with Face ID</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\"><font size=\"2\">12MP dual cameras with Portrait mode, 7MP TrueDepth front camera</font></span></li><li class=\"qa-bullet-point-list-element\" style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\" style=\"\"><font size=\"2\">Wireless charging</font></span></li></ul>', NULL, '<br>', 1, 13, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:05:39', '2020-11-23 23:58:37', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Red\",\"images\":[\"16057121831.jpg\",\"16057121832.jpg\",\"16057121833.jpg\",\"16057121834.jpg\",\"16057121835.jpg\",\"16057121836.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057121831.jpg\",\"16057121832.jpg\",\"16057121833.jpg\",\"16057121834.jpg\",\"16057121835.jpg\",\"16057121836.jpg\"]},{\"key\":2,\"color\":\"White\",\"images\":[\"16057119391.jpg\",\"16057119392.jpg\",\"16057119393.jpg\",\"16057119394.jpg\",\"16057119395.jpg\",\"16057119396.jpg\"]}]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(311, 'IBk2316uUs', 'normal', NULL, 0, 44, 72, NULL, NULL, 'Apple iPhone 11 Pro', 'apple-iphone-11-pro-ibk2316uus', '16057125481.jpg', '1605712548p9c7Nx3I.jpg', NULL, '64', '100', '0', '#fcd8be,#545d58', '0,0', '0,0', 795, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">6.5-inch Super Retina XDR OLED display</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Triple-camera system with 12MP Ultra Wide, Wide, and Telephoto cameras; Night mode, Portrait mode, and 4K video up to 60fps</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12MP TrueDepth front camera with Portrait Mode, 4K video, and Slo-Motion</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Water and dust resistant (4 meters for up to 30 minutes, IP68)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">90 Day Warranty - comes with a generic charger. Does not include headphones, SIM-card or original packaging</span></li></ul>', NULL, '<br>', 1, 7, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:15:48', '2020-11-24 03:13:30', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Rose Gold\",\"images\":[\"16057125481.jpg\",\"16057125482.jpg\",\"16057125483.jpg\"]},{\"key\":1,\"color\":\"Midnight Green\",\"images\":[\"16057125482.jpg\",\"16057125483.jpg\",\"16057125484.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(312, 'bS6316774p', 'normal', NULL, 0, 45, NULL, NULL, NULL, 'Samsung Galaxy S7 Edge Gold Unlocked', 'samsung-galaxy-s7-edge-gold-unlocked-bs6316774p', '16057133335.jpg', '1605713333Wqt9a2Cd.jpg', NULL, NULL, NULL, NULL, '#c5b09d', '0', '0', 180, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.5-inch QHD AMOLED Capacitive Touchscreen, 2560 x 1440 pixel resolution (534 PPI pixel density) + Corning Gorilla Glass 4 (Back Panel) w/ Always-on Display, TouchWiz UI, Curved Edge Screen</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Android OS, v6.0 (Marshmallow), Chipset: Exynos 8890 Octa, Processor: Octa-Core (Quad-Core 2.3 GHz Mongoose &amp; Quad-Core 1.6 GHz Cortex-A53), GPU: Mali-T880 MP12</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">12 Megapixel Camera with Dual Pixel Autofocus, OIS, F/1.7, 26mm, phase detection autofocus, LED Flash, 1/2.5\" sensor size, 1.4 m pixel size, geo-tagging, simultaneous 4K video, and 9MP image recording, touch focus, face/smile detection, Auto HDR, panorama + 5 Megapixel Front Camera with F/1.7, Selfie Flash, 22mm Lens, Dual Video Call, Auto HDR</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Internal Memory: 32GB, 4GB RAM - microSD Up to 256GB. This cell phone is compatible with GSM carriers like ATT and also with GSM SIM need cards (e.g. H20, Straight Talk, and select prepaid carriers) need to contact the service provider to check fully compatibility of the phone. This cellphone will NOT work with CDMA Carriers like Verizon, T-mobile, Sprint, Boost mobile or Virgin mobile.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">WLAN: Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot; Bluetooth: v4.2, A2DP, LE, apt-X; GPS: with A-GPS, GLONASS, BDS NFC; USB: microUSB v2.0, USB Host</span></li></ul>', NULL, '<br>', 1, 4, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:28:53', '2020-11-25 20:46:05', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(313, 'H3S3369BwE', 'normal', NULL, 0, 45, 104, NULL, NULL, 'Samsung Galaxy S8  Unlocked', 'samsung-galaxy-s8-unlocked-h3s3369bwe', '16057137971.jpg', '1605713797r85bw7Ig.jpg', NULL, NULL, NULL, NULL, '#67657b,#749dcb,#000000', '0,0,0', '0,0,0', 225, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Screen: Super AMOLED capacitive touchscreen, 16M colors, Size: 5.8 inches, Resolution: 1440 x 2960 pixels, Protection: Corning Gorilla Glass 5. OS: Android 7.0 (Nougat), upgradable to Android 8.0 (Oreo), Chipset: Exynos 8895 Octa. Memory: 64 GB, 4 GB RAM. Camera: 12MP, Front: 8 MP. Sensors: Iris scanner, fingerprint (rear-mounted), accelerometer, gyro, proximity, compass, barometer, heart rate, SpO2.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Unlocked cell phones are compatible with GSM carrier such as AT&amp;T and T-Mobile, but are not compatible with CDMA carriers such as Verizon and Sprint.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Please check if your GSM cellular carrier supports the bands for this model before purchasing, LTE may not be available in all regions: 2G bands: GSM 850 / 900 / 1800 / 1900 - SIM 1 &amp; SIM 2, 3G bands: HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100, LTE bands: 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 12(700), 13(700), 17(700), 18(800), 19(800), 20(800), 25(1900), 26(850), 28(700), 32(1500), 66(1700/2100), 38(2600), 39(1900), 40(2300), 41(2500).</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">This device may not include a US warranty as some manufacturers do not honor warranties for international items. Please contact the seller for specific warranty information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The box contains: Your new device, USB cable, Earphones, Charger (may be foreign), Documentation.</span></li></ul>', NULL, '<br>', 1, 4, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:36:37', '2020-11-25 13:33:29', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Orchid Grey\",\"images\":[\"16057137972.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057137972.jpg\",\"16057137973.jpg\",\"16057137974.jpg\",\"16057137975.jpg\"]},{\"key\":2,\"color\":\"Black\",\"images\":[\"16057137972.jpg\",\"16057137973.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(314, 'gtN3823YdR', 'normal', NULL, 0, 45, 105, NULL, NULL, 'Samsung S8 Plus Brand New', 'samsung-s8-plus-brand-new-gtn3823ydr', '16057142171.jpg', '16057142178ivwgu5n.jpg', NULL, NULL, NULL, NULL, '#a3a4a8,#000000', '0,0', '0,0', 285, 0, '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">The Galaxy S8+ expansive display stretches from edge to edge, giving you the most amount of screen in the least amount of space. And the Galaxy S8+ is even more expansive—our biggest screen yet.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">The Galaxy S8 camera still takes amazing photos in low light, and now has an enhanced front-facing camera so you can take better clearer selfies.</p>', NULL, '<br>', 1, 6, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:43:37', '2020-11-25 16:47:01', 0, NULL, NULL, NULL, 0, 0, '{\"2\":{\"key\":2,\"color\":\"Silver\",\"images\":[\"16057142172.jpg\"]},\"3\":{\"key\":3,\"color\":\"Black\",\"images\":[\"16057142172.jpg\"]}}', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(315, 'cRD4259HyP', 'normal', NULL, 0, 45, 74, NULL, NULL, 'Samsung Galaxy S9 Unlocked', 'samsung-galaxy-s9-unlocked-crd4259hyp', '16057148521.jpg', '1605714852dbVvPipP.jpg', NULL, NULL, NULL, NULL, '#9b7396,#000000', '0,0', '0,0', 275, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Dual Nano-SIM (4G + 3G) ; 3G WCDMA B1(2100) / B2(1900) / B4(AWS) / B5(850) / B8(900) TD-SCDMA B34(2010) / B39(1880) ; 4G LTE B1 / B2 / B3 / B4 / B5 / B7 / B8 / B12 / B13 / B17 / B18 / B19 / B20 / B25 / B26 / B28 / B32 / B66 / B38 / B39 / B40 / B41 ; 4G LTE &amp; 3G work with AT&amp;T and T-Mobile ; DOES NOT work with Sprint, Verizon, U.S. Cellular and all other CDMA carriers ; LTE compatibility: This is international stock, varies per carrier (ensure to check with your carrier before purchase)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">10nm, 64-bit, Octa-Core (2.7 GHz Quad + 1.7 GHz Quad) CPU ; 64GB ROM, 4GB RAM ; Supports microSD, up to 400 GB (uses SIM 2 slot) ; 3000 mAh battery.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Main Camera: 12 MP (f/1.5-2.4, 1/2.55\", 1.4 µm, Dual Pixel PDAF), phase detection autofocus, OIS, LED flash; Front Camera: 8 MP (f/1.7, 1/3.6\", 1.22 µm), autofocus, Auto HDR.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.8 inches, Super AMOLED capacitive touchscreen, 16M colors.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Package Content: Samsung Galaxy S9 (SM-G960F/DS), USB Cable, Earphone, Ejection pin, USB power adapter, Quick start guide, USB connector (USB Type-C), Micro USB connector, Clear view cover. PLEASE NOTE: this is an international version of the phone that comes with no warranty in the US.</span></li></ul>', NULL, '<br>', 1, 3, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 21:54:12', '2020-11-21 08:32:38', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Purple\",\"images\":[\"16057148522.jpg\",\"16057148523.jpg\",\"16057148524.jpg\"]},{\"key\":1,\"color\":\"Black\",\"images\":[\"16057148522.jpg\",\"16057148523.jpg\",\"16057148524.jpg\",\"16057148525.jpg\",\"16057148526.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(316, 'p0658366FM', 'normal', NULL, 0, 45, 106, NULL, NULL, 'Samsung Galaxy S9 Plus Unlocked', 'samsung-galaxy-s9-plus-unlocked-p0658366fm', '16057162491.jpg', '1605716249YZuQ0DJp.jpg', NULL, NULL, NULL, NULL, '#000000,#947192', '0,0', '0,0', 300, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Dual Nano-SIM (4G + 3G) ; 3G WCDMA B1(2100) / B2(1900) / B4(AWS) / B5(850) / B8(900) TD-SCDMA B34(2010) / B39(1880) ; 4G LTE B1 / B2 / B3 / B4 / B5 / B7 / B8 / B12 / B13 / B17 / B18 / B19 / B20 / B25 / B26 / B28 / B32 / B66 / B38 / B39 / B40 / B41 ; 4G LTE &amp; 3G work with AT&amp;T and T-Mobile ; DOES NOT work with Sprint, Verizon, U.S. Cellular and all other CDMA carriers ; LTE compatibility: This is international stock, varies per carrier (ensure to check with your carrier before purchase)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">10nm, 64-bit, Octa-Core (2.7 GHz Quad + 1.7 GHz Quad) CPU ; 64GB ROM, 4GB RAM ; Supports microSD, up to 400 GB (uses SIM 2 slot) ; 3000 mAh battery.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Main Camera: 12 MP (f/1.5-2.4, 1/2.55\", 1.4 µm, Dual Pixel PDAF), phase detection autofocus, OIS, LED flash; Front Camera: 8 MP (f/1.7, 1/3.6\", 1.22 µm), autofocus, Auto HDR.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.8 inches, Super AMOLED capacitive touchscreen, 16M colors.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Package Content: Samsung Galaxy S9 (SM-G960F/DS), USB Cable, Earphone, Ejection pin, USB power adapter, Quick start guide, USB connector (USB Type-C), Micro USB connector, Clear view cover. PLEASE NOTE: this is an international version of the phone that comes with no warranty in the US.</span></li></ul>', NULL, '<br>', 1, 7, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 22:17:29', '2020-11-24 01:27:24', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057162492.jpg\",\"16057162493.jpg\",\"16057162494.jpg\",\"16057162495.jpg\",\"16057162496.jpg\"]},{\"key\":1,\"color\":\"Purple\",\"images\":[\"16057162492.jpg\",\"16057162493.jpg\",\"16057162494.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(317, 'c386443qwO', 'normal', NULL, 0, 45, 107, NULL, NULL, 'Samsung Galaxy S10e Unlocked', 'samsung-galaxy-s10e-unlocked-c386443qwo', '16057174801.jpg', '16057174811wnnQV7X.jpg', NULL, NULL, NULL, NULL, '#000000,#b7d3dc,#45607e', '0,0,0', '0,0,0', 360, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">For USA Buyers: This Smartphone is compatible/will work with any GSM Networks such as AT&amp;T, T-Mobile. For exact 2G GSM, 3G, 4G/LTE compatibility, please check with your network provider in advance prior to your purchase. This phone WILL NOT WORK with any CDMA Networks such as VERIZON, SPRINT, US CELLULAR.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Hybrid/Dual-SIM (Nano-SIM), Network Compatibility : SIM CARD 1 [ 2G GSM and/or 3G 850(B5) / 900(B8) / 1700|2100(B4) / 1900(B2) / 2100(B1) and/or 3G TD-SCDMA : 2000 / 1900 and/or 4G LTE : 700(B12) / 700(B13) / 700(B17) / 700(B28) / 800(B18) / 800(B19) / 800(B20) / 850(B5) / 850(B26) / 900(B8) /1500(B32) / 1700|2100(B4) / 1700|2100 (B66) / 1800(B3) / 1900(B2) / 1900(B25) / 2100(B1) / 2600(B7) and/or : TD-LTE : 1900(B39) / 2300(B40) / 2500(B41) / 2600(B38) / 3500(B42) ] and SIM CARD 2 [ 2G GSM ]</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">5.8 inches, Dynamic AMOLED capacitive touchscreen, 16M colors, 1080 x 2280 pixels, 19:9 ratio (~438 ppi density), Corning Gorilla Glass 5</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">128GB Storage, 6GB RAM, Up to 512GB microSD Card slot (uses SIM 2 slot)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Android 9.0 (Pie), Exynos 9820 Octa (8 nm), Octa-core (2x2.73 GHz Mongoose M4 &amp; 2x2.31 GHz Cortex-A75 &amp; 4x1.95 GHz Cortex-A55), DualCamera 12MP, f/1.5-2.4, 16 MP, f/2.2, LED flash, auto-HDR, panorama, 2160p@60fps, 1080p@240fps, 720p@960fps, HDR, dual-video rec, Front Camera : 10MP, f/1.9, 26mm (wide), Dual video call, Auto-HDR, 2160p@30fps, 1080p@30fps</span></li></ul>', NULL, '<br>', 1, 4, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 22:38:00', '2020-11-19 20:35:53', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057174802.jpg\",\"16057174803.jpg\",\"16057174804.jpg\",\"16057174805.jpg\",\"16057174806.jpg\",\"16057174807.jpg\"]},{\"key\":1,\"color\":\"white\",\"images\":[\"16057174802 (1).jpg\",\"16057174803 (1).jpg\",\"16057174804 (1).jpg\",\"16057174805 (1).jpg\",\"16057174806 (1).jpg\",\"16057174807 (1).jpg\"]},{\"key\":2,\"color\":\"Blue\",\"images\":[\"16057174802.jpg\",\"16057174803.jpg\",\"16057174804.jpg\",\"16057174805.jpg\",\"16057174806.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(318, 'oTd7485L3j', 'normal', NULL, 0, 45, 108, NULL, NULL, 'Samsung Galaxy S10 Unlocked', 'samsung-galaxy-s10-unlocked-otd7485l3j', '16057215111.jpg', '1605721511nzRT3jrl.jpg', NULL, NULL, NULL, NULL, '#000000,#33597d', '0,0', '0,0', 450, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Fully Unlocked: Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.), both domestically and internationally.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arm\'s length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 2, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 23:45:11', '2020-11-21 02:23:30', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057215112.jpg\",\"16057215113.jpg\",\"16057215114.jpg\",\"16057215115.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057215112.jpg\",\"16057215113.jpg\",\"16057215114.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(319, 'RDX1534Df1', 'normal', NULL, 0, 45, 109, NULL, NULL, 'Samsung Galaxy S10+  Unlocked', 'samsung-galaxy-s10-unlocked-rdx1534df1', '16057217521.jpg', '1605721752ou6cXsmc.jpg', NULL, NULL, NULL, NULL, '#000000,#33597d', '0,0', '0,0', 495, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The octa-core processor and plenty of RAM deliver outstanding overall performance for opening and running applications, flipping through menus, and more</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Adapts to you and the way you use your phone, learning your preferences as you go. Your experience gets better over time, and it keeps things running smoother and longer</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Provides fast Web connection for downloading apps, streaming content, and staying connected on social media</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Its nearly frameless cinematic Infinity Display offers more detail and clarity, more immersive and uninterrupted content, in a slim, balanced form</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Sees what you see. The pro-grade camera effortlessly captures epic, pro-quality images of the world as you see it.</span></li></ul>', NULL, '<br>', 1, 5, NULL, NULL, NULL, 5, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-18 23:49:12', '2020-11-21 06:38:33', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057217522.jpg\",\"16057217523.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057217521.jpg\",\"16057217522.jpg\",\"16057217523.jpg\",\"16057217524.jpg\",\"16057217525.jpg\",\"16057217526.jpg\"]}]', NULL, '19', '', 'Select Accessory', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(320, '4EN74823uw', 'normal', NULL, 0, 45, 110, NULL, NULL, 'Samsung Galaxy S20 Unlocked', 'samsung-galaxy-s20-unlocked-4en74823uw', '16057281251.jpg', '1605728125tnH00KfA.jpg', NULL, NULL, NULL, NULL, '#e9bdca', '0', '0', 650, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); margin: 0px; padding: 0px; border: 0px; font-size: 1rem; vertical-align: baseline; float: left; text-align: justify; width: 511.984px; display: inline-block; font-family: SamsungOneLatinWeb400, arial; line-height: 1.75;\">Getting work done on your smartphone should be easy. The Galaxy S20 integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); margin: 0px; padding: 0px 0px 0px 30px; border: 0px; font-size: 1rem; vertical-align: baseline; float: right; width: 511.984px; text-align: justify; display: inline-block; font-family: SamsungOneLatinWeb400, arial; line-height: 1.75;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; vertical-align: baseline;\"><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20’s productivity experience<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">1</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">2</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">5</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 7.5px; line-height: 0; vertical-align: baseline; top: -0.5em; margin: 0px; padding: 0px; border: 0px;\">3</span></li></ul></div>', NULL, '<br>', 1, 3, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:35:25', '2020-11-21 07:54:28', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Pink\",\"images\":[\"16057281252.jpg\",\"16057281253.jpg\",\"16057281254.jpg\",\"16057281255.jpg\",\"16057281256.jpg\",\"1605728125no 1.webp\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(321, 'NlL8173PC0', 'normal', NULL, 0, 45, 111, NULL, NULL, 'Samsung Galaxy S20 Plus 5G Unlocked', 'samsung-galaxy-s20-plus-5g-unlocked-nll8173pc0', '16057285811.jpg', '1605728581qP4ZxbRy.jpg', NULL, NULL, NULL, NULL, '#68696b,#c3e3fa,#000000', '0,0,0', '0,0,0', 725, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">Getting work done on your smartphone should be easy. The Galaxy S20+ integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20+’s productivity experience<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></li><li style=\"margin: 0px; padding: 0px;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">5</span></li><li style=\"margin: 0px; padding: 0px;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li></ul></div>', NULL, '<br>', 1, 5, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:43:01', '2020-11-25 06:20:09', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Space Grey\",\"images\":[\"16057285812.jpg\",\"16057285813.jpg\",\"16057285814.jpg\",\"16057285815.jpg\",\"16057285816.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057285812.jpg\",\"16057285813.jpg\",\"16057285814.jpg\",\"16057285815.jpg\",\"16057285816.jpg\"]},{\"key\":2,\"color\":\"Black\",\"images\":[\"16057285812.jpg\",\"16057285813.jpg\",\"16057285814.jpg\",\"16057285815.jpg\",\"16057285816.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0);
INSERT INTO `products` (`id`, `sku`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `color_qty`, `color_price`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `color_gallery`, `vendor_prices`, `pro_type_id`, `pro_type_child`, `acc_brand_id`, `acc_type_id`, `carrier_id`, `cases`, `headphones`, `chargers`, `cables`) VALUES
(322, 'DK58604JJI', 'normal', NULL, 0, 45, 112, NULL, NULL, 'Samsung Galaxy Note20 Ultra Unlocked', 'samsung-galaxy-note20-ultra-unlocked-dk58604jji', '16057288421.jpg', '1605728843MR3QjVcD.jpg', NULL, NULL, NULL, NULL, '#000000,#f0efed', '0,0', '0,0', 800, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">The Galaxy Note20 Ultra 5G is the phone for a business that works seamlessly with popular business tools, and lets you work however is best for you. A powerful battery with Super Fast Charging saves time and lasts all day.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;The S Pen is the most natural way to capture your thoughts and now gives new ways to present your ideas. And you can keep your business data and personal data protected thanks to a Secure Processor and the Samsung Knox security platform.</div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Responsive S Pen that feels like you\'re using a real pen and lets you save notes across your Galaxy devices while on the go</li><li style=\"margin: 0px; padding: 0px;\">Seamless integration with Microsoft makes your work accessible across devices, from your PC to your phone<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Transform working from home by turning your TV into your monitor wirelessly with Samsung DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li><li style=\"margin: 0px; padding: 0px;\">5G support to take advantage of the latest advancements in mobile networks<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></li></ul></div>', NULL, '<br>', 1, 5, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:47:22', '2020-11-24 22:13:32', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057288422.jpg\",\"16057288423.jpg\",\"16057288424.jpg\",\"16057288425.jpg\",\"16057288426.jpg\"]},{\"key\":1,\"color\":\"white\",\"images\":[\"16057288422.jpg\",\"16057288423.jpg\",\"16057288424.jpg\",\"16057288425.jpg\",\"16057288426.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(323, 'D3W8845pBE', 'normal', NULL, 0, 45, 113, NULL, NULL, 'Samsung Note 5', 'samsung-note-5-d3w8845pbe', '16057292541.jpg', '1605729254rJK4JsVG.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 179.99, 0, '<span style=\"color: rgb(15, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\">Service Provider:<span style=\"font-weight: 700;\">Verizon</span>&nbsp;&nbsp;|&nbsp; Color:<span style=\"font-weight: 700;\">Black</span>&nbsp;&nbsp;|&nbsp; Size:<span style=\"font-weight: 700;\">32 GB</span></span><span style=\"color: rgb(15, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"></span><div class=\"a-row a-spacing-top-base\" style=\"width: 1464px; color: rgb(15, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; margin-top: 12px !important;\"><div class=\"a-column a-span6\" style=\"margin-right: 29.2656px; float: left; min-height: 1px; overflow: visible; width: 716.594px;\"><div class=\"a-row a-spacing-base\" style=\"width: 716.594px; margin-bottom: 12px !important;\"><div class=\"a-section table-padding\" style=\"margin-bottom: 0px; margin-left: 12px;\"><table id=\"productDetails_detailBullets_sections1\" class=\"a-keyvalue prodDetTable\" role=\"presentation\" style=\"margin-bottom: 22px; width: 704px; border-bottom: 1px solid rgb(231, 231, 231); table-layout: fixed; border-spacing: 0px; padding: 0px;\"><tbody><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Product Dimensions</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">2.99 x 0.29 x 6.03 inches</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Item Weight</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">6 ounces</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">ASIN</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">B013XAPPIK</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Item model number</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Galaxy Note 5</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Batteries</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">1 Lithium Polymer batteries required. (included)</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\"><br></th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\"></td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Best Sellers Rank</th><td style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial;\">#44,271 in Cell Phones &amp; Accessories (<a href=\"https://www.amazon.com/gp/bestsellers/wireless/ref=pd_zg_ts_wireless\" style=\"color: rgb(0, 113, 133);\">See Top 100 in Cell Phones &amp; Accessories</a>)<br>#584 in&nbsp;<a href=\"https://www.amazon.com/gp/bestsellers/wireless/2407749011/ref=pd_zg_hrsr_wireless\" style=\"color: rgb(0, 113, 133);\">Unlocked Cell Phones</a><br></td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Is Discontinued By Manufacturer</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">No</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">OS</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Android</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">RAM</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">32 GB</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Additional Features</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Touchscreen, Built-in-GPS, Bluetooth-enabled, Dual-camera, LTE, Smartphone</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Display resolution</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">2560 x 1440 pixels</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Other display features</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Wireless Phone</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Device interface - primary</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Touchscreen with Stylus Support</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Other camera features</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">16 MP</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Form Factor</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Bar</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Colour</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Black</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Phone Standy Time (with data)</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">336 hours</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Included Components</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Pre-Installed SIM Card, Wall Charger, Samsung Note 5, USB Cable</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Manufacturer</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">Samsung</td></tr><tr><th class=\"a-color-secondary a-size-base prodDetSectionEntry\" style=\"vertical-align: top; text-align: left; padding: 7px 14px 6px; color: rgb(17, 17, 17); background-color: rgb(243, 243, 243); font-weight: 400; border-top: 1px solid rgb(231, 231, 231); overflow-wrap: break-word; width: 352px; line-height: 20px !important;\">Date First Available</th><td class=\"a-size-base\" style=\"vertical-align: top; padding: 7px 14px 6px; border-top: 1px solid rgb(231, 231, 231); color: rgb(51, 51, 51); font-family: Arial; line-height: 20px !important;\">August 21, 2015</td></tr></tbody></table></div></div></div></div>', NULL, '<br>', 1, 5, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 01:54:14', '2020-11-24 23:25:32', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057292542.jpg\",\"16057292543.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(324, 'tch9393Boc', 'normal', NULL, 0, 45, 114, NULL, NULL, 'Samsung Galaxy Note 8 Unlocked', 'samsung-galaxy-note-8-unlocked-tch9393boc', '16057299091.jpg', '1605729909BQ5JKyXw.jpg', NULL, NULL, NULL, NULL, '#000000,#797081', '0,0', '0,0', 290, 0, '<p class=\"product-details__info-description--line product-details__info-description--line1\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">Features a smarter S Pen, our largest Infinity Display and our best camera yet.</p><p class=\"product-details__info-description--line product-details__info-description--line2\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; line-height: 26px; letter-spacing: 0.2px; color: rgb(98, 96, 96);\">With Galaxy Note8, you can create, share, and express your ideas more than ever before.</p>', NULL, '<br>', 1, 5, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:05:09', '2020-11-24 20:55:12', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057299092.jpg\"]},{\"key\":1,\"color\":\"Purple\",\"images\":[\"16057299092.jpg\",\"16057299093.jpg\",\"16057299094.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(325, '4Vr0120ueD', 'normal', NULL, 0, 45, 115, NULL, NULL, 'Samsung Galaxy Note 9  Unlocked', 'samsung-galaxy-note-9-unlocked-4vr0120ued', '16057313341.jpg', '1605731334DZxSvb8v.jpg', NULL, NULL, NULL, NULL, '#000000,#1f2c4d,#825b78', '0,0,0', '0,0,0', 390, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(85, 85, 85); font-size: 16px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The largest battery in a note, ever When you have a long-lasting battery, you really can go all day and all night</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The Note9 has twice as much storage as the Note8, which means more music, more videos, more pictures, and less worry when it comes to space on your phone</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">The Note9 gives you a quick network connection for incredibly fast streaming and downloading, so you can do more, uninterrupted. One Nano SIM and one MicroSD slot (up to 512GB)</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">Still amazing on screen, but now the S pen has more power off Screen Remotely control different applications and use the S pen to capture shots from far away, scroll, and play music</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px;\"><span class=\"a-list-item\">At 64 Inches, the Note9 has the largest screen of any Galaxy phone Perfect for gaming and streaming, our Super AMOLED display is bigger than ever before</span></li></ul>', NULL, '<br>', 1, 4, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:28:54', '2020-11-23 22:47:31', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057313342.jpg\",\"16057313343.jpg\",\"16057313344.jpg\",\"16057313345.jpg\",\"16057313346.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057313342.jpg\",\"16057313343.jpg\",\"16057313344.jpg\",\"16057313345.jpg\",\"16057313346.jpg\"]},{\"key\":2,\"color\":\"purple\",\"images\":[\"16057313342.jpg\",\"16057313343.jpg\",\"16057313344.jpg\",\"16057313345.jpg\",\"16057313346.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(326, 'Eh313435sM', 'normal', NULL, 0, 45, 77, NULL, NULL, 'Samsung Galaxy Note10 Unlocked', 'samsung-galaxy-note10-unlocked-eh313435sm', '16057314581.jpg', '1605731458HrPa0pXc.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 525, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px; color: rgb(17, 17, 17); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Fully Unlocked: Fully unlocked and compatible with any carrier of choice (e.g. AT&amp;T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket, Metro, etc.), both domestically and internationally.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">The device does not come with headphones or a SIM card. It does include a charger and charging cable that may be generic.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Inspected and guaranteed to have minimal cosmetic damage, which is not noticeable when the device is held at arm\'s length.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Successfully passed a full diagnostic test which ensures like-new functionality and removal of any prior-user personal information.</span></li><li style=\"margin: 0px; padding: 0px; line-height: 26px; list-style: disc; overflow-wrap: break-word;\"><span class=\"a-list-item\">Tested for battery health and guaranteed to have a minimum battery capacity of 80%.</span></li></ul>', NULL, '<br>', 1, 8, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:30:58', '2020-11-25 20:12:18', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057314582.jpg\",\"16057314583.jpg\",\"16057314584.jpg\",\"16057314585.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(327, 'NHU1461NI5', 'normal', NULL, 0, 45, 116, NULL, NULL, 'Samsung Galaxy Note10+ Unlocked', 'samsung-galaxy-note10-unlocked-nhu1461ni5', '16057315721.jpg', '1605731572yYcO1TOf.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 650, 0, '<br>', NULL, '<br>', 1, 4, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:32:52', '2020-11-23 04:10:21', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057315722.jpg\",\"16057315723.jpg\",\"16057315724.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(328, 'clF2061OAg', 'normal', NULL, 0, 45, 110, NULL, NULL, 'Samsung Galaxy S20 Unlocked', 'samsung-galaxy-s20-unlocked-clf2061oag', '16057322021.jpg', '1605732202SVXfYP5Y.jpg', NULL, NULL, NULL, NULL, '#e9bdca', '0', '0', 900, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">Getting work done on your smartphone should be easy. The Galaxy S20 integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20’s productivity experience<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></li><li style=\"margin: 0px; padding: 0px;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">5</span></li><li style=\"margin: 0px; padding: 0px;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li></ul></div>', NULL, '<br>', 1, 6, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:43:22', '2020-11-24 09:17:22', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Pink\",\"images\":[\"16057322022.jpg\",\"16057322023.jpg\",\"16057322024.jpg\",\"16057322025.jpg\",\"16057322026.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(329, 'wkE2204yBE', 'normal', NULL, 0, 45, 112, NULL, NULL, 'Samsung Galaxy S20 Ultra Unlocked', 'samsung-galaxy-s20-ultra-unlocked-wke2204ybe', '16057325281.jpg', '1605732528KB5iLVsm.jpg', NULL, NULL, NULL, NULL, '#000000,#c3e3fa,#68696b', '0,0,0', '0,0,0', 1050, 0, '<div class=\"p1 bottom-text\" style=\"color: rgb(85, 85, 85); font-size: 16px;\">Getting work done on your smartphone should be easy. The Galaxy S20+ integrates seamlessly with Microsoft Office so you can get the job done, wherever you are.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span>&nbsp;Experience hyperfast 5G connections to unlock revolutionary new ways of doing business.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span>&nbsp;Secure your data with Knox. Do more with one super-powerful device with DeX.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span>&nbsp;And you can power through any day with the all-day battery.<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">4</span></div><div class=\"p1 bottom-bullets\" style=\"color: rgb(85, 85, 85); font-size: 16px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Microsoft OneDrive and Office apps natively integrated into the Galaxy S20+’s productivity experience<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">1</span></li><li style=\"margin: 0px; padding: 0px;\">5G-capable so you can stream with virtually no lag and share and download large files in near real time<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span></li><li style=\"margin: 0px; padding: 0px;\">Protected by the Knox defense-grade security platform that’s trusted by governments around the world<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">5</span></li><li style=\"margin: 0px; padding: 0px;\">Connect to a monitor, keyboard, and mouse to power a complete desktop experience from your phone with DeX<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">3</span></li></ul></div>', NULL, '<br>', 1, 11, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:48:48', '2020-11-25 16:37:38', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057325282.jpg\",\"16057325283.jpg\",\"16057325284.jpg\",\"16057325285.jpg\",\"16057325286.jpg\"]},{\"key\":1,\"color\":\"Blue\",\"images\":[\"16057325282.jpg\",\"16057325283.jpg\",\"16057325284.jpg\",\"16057325285.jpg\",\"16057325286.jpg\"]},{\"key\":2,\"color\":\"Gray\",\"images\":[\"16057325282.jpg\",\"16057325283.jpg\",\"16057325284.jpg\",\"16057325285.jpg\",\"16057325286.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(330, 'Bgh2538Xla', 'normal', NULL, 0, 45, 117, NULL, NULL, 'Samsung Galaxy A01', 'samsung-galaxy-a01-bgh2538xla', '16057327561.jpg', '1605732756SNWt95z8.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 110, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Unlocked cell phones are compatible with GSM carriers such as AT&amp;T and T-Mobile, but are not compatible with CDMA carriers such as Verizon and Sprint.</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Please check if your GSM cellular carrier supports the bands for this model before purchasing, LTE may not be available in all areas:</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">This device may not include a US warranty as some manufacturers do not honor warranties for international items.</span></li></ul>', NULL, '<br>', 1, 5, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:52:36', '2020-11-23 04:10:11', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057327562.jpg\",\"16057327563.jpg\",\"16057327564.jpg\",\"16057327565.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(331, '4hV2836NbR', 'normal', NULL, 0, 45, 118, NULL, NULL, 'Samsung Galaxy A10e', 'samsung-galaxy-a10e-4hv2836nbr', '16057330531.jpg', '1605733053X202zho3.jpg', NULL, NULL, NULL, NULL, '#000000', '0', '0', 110, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">With a long-lasting battery, the Galaxy A10e gives you more time to post, talk, text, and share with friends and family</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Keep more with 32GB of built-in memory; Expand your memory up to 512GB with a Micro SD card</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Enjoy an edge to edge view on a 5. 83\" Infinity display</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Capture crisp, clear photos with an 8MP rear camera and a 5MP front Camera</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Wireless voice, data, and messaging services compatible with most major U.S. GSM and CDMA networks; Support for certain features and services such as VoWiFi and hotspot, vary by the wireless service provider</span></li></ul>', NULL, '<br>', 1, 6, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 02:57:33', '2020-11-23 04:10:15', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Black\",\"images\":[\"16057330532.jpg\",\"16057330533.jpg\",\"16057330534.jpg\",\"16057330535.jpg\",\"16057330536.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(332, '5Zg3056nnK', 'normal', NULL, 0, 45, 119, NULL, NULL, 'Samsung Galaxy A10s Unlocked', 'samsung-galaxy-a10s-unlocked-5zg3056nnk', '16057340101.jpg', '1605734010HdMbOTQj.jpg', NULL, NULL, NULL, NULL, '#192849', '0', '0', 130, 0, '<span style=\"color: rgb(98, 96, 96); letter-spacing: 0.2px;\">6.2 inches of HD+ TFT screen for a phone you\'ll love to watch. Whether you\'re into sitcoms or MMORPGs, Galaxy A10s\'s Infinity-V Display changes the way you experience them by putting you right in the action. See how far the experience takes you on the v-cut screen.</span><br>', NULL, '<br>', 1, 6, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:13:30', '2020-11-25 14:52:46', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Blue\",\"images\":[\"16057340102.jpg\",\"16057340103.jpg\",\"16057340104.jpg\",\"16057340105.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(333, '1mD42031Y3', 'normal', NULL, 0, 45, 120, NULL, NULL, 'Samsung Galaxy A11', 'samsung-galaxy-a11-1md42031y3', '16057343051.jpg', '1605734305jYwXzPTc.jpg', NULL, NULL, NULL, NULL, '#68696b', '0', '0', 135, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">6.4\" HD+ (720x1560) TFT LCD Infinity O, Dual SIM , 4000 mAh battery, Fingerprint (rear-mounted)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Internal Storage - 32GB, RAM - 2GB, Snapdragon 450, Octa-core 1.8 GHz, Android 10</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Triple Rear Camera: 13 MP, f/1.8, 28mm (wide), AF, 5 MP, f/2.2 (ultrawide), 2 MP, f/2.4, (depth), Front Camera: 8 MP, f/2.0</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">2G Bands: 850, 900, 1800, 1900MHz, 3G Bands: 850, 900, 1700, 1900, 4G LTE Bands: 1, 2, 3, 4, 5, 7, 8, 12, 17, 20, 28, 38, 40, 41</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">International Model Compatible with Most GSM Carriers like T-Mobile, AT&amp;T, MetroPCS, etc. Will NOT work with CDMA Carriers Such as Verizon, Sprint, Boost</span></li></ul>', NULL, '<br>', 1, 7, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:18:25', '2020-11-23 04:10:06', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Gray\",\"images\":[\"16057343052.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(334, '3Li47638yC', 'normal', NULL, 0, 45, 121, NULL, NULL, 'Samsung Galaxy A20S', 'samsung-galaxy-a20s-3li47638yc', '16057349061.jpg', '1605734906AA2ea0wQ.jpg', NULL, NULL, NULL, NULL, '#131b42', '0', '0', 160, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">International Model - No Warranty in the US. Compatible with Most GSM Carriers like T-Mobile, AT&amp;T, MetroPCS, etc. Will NOT work with CDMA Carriers Such as Verizon, Sprint, Boost</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">RAM 3GB , ROM 32GB Internal Memory ; MicroSD (Up to 512GB), Android 9.0 (Pie), Qualcomm SDM450 Snapdragon 450 (14 nm), Octa-core 1.8 GHz Cortex-A53, Adreno 506</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Main Rear Camera: 13.0 MP + 8.0 MP + 5.0 MP , F1.8 , F2.2 , F2.2 ; Front Camera: 8.0 MP , F2.0, Non-removable Li-Po 4000 mAh battery</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">3G HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100, 4G LTE band 1(2100), 2(1900), 3(1800), 5(850), 7(2600), 8(900), 20(800), 28(700), 34(2000), 38(2600), 39(1900), 40(2300), 41(2500)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">6.5 inches, 103.7 cm2 (~81.9% screen-to-body ratio) 720 x 1560 pixels (HD+), 19.5:9 ratio (~264 ppi density) IPS LCD capacitive touchscreen, 16M colors</span></li></ul>', NULL, '<br>', 1, 8, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:28:26', '2020-11-23 09:04:45', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Blue\",\"images\":[\"16057349062.jpg\",\"16057349063.jpg\",\"16057349064.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(335, 'HQR4971TYH', 'normal', NULL, 0, 45, 122, NULL, NULL, 'Samsung Galaxy A21S', 'samsung-galaxy-a21s-hqr4971tyh', '16057350612.jpg', '1605735061s3J3HkfN.jpg', NULL, NULL, NULL, NULL, '#131b42', '0', '0', 190, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Display: 6.5 inches PLS TFT capacitive touchscreen, Resolution: 720 x 1600 pixels</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Memory: 64GB 4GB RAM, MicroSD up to 512GB - Dual SIM (Nano-SIM, dual stand-by)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Main Camera: 48 MP + 8 MP + 2 MP + 2 MP w/ LED flash, panorama, HDR - Selfie Camera: 13 MP</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Android -- Exynos 850 -- Mali-G52 -- Octa-core (4x2.0 GHz Cortex-A55 &amp; 4x2.0 GHz Cortex-A55)</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Non-removable Li-Po 5000 mAh battery, Fast charging 15W</span></li></ul>', NULL, '<br>', 1, 7, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:31:01', '2020-11-25 01:44:40', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"white\",\"images\":[\"16057350611.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(336, 'OQB51840aC', 'normal', NULL, 0, 45, 123, NULL, NULL, 'Samsung Galaxy A31', 'samsung-galaxy-a31-oqb51840ac', '16057352751.jpg', '1605735275yZbT4Mbl.jpg', NULL, NULL, NULL, NULL, '#f0efed', '0', '0', 234, 0, '<ul class=\"a-unordered-list a-vertical a-spacing-mini\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(17, 17, 17); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Display: 6.4 inches Super AMOLED capacitive touchscreen, Resolution: 1080 x 2400 pixels</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Memory: 128GB, 4G RAM - MicroSD up to 512GB</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Android OS -- Mediatek MT6768 Helio P65 -- Octa-core (2x2.0 GHz &amp; 6x1.7 GHz) -- ARM Mali-G52</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Non-removable Li-Po 5000 mAh battery, Fast charging 15W</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\">Dimensions: 6.27 x 2.88 x 0.34 in, Weight: 6.53 oz</span></li></ul>', NULL, '<br>', 1, 9, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:34:35', '2020-11-23 04:10:32', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"white\",\"images\":[\"16057352752.jpg\",\"16057352753.jpg\",\"16057352754.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(337, 'eGx5311sew', 'normal', NULL, 0, 45, 124, NULL, NULL, 'Samsung Galaxy A51', 'samsung-galaxy-a51-egx5311sew', '16057354751.jpg', '1605735475TaFDocak.jpg', NULL, NULL, NULL, NULL, '#ffffff', '0', '0', 265, 0, '<br>', NULL, '<br>', 1, 21, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-19 03:37:55', '2020-11-25 19:54:22', 0, NULL, NULL, NULL, 0, 0, '[{\"key\":0,\"color\":\"Prism Crush Black\",\"images\":[\"16057354752.jpg\",\"16057354753.jpg\",\"16057354754.jpg\",\"16057354755.jpg\"]}]', NULL, '19', '', '', '', '1,3,4,5,6,7', 0, 0, 0, 0),
(339, 'iop4418Pwq', 'normal', NULL, 0, 49, NULL, NULL, NULL, 'Nokia X', 'nokia-x-iop4418pwq', '1606314615image.jpeg', '16063146150S93LdCp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 450, 490, '<br>', NULL, '<br>', 1, 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-25 20:30:15', '2020-11-25 20:30:15', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', '', '', '3', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_clicks`
--

CREATE TABLE `product_clicks` (
  `id` int(191) NOT NULL,
  `product_id` int(191) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_clicks`
--

INSERT INTO `product_clicks` (`id`, `product_id`, `date`) VALUES
(20, 95, '2020-05-21'),
(21, 95, '2020-05-21'),
(22, 95, '2020-05-21'),
(23, 95, '2020-05-21'),
(24, 95, '2020-05-21'),
(25, 95, '2020-05-21'),
(26, 95, '2020-05-21'),
(27, 95, '2020-05-21'),
(28, 95, '2020-05-21'),
(29, 122, '2020-05-21'),
(320, 95, '2020-07-01'),
(340, 95, '2020-07-21'),
(341, 122, '2020-07-21'),
(484, 95, '2020-08-25'),
(485, 122, '2020-08-25'),
(491, 95, '2020-08-25'),
(492, 122, '2020-08-25'),
(567, 95, '2020-08-27'),
(576, 122, '2020-08-27'),
(728, 95, '2020-09-05'),
(767, 95, '2020-09-07'),
(798, 95, '2020-09-07'),
(799, 122, '2020-09-07'),
(817, 95, '2020-09-08'),
(819, 122, '2020-09-08'),
(906, 122, '2020-09-10'),
(949, 122, '2020-09-12'),
(966, 95, '2020-09-13'),
(982, 95, '2020-09-13'),
(986, 122, '2020-09-13'),
(1030, 122, '2020-09-13'),
(1127, 95, '2020-09-17'),
(1128, 122, '2020-09-17'),
(1129, 122, '2020-09-17'),
(1130, 95, '2020-09-17'),
(1133, 95, '2020-09-17'),
(1134, 122, '2020-09-17'),
(1136, 95, '2020-09-17'),
(1137, 122, '2020-09-17'),
(1139, 122, '2020-09-17'),
(1140, 95, '2020-09-17'),
(1187, 95, '2020-09-17'),
(1188, 122, '2020-09-17'),
(1192, 95, '2020-09-17'),
(1193, 95, '2020-09-17'),
(1194, 122, '2020-09-17'),
(1195, 122, '2020-09-17'),
(1197, 95, '2020-09-17'),
(1198, 122, '2020-09-17'),
(1199, 95, '2020-09-17'),
(1200, 122, '2020-09-17'),
(1205, 95, '2020-09-18'),
(1206, 122, '2020-09-18'),
(1355, 95, '2020-09-23'),
(1576, 122, '2020-09-30'),
(1577, 95, '2020-09-30'),
(1592, 122, '2020-09-30'),
(1639, 122, '2020-09-30'),
(1640, 95, '2020-09-30'),
(1647, 122, '2020-10-01'),
(1750, 122, '2020-10-02'),
(1751, 122, '2020-10-02'),
(1752, 95, '2020-10-02'),
(1753, 95, '2020-10-02'),
(1800, 95, '2020-10-03'),
(1889, 122, '2020-10-05'),
(1962, 122, '2020-10-07'),
(2018, 95, '2020-10-09'),
(2051, 95, '2020-10-10'),
(2093, 95, '2020-10-12'),
(2125, 95, '2020-10-13'),
(2147, 122, '2020-10-14'),
(2183, 95, '2020-10-15'),
(2185, 122, '2020-10-15'),
(2199, 122, '2020-10-16'),
(2229, 95, '2020-10-17'),
(2290, 95, '2020-10-19'),
(2295, 122, '2020-10-19'),
(2298, 95, '2020-10-19'),
(2300, 95, '2020-10-19'),
(2329, 122, '2020-10-20'),
(2447, 122, '2020-10-22'),
(2501, 122, '2020-10-24'),
(2577, 95, '2020-10-25'),
(2581, 122, '2020-10-25'),
(2723, 122, '2020-10-29'),
(2724, 95, '2020-10-29'),
(2756, 122, '2020-11-01'),
(2776, 95, '2020-11-01'),
(2892, 95, '2020-11-08'),
(2896, 122, '2020-11-08'),
(3007, 95, '2020-11-13'),
(3012, 95, '2020-11-13'),
(3024, 122, '2020-11-13'),
(3026, 122, '2020-11-13'),
(3032, 122, '2020-11-14'),
(3042, 95, '2020-11-14'),
(3043, 122, '2020-11-14'),
(3090, 95, '2020-11-17'),
(3105, 300, '2020-11-17'),
(3106, 300, '2020-11-17'),
(3107, 300, '2020-11-17'),
(3108, 300, '2020-11-17'),
(3109, 301, '2020-11-17'),
(3110, 301, '2020-11-17'),
(3111, 302, '2020-11-17'),
(3112, 303, '2020-11-17'),
(3113, 302, '2020-11-17'),
(3114, 303, '2020-11-17'),
(3115, 301, '2020-11-17'),
(3116, 304, '2020-11-17'),
(3117, 305, '2020-11-17'),
(3118, 303, '2020-11-18'),
(3119, 310, '2020-11-18'),
(3120, 310, '2020-11-18'),
(3121, 310, '2020-11-18'),
(3122, 310, '2020-11-18'),
(3123, 310, '2020-11-18'),
(3124, 301, '2020-11-18'),
(3125, 302, '2020-11-18'),
(3126, 304, '2020-11-18'),
(3127, 303, '2020-11-18'),
(3128, 308, '2020-11-18'),
(3129, 319, '2020-11-18'),
(3130, 309, '2020-11-18'),
(3131, 308, '2020-11-18'),
(3132, 306, '2020-11-18'),
(3133, 300, '2020-11-18'),
(3134, 301, '2020-11-18'),
(3135, 302, '2020-11-18'),
(3136, 316, '2020-11-18'),
(3137, 304, '2020-11-18'),
(3138, 303, '2020-11-18'),
(3139, 321, '2020-11-18'),
(3140, 305, '2020-11-18'),
(3141, 310, '2020-11-18'),
(3142, 331, '2020-11-18'),
(3143, 332, '2020-11-18'),
(3144, 324, '2020-11-18'),
(3145, 321, '2020-11-18'),
(3146, 330, '2020-11-18'),
(3147, 320, '2020-11-18'),
(3148, 328, '2020-11-18'),
(3149, 325, '2020-11-18'),
(3150, 336, '2020-11-18'),
(3151, 337, '2020-11-18'),
(3152, 319, '2020-11-18'),
(3153, 334, '2020-11-18'),
(3154, 323, '2020-11-18'),
(3155, 333, '2020-11-19'),
(3156, 329, '2020-11-19'),
(3157, 322, '2020-11-19'),
(3158, 335, '2020-11-19'),
(3159, 326, '2020-11-19'),
(3160, 304, '2020-11-19'),
(3161, 314, '2020-11-19'),
(3162, 316, '2020-11-19'),
(3163, 317, '2020-11-19'),
(3164, 311, '2020-11-19'),
(3165, 300, '2020-11-19'),
(3166, 309, '2020-11-19'),
(3167, 311, '2020-11-19'),
(3168, 306, '2020-11-19'),
(3169, 308, '2020-11-19'),
(3170, 307, '2020-11-19'),
(3171, 306, '2020-11-19'),
(3172, 303, '2020-11-19'),
(3173, 310, '2020-11-19'),
(3174, 317, '2020-11-19'),
(3175, 313, '2020-11-19'),
(3176, 316, '2020-11-19'),
(3177, 314, '2020-11-19'),
(3178, 333, '2020-11-19'),
(3179, 334, '2020-11-19'),
(3180, 335, '2020-11-19'),
(3181, 336, '2020-11-19'),
(3182, 310, '2020-11-19'),
(3183, 337, '2020-11-19'),
(3184, 304, '2020-11-19'),
(3185, 317, '2020-11-19'),
(3186, 313, '2020-11-19'),
(3187, 308, '2020-11-19'),
(3188, 301, '2020-11-19'),
(3189, 316, '2020-11-19'),
(3190, 314, '2020-11-19'),
(3191, 306, '2020-11-19'),
(3192, 307, '2020-11-19'),
(3193, 333, '2020-11-19'),
(3194, 334, '2020-11-19'),
(3195, 335, '2020-11-19'),
(3196, 307, '2020-11-19'),
(3197, 336, '2020-11-19'),
(3198, 337, '2020-11-19'),
(3199, 318, '2020-11-19'),
(3200, 319, '2020-11-19'),
(3201, 317, '2020-11-19'),
(3202, 313, '2020-11-19'),
(3203, 316, '2020-11-19'),
(3204, 333, '2020-11-19'),
(3205, 334, '2020-11-19'),
(3206, 335, '2020-11-19'),
(3207, 336, '2020-11-19'),
(3208, 314, '2020-11-19'),
(3209, 337, '2020-11-19'),
(3210, 302, '2020-11-19'),
(3211, 302, '2020-11-19'),
(3212, 302, '2020-11-19'),
(3213, 337, '2020-11-19'),
(3214, 302, '2020-11-19'),
(3215, 302, '2020-11-19'),
(3216, 302, '2020-11-19'),
(3217, 302, '2020-11-19'),
(3218, 309, '2020-11-19'),
(3219, 324, '2020-11-19'),
(3220, 323, '2020-11-19'),
(3221, 311, '2020-11-19'),
(3222, 312, '2020-11-19'),
(3223, 337, '2020-11-19'),
(3224, 337, '2020-11-19'),
(3225, 325, '2020-11-19'),
(3226, 321, '2020-11-19'),
(3227, 315, '2020-11-19'),
(3228, 322, '2020-11-19'),
(3229, 329, '2020-11-19'),
(3230, 320, '2020-11-19'),
(3231, 337, '2020-11-19'),
(3232, 328, '2020-11-19'),
(3233, 337, '2020-11-19'),
(3234, 336, '2020-11-19'),
(3235, 326, '2020-11-19'),
(3236, 311, '2020-11-20'),
(3237, 310, '2020-11-20'),
(3238, 301, '2020-11-20'),
(3239, 330, '2020-11-20'),
(3240, 331, '2020-11-20'),
(3241, 322, '2020-11-20'),
(3242, 332, '2020-11-20'),
(3243, 327, '2020-11-20'),
(3244, 329, '2020-11-20'),
(3245, 305, '2020-11-20'),
(3246, 326, '2020-11-20'),
(3247, 328, '2020-11-20'),
(3248, 309, '2020-11-20'),
(3249, 302, '2020-11-20'),
(3250, 308, '2020-11-20'),
(3251, 300, '2020-11-20'),
(3252, 301, '2020-11-20'),
(3253, 302, '2020-11-20'),
(3254, 304, '2020-11-20'),
(3255, 303, '2020-11-20'),
(3256, 303, '2020-11-20'),
(3257, 305, '2020-11-20'),
(3258, 306, '2020-11-20'),
(3259, 309, '2020-11-20'),
(3260, 309, '2020-11-20'),
(3261, 309, '2020-11-20'),
(3262, 300, '2020-11-20'),
(3263, 307, '2020-11-20'),
(3264, 310, '2020-11-20'),
(3265, 331, '2020-11-20'),
(3266, 332, '2020-11-20'),
(3267, 311, '2020-11-20'),
(3268, 312, '2020-11-20'),
(3269, 324, '2020-11-20'),
(3270, 325, '2020-11-20'),
(3271, 322, '2020-11-20'),
(3272, 305, '2020-11-20'),
(3273, 318, '2020-11-20'),
(3274, 308, '2020-11-20'),
(3275, 319, '2020-11-20'),
(3276, 305, '2020-11-20'),
(3277, 310, '2020-11-20'),
(3278, 315, '2020-11-21'),
(3279, 319, '2020-11-21'),
(3280, 321, '2020-11-21'),
(3281, 320, '2020-11-21'),
(3282, 306, '2020-11-21'),
(3283, 315, '2020-11-21'),
(3284, 323, '2020-11-21'),
(3285, 336, '2020-11-21'),
(3286, 335, '2020-11-21'),
(3287, 337, '2020-11-21'),
(3288, 310, '2020-11-21'),
(3289, 305, '2020-11-21'),
(3290, 326, '2020-11-21'),
(3291, 330, '2020-11-21'),
(3292, 329, '2020-11-21'),
(3293, 328, '2020-11-21'),
(3294, 334, '2020-11-21'),
(3295, 333, '2020-11-21'),
(3296, 327, '2020-11-21'),
(3297, 331, '2020-11-21'),
(3298, 312, '2020-11-21'),
(3299, 305, '2020-11-21'),
(3300, 336, '2020-11-21'),
(3301, 336, '2020-11-21'),
(3302, 333, '2020-11-21'),
(3303, 327, '2020-11-21'),
(3304, 334, '2020-11-21'),
(3305, 314, '2020-11-21'),
(3306, 95, '2020-11-21'),
(3307, 309, '2020-11-21'),
(3308, 308, '2020-11-21'),
(3309, 309, '2020-11-21'),
(3310, 122, '2020-11-22'),
(3311, 303, '2020-11-22'),
(3312, 311, '2020-11-22'),
(3313, 309, '2020-11-22'),
(3314, 328, '2020-11-22'),
(3315, 334, '2020-11-22'),
(3316, 309, '2020-11-22'),
(3317, 331, '2020-11-22'),
(3318, 122, '2020-11-22'),
(3319, 330, '2020-11-22'),
(3320, 323, '2020-11-22'),
(3321, 303, '2020-11-22'),
(3322, 324, '2020-11-22'),
(3323, 332, '2020-11-22'),
(3324, 326, '2020-11-22'),
(3325, 333, '2020-11-22'),
(3326, 337, '2020-11-22'),
(3327, 330, '2020-11-22'),
(3328, 331, '2020-11-22'),
(3329, 329, '2020-11-22'),
(3330, 327, '2020-11-22'),
(3331, 336, '2020-11-22'),
(3332, 326, '2020-11-22'),
(3333, 335, '2020-11-23'),
(3334, 334, '2020-11-23'),
(3335, 316, '2020-11-23'),
(3336, 329, '2020-11-23'),
(3337, 306, '2020-11-23'),
(3338, 329, '2020-11-23'),
(3339, 95, '2020-11-23'),
(3340, 337, '2020-11-23'),
(3341, 337, '2020-11-23'),
(3342, 303, '2020-11-23'),
(3343, 303, '2020-11-23'),
(3344, 325, '2020-11-23'),
(3345, 337, '2020-11-23'),
(3346, 310, '2020-11-23'),
(3347, 316, '2020-11-23'),
(3348, 307, '2020-11-23'),
(3349, 311, '2020-11-23'),
(3350, 328, '2020-11-24'),
(3351, 337, '2020-11-24'),
(3352, 329, '2020-11-24'),
(3353, 308, '2020-11-24'),
(3354, 329, '2020-11-24'),
(3355, 308, '2020-11-24'),
(3356, 324, '2020-11-24'),
(3357, 322, '2020-11-24'),
(3358, 332, '2020-11-24'),
(3359, 323, '2020-11-24'),
(3360, 335, '2020-11-24'),
(3361, 337, '2020-11-24'),
(3362, 321, '2020-11-25'),
(3363, 305, '2020-11-25'),
(3364, 337, '2020-11-25'),
(3365, 329, '2020-11-25'),
(3366, 309, '2020-11-25'),
(3367, 307, '2020-11-25'),
(3368, 313, '2020-11-25'),
(3369, 304, '2020-11-25'),
(3370, 332, '2020-11-25'),
(3371, 337, '2020-11-25'),
(3372, 326, '2020-11-25'),
(3374, 329, '2020-11-25'),
(3375, 314, '2020-11-25'),
(3376, 337, '2020-11-25'),
(3377, 337, '2020-11-25'),
(3378, 309, '2020-11-25'),
(3379, 309, '2020-11-25'),
(3380, 309, '2020-11-25'),
(3381, 337, '2020-11-25'),
(3382, 326, '2020-11-25'),
(3383, 309, '2020-11-25'),
(3384, 312, '2020-11-25'),
(3385, 305, '2020-11-25');

-- --------------------------------------------------------

--
-- Table structure for table `product_discounts`
--

CREATE TABLE `product_discounts` (
  `id` int(11) NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `discount_id` int(10) UNSIGNED NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_discounts`
--

INSERT INTO `product_discounts` (`id`, `product_id`, `discount_id`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(138, 319, 1, '154', '2020-11-18 18:03:20', '2020-11-18 18:03:20', NULL),
(139, 319, 5, '10', '2020-11-18 18:03:20', '2020-11-18 18:03:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `product_id` int(191) NOT NULL,
  `review` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rating` tinyint(2) NOT NULL,
  `review_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(11) NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `comment_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `product_id` int(192) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `photo`, `title`, `subtitle`, `details`) VALUES
(4, '1557343012img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.'),
(5, '1557343018img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.'),
(6, '1557343024img.jpg', 'Jhon Smith', 'CEO & Founder', 'Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi.');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `section`) VALUES
(16, 'Manager', 'orders , products , affilate_products , customers , vendors , vendor_subscription_plans , categories , bulk_product_upload , product_discussion , set_coupons , blog , messages , general_settings , home_page_settings , menu_page_settings , emails_settings , payment_settings , social_settings , language_settings , seo_tools , subscribers'),
(17, 'Moderator', 'orders , products , customers , vendors , categories , blog , messages , home_page_settings , payment_settings , social_settings , language_settings , seo_tools , subscribers'),
(18, 'Staff', 'orders , products , vendors , vendor_subscription_plans , categories , blog , home_page_settings , menu_page_settings , language_settings , seo_tools , subscribers');

-- --------------------------------------------------------

--
-- Table structure for table `seotools`
--

CREATE TABLE `seotools` (
  `id` int(10) UNSIGNED NOT NULL,
  `google_analytics` text COLLATE utf8mb4_unicode_ci,
  `meta_keys` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seotools`
--

INSERT INTO `seotools` (`id`, `google_analytics`, `meta_keys`) VALUES
(1, '<script>//Google Analytics Scriptfffffffffffffffffffffffssssfffffs</script>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `user_id`, `title`, `details`, `photo`) VALUES
(2, 0, 'FREE SHIPPING', 'Free Shipping All Order', '1561348133service1.png'),
(3, 0, 'PAYMENT METHOD', 'Secure Payment', '1561348138service2.png'),
(4, 0, '30 DAY RETURNS', '30-Day Return Policy', '1561348143service3.png'),
(5, 0, 'HELP CENTER', '24/7 Support System', '1561348147service4.png'),
(6, 13, 'FREE SHIPPING', 'Free Shipping All Order', '1563247602brand1.png'),
(7, 13, 'PAYMENT METHOD', 'Secure Payment', '1563247614brand2.png'),
(8, 13, '30 DAY RETURNS', '30-Day Return Policy', '1563247620brand3.png'),
(9, 13, 'HELP CENTER', '24/7 Support System', '1563247670brand4.png');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` int(11) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text,
  `subtitle` text,
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `user_id`, `title`, `subtitle`, `price`) VALUES
(1, 0, 'Free Shipping', '(10 - 12 days)', 0),
(2, 0, 'Express Shipping', '(5 - 6 days)', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(191) UNSIGNED NOT NULL,
  `subtitle_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle_size` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_color` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_anime` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `title_size` varchar(50) DEFAULT NULL,
  `title_color` varchar(50) DEFAULT NULL,
  `title_anime` varchar(50) DEFAULT NULL,
  `details_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details_size` varchar(50) DEFAULT NULL,
  `details_color` varchar(50) DEFAULT NULL,
  `details_anime` varchar(50) DEFAULT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `subtitle_text`, `subtitle_size`, `subtitle_color`, `subtitle_anime`, `title_text`, `title_size`, `title_color`, `title_anime`, `details_text`, `details_size`, `details_color`, `details_anime`, `photo`, `position`, `link`) VALUES
(11, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723943406.jpg', 'slide-one', '#'),
(12, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723947402.jpg', 'slide-one', '#'),
(13, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723990903.jpg', 'slide-one', '#'),
(14, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723992904.jpg', 'slide-one', '#'),
(15, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723995205.jpg', 'slide-one', '#'),
(17, NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', NULL, NULL, '#000000', 'fadeIn', '159723999301.jpg', 'slide-one', '#');

-- --------------------------------------------------------

--
-- Table structure for table `socialsettings`
--

CREATE TABLE `socialsettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gplus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dribble` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_status` tinyint(4) NOT NULL DEFAULT '1',
  `g_status` tinyint(4) NOT NULL DEFAULT '1',
  `t_status` tinyint(4) NOT NULL DEFAULT '1',
  `l_status` tinyint(4) NOT NULL DEFAULT '1',
  `d_status` tinyint(4) NOT NULL DEFAULT '1',
  `f_check` tinyint(10) DEFAULT NULL,
  `g_check` tinyint(10) DEFAULT NULL,
  `fclient_id` text COLLATE utf8mb4_unicode_ci,
  `fclient_secret` text COLLATE utf8mb4_unicode_ci,
  `fredirect` text COLLATE utf8mb4_unicode_ci,
  `gclient_id` text COLLATE utf8mb4_unicode_ci,
  `gclient_secret` text COLLATE utf8mb4_unicode_ci,
  `gredirect` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socialsettings`
--

INSERT INTO `socialsettings` (`id`, `facebook`, `gplus`, `twitter`, `linkedin`, `dribble`, `f_status`, `g_status`, `t_status`, `l_status`, `d_status`, `f_check`, `g_check`, `fclient_id`, `fclient_secret`, `fredirect`, `gclient_id`, `gclient_secret`, `gredirect`) VALUES
(1, 'https://www.facebook.com/', 'https://plus.google.com/', 'https://twitter.com/', 'https://www.linkedin.com/', 'https://dribbble.com/', 1, 1, 1, 1, 1, 0, 1, '503140663460329', 'f66cd670ec43d14209a2728af26dcc43', 'https://localhost/wireless/auth/facebook/callback', '536622682580-n50patemn5m6knmmvjl9dgqot0qhtf5o.apps.googleusercontent.com', 'LvTUnxm9JnyWwguZCgL_UlRl', 'http://localhost/wireless/auth/google/callback');

-- --------------------------------------------------------

--
-- Table structure for table `social_providers`
--

CREATE TABLE `social_providers` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `provider_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(191) UNSIGNED NOT NULL,
  `category_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` text,
  `is_featured` tinyint(4) DEFAULT '0',
  `is_newest` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `name`, `slug`, `status`, `photo`, `is_featured`, `is_newest`) VALUES
(69, 44, 'Iphone x', 'iphone-x', 1, '1597262283xs-max.png', 1, 1),
(70, 44, 'Iphone XS MAX', 'iphone-xs-max', 1, '1597262291xs-max.png', 1, 1),
(71, 44, 'Iphone 11', 'iphone-11', 1, '159726230011.png', 1, 1),
(72, 44, 'Iphone 11 Pro', 'iphone-11-pro', 1, '159726230711-pro.png', 1, 1),
(73, 44, 'Iphone 11 Pro Max', 'iphone-11-pro-max', 1, '159726233911-pro.png', 1, 1),
(74, 45, 'Galaxy S9', 'galaxy-s9', 1, '159726261911.png', 1, 1),
(75, 45, 'Galaxy Note 9', 'galaxy-note-9', 1, '1597323275note-9.png', 1, 1),
(76, 45, 'Galaxy S10', 'galaxy-s10', 1, '1597323340s10.png', 1, 1),
(77, 45, 'Galaxy Note 10', 'galaxy-note-10', 1, '1597323332note-10.png', 1, 1),
(78, 45, 'Galaxy Note 20 Ultra', 'galaxy-note-20-ultra', 1, '1597323215s20-ultra.png', 1, 1),
(79, 48, 'Google Pixel 4A', 'google-pixel-4a', 1, '1597326831Google-Pixel-4a.png', 1, 0),
(80, 48, 'Google Pixel 2 XL', 'google-pixel-2-xl', 1, '1597326765Google-Pixel-2-XL.png', 0, 0),
(81, 48, 'Google Pixel 3a XL', 'google-pixel-3a-xl', 1, '1597326467Google-Pixel-3a-XL.png', 0, 0),
(82, 48, 'Google Pixel 3 XL', 'google-pixel-3-xl', 1, '1597326421Google-Pixel-3-XL.png', 1, 0),
(83, 48, 'Google Pixel 4 XL', 'google-pixel-4-xl', 1, '1597326372Google-Pixel-4-XL.png', 1, 0),
(84, 47, 'Motorola One', 'motorola-one', 1, '1597325070Motorola-One.png', 0, 0),
(85, 47, 'Moto G7 Power', 'moto-g7-power', 1, '1597325046Moto-G7-Power.png', 0, 0),
(86, 47, 'Motorola One Hyper', 'motorola-one-hyper', 1, '1597325010Motorola-One-Hyper.png', 1, 0),
(87, 47, 'Moto G Stylus', 'moto-g-stylus', 1, '1597324959Moto-G-Stylus.png', 1, 0),
(88, 47, 'Motorola One Action', 'motorola-one-action', 1, '1597324942Motorola-One-Action.png', 0, 0),
(89, 46, 'LG V50 ThinQ 5G', 'lg-v50-thinq-5g', 1, '1597324414LG-V50-ThinQ-5G.png', 1, 0),
(90, 46, 'LG G8 ThinQ', 'lg-g8-thinq', 1, '1597324406LG-G8-ThinQ.png', 1, 0),
(91, 46, 'LG G8X ThinQ', 'lg-g8x-thinq', 1, '1597324398LG-G8X-ThinQ.png', 1, 0),
(92, 46, 'LG G7 ThinQ', 'lg-g7-thinq', 1, '1597324390LG-G7-ThinQ.png', 0, 0),
(93, 46, 'LG V30', 'lg-v30', 1, '1597324381lg-v30.png', 0, 0),
(94, 49, 'All', 'all-models', 1, NULL, 0, 0),
(95, 44, 'iPhone 6 Plus', 'iphone-6-plus', 1, '160450643951Ykvmxr8QL._AC_.jpg', 0, 0),
(96, 44, 'iPhone 6s', 'iphone-6s', 1, '16056403181.jpeg', 0, 0),
(97, 44, 'iPhone 6s Plus', 'iphone-6s-plus', 1, '16056429461.jpg', 0, 0),
(98, 44, 'iPhone 7', 'iphone-7', 1, '16056431741.jpg', 0, 0),
(99, 44, 'iPhone 7 Plus', 'iphone-7-plus', 1, '16056441991.jpg', 0, 0),
(100, 44, 'iPhone 8', 'iphone-8', 1, '16056446014.jpg', 0, 0),
(101, 44, 'iPhone 8 Plus', 'iphone-8-plus', 1, '16056452034.jpg', 0, 0),
(102, 44, 'iPhone XR', 'iphone-xr', 1, '16056460214.jpg', 0, 0),
(103, 45, 'Samsung S7 edge', 'Samsung-S7-edge', 1, '16057132233.jpg', 0, 0),
(104, 45, 'Samsung S8', 'samsung-s8', 1, '16057134612.jpg', 0, 0),
(105, 45, 'Samsung S8 Plus', 'samsung-s8-plus', 1, '16057139102.jpg', 0, 0),
(106, 45, 'Samsung S9 Plus', 'Samsung-S9-Plus', 1, '16057159842.jpg', 0, 0),
(107, 45, 'Samsung S10e', 'Samsung-S10e', 1, '16057166173.jpg', 0, 0),
(108, 45, 'Samsung S10', 'Samsung-S10', 1, '16057214143.jpg', 0, 0),
(109, 45, 'Samsung S10 Plus', 'Samsung-S10-Plus', 1, '16057216603.jpg', 0, 0),
(110, 45, 'Samsung S20', 'Samsung-S20', 1, '16057279963.jpg', 0, 0),
(111, 45, 'Samsung S20 Plus 5G', 'Samsung-S20-Plus-5G', 1, '16057282463.jpg', 0, 0),
(112, 45, 'Samsung S20 Ultra', 'Samsung-S20-Ultra', 1, '16057287253.jpg', 0, 0),
(113, 45, 'Samsung Note 5', 'Samsung-Note-5', 1, '16057291693.jpg', 0, 0),
(114, 45, 'Samsung Note 8', 'Samsung-note-8', 1, '16057294712.jpg', 0, 0),
(115, 45, 'Samsung Note 9', 'Samsung-Note-9', 1, '16057302836.jpg', 0, 0),
(116, 45, 'Samsung Note 10 Plus', 'Samsung-Note-10Plus', 1, '16057315322.jpg', 0, 0),
(117, 45, 'Samsung A01', 'Samsung-A01', 1, '16057327103.jpg', 0, 0),
(118, 45, 'Samsung A10e', 'Samsung-A10e', 1, '16057329973.jpg', 0, 0),
(119, 45, 'Samsung A10s', 'Samsung-A10s', 1, '16057332462.jpg', 0, 0),
(120, 45, 'Samsung A11', 'Samsung-A11', 1, '16057342481.jpg', 0, 0),
(121, 45, 'Samsung A20s', 'Samsung-A20s', 1, '16057348552.jpg', 0, 0),
(122, 45, 'Samsung A21s', 'Samsung-A21s', 1, '16057350101.jpg', 0, 0),
(123, 45, 'Samsung A31', 'Samsung-A31', 1, '16057352362.jpg', 0, 0),
(124, 45, 'Samsung A51', 'Samsung-A51', 1, '16057353973.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(191) NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`) VALUES
(1, 'hellothisis@mymy.com'),
(2, 'vm.lafayette@gmail.com'),
(3, 'turkiyeteknoloji@yahoo.com'),
(4, 'orfibnina7@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `title`, `currency`, `currency_code`, `price`, `days`, `allowed_products`, `details`) VALUES
(5, 'Standard', '$', 'NGN', 60, 45, 25, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>'),
(6, 'Premium', '$', 'USD', 120, 90, 90, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(7, 'Unlimited', '$', 'USD', 250, 365, 0, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_provider` tinyint(10) NOT NULL DEFAULT '0',
  `status` tinyint(10) NOT NULL DEFAULT '0',
  `verification_link` text COLLATE utf8mb4_unicode_ci,
  `email_verified` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `affilate_code` text COLLATE utf8mb4_unicode_ci,
  `affilate_income` double NOT NULL DEFAULT '0',
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `owner_name` text COLLATE utf8mb4_unicode_ci,
  `shop_number` text COLLATE utf8mb4_unicode_ci,
  `shop_address` text COLLATE utf8mb4_unicode_ci,
  `reg_number` text COLLATE utf8mb4_unicode_ci,
  `shop_message` text COLLATE utf8mb4_unicode_ci,
  `shop_details` text COLLATE utf8mb4_unicode_ci,
  `shop_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personal_id` text COLLATE utf8mb4_unicode_ci,
  `tax_id` text COLLATE utf8mb4_unicode_ci,
  `f_url` text COLLATE utf8mb4_unicode_ci,
  `g_url` text COLLATE utf8mb4_unicode_ci,
  `t_url` text COLLATE utf8mb4_unicode_ci,
  `l_url` text COLLATE utf8mb4_unicode_ci,
  `is_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `f_check` tinyint(1) NOT NULL DEFAULT '0',
  `g_check` tinyint(1) NOT NULL DEFAULT '0',
  `t_check` tinyint(1) NOT NULL DEFAULT '0',
  `l_check` tinyint(1) NOT NULL DEFAULT '0',
  `mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `current_balance` double NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  `discount_list` int(10) UNSIGNED DEFAULT NULL,
  `credit_limit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `credit_line` text COLLATE utf8mb4_unicode_ci,
  `credit_usage` int(11) DEFAULT '0',
  `usage_date` date DEFAULT NULL,
  `last_usage_date` date DEFAULT NULL,
  `secure` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nonsecure` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `zip`, `city`, `country`, `address`, `phone`, `fax`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_provider`, `status`, `verification_link`, `email_verified`, `affilate_code`, `affilate_income`, `shop_name`, `owner_name`, `shop_number`, `shop_address`, `reg_number`, `shop_message`, `shop_details`, `shop_image`, `personal_id`, `tax_id`, `f_url`, `g_url`, `t_url`, `l_url`, `is_vendor`, `f_check`, `g_check`, `t_check`, `l_check`, `mail_sent`, `shipping_cost`, `current_balance`, `date`, `ban`, `discount_list`, `credit_limit`, `credit_line`, `credit_usage`, `usage_date`, `last_usage_date`, `secure`, `nonsecure`) VALUES
(13, 'Vendor', '1557677677bouquet_PNG62.png', '1234', 'Washington, DC', 'Algeria', 'Space Needle 400 Broad St, Seattles', '3453453345453411', '23123121', 'vendor@gmail.com', '$2y$10$.4NrvXAeyToa4x07EkFvS.XIUEc/aXGsxe1onkQ.Udms4Sl2W9ZYq', 'kqxYb8q3ZpHpJbjCzBea5rX3wgtdIyCyxEkAlxtcUzKmjXcaGz3j5BoYGPWV', '2018-03-07 18:05:44', '2020-06-17 03:16:38', 0, 2, '$2y$10$oIf1at.0LwscVwaX/8h.WuSwMKEAAsn8EJ.9P7mWzNUFIcEBQs8ry', 'Yes', '$2y$10$oIf1at.0LwscVwaX/8h.WuSwMKEAAsn8EJ.9P7mWzNUFIcEBQs8rysdfsdfds', 5000, 'Test Stores', 'User', '43543534', 'Space Needle 400 Broad St, Seattles', 'asdasd', 'sdf', '<br>', '1591356383beautyworld-vendor-banner.jpg', '1591358775iphone7.png', '1591358775featured_3.jpg', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 4978.02, '2019-11-24', 0, 5, '2000', '30', 0, NULL, NULL, '47,48', ''),
(22, 'Users', NULL, '1231', 'Test City', 'United States', 'Test Address', '34534534534', '34534534534', 'user@gmail.com', '$2y$10$.4NrvXAeyToa4x07EkFvS.XIUEc/aXGsxe1onkQ.Udms4Sl2W9ZYq', 'pktEyMBKOBTjaURr9LVq7V6Evyrl2YZNyCE3e3xE2TtfWq29rkt46mZV9J6z', '2019-06-20 12:26:24', '2020-05-28 08:39:33', 0, 0, '1edae93935fba69d9542192fb854a80a', 'Yes', '8f09b9691613ecb8c3f7e36e34b97b80', 4963.6900000000005, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(27, 'khan', NULL, '23456', 'karachi', 'Pakistan', 'Space Needle 400 Broad St, Seattles', '34534534', NULL, 'shahrukh@gmail.com', '$2y$12$iANezsvkPhCmK22ENhU8XeC93ApgkorryUHoNUxkdIIKvj8O7Noum', 'oCYVkijy6vGAYJpYYAg1wxEoYaRlcIeDM2bOEciS2PZjMMIYGCZ8Dx5DlJMd', '2019-10-05 04:15:08', '2020-06-20 10:36:15', 0, 0, '0521bba4c819528b6a18a581a5842f17', 'Yes', 'bb9d23401cd70f11998fe36ea7677797', 0, 'Test Store', 'User', '43543534', 'Space Needle 400 Broad St, Seattles', 'asdasd', 'ds', 'dfhgdfgd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2019-11-24', 0, 1, '1500', '10', 0, NULL, NULL, '47,49,52', ''),
(28, 'User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'junnun@gmail.com', '$2y$10$YDfElg7O3K6eQK5enu.TBOyo.8TIr6Ynf9hFQ8dsIDeWAfmmg6hA.', 'vHXrRc3RAqweqyTlHqTF08P0tV3cE4PxS4Appj1m9xgv2FWTVvJqZItcLFFv', '2019-10-13 05:39:13', '2019-10-13 05:39:13', 0, 0, '8036978c6d71501e893ba7d3f3ecc15d', 'Yes', '33899bafa30292165430cb90b545728a', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(29, 'john doe', '1591772541834-8345234_happy-customer-clipart-png-student-cartoon-gif-png.png', NULL, NULL, NULL, 'asdfsdfs', '8328311884', NULL, 'devmrmsoft@gmail.com', '$2y$10$p35S2FczpEfpbe41CX4j4.XE548tHBtF5weGLPxZ56MX5dsOFtaCC', 'gG16lGioAyehAPjm01ey1i3Hrh9AdeOfWfF6oo8PLVjgYqGMNoWn14p8zOAQ', '2020-05-15 21:52:47', '2020-06-10 02:02:21', 0, 0, 'da3f643a31172039ee73c06186414eaa', 'Yes', '58a1689a0482240c973ef347382be897', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(30, 'Shahrukh Khan', NULL, '61602', 'Peoria', 'US', '3982  Coburn Hollow Road', '03453305509', NULL, 'shahrukh.khan7991@gmail.com', '$2y$12$c73PMgdzEX3krfjSfeTM0um33Lad49lbXUTcuHQu563jGOnhZPSEK', '0dyfDnrgB6WoHTmDMuezQ2WJR0FTi9xW7I1w3b90JrqdbP5SriA569cNVpFm', '2020-06-16 06:11:58', '2020-06-16 06:11:58', 0, 0, '51027db67f7fe0ed2ea9fdb8c8f0bf6e', 'Yes', 'a8a2c7858303dc7682a47c721ab7d50d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(31, 'shayan haider', NULL, '78701', 'austin', '12', '7800 Harwin Drive, Suit A4 Houston, TX 7703', '8328311884', NULL, 'shayanhaider666@gmail.com', '$2y$10$p35S2FczpEfpbe41CX4j4.XE548tHBtF5weGLPxZ56MX5dsOFtaCC', '2bxMzYY8j6pRPSp6YUve33BHLEpBk8lVdwCIIKzvSuVcyYb20sKa7fHGjhRQ', '2020-06-20 10:16:59', '2020-10-30 23:23:13', 0, 0, '015e44d4996124a340edbefa3a8a2ae4', 'Yes', '39ff94f3cec2d8c5ad4c9fc948db0c29', 0, 'shayan horizon wireless', 'owner', '090078601', '7800 Harwin Drive, Suit A4 Houston, TX 7703', NULL, NULL, '<br>', NULL, '159266621901.png', '159266621914.png', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '1400', '20', 0, NULL, NULL, '47,49,50', ''),
(32, 'Talha Vaid', NULL, NULL, NULL, NULL, '7250 Harwin Dr Ste K', '8326144581', NULL, 'talhavaid.ae@gmail.com', '$2y$10$rZ4S5OL0AsKBJnrVKH1/OO29bFur3XYb3cWCkbM215G7UCsq1yYMy', NULL, '2020-08-23 04:29:35', '2020-08-23 04:29:35', 0, 0, '64fe0bccc363b4126421a09411aa1406', 'No', 'f04e38216c6ff3edee24b72344dda72e', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(33, 'richard strubin', NULL, NULL, NULL, NULL, '160 arrowhead rd cadiz,ky', '2709240821', NULL, 'richardstrubin@mediacombb.net', '$2y$10$2xAZ.5lAhaHHSA8QbgFIQuYoEB3F4LU/125D0EACuPLNsbL/cjVWy', NULL, '2020-08-24 22:01:35', '2020-08-24 22:01:35', 0, 0, 'e5998b07e1bc82b368f4cec5ce3aa5d2', 'No', 'c25ec34b9c482bd40da02b0fddc9105a', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(34, 'habeeb ahmed', NULL, NULL, NULL, NULL, '233 BUCK CREEK ROAD.  APT 102', '5022990980', NULL, 'indiausa517@gmail.com', '$2y$10$WUXp7Exk2Ji3Mc041N7ArOYrx1/jyNFBaJldmtbev01W2WDuXJ7XK', NULL, '2020-08-31 06:49:44', '2020-09-03 03:35:39', 0, 0, 'f5f337973c37d96be0bbd8bf75d90fe5', 'No', 'ac190616d1e1f9bb03d8aba1b3172157', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(35, 'habeeb ahmed', NULL, NULL, NULL, NULL, '233 BUCK CREEK ROAD.  APT 102', '5022990980', NULL, 'indiausa02@gmail.com', '$2y$10$Zvrio4iju8Z7KeIQN07TZubbPD5zVz3tjCFC8vUr0Pwdd6mM2JXca', NULL, '2020-08-31 07:00:55', '2020-08-31 07:00:55', 0, 0, '012db56d26fc5e1b2eaf9332d2d1d43e', 'No', '5e46dd35093f091c551327b75aae8347', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(36, 'Fawaz Qazi', NULL, NULL, NULL, NULL, '14317 Walters Road #101', '8323751104', NULL, 'unlimitedprepaidwireless@gmail.com', '$2y$10$9yz6uGv9F0tz0WDVqc54euBvsKTov7vBOn1lndQy7e608dxbVmBGG', NULL, '2020-09-02 05:30:39', '2020-09-02 05:35:50', 0, 0, 'b955a8af1d11f2be2bd28d048a8c8aa6', 'No', '9371e0cd10b22ca7cd4d3382e6bc1faf', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(37, 'Damon P Coleman', NULL, NULL, NULL, NULL, '11811 North Freeway ste 508', '8322306817', NULL, 'Yourchoicewireless2@gmail.com', '$2y$10$sGOTnL7c9jL.H98CTvpK0O/gAwokvMYHwKgmWff.yzvNFCNekMt0C', NULL, '2020-09-07 02:19:15', '2020-09-07 02:19:15', 0, 0, 'd97527086412ec23458158615b545daa', 'No', '084c36c30a1945afa5ce5e54745bcb85', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(38, 'amsallem thierry', NULL, NULL, NULL, NULL, '99 rue de courcelles 75017 paris', '+33612040404', NULL, 'vm.lafayette@gmail.com', '$2y$10$BDB3Nj91W4H3Jw7LezncFePJdRwEjrFK.vGAHkEkFOKSqksoDRvlW', NULL, '2020-09-10 20:24:10', '2020-09-10 20:24:10', 0, 0, 'fc389527dee4189a575c5fbec45a2158', 'No', '24bd9e46989d5bcec16dab4bf8377335', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(39, 'amsallem thierry', NULL, NULL, NULL, NULL, '99 rue de courcelles 75017 paris', '+33612040404', NULL, 'thierry@mobileone.fr', '$2y$10$GrMrCw84bcHXvxbTmYc4gOa3BBaP.LNHx/QTMDlSde82n4Zy96.NG', NULL, '2020-09-10 20:30:06', '2020-09-10 20:30:06', 0, 0, 'ea310a1dce77625db62014665ab5c51d', 'No', '43e8d1b5e04e72eb777f6e42d4ea040f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(40, 'robert pineda', NULL, NULL, NULL, NULL, '1221 n tustin st', '5628107628', NULL, 'rpwireless2012@yahoo.com', '$2y$10$m73xbFj8492gXRJXkxSK6usOL4kqBHQYF/DyAfg/qbaiXr69RQ6.i', NULL, '2020-09-16 21:26:25', '2020-09-16 21:26:25', 0, 0, '7ed889327976fc2340e9638bb690e735', 'No', '2d6fc3c5ca61f37639ee38129d6d0d73', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(41, 'Don Richard', NULL, NULL, NULL, NULL, '1755 Georgian Dr', '7736093210', NULL, 'admin@my1solution.com', '$2y$10$ferFRIqQLCS1Gx56Ym8HU.pEgUi961a/Q3DFNUqOE4W9dGdI6Xppy', NULL, '2020-09-17 20:18:30', '2020-09-17 20:18:30', 0, 0, 'b1c9edcec5e77e99fa5adccee381e7b7', 'No', 'bc1ffdda7476a43d39edf05a7aa673a5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(42, 'valentin bautista', NULL, NULL, NULL, NULL, '1104 nw park st', '8632615835', NULL, 'elprimowireless@yahoo.com', '$2y$10$jA.na.sLrTvw85oOkYbD8eWWUF/4bUyvPZiOpcA6U9qu3uwjKIDoK', 'XZSjZ1AXeDFDvCclbUwxuPpp7ZrVt4UjVMOKiO3tskziLNq8g54a2c6LXAfp', '2020-09-18 23:12:46', '2020-09-18 23:12:46', 0, 0, 'd94a1e7967a6f2178e33756728e443d1', 'No', 'c5908ef05b0433e0839c59a0fb76aa9f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(43, 'Fabian Carpio', NULL, NULL, NULL, NULL, '4555 E Charleston Blvd., Suite 107', '7024329500', NULL, 'Fabiancarpio@aol.com', '$2y$10$CfHPKj7ZOT7nrQ/k/AmfseCbVG5WkfGSkIzVjOSaXt4HPsKphFSd6', '5Tqlmfk2ksq0b7fGgYUxVAXoMqzE3O9c8kOi5wVFttd7uelO1FWDU1fqldK4', '2020-09-23 00:00:50', '2020-09-25 00:05:05', 0, 0, '64c41e9d4b149eaffe9e49b11368ee9b', 'No', 'f585074e7b3baf2e18d2af5ebac1faf6', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(44, 'Bashirat Kareem', NULL, NULL, NULL, NULL, '11735 S GLEN DR APT 1309', '9175933547', NULL, 'bashonweb@yahoo.com', '$2y$10$TX.1mjfNyXekY.4isPINReWD07.QlmeYoEXQdsRvtuqc2lRJpafEK', 'zW9lxt94scCbZH0q4B3kGe323lJY1XoPjMHYMF6rJwgrNpfCMrwaex7EbXL0', '2020-09-25 06:13:00', '2020-09-25 06:13:00', 0, 0, '38011f7f5bf7f9759c1b23ed81ded419', 'No', 'c13dd37ce9b24343723f412f403da3d5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(45, 'Ngoc-noc nguyen', NULL, NULL, NULL, NULL, '1156 park oak ct  milpitas ca   95035', '4086806306', NULL, 'ezgoyr2@gmail.com', '$2y$10$7r6aFDmOXYzIhtO42aN0P.m.SWfxkpuV6G2OVc3hyiUC0KXSEF9nW', NULL, '2020-09-27 01:04:48', '2020-09-27 01:22:13', 0, 0, 'bcdd43a70c438b38cb4de41535887c32', 'No', '39167d108f2e615b0181baf57945989c', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(46, 'Ngoc-nga Nguyen', NULL, NULL, NULL, NULL, '1156 0ak park ct  Milpitas ca 95035', '4086806306', NULL, 'ejneedruru@gmail.com', '$2y$10$MvFMm2tsPRaMtM5Up7N0BulDf1NEp6gaVZrgiOJ462lgNup0HLRie', NULL, '2020-09-27 01:33:45', '2020-09-27 01:33:45', 0, 0, '8e124a9d250a939dc1ef3f996a1beaab', 'No', '0adabf6043327c965e1d6a3c47613d57', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(47, 'Carlos Cisneros', NULL, NULL, NULL, NULL, '104 east calton RD suite 103', '9565161111', NULL, 'ccisneros1997@gmail.com', '$2y$10$SxxuXZ/Il5mphIW1wvy7PeaGBTjUnvt6.9pfa52Y2OOtuIdanTBjm', NULL, '2020-09-29 20:19:27', '2020-09-29 20:19:27', 0, 0, '3d49c9291e5579e1229aaa0bca27cf8a', 'No', 'd24808d5dddc9b509042135ae3752758', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(48, 'Ken', NULL, NULL, NULL, NULL, '28219 Red Raven Rd', '2163893555', NULL, 'kingber41@aol.com', '$2y$10$NhisdYk9IU4oSVymZq5rV.kXgGFbY1l40itKO8CHko6HSobh01zsm', 'zI7OgByLxPvj4055CaQ5zdi7ypz9DmzdNriomoGIvtLQ7DPba6Mftv8NAejn', '2020-09-29 21:27:02', '2020-09-29 21:27:02', 0, 0, '601f01500686f82327b79edab033d98b', 'No', '1ccff936c3fc8b283fd11dd40e4c027f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(49, 'Zohaib Malik', NULL, '33612', 'TAMPA', 'United States', '2159k university square mall', '8134011165', NULL, 'simplemobile786@gmail.com', '$2y$10$UiQTTPDvHUsvL2k8/k59T.R22F1PVml7aUfSDHXv3ljlPe2cwZDOa', NULL, '2020-09-30 21:52:28', '2020-10-07 21:28:31', 0, 0, 'f23281ad941b3859449e8e97819a5ca3', 'No', '43efb59303a763c2910d7187945c16f5', 0, 'Totalwireless Inc', NULL, '8139771657', 'Www.mytotalwireless.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(50, 'Andrew Saucedo', NULL, NULL, NULL, NULL, '5551 S Monitor Ave', '7089275235', NULL, 'andysaucedo@comcast.net', '$2y$10$UAMDYP6s6fj0NACoA2dkw.sufLyl3o.72Ee9CTPufgmsnFg8RQwUy', NULL, '2020-10-06 21:15:21', '2020-10-06 21:15:21', 0, 0, 'bfd82b7e3909e74162ea879395b41531', 'No', 'e65b046dd30d506a725c5b8027aac254', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(51, 'roger johnson', NULL, NULL, NULL, NULL, '640 center ave #109 martinez,94553', '4088899662', NULL, 'noneedruru@gmail.com', '$2y$10$tM78no0D/OpcVje42M/aauKy3TXQx3TQqy0FU/Ba0L0o.pMz5vL8C', NULL, '2020-10-07 17:07:23', '2020-10-07 17:07:23', 0, 0, '46e5a1956ac2c3c91d2ab23472499fc6', 'No', '1b7124acb0890aa48f41120b9dba43ad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(52, 'Cristian Avilés', NULL, NULL, NULL, NULL, 'Urb Hacienda Boriquen calle Mabo D5, Toa Alta PR 00953', '7872287682', NULL, 'tresuelvo1@gmail.com', '$2y$10$IYFCniX5YVj/k0oS5GdXHud/uw7eod37eIcPteCCaOm126d9uErrO', 'POtoPHmDpRftg88MUMIgcK0urm3LgPdAGPgqRfITmvvAudvKxBF9v6YjSMvm', '2020-10-08 07:03:37', '2020-10-22 07:07:41', 0, 0, 'db2cdc095e73a0d1bf2d738dedc64a81', 'No', '060335b92164f4e6c11f1d3ff4924bf5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(53, 'wylder martinez', NULL, NULL, NULL, NULL, '1580 college view dr, Apartment 3', '2134351655', NULL, 'wyldermartinez@gmail.com', '$2y$10$d2WT5gUgUlLICJXCjnYo3uvl61S0A8jfITY5kh8dwOAyELYkKCrLa', 'ufNQAQRxrNJgRYnjU6BeQadseLdR2sDqZzHxsuXi0acNA2CNf5nyiOd5ScnK', '2020-10-10 04:10:06', '2020-10-10 04:10:06', 0, 0, 'c33cd43f942a46913da25d62abe56102', 'No', '17b188f1bc42b1a2aa554578f4298a48', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(54, 'speedy mobil tek', NULL, NULL, NULL, NULL, '10200 hawthorne blvd', '3104916052', NULL, 'speedytek0824@gmail.com', '$2y$10$KmXmzdy4kWQPz5mJhCoSduubVJM5kw64L2u5YNV5KGWhJqAu51L2O', NULL, '2020-10-11 03:24:50', '2020-10-11 03:24:50', 0, 0, 'd61fd56bbee0b930cfd58ad98065c01e', 'No', 'e6e5bc3d4ff5dcf7a6030a0df01ff045', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(55, 'Iqbal Hossain', NULL, '06611', 'Trumbull', 'United States', '5065 Main Street #193', '2033720060', NULL, 'hossainp24@yahoo.com', '$2y$10$YjNBd27Dt6twHAynl8yRx.CdHZL2k.PXv69MW7wDlevqcgaAxLUgG', 'eyhuuYI20FHB3FvpWj4TfoFA3pFpJgdlzqrt3couLq09UzpnVKiwQuVkOJlk', '2020-10-12 21:29:02', '2020-10-12 21:29:02', 0, 0, '052603025f9ed710389dd86805256223', 'No', '3050d1583f475b45e4266ebae8dc0e52', 0, 'cell phone world', NULL, '2033720060', '(cell phone world) 5065 main st', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(58, 'shahrukh khan', NULL, '27591', 'Wendell', 'United States', '3204  Dola Mine Road', '321456987', NULL, 'vforce2008@gmail.com', '$2y$10$aL7vMmg2T1vGw5hKhQBKdOVdOhagKASpY3ZNDdwF/153EobUQ.45i', '7gQKuGVUQGRj7ecPEtoqORUYyWrWN1IYdQ0H0CDLqsBjOB5WNrUJg4XWq6UT', '2020-10-14 23:05:36', '2020-10-14 23:19:03', 0, 0, '8b7dece3362b894a3cfeccd8b80936ce', 'Yes', '0facd6947d46441db60efb38381a34c8', 0, 'dev', NULL, '1234567989', '3204  Dola Mine Road', NULL, NULL, NULL, NULL, '1602698736Bangamary .png', '1602698736banner.png', NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 1, 0, 0, '2020-11-13', 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(59, 'nazmul islam', NULL, NULL, NULL, NULL, '148 west Plumtree ln apt#24k Midvale ut 84047.', '5188198684', NULL, 'sixbrosenterpriseinc@gmail.com', '$2y$10$px.bkVQLj/oWw9wyFX3ZNeYj45qrVvexCeHvKvevMsa62YjoTYTWi', 'KZbOihSydWKWvGul1fOOSeJRnsucMn81Jxr9krYpY0kRglUymhI7J6vcSsmn', '2020-10-16 12:00:32', '2020-10-16 12:07:09', 0, 0, 'e10b7125cec85a7241cef6f026bed74a', 'No', '22b2ab4c94340c63c85b782cf53c5094', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(60, 'nazmul', NULL, NULL, NULL, NULL, '148 west Plumtree ln apt 24k,midvale ut 84047.', '5188198684', NULL, 'nazmul1989r@gmail.com', '$2y$10$Nix4iWWQGrWHkipY925BH.2QomSbYkQ9kUyJ3gZL92kPKn3zVXFte', NULL, '2020-10-16 12:12:17', '2020-10-16 12:12:17', 0, 0, '40c5498ac3e1e5a8ef46fb553e3c4545', 'No', 'c6bac6d46da68f0ef5ad68f1d893448e', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(61, 'sami baradie', NULL, NULL, NULL, NULL, '1359 fulton st', '7188579161', NULL, 'asphotocell@optimum.net', '$2y$10$OMMfehZ.4W9hImAKejc6AeYh/1YNKzPnjXJn7n7TvP.q/u2KkKduS', NULL, '2020-10-16 20:20:32', '2020-10-27 21:10:29', 0, 0, '18ff79390f479cf98413863275bf52f4', 'No', '85df2e6d2473c2dd9fbda94e8494cccc', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(62, 'Federico Tarragona', NULL, NULL, NULL, NULL, 'C/ GREGORIO ORDOÑEZ N° 12', '+34961412695', NULL, 'federico@cirkuitplanet.com', '$2y$10$OdeFcorjFOchAdb1Q3jirO4hmCm4xSK7skZGkFeJzGrNk6y.eifZa', NULL, '2020-10-19 17:55:11', '2020-10-19 17:55:11', 0, 0, 'c16b5cffb97e7615ab474049742eed94', 'No', 'bf16fe87dbfd63c706a2e52ba5c11d1d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(63, 'Kwan Park', NULL, NULL, NULL, NULL, '23830 HIGHWAY 99 STE 201', '4256974886', NULL, 'kpark@intercomws.com', '$2y$10$4uhWTMv7hbaMMude7PmeWe5td3DbFcAmx57kup1yuWoG4FNKxKbzq', NULL, '2020-10-21 00:45:49', '2020-10-21 00:47:23', 0, 0, '46023001ecbdf611582a826ff9fa1901', 'No', '7fac49cd4a581b52c97e8f76b8123af7', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(64, 'Kwan Park', NULL, NULL, NULL, NULL, '23830 HIGHWAY 99 STE 201', '4256974886', NULL, 'office@intercomws.com', '$2y$10$0LqDrGevCg7vpHBMfsHbfOMTM1Yl.Hzrsr7vsYtRHShmJc7kld8dO', NULL, '2020-10-21 00:46:18', '2020-10-21 00:46:18', 0, 0, 'd413a6f3bc8e88e79624b9e934f5f4be', 'No', 'a92721277a3a264ad12146ea987768e3', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(65, 'Federico Tarragona', NULL, NULL, NULL, NULL, 'C/ GREGORIO ORDOÑEZ N° 12', '961412695', NULL, 'federico@framlogistics.com', '$2y$10$3mzDmZ7HtohCPQyGB0LPren.oByAuTOGEedvqnS.fzMgGGGrfe7Qm', NULL, '2020-10-21 14:07:38', '2020-10-21 14:07:38', 0, 0, 'cd502bdae27d22f04a4a3477a908c194', 'No', '26c3e210426ef6e8c2e1aac68a363787', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(66, 'Faycal Bendedouch', NULL, NULL, NULL, NULL, '9090 Av du Parc, #102, Montréal, QC H2N 1Y8', '438-881-4955', NULL, 'liq.electro@gmail.com', '$2y$10$evVOtBf66ebKr/H2B0PEzeL4VXuBstQoxLk85tBiVfGd6izOOpIQW', NULL, '2020-10-22 01:14:12', '2020-10-22 01:14:12', 0, 0, '05aea19c3000cf576e804cb89d17e079', 'No', '1621581dba54e857bee24783155e5deb', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(67, 'Mahdi', NULL, NULL, NULL, NULL, '2940 pillsbury ave s suite minneapolis mn 55408', '6123564502', NULL, 'aliomahdi@gmail.com', '$2y$10$77zCfFRNHiFqtQ08wyzHyu/nyE9QWqRK9kFG0PkZdApIthx399WSu', 'cJ9qOmFO8cKo2D8p6EQ01km406SHGNlalcl7RufPgVDjhJskAjPgnFTNdkcp', '2020-10-22 15:19:25', '2020-10-22 15:42:45', 0, 0, '94909bc41a5cc9559742494b0f1c0490', 'No', 'c3d19a649a2b17c3f66f77ad6f96ce6f', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(68, 'Galaxia Esquivel', NULL, NULL, NULL, NULL, '5913 Glenmont Drive, Apt:420', '3463898683', NULL, 'galaxiaesquivel1@gmail.com', '$2y$10$LgNAXadfyBDlFplrfiEZeerVqxo9h3YP55sj9/gXP2bCbup/.kdmC', NULL, '2020-10-29 03:46:59', '2020-10-29 03:46:59', 0, 0, 'c43605122d7837e98417c99b33735fc0', 'No', '0e30c3b45452ae9903b13febffab2eb0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(69, 'Galaxia Esquivel', NULL, NULL, NULL, NULL, '5913 Glenmont Drive, Apt:420', '3463898683', NULL, 'galaxyesquivel1@gmail.com', '$2y$10$RwJ2p/7Lh8GtOoggchzok.4LsqkdoJ9Bmws1XFk2h0eWKH2gVjG4m', '5cmHIEyToA00EgTDCPn5SKIp1qe7MSArsU7RyL04baVgTDLR18sQRvYJtIqN', '2020-10-29 03:47:47', '2020-10-29 03:47:47', 0, 0, 'd7504da379eb450bb4c17c7fd7a434a8', 'No', '3b70048a72cbc148e19a688f01fa131d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(70, 'Gilberto Jimenez', NULL, '01843', 'Lawrence', 'United States', '191 S Broadway', '9786012883', NULL, 'jqcommunication@gmail.com', '$2y$10$/GenoL.DV74sF9TAqFQ0tOwDzhWY.CoKNJ9tF7PwCvac/qgY7f7F.', NULL, '2020-11-02 08:21:58', '2020-11-02 08:21:58', 0, 0, '19e863cae53db58b1992f1557ea0c94b', 'No', '92c7d82a33ff5bbafeed18662b256e8f', 0, 'JQ Communication', NULL, '9782080604', '191 s Broadway', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(71, 'richard a scholl', NULL, NULL, NULL, NULL, '111 w bay shore rd , bay shore NY 11706', '801 893 1876', NULL, 'mylife007821@gmail.com', '$2y$10$hMvQ.uuRDDv34HHdk5qwOuCkRDwHhD6nvgyDoZ9K9Nc9ytNKKzSQu', NULL, '2020-11-10 03:22:16', '2020-11-10 03:22:16', 0, 0, 'dbae4c70a015a0fc83ce1cb32b0507fa', 'No', '773a07e17fe4006ca9ca0244357c2aba', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(72, 'Raan Syd', NULL, NULL, NULL, NULL, 'Spring field mall', '7033986537', NULL, 'raansyd86@gmail.com', '$2y$10$IHz1VE1Dz3TvyfdzK5niduwNon9A8rE6V07mbNyH3PxSDgLmVdnv2', NULL, '2020-11-14 09:10:30', '2020-11-14 09:10:30', 0, 0, 'e8bf20372766daeff1736980bba5b3e6', 'No', 'cd164f7ce57de79b27ada6ac4f11353d', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(73, 'Sandy Stephens', NULL, NULL, NULL, NULL, 'Po Box 369, Whitley City, KY  42653', '6063765602', NULL, 'sandy@stephensprop.com', '$2y$10$w7LmhtMEGJ4BvVUKnjAIheHHutuYO2vqiB2o6724Q1QpuBHMAyC6.', NULL, '2020-11-17 03:31:00', '2020-11-17 03:31:00', 0, 0, '24a013b42df72ab44e68ea88050b5698', 'No', '4e580ec875110795afa75736d68beef2', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(74, 'Shayan Haider', NULL, NULL, NULL, NULL, 'Near Quba Masjid House NO# E/78 in Defence View Phase 2', '03174711209', NULL, 'shayanhaider333@gmail.com', '$2y$10$Z.JU.G3XuCeWzk0gJNqKiOmc7MsF.7ToeNVIxHgx5fAZzSBuFiig2', NULL, '2020-11-18 23:15:12', '2020-11-18 23:15:12', 0, 0, 'a2454102c44919e8a7a0dc1fc8ac515f', 'No', 'b15a3f6f2696551ae2b7e13e81eed53a', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(75, 'Ammad', NULL, NULL, NULL, NULL, 'lahore', '03138486712', NULL, 'sajjadkhan2386@gmail.com', '$2y$10$2bQYpAKwpOSc6e/GnThBOuecVoklsAWIb6HXhlX0TZrcjmZF.ede2', 'v9wtcJT1YZkslcE1Mo6QkbnnS8QlgsY5e6EKwIfekHI1UVN5kkdRE9fS5jTP', '2020-11-19 08:00:53', '2020-11-19 08:00:53', 0, 0, '5b2c6aa433382823b08d35e8734c8abd', 'No', 'bbffc6162a113088bf87da0f8dbbee92', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(76, 'Ubanto', NULL, NULL, NULL, NULL, 'Taxes', '123456789012', NULL, 'ubanto@gmail.com', '$2y$10$R13ezslbsrT0fYmJrUuVd.8fs.aoEr1MbXJpG2Yv5Y3M6khcOptMC', 'XnJD28ymbBwfBp98yYUZ42yiROyrrD0rqAQIxgobHnYEcU4DAFJPcAzH8ulG', '2020-11-25 13:30:42', '2020-11-25 13:30:42', 0, 0, 'e1f8138bfbd98a9ac9a547b4325b4c19', 'No', 'dc6d2c1c63b2999b07d8eee5177579a5', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(77, 'Perer Hills', NULL, NULL, NULL, NULL, 'Taxes', '123456789012', NULL, 'uf2178517@gmail.com', '$2y$10$nrIYJTN2S7uej7Jncmt7QeBS6hA0OupG4ZR7AvvIWzm5u.phSr1dm', 'MQZDGg9ZwzMaCSbuCD7DZIUU89DJGinM91af2dsisEScWhV36zKZEFwn55k6', '2020-11-25 18:35:58', '2020-11-25 18:35:58', 0, 0, 'c94e531702b6de454f95a55a59354864', 'No', '503bd96ae707f32b9a0623227383be67', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `order_number`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 13, '2csj1590602773', 1, '2020-05-27 13:06:13', '2020-06-05 06:26:54'),
(2, 13, 'f3uh1590603132', 1, '2020-05-27 13:12:12', '2020-06-05 06:26:54'),
(3, 13, 'pvtt1591012687', 1, '2020-06-01 06:58:08', '2020-06-05 06:26:54'),
(4, 13, 'yaDw1591013386', 1, '2020-06-01 07:09:46', '2020-06-05 06:26:54'),
(5, 13, 'Ss0C1591013646', 1, '2020-06-01 07:14:06', '2020-06-05 06:26:54'),
(6, 13, 'SS1P1591013979', 1, '2020-06-01 07:19:40', '2020-06-05 06:26:54'),
(7, 13, 'Ocjm1591349941', 1, '2020-06-05 04:39:02', '2020-06-05 06:26:54'),
(8, 13, 'e8vz1591350215', 1, '2020-06-05 04:43:36', '2020-06-05 06:26:54'),
(9, 13, 'jKNF1591350507', 1, '2020-06-05 04:48:27', '2020-06-05 06:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_subscriptions`
--

CREATE TABLE `user_subscriptions` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `subscription_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Free',
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payment_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_subscriptions`
--

INSERT INTO `user_subscriptions` (`id`, `user_id`, `subscription_id`, `title`, `currency`, `currency_code`, `price`, `days`, `allowed_products`, `details`, `method`, `txnid`, `charge_id`, `created_at`, `updated_at`, `status`, `payment_number`) VALUES
(81, 27, 5, 'Standard', '$', 'NGN', 60, 45, 25, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Paystack', '688094995', NULL, '2019-10-09 21:32:57', '2019-10-09 21:32:57', 1, NULL),
(84, 13, 5, 'Standard', '$', 'NGN', 60, 45, 500, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Paystack', '242099342', NULL, '2019-10-10 02:35:29', '2019-10-10 02:35:29', 1, NULL),
(85, 58, 8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>', 'Free', NULL, NULL, '2020-10-14 23:19:03', '2020-10-14 23:19:03', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_orders`
--

CREATE TABLE `vendor_orders` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `order_id` int(191) NOT NULL,
  `qty` int(191) NOT NULL,
  `price` double NOT NULL,
  `order_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_orders`
--

INSERT INTO `vendor_orders` (`id`, `user_id`, `order_id`, `qty`, `price`, `order_number`, `status`) VALUES
(1, 13, 1, 1, 130, '2csj1590602773', 'pending'),
(2, 13, 2, 1, 130, 'f3uh1590603132', 'pending'),
(3, 13, 3, 40, 3584, 'pvtt1591012687', 'pending'),
(4, 13, 4, 5, 700, 'yaDw1591013386', 'pending'),
(5, 13, 5, 5, 600, 'Ss0C1591013646', 'pending'),
(6, 13, 6, 1, 120, 'SS1P1591013979', 'pending'),
(7, 13, 7, 9, 1170, 'Ocjm1591349941', 'pending'),
(8, 13, 7, 9, 1170, 'Ocjm1591349941', 'pending'),
(9, 13, 8, 2, 260, 'e8vz1591350215', 'pending'),
(10, 13, 8, 2, 260, 'e8vz1591350215', 'pending'),
(11, 13, 9, 2, 260, 'jKNF1591350507', 'pending'),
(12, 13, 9, 2, 260, 'jKNF1591350507', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE `verifications` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `attachments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('Pending','Verified','Declined') DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `admin_warning` tinyint(1) NOT NULL DEFAULT '0',
  `warning_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`id`, `user_id`, `attachments`, `status`, `text`, `admin_warning`, `warning_reason`, `created_at`, `updated_at`) VALUES
(4, 13, '1573723849Baby.tux-800x800.png,1573723849Baby.tux-800x800.png', 'Declined', 'TEst', 0, NULL, '2019-11-14 03:30:49', '2020-06-16 07:12:09'),
(6, 30, NULL, NULL, NULL, 1, 'hello we didn\'t recieved it yet', '2020-06-16 06:07:47', '2020-06-16 06:07:47'),
(10, 31, '1592666391brand6.jpg,1592666391images.png', 'Verified', 'here it is', 0, 'hello send us your images', '2020-06-20 10:18:48', '2020-06-20 10:20:09'),
(11, 31, NULL, NULL, NULL, 1, 'hey send me your TAX ID.', '2020-07-24 14:23:47', '2020-07-24 14:23:47');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(191) UNSIGNED NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`) VALUES
(12, 22, 119),
(13, 22, 118),
(14, 22, 117),
(15, 22, 116);

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

CREATE TABLE `withdraws` (
  `id` int(191) NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `swift` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` float DEFAULT NULL,
  `fee` float DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('pending','completed','rejected') NOT NULL DEFAULT 'pending',
  `type` enum('user','vendor') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessory_brands`
--
ALTER TABLE `accessory_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accessory_types`
--
ALTER TABLE `accessory_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_languages`
--
ALTER TABLE `admin_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user_conversations`
--
ALTER TABLE `admin_user_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user_messages`
--
ALTER TABLE `admin_user_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_types`
--
ALTER TABLE `cat_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_type_children`
--
ALTER TABLE `cat_type_children`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `childcategories`
--
ALTER TABLE `childcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counters`
--
ALTER TABLE `counters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite_sellers`
--
ALTER TABLE `favorite_sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generalsettings`
--
ALTER TABLE `generalsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_tracks`
--
ALTER TABLE `order_tracks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagesettings`
--
ALTER TABLE `pagesettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pickups`
--
ALTER TABLE `pickups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategory_id` (`subcategory_id`);
ALTER TABLE `products` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `products` ADD FULLTEXT KEY `attributes` (`attributes`);

--
-- Indexes for table `product_clicks`
--
ALTER TABLE `product_clicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `discounts_id` (`discount_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seotools`
--
ALTER TABLE `seotools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socialsettings`
--
ALTER TABLE `socialsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_providers`
--
ALTER TABLE `social_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_orders`
--
ALTER TABLE `vendor_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraws`
--
ALTER TABLE `withdraws`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessory_brands`
--
ALTER TABLE `accessory_brands`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `accessory_types`
--
ALTER TABLE `accessory_types`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `admin_languages`
--
ALTER TABLE `admin_languages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_user_conversations`
--
ALTER TABLE `admin_user_conversations`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_user_messages`
--
ALTER TABLE `admin_user_messages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute_options`
--
ALTER TABLE `attribute_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `cat_types`
--
ALTER TABLE `cat_types`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cat_type_children`
--
ALTER TABLE `cat_type_children`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `childcategories`
--
ALTER TABLE `childcategories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `counters`
--
ALTER TABLE `counters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `favorite_sellers`
--
ALTER TABLE `favorite_sellers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=518;

--
-- AUTO_INCREMENT for table `generalsettings`
--
ALTER TABLE `generalsettings`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `order_tracks`
--
ALTER TABLE `order_tracks`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pagesettings`
--
ALTER TABLE `pagesettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `pickups`
--
ALTER TABLE `pickups`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

--
-- AUTO_INCREMENT for table `product_clicks`
--
ALTER TABLE `product_clicks`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3386;

--
-- AUTO_INCREMENT for table `product_discounts`
--
ALTER TABLE `product_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `seotools`
--
ALTER TABLE `seotools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `socialsettings`
--
ALTER TABLE `socialsettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_providers`
--
ALTER TABLE `social_providers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `vendor_orders`
--
ALTER TABLE `vendor_orders`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `withdraws`
--
ALTER TABLE `withdraws`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `subcategory_id_foreign_key` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`);

--
-- Constraints for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD CONSTRAINT `product_discounts_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_discounts_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
