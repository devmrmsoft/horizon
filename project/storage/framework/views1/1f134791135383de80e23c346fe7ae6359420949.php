

<?php $__env->startSection('content'); ?>
<?php
$brands = json_decode( DB::table('categories')->select('id','name','slug','photo')->where('brands',1)->orderBy('id','asc')->take(10)->get());

	$partners = DB::table('partners')->get();
?>
	<?php if($ps->slider == 1): ?>

		<?php if(count($sliders)): ?>
			<?php echo $__env->make('includes.slider-style', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
	<?php endif; ?>

	<?php if($ps->slider == 1): ?>
		<!-- Hero Area Start -->
		<section class="hero-area">
			<?php if($ps->slider == 1): ?>

				<?php if(count($sliders)): ?>
					<div class="hero-area-slider">
						<div class="slide-progress"></div>
						<div class="intro-carousel">
							<?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="intro-content <?php echo e($data->position); ?>" style="background-image: url(<?php echo e(asset('assets/images/sliders/'.$data->photo)); ?>)">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
												<div class="slider-content">
													<!-- layer 1 -->
													<div class="layer-1">
														<h4 style="font-size: <?php echo e($data->subtitle_size); ?>px; color: <?php echo e($data->subtitle_color); ?>" class="subtitle subtitle<?php echo e($data->id); ?>" data-animation="animated <?php echo e($data->subtitle_anime); ?>"><?php echo e($data->subtitle_text); ?></h4>
														<h2 style="font-size: <?php echo e($data->title_size); ?>px; color: <?php echo e($data->title_color); ?>" class="title title<?php echo e($data->id); ?>" data-animation="animated <?php echo e($data->title_anime); ?>"><?php echo e($data->title_text); ?></h2>
													</div>
													<!-- layer 2 -->
													<div class="layer-2">
														<p style="font-size: <?php echo e($data->details_size); ?>px; color: <?php echo e($data->details_color); ?>"  class="text text<?php echo e($data->id); ?>" data-animation="animated <?php echo e($data->details_anime); ?>"><?php echo e($data->details_text); ?></p>
													</div>
													<!-- layer 3 -->
													<div class="layer-3">
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				<?php endif; ?>

			<?php endif; ?>

		</section>
		<!-- Hero Area End -->
	<?php endif; ?>
    
    <section class="featuredBrand-section">
        <div class="__featuredBrand-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 __featuredBrand">
                        <div class="_featuredBrand">
                            <h1>Featured Brands</h1>                            
                        </div>
                    </div>
                    <div class="col-md-12 pt-3 __featuredBrand">
                        <div class="owl-carousel owl-theme __featuredSlider">
								<?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="item">
									<a href="<?php echo e(route('front.category',$brand->slug)); ?>">
										<img src="<?php echo e(asset('/assets/images/categories/')); ?>/<?php echo e($brand->photo); ?>" alt="" class="brand-navigation-image">
									</a>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="">
        <div class="product" style="background:#ffffff;">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col">
                        <div class="product00">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
							  <li class="nav-item">
							    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Smart Phones</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="cases-tab" data-toggle="tab" href="#cases" role="tab" aria-controls="profile" aria-selected="false">Cases</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="headphones-tab" data-toggle="tab" href="#headphones" role="tab" aria-controls="contact" aria-selected="false">Headphones</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="chargers-tab" data-toggle="tab" href="#chargers" role="tab" aria-controls="contact" aria-selected="false">Chargers</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="screen-tab" data-toggle="tab" href="#screen" role="tab" aria-controls="contact" aria-selected="false">Screen</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="protectors-tab" data-toggle="tab" href="#protectors" role="tab" aria-controls="contact" aria-selected="false">Protectors</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="cables-tab" data-toggle="tab" href="#cables" role="tab" aria-controls="contact" aria-selected="false">Cables</a>
							  </li>
							</ul>
							<div class="tab-content" id="myTabContent">
						  		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						  			<div class="row">						  				
										<?php $__currentLoopData = $smart_phones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo $__env->make('includes.product.home-latest-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="cases" role="tabpanel" aria-labelledby="cases-tab">
									
						  			<div class="row">						  				
										<?php $__currentLoopData = $cases_phones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo $__env->make('includes.product.home-latest-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="headphones" role="tabpanel" aria-labelledby="headphones-tab">
									
						  			<div class="row">						  				
										<?php $__currentLoopData = $headphone_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo $__env->make('includes.product.home-latest-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="chargers" role="tabpanel" aria-labelledby="chargers-tab">
									
						  			<div class="row">						  				
										<?php $__currentLoopData = $chargers_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo $__env->make('includes.product.home-latest-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="screen" role="tabpanel" aria-labelledby="screen-tab">
									
						  			<div class="row">						  				
										<?php $__currentLoopData = $smart_phones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo $__env->make('includes.product.home-latest-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="protectors" role="tabpanel" aria-labelledby="protectors-tab">
									
						  			<div class="row">						  				
										<?php $__currentLoopData = $smart_phones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo $__env->make('includes.product.home-latest-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  			</div>
								</div>
							  	<div class="tab-pane fade" id="cables" role="tabpanel" aria-labelledby="cables-tab">
									
						  			<div class="row">						  				
										<?php $__currentLoopData = $cables_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo $__env->make('includes.product.home-latest-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  			</div>
								</div>
							</div>
                        </div>
                    </div>
                    <!--Product Column-->
                </div>
                <!--Product Row-->
            </div>
            <!--Product Container-->
        </div>
    </section>
    <!---Featured Brand Section-->
	<?php if($ps->featured == 1): ?>
    <section class="featuredProduct" style="background:#eeeeee;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 featuredProduct-col">
                    <div class="featuredTitle">
                        <h1>featured products</h1>
                    </div>
                </div>
                <div class="col-md-12 featuredProduct-col">
                    <div class="owl-carousel featuredProduct-Slider owl-theme">
    						<?php $__currentLoopData = $feature_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    							<?php echo $__env->make('includes.product.slider-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!---Featured Product Section-->
	<?php if($ps->featured_category == 1): ?>

	
	
	

	<?php endif; ?>

	<?php if($ps->small_banner == 1): ?>

		<!-- Banner Area One Start -->
		<section class="banner-section">
			<div class="container">
				<?php $__currentLoopData = $top_small_banners->chunk(2); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chunk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="row">
						<?php $__currentLoopData = $chunk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-lg-6 remove-padding">
								<div class="left">
									<a class="banner-effect" href="<?php echo e($img->link); ?>" target="_blank">
										<img src="<?php echo e(asset('assets/images/banners/'.$img->photo)); ?>" alt="">
									</a>
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</section>
		<!-- Banner Area One Start -->
	<?php endif; ?>

	<section id="extraData">
		<div class="text-center">
			<img src="<?php echo e(asset('assets/images/'.$gs->loader)); ?>">
		</div>
	</section>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script>
        $(window).on('load',function() {

            setTimeout(function(){

                $('#extraData').load('<?php echo e(route('front.extraIndex')); ?>');

            }, 500);
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-prev').text('<');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-prev').addClass('adjust-arrow');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-next').text('>');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-next').addClass('adjust-arrow');
        });


	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>