<?php $__empty_1 = true; $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
	<div class="form-check ml-0 pl-0">
	  <input name="filter_phone_models[]" class="form-check-input attribute-input" type="checkbox" id="filter_phone_model-<?php echo e($model->id); ?>" value="<?php echo e($model->id); ?>" onchange="filter();">
	  <label class="form-check-label" for="filter_phone_model-<?php echo e($model->id); ?>"><?php echo e($model->name); ?></label>
	</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

<?php endif; ?>