
                    <div id="carouselExampleIndicators" class="carousel slide" data-interval="false"
                        data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner " role="listbox">
                            <!-- Slide One - Set the background image for this slide in the line below -->
                            <div class="carousel-item active">
                                <a href="<?php echo e(asset('assets/images/color_galleries/')); ?>/<?php echo e($slider_images[0]); ?>"><img src="<?php echo e(asset('assets/images/color_galleries/')); ?>/<?php echo e($slider_images[0]); ?>" class="img-fluid"></a>
                            </div>

                            <?php $__currentLoopData = $slider_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="carousel-item">
                                <a href="<?php echo e(asset('assets/images/color_galleries/')); ?>/<?php echo e($gal); ?>"><img src="<?php echo e(asset('assets/images/color_galleries/')); ?>/<?php echo e($gal); ?>" class="img-fluid"></a>
                            </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>