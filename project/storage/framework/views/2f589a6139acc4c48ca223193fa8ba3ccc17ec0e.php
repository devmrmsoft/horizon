<div class="row silver-bg">
		
	<div class="col-md-4">
		<select class="js-example-basic-single prdouct_id" id="prdouct_id" name="prdouct_id">
		<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <option value="<?php echo e($product->id); ?>"><?php echo e($product->name); ?></option>.
		 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</select>
	</div>
	<div class="col-md-3">
		<input type="text" name="product_qty" class="input-field" placeholder="Qty">
	</div>
	<div class="col-md-3">
		<a href="javascript:;" onclick="addSingleProduct(this)" class="add-more float-right"><i class="fas fa-plus"></i><?php echo e(__('Insert Product')); ?> </a>
	</div>
	<div class="col-md-2 remove-this-row">
		<button class="float-right " onclick="removeThisRow(this)"><span class="remove "><i class="fas fa-times"></i></span></button>
	</div>
</div>
