

<?php $__env->startSection('styles'); ?>

<style type="text/css">
  .product-size > .title{
    position: absolute;
  }
  .product-size > .siz-list{
    margin-left: 50px;
  }
  .product-size > .siz-list > li{
    width: 60px;
  }
  .product-details-page .right-area .product-info .product-size .siz-list li .box{
    width: 100%;
  }
</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



    <section class="shopDetail_title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h1><?php echo e($productt->name); ?></h1>
                    </div>
                    <div class="star-rating">
                        <span class="fa fa-star-o" data-rating="1"></span>
                        <span class="fa fa-star-o" data-rating="2"></span>
                        <span class="fa fa-star-o" data-rating="3"></span>
                        <span class="fa fa-star-o" data-rating="4"></span>
                        <span class="fa fa-star-o" data-rating="5"></span>
                        <input type="hidden" name="whatever1" class="rating-value" value="4.5">
                        <span class="counter">3.5 (250)</span>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="shopDetail_product">
        <div class="container product-details-page">
            <div class="row shopDetail_product_row right-area">


                <div class="col-md-7 shopDetail_product_col">
                    <div id="carouselExampleIndicators" class="carousel slide" data-interval="false"
                        data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner " role="listbox">
                            <!-- Slide One - Set the background image for this slide in the line below -->
                            <div class="carousel-item active">
                                <a href="<?php echo e(filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('assets/images/products/'.$productt->photo)); ?>"><img src="<?php echo e(filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('assets/images/products/'.$productt->photo)); ?>" class="img-fluid"></a>
                            </div>

                            <?php $__currentLoopData = $productt->galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="carousel-item">
                                <a href="<?php echo e(asset('assets/images/galleries/'.$gal->photo)); ?>"><img src="<?php echo e(asset('assets/images/galleries/'.$gal->photo)); ?>" class="img-fluid"></a>
                            </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <!---Product Image Slider-->
                <div class="col-md-5 shopDetail_text product-info">
                   

                   

                  <?php if(!empty($productt->size)): ?>
                  
                  <?php endif; ?>
                <div class="product-info">
                  <h4 class="product-name"><?php echo e($productt->name); ?></h4>
                  <div class="info-meta-1">
                    <ul>

                      <?php if($productt->type == 'Physical'): ?>
                      <?php if($productt->emptyStock()): ?>
                      <li class="product-outstook">
                        <p>
                          <i class="icofont-close-circled"></i>
                          <?php echo e($langg->lang78); ?>

                        </p>
                      </li>
                      <?php else: ?>
                      <li class="product-isstook">
                        <p>
                          <i class="icofont-check-circled"></i>
                          <?php echo e($gs->show_stock == 0 ? '' : $productt->stock); ?> <?php echo e($langg->lang79); ?>

                        </p>
                      </li>
                      <?php endif; ?>
                      <?php endif; ?>
                      <li>
                        <div class="ratings">
                          <div class="empty-stars"></div>
                          <div class="full-stars" style="width:<?php echo e(App\Models\Rating::ratings($productt->id)); ?>%"></div>
                        </div>
                      </li>
                      <li class="review-count">
                        <p><?php echo e(count($productt->ratings)); ?> <?php echo e($langg->lang80); ?></p>
                      </li>
                  <?php if($productt->product_condition != 0): ?>
                     <li>
                       <div class="<?php echo e($productt->product_condition == 2 ? 'mybadge' : 'mybadge1'); ?>">
                        <?php echo e($productt->product_condition == 2 ? 'New' : 'Used'); ?>

                       </div>
                     </li>
                  <?php endif; ?>
                    </ul>
                  </div>


                  <div class="product-price">
                    <span style="display: none;"  id="sizeprice"></span>
                    <p class="title"><?php echo e($langg->lang87); ?> :</p>
                    <p class="price"><span><?php echo e($productt->showPrice($productt->id)); ?></span>
                      <small><del><?php echo e($productt->showPreviousPrice()); ?></del></small></p>
                      <?php if($productt->youtube != null): ?>
                      <a href="<?php echo e($productt->youtube); ?>" class="video-play-btn mfp-iframe">
                        <i class="fas fa-play"></i>
                      </a>
                    <?php endif; ?>
                  </div>

                  <div class="info-meta-2">
                    <ul>

                      <?php if($productt->type == 'License'): ?>

                      <?php if($productt->platform != null): ?>
                      <li>
                        <p><?php echo e($langg->lang82); ?>: <b><?php echo e($productt->platform); ?></b></p>
                      </li>
                      <?php endif; ?>

                      <?php if($productt->region != null): ?>
                      <li>
                        <p><?php echo e($langg->lang83); ?>: <b><?php echo e($productt->region); ?></b></p>
                      </li>
                      <?php endif; ?>

                      <?php if($productt->licence_type != null): ?>
                      <li>
                        <p><?php echo e($langg->lang84); ?>: <b><?php echo e($productt->licence_type); ?></b></p>
                      </li>
                      <?php endif; ?>

                      <?php endif; ?>

                    </ul>
                  </div>


                  <?php if(!empty($productt->size)): ?>
                  <div class="product-size">
                    <p class="title"><?php echo e($langg->lang88); ?> :</p>
                    <ul class="siz-list">
                      <?php
                      $is_first = true;
                      ?>
                      <?php $__currentLoopData = $productt->size; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <li class="<?php echo e($is_first ? 'active' : ''); ?>">
                        <span class="box"><?php echo e($data1); ?> GB
                          <input type="hidden" class="size" value="<?php echo e($data1); ?>">
                          <input type="hidden" class="size_qty" value="<?php echo e($productt->size_qty[$key]); ?>">
                          <input type="hidden" class="size_key" value="<?php echo e($key); ?>">
                          <input type="hidden" class="size_price"
                            value="<?php echo e(round($productt->size_price[$key] * $curr->value,2)); ?>">
                        </span><div>+ $<?php echo e(round($productt->size_price[$key] * $curr->value,2)); ?></div>
                      </li>
                      <?php
                      $is_first = false;
                      ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <li>
                    </ul>
                  </div>
                  <?php endif; ?>

                  <?php if(!empty($productt->color) && !empty($productt->color_qty) && !empty($productt->color_price)): ?>
                  <div class="product-color">
                    <p class="title"><?php echo e($langg->lang89); ?> :</p>
                    <ul class="color-list">
                      <?php
                      $is_first = true;
                      $colorPrices  = explode(',',$productt->color_price);
                      $colorQty  = explode(',',$productt->color_qty);
                      ?>
                      <?php $__currentLoopData = $productt->color; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <li class="<?php echo e($is_first ? 'active' : ''); ?>">
                        <span class="box colorBox" data-key="<?php echo e($key); ?>" data-color="<?php echo e($productt->color[$key]); ?>" style="background-color: <?php echo e($productt->color[$key]); ?>"></span>
                        <input type="hidden" class="color_price" value="<?php echo e($colorPrices[$key]); ?>">
                        <input type="hidden" id="color_stock" value="<?php echo e($colorQty[0]); ?>">
                        <input type="hidden" class="color_key" value="<?php echo e($key); ?>">
                      </li>
                      <?php
                      $is_first = false;
                      ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </ul>
                  </div>
                  <?php endif; ?>

                  <?php if(!empty($productt->size)): ?>

                  <input type="hidden" id="stock" value="<?php echo e($productt->size_qty[0]); ?>">
                  <?php else: ?>
                  <?php
                  $stck = (string)$productt->stock;
                  ?>
                  <?php if($stck != null): ?>
                  <input type="hidden" id="stock" value="<?php echo e($stck); ?>">
                  <?php elseif($productt->type != 'Physical'): ?>
                  <input type="hidden" id="stock" value="0">
                  <?php else: ?>
                  <input type="hidden" id="stock" value="">
                  <?php endif; ?>

                  <?php endif; ?>
                  
                    <input type="hidden" id="actual_retail_price" value="<?php echo e(trim($productt->showPrice($productt->id),'$')); ?>">
                  <input type="hidden" id="product_price" value="<?php echo e(round($productt->vendorPrice($productt->id) * $curr->value,2)); ?>">

                  <input type="hidden" id="product_id" value="<?php echo e($productt->id); ?>">
                  <input type="hidden" id="curr_pos" value="<?php echo e($gs->currency_format); ?>">
                  <input type="hidden" id="curr_sign" value="<?php echo e($curr->sign); ?>">
                  <div class="info-meta-3">
                    <ul class="meta-list">
                      <?php if($productt->product_type != "affiliate"): ?>
                      <li class="d-block count <?php echo e($productt->type == 'Physical' ? '' : 'd-none'); ?>">
                        <div class="qty">
                          <ul>
                            <li>
                              <span class="qtminus">
                                <i class="icofont-minus"></i>
                              </span>
                            </li>
                            <li>
                              <span class="qttotal">1</span>
                            </li>
                            <li>
                              <span class="qtplus">
                                <i class="icofont-plus"></i>
                              </span>
                            </li>
                          </ul>
                        </div>
                      </li>
                      <?php endif; ?>

                      <?php if(!empty($productt->attributes)): ?>
                        <?php
                          $attrArr = json_decode($productt->attributes, true);
                        ?>
                      <?php endif; ?>
                      <?php if(!empty($attrArr)): ?>
                        <div class="product-attributes my-4">
                          <div class="row">
                          <?php $__currentLoopData = $attrArr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attrKey => $attrVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(array_key_exists("details_status",$attrVal) && $attrVal['details_status'] == 1): ?>

                          <div class="col-lg-6">
                              <div class="form-group mb-2">
                                <strong for="" class="text-capitalize"><?php echo e(str_replace("_", " ", $attrKey)); ?> :</strong>
                                <div class="">
                                <?php $__currentLoopData = $attrVal['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <div class="custom-control custom-radio">
                                    <input type="hidden" class="keys" value="">
                                    <input type="hidden" class="values" value="">
                                    <input type="radio" id="<?php echo e($attrKey); ?><?php echo e($optionKey); ?>" name="<?php echo e($attrKey); ?>" class="custom-control-input product-attr"  data-key="<?php echo e($attrKey); ?>" data-price = "<?php echo e($attrVal['prices'][$optionKey] * $curr->value); ?>" value="<?php echo e($optionVal); ?>" <?php echo e($loop->first ? 'checked' : ''); ?>>
                                    <label class="custom-control-label" for="<?php echo e($attrKey); ?><?php echo e($optionKey); ?>"><?php echo e($optionVal); ?>


                                    <?php if(!empty($attrVal['prices'][$optionKey])): ?>
                                      +
                                      <?php echo e($curr->sign); ?> <?php echo e($attrVal['prices'][$optionKey] * $curr->value); ?>

                                    <?php endif; ?>
                                    </label>
                                  </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                              </div>
                          </div>
                            <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </div>
                        </div>
                      <?php endif; ?>

                      <?php if($productt->product_type == "affiliate"): ?>

                      <li class="addtocart">
                        <a href="<?php echo e(route('affiliate.product', $productt->slug)); ?>" target="_blank"><i
                            class="icofont-cart"></i> <?php echo e($langg->lang251); ?></a>
                      </li>
                      <?php else: ?>
                      <?php if($productt->emptyStock()): ?>
                      <li class="addtocart">
                        <a href="javascript:;" class="cart-out-of-stock">
                          <i class="icofont-close-circled"></i>
                          <?php echo e($langg->lang78); ?></a>
                      </li>
                      <?php else: ?>
                      <li class="addtocart">
                        <a href="javascript:;" id="addcrt"><i class="icofont-cart"></i><?php echo e($langg->lang90); ?></a>
                      </li>
                      
                      <?php endif; ?>

                      <?php endif; ?>
                      
                    </ul>
                  </div>
                  <script async src="https://static.addtoany.com/menu/page.js"></script>


                  <?php if($productt->ship != null): ?>
                  <p class="estimate-time"><?php echo e($langg->lang86); ?>: <b> <?php echo e($productt->ship); ?></b></p>
                  <?php endif; ?>
                  <?php if( $productt->sku != null ): ?>
                  <p class="p-sku">
                    <?php echo e($langg->lang77); ?>: <span class="idno"><?php echo e($productt->sku); ?></span>
                  </p>
                  <?php endif; ?>
                </div>
              </div>
                <!--Product Detail-->
            </div>
        </div>
    </section>
    <!--Single Page Main Product-->

    <section class="reviews-tab">
        <div class="container">
            <div class="row reviews-tab-row">
                <div class="col-md-12 reviews-tab-col">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-overview-tab" data-toggle="pill" href="#pills-overview" role="tab" aria-controls="pills-overview" aria-selected="true">Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-features-tab" data-toggle="pill" href="#pills-features" role="tab" aria-controls="pills-features" aria-selected="false">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="false">Reviews</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-accessories-tab" data-toggle="pill" href="#pills-accessories" role="tab" aria-controls="pills-accessories" aria-selected="false">Accessories</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-overview" role="tabpanel" aria-labelledby="pills-overview-tab">
                            <div class="pr-1">
                                <p><?php echo $productt->details; ?></p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-features" role="tabpanel" aria-labelledby="pills-features-tab">
                            2
                        </div>
                        <div class="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                            3
                        </div>
                        <div class="tab-pane fade" id="pills-accessories" role="tabpanel" aria-labelledby="pills-accessories-tab">
                            4
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">

  $(document).on("submit", "#emailreply1", function () {
    var token = $(this).find('input[name=_token]').val();
    var subject = $(this).find('input[name=subject]').val();
    var message = $(this).find('textarea[name=message]').val();
    var $type  = $(this).find('input[name=type]').val();
    $('#subj1').prop('disabled', true);
    $('#msg1').prop('disabled', true);
    $('#emlsub').prop('disabled', true);
    $.ajax({
      type: 'post',
      url: "<?php echo e(URL::to('/user/admin/user/send/message')); ?>",
      data: {
        '_token': token,
        'subject': subject,
        'message': message,
        'type'   : $type
      },
      success: function (data) {
        $('#subj1').prop('disabled', false);
        $('#msg1').prop('disabled', false);
        $('#subj1').val('');
        $('#msg1').val('');
        $('#emlsub').prop('disabled', false);
        if(data == 0)
          toastr.error("Oops Something Goes Wrong !!");
        else
          toastr.success("Message Sent !!");
        $('.close').click();
      }

    });
    return false;
  });

</script>


<script type="text/javascript">

  $(document).on("submit", "#emailreply", function () {
    var token = $(this).find('input[name=_token]').val();
    var subject = $(this).find('input[name=subject]').val();
    var message = $(this).find('textarea[name=message]').val();
    var email = $(this).find('input[name=email]').val();
    var name = $(this).find('input[name=name]').val();
    var user_id = $(this).find('input[name=user_id]').val();
    var vendor_id = $(this).find('input[name=vendor_id]').val();
    $('#subj').prop('disabled', true);
    $('#msg').prop('disabled', true);
    $('#emlsub').prop('disabled', true);
    $.ajax({
      type: 'post',
      url: "<?php echo e(URL::to('/vendor/contact')); ?>",
      data: {
        '_token': token,
        'subject': subject,
        'message': message,
        'email': email,
        'name': name,
        'user_id': user_id,
        'vendor_id': vendor_id
      },
      success: function () {
        $('#subj').prop('disabled', false);
        $('#msg').prop('disabled', false);
        $('#subj').val('');
        $('#msg').val('');
        $('#emlsub').prop('disabled', false);
        toastr.success("<?php echo e($langg->message_sent); ?>");
        $('.ti-close').click();
      }
    });
    return false;
  });

    $('.colorBox').on('click',function(){
        var getColor        =   $(this).attr('data-color');
        var getColorKey     =   $(this).attr('data-key');
        var pro_id          =   $('#product_id').val();
        $.ajax({
          url:mainurl+"/get_product_color_gallery_slider_images",
          type: "GET",
          data:{getColor:getColor,getColorKey:getColorKey,pro_id:pro_id},
          dataType: 'JSON',
          success: function(data){
            if(data.success == true){
              $('.shopDetail_product_col').html(data.html);
             }
          }
        });
      });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>