                                
                                <?php if($prod->user_id != 0): ?>

                                
                                <?php if($prod->user->is_vendor == 2): ?>
                                		
                                    <div class="item">
                                        <div class="__product-top">
                                            <div class="product-image">
                                                <a href="<?php echo e(route('front.product', $prod->slug)); ?>"><img src="<?php echo e($prod->thumbnail ? asset('assets/images/thumbnails/'.$prod->thumbnail):asset('assets/images/noimage.png')); ?>"
                                                        class="img-fluid"></a>
                                            </div>
                                            <div class="product-title text-center">
                                                <a href="<?php echo e(route('front.product', $prod->slug)); ?>"><?php echo e($prod->showName()); ?></a>
                                            </div>
                                        </div>
                                        <div class="__product-bottom">
                                            <div class="description">
                                                <h1><?php echo e($prod->showPrice($prod->id)); ?></h1>
                                                <h2><del><?php echo e($prod->showPreviousPrice()); ?></del></h2>
                                                <!--<a tabindex="0" data-toggle="tooltip" title="Add to Cart!" class="add-to-cart add-to-cart-btn" data-href="<?php echo e(route('product.cart.add',$prod->id)); ?>"><i-->
                                                <!--        class="fas fa-shopping-cart"></i></a>-->
                                            </div>
                                            <div class="view-more-button">

											<?php if(Auth::guard('web')->check()): ?>
                                                <a href="javascript:;" class="add-to-wish" data-href="<?php echo e(route('user-wishlist-add',$prod->id)); ?>" data-toggle="tooltip" data-placement="right" title="<?php echo e($langg->lang54); ?>" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
											<?php else: ?>
                                                <a href="javascript:;" rel-toggle="tooltip" title="<?php echo e($langg->lang54); ?>" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
                                            <?php endif; ?>
                                                <a href="javascript:;" class="add-to-compare" data-href="<?php echo e(route('product.compare.add',$prod->id)); ?>"  data-toggle="tooltip" data-placement="right" title="<?php echo e($langg->lang57); ?>" data-placement="right"><i class="fas fa-retweet"></i> Compare</a>
                                            </div>
                                        </div>
                                    </div>

								<?php endif; ?>

                                

								<?php else: ?> 


                                     <div class="item">
                                        <div class="__product-top">
                                            <div class="product-image">
                                                <a href="<?php echo e(route('front.product', $prod->slug)); ?>"><img src="<?php echo e($prod->thumbnail ? asset('assets/images/thumbnails/'.$prod->thumbnail):asset('assets/images/noimage.png')); ?>"
                                                        class="img-fluid"></a>
                                            </div>
                                            <div class="product-title text-center">
                                                <a href="<?php echo e(route('front.product', $prod->slug)); ?>"><?php echo e($prod->showName()); ?></a>
                                            </div>
                                        </div>
                                        <div class="__product-bottom">
                                            <div class="description">
                                                <h2><?php echo e($prod->showPrice($prod->id)); ?><sup><del><?php echo e($prod->showPreviousPrice()); ?></del></sup></h2>
                                                <a tabindex="0" data-toggle="tooltip" title="Add to Cart!" class="add-to-cart add-to-cart-btn" data-href="<?php echo e(route('product.cart.add',$prod->id)); ?>"><i class="fas fa-shopping-cart"></i></a>
                                            </div>
                                            <div class="view-more-button">

											<?php if(Auth::guard('web')->check()): ?>
                                                <a href="javascript:;" class="add-to-wish" data-href="<?php echo e(route('user-wishlist-add',$prod->id)); ?>" data-toggle="tooltip" data-placement="right" title="<?php echo e($langg->lang54); ?>" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
											<?php else: ?>
                                                <a href="javascript:;" rel-toggle="tooltip" title="<?php echo e($langg->lang54); ?>" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
                                            <?php endif; ?>
                                                <a href="javascript:;" class="add-to-compare" data-href="<?php echo e(route('product.compare.add',$prod->id)); ?>"  data-toggle="tooltip" data-placement="right" title="<?php echo e($langg->lang57); ?>" data-placement="right"><i class="fas fa-retweet"></i> Compare</a>
                                            </div>
                                        </div>
                                    </div>

								<?php endif; ?>