<?php
use App\accessoryBrand as Brands;
use App\accessoryType as AType;
use App\Models\Category;
use App\Models\Subcategory as DeviceModel;

$brandsDropDown =   Brands::where('status',1)->get();

$devices = json_decode( DB::table('categories')->select('id','name','slug')->where('devices',1)->orderBy('id','asc')->take(8)->get());
$phoneDeviceBrands  =   Category::where('status', 1)->orderBy('id','desc')->take(8)->get();
$brands = json_decode( DB::table('categories')->select('id','name','slug','photo')->where('brands',1)->orderBy('id','asc')->take(10)->get());
$carriers   =   DB::table('carriers')->select('id','name','status','slug','photo')->orderBy('id','asc')->take(8)->get();

$accessories = json_decode(DB::table('categories')->select('id','name','slug')->where('accessories',1)->orderBy('id','asc')->take(8)->get());
$arrivals = json_decode(DB::table('categories')->select('id','name','slug')->where('arrivals',1)->orderBy('id','asc')->take(8)->get());

$cat_type_children = DB::table('cat_type_children')->orderBy('id','asc')->get(); 
$img_array['0']="1";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if(isset($page->meta_tag) && isset($page->meta_description)): ?>
        <meta name="keywords" content="<?php echo e($page->meta_tag); ?>">
        <meta name="description" content="<?php echo e($page->meta_description); ?>">
        <title><?php echo e($gs->title); ?></title>
    <?php elseif(isset($blog->meta_tag) && isset($blog->meta_description)): ?>
        <meta name="keywords" content="<?php echo e($blog->meta_tag); ?>">
        <meta name="description" content="<?php echo e($blog->meta_description); ?>">
        <title><?php echo e($gs->title); ?></title>
    <?php elseif(isset($productt)): ?>
        <meta name="keywords" content="<?php echo e(!empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): ''); ?>">
        <meta name="description" content="<?php echo e($productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description)); ?>">
        <meta property="og:title" content="<?php echo e($productt->name); ?>" />
        <meta property="og:description" content="<?php echo e($productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description)); ?>" />
        <meta property="og:image" content="<?php echo e(asset('assets/images/thumbnails/'.$productt->thumbnail)); ?>" />
        <meta name="author" content="GeniusOcean">
        <title><?php echo e(substr($productt->name, 0,11)."-"); ?><?php echo e($gs->title); ?></title>
    <?php else: ?>
        <meta name="keywords" content="<?php echo e($seo->meta_keys); ?>">
        <meta name="author" content="AurangZeb">
        <title><?php echo e($gs->title); ?></title>
    <?php endif; ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- favicon -->
    <link rel="icon"  type="image/x-icon" href="<?php echo e(asset('assets/images/'.$gs->favicon)); ?>"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<?php if($langg->rtl == "1"): ?>

    <!-- stylesheet -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/rtl/all.css')); ?>">

    <!--Updated CSS-->
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/rtl/styles.php?color='.str_replace('#','',$gs->colors).'&amp;'.'header_color='.str_replace('#','',$gs->header_color).'&amp;'.'footer_color='.str_replace('#','',$gs->footer_color).'&amp;'.'copyright_color='.str_replace('#','',$gs->copyright_color).'&amp;'.'menu_color='.str_replace('#','',$gs->menu_color).'&amp;'.'menu_hover_color='.str_replace('#','',$gs->menu_hover_color))); ?>">

<?php else: ?>

    <!-- stylesheet -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/all.css')); ?>">

    <!--Updated CSS-->
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/styles.php?color='.str_replace('#','',$gs->colors).'&amp;'.'header_color='.str_replace('#','',$gs->header_color).'&amp;'.'footer_color='.str_replace('#','',$gs->footer_color).'&amp;'.'copyright_color='.str_replace('#','',$gs->copyright_color).'&amp;'.'menu_color='.str_replace('#','',$gs->menu_color).'&amp;'.'menu_hover_color='.str_replace('#','',$gs->menu_hover_color))); ?>">

<?php endif; ?>



    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/StyleSheet/Bootstrap4/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/StyleSheet/Bootstrap4/owl.theme.default.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/StyleSheet/Bootstrap4/animated.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/modified/style-modified.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/StyleSheet/Bootstrap4/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/fonts/impact.ttf')); ?>">



    <?php echo $__env->yieldContent('styles'); ?>

</head>

<body>

<?php if($gs->is_loader == 1): ?>
    <div class="preloader" id="preloader" style="background: url(<?php echo e(asset('assets/images/'.$gs->loader)); ?>) no-repeat scroll center center #FFF;"></div>
<?php endif; ?>

<?php if($gs->is_popup== 1): ?>

<?php if(isset($visited)): ?>
    <div style="display:none">
        <img src="<?php echo e(asset('assets/images/'.$gs->popup_background)); ?>">
    </div>

    <!--  Starting of subscribe-pre-loader Area   -->
    <div class="subscribe-preloader-wrap" id="subscriptionForm" style="display: none;">
        <div class="subscribePreloader__thumb" style="background-image: url(<?php echo e(asset('assets/images/'.$gs->popup_background)); ?>);">
            <span class="preload-close"><i class="fas fa-times"></i></span>
            <div class="subscribePreloader__text text-center">
                <h1><?php echo e($gs->popup_title); ?></h1>
                <p><?php echo e($gs->popup_text); ?></p>
                <form action="<?php echo e(route('front.subscribe')); ?>" id="subscribeform" method="POST">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group">
                        <input type="email" name="email"  placeholder="<?php echo e($langg->lang741); ?>" required="">
                        <button id="sub-btn" type="submit"><?php echo e($langg->lang742); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--  Ending of subscribe-pre-loader Area   -->

<?php endif; ?>

<?php endif; ?>
    <section class="sticky-header">
            <div class="top-header logo-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 top-header-col d-flex align-items-center justify-content-start">
                            <div class="help-content">
                                <a href="#"><i class="fas fa-phone-alt"></i></a>
                                <a href="#">Help | 713 988 6565</a>
                            </div>
                        </div>
                        <div class="col-md-8 top-header-col d-flex align-items-end justify-content-end order-lg-last">
                            <div class="top-nav-bar helpful-links">
                                <ul class="nav helpful-links-inner">
                                    
                                            <?php if(!Auth::guard('web')->check()): ?>
                                            <li class="login">
                                                <a href="<?php echo e(route('user.login')); ?>" class="sign-log">
                                                    <div class="links">
                                                        <span class="sign-in"><?php echo e($langg->lang12); ?></span>
                                                    </div>
                                                </a>
                                            </li>
                                            <?php else: ?>
                                            <li class="login">
                                                <?php $__currentLoopData = $devices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <a href="<?php echo e(url('devices')); ?>" class="sign-log">
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="links">
                                                        <span class="join">On Sale</span>
                                                    </div>
                                                </a>
                                            </li>
                                                <li class="profilearea my-dropdown">
                                                    <a href="<?php echo e(url('/user/dashboard')); ?>" id="profile-icon" class="profile carticon">
                                                        <span class="text">
                                                            <i class="far fa-user"></i> <?php echo e($langg->lang11); ?> 
                                                            <i class="sign-log"></i>
                                                        </span>
                                                    </a>
                                                    <div class="my-dropdown-menu profile-dropdown">
                                                        <ul class="profile-links">
                                                            <li>
                                                                <a href="<?php echo e(route('user-dashboard')); ?>"><i class="fas fa-angle-double-right"></i> <?php echo e($langg->lang221); ?></a>
                                                            </li>
                                                            <?php if(Auth::user()->IsVendor()): ?>
                                                            <li>
                                                                <a href="<?php echo e(route('vendor-dashboard')); ?>"><i class="fas fa-angle-double-right"></i> <?php echo e($langg->lang222); ?></a>
                                                            </li>
                                                            <?php endif; ?>
        
                                                            <li>
                                                                <a href="<?php echo e(route('user-profile')); ?>"><i class="fas fa-angle-double-right"></i> <?php echo e($langg->lang205); ?></a>
                                                            </li>
        
                                                            <li>
                                                                <a href="<?php echo e(route('user-logout')); ?>"><i class="fas fa-angle-double-right"></i> <?php echo e($langg->lang223); ?></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            <?php endif; ?>
        
                                    <?php if($gs->reg_vendor == 1): ?>
                                        <li class="nav-item">
                                        <?php if(Auth::check()): ?>
                                            <?php if(Auth::guard('web')->user()->is_vendor == 2): ?>
                                                <a href="<?php echo e(route('vendor-dashboard')); ?>" class="sell-btn">Retailer</a>
                                            <?php else: ?>
                                                <a href="<?php echo e(route('user-package')); ?>" class="sell-btn">Retailer</a>
                                            <?php endif; ?>
                                        </li>
                                        <?php else: ?>
                                        <li class="nav-item">
                                            <a href="<?php echo e(route('retailer-register')); ?>" class="sell-btn">Retailer</a>
                                        </li>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <li class="nav-item wishlist"  data-toggle="tooltip" data-placement="top" title="<?php echo e($langg->lang9); ?>">
                                        <?php if(Auth::guard('web')->check()): ?>
                                            <a href="<?php echo e(route('user-wishlists')); ?>" class="wish">
                                                <i class="far fa-heart"></i>
                                                <span id="wishlist-count"><?php echo e(Auth::user()->wishlistCount()); ?></span>
                                            </a>
                                        <?php else: ?>
                                            <a href="javascript:;" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" class="wish">
                                                <i class="far fa-heart"></i>
                                                <span id="wishlist-count">0</span>
                                            </a>
                                        <?php endif; ?>
                                    </li> 
                                    <li class="nav-item compare"  data-toggle="tooltip" data-placement="top" title="<?php echo e($langg->lang10); ?>">
                                        <a href="<?php echo e(route('product.compare')); ?>" class="wish compare-product">
                                                <i class="fas fa-exchange-alt"></i>
                                                <span id="compare-count"><?php echo e(Session::has('compare') ? count(Session::get('compare')->items) : '0'); ?></span>
                                            <!-- <div class="icon">
                                            </div> -->
                                        </a>
                                    </li>
        
                                    <li class="my-dropdown"  data-toggle="" data-placement="top" title="<?php echo e($langg->lang3); ?>">
                                        <a href="javascript:;" class="cart carticon">
                                            <div class="icon">
                                                <i class="icofont-cart"></i>
                                                <span class="cart-quantity" id="cart-count"><?php echo e(Session::has('cart') ? count(Session::get('cart')->items) : '0'); ?></span>
                                            </div>
        
                                        </a>
                                        <div class="my-dropdown-menu" id="cart-items">
                                            <?php echo $__env->make('load.cart', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        </div>
                                    </li>
        
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Top Header Area End -->
    
        <header id="header-nav" class="main-header header sticky-top">
        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand web-logo" href="<?php echo e(route('front.index')); ?>""><img class="img-fluid" src="<?php echo e(asset('assets/images/'.$gs->logo)); ?>" width="130" height="auto"></a>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto navigation">
                                    
                                     <!-- CATEGORY -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="<?php echo e(url('categories')); ?>" id="navbarDropdown"> Categories <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <div style="display:flex;">
                                                <ul class="nav sub-nav-lvl2 flex-column" style="width:80%;height:200px;padding-top:10px;">
                                                    <?php $__currentLoopData = $cat_type_children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li class="nav-item sub-cat-items-lvl2">
                                                        <?php 
                                                            $img_array[$cat->id]=$cat->image;
                                                        ?>
                                                        <a id="<?php echo e($cat->id); ?>" href="<?php echo e(route('front.accessories',$cat->slug)); ?>" class="sub-cat-link-lvl2 cat-img"><?php echo e($cat->name); ?></a>
                                                        
                                                    </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                                <ul class="sub-nav-down">
                                                    <li class="nav-item">
                                                        <ul class="nav sub-nav-lvl2 flex-column">
                                                            <img id="cat-side-img" src="//horizonwirelesstx.com/assets/images/categories/1602603895cases.png" class="sub-new-img">
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="<?php echo e(url('categories')); ?>" type="button">see all Categories</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    
                                    
                                    <!-- DEVICES -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="<?php echo e(url('devices')); ?>" id="navbarDropdown"> Devices <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <ul class="nav sub-nav">
                                                <?php $__currentLoopData = $devices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="nav-item sub-items">
                                                    <a href="<?php echo e(url('/devices/')); ?>/<?php echo e($dev->slug); ?>" class="sub-link"><?php echo e($dev->name); ?></a>
                                                    <ul class="nav sub-nav-lvl2 flex-column">
                                                        <?php $subcats = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$dev->id)->where('products.acc_type_id','=','')->orderBy('subcategories.id','desc')->take(5)->get());
                                                        /*$subcats = DeviceModel::where('category_id', $dev->id)->where('status', 1)->orderBy('id','desc')->take(5)->get();*/
                                                        ?>
                                                        <?php $__currentLoopData = $subcats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <li class="sub-items-lvl2">
                                                                <a href="<?php echo e(url('/devices/')); ?>/<?php echo e($dev->slug); ?>/<?php echo e($sub->slug); ?>" class="sub-link-lvl2"><?php echo e($sub->name); ?></a>
                                                            </li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <a href="<?php echo e(url('/devices/')); ?>/<?php echo e($dev->slug); ?>" type="button" class="see-all-btn">see all <?php echo e($dev->name); ?></a>
                                                    </ul>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="<?php echo e(url('devices')); ?>" type="button">see all devices</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    
                                    <!-- BRANDS -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="<?php echo e(url('all-brands')); ?>" id="navbarDropdown"> Brands <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <ul class="nav sub-nav">
                                                <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="nav-item sub-items brands-sub-items">
                                                    <a href="<?php echo e(route('front.category',$brand->slug)); ?>">
                                                        <img id="" src="<?php echo e(asset('/assets/images/categories/')); ?>/<?php echo e($brand->photo); ?>" class="brand-navigation-image">
                                                    </a>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="<?php echo e(url('all-brands')); ?>" type="button">see all Brands</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- CARRIERS -->
                                    <li class="nav-item dropdown listing">
                                        <a class="nav-link dropdown-toggle list-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Carriers <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                           <ul class="nav sub-nav">
                                                <?php $__currentLoopData = $carriers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carrier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="nav-item sub-items carrier-sub-items">
                                                    <a href="<?php echo e(route('front.carrier',$carrier->slug)); ?>" class="sub-link">
                                                        <img id="" src="<?php echo e(asset('/assets/front/images/carriers/')); ?>/<?php echo e($carrier->photo); ?>" class="brand-navigation-image"></a>
                                                    
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="#" type="button">see all carriers</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown listing" style="display:none;">
                                        <a class="nav-link dropdown-toggle list-link" href="<?php echo e(route('front.accessories')); ?>" id="navbarDropdown"> Accessories <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <ul class="nav sub-nav">
                                                <?php $__currentLoopData = $accessories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accessory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="nav-item sub-items">
                                                    <a href="#" class="sub-link"><?php echo e($accessory->name); ?></a>
                                                    <ul class="nav sub-nav-lvl2 flex-column">
                                                        <?php $subcats_accessories = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug')->where('subcategories.category_id',$accessory->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                        <?php $__currentLoopData = $subcats_accessories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_accessory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <li class="sub-items-lvl2">
                                                                <a href="<?php echo e(url('/accessory/')); ?>/<?php echo e($accessory->slug); ?>/<?php echo e($sub_accessory->slug); ?>" class="sub-link-lvl2"><?php echo e($sub_accessory->name); ?></a>
                                                            </li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <a href="<?php echo e(url('/accessory/')); ?>/<?php echo e($accessory->slug); ?>" type="button" class="see-all-btn">see all <?php echo e($accessory->name); ?></a>
                                                    </ul>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                            </ul>
                                            <div class="row justify-content-center">
                                                <div class="col-md-7 text-center caterogy-button">
                                                    <a href="<?php echo e(route('front.accessories')); ?>" type="button">see all Accessory</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item listing" style="display:none;">
                                        <a class="nav-link list-link" href="#">New Arrivals</a>
                                    </li>
                                </ul>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#search"><i class="fas fa-search"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div id="search">
            <button type="button" class="close">×</button>
            <form id="searchForm" class="search-form" action="<?php echo e(route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')])); ?>" method="GET">
                <input type="search" placeholder="Search Your Product Here" id="prod_name" name="search" value="<?php echo e(request()->input('search')); ?>" autocomplete="off">
                <button type="submit" class="btn"><span>GO</span> <i class="fas fa-angle-double-right"></i></button>
            </form>
        </div>
    </header>
        
        <header id="mobile-nav" class="main-header header sticky-top .d-none .d-sm-flex d-lg-none">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mobile-nav-col">
                        <div class="search-button-1">
                            <a href="#" class="search-toggle-1" data-selector="#mobile-nav"></a>
                        </div>
                        <div id="my-mob-nav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            <ul class="siderbar-content">
                            <a class="nav-link" href="<?php echo e(url('categories')); ?>" data-toggle="collapse" aria-expanded="false" data-target="#Devices" aria-controls="Devices">Devices <i class="fas fa-chevron-down"></i>
                            </a>
                                <div id="Devices" class="collapse submenu">
                                    <ul class="nav flex-column first-submenu">
                                        <?php $__currentLoopData = $devices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="nav-item">
                                            <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model<?php echo e($dev->id); ?>" aria-controls="Model"><?php echo e($dev->name); ?><i class="fas fa-chevron-down"></i>
                                            </a>
                                            <div id="Model<?php echo e($dev->id); ?>" class="collapse submenu">
                                                <ul class="nav flex-column second-submenu">
                                                    <?php $subcats = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$dev->id)->where('products.acc_type_id','=','')->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                    <?php $__currentLoopData = $subcats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="/category/<?php echo e($dev->slug); ?>/<?php echo e($sub->slug); ?>" ><?php echo e($sub->name); ?>

                                                        </a>
                                                    </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        </li>     
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            
                            
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Brands" aria-controls="Brands">Brands <i class="fas fa-chevron-down"></i>
                            </a>
                                <div id="Brands" class="collapse submenu">
                                    <ul class="nav flex-column first-submenu">
                                        <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="nav-item">
                                            <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model<?php echo e($brand->id); ?>1" aria-controls="Model"><?php echo e($brand->name); ?><i class="fas fa-chevron-down"></i>
                                            </a>
                                            <div id="Model<?php echo e($brand->id); ?>1" class="collapse submenu">
                                                <ul class="nav flex-column second-submenu">
                                                    <?php $subcats_brand = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$brand->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                    <?php $__currentLoopData = $subcats_brand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li class="nav-item">
                                                       <a class="nav-link" href="/category/<?php echo e($brand->slug); ?>/<?php echo e($sub_brand->slug); ?>" ><?php echo e($sub_brand->name); ?></a>
                                                    </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        </li>     
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                                
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Carriers" aria-controls="Carriers">Carriers <i class="fas fa-chevron-down"></i>
                            </a>
                            <div id="Carriers" class="collapse submenu">
                                    <ul class="nav flex-column first-submenu">
                                        <?php $__currentLoopData = $carriers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carrier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="nav-item">
                                            <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model<?php echo e($carrier->id); ?>12" aria-controls="Model"><?php echo e($carrier->name); ?><i class="fas fa-chevron-down"></i>
                                            </a>
                                            <div id="Model<?php echo e($carrier->id); ?>12" class="collapse submenu">
                                                <ul class="nav flex-column second-submenu">
                                                    <?php $subcats_carrier = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$carrier->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                    <?php $__currentLoopData = $subcats_carrier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_carrier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li class="nav-item">
                                                       <a class="nav-link" href="/category/<?php echo e($carrier->slug); ?>/<?php echo e($sub_carrier->slug); ?>" ><?php echo e($sub_carrier->name); ?></a>
                                                       
                                                    </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        </li>     
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Accessories" aria-controls="Accessories">Accessories <i class="fas fa-chevron-down"></i>
                            </a>
                            <div id="Accessories" class="collapse submenu">
                                <ul class="nav flex-column first-submenu">
                                    <?php $__currentLoopData = $accessories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accessory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="nav-item">
                                        <a class="nav-link" id="model" onclick="" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Model<?php echo e($accessory->id); ?>123" aria-controls="Model"><?php echo e($accessory->name); ?><i class="fas fa-chevron-down"></i>
                                        </a>
                                        <div id="Model<?php echo e($accessory->id); ?>123" class="collapse submenu">
                                            <ul class="nav flex-column second-submenu">
                                                <?php $subcats_accessory = json_decode(DB::table('subcategories')->join('products','subcategories.id','=','products.subcategory_id')->select('subcategories.id','subcategories.name','subcategories.slug','products.acc_type_id')->where('subcategories.category_id',$accessory->id)->orderBy('subcategories.id','desc')->take(5)->get()); ?>
                                                <?php $__currentLoopData = $subcats_accessory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_accessory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="nav-item">
                                                   <a class="nav-link" href="/category/<?php echo e($accessory->slug); ?>/<?php echo e($sub_accessory->slug); ?>" ><?php echo e($sub_accessory->name); ?></a>
                                                   
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </li>     
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#Arrivals" aria-controls="Arrivals">New Arrivals <i class="fas fa-chevron-down"></i>
                            </a>
                            <a href="#" class="nav-link">
                                About Us
                            </a>
                        </ul>
                        </div>
                        <span style="font-size:33px;cursor:pointer;display:inline-block;line-height:normal;"
                            onclick="openNav()">&#9776;</span>
                        <div class="site-logo">
                            <a href="<?php echo e(route('front.index')); ?>"><img src="<?php echo e(asset('assets/images/'.$gs->logo)); ?>" class="img-fluid" width="135" height="auto"></a>
                        </div>
                        <form action="" class="search-box">
                            <input type="text" class="text search-input" placeholder="Type here to search..." />
                        </form>
                    </div>
                </div>
            </div>
        </header>
        <!---Mobile Responsive Header-->
    </section>

    <!--Main-Menu Area Start-->
    
    <!--Main-Menu Area End-->

<?php echo $__env->yieldContent('content'); ?>


    <footer class="siteFooter">
        <div class="siteFooter1">
            <div class="container">
                <div class="row">
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>Support</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a href="#" class="nav-link"><i class="fas fa-phone"></i>
                                        713-988-6565</a></li>
                                <li class="nav-item"><a href="#" class="nav-link"><i class="far fa-envelope"></i>
                                        info@horizonwirelesstx.com</a></li>
                                <li class="nav-item"><a href="#" class="nav-link"><i
                                            class="fas fa-envelope-open-text"></i> Accessibility</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>Shop</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="#">New Arrivals</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Devices</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Carriers</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>information</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="#">About Us</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Contact Us</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">FAQs</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>My Account</h1>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="#">My Account</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Sign In</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">View Cart</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Register</a></li>
                            </ul>
                        </div>
                    </div>
                    <!---Column-->
                    <div class="col-md siteFooter-col">
                        <div class="footernav">
                            <h1>Social Links</h1>
                            <a href="#"><i class="fab fa-facebook-square"></i></a>
                            <a href="#"><i class="fab fa-twitter-square"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-youtube"></i></a>
                        </div>
                    </div>
                    <!---Column-->
                </div>
                <!--Row-->
            </div>
            <!--Container-->
        </div>
    </footer>
    <footer class="copyRight-footer">
        <div class="copyRight">
            <div class="container">
                <div class="row">
                    <div class="col-md d-flex align-items-center copyRight-col">
                        <div class="copyRight00">
                            <h4 style="margin-bottom:0;">&copy; 2020 Horizon Wireless, All Rights Reserved.</h4>
                        </div>
                        <div class="copyRight01">
                            <i class="fab fa-cc-paypal" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="Paypal!" style="color: #009CDE;"></i><span
                                class="sr-only">Paypal!</span>
                            <i class="fab fa-cc-visa" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="Visa" style="color: #00579F;"></i><span class="sr-only">Visa</span>
                            <i class="fab fa-cc-mastercard" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="MasterCard" style="color: #d9222a;"></i><span
                                class="sr-only">MasterCard</span>
                            <i class="fab fa-cc-amex" data-toggle="tooltip" title="" aria-hidden="true"
                                data-original-title="AmercianX" style="color: #016CCA;"></i><span
                                class="sr-only">AmercianX</span>
                        </div>
                        <div class="copyRight02">
                            <h4>Powered By</h4>
                            <a href="https://mrm-soft.com/" target="__blank"><img src="<?php echo e(asset('assets/front/assets/img/mrm logo-02.png')); ?>"
                                    width="50"></a>
                        </div>
                    </div>
                    <!--Column-->
                </div>
                <!---Row-->
            </div>
            <!--Container-->
        </div>
    </footer>
    <!--Copy Right Footer-->

    <!-- Back to Top Start -->
    <div class="bottomtotop">
        <i class="fas fa-chevron-right"></i>
    </div>
    <!-- Back to Top End -->

    <!-- LOGIN MODAL -->
    <div class="modal fade" id="comment-log-reg" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title"
        aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <nav class="comment-log-reg-tabmenu">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link login active" id="nav-log-tab1" data-toggle="tab" href="#nav-log1"
                                role="tab" aria-controls="nav-log" aria-selected="true">
                                <?php echo e($langg->lang197); ?>

                            </a>
                            <a class="nav-item nav-link" id="nav-reg-tab1" data-toggle="tab" href="#nav-reg1" role="tab"
                                aria-controls="nav-reg" aria-selected="false">
                                <?php echo e($langg->lang198); ?>

                            </a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-log1" role="tabpanel"
                            aria-labelledby="nav-log-tab1">
                            <div class="login-area">
                                <div class="header-area">
                                    <h4 class="title"><?php echo e($langg->lang172); ?></h4>
                                </div>
                                <div class="login-form signin-form">
                                    <?php echo $__env->make('includes.admin.form-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <form class="mloginform" action="<?php echo e(route('user.login.submit')); ?>" method="POST">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="form-input">
                                            <input type="email" name="email" placeholder="<?php echo e($langg->lang173); ?>"
                                                required="">
                                            <i class="icofont-user-alt-5"></i>
                                        </div>
                                        <div class="form-input">
                                            <input type="password" class="Password" name="password"
                                                placeholder="<?php echo e($langg->lang174); ?>" required="">
                                            <i class="icofont-ui-password"></i>
                                        </div>
                                        <div class="form-forgot-pass">
                                            <div class="left">
                                                <input type="checkbox" name="remember" id="mrp"
                                                    <?php echo e(old('remember') ? 'checked' : ''); ?>>
                                                <label for="mrp"><?php echo e($langg->lang175); ?></label>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:;" id="show-forgot">
                                                    <?php echo e($langg->lang176); ?>

                                                </a>
                                            </div>
                                        </div>
                                        <input type="hidden" name="modal" value="1">
                                        <input class="mauthdata" type="hidden" value="<?php echo e($langg->lang177); ?>">
                                        <button type="submit" class="submit-btn"><?php echo e($langg->lang178); ?></button>
                                        <?php if(App\Models\Socialsetting::find(1)->f_check == 1 ||
                                        App\Models\Socialsetting::find(1)->g_check == 1): ?>
                                        <div class="social-area">
                                            <h3 class="title"><?php echo e($langg->lang179); ?></h3>
                                            <p class="text"><?php echo e($langg->lang180); ?></p>
                                            <ul class="social-links">
                                                <?php if(App\Models\Socialsetting::find(1)->f_check == 1): ?>
                                                <li>
                                                    <a href="<?php echo e(route('social-provider','facebook')); ?>">
                                                        <i class="fab fa-facebook-f"></i>
                                                    </a>
                                                </li>
                                                <?php endif; ?>
                                                <?php if(App\Models\Socialsetting::find(1)->g_check == 1): ?>
                                                <li>
                                                    <a href="<?php echo e(route('social-provider','google')); ?>">
                                                        <i class="fab fa-google-plus-g"></i>
                                                    </a>
                                                </li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-reg1" role="tabpanel" aria-labelledby="nav-reg-tab1">
                            <div class="login-area signup-area">
                                <div class="header-area">
                                    <h4 class="title"><?php echo e($langg->lang181); ?></h4>
                                </div>
                                <div class="login-form signup-form">
                                    <?php echo $__env->make('includes.admin.form-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <form class="mregisterform" action="<?php echo e(route('user-register-submit')); ?>"
                                        method="POST">
                                        <?php echo e(csrf_field()); ?>


                                        <div class="form-input">
                                            <input type="text" class="User Name" name="name"
                                                placeholder="<?php echo e($langg->lang182); ?>" required="">
                                            <i class="icofont-user-alt-5"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="email" class="User Name" name="email"
                                                placeholder="<?php echo e($langg->lang183); ?>" required="">
                                            <i class="icofont-email"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="text" class="User Name" name="phone"
                                                placeholder="<?php echo e($langg->lang184); ?>" required="">
                                            <i class="icofont-phone"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="text" class="User Name" name="address"
                                                placeholder="<?php echo e($langg->lang185); ?>" required="">
                                            <i class="icofont-location-pin"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="password" class="Password" name="password"
                                                placeholder="<?php echo e($langg->lang186); ?>" required="">
                                            <i class="icofont-ui-password"></i>
                                        </div>

                                        <div class="form-input">
                                            <input type="password" class="Password" name="password_confirmation"
                                                placeholder="<?php echo e($langg->lang187); ?>" required="">
                                            <i class="icofont-ui-password"></i>
                                        </div>


                                        <?php if($gs->is_capcha == 1): ?>

                                        <ul class="captcha-area">
                                            <li>
                                                <p><img class="codeimg1"
                                                        src="<?php echo e(asset("assets/images/capcha_code.png")); ?>" alt=""> <i
                                                        class="fas fa-sync-alt pointer refresh_code "></i></p>
                                            </li>
                                        </ul>

                                        <div class="form-input">
                                            <input type="text" class="Password" name="codes"
                                                placeholder="<?php echo e($langg->lang51); ?>" required="">
                                            <i class="icofont-refresh"></i>
                                        </div>


                                        <?php endif; ?>

                                        <input class="mprocessdata" type="hidden" value="<?php echo e($langg->lang188); ?>">
                                        <button type="submit" class="submit-btn"><?php echo e($langg->lang189); ?></button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGIN MODAL ENDS -->

    <!-- FORGOT MODAL -->
    <div class="modal fade" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title"
        aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="login-area">
                        <div class="header-area forgot-passwor-area">
                            <h4 class="title"><?php echo e($langg->lang191); ?> </h4>
                            <p class="text"><?php echo e($langg->lang192); ?> </p>
                        </div>
                        <div class="login-form">
                            <?php echo $__env->make('includes.admin.form-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <form id="mforgotform" action="<?php echo e(route('user-forgot-submit')); ?>" method="POST">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-input">
                                    <input type="email" name="email" class="User Name"
                                        placeholder="<?php echo e($langg->lang193); ?>" required="">
                                    <i class="icofont-user-alt-5"></i>
                                </div>
                                <div class="to-login-page">
                                    <a href="javascript:;" id="show-login">
                                        <?php echo e($langg->lang194); ?>

                                    </a>
                                </div>
                                <input class="fauthdata" type="hidden" value="<?php echo e($langg->lang195); ?>">
                                <button type="submit" class="submit-btn"><?php echo e($langg->lang196); ?></button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- FORGOT MODAL ENDS -->


<!-- VENDOR LOGIN MODAL -->
    <div class="modal fade" id="vendor-login" tabindex="-1" role="dialog" aria-labelledby="vendor-login-Title" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" style="transition: .5s;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <nav class="comment-log-reg-tabmenu">
                    <div class="nav nav-tabs" id="nav-tab1" role="tablist">
                        <a class="nav-item nav-link login active" id="nav-log-tab11" data-toggle="tab" href="#nav-log11" role="tab" aria-controls="nav-log" aria-selected="true">
                            Retailer Login
                        </a>
                        <a class="nav-item nav-link" id="nav-reg-tab11" data-toggle="tab" href="#nav-reg11" role="tab" aria-controls="nav-reg" aria-selected="false">
                            Retailer Register
                        </a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-log11" role="tabpanel" aria-labelledby="nav-log-tab">
                        <div class="login-area">
                          <div class="login-form signin-form">
                                <?php echo $__env->make('includes.admin.form-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <form class="mloginform" action="<?php echo e(route('user.login.submit')); ?>" method="POST">
                              <?php echo e(csrf_field()); ?>

                              <div class="form-input">
                                <input type="email" name="email" placeholder="<?php echo e($langg->lang173); ?>" required="">
                                <i class="icofont-user-alt-5"></i>
                              </div>
                              <div class="form-input">
                                <input type="password" class="Password" name="password" placeholder="<?php echo e($langg->lang174); ?>" required="">
                                <i class="icofont-ui-password"></i>
                              </div>
                              <div class="form-forgot-pass">
                                <div class="left">
                                  <input type="checkbox" name="remember"  id="mrp1" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                                  <label for="mrp1"><?php echo e($langg->lang175); ?></label>
                                </div>
                                <div class="right">
                                  <a href="javascript:;" id="show-forgot1">
                                    <?php echo e($langg->lang176); ?>

                                  </a>
                                </div>
                              </div>
                              <input type="hidden" name="modal"  value="1">
                               <input type="hidden" name="vendor"  value="1">
                              <input class="mauthdata" type="hidden"  value="<?php echo e($langg->lang177); ?>">
                              <button type="submit" class="submit-btn"><?php echo e($langg->lang178); ?></button>
                                  <?php if(App\Models\Socialsetting::find(1)->f_check == 1 || App\Models\Socialsetting::find(1)->g_check == 1): ?>
                                  <div class="social-area">
                                      <h3 class="title"><?php echo e($langg->lang179); ?></h3>
                                      <p class="text"><?php echo e($langg->lang180); ?></p>
                                      <ul class="social-links">
                                        <?php if(App\Models\Socialsetting::find(1)->f_check == 1): ?>
                                        <li>
                                          <a href="<?php echo e(route('social-provider','facebook')); ?>">
                                            <i class="fab fa-facebook-f"></i>
                                          </a>
                                        </li>
                                        <?php endif; ?>
                                        <?php if(App\Models\Socialsetting::find(1)->g_check == 1): ?>
                                        <li>
                                          <a href="<?php echo e(route('social-provider','google')); ?>">
                                            <i class="fab fa-google-plus-g"></i>
                                          </a>
                                        </li>
                                        <?php endif; ?>
                                      </ul>
                                  </div>
                                  <?php endif; ?>
                            </form>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-reg11" role="tabpanel" aria-labelledby="nav-reg-tab">
                <div class="login-area signup-area">
                    <div class="login-form signup-form">
                       <?php echo $__env->make('includes.admin.form-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <form class="mregisterform" action="<?php echo e(route('user-register-submit')); ?>" method="POST">
                          <?php echo e(csrf_field()); ?>


                          <div class="row">

                          <div class="col-lg-6">
                            <div class="form-input">
                                <input type="text" class="User Name" name="name" placeholder="<?php echo e($langg->lang182); ?>" required="">
                                <i class="icofont-user-alt-5"></i>
                                </div>
                           </div>

                           <div class="col-lg-6">
 <div class="form-input">
                                <input type="email" class="User Name" name="email" placeholder="<?php echo e($langg->lang183); ?>" required="">
                                <i class="icofont-email"></i>
                            </div>

                            </div>

                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="shop_name" placeholder="Business Name" required="">
                                    <i class="icofont-cart"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="address" placeholder="Shipping Address" required="">
                                    <i class="icofont-location-pin"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="shop_address" placeholder="Business Address" required="">
                                    <i class="icofont-opencart"></i>
                                </div>
                            </div>

                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="shop_number" placeholder="Business Number" required="">
                                    <i class="icofont-shopping-cart"></i>
                                </div>
                            </div>

                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="phone" placeholder="Contact number" required="">
                                    <i class="icofont-phone"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="city" placeholder="City" required="">
                                    <i class="icofont-ui-cart"></i>
                                </div>
                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                    <input type="text" class="User Name" name="zip" placeholder="ZIP" required="">
                                    <i class="icofont-ui-cart"></i>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="file-upload-area">
                                    <div class="upload-file">
                                        <input type="file" name="personal_id" class="upload">
                                        <span>Personal ID</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="file-upload-area">
                                    <div class="upload-file">
                                        <input type="file" name="tax_id" class="upload">
                                        <span>Tax ID</span>
                                    </div>
                                </div>
                            </div>

                            

                           <div class="col-lg-6">
  <div class="form-input">
                                <input type="password" class="Password" name="password" placeholder="<?php echo e($langg->lang186); ?>" required="">
                                <i class="icofont-ui-password"></i>
                            </div>

                            </div>
                           <div class="col-lg-6">
                                <div class="form-input">
                                <input type="password" class="Password" name="password_confirmation" placeholder="<?php echo e($langg->lang187); ?>" required="">
                                <i class="icofont-ui-password"></i>
                                </div>
                            </div>

                            <?php if($gs->is_capcha == 1): ?>

<div class="col-lg-6">


                            <ul class="captcha-area">
                                <li>
                                    <p>
                                        <img class="codeimg1" src="<?php echo e(asset("assets/images/capcha_code.png")); ?>" alt=""> <i class="fas fa-sync-alt pointer refresh_code "></i>
                                    </p>

                                </li>
                            </ul>


</div>

<div class="col-lg-6">

 <div class="form-input">
                                <input type="text" class="Password" name="codes" placeholder="<?php echo e($langg->lang51); ?>" required="">
                                <i class="icofont-refresh"></i>

                            </div>



                          </div>

                          <?php endif; ?>

                            <input type="hidden" name="vendor"  value="1">
                            <input class="mprocessdata" type="hidden"  value="<?php echo e($langg->lang188); ?>">
                            <button type="submit" class="submit-btn"><?php echo e($langg->lang189); ?></button>

                            </div>




                        </form>
                    </div>
                </div>
                    </div>
                </div>
      </div>
    </div>
  </div>
</div>
<!-- VENDOR LOGIN MODAL ENDS -->

<!-- Product Quick View Modal -->

      <div class="modal fade" id="quickview" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog quickview-modal modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="submit-loader">
                <img src="<?php echo e(asset('assets/images/'.$gs->loader)); ?>" alt="">
            </div>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="container quick-view-modal">

                </div>
            </div>
          </div>
        </div>
      </div>
<!-- Product Quick View Modal -->

<!-- Order Tracking modal Start-->
    <div class="modal fade" id="track-order-modal" tabindex="-1" role="dialog" aria-labelledby="order-tracking-modal" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"> <b><?php echo e($langg->lang772); ?></b> </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                        <div class="order-tracking-content">
                            <form id="track-form" class="track-form">
                                <?php echo e(csrf_field()); ?>

                                <input type="text" id="track-code" placeholder="<?php echo e($langg->lang773); ?>" required="">
                                <button type="submit" class="mybtn1"><?php echo e($langg->lang774); ?></button>
                                <a href="#"  data-toggle="modal" data-target="#order-tracking-modal"></a>
                            </form>
                        </div>

                        <div>
                            <div class="submit-loader d-none">
                                <img src="<?php echo e(asset('assets/images/'.$gs->loader)); ?>" alt="">
                            </div>
                            <div id="track-order">

                            </div>
                        </div>

            </div>
            </div>
        </div>
    </div>
<!-- Order Tracking modal End -->

<script type="text/javascript">
  var mainurl = "<?php echo e(url('/')); ?>";
  var gs      = <?php echo json_encode($gs); ?>;
  var langg    = <?php echo json_encode($langg); ?>;
</script>

    <!-- jquery -->
    
    <script src="<?php echo e(asset('assets/front/js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/js/vue.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/jquery-ui/jquery-ui.min.js')); ?>"></script>
    <!-- popper -->
    <script src="<?php echo e(asset('assets/front/js/popper.min.js')); ?>"></script>
    <!-- bootstrap -->
    <script src="<?php echo e(asset('assets/front/js/bootstrap.min.js')); ?>"></script>
    <!-- plugin js-->
    <script src="<?php echo e(asset('assets/front/js/plugin.js')); ?>"></script>

    <script src="<?php echo e(asset('assets/front/js/xzoom.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/js/jquery.hammer.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/js/setup.js')); ?>"></script>

    <script src="<?php echo e(asset('assets/front/js/toastr.js')); ?>"></script>
    <!-- main -->
    <script src="<?php echo e(asset('assets/front/js/main.js')); ?>"></script>
    <!-- custom -->
    <script src="<?php echo e(asset('assets/front/js/custom.js')); ?>"></script>

    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>

    <?php echo $seo->google_analytics; ?>


    <?php if($gs->is_talkto == 1): ?>
        <!--Start of Tawk.to Script-->
        <?php echo $gs->talkto; ?>

        <!--End of Tawk.to Script-->
    <?php endif; ?>

    <?php echo $__env->yieldContent('scripts'); ?>


    <script src="<?php echo e(asset('assets/front/assets/JavaScript/main.js')); ?>"></script>
    
    <script>
        var img = <?php echo json_encode($img_array); ?>; 
        
        $('.cat-img').on('mouseover',function(){
            
            $('#cat-side-img').attr("src", "//horizonwirelesstx.com/assets/images/categories/"+img[$(this).attr('id')]);
            
        });
    </script>


</body>

</html>