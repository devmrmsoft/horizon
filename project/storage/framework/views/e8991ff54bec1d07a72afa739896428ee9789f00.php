<?php
use App\Models\Subcategory;
$filterBrands = DB::table('categories')->where('status',1)->get();

?>
        <div class="col-lg-3 col-md-6">
          <div class="left-area filter-column" id="Filters">
            <a class="closebtn" href="javascript:void(0)" onclick="closeFilter()"><i class="fas fa-times" aria-hidden="true"></i>
            </a> 

            <?php if((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) && !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) && !empty(json_decode($childcat->attributes, true)))): ?>

              <div class="tags-area">
                <div class="header-area">
                  <h4 class="title">
                      Filters
                  </h4>
                </div>
                <div class="body-area">
                  <form id="attrForm" action="<?php echo e(route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])); ?>" method="post">
                    <ul class="filter">
                      <div class="single-filter">
                        <?php if(!empty($cat) && !empty(json_decode($cat->attributes, true))): ?>
                          <?php $__currentLoopData = $cat->attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $attr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="my-2 sub-title">
                              <span><i class="fas fa-arrow-alt-circle-right"></i> <?php echo e($attr->name); ?></span>
                            </div>
                            <?php if(!empty($attr->attribute_options)): ?>
                              <?php $__currentLoopData = $attr->attribute_options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="form-check ml-0 pl-0">
                                  <input name="<?php echo e($attr->input_name); ?>[]" class="form-check-input attribute-input" type="checkbox" id="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>" value="<?php echo e($option->name); ?>">
                                  <label class="form-check-label" for="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>"><?php echo e($option->name); ?></label>
                                </div>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>


                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> Brands</span>
                        </div>
                        <?php if(isset($cat->id)): ?>
                          <?php 
                            $getCatModels   = Subcategory::where('category_id',$cat->id)->get();
                          ?>

                          <?php $__currentLoopData = $getCatModels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterBrand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <div class="form-check ml-0 pl-0">
                            <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-<?php echo e($filterBrand->id); ?>" value="<?php echo e($filterBrand->id); ?>" onchange='brandFilterChange(this);'>
                            <label class="form-check-label" for="filter_brands-<?php echo e($filterBrand->id); ?>"><?php echo e($filterBrand->name); ?> : Test</label>
                          </div>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> Model</span>
                        </div>
                        <div class="form-check ml-0 pl-0 " id="load-models">
                        </div>
                        <?php if(!empty($subcat) && !empty(json_decode($subcat->attributes, true))): ?>
                          <?php $__currentLoopData = $subcat->attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $attr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <div class="my-2 sub-title">
                            <span><i class="fas fa-arrow-alt-circle-right"></i> <?php echo e($attr->name); ?></span>
                          </div>
                            <?php if(!empty($attr->attribute_options)): ?>
                              <?php $__currentLoopData = $attr->attribute_options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="form-check  ml-0 pl-0">
                                  <input name="<?php echo e($attr->input_name); ?>[]" on class="form-check-input attribute-input" type="checkbox" id="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>" value="<?php echo e($option->name); ?>">
                                  <label class="form-check-label" for="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>"><?php echo e($option->name); ?></label>
                                </div>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>

                        <?php if(!empty($childcat) && !empty(json_decode($childcat->attributes, true))): ?>
                          <?php $__currentLoopData = $childcat->attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $attr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <div class="my-2 sub-title">
                            <span><i class="fas fa-arrow-alt-circle-right"></i> <?php echo e($attr->name); ?></span>
                          </div>
                            <?php if(!empty($attr->attribute_options)): ?>
                              <?php $__currentLoopData = $attr->attribute_options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="form-check  ml-0 pl-0">
                                  <input name="<?php echo e($attr->input_name); ?>[]" class="form-check-input attribute-input" type="checkbox" id="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>" value="<?php echo e($option->name); ?>">
                                  <label class="form-check-label" for="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>"><?php echo e($option->name); ?></label>
                                </div>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                      </div>
                    </ul>
                  </form>
                </div>
              </div>
            <?php else: ?>

              <div class="tags-area">
                <div class="header-area">
                  <h4 class="title">
                      Filters
                  </h4>
                </div>
                <div class="body-area">
                  <form id="attrForm" action="<?php echo e(route('front.devices', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])); ?>" method="post">
                    <ul class="filter">
                      <div class="single-filter">

                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> Brands</span>
                        </div>
                    
                          <?php 
                            if(isset($cat->id)){
                                $getCatModels   = Subcategory::where('category_id',$cat->id)->get();
                            ?>

                        <?php } else{ ?>
                        <?php if(isset($deviceCats)): ?>

                          <?php $__currentLoopData = $deviceCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterBrand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <div class="form-check ml-0 pl-0">
                            <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-<?php echo e($filterBrand->id); ?>" value="<?php echo e($filterBrand->id); ?>" onchange='brandFilterChange(this);'>
                            <label class="form-check-label" for="filter_brands-<?php echo e($filterBrand->id); ?>"><?php echo e($filterBrand->name); ?></label>
                          </div>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <?php } ?>
                        <div class="my-2 sub-title" id="load-models-after">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> Model</span>
                        </div>
                        <div id="load-models">
                            
                            <?php if(isset($cat->id)): ?>
                              <?php 
                                $getCatModels   = Subcategory::where('category_id',$cat->id)->get();
                                if(isset($subcat->id)){
                                }else{
                              ?>
    
                              <?php $__currentLoopData = $getCatModels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterBrand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <div class="form-check ml-0 pl-0">
                                <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-<?php echo e($filterBrand->id); ?>" value="<?php echo e($filterBrand->id); ?>" onchange='brandFilterChange(this);'>
                                <label class="form-check-label" for="filter_brands-<?php echo e($filterBrand->id); ?>"><?php echo e($filterBrand->name); ?></label>
                              </div>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              <?php } ?>
                            <?php endif; ?>
                        </div>

                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> Carriers</span>
                        </div>
                        <?php if(isset($deviceCarriers)): ?>

                          <?php $__currentLoopData = $deviceCarriers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterBrand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <div class="form-check ml-0 pl-0">
                            <input name="filter_device_carriers[]" class="form-check-input attribute-input filter_device_carriers" type="checkbox" id="filter_device_carriers-<?php echo e($filterBrand->id); ?>" value="<?php echo e($filterBrand->id); ?>">
                            <label class="form-check-label" for="filter_device_carriers-<?php echo e($filterBrand->id); ?>"><?php echo e($filterBrand->name); ?></label>
                          </div>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        
                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> Storage</span>
                        </div>
                        <?php 
                        $storage  = 8;
                        for ($i=0; $i < 7; $i++) { 

                          ?>
                          <div class="form-check ml-0 pl-0">
                            <input name="filter_device_storage[]" class="form-check-input attribute-input filter_device_storage" type="checkbox" id="filter_device_storage-<?php echo e($storage); ?>" value="<?php echo e($storage); ?>">
                            <label class="form-check-label" for="filter_device_storage-<?php echo e($storage); ?>"><?php echo e($storage); ?> GB</label>
                          </div>
                          <?php
                          $storage  +=  $storage;
                        }
                        ?>

                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i>Phone Color</span>
                        </div>
                        <?php 
                        $colors  = [0 => 'Red', 1 => 'Blue', 2 => 'Black', 3 => 'Silver'];
                        for ($i=0; $i < count($colors); $i++) {

                          ?>
                          <div class="form-check ml-0 pl-0">
                            <input name="filter_device_color[]" class="form-check-input attribute-input" type="checkbox" id="filter_device_color-<?php echo e($i); ?>" value="<?php echo e($colors[$i]); ?>">
                            <label class="form-check-label" for="filter_device_color-<?php echo e($i); ?>"><?php echo e($colors[$i]); ?></label>
                          </div>
                          <?php
                          $storage  +=  $storage;
                        }
                        ?>

                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i>Phone Conditions</span>
                        </div>
                        <?php 
                        $conditions  = [1 => 'A', 2 => 'A-', 3 => 'B', 4 => 'C', 5 => 'A/B'];
                        for ($i=1; $i <= count($conditions); $i++) {
                          ?>
                          <div class="form-check ml-0 pl-0">
                            <input name="filter_device_conditions[]" class="form-check-input attribute-input" type="checkbox" id="filter_device_conditions-<?php echo e($i); ?>" value="<?php echo e($i); ?>">
                            <label class="form-check-label" for="filter_device_conditions-<?php echo e($i); ?>"><?php echo e($conditions[$i]); ?></label>
                          </div>
                          <?php
                        }
                        ?>
                      </div>
                    </ul>
                  </form>
                </div>
              </div>
            <?php endif; ?>
            
            <div class="filter-result-area">
              <div class="header-area">
                <h4 class="title">
                  <?php echo e($langg->lang61); ?>

                </h4>
              </div>
              <div class="body-area">
                <form id="catalogForm" action="<?php echo e(route('front.getCatModelpro', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])); ?>" method="GET">
                  <?php if(!empty(request()->input('search'))): ?>
                    <input type="hidden" name="search" value="<?php echo e(request()->input('search')); ?>">
                  <?php endif; ?>
                  <?php if(!empty(request()->input('sort'))): ?>
                    <input type="hidden" name="sort" value="<?php echo e(request()->input('sort')); ?>">
                  <?php endif; ?>
                  

                    <div class="price-range-block">
                      <div id="slider-range" class="price-filter-range" name="rangeInput"></div>
                      <div class="livecount">
                        <input type="number" min=0  name="min"  id="min_price" class="price-range-field" />
                        <span><?php echo e($langg->lang62); ?></span>
                        <input type="number" min=0  name="max" id="max_price" class="price-range-field" />
                      </div>
                    </div>

                    <button class="filter-btn" type="submit"><?php echo e($langg->lang58); ?></button>
                </form>
              </div>
            </div>


            <?php if(!isset($vendor)): ?>

            


            <?php else: ?>

            <div class="service-center">
              <div class="header-area">
                <h4 class="title">
                    <?php echo e($langg->lang227); ?>

                </h4>
              </div>
              <div class="body-area">
                <ul class="list">
                  <li>
                      <a href="javascript:;" data-toggle="modal" data-target="<?php echo e(Auth::guard('web')->check() ? '#vendorform1' : '#comment-log-reg'); ?>">
                          <i class="icofont-email"></i> <span class="service-text"><?php echo e($langg->lang228); ?></span>
                      </a>
                  </li>
                  <li>
                        <a href="tel:+<?php echo e($vendor->shop_number); ?>">
                          <i class="icofont-phone"></i> <span class="service-text"><?php echo e($vendor->shop_number); ?></span>
                        </a>
                  </li>
                </ul>
              <!-- Modal -->
              </div>

              <div class="footer-area">
                <p class="title">
                  <?php echo e($langg->lang229); ?>

                </p>
                <ul class="list">


              <?php if($vendor->f_check != 0): ?>
              <li><a href="<?php echo e($vendor->f_url); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
              <?php endif; ?>
              <?php if($vendor->g_check != 0): ?>
              <li><a href="<?php echo e($vendor->g_url); ?>" target="_blank"><i class="fab fa-google"></i></a></li>
              <?php endif; ?>
              <?php if($vendor->t_check != 0): ?>
              <li><a href="<?php echo e($vendor->t_url); ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
              <?php endif; ?>
              <?php if($vendor->l_check != 0): ?>
              <li><a href="<?php echo e($vendor->l_url); ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
              <?php endif; ?>


                </ul>
              </div>
            </div>


            <?php endif; ?>


          </div>
        </div>
        
        <?php $__env->startSection('scripts'); ?>
        
        <script>
            function openFilter() {
                document.getElementById("Filters").style.width = "300px";
            }
            function closeFilter() {
                document.getElementById("Filters").style.width = "0";
            }
        </script>
        
        
        
        
        <?php $__env->stopSection(); ?>