
<?php $__env->startSection('content'); ?>

<div class="category-page" style="margin-top: 90px;">
    <div class="container">
        <div class="row">
            <?php $__currentLoopData = $categoryModels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-3">
                <div class="bg-white">
                    <div class="sub-category-menu">
                        <h5 class="category-name"><a href="<?php echo e(url('category')); ?>/<?php echo e($slug); ?>/<?php echo e($category->slug); ?>"><?php echo e($category->name); ?></a></h5>
                        
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>