			<?php if(count($prods) > 0): ?>
					<?php $__currentLoopData = $prods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="col-lg-4 col-md-4 col-6 remove-padding">
                                        <div class="productTab">
                                            <div class="productTab0">
                                            </div>
                                            <div class="productTab1">
                                                <a href="<?php echo e(route('front.product', $prod->slug)); ?>"><?php echo e($prod->showName()); ?></a>
                                            </div>
                                            <div class="productTab2">
                                                <a href="<?php echo e(route('front.product', $prod->slug)); ?>"><img src="<?php echo e($prod->photo ? asset('assets/images/products/'.$prod->photo):asset('assets/images/noimage.png')); ?>"
                                                        class="img-fluid"></a>
                                            </div>
                                            <div class="productTab3">
                                                <h1><?php echo e($prod->showPrice($prod->id)); ?></h1>
                                                <h2><del><?php echo e($prod->showPreviousPrice()); ?></del></h2>
                                                <a tabindex="0" data-toggle="tooltip" title="Add to Cart!" class="add-to-cart add-to-cart-btn" data-href="<?php echo e(route('product.cart.add',$prod->id)); ?>"><i
                                                        class="fas fa-shopping-cart"></i></a>
                                            </div>
                                            <div class="productTab4">

											<?php if(Auth::guard('web')->check()): ?>
                                                <a href="javascript:;" class="add-to-wish" data-href="<?php echo e(route('user-wishlist-add',$prod->id)); ?>" data-toggle="tooltip" data-placement="right" title="<?php echo e($langg->lang54); ?>" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
											<?php else: ?>
                                                <a href="javascript:;" rel-toggle="tooltip" title="<?php echo e($langg->lang54); ?>" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
                                            <?php endif; ?>
                                                <a href="javascript:;" class="add-to-compare" data-href="<?php echo e(route('product.compare.add',$prod->id)); ?>"  data-toggle="tooltip" data-placement="right" title="<?php echo e($langg->lang57); ?>" data-placement="right"><i class="fas fa-retweet"></i> Compare</a>
                                            </div>
                                        </div>
                                    </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<div class="col-lg-12">
					<div class="page-center mt-5">
						<?php echo $prods->appends(['search' => request()->input('search')])->links(); ?>

					</div>
				</div>
			<?php else: ?>
				<div class="col-lg-12">
					<div class="page-center">
						 <h4 class="text-center"><?php echo e($langg->lang60); ?></h4>
					</div>
				</div>
			<?php endif; ?>


<?php if(isset($ajax_check)): ?>


<script type="text/javascript">


// Tooltip Section


    $('[data-toggle="tooltip"]').tooltip({
      });
      $('[data-toggle="tooltip"]').on('click',function(){
          $(this).tooltip('hide');
      });




      $('[rel-toggle="tooltip"]').tooltip();

      $('[rel-toggle="tooltip"]').on('click',function(){
          $(this).tooltip('hide');
      });


// Tooltip Section Ends

</script>

<?php endif; ?>